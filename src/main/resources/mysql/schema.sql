CREATE DATABASE IF NOT EXISTS `portal` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
use `portal`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(300) NOT NULL,
  `first_name` varchar(300) DEFAULT NULL,
  `last_name` varchar(300) DEFAULT NULL,
  `username` varchar(300) DEFAULT NULL,
  `user_email` varchar(300) NOT NULL,
  `password` varchar(150) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `confirmation_status` enum('0','1') NOT NULL DEFAULT '0',
  `password_update_count` int(10) NOT NULL DEFAULT '0',
  `created_by` int(10) NOT NULL DEFAULT '0',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_time` timestamp NULL DEFAULT NULL,
  `modified_by` int(10) NOT NULL DEFAULT '0',
  `user_uuid` varchar(100) DEFAULT NULL,
  `user_status` enum('0','1') NOT NULL DEFAULT '1',
  `last_login_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(10) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) NOT NULL DEFAULT '0',
  `modified_time` timestamp NULL DEFAULT NULL,
  `modified_by` int(10) DEFAULT '0',
  `role_uuid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `groups` (
  `group_id` int(10) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(250) DEFAULT NULL,
  `created_by` int(10) NOT NULL DEFAULT '0',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(10) DEFAULT '0',
  `modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `group_uuid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `group_roles` (
  `group_role_id` int(10) NOT NULL AUTO_INCREMENT,
  `role_id` int(10) NOT NULL DEFAULT '0',
  `group_id` int(10) NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(10) DEFAULT NULL,
  `modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`group_role_id`),
  KEY `business_id` (`role_id`),
  CONSTRAINT `group_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
  CONSTRAINT `group_id` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `user_roles` (
  `user_role_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL DEFAULT '0',
  `role_id` int(10) NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(10) DEFAULT NULL,
  `modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_role_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `user_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `permission` (
  `permission_id` int(10) NOT NULL AUTO_INCREMENT,
  `permission_name` varchar(100) NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(10) DEFAULT NULL,
  `modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE DT.`business_configurations_version` (
`business_config_version_id` int(10) NOT NULL AUTO_INCREMENT,
`business_id` int(10) NOT NULL,
`email` enum('0','1') DEFAULT '0',
`sms` enum('0','1') DEFAULT '0',
`qrcode` enum('0','1') DEFAULT '0',
`shareable_link` enum('0','1') DEFAULT '0',
`text_analytics` enum('0','1') DEFAULT '0',
`created_by` int(10) NOT NULL,
`modified_by` int(10) DEFAULT NULL,
`created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
`modified_time` timestamp NULL DEFAULT NULL,
`business_config_version_uuid` varchar(100) DEFAULT NULL,
`logic` enum('0','1') DEFAULT '0',
`respondent` enum('0','1') DEFAULT '0',
`segment` enum('0','1') DEFAULT '0',
`kiosk` enum('0','1') DEFAULT '0',
`metadata_question` enum('0','1') DEFAULT '0',
`mobile_app` enum('0','1') DEFAULT '0',
`emotion_analysis` enum('0','1') DEFAULT '0',
`intent_analysis` enum('0','1') DEFAULT '0',
`trigger_toggle` enum('0','1') DEFAULT '0',
`advanced_schedule` enum('0','1') DEFAULT '0',
`english_toggle` enum('0','1') DEFAULT '0',
`arabic_toggle` enum('0','1') DEFAULT '0',
`multi_surveys` enum('0','1') DEFAULT '0',
`bitly` enum('0','1') DEFAULT '0',
`web_hooks` enum('0','1') DEFAULT '0',
`preferred_metric` enum('0','1') DEFAULT '0',
`notification` enum('0','1') DEFAULT '0',
PRIMARY KEY (`business_config_version_id`),
KEY `business_id` (`business_id`),
CONSTRAINT `version_configuration_business_id` FOREIGN KEY (`business_id`) REFERENCES `business` (`business_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `cohort_analysis` ENUM('0', '1')  DEFAULT '0' AFTER `hippa`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `cohort_analysis` ENUM('0', '1')  DEFAULT '0' AFTER `hippa`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `advanced_templates` ENUM('0', '1')  DEFAULT '0' AFTER `cohort_analysis`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `advanced_templates` ENUM('0', '1')  DEFAULT '0' AFTER `cohort_analysis`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `metric_correlation` ENUM('0', '1')  DEFAULT '1' AFTER `advanced_templates`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `metric_correlation` ENUM('0', '1')  DEFAULT '1' AFTER `advanced_templates`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `api_key` ENUM('0', '1')  DEFAULT '0' AFTER `metric_correlation`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `api_key` ENUM('0', '1')  DEFAULT '0' AFTER `metric_correlation`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `whatfix` ENUM('0', '1')  DEFAULT '0' AFTER `api_key`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `whatfix` ENUM('0', '1')  DEFAULT '0' AFTER `api_key`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `multiple_sublink` ENUM('0', '1')  DEFAULT '0' AFTER `whatfix`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `multiple_sublink` ENUM('0', '1')  DEFAULT '0' AFTER `whatfix`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `export_links` ENUM('0', '1')  DEFAULT '0' AFTER `advanced_analysis`;


CREATE TABLE IF NOT EXISTS DT.`user_activity` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `business_id` int(10) NOT NULL,
  `username` varchar(300) DEFAULT NULL,
  `user_email` varchar(300) NOT NULL,
  `location` varchar(120) NOT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  `role` varchar(120) DEFAULT NULL,
  `activity` varchar(1024) DEFAULT NULL,
  `created_by` int(10) NOT NULL DEFAULT '0',
  `created_time` timestamp NOT NULL DEFAULT current_timestamp
  `modified_by` int(10) DEFAULT NULL,

  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;


ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `advanced_analysis` ENUM('0', '1')  DEFAULT '0' AFTER `multiple_sublink`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `advanced_analysis` ENUM('0', '1')  DEFAULT '0' AFTER `multiple_sublink`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `export_links` ENUM('0', '1')  DEFAULT '0' AFTER `advanced_analysis`;

CREATE TABLE IF NOT EXISTS `text_analytics_configs` (
 `id` INT(10) NOT NULL AUTO_INCREMENT,
 `text_analytics` ENUM('0','1') DEFAULT '0',
 `approval_status` ENUM('0','1','2') DEFAULT '0',
 `approved_by` JSON DEFAULT NULL,
 `approvers` JSON DEFAULT NULL,
 `business_id` int(5) NOT NULL,
 `text_analytics_plan_id` INT(10) NOT NULL DEFAULT '0',
 `configs` JSON DEFAULT NULL,
 `config_uuid` VARCHAR(100) NOT NULL,
 `created_by` int(10) NOT NULL DEFAULT '0',
 `created_time` timestamp NOT NULL DEFAULT current_timestamp,
 `modified_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `business_id` (`business_id`),
  CONSTRAINT `text_analytics_business` FOREIGN KEY (`business_id`) REFERENCES `business` (`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;


ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `respondent_tracker` ENUM('0', '1')  DEFAULT '0' AFTER `export_links`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `respondent_tracker` ENUM('0', '1')  DEFAULT '0' AFTER `export_links`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `from_customization` ENUM('0', '1')  DEFAULT '0' AFTER `respondent_tracker`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `from_customization` ENUM('0', '1')  DEFAULT '0' AFTER `respondent_tracker`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `integrations` ENUM('0', '1')  DEFAULT '0' AFTER `from_customization`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `integrations` ENUM('0', '1')  DEFAULT '0' AFTER `from_customization`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `no_of_integrations` INT(10) NULL DEFAULT '0' AFTER `integrations`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `no_of_integrations` INT(10) NULL DEFAULT '0' AFTER `integrations`;

CREATE TABLE IF NOT EXISTS `previous_ta_config`(
 `id` INT(10) NOT NULL AUTO_INCREMENT,
 `config_id` int(10) NOT NULL,
 `configs` JSON DEFAULT NULL,
 `business_id` int(10) NOT NULL,
 `created_time` timestamp NOT NULL DEFAULT current_timestamp,
 `modified_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `no_of_kiosks` INT(10) NULL DEFAULT '-1' AFTER `no_of_integrations`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `no_of_kiosks` INT(10) NULL DEFAULT '-1' AFTER `no_of_integrations`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `dynamic_links` ENUM('0', '1')  DEFAULT '0' AFTER `no_of_kiosks`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `dynamic_links` ENUM('0', '1')  DEFAULT '0' AFTER `no_of_kiosks`;

CREATE TABLE IF NOT EXISTS `deactivate_kiosks` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `business_id` int(10)NOT NULL,
  `status` int(10) DEFAULT NULL,
  `allowed_number` int(10) DEFAULT NULL,
  `difference_number` int(10) DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `no_of_shareable_sublink` INT(10) NULL DEFAULT '-1' AFTER `dynamic_links`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `no_of_shareable_sublink` INT(10) NULL DEFAULT '-1' AFTER `dynamic_links`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `no_of_multiple_static_qr` INT(10) NULL DEFAULT '-1' AFTER `no_of_shareable_sublink`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `no_of_multiple_static_qr` INT(10) NULL DEFAULT '-1' AFTER `no_of_shareable_sublink`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `no_of_dynamic_switch_qr` INT(10) NULL DEFAULT '-1' AFTER `no_of_multiple_static_qr`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `no_of_dynamic_switch_qr` INT(10) NULL DEFAULT '-1' AFTER `no_of_multiple_static_qr`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `no_of_dynamic_group_qr` INT(10) NULL DEFAULT '-1' AFTER `no_of_dynamic_switch_qr`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `no_of_dynamic_group_qr` INT(10) NULL DEFAULT '-1' AFTER `no_of_dynamic_switch_qr`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `focus_metric` json DEFAULT null AFTER `mfa`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `focus_metric` json DEFAULT null AFTER `mfa`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `advanced_report` ENUM('0', '1')  DEFAULT '0' AFTER `focus_metric`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `advanced_report` ENUM('0', '1')  DEFAULT '0' AFTER `focus_metric`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `previously_used_question_library` ENUM('0', '1')  DEFAULT '0' AFTER `advanced_report`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `previously_used_question_library` ENUM('0', '1')  DEFAULT '0' AFTER `advanced_report`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `predefined_question_library` ENUM('0', '1')  DEFAULT '0' AFTER `previously_used_question_library`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `predefined_question_library` ENUM('0', '1')  DEFAULT '0' AFTER `previously_used_question_library`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `close_loop` ENUM('0', '1')  DEFAULT '0' AFTER `custom_dashboard`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `close_loop` ENUM('0', '1')  DEFAULT '0' AFTER `custom_dashboard`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `program_throttling` ENUM('0', '1')  DEFAULT '0' AFTER `close_loop`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `program_throttling` ENUM('0', '1')  DEFAULT '0' AFTER `close_loop`;

CREATE TABLE if not exists DT.`custom_eux_themes` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `theme_name` VARCHAR(300) NOT NULL,
  `business_id` JSON NULL,
  `theme_uuid` VARCHAR(100) NOT NULL,
  `page_theme_style_code` text NULL,
  `page_theme_bg_img` VARCHAR(200) NULL,
  `buttons_style_code` text NULL,
  `progress_bar_style_code` text NULL,
  `created_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_time` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `smiley_rating_style_code` text NULL,
  `smiley_rating_bg_img` JSON NULL,
  `star_rating_style_code` text NULL,
  `star_rating_bg_img` JSON NULL,
  `thumb_rating_style_code` text NULL,
  `thumb_rating_bg_img` JSON NULL,
  `slider_rating_style_code` text NULL,
  `slider_rating_bg_img` text NULL,
  `scale_rating_style_code` text NULL,
  `scale_rating_bg_img` JSON NULL,
  `multiple_response_style_code` text NULL,
  `muliple_response_bg_img` JSON NULL,
  `single_response_style_code` text NULL,
  `single_response_bg_img` JSON NULL,
  `dropdown_style_code` text NULL,
  `open_ended_style_code` text NULL,
  `nps_style_code` text NULL,
  `nps_bg_img` JSON NULL,
  `ranking_style_code` text NULL,
  `ranking_bg_img` text NULL,
  `matrix_rating_style_code` text NULL,
  `matrix_rating_bg_img` JSON NULL,
  PRIMARY KEY (id)
);


ALTER TABLE DT.business_configurations
ADD COLUMN chatbot ENUM('0', '1')  DEFAULT '0' AFTER custom_themes,
ADD COLUMN audit_program ENUM('0', '1')  DEFAULT '0' AFTER chatbot,
ADD COLUMN collaboration ENUM('0', '1')  DEFAULT '0' AFTER audit_program,
ADD COLUMN custom_dashboard ENUM('0', '1')  DEFAULT '0' AFTER collaboration;

ALTER TABLE DT.business_configurations_version
ADD COLUMN chatbot ENUM('0', '1')  DEFAULT '0' AFTER custom_themes,
ADD COLUMN audit_program ENUM('0', '1')  DEFAULT '0' AFTER chatbot,
ADD COLUMN collaboration ENUM('0', '1')  DEFAULT '0' AFTER audit_program,
ADD COLUMN custom_dashboard ENUM('0', '1')  DEFAULT '0' AFTER collaboration;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `custom_themes` ENUM('0', '1')  DEFAULT '0' AFTER `predefined_question_library`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `custom_themes` ENUM('0', '1')  DEFAULT '0' AFTER `predefined_question_library`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `whatsapp` ENUM('0', '1')  DEFAULT '0' AFTER `collaboration`,
ADD COLUMN `no_of_whatsapp` INT(10) NULL DEFAULT '0' AFTER `whatsapp`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `whatsapp` ENUM('0', '1')  DEFAULT '0' AFTER `collaboration`,
ADD COLUMN `no_of_whatsapp` INT(10) NULL DEFAULT '0' AFTER `whatsapp`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `r_coefficient` ENUM('0','1') DEFAULT '0' AFTER `no_of_whatsapp`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `r_coefficient` ENUM('0','1') DEFAULT '0' AFTER `no_of_whatsapp`;

ALTER TABLE `DT`.`plan_configurations`
ADD COLUMN `r_coefficient` ENUM('0','1') DEFAULT '0' AFTER `no_of_whatsapp`;

ALTER TABLE `DT`.`business_usage` ADD COLUMN `whatsapp` INT(10) NULL DEFAULT '0' AFTER `kiosks`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `game_plan` ENUM('0', '1')  DEFAULT '0' AFTER `text_analytics_config_id`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `game_plan` ENUM('0', '1')  DEFAULT '0' AFTER `text_analytics_config_id`;

ALTER TABLE `DT`.`plan_configurations`
ADD COLUMN `game_plan` ENUM('0','1') DEFAULT '0' AFTER `program_throttling`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `notes` ENUM('0', '1')  DEFAULT '0' AFTER `game_plan`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `notes` ENUM('0', '1')  DEFAULT '0' AFTER `game_plan`;

ALTER TABLE `DT`.`plan_configurations`
ADD COLUMN `notes` ENUM('0','1') DEFAULT '0' AFTER `game_plan`;

ALTER TABLE `DT`.`plan_configurations`
ADD COLUMN `active` ENUM('0','1') DEFAULT '0' AFTER `r_coefficient`;

ALTER TABLE `DT`.`plan_configurations`
ADD COLUMN `pricing` int(10) DEFAULT 0 AFTER `active`;

ALTER TABLE DT.plan_configurations
ADD COLUMN `trial_days` int(3) DEFAULT 30 AFTER `pricing`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `unique_links` json DEFAULT null AFTER `submission_delay`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `unique_links` json DEFAULT null AFTER `submission_delay`;

ALTER TABLE `DT`.`plan_configurations`
ADD COLUMN `unique_links` json DEFAULT null AFTER `trial_days`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `program_features` json DEFAULT null AFTER `submission_delay`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `program_features` json DEFAULT null AFTER `submission_delay`;

ALTER TABLE `DT`.`plan_configurations`
ADD COLUMN `program_features` json DEFAULT null AFTER `trial_days`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `refresh_kiosk_qr` ENUM('0', '1')  DEFAULT '0' AFTER `unique_links`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `refresh_kiosk_qr` ENUM('0', '1')  DEFAULT '0' AFTER `unique_links`;

ALTER TABLE `DT`.`plan_configurations`
ADD COLUMN `plan_properties` json DEFAULT null AFTER `unique_links`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `ai_surveys_limit` int  DEFAULT '3' AFTER `refresh_kiosk_qr`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `ai_surveys_limit` int  DEFAULT '3' AFTER `refresh_kiosk_qr`;

ALTER TABLE `DT`.`plan_configurations`
ADD COLUMN `sso` json DEFAULT null AFTER `plan_properties`;

ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `sso` json DEFAULT null AFTER `ai_surveys_limit`;

ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `sso` json DEFAULT null AFTER `ai_surveys_limit`;


ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `ai_surveys` ENUM('0','1') DEFAULT '0' ;
ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `ai_surveys` ENUM('0','1') DEFAULT '0' ;

ALTER TABLE `DT`.`plan_configurations`
ADD COLUMN `lti` ENUM('0','1') DEFAULT '0';
ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `lti` ENUM('0','1') DEFAULT '0' ;
ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `lti` ENUM('0','1') DEFAULT '0' ;

ALTER TABLE `DT`.`plan_configurations`
ADD COLUMN `lti_config` JSON DEFAULT NULL;
ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `lti_config` JSON DEFAULT NULL ;
ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `lti_config` JSON DEFAULT NULL ;

ALTER TABLE `DT`.`plan_configurations`
ADD COLUMN `response_quota` ENUM('0','1') DEFAULT '0',
ADD COLUMN `recurrence` ENUM('0','1') DEFAULT '0';
ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `response_quota` ENUM('0','1') DEFAULT '0',
ADD COLUMN `recurrence` ENUM('0','1') DEFAULT '0';
ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `response_quota` ENUM('0','1') DEFAULT '0',
ADD COLUMN `recurrence` ENUM('0','1') DEFAULT '0';

ALTER TABLE `DT`.`plan_configurations`
ADD COLUMN `custom_dashboard_builder` ENUM('0','1') DEFAULT '0';
ALTER TABLE `DT`.`business_configurations`
ADD COLUMN `custom_dashboard_builder` ENUM('0','1') DEFAULT '0' ;
ALTER TABLE `DT`.`business_configurations_version`
ADD COLUMN `custom_dashboard_builder` ENUM('0','1') DEFAULT '0' ;

