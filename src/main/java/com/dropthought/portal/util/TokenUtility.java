package com.dropthought.portal.util;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.xml.bind.DatatypeConverter;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;
import java.util.UUID;

;

/**
 * Created by Prabhakar Thakur on 3/7/2020
 */
@Component
public class TokenUtility {

    /**
     * Logger Factory Initialized
     */
    protected static final Logger logger = LoggerFactory.getLogger(TokenUtility.class);

    private static final String lCase = "abcdefghijklmnopqrstuvwxyz";
    private static final String uCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String sChar = "!@#$%^&*(){}?[]";
    private static final String intChar = "0123456789";


    /**
     * function to generate a token
     */
    public String generateToken() {
        String user_token = "";
        try {
            SecureRandom random = new SecureRandom();
            user_token = new BigInteger(130, random).toString(32);
            //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            //System.out.println("user_token is"+user_token);
        } catch (Exception e) {
            //  logger.info("Exception occurred");
        }
        return user_token;
    }

    /**
     * function to generate UUID
     *
     * @return
     */
    public String generateUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

    /**
     * function to generate random string
     *
     * @param length
     * @return
     */
 /*   public String generateRandomString(int length) {

        StringBuilder stringBuilder = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwzyz0123456789!@#$%^&*(){}?[]";

        try {
            Random random = new Random();
            for (int i = 0; i < length; i++) {
                int index = random.nextInt(characters.length());
                stringBuilder.append(characters.charAt(index));
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }


        String generatedString = stringBuilder.toString();
        return generatedString;
    }*/

    /**
     * function to generate random string
     * OKTA password validation: (Password requirements: at least 8 characters, a lowercase letter, an uppercase letter, a number, and a symbol.
     * no parts of your username. Your password cannot be any of your last 4 passwords.)
     *
     * @param length
     * @return
     */
    public static String generateRandomString(int length) {

        StringBuilder pass = new StringBuilder();
        Random random = new Random();

        try {
            boolean isValidPassword = false;
            boolean isDigit = false;
            boolean isSmallCase = false;
            boolean isUpperCase = false;
            boolean isSymbol = false;
            while(!isValidPassword) {

                while (pass.length() != length) {
                    int randomPick = random.nextInt(4);
                    if (randomPick == 0) {
                        int spot = random.nextInt(26);
                        pass.append(lCase.charAt(spot));
                        isSmallCase = true;
                    } else if (randomPick == 1) {
                        int spot = random.nextInt(26);
                        pass.append(uCase.charAt(spot));
                        isUpperCase = true;
                    } else if (randomPick == 2) {
                        int spot = random.nextInt(8);
                        pass.append(sChar.charAt(spot));
                        isSymbol = true;
                    } else {
                        int spot = random.nextInt(10);
                        pass.append(intChar.charAt(spot));
                        isDigit = true;
                    }
                }

                if(isDigit && isSmallCase && isUpperCase && isSymbol){
                    isValidPassword = true;
                }else{
                    pass.delete(0, pass.length());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pass.toString();
    }

    /**
     * function to generate random string
     *
     * @param length
     * @return
     */
    public String generatealphaNumericRandomString(int length) {

        StringBuilder stringBuilder = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwzyz0123456789";

        try {
            Random random = new Random();
            for (int i = 0; i < length; i++) {
                int index = random.nextInt(characters.length());
                stringBuilder.append(characters.charAt(index));
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }


        String generatedString = stringBuilder.toString();
        return generatedString;
    }

    /**
     * function to generate encoded token with businessuuid
     */
    public String encodeBusinessID(String businessUUID) {
        String encodedToken = "";
        try {
            // Encode into Base64 format
            byte[] bytesEncoded   = Base64.encodeBase64(businessUUID.getBytes());
            encodedToken = new String(bytesEncoded);

        } catch (Exception e) {
        }
        return encodedToken ;
    }

//    /**
//     * function to generate encoded token with businessuuid
//     */
//    public String encodeBusinessID(String businessUUID) {
//        String encodedToken = "";
//        try {
//            // Encode into Base64 format
//            byte[] bytesEncoded   = Base64.encodeBase64(businessUUID.getBytes());
//            encodedToken = new String(bytesEncoded);
//
//        } catch (Exception e) {
//        }
//        return encodedToken ;
//    }
//
//
//
//    /**
//     *  function to decode token with businessuuid
//     * @param encodedToken
//     * @return
//     */
//    public String getBusinessUUIdFromToken(String encodedToken){
//        String[] decodedToken = new String(encodedToken).split(SQLConstants.TOKEN_DELIMITER);
//
//        if(decodedToken.length > 0) {
//            String decodedbusinessUUID = decodedToken[1];
//            byte[] valueDecoded = Base64.decodeBase64(encodedToken.getBytes());
//            return new String(valueDecoded);
//        }
//
//        return "";
//    }

    /**
     * method to get tempString for each business
     * @param cc
     * @return
     */
    public static String getTokenTempString(String cc){
        String tmpString = null;
        int equalCount = 0;
        try {
            tmpString = DatatypeConverter.printBase64Binary(cc.getBytes("UTF-8"));
            if (cc.length() > 0) {
                if (cc.length() == 1 || cc.length() == 4) {
                    equalCount = 2;
                }
                if (cc.length() == 2 || cc.length() == 5) {
                    equalCount = 1;
                }
            }
            if (equalCount == 1) {
                tmpString = tmpString.substring(0, tmpString.length() - 1);
                tmpString = tmpString + equalCount;
            }
            if (equalCount == 2) {
                tmpString = tmpString.substring(0, tmpString.length() - 2);
                tmpString = tmpString + equalCount;
            }
            if (equalCount == 0) {
                tmpString = tmpString + equalCount;
            }
        }catch (Exception e){
            logger.error("Error in  getTokenTempString() : " + e);
        }
        return tmpString;
    }

    /**
     * get random short url with 10char
     * @return
     */
    public static String generateRandomShortUrl(){
        String generatedString = RandomStringUtils.randomAlphanumeric(10);
        return generatedString;
    }

}
