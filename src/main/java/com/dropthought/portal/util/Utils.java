package com.dropthought.portal.util;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;
/**
 * Modified by Prabhakar Thakur on 7/3/2020.
 */
public class Utils {

    private static final Logger logger = LogManager.getLogger(Utils.class);

    public static final String SUCCESS = "success";
    public static final String ERROR = "error";
    public static final Integer RESET_PASS_EXPIRY = 30;
    public static final Integer WELCOME_EMAIL_EXPIRY = 2880; //30 * 2 * 24 * 2  DTV-5712 Welcome Email expiry time 48 hrs

    /**
     * Function to check given object is null
     *
     * @param obj
     * @return
     */
    public static Boolean isNull(Object obj) {
        return (obj == null) ? true : false;
    }

    /**
     * Function to check given object is not null
     *
     * @param obj
     * @return
     */
    public static Boolean isNotNull(Object obj) {
        return (obj != null) ? true : false;
    }

    /**
     * Function to check given string is not empty
     *
     * @param input
     * @return
     */
    public static Boolean notEmptyString(String input) {
        Boolean value = false;
        try {
            if (input != null && input.length() > 0) {
                value = true;
            }
        } catch (Exception e) {
            // e.printStackTrace();
        }
        return value;
    }

    /**
     * Function to check if a string is empty or null
     *
     * @param input
     * @return
     */
    public static Boolean emptyString(String input) {
        Boolean value = false;
        try {
            input = input.trim();
            if (input == null || input.length() == 0) {
                value = true;
            }
        } catch (Exception e) {
            value = true;
        }
        return value;
    }

    /**
     * function to get UTC  current time in the HH:mm:ss format
     *
     * @return currentDate
     */
    public static String getCurrentUTCDateAsString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = new Date();
        String currentDate = dateFormat.format(date);
        return currentDate;
    }

    /**
     * Compare two dates in the format yyyy-MM-dd HH:mm:ss
     *
     * @param dateString1
     * @param dateString2
     * @return
     */
    public static Boolean compareDates(String dateString1, String dateString2) {
        Boolean returnValue = false;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date1 = sdf.parse(dateString1);
            Date date2 = sdf.parse(dateString2);
            if (date1.compareTo(date2) > 0) {
                returnValue = true;
            } else if (date1.compareTo(date2) == 0) {
                returnValue = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnValue;
    }

    /**
     * Check two dates are equal in the format yyyy-MM-dd HH:mm:ss
     *
     * @param dateString1
     * @param dateString2
     * @return
     */
    public static Boolean checkEqualDates(String dateString1, String dateString2) {
        Boolean returnValue = false;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date1 = sdf.parse(dateString1);
            Date date2 = sdf.parse(dateString2);
            if (date1.compareTo(date2) == 0) {
                returnValue = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnValue;
    }

    /**
     * function to find the difference between the dates
     *
     * @param dateString1
     * @param dateString2
     * @return
     */
    public static long differenceMinutes(String dateString1, String dateString2) {
        long numberOfMins = 0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date1 = sdf.parse(dateString1);
            Date date2 = sdf.parse(dateString2);
            long difference = date2.getTime() - date1.getTime();
            numberOfMins = TimeUnit.MINUTES.convert(difference, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return numberOfMins;
    }

    /**
     * method to get datetime based on give timezone
     * @param dateString
     * @param timeZone
     * @return
     * @throws ParseException
     */
    public static String getDateAsStringByTimezone(String dateString, String timeZone) throws ParseException {

        String formattedDate = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = dateFormat.parse(dateString);

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));

        formattedDate = dateFormat.format(calendar.getTime());
        return formattedDate;
    }

    /**
     * function to check if number is convertible to String
     *
     * @param numberString
     * @return
     */
    public static boolean isStringNumber(String numberString) {
        boolean validInteger = true;
        try {
            Double number = Double.parseDouble(numberString);
        } catch (NumberFormatException e) {
            //e.printStackTrace();
            validInteger = false;
        }
        return validInteger;
    }

    /**
     * function to find the difference between the dates in days
     *
     * @param dateString1
     * @param dateString2
     * @return
     */
    public static double differenceDays(String dateString1, String dateString2) {
        double numberOfDays = 0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date1 = sdf.parse(dateString1);
            Date date2 = sdf.parse(dateString2);
            long difference = date2.getTime() - date1.getTime();
            numberOfDays = TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return numberOfDays;
    }


    /**
     * Function to calculate the total number of pages given a list of items
     *
     * @param sizeOfItems
     * @return
     */
    public static int computeTotalPages(int sizeOfItems) {
        int totalPages = 1;
        int itemsPerPage = 10;
        totalPages = (sizeOfItems) / 10;
        if (sizeOfItems % itemsPerPage > 0) {
            totalPages = totalPages + 1;
        }
        return totalPages;
    }

    /**
     *
     * @param dateString
     * @param addHours
     * @return
     */
    public static String getFormattedDateTimeAddHours(String dateString, Integer addHours) {

        String result = null;
        try {

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date newDateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateString);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(newDateString);

            calendar.add(Calendar.HOUR, addHours);
            result = df.format(calendar.getTime());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

    }

    /**
     * Function to convert to start of the day
     *
     * @return
     */
    public static String convertToStartOfDay(String givenDateStr) {
        String responseDate = "";
        try {
            if (Utils.notEmptyString(givenDateStr)) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = sdf.parse(givenDateStr);
                String givenDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
                // givenDate = givenDate.substring(0,10);
                responseDate = givenDate + " 00:00:00";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseDate;
    }

    /**
     * Function to convert to end of the day
     *
     * @return
     */

    public static String convertToEndOfDay(String givenDateStr) {
        String responseDate = "";
        try {
            if (Utils.notEmptyString(givenDateStr)) {
                //String givenDate  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(givenDateStr);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = sdf.parse(givenDateStr);
                String givenDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
                //givenDate = givenDate.substring(0,10);
                responseDate = givenDate + " 23:59:59";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseDate;
    }

    /**
     * Function to calculate the total number of pages given a list of items for survey card
     *
     * @param sizeOfItems
     * @return
     */
    public static Integer computeTotalPagesSurveyCard(Integer sizeOfItems) {
        Integer totalPages = 1;
        Integer itemsPerPage = 12;
        totalPages = (sizeOfItems) / 12;
        if (sizeOfItems % itemsPerPage > 0) {
            totalPages = totalPages + 1;
        }
        return totalPages;
    }

    /**
     * function to convert a datestring specified in a timezone to UTC in the yyyy-MM-dd HH:mm:ss format
     *
     * @param dateString
     * @param timeZone
     * @return
     * @throws ParseException
     */
    public static String getUTCDateAsString(String dateString, String timeZone) throws ParseException {

        String formattedDate = "";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
            Date date = dateFormat.parse(dateString);
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            formattedDate = dateFormat.format(calendar.getTime());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    /**
     * returns date in the format 27-May-2021 09:29 AM
     * @param dateString
     * @return
     */
    public static String getFormattedDate(String dateString) {
        String result = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date d = sdf.parse(dateString);
            sdf.applyPattern("dd-MMM-yyyy hh:mm a");
            result = sdf.format(d);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * returns date in the format 27-May-2021
     * @param dateString
     * @return
     */
    public static String getDayFormattedDate(String dateString) {
        String result = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date d = sdf.parse(dateString);
            sdf.applyPattern("dd-MMM-yyyy");
            result = sdf.format(d);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * returns date in the given format
     * @param date
     * @return
     */
    public static String getFormattedDate(String date, String outputDateFormat) {
        String formattedDate = "";
        try {
            // Remove the milliseconds part by finding the last occurrence of '.'
            if (date.contains(".")) {
                date = date.substring(0, date.lastIndexOf('.'));
            }
            // Parse the input date time string
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDate localDate = LocalDate.parse(date, formatter);

            // Format the date to "MMM dd yyyy" format
            DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern(outputDateFormat);
            formattedDate = localDate.format(outputFormatter);

        } catch (Exception e) {
            logger.error("Error in getFormattedDate() : " + e);
        }

        return formattedDate;
    }

    /**
     * function to generate encoded string
     */
    public static String encodeString(String stringToEncode) {
        String encodedStr = "";
        try {
            // Encode into Base64 format
            byte[] bytesEncoded   = Base64.encodeBase64(stringToEncode.getBytes());
            encodedStr = new String(bytesEncoded);

        } catch (Exception e) {
        }
        return encodedStr ;
    }

    /**
     *  function to decode token with businessuuid
     * @param encodedToken
     * @return
     */
    public static String decodeString(String encodedToken){
        String decodedToken = "";
        if(encodedToken.length() > 0) {
            byte[] valueDecoded = Base64.decodeBase64(encodedToken.getBytes());
            decodedToken = new String(valueDecoded);
        }
        return decodedToken;
    }

    /**
     * method to compute time to live of short url based on survey enddate and currendate
     * @param surveyEndDate
     * @return
     */
    public static String computeTTL(String surveyEndDate){
        //default 1 year
        String ttl = "8760h";
        try{
            String currentDate = getCurrentUTCDateAsString();
            Integer noOfMinutes = (int)differenceMinutes(currentDate,surveyEndDate);
            ttl = (noOfMinutes > 0 ? noOfMinutes+"m" : null) ;
        }catch (Exception e){
            logger.error("Error in computeTTL() : " + e);
        }
        return ttl;
    }

    /**
     * method to compute time to live of short url based on survey kickoff time and default/ user given expiry time
     * @param kickoffTime
     * @param expiryDate
     * @return
     */
    public static String computeTTL(String kickoffTime, String expiryDate){
        //default 1 year
        String ttl = "8760h";
        try{
            Integer noOfMinutes = (int)differenceMinutes(kickoffTime, expiryDate);
            ttl = (noOfMinutes)+"m";
        } catch (Exception e){
            e.printStackTrace();
        }
        return ttl;
    }

    /**
     * host name to resolve to EUX application
     * @param hostname
     * @return
     */
    public static String computeHostnameEUX(String hostname){
        String resolvedHostName = "";
        try{
            if(hostname.contains("test")){
                resolvedHostName = "https://test-eux.dropthought.com";
            }else if(hostname.contains("stage")){
                resolvedHostName = "https://stage-eux.dropthought.com";
            }else if(hostname.contains("automation")){
                resolvedHostName = "https://automation-eux.dropthought.com";
            } else if(hostname.contains("sandbox")){
                resolvedHostName = "https://sandbox-eux.dropthought.com";
            }else{
                resolvedHostName = "https://eux.dropthought.com";
            }
        }catch (Exception e){
            logger.error("Error in computeHostnameEUX() : " + e);
        }
        return resolvedHostName;
    }

    /**
     * Add minitues to curren UTC date time
     *
     * @param addMinutes
     * @return
     */
    public static String addMinutesToDate(int addMinutes) {
        String dateString = "";
        try {
            Calendar calendar = Calendar.getInstance();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            calendar.add(Calendar.MINUTE, addMinutes);
            dateString = df.format(calendar.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateString;
    }

    /**
     * @param dateString
     * @return
     */
    public static Date convertStringToDate(String dateString) {
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * @param dateString
     * @return
     */
    public static Date convertStringToDate(String dateString, String format) {
        Date date = null;
        try {
            date = new SimpleDateFormat(format).parse(dateString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     *
     * @param date
     * @param addHours
     * @return
     */
    public static String addHoursToDate(Date date, int addHours) {
        String dateString = "";
        try {

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            calendar.add(Calendar.HOUR_OF_DAY, addHours);
            dateString = df.format(calendar.getTime());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateString;
    }

    /**
     *
     * @param date
     * @param addDays
     * @return
     */
    public static String addDaysToDate(Date date, int addDays) {
        String dateString = "";
        try {

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            calendar.add(Calendar.DAY_OF_MONTH, addDays);
            dateString = df.format(calendar.getTime());

        } catch (Exception e) {
            logger.error("Error in addDaysToDate() : " + e);
        }
        return dateString;
    }

    /**
     *
     * @param date
     * @param addWeeks
     * @return
     */
    public static String addWeeksToDate(Date date, int addWeeks) {
        String dateString = "";
        try {

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            calendar.add(Calendar.WEEK_OF_YEAR, addWeeks);
            dateString = df.format(calendar.getTime());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateString;
    }

    /**
     *
     * @param date
     * @param addMonths
     * @return
     */
    public static String addMonthsToDate(Date date, int addMonths) {
        String dateString = "";
        try {

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            calendar.add(Calendar.MONTH, addMonths);
            dateString = df.format(calendar.getTime());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateString;
    }

    /**
     *
     * @param date
     * @param minutes
     * @return
     */
    public static String addMinutesToDate(Date date, int minutes) {

        String dateString = "";
        try {

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            calendar.add(Calendar.MINUTE, minutes);
            dateString = df.format(calendar.getTime());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateString;
    }

    /**
     * function to generate UUID
     *
     * @return
     */
    public static String generateUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
    public static Map<String, Object> successResponse() {
        Map<String, Object> response = new HashMap<>();
        response.put("success", true);
        return response;
    }

    public static Map<String, Object> failureResponse() {
        Map<String, Object> response = new HashMap<>();
        response.put("success", true);
        return response;
    }

    public static Map<String, Object> failureResponse(String message) {
        Map<String, Object> response = new HashMap<>();
        response.put("success", false);
        if(notEmptyString(message))
            response.put("message", message);
        return response;
    }

    public static long getPreviousDayTimeMillisecondsUTC() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTimeInMillis();
    }

    public static long getCurrentTimeMillisecondsUTC() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        return calendar.getTimeInMillis();

    }

    public static LocalDate convertDateToLocalDate(Date contractStartDate) {
        return contractStartDate.toInstant().atZone(TimeZone.getTimeZone("UTC").toZoneId()).toLocalDate();

    }

    public static <T> T getNotNullOrDefault(T value, T defaultValue) {
        return value != null ? value : defaultValue;
    }

    public static Object handleNullValueAndReplace(Map<String, Object> map, String key, String regex, String replacement) {
        Object value = map.remove(key);
        if (value != null) {
            return  value.toString().replaceAll(regex, replacement);
        }else{
            return null;
        }
    }

    /**
     * Function to set contract end date
     *
     * @param contractStartDate
     * @param contractEndDate
     * @return
     */
    public static String setContractEndDate(String contractStartDate, String contractEndDate) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            if (Utils.emptyString(contractEndDate)) {
                Calendar cal = Calendar.getInstance();
                Date startDate = simpleDateFormat.parse(contractStartDate);
                cal.setTime(startDate);
                cal.add(Calendar.YEAR, 1);
                contractEndDate = simpleDateFormat.format(cal.getTime());
            }

            if (Utils.notEmptyString(contractEndDate) && contractEndDate.length() > 19) {
                contractEndDate = contractEndDate.substring(0, 19);
            }
        } catch (Exception e) {
            logger.error("Error in setContractEndDate() : " + e);
        }
        return contractEndDate;
    }

    public static String retriveOnlyNumbersPhone(String phone) {
        try{
            phone = phone.replaceAll("[^0-9]", "");
            if(phone.length() > 10)
                phone = phone.substring(phone.length() - 10);

        }catch (Exception e) {
            e.printStackTrace();
        }
        return phone;
    }

    /**
     * Function to get date from date time
     *
     * @param dateTime
     * @return
     */
    public static String getDateFromDateTime(String dateTime) {

        String date = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date d = sdf.parse(dateTime);
            sdf.applyPattern("yyyy-MM-dd");
            date = sdf.format(d);

        } catch (Exception e) {
            logger.error("Error in getDateFromDateTime() : " + e);
        }

        return date;
    }


    /**
     * return button verbiage based on SSO
     * @param sso
     * @return
     */
    public static String getWelcomeButtonVerbiage(String sso){
        logger.info("SSO {} " + sso);
        String verbiage = "";

        switch (sso.toLowerCase()) {
            case "google":
                verbiage = "Sign-in with Google SSO";
                break;
            case "microsoft":
                verbiage = "Sign-in with Microsoft SSO";
                break;
            case "apple":
                verbiage = "Sign-in with Apple SSO";
                break;

        }
        logger.info("Verbiage {} " + verbiage);
        return verbiage;
    }

    /**
     * return button verbiage based on SSO
     * @param sso
     * @return
     */
    public static String getWelcomeMsg(String sso){
        logger.info("welcome msg SSO {} " + sso);
        String verbiage = "";

        switch (sso.toLowerCase()) {
            case "google":
                verbiage = Constants.KICKSTART_MSG_GOOGLE_SSO;
                break;
            case "microsoft":
                verbiage = Constants.KICKSTART_MSG_MS_SSO;
                break;
            case "apple":
                verbiage = Constants.KICKSTART_MSG_APPLE_SSO;
                break;
            default:
                verbiage = Constants.KICKSTART_MSG;
                break;

        }
        logger.info("Verbiage {} " + verbiage);
        return verbiage;
    }

    /**
     * Method to convert a input date to Mail accepted date format
     * i/p = "2024-10-04 14:26:51"  o/p = "August 04, 2024"
     * @param inputDate
     * @return
     */
    public static String getFormattedDateForAccountChangeMail(String inputDate) {

        // Define the input and output date formats
        String outputDate = "";
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        SimpleDateFormat outputFormat = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);

        try {
            // Remove the milliseconds part by finding the last occurrence of '.'
            if (inputDate.contains(".")) {
                inputDate = inputDate.substring(0, inputDate.lastIndexOf('.'));
            }
            // Parse the input date
            Date date = inputFormat.parse(inputDate);
            // Format the date to the desired output format
            outputDate = outputFormat.format(date);
        } catch (ParseException e) {
            logger.error("Error in getFormattedDateForEmail() : " + e.getMessage());
        }

        return Utils.notEmptyString(outputDate) ? outputDate : inputDate;
    }
}
