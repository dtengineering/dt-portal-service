package com.dropthought.portal.util;

import java.util.HashMap;
import java.util.Map;
/**
 * Modified by Prabhakar Thakur on 14/7/2020.
 */
public class Constants {

    public static final String OK = "ok";
    public static final String ERROR = "error";
    public static final String RESULT = "result";
    public static final String TOKEN_DELIMITER = "~~";
    public static final String KICKOFF = "kickoff";
    public static final String REMINDER_1 = "reminder_1";
    public static final String REMINDER_2 = "reminder_2";
    public static final String DATATYPE_STATUS = "_status";
    public static final String BUSINESS_ID = "business_id";
    public static final String BUSINESS_UUID = "business_uuid";
    public static final String SURVEY_ID = "survey_id";
    public static final String SURVEY_UUID = "survey_uuid";
    public static final String FROM_NAME = "from_name";
    public static final String USER_ID = "user_id";
    public static final String TAG_ID = "tag_id";
    public static final String SURVEY_INTERVAL = "survey_interval";
    public static final String TYPE = "type";
    public static final String FROM_EMAIL = "from_email";
    public static final String SUBJECT = "subject";
    public static final String MESSAGE = "message";
    public static final String HOST = "host";
    public static final String SURVEY_LOGO = "survey_logo";
    public static final String REMINDER_1_MESSAGE = "reminder_1_message";
    public static final String REMINDER_1_SUBJECT = "reminder_1_subject";
    public static final String REMINDER_2_MESSAGE = "reminder_2_message";
    public static final String REMINDER_2_SUBJECT = "reminder_2_subject";
    public static final String IMAGE_URL = "image_url";
    public static final String SMS_MESSAGE = "sms_message";
    public static final String RECIPIENT_ID = "recipient_id";
    public static final String AFTER_KICKOFF = "after_kickoff";
    public static final String AFTER_REMINDER_1 = "after_reminder_1";
    public static final String AFTER_REMINDER_2 = "after_reminder_2";
    public static final String CHANNELS = "channels";
    public static final String METADATA_TIME = "metadata_time";
    public static final String HEADER = "header";
    public static final String DATA = "data";
    public static final String EMAIL = "email";
    public static final String PHONE = "phone";
    public static final String NAME = "name";
    public static final String LANGUAGE = "language";
    public static final String DYNAMIC_ID = "dynamic_id";
    public static final String ID = "id";
    public static final String ALERT_SURVEY_ID = "alert_survey_id";
    public static final String ALERT_SURVEY_DATA = "alert_survey_data";
    public static final String LAST_ACTION = "last_action";
    public static final String LAST_ACTION_TIME = "last_action_time";
    public static final String DYNAMIC_TOKEN_ID = "dynamic_token_id";
    public static final String CREATED_TIME = "created_time";
    public static final String PARTICIPANT_ID = "participant_id";
    public static final String PARTICIPANT_GROUP_ID = "participant_group_id";
    public static final String PARTICIPANT_GROUP_UUID = "participant_group_uuid";
    public static final String CREATED_BY= "created_by";
    public static final String TIMEZONE = "timezone";
    public static final String METADATA_HEADER = "metaDataHeader";
    public static final Double EXPIRY_MONTHS = 182.5;

    public static final String BUSINESS_USAGE_ID = "business_usage_id";
    public static final String USERS = "users";
    public static final String PROGRAMS = "programs";
    public static final String RESPONSES = "responses";
    public static final String METRICS = "metrics";
    public static final String FILTERS = "filters";
    public static final String TRIGGERS = "triggers";
    public static final String EMAILS = "emails";
    public static final String API = "api";
    public static final String LISTS = "lists";
    public static final String RECIPIENTS = "recipients";
    public static final String DATA_RETENTION = "data_retention";
    public static final String CURRENT_MONTH_START = "current_month_start";
    public static final String CURRENT_MONTH_END = "current_month_end";
    public static final String USAGE_EMAIL_FLAG = "usage_email_flag";



    public static final String STATE_0_DRAFT = "0";
    public static final String STATE_1_SENT = "1";
    public static final String STATE_2_PARTIALLY_ACTIVE = "2";
    public static final String STATE_3_ACTIVE = "3";
    public static final String STATE_4_EXPIRED = "4";
    public static final String STATE_5_SCHEDULED = "5";

    public static final String SENT = "sent";
    public static final String TOBEPROCESSED = "toprocess";
    public static final String PROCESSING = "processing";
    public static final String SMS = "sms";
    public static final String WHATSAPP = "whatsapp";
    public static final String COMPLETED = "completed";
    public static final String SUCCESS = "success";
    public static final String FAILED = "failed";
    public static final String PUBLISHED = "PUBLISHED";

    public static final String STATUS = "STATUS";
    public static final String TODO = "TODO";
    public static final String NOTFOUND = "NOT_FOUND";
    public static final String DATE_FORMAT_YYYYMMDDHHMMSS = "yyyy-MM-dd HH:mm:ss";

    public static final String JOURNEY = "journey";
    public static final String NONE = "none";
    public static final String SCHEDULER = "scheduler";
    public static final String TIMEZONE_AMERICA_LOSANGELES = "America/Los_Angeles";

    public static final String DEFAULT_LOGO_IMG="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAK8AAAAkCAYAAAD/5WpuAAAABGdBTUEAALGPC/xhBQAADrdJREFUeAHtXAt0VMUZnrm7IQkCCSjoAaRFqU9qW4FsHuCjWiHmYeEUtK1YCigm2Vh7Tl8+WnMsVm0VOSfZAPLwgagVrZKHFCrViiHZKGg9xUcVrUo8Aj4Ims1r751+c+/O3bt3793cTXYDxL2cZGb++f9//vnnn3/++88NlPTjKc5blUMU+RJGiIcyMoUQNoERegJnRQnrwO82Rsm7lBA/kVw7GprLWvsxTIokpYGYGoB9OXt+6nlkVDtrv55Q5XrGyGRnVBoWpeR9wuiqLJq9ZpP/6iPx0KZwUxqw00CfxssYo8W5vmvhYf/ECDvRjpETOCX0MyLRm+qby9dRCo6pJ6WBAWggpvHOn7VhbGd3YCOMdvYAxogihRFvy0wfvnDzzsWHojpTgJQGHGrA1niLPau/xUhwO2Hsmw55xYWmhhIkbXaD//p34iJMIac0ENKApfGW5vmmKAprQsgwLpmaggEfkCRpZl1zxbvJHCfFe2hqIMp4i2bWjiY98suY7umDM2X63rCRJOfpHd7PBme81ChDRQNS1ER65Y2ADZLh8tHZab1fsY38xTBKlhQgpYEYGogw3iJPzWLCSFEM/KR0IfVWiFBlSVKYp5gOWQ3o3q60YP1IpbfzPWQWTjoas+VpNOSBT0vlgY+G9o/PMXXPy+TADfEYLn/ZopJ0Faa9QJLolYTSN7kKAO/kqTBCpXskIi3FlZujSwmeQz5C2suPTzWmpD4aGnDzQauqmPTKVl8Z4s84ZKBtDS0VfxUERZ7qK1E/Gz+bG1q9P9PhOdU3oT5KtGOVjClliH3vTtQFBsKgNUj1XRca81Bja2VSsyex5mbVV5xbcyvky8eWPw9H4P31fu/tZjzotR3LouoPjmFtg79SzMeMesy3jXMhElnf2FK5tC+h5+auPTvIuq9DaPltOMt/1LeU3y1oVOPds903E0YzQQAHUuIbB6W/9Ng6k67IW43FJE395WGkg0GMMmxHRxvISJ/sOlPYH7UxcO5QaaTleCHDDfUdc3OwlNkOaJyL4syh9SrdxdDOjZwlnNs+I2vVeOUgvSw+r2tkkdi6oiiXgmNCjDexkiWXGz5oGlOSt+YsdRQXCdS/tOzD5I54/HHHBwVjSwuqp3PJ09NG7FeNl1JlBtxyzAdH+R54si2USO8plPUyWFlMAtFJ6SYJg/Im6C7EEXiO6LIsqeKxhDsEzvOsnShLXaMpc38ZZLI6PzNpqafmBxD+CmzlqYzSb2BeJ2J3Z1JGDyPkUWXlNCU5q76vUPkGGFYuMnljgNdOKHtZYi5fXWt5o5FvSW7NUpxes3DynAn4KXAGo1EOx3y/QFiwG/pb3+CveNJIY6yDdjGTexarMIUEUaYZ+431+fOfGNa9/9Ak2UWVCee4Prr//mW9xn5ej0d2M2087ZLc2kLYQhn0kgMTwrcvNAg9fY75Y/PRN/De83ydv/wRO55O5iJosUZz5V4yl7c75Y4HxOJOFghRJeWKpMsa/N4NUX0OAI1+720CrSin5mZM6g7Rtimn2MBtwQsve/iEL460/wFn76Ju1jWOyByVr7/1IxP2LIxKmzt2bXjfMv1YLsmtvkVRgst5p9av4p2ERqFM5MJiT/VdiD95PK8+uJFcq9XC3EJd2AxsDoxzDmLwB6GPn4fg9oWQLQqDTkDc+GjggwPz0JXO59n2mtJe4vHdVe+vuEug9y17zZ1YT6yF9sSKRYtyq9cRhWhpTLx8N/ors8J0vr8oivwrta1Pm7lRHQ8YfliuTBQ+32jjpWR8cU7NpsCHB+fBiWSoc/m3fASy/7m+pbIvGwFr5pL4wFg/PpjNI90OhffLcKMYUii8z4fCazl/5uc9kfn54SMvMIX8Bgbi7IXMYBzwEgqyIx/hZegdeM3X+chFObWzFIWG4lFrWaCz3xXn1Vxi1QsvexA/z4LnVp4C1HEYW1TiqVmotyMq9DVkZmr5D2jvjOgKNTC/mdg8P0bToEeWpTDlzlJP7dUczZns7CbumUNsYQCG+NMcixrbBrzSnNoiwhTNcMFI1SOl+Iab65Kq7kPjb3P5xEgB3MFPVMMVgoA/zvPlJbm+RQIUUVLSBN5e/kMkt094XvVD8ghENCBIQHJnrBTw4hyflxHlHnR0YAc6+jwS3qYFsUq2xoNN0jeoYGoqYRQZJlDMZqdyEME8U+MgjhgylhcZxUHPyHSMNykmA3w4j1DBhCNXqqxChBKVKigZ9hRjPQgjlIex2Kre8MLFU3s7LPhvg2e7hsMRo42Xg+RNYSAwQO7FNkbRUPocnMSvo+AmABbuS8zpFfBDWMK+K7oVJi9FHR4uUnZ8glruYul/Y1LXJTCMh3TZqcxl/6egj7eUqVwGXurDjdVFScGWFq+fA7jnxzou13rtf/O5oHc3DDgLrL4nMLEZ+VweFO1wSfdCrz7RdouKZUnZ7rqmJXyA0KNw5aZDaMPOF312JcPXaWSM2huarB0mhyO+dIAV5gDsBaKF3d/jllzTn2kp+x+HFXt8K+EdfiH6nZZQaj6MTEOn1I8juTZE+1ixp+YqeIxS3sZGyQ3BbYu6psqPcTy+CJpijgTvrhucLZFNB+R6SXJnXi7WBGHYv8DxAg2dTuUlcAqMsje2eFeF2D0K2a8UsgOWF4L3q8DcZ4CXSouyZUtLpWq4zpnRXVkku1BcSiEM2wGVa6cB0+bSFy81bLBHom0RfZQmJJ0WwdPUwCHzlQkUswmDPSOMQHcLww3D4q9hMU4WVNhMb4t6qNTbwHMUpuAIOBzmwfSYMQxzWmNtwnBVCkreClMyNdVmlCmm7KGX6DB9nDUWvomFIX8SJzXQWZswXE6LbWCYiyGMicG4D+NlPUZa7GiXsZ2cumnD9DEIFmu4AcXRbZ4B37pqiImB0G1CCrcj8Uxog9Fk+lspNr22lpEyhWXVxDG0WdpAJISx6bYDu1AGwkulZQ7iZNMgugAmuMbPICAH4EgyBOJWFImAsXcSwWVAPNQMi+BAzTF4OGSKwBP4liVPm2mPw+tygR53GSFTpOwIWcJzicCzHwVrjsPN4qG0S0CBMULUB7OMabzIe2ZHCMPIxxHtJDQgUHN8bGm7jk8HciTrXHgse0C04GHMeekzRZ8RT8DM5RUX+E4F3oU6nNH/6PUkVIwymWWnRDlXDMlz2qKOskPUzYaIONRomGE8Rj4QNEhR5iy66IHwxtA7EltBGBSxkWK+sCFWi3i5AOkKvMHfjcS+PgmzeBggckMwLetqxrNru1zSc3Z9VnD8HefrUPAs3of8wrQf5q85/Zldy/ZZ4TqF4RjcBdz5Kj5j00pyfL/MGE4e6wqQixWiXC74hPBEUy8Bn4OXqa2QjQW7celiMAAqMT3TwE8y4IZCMS1m1Zn0s2KWHdmeG4dn0se7usgcXCbM1tnisiVcJ69BxgLehnfOnZu/atzTu8oO8rI3GMzFJtAeCjxRRRoQelc3MkK3Ew8FOnYgy/A4USQ3sgUXCbyBltBXlx6UMJpflFc9WyKurrT0tH0xjRczmchzmQ3NXjUdhFTOSgjDf2I9YyM6XWQhdvm5MCzYNbsK6tHTWhF4aADjrWd2VegKMvdbtSmVNiDkUo0XvNOCwZ5XkHRvwiaFUbDzrGj6hrmqkf79EejVnQ6DXREIkBUqnb6ShLjctMaGF3TALyYiezG/7dMzpq5rEGDtJDtVa7IlyEqchxG7MOh+kWoTqM7LSNmh8/sCAXafmZ5KVGRQkE2T1sHgVOMF/uieoPxfZCb29cjBKaAbJWg5nqgzyXUPleWF3HA1GMtHKi4fesPUBVYCSkrfFwwx1rlwhX9X8Ku7U1kZ6SWtxlLIar4DrbqsYXSm+qdEoc7G5sptDS3eFY3+inthnS9Y0wiorTEIhKiyrrn8IXgw3ZtBb9lQHj6oV2+1bC5fDKEGYeGwI8S9sbV8pySx30cNZgBIlN5Wt8v7ogFkW4Uxfoitecv47wwrrnrh4vBLlsSe1InwooXFyYPxXAzZ8a1J/x4nsuNV6776Zq++h1QdEropPCLyroydDz3qhotwZFN9S8WDAgf/kQz+YxmJe/J9ApaMMmPiuB1Y3z1m3nAEihtCUuwyc5/eRv+UXlneg+PndhdJ31Lnv1aNBzndvILVY4Oycgbutc+Hl8Hxgn8Mx1+P0lDkqb1hRmHZq1VV2ldm/CYsoBw4RWdsqkDAtkxp3AYTuM8m6Pg+vwbfFjTgeFmM+jSEOzBghDaUHEb7E7j8t4G3UzBrbPVmi7pdya8ocQvVbPy2AUZ1GIvoB69q5H6329HyhSYu12+RABjppsrn/AhWcVsjKcZmjLj500AHl38+ZB0P3XHDPgLZediiPfwFL2xEkdkURtuwiBpvwya0kh0bSP0uA559NT5FrBPseRnS4dUIjxqxgkswz2kIH7KgN2xsfJdB6Pr61orHjDS83ugv31110fNn7e7eO0eRkfOm7CR4w06FsKmQ+VINX12fUNUwFwl14yORNpzQ6lxgS3zd1Gfz5gWdVVVVM/ZsnTBWcikjXBIiLXdazyji/hRz4leK1VyBjh5MJAAFdEM4BPJ9plu6sSgdwE0Hc8tbPMOgC/C97WZD+7iqGnUIY9jY/2P/uJq2pbBwdA/Aiy0KdXZgXY0vfZY0/QHGjnktOGJn8i+lhlt0WYEc3cZxT4Ur2uPWcK0m/nWAwUjfgAODSdBPcQLAW7JOeOqTAblQzB8b+WVRT3QZt/EmWgC4/ldHZ49almi+g84v1vE+6MIM2oCT4ciQIoP96me3XuFCdEvUdWuypDmqxovd+lbmsBNmb9x+jW3qLVkTTzRf46eCieZ9rPJDTFwOUy3EyXkOyolYTx5K4lZWvSXdSd3k3rqmsr3Jkj/umDdRgnCPm+Z2z9FfZhLFOMXna6MBvBwO/oOd+ujo7KxZKcMdfN0PpREHN2ygZD92y434K9mnhpISU3M5OhoYFOOFp92LlNmqyWNOW1e99XLDl01HZ9KpUYeGBhJvvHjrRjyLD3jwAQojrbhS3IY/wHt9aKgrNYtjSQP/B/DJkgv72OkPAAAAAElFTkSuQmCC";
    public static final String DEFAULT_LOGO_URL="https://app.dropthought.com/assets/images/dt-logo.png";
    public static final String DEFAULT_LOGO = "https://dt-program-banner.s3.us-west-1.amazonaws.com/a597f131-1cfe-4382-9780-2189425da87d-image.png";
    public static final String LANGUAGE_EN="en";
    public static final String DEFAULT_TEMPLATE = "emailContent.html";
    public static final String DEFAULT_THEME = "#6f5ba7";

    public static final String PRECURSOR_DATE = "precursor_date";
    public static final String START_DATE = "start_date";
    public static final String END_DATE = "end_date";
    public static final String CONTRACT_STATE = "state";
    public static final String CONTRACT_STATE_ACTIVE ="1";
    public static final String CONTRACT_STATE_INACTIVE = "0";
    public static final String CONTRACT_PARTIALLY_EXPIRED ="2";
    public static final String CONTRACT_EXPIRED ="3";
    public static final String CONTRACT_PARTIALLY_EXPIRED_PUBLISH ="-2";
    public static final String CONTRACT_EXPIRED_PUBLISH ="-3";

    public static final String ACTIVATE = "activate";
    public static final String DEACTIVATE = "deactivate";
    public static final String ACTIVATED = "Activated";
    public static final String DEACTIVATED = "Deactivated";

    public static final String APPLE = "APPLE";
    public static final String GOOGLE = "GOOGLE";
    public static final String MICROSOFT = "MICROSOFT";

    public static final Map EMAIL_TEMPLATE = new HashMap();

    public static final String BUSINESS_CONTRACT_REMINDER_TEMPLATE = "businessContractReminder.html";
    public static final String BUSINESS_CONTRACT_TERMINATION_TEMPLATE ="buisnessContractExpired.html";
    public static final String BUSINESS_CONTRACT_REMINDER_SUBJECT ="Heads Up! Your contract is about to expire.";
    public static final String BUSINESS_CONTRACT_TERMINATION_SUBJECT ="Sorry to say this but your contract has expired…";

    public static final String BUSINESS_SETTINGS_REMINDER_TEMPLATE ="businessSettingsReminder.html";
    public static final String BUSINESS_SETTINGS_REMINDER_SUBJECT ="Heads Up, there has been a change in your Access Settings.";
    public static final String BUSINESS_CONFIG_ACTIVE_STATUS = "1";
    public static final String USER_CONFIG_ACTIVE_STATUS = "1";

    public static final String USER_EMAIL_EXIST ="User email already exists";
    public static final String USER_EXIST_ALREADY ="User exists already";
    public static final String USER_ADDED ="User added successfully";
    public static final String USER_UPDATED ="User updated successfully";
    public static final String USER_NOT_EXIST ="User does not exist";
    public static final String USER_DELETED ="User deleted successfully";
    public static final String USER_DELETED_ALREADY ="User deleted already";

    public static final String BUSINESS_USAGE_REMINDER_TEMPLATE ="businessUsageReminder.html";
    public static final String BUSINESS_USAGE_REMINDER_SUBJECT ="Heads up! You are about to reach your limits...";

    public static final String USERS_USAGE_MESSAGE = "You have created %s accounts so far. Your Dropthought account is about to reach the limit set for number of accounts. If you want to increase that limit, please contact your CS Executive.";
    public static final String PROGRAM_USAGE_MESSAGE = "You have created %s programs so far. Your Dropthought account is about to reach the limit set for number of programs. If you want to increase that limit, please contact your CS Executive.";
    public static final String RESPONSE_USAGE_MESSAGE = "You have received %s responses so far. Your Dropthought account is about to reach the limit set for number of responses. If you want to increase that limit, please contact your CS Executive.";
    public static final String RECIPIENT_USAGE_MESSAGE = "You have used %s recipients so far. Your Dropthought account is about to reach the limit set for number of recipients. If you want to increase that limit, please contact your CS Executive.";
    public static final String LIST_USAGE_MESSAGE = "You have used %s lists so far. Your Dropthought account is about to reach the limit set for number of lists. If you want to increase that limit, please contact your CS Executive.";
    public static final String EMAIL_USAGE_MESSAGE = "You have sent %s emails so far. Your Dropthought account is about to reach the limit set for number of email invites. If you want to increase that limit, please contact your CS Executive.";
    public static final String SMS_USAGE_MESSAGE = "You have sent %s SMSs so far. Your Dropthought account is about to reach the limit set for number of SMS invites. If you want to increase that limit, please contact your CS Executive.";
    public static final String WHATSAPP_USAGE_MESSAGE = "You have sent %s WhatsApp messages so far. Your Dropthought account is about to reach the limit set for number of Whatsapp invites. If you want to increase that limit, please contact your CS Executive.";
    public static final String TRIGGER_USAGE_MESSAGE = "You have created %s triggers so far. Your Dropthought account is about to reach the limit set for the number of Triggers. If you want to increase that limit, please contact your CS Executive.";
    public static final String API_USAGE_MESSAGE = "You have made %s API requests so far.  Your Dropthought account is about to reach the limit set for API requests. If you want to increase that limit, please contact your CS Executive.";
    public static final String FILTER_USAGE_MESSAGE = "You have created %s filters so far. Your Dropthought account is about to reach the limit set for number of filters. If you want to increase that limit, please contact your CS Executive.";
    public static final String METRIC_USAGE_MESSAGE = "You have created %s metrics so far. Your Dropthought account is about to reach the limit set for number of metrics. If you want to increase that limit, please contact your CS Executive.";
    public static final String KIOSKS_USAGE_MESSAGE = "You have registered %s kiosks so far. Your Dropthought account is about to reach the limit set for number of kiosks. If you want to increase that limit, please contact your CS Executive.";
    public static final String INTEGRATIONS_USAGE_MESSAGE = "You have created %s integrations so far. Your Dropthought account is about to reach the limit set for number of kiosks. If you want to increase that limit, please contact your CS Executive.";

    public static final String KICKSTART_MSG = "We are genuinely thrilled to have you onboard with Dropthought. Our community is driven by inquisitive minds like yours, seeking to cultivate impactful interactions and a better understanding of their professional environment. We have already created an account for you. As the first step, use the button below to update your password and get started";

    public static final String KICKSTART_MSG_MS_SSO = "We are genuinely thrilled to have you onboard with Dropthought. Our community is driven by inquisitive minds like yours, seeking to cultivate impactful interactions and a better understanding of their professional environment. We have already created an account for you and your organization requires a mandatory Microsoft SSO sign-in. As the first step, use the button below to sign-in via Microsoft SSO and get started";
    public static final String KICKSTART_MSG_GOOGLE_SSO = "We are genuinely thrilled to have you onboard with Dropthought. Our community is driven by inquisitive minds like yours, seeking to cultivate impactful interactions and a better understanding of their professional environment. We have already created an account for you and your organization requires a mandatory Google SSO sign-in. As the first step, use the button below to sign-in via Google SSO and get started";
    public static final String KICKSTART_MSG_APPLE_SSO = "We are genuinely thrilled to have you onboard with Dropthought. Our community is driven by inquisitive minds like yours, seeking to cultivate impactful interactions and a better understanding of their professional environment. We have already created an account for you and your organization requires a mandatory Apple SSO sign-in. As the first step, use the button below to sign-in via Apple SSO and get started";

    public static final String KING_NEW_LOGIN_TEMPLATE = "kingNewLoginInstructions.html";
    public static final String KING_ADMIN_READWRITE_TEMPLATE = "KingAdminWriteUserKickstartTemplate.html";
    public static final String READ_ONLY_USER_KICKSTART_TEMPLATE = "ReadonlyUserKickstartTemplate.html";
    public static final String WELCOME_ABOARD_MESSAGE = "Welcome Aboard DropThought : Rediscover The Art of Asking";
    public static final String SENDER_EMAIL = "enterprise_support@dropthought.com" ;
    public static final String SENDER_NAME = "No-reply" ;
    public static final String QRCODE = "qr";
    public static final String LINK = "link";
    public static final String KIOSK = "kiosk";
    public static final String EXPORTLINKS = "uniqueLinks";
    public static final String PLAN_RENEWAL_REMINDER = "planRenewal";
    public static final String PLAN_RENEWAL_REMINDER_TEMPLATE_FOR_SUBSCRIPTION = "planRenewalReminderSubscriptionTemplate.html";
    public static final String PLAN_RENEWAL_REMINDER_SUBJECT_FOR_SUBSCRIPTION = "Your Dropthought Lite subscription ends soon!";
    public static final String PLAN_RENEWAL_REMINDER_TEMPLATE_FOR_TRIAL = "planRenewalTemplate.html";
    public static final String PLAN_RENEWAL_REMINDER_SUBJECT_FOR_TRIAL = "Your Free Trial Ends Soon! | Dropthought";
    public static final String PLAN_CONFIG_ID = "plan_config_id";
    public static final String CONTRACT_END_DATE = "contract_end_date";
    public static final String DT_LITE_PLAN_NAME = "DT Lite";
    public static final String DT_LITE_PRO_PLAN_NAME = "DT Lite Pro";
    public static final String BUSINESS_EMAIL = "business_email";
    public static final String PAYMENT_FAILURE_TEMPLATE = "paymentFailureTemplate.html";
    public static final String PAYMENT_FAILURE_SUBJECT = "Uh - Oh! - Looks like your Dropthought's payment didn't go through!";
    public static final String PLAN_UPDATE_TEMPLATE = "planUpdateTemplate.html";
    public static final String PLAN_UPDATE_SUBJECT = "Heads Up! Your Dropthought Plan has been Changed";
    public static final String PLAN_EXPIRED = "planExpired";
    public static final String PLAN_EXPIRED_TEMPLATE = "subscriptionExpiredTemplate.html";
    public static final String PLAN_EXPIRED_SUBJECT = "Your Dropthought Lite has expired";
    public static final Integer ZERO_INT = 0;
    public static final String ZERO_STRING = "0";
    public static final String ONE_STRING = "1";
    public static final String EMPTY_STRING = "";
    public static final String EMPTY_OBJECT = "{}";
    public static final String USERID = "userId";
    public static final String BUSINESSID = "businessId";
    public static final String HIPPA = "hippa";
    public static final String DEVELOPER_MESSAGE = "developMessage";
    public static final String ERROR_OCCURED = "Error occurred.";
    public static final String TRIAL_STATUS = "trial_status";
    public static final int DT_LITE_PLAN_ID = 3;
    public static final String PLAN_EXPIRED_AFTER_TRAIL_TEMPLATE = "subscriptionExpiredAfterTrial.html";
    public static final String PLAN_EXPIRED_AFTER_TRAIL_SUBJECT = "Your Dropthought Lite free trail is over";
    public static final String BUSINESS_NAME = "business_name";


    public Constants() {
        EMAIL_TEMPLATE.put("en","emailContent.html");
        EMAIL_TEMPLATE.put("ar","emailContentRtl.html");
    }

    //business
    public static String GET_BUSINESS_UUID_BY_ID = "select business_uuid from business where business_id=?";
    public static String GET_BUSINESS_ID_BY_CONFIG_UUID = "select business_id from business_configurations where business_config_uuid=?";
    public static final String GET_BUSINESS_BY_UUID = "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid from business where business_uuid=?";
    public static final String GET_BUSINESS_ID_BY_UUID = "select business_id from business where business_uuid=?";
    public static final String GET_BUSINESS_CONTRACT_DATES_BY_ID = "select contract_start_date, contract_end_date from business where business_id = ?";
    public static final String UPDATE_BUSINESS_GLOBAL_SETTING_EMAIL_ALERT = "UPDATE business_global_settings SET business_email_alert = ? where business_id = ?";
    //business config
    public static final String CREATE_BUSINESS_CONFIG = "insert into business_configurations (business_id, email, sms, qrcode, " +
            "shareable_link, text_analytics, logic, respondent, created_by, created_time, business_config_uuid, segment, kiosk, " +
            "metadata_question, mobile_app, emotion_analysis, intent_analysis, trigger_toggle, advanced_schedule, english_toggle, " +
            "arabic_toggle, multi_surveys, bitly, web_hooks, preferred_metric, category_analysis, advanced_text_analytics, no_of_users, " +
            "no_of_active_programs, no_of_responses, no_of_metrics, no_of_emails_sent, no_of_sms, no_of_api, no_of_filters, no_of_triggers, " +
            "data_retention, no_of_lists, no_of_recipients, hippa, cohort_analysis, advanced_templates, metric_correlation, api_key, whatfix, " +
            "multiple_sublink, advanced_analysis, export_links, respondent_tracker, from_customization, integrations, no_of_integrations, " +
            "no_of_kiosks, dynamic_links, no_of_shareable_sublink, no_of_multiple_static_qr, no_of_dynamic_switch_qr, no_of_dynamic_group_qr, " +
            "mfa, focus_metric, advanced_report, previously_used_question_library, predefined_question_library, custom_themes, chatbot, " +
            "audit_program, collaboration, custom_dashboard, whatsapp, no_of_whatsapp, r_coefficient, close_loop, program_throttling, " +
            "text_analytics_config_id, game_plan, notes, notification, submission_delay, unique_links, program_features, refresh_kiosk_qr, ai_surveys_limit, task_manager, ai_surveys, lti, lti_config, sso, response_quota, recurrence, custom_dashboard_builder) " +
            "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    public static final String GET_BUSINESS_CONFIG_BY_CONFIG_UUID = "select business_config_id, business_id, email, sms, qrcode, shareable_link, text_analytics, business_config_uuid, logic, respondent, segment, kiosk, metadata_question, mobile_app, emotion_analysis, intent_analysis, trigger_toggle, advanced_schedule, english_toggle, arabic_toggle, multi_surveys, bitly, web_hooks, preferred_metric, refresh_kiosk_qr from business_configurations where business_config_uuid = ?";
    public static final String GET_BUSINESS_CONFIG_BY_BUSINESS_ID = "select business_config_id, business_id, email, sms, qrcode, shareable_link, text_analytics, business_config_uuid, logic, respondent, segment, kiosk, metadata_question, mobile_app, emotion_analysis, intent_analysis, trigger_toggle, advanced_schedule, english_toggle, arabic_toggle, multi_surveys, bitly, web_hooks, preferred_metric, no_of_kiosks, refresh_kiosk_qr, ai_surveys_limit from business_configurations where business_id = ?";
    public static final String DELETE_CONFIG_BY_CONFIG_UUID  = "delete from business_configurations where business_config_uuid = ?";
    public static final String DELETE_CONFIG_BY_BUSINESSID = "delete from business_configurations where business_id = ?";
    public static final String CREATE_BUSINESS_VERSION_CONFIG = "insert into business_configurations_version (" +
            "business_id,email,sms,qrcode,shareable_link,text_analytics,logic,respondent,created_by," +
            "created_time,business_config_version_uuid,segment,kiosk,metadata_question,mobile_app,emotion_analysis,intent_analysis," +
            "trigger_toggle,advanced_schedule,english_toggle,arabic_toggle,multi_surveys,bitly,web_hooks,preferred_metric,notification, " +
            "category_analysis, advanced_text_analytics, no_of_users, no_of_active_programs, no_of_responses, no_of_metrics, " +
            "no_of_emails_sent, no_of_sms, no_of_api, no_of_filters, no_of_triggers, data_retention, no_of_lists, no_of_recipients, hippa, " +
            "cohort_analysis, advanced_templates, metric_correlation, api_key, whatfix, multiple_sublink, advanced_analysis, export_links," +
            " respondent_tracker, from_customization, integrations, no_of_integrations, no_of_kiosks, dynamic_links," +
            " no_of_shareable_sublink, no_of_multiple_static_qr, no_of_dynamic_switch_qr, no_of_dynamic_group_qr, mfa, focus_metric, " +
            "advanced_report, previously_used_question_library, predefined_question_library, custom_themes, chatbot, audit_program," +
            " collaboration, custom_dashboard, whatsapp, no_of_whatsapp, r_coefficient, close_loop, program_throttling, text_analytics_config_id, game_plan, notes, submission_delay, unique_links, program_features, refresh_kiosk_qr, ai_surveys_limit, task_manager, ai_surveys, lti, lti_config, sso, response_quota, recurrence, custom_dashboard_builder) " +
            "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?," +
            "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    public static final String GET_BUSINESS_VERSION_ALL_CONFIG = "select business_config_version_id as id, business_id, email, sms, qrcode, shareable_link, text_analytics, business_config_version_uuid, logic, respondent, segment, kiosk, metadata_question, mobile_app, emotion_analysis, intent_analysis, trigger_toggle, advanced_schedule, english_toggle, arabic_toggle, multi_surveys, bitly, web_hooks, preferred_metric, notification, category_analysis,advanced_text_analytics,no_of_users,no_of_active_programs,no_of_responses,no_of_metrics,no_of_emails_sent,no_of_sms,no_of_api,no_of_filters,no_of_triggers,data_retention, no_of_lists, no_of_recipients, created_time as createdTime, hippa, cohort_analysis, advanced_templates, metric_correlation, api_key, whatfix, multiple_sublink, advanced_analysis, export_links, respondent_tracker, from_customization, integrations, no_of_integrations, no_of_kiosks, dynamic_links, no_of_shareable_sublink, no_of_multiple_static_qr, no_of_dynamic_switch_qr, no_of_dynamic_group_qr, mfa, focus_metric, advanced_report, previously_used_question_library, predefined_question_library, custom_themes, chatbot, audit_program, collaboration, custom_dashboard, whatsapp, no_of_whatsapp, r_coefficient, close_loop, program_throttling, text_analytics_config_id, game_plan, notes, unique_links, program_features, submission_delay, refresh_kiosk_qr, ai_surveys_limit, sso from,task_manager, ai_surveys,lti,lti_config, response_quota, recurrence, custom_dashboard_builder business_configurations_version where business_id = ?  order by created_time desc";
    public static final String GET_BUSINESS_VERSION_ALL_CONFIG_COUNT = "select count(business_config_version_id) as count from business_configurations_version where business_id = ?  order by created_time desc";
    public static final String GET_BUSINESS_VERSION_CONFIG = "select business_config_version_id as id, business_id, email, sms, qrcode, shareable_link, text_analytics, business_config_version_uuid, logic, respondent, segment, kiosk, metadata_question, mobile_app, emotion_analysis, intent_analysis, trigger_toggle, advanced_schedule, english_toggle, arabic_toggle, multi_surveys, bitly, web_hooks, preferred_metric, notification, category_analysis,advanced_text_analytics,no_of_users,no_of_active_programs,no_of_responses,no_of_metrics,no_of_emails_sent,no_of_sms,no_of_api,no_of_filters,no_of_triggers,data_retention, no_of_lists, no_of_recipients, created_time as createdTime, hippa, cohort_analysis, advanced_templates, metric_correlation, api_key, whatfix, multiple_sublink, advanced_analysis, export_links, respondent_tracker, from_customization, integrations, no_of_integrations, no_of_kiosks, dynamic_links, no_of_shareable_sublink, no_of_multiple_static_qr, no_of_dynamic_switch_qr, no_of_dynamic_group_qr, mfa, focus_metric, advanced_report, previously_used_question_library, predefined_question_library, custom_themes, chatbot, audit_program, collaboration, custom_dashboard, whatsapp, no_of_whatsapp, r_coefficient, close_loop, program_throttling, text_analytics_config_id, game_plan, notes, unique_links, program_features, submission_delay, refresh_kiosk_qr, ai_surveys_limit, sso,task_manager, ai_surveys,lti,lti_config, response_quota, recurrence, custom_dashboard_builder from business_configurations_version where business_id = ?  order by created_time desc limit ?, ?";
    public static final String GET_BUSINESS_VERSION_CONFIG_BY_VERSIONID= "select business_config_version_id as id, business_id, email, sms, qrcode, shareable_link, text_analytics, business_config_version_uuid, logic, respondent, segment, kiosk, metadata_question, mobile_app, emotion_analysis, intent_analysis, trigger_toggle, advanced_schedule, english_toggle, arabic_toggle, multi_surveys, bitly, web_hooks, preferred_metric, notification, category_analysis,advanced_text_analytics,no_of_users,no_of_active_programs,no_of_responses,no_of_metrics,no_of_emails_sent,no_of_sms,no_of_api,no_of_filters,no_of_triggers,data_retention, no_of_lists, no_of_recipients, created_time as createdTime, hippa, cohort_analysis, advanced_templates, metric_correlation, api_key, whatfix, multiple_sublink, advanced_analysis, export_links, respondent_tracker, from_customization, integrations, no_of_integrations, no_of_kiosks, dynamic_links, no_of_shareable_sublink, no_of_multiple_static_qr, no_of_dynamic_switch_qr, no_of_dynamic_group_qr, mfa, focus_metric, advanced_report, previously_used_question_library, predefined_question_library, custom_themes, chatbot, audit_program, collaboration, custom_dashboard, whatsapp, no_of_whatsapp, r_coefficient, close_loop, program_throttling, text_analytics_config_id, game_plan, notes, unique_links, program_features, submission_delay, refresh_kiosk_qr, ai_surveys_limit, sso,task_manager, ai_surveys,lti,lti_config, response_quota, recurrence, custom_dashboard_builder from business_configurations_version where business_id = ?  and business_config_version_uuid = ?";
    public static final String GET_BUSINESS_VERSION_CONFIG_BY_ID = "select business_config_version_id as id, business_id, email, sms, qrcode, shareable_link, text_analytics, business_config_version_uuid, logic, respondent, segment, kiosk, metadata_question, mobile_app, emotion_analysis, intent_analysis, trigger_toggle, advanced_schedule, english_toggle, arabic_toggle, multi_surveys, bitly, web_hooks, preferred_metric, notification, category_analysis,advanced_text_analytics,no_of_users,no_of_active_programs,no_of_responses,no_of_metrics,no_of_emails_sent,no_of_sms,no_of_api,no_of_filters,no_of_triggers,data_retention, no_of_lists, no_of_recipients, created_time as createdTime, hippa, cohort_analysis, advanced_templates, metric_correlation, api_key, whatfix, multiple_sublink, advanced_analysis, export_links, respondent_tracker, from_customization, integrations, no_of_integrations, no_of_kiosks, dynamic_links, no_of_shareable_sublink, no_of_multiple_static_qr, no_of_dynamic_switch_qr, no_of_dynamic_group_qr, mfa, focus_metric, advanced_report, previously_used_question_library, predefined_question_library, custom_themes, chatbot, audit_program, collaboration, custom_dashboard, whatsapp, no_of_whatsapp, r_coefficient, close_loop, program_throttling, text_analytics_config_id, game_plan, notes, unique_links, program_features, submission_delay, refresh_kiosk_qr, ai_surveys_limit, sso, task_manager, ai_surveys,lti,lti_config, response_quota, recurrence, custom_dashboard_builder from business_configurations_version where business_config_version_id = ?";
    public static final String GET_BUSINESS_VERSION_BY_BUSINESS_ID = "select business_config_version_id as id, business_id, email, sms, qrcode, shareable_link, text_analytics, business_config_version_uuid, logic, respondent, segment, kiosk, metadata_question, mobile_app, emotion_analysis, intent_analysis, trigger_toggle, advanced_schedule, english_toggle, arabic_toggle, multi_surveys, bitly, web_hooks, preferred_metric, notification, category_analysis,advanced_text_analytics,no_of_users,no_of_active_programs,no_of_responses,no_of_metrics,no_of_emails_sent,no_of_sms,no_of_api,no_of_filters,no_of_triggers,data_retention, no_of_lists, no_of_recipients, created_time as createdTime, hippa, cohort_analysis, advanced_templates, metric_correlation, api_key, whatfix, multiple_sublink, advanced_analysis, export_links, respondent_tracker, from_customization, integrations, no_of_integrations, no_of_kiosks, dynamic_links, mfa, focus_metric, advanced_report, previously_used_question_library, predefined_question_library, custom_themes, chatbot, audit_program, collaboration, custom_dashboard, game_plan, notes, unique_links, program_features, submission_delay, ai_surveys_limit, sso,task_manager,lti,lti_config from business_configurations_version where business_id = ?";
    public static final String GET_BUSINESS_CONFIGURATIONS_BY_BUSINESS_ID = "select * from business_configurations where business_id = ? order by business_config_id desc limit 1 ";

    public static final String CREATE_PLAN_CONFIG = "insert into plan_configurations (plan_name,email,sms,qrcode,shareable_link," +
            "text_analytics,created_by,modified_by,created_time,modified_time,plan_config_uuid,logic,respondent,segment,kiosk," +
            "metadata_question,mobile_app,emotion_analysis,intent_analysis,trigger_toggle,advanced_schedule,english_toggle," +
            "arabic_toggle,multi_surveys,bitly,web_hooks,preferred_metric,category_analysis,advanced_text_analytics,no_of_users," +
            "no_of_active_programs,no_of_responses,no_of_metrics,no_of_sms,no_of_api,no_of_filters,no_of_triggers,data_retention," +
            "no_of_lists,no_of_recipients,hippa,cohort_analysis,advanced_templates,metric_correlation,api_key,whatfix,multiple_sublink," +
            "advanced_analysis,export_links,respondent_tracker,from_customization,integrations,no_of_integrations,no_of_kiosks,dynamic_links," +
            "no_of_shareable_sublink,no_of_multiple_static_qr,no_of_dynamic_switch_qr,no_of_dynamic_group_qr,mfa,focus_metric,advanced_report," +
            "previously_used_question_library,predefined_question_library,custom_themes,chatbot,audit_program,collaboration,custom_dashboard, " +
            "whatsapp, no_of_whatsapp, r_coefficient, close_loop, program_throttling, game_plan, notes, active, pricing, trial_days, notification, " +
            " no_of_emails_sent, unique_links, program_features, plan_properties, task_manager, ai_surveys, lti, lti_config, sso, response_quota, recurrence, custom_dashboard_builder) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?," +
            "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    //create tracker config
    public static final String CREATE_TRACKER_CONFIG = "insert into track_business_config (business_id, parameter, created_time, track_uuid) values (?,?,?,?)";
    public static final String GET_TACKER_CONFIG = "select business_id, parameter, created_time, track_uuid from track_business_config where business_id = ? and parameter = ? order by created_time desc";
    public static final String GET_SSO_CONFIG = "select sso from business_configurations where business_id = ?";
    public static final String KING_NEW_USER_TEMPLATE = "kingLoginInstructions.html";
    public static final String KING_USER_PASSWORD_UPDATE_TEMPLATE = "kingPasswordUpdate.html";
    public static final String PASSWORD_UPDATE_TEMPLATE = "passwordUpdate.html";
    public static final String KING_NEW_USER_SUBJECT ="Welcome Aboard!";
    public static final String KING_BUTTON_VERBIAGE = "Access Account";
    public static final String SET_PASSWORD = "Set up Password";
    public static final String FROM_CUSTOMER_SUCCESS_EMAIL = "cs@dropthought.com";
    public static final String FROM_CUSTOMER_SUCCESS_NAME = "Customer Success Team";

    public static final Integer LIMIT_UNLIMITED = -1 ;
    public static final String LIMIT_DEFAULT = "0" ;

    public static final String TRANSACTION_ID = "correlationId";
    public static final String WORKER_TRANSACTION_ID = "wcorrelationId";

    public static final String KING_INACTIVE_STATE = "0";

    public static final String KING_ROLE = "admin";
    public static final String SUPER_ADMIN_ROLE = "super admin";
    public static final String READ_ONLY_ROLE = "viewer";
    public static final String READ_WRITE_ROLE = "creator";
    public static final String ACTIVE_STATUS = "1";
    public static final String DRAFT_STATE = "Draft";
    public static final String SCHEDULED_STATE = "Scheduled";
    public static final String ACTIVE_STATE = "Active";
    public static final String EXPIRED_STATE = "Expired";
    public static final String INACTIVE_STATE = "Inactive";

    public static final String TA_STATUS_NOFICATION_MSG="Hey Droplet,<br><span style='line-height: 1.6'> The request you have submitted for the text analytics settings for %s has been %s.<br>If denied, you can always go back to CS portal and edit the settings and request a new approval.<br>If approved, please go to CS portal and save the changes and send the update email to the client.<br><br><br></span>Over and out,<br>Dropthought Team";

    public static final String TA_REQUEST_APPROVAL_SUBJECT = "Text Analytics settings Approval Request for %s";
    public static final String TA_REQUEST_APPROVAL_PLAN = "%s Sentiment Categorization applied for %s";

    public static final String RESETPASSWORD = "resetpassword";
    public static final String SAVEPASSWORD = "savepassword";
    public static final String EMPTY = "";
    public static final String UPLOAD_ID = "upload_id";
    public static final String METADATA_EXCEL_INDEX = "metadata_excel_index";
    public static final String QUESTION_DATA = "question_data";
    public static final String QUESTION_UUID = "question_uuid";
    public static final String TIMESTAMP = "timestamp";
    public static final String QUESTION_EXCEL_INDEX = "question_excel_index";
    public static final String UPLOAD_METADATA = "upload_metadata";
    public static final String UPLOAD_QUESTIONS = "upload_questions";
    public static final String FEEDBACK_ID = "feedbackId";
    public static final String RESPONDENT_TRACKER = "respondent_tracker";
    public static final String SUBMISSION_TOKEN = "submission_token";
    public static final String SSO = "sso";

    public static final String YASS = "YASS";
    public static final String BITLY = "BITLY";

    public static final String DEFAULT_COMPANYCODE = "DT" ;
    public static final String DEFAULT_LANGUAGE = "en" ;

    public static  final String COLOR_BLACK="#ffffff";
    public static  final String COLOR_WHITE="#000000";

    public static final Integer TRIAL_DAYS = 30;
    public static final String AISURVEYS_LIMIT = "3";

    public static final String KING_NEW_LOGIN_TEMPLATE_FOR_PLAN = "welcomeAbordPlanActivation.html";

    public static final String ACCOUNT_CHANGE_TEMPLATE = "accountChangeToEnterpriseTemplate.html";
    public static final String ACCOUNT_CHANGE_SUBJECT = "Your Dropthought Subscription Updates";

}
