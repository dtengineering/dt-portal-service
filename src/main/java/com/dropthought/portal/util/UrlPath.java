package com.dropthought.portal.util;



/**
 * Created by Prabhakar Thakur on 15/7/2020
 */
public class UrlPath {

    /**
     * API Signatures
     */
    public static final String USERUUID = "{userUUID}";
    public static final String USERID = "{userId}";
    public static final String RESETPASSWORD = "/resetpassword";
    public static final String LOGIN = "/login";

}
