package com.dropthought.portal.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmailTemplateUtility {

    private static Logger logger = LoggerFactory.getLogger(EmailTemplateUtility.class);

    /**
     *
     * @param hostName
     * @param embedFirstQuestion
     * @return
     */
    public static  String getEnvironmentEmailContent(String hostName, boolean embedFirstQuestion) {

        String htmlContent = "";
        String emailContent = "<tr style=\"padding: 0; text-align: center; vertical-align: top;\">\n" +
                "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse; color: #464646; font-family: 'Avenir LT W01_85 Heavy1475544',-apple-system,system-ui,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif; font-size: 14px; font-weight: 400; hyphens: auto; line-height: 19px; margin: 0; padding: 0 0 10px; text-align: left; vertical-align: top; word-break: break-word;\">\n" +
                "<p class=\"lead\" style=\"color: #464646; font-family: 'Avenir LT W01_85 Heavy1475544',-apple-system,system-ui,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif; font-size: 12px; font-weight: 400; line-height: 24px; margin: 0; margin-bottom: 10px; padding: 0; text-align: center;\">\n" +
                "Environment : ${environment}</p>\n" +
                "</td>\n" +
                "<td class=\"expander\"\n" +
                "style=\"-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse; color: #464646; font-family: 'Avenir LT W01_85 Heavy1475544',-apple-system,system-ui,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif; font-size: 14px; font-weight: 400; hyphens: auto; line-height: 19px; margin: 0; padding: 0; text-align: left; vertical-align: top; visibility: hidden; width: 0; word-break: break-word;\"></td>\n" +
                "</tr>";

        String tableContentStart = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\">\n" +
                "<tr>\n" +
                "  <td align=\"left\" valign=\"top\" style=\"padding:32px 0 32px 0;\">";

        String tableContentEnd = "</td>\n" +
                "</tr>\n" +
                "</table>";

        if (hostName.contains("test")) {
            emailContent = emailContent.replace("${environment}", "test");
        } else if (hostName.contains("stage")) {
            emailContent = emailContent.replace("${environment}", "stage");
        } else if (hostName.contains("automation")) {
            emailContent = emailContent.replace("${environment}", "automation");
        } else {
            emailContent = "";
        }

        if (Utils.notEmptyString(emailContent) && embedFirstQuestion) {

            htmlContent = tableContentStart + "\n" + emailContent + "\n" + tableContentEnd;
            return htmlContent;
        }

        return  emailContent;
    }
}
