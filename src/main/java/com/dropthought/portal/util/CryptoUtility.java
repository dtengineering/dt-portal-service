package com.dropthought.portal.util;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.math.BigInteger;
import java.net.URL;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

;

/**
 * Created by Prabhakar Thakur on 7/3/2020.
 */
@Component
public class CryptoUtility {

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    PublicKey RSApublicKey;
    PrivateKey RSAprivateKey;
    //
    String RSA_PUBLIC_KEY_FILE = "RSApublic.key";
    String RSA_PRIVATE_KEY_FILE = "RSAprivate.key";

    private String keyString = "DropThought,2017@zaq1!xsw2@cde3#$4rfvbgt5%^6yhnmju7&8*ik,<.>lo9(0)p;:/?[{]};:'";

// commenting this, to resolve filenotfound issue DTV-3501
//    public CryptoUtility() {
//
//
//        if (!checkIfFileExists(RSA_PUBLIC_KEY_FILE)) {
//            logger.info("Key file not already present");
//            generateRSAKeyPairs();
//        } else {
//            logger.info("Key file already present");
//            try {
//
//                this.RSApublicKey = readRSAPublicKeyFromFile(RSA_PUBLIC_KEY_FILE);
//                this.RSAprivateKey = readRSAPrivateKeyFromFile(RSA_PRIVATE_KEY_FILE);
//            } catch (FileNotFoundException e) {
//                //logger.error(e.getMessage());
//            } catch (IOException e) {
//                //logger.error(e.getMessage());
//            } catch (Exception e) {
//                //logger.error(e.getMessage());
//            }
//        }
//
//    }


    /**
     * Check if file already present
     *
     * @param filename
     * @return boolean Hash
     */
    public boolean checkIfFileExists(String filename) {
        boolean returnValue = false;
        URL url = null;
        url = this.getClass().getResource("/RSApublic.key");
        /*if(this.getClass().getClassLoader().equals("WebappClassLoader"))
        {
            //System.out.println("Web app");
            url = this.getClass().getClassLoader().getResource("/RSApublic.key");
        }
        else
        {
            //System.out.println("Sun misc class loader");
            url = this.getClass().getResource("/RSApublic.key");
        }*/


        try {
            if (url != null) {
                FileInputStream in = new FileInputStream(url.getFile());
                if (in != null) {
                    logger.info("File already present");
                    returnValue = true;
                } else {
                    logger.info("File not already present");
                }
            } else {
                logger.info("File not already present");
            }

        } catch (FileNotFoundException e) {
            //logger.error(e.getMessage());
        } catch (Exception e) {
            //logger.error(e.getMessage());
        }


        return returnValue;
    }

    /**
     * Computes SHA - 256 Hash
     *
     * @param inputString
     * @return String Hash
     */
    public String computeSHAHash(String inputString) {
        String outputHash = null;

        try {

            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(inputString.getBytes());
            byte byteData[] = digest.digest();
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                String hex = Integer.toHexString(0xff & byteData[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            outputHash = hexString.toString();


        } catch (NoSuchAlgorithmException e) {
            //logger.error(e.getMessage());
        } catch (Exception e) {
            //logger.error(e.getMessage());
        }

        return outputHash;
    }

    /**
     * Computes RSA encryption
     *
     * @param text
     * @return cipher
     **/
    public String RSAencrypt(String text) {

        byte[] cipherText = null;
        String encryptedString = null;
        try {

            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, this.RSApublicKey);
            cipherText = cipher.doFinal(text.getBytes());

        } catch (NoSuchAlgorithmException e) {
            //logger.error(e.getMessage());
        } catch (NoSuchPaddingException e) {
            //logger.error(e.getMessage());
        } catch (InvalidKeyException e) {
            //logger.error(e.getMessage());
        } catch (IllegalBlockSizeException e) {
            //logger.error(e.getMessage());
        } catch (BadPaddingException e) {
            //logger.error(e.getMessage());
        } catch (Exception e) {
            //logger.error(e.getMessage());
        }

        encryptedString = Base64.encodeBase64String(cipherText);


        //return cipherText;
        return encryptedString;
    }

    /**
     * Computes RSA decryption
     *
     * @param cipherText
     * @return plaintext
     **/
    public String RSAdecrypt(String cipherText) {

        byte[] text = Base64.decodeBase64(cipherText.getBytes());
        byte[] decrytpedText = null;

        try {
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, this.RSAprivateKey);
            decrytpedText = cipher.doFinal(text);
        } catch (NoSuchAlgorithmException e) {
            //logger.error(e.getMessage());
        } catch (NoSuchPaddingException e) {
            //logger.error(e.getMessage());
        } catch (InvalidKeyException e) {
            //logger.error(e.getMessage());
        } catch (IllegalBlockSizeException e) {
            //logger.error(e.getMessage());
        } catch (BadPaddingException e) {
            //logger.error(e.getMessage());
        } catch (Exception e) {
            //logger.error(e.getMessage());
        }

        return new String(decrytpedText);

    }


    public void generateRSAKeyPairs() {
        try {
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
            keyGen.initialize(2048, random);
            KeyPair key = keyGen.generateKeyPair();
            this.RSApublicKey = key.getPublic();
            this.RSAprivateKey = key.getPrivate();
            KeyFactory factory = KeyFactory.getInstance("RSA");
            RSAPublicKeySpec RSApublicKeySpec = factory.getKeySpec(this.RSApublicKey, RSAPublicKeySpec.class);

            RSAPrivateKeySpec RSAprivateKeySpec = factory.getKeySpec(this.RSAprivateKey, RSAPrivateKeySpec.class);

            saveTofile(RSA_PUBLIC_KEY_FILE, RSApublicKeySpec.getModulus(), RSApublicKeySpec.getPublicExponent());
            saveTofile(RSA_PRIVATE_KEY_FILE, RSAprivateKeySpec.getModulus(), RSAprivateKeySpec.getPrivateExponent());

        } catch (InvalidKeySpecException e) {
            //logger.error(e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            //logger.error(e.getMessage());
        } catch (NoSuchProviderException e) {
            //logger.error(e.getMessage());

        } catch (Exception e) {
            //logger.error(e.getMessage());
        }
    }


    public void saveTofile(String fileName, BigInteger mod, BigInteger exp) {

        //System.out.println("Saving to file");
        try {

            URL url = this.getClass().getClassLoader().getResource(fileName);
            FileOutputStream out = new FileOutputStream(url.getFile());
            ObjectOutputStream fileOut = new ObjectOutputStream(new BufferedOutputStream(out));
            fileOut.writeObject(mod);
            fileOut.writeObject(exp);
            fileOut.close();

        } catch (FileNotFoundException e) {
            logger.error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

    }

    public PublicKey readRSAPublicKeyFromFile(String keyFileName) throws IOException {

        URL url = this.getClass().getClassLoader().getResource("RSApublic.key");
        FileInputStream in = new FileInputStream(url.getFile());

        ObjectInputStream oin = new ObjectInputStream(new BufferedInputStream(
                in));
        try {
            BigInteger m = (BigInteger) oin.readObject();
            BigInteger e = (BigInteger) oin.readObject();
            KeyFactory fact = KeyFactory.getInstance("RSA");
            oin.close();
            return fact.generatePublic(new RSAPublicKeySpec(m, e));

        } catch (Exception e) {
            throw new RuntimeException("Run time serialization error", e);
            //logger.error(e.getMessage());
        }

    }


    public PrivateKey readRSAPrivateKeyFromFile(String keyFileName) throws IOException {

        //System.out.println("Read RSA private key");
        //InputStream in = new FileInputStream(keyFileName);
        //InputStream in = getClass().getResourceAsStream("/RSAprivate.key");

        URL url = this.getClass().getClassLoader().getResource("RSAprivate.key");
        FileInputStream in = new FileInputStream(url.getFile());

        ObjectInputStream oin = new ObjectInputStream(new BufferedInputStream(in));


        try {
            KeyFactory fact = KeyFactory.getInstance("RSA");

            BigInteger m = (BigInteger) oin.readObject();
            BigInteger e = (BigInteger) oin.readObject();

            return fact.generatePrivate(new RSAPrivateKeySpec(m, e));

        } catch (Exception e) {
            throw new RuntimeException("Run time serialization error", e);
            //logger.error(e.getMessage());
        }

    }

    /**
     * Computes AES Encryption
     *
     * @param plainText
     * @return
     */
    public String[] AESEncrypt(String plainText) {

        String encryptedString = null;
        String initVector = null;
        String RSAinitVector = null;


        try {

            //Setup the Cipher
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");

            //Setup the initialization vector
            byte[] iv = new byte[cipher.getBlockSize()];
            new SecureRandom().nextBytes(iv);
            IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

            //Hash the key with SHA-256 and trim the output to 128 bit for the key
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(keyString.getBytes());
            byte[] key = new byte[16];
            System.arraycopy(digest.digest(), 0, key, 0, key.length);
            SecretKeySpec keySpec = new SecretKeySpec(key, "AES");

            //Encrypt
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivParameterSpec);
            byte[] encrypted = cipher.doFinal(plainText.getBytes());

            //Byte array to text string of the ciphertext with Base64 encoding
            encryptedString = Base64.encodeBase64String(encrypted);

            //Byte array to text string of the initialization vector with Base64 encoding
            byte[] base64IV = Base64.encodeBase64(iv);
            initVector = new String(base64IV);

            RSAinitVector = RSAencrypt(initVector);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return new String[]{encryptedString, RSAinitVector};
    }


    /**
     * Computes AES decryption
     *
     * @param encryptedString
     * @param RSAinitVector
     * @return
     */
    public String AESDecrypt(String encryptedString, String RSAinitVector) {

        String initVector = RSAdecrypt(RSAinitVector);
        byte[] encryptedData = Base64.decodeBase64(encryptedString.getBytes());
        byte[] rawIV = Base64.decodeBase64(initVector.getBytes());

        String plainText = null;
        String returnText = null;
        Cipher cipher = null;
        try {

            //Set up the Cipher
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            //Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");

            //Initialization vector from the bytearray
            IvParameterSpec ivParameterSpec = new IvParameterSpec(rawIV);

            //Generating the key from the Keystring
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(keyString.getBytes());
            byte[] key = new byte[16];
            System.arraycopy(digest.digest(), 0, key, 0, key.length);
            SecretKeySpec keySpec = new SecretKeySpec(key, "AES");

            cipher.init(Cipher.DECRYPT_MODE, keySpec, ivParameterSpec);

            //Decrypting the cipher text
            byte[] decrypted = cipher.doFinal(encryptedData);
            plainText = new String(decrypted);

            JSONObject jsonObject = new JSONObject(plainText);
            returnText = (String) jsonObject.get("plainText");


        } catch (NoSuchAlgorithmException e) {
            //logger.error(e.getMessage());
        } catch (NoSuchPaddingException e) {
            //logger.error(e.getMessage());
        } catch (BadPaddingException e) {
            //logger.error(e.getMessage());
        } catch (InvalidKeyException e) {
            //logger.error(e.getMessage());
        } catch (IllegalBlockSizeException e) {
            //logger.error(e.getMessage());
        } catch (InvalidAlgorithmParameterException e) {
            //logger.error(e.getMessage());
        } catch (JSONException e) {
            //logger.error(e.getMessage());
        } catch (Exception e) {
            //logger.error(e.getMessage());
        }

        return returnText;

    }
}