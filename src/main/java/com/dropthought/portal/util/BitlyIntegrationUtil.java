package com.dropthought.portal.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

public class BitlyIntegrationUtil {

    private static final Logger logger = LogManager.getLogger(BitlyIntegrationUtil.class);


    public static String generateBitlyLink(String longurl, String domain, String bitlykey, String bitlyAPI) {

        String shortLink = null;
        Map<String, Object> responseMap = new HashMap<>();
        try {
            JSONObject json = new JSONObject();
            json.put("long_url", longurl);
            json.put("domain", domain);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", bitlykey);
            HttpEntity<String> entity = new HttpEntity<String>(json.toString(), headers);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<Object> responseEntity = restTemplate.exchange(bitlyAPI, HttpMethod.POST, entity, Object.class);
            if (responseEntity != null && (responseEntity.getStatusCode() == HttpStatus.OK ||
                    responseEntity.getStatusCode() == HttpStatus.CREATED)) {
                if (responseEntity.getBody() != null) {
                        logger.debug(responseEntity.getBody().toString());
                    responseMap = (Map) responseEntity.getBody();
                    shortLink = responseMap.containsKey("link") && Utils.isNotNull(responseMap.get("link")) &&
                            Utils.notEmptyString(responseMap.get("link").toString()) ?
                            responseMap.get("link").toString() : "";
                }
            }
        } catch (Exception e) {
            logger.error("Error in bitly integration : " + e.getMessage());
        }

        return shortLink;
    }

    //For testing purpose. To be removed
    /*public static void main(String[] args) {
        generateBitlyLink("https://stage-eux.dropthought.com/en/welcome/j7q7b6f6ifrkkb9hsddnhhka66~~MTQ1OGE3ODMtNGI3Yy00Zjg0LWI0NTMtNWNhNTk1ZTY1ZWJi$RFQ1",
                "bit.ly", "89157c943cb4c64a9b019a77f009df89fd1c9333",
                "https://api-ssl.bitly.com/v4/shorten");

    }*/
}
