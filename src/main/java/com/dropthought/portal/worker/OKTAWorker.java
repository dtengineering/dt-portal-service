package com.dropthought.portal.worker;

import com.dropthought.portal.service.OKTAService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class OKTAWorker {

    @Autowired
    private OKTAService oktaService;

    private Logger logger = LoggerFactory.getLogger(OKTAWorker.class);

    public void receiveOKTA(byte[] queueParams) {
        logger.info("Received message for OKTA worker");
        String json = new String(queueParams, StandardCharsets.UTF_8);
        logger.info("queue params {}", json);
        try {
            ObjectMapper obj = new ObjectMapper();
            Map inputMap = obj.readValue(json, HashMap.class);
            oktaService.updateOKTAParameters(inputMap);
        } catch (Exception e) {
            logger.error("exception at receiveOKTA. Msg {}", e.getMessage());
        }
    }
}
