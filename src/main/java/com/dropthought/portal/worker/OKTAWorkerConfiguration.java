package com.dropthought.portal.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("oktaWorker")
@Configuration
public class OKTAWorkerConfiguration implements ApplicationListener<ApplicationReadyEvent> {

    private Logger logger = LoggerFactory.getLogger(OKTAWorkerConfiguration.class);

    @Value("${okta.queue}")
    private String queueName;

    @Value("${okta.routing.key}")
    private String routingKey;

    @Value("${okta.direct}")
    private String topicName;

    @Bean
    public DirectExchange receiverOktaExchange() {
        return new DirectExchange(topicName);
    }

    @Bean
    public Queue oktaReceivingQueue() {
        if (queueName == null) {
            throw new IllegalStateException("No queue to listen to! Please specify the name of the queue to listen to with the property 'okta.queue'");
        }
        return new Queue(queueName);
    }

    @Bean
    public Binding binding(Queue oktaReceivingQueue, DirectExchange oktaExchange) {
        if (routingKey == null) {
            throw new IllegalStateException("No events to listen to! Please specify the routing key for the events to listen to with the property 'okta.routingKey'.");
        }
        return BindingBuilder
                .bind(oktaReceivingQueue)
                .to(oktaExchange)
                .with(routingKey);
    }

    @Bean
    public SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
                                                    MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queueName);
        container.setMessageListener(listenerAdapter);
        return container;
    }

    @Bean
    public MessageListenerAdapter listenerAdapter(OKTAWorker oktaWorker) {
        return new MessageListenerAdapter(oktaWorker, "receiveOKTA");
    }

    @Bean
    public OKTAWorker oktaWorker() {
        return new OKTAWorker();
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        logger.info("TO OKTA MATCHING KEY '{}' FROM QUEUE '{}'!", routingKey, queueName);
    }
}
