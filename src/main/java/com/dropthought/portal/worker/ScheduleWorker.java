package com.dropthought.portal.worker;

import com.dropthought.portal.service.SchedulerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

//@Configuration
public class ScheduleWorker {

    private Logger logger = LoggerFactory.getLogger(ScheduleWorker.class);

    @Autowired
    SchedulerService schedulerService;

    public void receive(Map QueueParams) {
        logger.info("Received message for send mail for business");
        schedulerService.sendBusinessEmail(QueueParams);
    }
}
