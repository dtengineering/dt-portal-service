package com.dropthought.portal.businessconfigurations.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.json.JSONObject;

import javax.validation.constraints.Positive;
import java.io.Serializable;
import java.util.Map;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@SuperBuilder
public class BusinessConfigurationsBean implements Serializable {
    private String advancedAnalysis;
    private String advancedReport;
    private String advancedSchedule;
    private String advancedTemplates;
    private String advancedTextAnalytics;
    private String apiKey;
    private String arabic;
    private String auditProgram;
    private String aiSurveysLimit;
    private String aiSurveys;
    private String lti;
    private String bitly;
    private Integer businessConfigId;
    private String taskManager;

    @Positive(message = "Business Id should be positive number")
    private Integer businessId;

    private String categoryAnalysis;
    private Map channels;
    private String chatbot;
    private String closeLoop;
    private String cohortAnalysis;
    private String collaboration;
    private String customDashboard;
    private String customThemes;
    private Integer createdBy;
    private Integer dataRetention;
    private String defaultRecommendation;
    private String dynamicLinks;
    private String email;
    private String emotionAnalysis;
    private String english;
    private String exportLinks;
    private String focusMetric;
    private String fromCustomization;
    private String gamePlan;
    private String hippa;
    private String integrations;
    private String intentAnalysis;
    private String kiosk;
    private String logic;
    private String metadataQuestion;
    private String mfa;
    private String metricCorrelation;
    private String mobileApp;
    private Integer modifiedBy;
    private String multiSurveys;
    private String multipleSublink;
    private String notification;
    private Integer noOfActivePrograms;
    private Integer noOfApi;
    private Integer noOfDynamicGroupQr;
    private Integer noOfDynamicSwitchQr;
    private Integer noOfEmailsSent;
    private Integer noOfFilters;
    private Integer noOfIntegrations;
    private Integer noOfKiosks;
    private Integer noOfLists;
    private Integer noOfMetrics;
    private Integer noOfMultipleStaticQr;
    private Integer noOfRecipients;
    private Integer noOfResponses;
    private Integer noOfShareableSublink;
    private Integer noOfSms;
    private Integer noOfTriggers;
    private Integer noOfUsers;
    private Integer noOfWhatsapp;
    private String notes;
    private String predefinedQuestionLibrary;
    private String preferredMetric;
    private String previouslyUsedQuestionLibrary;
    private String programFeatures;
    private String programMetric;
    private String programOverview;
    private String programThrottling;
    private String qrcode;
    private String rCoefficient;
    private String refreshKioskQr;
    private String respondent;
    private String respondentTracker;
    private String segment;
    private String shareableLink;
    private String sms;
    private String submissionDelay;
    private Integer textAnalyticsConfigId;
    private String textAnalytics;
    private Integer trialDays;
    private String trigger;
    private String uniqueLinkEmail;
    private String uniqueLinks;
    private String uniqueLinkSms;
    private String uniqueLinkWhatsapp;
    private String userDefinedRecommendation;
    private String webHooks;
    private String whatsapp;
    private String whatfix;
    private String sso;
    private String googleSSO;
    private String microsoftSSO;
    private String appleSSO;
    private Map ltiConfig;
    private String responseQuota;
    private String recurrence;
    private String customDashboardBuilder;


    // Constants for default values
    private static final String ZERO_STRING = "0";
    private static final int LIMIT_UNLIMITED = -1;
    private static final String AISURVEYS_LIMIT = "3";

    // Static method with default values
    public static BusinessConfigurationsBean getDefaultBusinessConfiguration() {
        return BusinessConfigurationsBean.builder()
                .taskManager(ZERO_STRING)
                .email(ZERO_STRING)
                .qrcode(ZERO_STRING)
                .shareableLink(ZERO_STRING)
                .sms(ZERO_STRING)
                .whatsapp(ZERO_STRING)
                .textAnalytics(ZERO_STRING)
                .logic(ZERO_STRING)
                .respondent(ZERO_STRING)
                .segment(ZERO_STRING)
                .kiosk(ZERO_STRING)
                .metadataQuestion(ZERO_STRING)
                .advancedSchedule(ZERO_STRING)
                .english(ZERO_STRING)
                .arabic(ZERO_STRING)
                .multiSurveys(ZERO_STRING)
                .mobileApp(ZERO_STRING)
                .notification(ZERO_STRING)
                .emotionAnalysis(ZERO_STRING)
                .intentAnalysis(ZERO_STRING)
                .webHooks(ZERO_STRING)
                .preferredMetric(ZERO_STRING)
                .categoryAnalysis(ZERO_STRING)
                .trigger(ZERO_STRING)
                .bitly(ZERO_STRING)
                .noOfUsers(LIMIT_UNLIMITED)
                .noOfActivePrograms(LIMIT_UNLIMITED)
                .noOfTriggers(LIMIT_UNLIMITED)
                .noOfResponses(LIMIT_UNLIMITED)
                .noOfMetrics(LIMIT_UNLIMITED)
                .noOfEmailsSent(LIMIT_UNLIMITED)
                .noOfSms(LIMIT_UNLIMITED)
                .noOfWhatsapp(LIMIT_UNLIMITED)
                .noOfApi(LIMIT_UNLIMITED)
                .noOfFilters(LIMIT_UNLIMITED)
                .noOfLists(LIMIT_UNLIMITED)
                .noOfRecipients(LIMIT_UNLIMITED)
                .dataRetention(LIMIT_UNLIMITED)
                .hippa(ZERO_STRING)
                .cohortAnalysis(ZERO_STRING)
                .advancedTemplates(ZERO_STRING)
                .metricCorrelation(ZERO_STRING)
                .apiKey(ZERO_STRING)
                .whatfix(ZERO_STRING)
                .multipleSublink(ZERO_STRING)
                .advancedAnalysis(ZERO_STRING)
                .exportLinks(ZERO_STRING)
                .respondentTracker(ZERO_STRING)
                .fromCustomization(ZERO_STRING)
                .integrations(ZERO_STRING)
                .noOfKiosks(LIMIT_UNLIMITED)
                .dynamicLinks(ZERO_STRING)
                .noOfShareableSublink(LIMIT_UNLIMITED)
                .noOfMultipleStaticQr(LIMIT_UNLIMITED)
                .noOfDynamicSwitchQr(LIMIT_UNLIMITED)
                .noOfDynamicGroupQr(LIMIT_UNLIMITED)
                .mfa(ZERO_STRING)
                .advancedReport(ZERO_STRING)
                .gamePlan(ZERO_STRING)
                .previouslyUsedQuestionLibrary(ZERO_STRING)
                .predefinedQuestionLibrary(ZERO_STRING)
                .focusMetric(new JSONObject()
                        .put("focusMetric", ZERO_STRING)
                        .put("defaultRecommendation", ZERO_STRING)
                        .put("userDefinedRecommendation", ZERO_STRING)
                        .toString())
                .uniqueLinks(new JSONObject()
                        .put("uniqueLinkEmail", ZERO_STRING)
                        .put("uniqueLinkSms", ZERO_STRING)
                        .put("uniqueLinkWhatsapp", ZERO_STRING)
                        .toString())
                .programFeatures(new JSONObject()
                        .put("programOverview", ZERO_STRING)
                        .put("programMetric", ZERO_STRING)
                        .toString())
                .sso(new JSONObject()
                        .put("googleSSO", ZERO_STRING)
                        .put("microsoftSSO", ZERO_STRING)
                        .put("appleSSO", ZERO_STRING)
                        .toString())
                .chatbot(ZERO_STRING)
                .auditProgram(ZERO_STRING)
                .collaboration(ZERO_STRING)
                .customDashboard(ZERO_STRING)
                .rCoefficient(ZERO_STRING)
                .closeLoop(ZERO_STRING)
                .programThrottling(ZERO_STRING)
                .notes(ZERO_STRING)
                .submissionDelay(ZERO_STRING)
                .refreshKioskQr(ZERO_STRING)
                .aiSurveysLimit(AISURVEYS_LIMIT)
                .aiSurveys(ZERO_STRING)
                .lti(ZERO_STRING)
                .recurrence(ZERO_STRING)
                .responseQuota(ZERO_STRING)
                .customDashboardBuilder(ZERO_STRING)
                .build();

    }
}
