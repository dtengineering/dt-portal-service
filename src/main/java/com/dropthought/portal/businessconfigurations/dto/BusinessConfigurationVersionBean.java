package com.dropthought.portal.businessconfigurations.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.Positive;
import java.util.Map;
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@SuperBuilder
public class BusinessConfigurationVersionBean {

    private Integer businessVersionConfigId;

    @Positive(message = "Business Id should be positive")
    private Integer businessId;

    private String email;

    private String sms;

    private String qrcode;

    private String kiosk;

    private String shareableLink;

    private String textAnalytics;

    private String logic;

    private String respondent;

    private String segment;

    private String metadataQuestion;

    private String mobileApp;

    private String emotionAnalysis;

    private String intentAnalysis;

    private String trigger;

    private String advancedSchedule;

    private String english;

    private String arabic;

    private String multiSurveys;

    private String notification;

    private String bitly;

    private String webHooks;

    private String preferredMetric;

    private Integer createdBy;

    private Integer modifiedBy;

    private Map channels;

    private String categoryAnalysis;

    private String advancedTextAnalytics;

    private Integer noOfUsers;

    private Integer noOfActivePrograms;

    private Integer noOfResponses;

    private Integer noOfMetrics;

    private Integer noOfEmailsSent;

    private Integer noOfSms;

    private Integer noOfApi;

    private Integer noOfFilters;

    private Integer noOfTriggers;

    private Integer dataRetention;

    private Integer noOfLists;

    private Integer noOfRecipients;

    private String hippa;

    private String cohortAnalysis;

    private String advancedTemplates;

    private String metricCorrelation;

    private String apiKey;

    private String whatfix;

    private String multipleSublink;

    private String advancedAnalysis;

    private String exportLinks;

    private String respondentTracker;

    private String fromCustomization;

    private String integrations;

    private Integer noOfIntegrations;

    private Integer noOfKiosks;

    private String dynamicLinks;

    private Integer noOfShareableSublink;

    private Integer noOfMultipleStaticQr;

    private Integer noOfDynamicSwitchQr;

    private Integer noOfDynamicGroupQr;

    private String mfa;

    private String focusMetric;

    private String advancedReport;

    private String previouslyUsedQuestionLibrary;

    private String predefinedQuestionLibrary;

    private String defaultRecommendation;

    private String userDefinedRecommendation;

    private String customThemes;

    private String chatbot;

    private String auditProgram;

    private String collaboration;

    private String customDashboard;

    private String whatsapp;

    private Integer noOfWhatsapp;

    private String rCoefficient;

    private String closeLoop;

    private String programThrottling;
    private Integer textAnalyticsConfigId;
    private String gamePlan;
    private String notes;
    private String submissionDelay;
    private String uniqueLinkEmail;
    private String uniqueLinkSms;
    private String uniqueLinkWhatsapp;
    private String uniqueLinks;
    private String programFeatures;
    private String programOverview;
    private String programMetric;
    private String sso;
    private String googleSSO;
    private String microsoftSSO;
    private String appleSSO;

    private String refreshKioskQr;
    private String aiSurveysLimit;
    private String taskManager;
    private String aiSurveys;
    private String lti;
    private Map ltiConfig;
    private String responseQuota;
    private String recurrence;
    private String customDashboardBuilder;
}
