package com.dropthought.portal.businessconfigurations.controller;

import com.dropthought.portal.businessconfigurations.dto.BusinessConfigurationVersionBean;
import com.dropthought.portal.businessconfigurations.service.BusinessConfigVersionService;
import com.dropthought.portal.common.DTConstants;
import com.dropthought.portal.service.ClientService;
import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.Utils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.*;
@Profile("worker")
@RestController
@RequestMapping(path = "api/v1/portal/businessconfigversion/business")
@Validated
public class BusinessConfigVersionController {
    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Http Headers
     */
    protected final HttpHeaders httpHeaders = new HttpHeaders();

    @Autowired
    private BusinessConfigVersionService businessConfigVersionService;

    @Autowired
    private ClientService clientService;

    /**
     * Create business version configuration
     *
     * @param businessConfigurationVersionBean
     * @return
     */
    @PostMapping()
    @ResponseBody
    public ResponseEntity<String> createBusinessVersionConfig(@Valid @RequestBody BusinessConfigurationVersionBean businessConfigurationVersionBean) {
        logger.info("START createBusinessVersionConfig");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(businessConfigurationVersionBean)) {
                Integer id = businessConfigVersionService.createBusinessVersionConfigurations(businessConfigurationVersionBean);
                if (id > 0) {
                    Map responseMap = businessConfigVersionService.getBusinessConfigurationsVersionById(id, businessConfigurationVersionBean.getBusinessId());
                    if (responseMap.containsKey("businessConfigVersionId")) {
                        resultMap.put(Constants.SUCCESS, true);
                        resultMap.put(Constants.RESULT, responseMap);
                    } else {
                        resultMap.put(Constants.SUCCESS, true);
                        resultMap.put(Constants.RESULT, Collections.emptyList());
                    }
                }
            }
            else{
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide mandatory fields");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
        }
        logger.info("END createBusinessVersionConfig");
        return new ResponseEntity<>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Get business version configuration
     *
     * @param businessUniqueId
     * @return
     */
    @GetMapping("{businessUniqueId}")
    @ResponseBody
    public ResponseEntity<String> getBusinessVersionConfig(@PathVariable @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessId") String businessUniqueId, @RequestParam(defaultValue = "1") Integer pageId, @RequestParam(defaultValue = "America/Los_Angeles") String timezone) {
        logger.info("get business config version starts");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            if(Utils.notEmptyString(businessUniqueId)) {
                int businessId = businessConfigVersionService.retrieveBusinessByUUID(businessUniqueId);
                List response = businessConfigVersionService.getBusinessConfigurationsVersionByBusinessId(pageId, timezone, businessId);
                if (!response.isEmpty()) {
                    resultMap.put(Constants.SUCCESS, true);
                    resultMap.put(Constants.RESULT, response);
                    resultMap.put("totalPages", Utils.computeTotalPages(businessConfigVersionService.getAllBusinessConfigurationsVersionsCountByBusinessId(businessId)));
                    resultMap.put("currentPage", pageId);
                } else {

                    List clients = clientService.getClientByClientId(businessUniqueId, timezone);
                    Map hippaMap = new HashMap();
                    String hippa = Constants.ZERO_STRING;
                    if(clients.size()>0){
                        Map clientMap = (Map)clients.get(0);
                        hippa = clientMap.containsKey(Constants.HIPPA)? clientMap.get(Constants.HIPPA).toString() : Constants.ZERO_STRING;
                    }
                    hippaMap.put(Constants.HIPPA,hippa);
                    resultMap.put(Constants.SUCCESS, true);
                    List versions = new ArrayList();
                    versions.add(hippaMap);
                    resultMap.put(Constants.RESULT, versions);
                }
            }
            else{
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide mandatory fields");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
        }
        logger.info("get business config version ends");
        return new ResponseEntity<>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Get business version configuration
     *
     * @param businessUniqueId
     * @param versionId
     * @return
     */
    @GetMapping("{businessUniqueId}/version/{versionId}")
    @ResponseBody
    public ResponseEntity<String> getBusinessVersionConfigByVersion(@PathVariable @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessId") String businessUniqueId,
                                                                    @PathVariable @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid versionId") String versionId) {
        logger.info("get business config version starts");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            if(Utils.notEmptyString(businessUniqueId) && Utils.notEmptyString(versionId)) {
                List response = businessConfigVersionService.getBusinessConfigurationsVersionByVersionId(businessUniqueId, versionId);
                if (response.size() > 0) {
                    Map responseMap = (Map) response.get(0);
                    resultMap.put(Constants.SUCCESS, true);
                    resultMap.put(Constants.RESULT, responseMap);
                } else {
                    resultMap.put(Constants.SUCCESS, true);
                    resultMap.put(Constants.RESULT, Collections.emptyList());
                }
            }
            else{
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Version and Business Id are mandatory fields");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
        }
        logger.info("get business config version ends");
        return new ResponseEntity<>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Create business version configuration
     *
     * @param businessConfigurationVersionBean
     * @return
     */
    @PutMapping()
    @ResponseBody
    public ResponseEntity<String> updateBusinessVersionConfig(@Valid @RequestBody BusinessConfigurationVersionBean businessConfigurationVersionBean) {
        logger.info("update business config version starts");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(businessConfigurationVersionBean)) {
                Integer id = businessConfigVersionService.createBusinessVersionConfigurations(businessConfigurationVersionBean);
                if (id > 0) {
                    Map responseMap = businessConfigVersionService.getBusinessConfigurationsVersionById(id, businessConfigurationVersionBean.getBusinessId());
                    if (responseMap.containsKey("businessConfigVersionId")) {
                        resultMap.put(Constants.SUCCESS, true);
                        resultMap.put(Constants.RESULT, responseMap);
                    } else {
                        resultMap.put(Constants.SUCCESS, true);
                        resultMap.put(Constants.RESULT, Collections.emptyList());
                    }
                }else {
                    resultMap.put(Constants.SUCCESS, false);
                    resultMap.put(Constants.RESULT, Collections.emptyList());
                }
            }
            else{
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide mandatory fields");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
        }
        logger.info("update business config version ends");
        return new ResponseEntity<>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
    }

}
