package com.dropthought.portal.businessconfigurations.controller;


import com.dropthought.portal.businessconfigurations.dto.BusinessConfigurationsBean;
import com.dropthought.portal.businessconfigurations.service.BusinessConfigVersionService;
import com.dropthought.portal.businessconfigurations.service.BusinessConfigurationsService;
import com.dropthought.portal.common.DTConstants;
import com.dropthought.portal.service.ClientService;
import com.dropthought.portal.service.PdfService;
import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.Utils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.*;
@Profile("worker")
@RestController
@RequestMapping(path = "/api/v1/portal/businessconfig")
@Validated
public class BusinessConfigurationsController {

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Http Headers
     */
    protected final HttpHeaders httpHeaders = new HttpHeaders();

    @Autowired
    private ClientService clientService;

    @Autowired
    private BusinessConfigurationsService businessConfigurationsService;

    @Autowired
    private BusinessConfigVersionService businessConfigVersionService;

    @Autowired
    private PdfService pdfService;

    /**
     * Get business configurations by business uuid
     *
     * @param businessUUID
     * @return
     */
    @GetMapping("business/{clientId}")
    @ResponseBody
    public ResponseEntity<String> getClientConfigByClientUUID(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessId") String businessUUID, @RequestParam(defaultValue = "America/Los_Angeles") String timezone) {
        logger.info("start getClientConfigByClientUUID");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(businessUUID)) {
                int isTrueUUID = clientService.retrieveBusinessIDByUUID(businessUUID);
                if (isTrueUUID > 0) {
                    List response = businessConfigurationsService.getBusinessConfigurationsByBusinessId(businessUUID);
                    Integer businessId = clientService.retrieveBusinessIDByUUID(businessUUID);
                    String kioskLastConfigDate = businessConfigurationsService.getKioskLatestConfigDate(businessId, "noOfKiosk");
                    kioskLastConfigDate = Utils.notEmptyString(kioskLastConfigDate) ? Utils.getDateAsStringByTimezone(kioskLastConfigDate, timezone) : kioskLastConfigDate;
                    if (response.size() > 0) {
                        Map responseMap = (Map) response.get(0);
                        responseMap.put("kioskLastConfigTime",kioskLastConfigDate);
                        resultMap.put(Constants.SUCCESS, true);
                        resultMap.put(Constants.RESULT, responseMap);
                    } else {
                        resultMap.put(Constants.SUCCESS, true);
                        resultMap.put(Constants.RESULT, Collections.emptyList());
                    }
                    logger.info("response {}", resultMap);
                } else {
                    resultMap.put(Constants.SUCCESS, false);
                    resultMap.put(Constants.ERROR, "Provided businessUUID is invalid");
                }

            }
            else{
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide businessUUID mandatory field");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
        }
        logger.info("End getClientConfigByClientUUID");
        return new ResponseEntity<>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Update client business configuration information by businessUUID
     *
     * @param businessUUID
     * @param businessConfigurationsBean
     * @return
     */
    @PutMapping("business/{clientId}")
    @ResponseBody
    public ResponseEntity<String> updateClientConfig(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid clientId") String businessUUID,
                                                     @Valid @RequestBody BusinessConfigurationsBean businessConfigurationsBean,
                                                     @RequestParam(defaultValue = "true") boolean optimized) {
        logger.info("[START] updateClientConfig");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(businessUUID)) {
                List response = businessConfigurationsService.getBusinessConfigurationsByBusinessId(businessUUID);
                if (!response.isEmpty()) {
                    Map responseMap = (Map) response.get(0);
                    Map oldBusinessConfigurationMap = (Map) response.get(0);
                    Integer businessId = Integer.parseInt(responseMap.get(Constants.BUSINESSID).toString());
                    businessConfigurationsBean.setBusinessId(businessId);
                    boolean success = businessConfigurationsService.validateConfiguration(businessConfigurationsBean);
                    if (success) {
                        int updateStatus = optimized ? businessConfigurationsService.updateBusinessConfigurationsV1(businessConfigurationsBean,
                                businessUUID, oldBusinessConfigurationMap) : businessConfigurationsService.updateBusinessConfigurations(businessConfigurationsBean,
                                businessUUID, oldBusinessConfigurationMap);
                        if (updateStatus > 0) {
                            businessConfigVersionService.createBusinessVersionConfigurations(businessConfigurationsBean,false);
                            response = businessConfigurationsService.getBusinessConfigurationsByBusinessId(businessUUID);
                            if (!response.isEmpty()) {
                                responseMap = (Map) response.get(0);
                                resultMap.put(Constants.SUCCESS, true);
                                resultMap.put(Constants.RESULT, responseMap);
                            } else {
                                resultMap.put(Constants.SUCCESS, true);
                                resultMap.put(Constants.RESULT, Collections.emptyList());
                            }
                        }
                        logger.info("response {}", resultMap);
                    }
                    else {

                        resultMap.put(Constants.SUCCESS, false);
                        resultMap.put(Constants.DEVELOPER_MESSAGE, " At least one publishing channel should be enabled during the contract duration");
                    }
                }
                else {

                    Map configMap = clientService.getUserAndBusinessToCreateBusinessConfig(businessUUID);
                    int userId = Optional.ofNullable((Integer) configMap.get(Constants.USERID)).orElse(0);
                    int businessId = Optional.ofNullable((Integer) configMap.get(Constants.BUSINESSID)).orElse(0);


                    businessConfigurationsBean = BusinessConfigurationsBean.getDefaultBusinessConfiguration();
                    businessConfigurationsBean.setCreatedBy(userId);
                    businessConfigurationsBean.setBusinessId(businessId);

                    if(userId > 0 && businessId > 0){
                        Integer insertStatus = businessConfigurationsService.createBusinessConfigurations(businessConfigurationsBean,false);
                        response = businessConfigurationsService.getBusinessConfigurationsByBusinessId(businessUUID);
                        if (response.size() > 0) {
                            Map responseMap = (Map) response.get(0);
                            resultMap.put(Constants.SUCCESS, true);
                            resultMap.put(Constants.RESULT, responseMap);
                        } else {
                            resultMap.put(Constants.SUCCESS, true);
                            resultMap.put(Constants.RESULT, Collections.emptyList());
                        }
                    }else{
                        resultMap.put(Constants.SUCCESS, false);
                        resultMap.put(Constants.DEVELOPER_MESSAGE, "Business configurations not found");
                    }


                }
            }
            else{
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide businessUUID mandatory field");
            }
        }
        catch (Exception e){
            e.printStackTrace();
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
        }
        logger.info("End updateClientConfig");
        return new ResponseEntity<>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
    }


    /**
     * Create business configurations
     *
     * @param businessConfigurationsBean
     * @return
     */
    @PostMapping()
    @ResponseBody
    public ResponseEntity<String> createClientConfig(@Valid @RequestBody BusinessConfigurationsBean businessConfigurationsBean) {
        logger.info("begins client business config creation");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(businessConfigurationsBean)) {
                int insertStatus = businessConfigurationsService.createBusinessConfigurations(businessConfigurationsBean,false);
                if (insertStatus > 0) {
                    businessConfigVersionService.createBusinessVersionConfigurations(businessConfigurationsBean,false);
                    String businessUUID = clientService.getBusinessUUIDByID(businessConfigurationsBean.getBusinessId());
                    List response = businessConfigurationsService.getBusinessConfigurationsByBusinessId(businessUUID);
                    if (!response.isEmpty()) {
                        Map responseMap = (Map) response.get(0);
                        resultMap.put(Constants.SUCCESS, true);
                        resultMap.put(Constants.RESULT, responseMap);
                    } else {
                        resultMap.put(Constants.SUCCESS, true);
                        resultMap.put(Constants.RESULT, Collections.emptyList());
                    }
                }
            }
            else{
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide mandatory fields");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
        }
        logger.info("create client business creation ends");
        return new ResponseEntity<>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Get business configuration by business configuuid
     *
     * @param businessConfigUUID
     * @return
     */
    @GetMapping("{businessConfigUUID}")
    @ResponseBody
    public ResponseEntity<String> getClientConfigByConfigUUID(@PathVariable("businessConfigUUID") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessconfigId") String businessConfigUUID) {
        logger.info("START getClientConfigByConfigUUID");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(businessConfigUUID)) {
                int businessId = clientService.retrieveBusinessIdByConfigUUID(businessConfigUUID);
                String businessUUID = clientService.getBusinessUUIDByID(businessId);
                if (businessId > 0) {
                    List response = businessConfigurationsService.getBusinessConfigurationsByBusinessId(businessUUID);
                    if (response.size() > 0) {
                        Map responseMap = (Map) response.get(0);
                        resultMap.put(Constants.SUCCESS, true);
                        resultMap.put(Constants.RESULT, responseMap);
                    } else {
                        resultMap.put(Constants.SUCCESS, true);
                        resultMap.put(Constants.RESULT, Collections.emptyList());
                    }
                    logger.info("response {}", resultMap);
                } else {
                    resultMap.put(Constants.SUCCESS, false);
                    resultMap.put(Constants.ERROR, "Provided businessConfigUUID is invalid");
                }

            }
            else{
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide businessConfigUUID mandatory field");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
        }
        logger.info("get client business config details ends");
        return new ResponseEntity<>( new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
    }


    /**
     * Delete business configuration by business configuuid
     *
     * @param businessConfigUUID
     * @return
     */
    @DeleteMapping("{businessConfigUUID}")
    @ResponseBody
    public ResponseEntity<String> deleteConfigByConfigUUID(@PathVariable("businessConfigUUID") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessconfigId") String businessConfigUUID) {
        logger.info("get client business config details begins");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(businessConfigUUID)) {
                int businessId = clientService.retrieveBusinessIdByConfigUUID(businessConfigUUID);
                if (businessId > 0) {
                    int deleteStatus = businessConfigurationsService.deleteBusinessConfigurationsByConfigId(businessConfigUUID);
                    if (deleteStatus > 0) {
                        resultMap.put(Constants.SUCCESS, true);
                        resultMap.put(Constants.RESULT, Collections.emptyList());
                    } else {
                        resultMap.put(Constants.SUCCESS, false);
                        resultMap.put(Constants.ERROR, "bad request");
                        resultMap.put(Constants.DEVELOPER_MESSAGE, "business configurations was not deleted");
                    }
                    logger.info("response {}", resultMap);
                } else {
                    resultMap.put(Constants.SUCCESS, false);
                    resultMap.put(Constants.ERROR, "Provided businessConfigUUID is invalid");
                }

            }
            else{
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide businessConfigUUID mandatory field");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
        }
        logger.info("get client business config details ends");
        return new ResponseEntity<>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Delete business configurations by business UUID
     *
     * @param businessUUID
     * @return
     */
    @DeleteMapping("business/{clientId}")
    @ResponseBody
    public ResponseEntity<String> deleteClientConfigByClintUUID(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessId") String businessUUID) {
        logger.info("delete client business config details begins");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(businessUUID)) {
                int businessId = clientService.retrieveBusinessIDByUUID(businessUUID);
                if (businessId > 0) {
                    int deleteStatus = businessConfigurationsService.deleteBusinessConfigurationsByBusinessId(businessId);
                    if (deleteStatus > 0) {
                        resultMap.put(Constants.SUCCESS, true);
                        resultMap.put(Constants.RESULT, new ArrayList<>());
                    } else {
                        resultMap.put(Constants.SUCCESS, false);
                        resultMap.put(Constants.ERROR, "bad request");
                        resultMap.put(Constants.DEVELOPER_MESSAGE, "business configurations was not deleted");
                    }
                    logger.info("response {}", resultMap);
                } else {
                    resultMap.put(Constants.SUCCESS, false);
                    resultMap.put(Constants.ERROR, "Provided businessUUID is invalid");
                }

            }
            else{
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide businessUUID mandatory field");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
        }
        logger.info("delete client business config details ends");
        return new ResponseEntity<>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
    }


    /**
     * Get settings base64 encoded pdf by businessUUID
     *
     * @param businessUUID
     * @return
     */
    @GetMapping("generateSettingsPDF/{clientId}")
    @ResponseBody
    public ResponseEntity<String> getGeneratedSettingsPDFByClientId(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessId") String businessUUID) {
        logger.info("get business config settings encoded pdf begins");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(businessUUID)) {
                int isTrueUUID = clientService.retrieveBusinessIDByUUID(businessUUID);
                if (isTrueUUID > 0) {
                    String encodedPDF = pdfService.generateSettingsAttachmentPDF(businessUUID);
                    if (encodedPDF.length() > 0) {
                        resultMap.put(Constants.SUCCESS, true);
                        resultMap.put(Constants.RESULT, encodedPDF);
                    } else {
                        resultMap.put(Constants.SUCCESS, true);
                        resultMap.put(Constants.RESULT, new ArrayList<>());
                    }
                    logger.info("response {}", resultMap);
                } else {
                    resultMap.put(Constants.SUCCESS, false);
                    resultMap.put(Constants.ERROR, "Provided businessUUID is invalid");
                }
            }
            else{
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide businessUUID mandatory field");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
        }
        logger.info("get business config settings encoded pdf ends");
        return new ResponseEntity<>( new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
    }


    /**
     *  Get client business information by clientId
     *
     * @param businessUUID
     * @return
     */
    @GetMapping("/usage/{clientId}")
    @ResponseBody
    public ResponseEntity<String> getBusinessUsageByClientId(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessId") String businessUUID,
                                            @RequestParam(value = "source", defaultValue = "") String source) {
        logger.info("get client usage details begins");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            if (Utils.isNotNull(businessUUID)) {
                int isTrueUUID = clientService.retrieveBusinessIDByUUID(businessUUID);
                if (isTrueUUID > 0) {
                    Map response = clientService.getClientUsageByClientId(businessUUID, source);
                    if (response.size() > 0) {
                        resultMap.put(Constants.SUCCESS, true);
                        resultMap.put(Constants.RESULT, response);
                    } else {
                        resultMap.put(Constants.SUCCESS, true);
                        resultMap.put(Constants.RESULT, Collections.emptyList());
                    }
                    logger.info("response {}", resultMap);
                } else {
                    resultMap.put(Constants.SUCCESS, false);
                    resultMap.put(Constants.ERROR, "Provided businessUUID is invalid");
                }

            } else {
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide businessUUID mandatory field");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
        }
        logger.info("get client business details ends");
        return new ResponseEntity<>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     *  upsert text analytics data by clientId
     *
     * @param businessUUID
     * @return
     */
    @PostMapping("/txtanalytics/{clientId}")
    @ResponseBody
    public ResponseEntity<String> upsertTextAnalyticsByBusinessId(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessId") String businessUUID,
                                                  @RequestBody String data) {
        logger.info("upsert text analytics api start");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            if (Utils.isNotNull(businessUUID)) {
                int isTrueUUID = clientService.retrieveBusinessIDByUUID(businessUUID);
                if (isTrueUUID > 0) {
                    Map inputMap = new HashMap();
                    Map responseMap = businessConfigurationsService.upsertTextAnalytics(data, businessUUID);
                    if (responseMap.size() > 0) {
                        resultMap.put(Constants.SUCCESS,true);
                        resultMap.put(Constants.RESULT, responseMap);
                    } else {
                        resultMap.put(Constants.SUCCESS, true);
                        resultMap.put(Constants.RESULT, new ArrayList<>());
                    }
                } else {
                    resultMap.put(Constants.SUCCESS, false);
                    resultMap.put(Constants.ERROR, "Provided businessUUID is invalid");
                }

            } else {
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide businessUUID mandatory field");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
        }
        logger.info("upsert text analytics api end");
        return new ResponseEntity<>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
    }


    /**
     *
     * @param businessUUID
     * @return
     */
    @GetMapping("/txtanalytics/{clientId}")
    @ResponseBody
    public ResponseEntity<String> retrieveTextAnalyticsByBusinessId(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessId") String businessUUID) {
        logger.info("begin text analytics api start");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            if (Utils.isNotNull(businessUUID)) {
                int isTrueUUID = clientService.retrieveBusinessIDByUUID(businessUUID);
                if (isTrueUUID > 0) {
                    Map responseMap = businessConfigurationsService.retriveTextAnalyticsMapping(businessUUID);
                    if (responseMap.size() > 0) {
                        resultMap.put(Constants.SUCCESS, true);
                        resultMap.put(Constants.RESULT, responseMap);
                    } else {
                        resultMap.put(Constants.SUCCESS, true);
                        resultMap.put(Constants.RESULT, new ArrayList<>());
                    }
                } else {
                    resultMap.put(Constants.SUCCESS, false);
                    resultMap.put(Constants.ERROR, "Provided businessUUID is invalid");
                }

            } else {
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide businessUUID mandatory field");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
        }
        logger.info("end text analytics api end");
        return new ResponseEntity<>( new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
    }

}
