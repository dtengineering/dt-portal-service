package com.dropthought.portal.businessconfigurations.service;

import com.dropthought.portal.businessconfigurations.dao.BusinessConfigurationsDao;
import com.dropthought.portal.businessconfigurations.dto.BusinessConfigurationsBean;
import com.dropthought.portal.businessusage.BusinessUsageService;
import com.dropthought.portal.model.*;
import com.dropthought.portal.service.*;
import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.EmailTemplateUtility;
import com.dropthought.portal.util.TokenUtility;
import com.dropthought.portal.util.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stripe.model.tax.Registration;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.*;

@Component
public class BusinessConfigurationsService extends BaseService {

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final String insertDeactivatedKiosk = "insert into deactivate_kiosks(business_id, allowed_number, difference_number, created_time) values(?,?,?,?)";

    /**
     * JdbcTemplate for DB Connectivity.
     */
    @Autowired
    @Qualifier("jdbcDt")
    JdbcTemplate jdbcTemplate;

    @Autowired
    ClientService clientService;
    @Autowired
    RestTemplateService restTemplateService;

    @Autowired
    NotificationService notificationService;

    @Autowired
    TokenUtility tokenUtility;

    @Autowired
    PdfService pdfService;

    @Autowired
    IntegrationAppService integrationAppService;

    @Autowired
    BusinessUsageService businessUsageService;

    @Autowired
    BusinessConfigurationsDao businessConfigurationsDao;

    @Autowired
    TextAnalyticsConfigurationService textAnalyticsConfigurationService;

    @Autowired
    DeviceService deviceService;

    @Autowired
    private TextAnalyticsConfigurationService textAnalyticsConfigService;

    @Value("${dt.schedule.url}")
    private String dtScheduleUrl;

    @Value("${dt.event.url}")
    private String dtEventUrl;

    @Value("${dt.upsert.url}")
    private String dtUpsertUrl;

    @Value("${dt.kioskblock.url}")
    protected String uri;

    @Value("${dtp.domain}")
    private String dtpDomain;

    @Value("${idm.okta.url}")
    private String idmOKTAUrl;

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();

    /**
     * function to retrieve a business by businessUUID
     *
     * @param businessUUID
     * @return
     */
    public Map retrieveBusinessByUUID(String businessUUID) {
        logger.debug("begin retrieving client business by business uuid");
        String selectBusinessByUUID = "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid from business where business_uuid=?";

        Map businessMap = jdbcTemplate.queryForMap(selectBusinessByUUID, new Object[]{businessUUID});
        logger.debug("end retrieving client business by business uuid");
        return businessMap;
    }

    /**
     * Get business configuration by business uuid
     *
     * @param businessUUID
     * @return
     */
    public List getBusinessConfigurationsByBusinessId(String businessUUID) {
        List result = new ArrayList();
        logger.info("begin retrieving configs by business id");
        try {
            Integer businessId = clientService.retrieveBusinessIDByUUID(businessUUID);
            result = businessConfigurationsDao.getBusinessConfigurationsByBusinessId(businessId);
            //get business contract dates
            Map businessMap = clientService.getBusinessContractDates(businessId);

            List businessConfigurationList = new ArrayList();
            Iterator<Map> iterator = result.iterator();
            while (iterator.hasNext()) {
                Map businessConfigMap = iterator.next();
                if (Integer.parseInt(businessConfigMap.get("email").toString()) == 0 && Integer.parseInt(businessConfigMap.get("sms").toString()) == 0 && Integer.parseInt(businessConfigMap.get("whatsapp").toString()) == 0 && Integer.parseInt(businessConfigMap.get("qrcode").toString()) == 0 && Integer.parseInt(businessConfigMap.get("shareable_link").toString()) == 0) {
                    businessConfigMap.put("default", true);
                    result = new ArrayList();
                    result.add(businessConfigMap);
                }

                // Modify keys
                businessConfigMap.put("businessConfigId", businessConfigMap.remove("business_config_uuid"));
                businessConfigMap.put("businessId", businessConfigMap.remove("business_id"));
                businessConfigMap.put("sharableLink", businessConfigMap.remove("shareable_link"));
                businessConfigMap.put("textAnalytics", businessConfigMap.remove("text_analytics"));
                businessConfigMap.put("metadataQuestion", businessConfigMap.remove("metadata_question"));
                businessConfigMap.put("mobileApp", businessConfigMap.remove("mobile_app"));
                businessConfigMap.put("emotionAnalysis", businessConfigMap.remove("emotion_analysis"));
                businessConfigMap.put("intentAnalysis", businessConfigMap.remove("intent_analysis"));
                businessConfigMap.put("trigger", businessConfigMap.remove("trigger_toggle"));
                businessConfigMap.put("advancedSchedule", businessConfigMap.remove("advanced_schedule"));
                businessConfigMap.put("english", businessConfigMap.remove("english_toggle"));
                businessConfigMap.put("arabic", businessConfigMap.remove("arabic_toggle"));
                businessConfigMap.put("multiSurveys", businessConfigMap.remove("multi_surveys"));
                businessConfigMap.put("webHooks", businessConfigMap.remove("web_hooks"));
                businessConfigMap.put("preferredMetric", businessConfigMap.remove("preferred_metric"));
                businessConfigMap.put("notification", businessConfigMap.remove("notification"));
                businessConfigMap.put("categoryAnalysis", businessConfigMap.remove("category_analysis"));
                businessConfigMap.put("advancedTextAnalytics", businessConfigMap.remove("advanced_text_analytics"));
                businessConfigMap.put("noOfUsers", businessConfigMap.remove("no_of_users"));
                businessConfigMap.put("noOfActivePrograms", businessConfigMap.remove("no_of_active_programs"));
                businessConfigMap.put("noOfResponses", businessConfigMap.remove("no_of_responses"));
                businessConfigMap.put("noOfMetrics", businessConfigMap.remove("no_of_metrics"));
                businessConfigMap.put("noOfEmailsSent", businessConfigMap.remove("no_of_emails_sent"));
                businessConfigMap.put("noOfSms", businessConfigMap.remove("no_of_sms"));
                businessConfigMap.put("noOfWhatsapp", businessConfigMap.remove("no_of_whatsapp"));
                businessConfigMap.put("noOfApi", businessConfigMap.remove("no_of_api"));
                businessConfigMap.put("noOfFilters", businessConfigMap.remove("no_of_filters"));
                businessConfigMap.put("noOfTriggers", businessConfigMap.remove("no_of_triggers"));
                businessConfigMap.put("dataRetention", businessConfigMap.remove("data_retention"));
                businessConfigMap.put("noOfLists", businessConfigMap.remove("no_of_lists"));
                businessConfigMap.put("noOfRecipients", businessConfigMap.remove("no_of_recipients"));
                businessConfigMap.put("cohortAnalysis", businessConfigMap.remove("cohort_analysis"));
                businessConfigMap.put("advancedTemplates", businessConfigMap.remove("advanced_templates"));
                businessConfigMap.put("metricCorrelation", businessConfigMap.remove("metric_correlation"));
                businessConfigMap.put("apiKey", businessConfigMap.remove("api_key"));
                businessConfigMap.put("multipleSublink", businessConfigMap.remove("multiple_sublink"));
                businessConfigMap.put("advancedAnalysis", businessConfigMap.remove("advanced_analysis"));
                businessConfigMap.put("exportLinks", businessConfigMap.remove("export_links"));
                businessConfigMap.put("respondentTracker", businessConfigMap.remove("respondent_tracker"));
                businessConfigMap.put("fromCustomization", businessConfigMap.remove("from_customization"));
                businessConfigMap.put("noOfIntegrations", businessConfigMap.remove("no_of_integrations"));
                businessConfigMap.put("noOfKiosks", businessConfigMap.remove("no_of_kiosks"));
                businessConfigMap.put("dynamicLinks", businessConfigMap.remove("dynamic_links"));
                businessConfigMap.put("noOfShareableSublink", businessConfigMap.remove("no_of_shareable_sublink"));
                businessConfigMap.put("noOfMultipleStaticQr", businessConfigMap.remove("no_of_multiple_static_qr"));
                businessConfigMap.put("noOfDynamicSwitchQr", businessConfigMap.remove("no_of_dynamic_switch_qr"));
                businessConfigMap.put("noOfDynamicGroupQr", businessConfigMap.remove("no_of_dynamic_group_qr"));
                businessConfigMap.put("focusMetric", businessConfigMap.remove("focus_metric"));
                businessConfigMap.put("uniqueLinks", businessConfigMap.remove("unique_links"));
                businessConfigMap.put("sso", businessConfigMap.remove("sso"));
                businessConfigMap.put("programFeatures", businessConfigMap.remove("program_features"));
                businessConfigMap.put("advancedReport", businessConfigMap.remove("advanced_report"));
                businessConfigMap.put("gamePlan", businessConfigMap.remove("game_plan"));
                businessConfigMap.put("previouslyUsedQuestionLibrary", businessConfigMap.remove("previously_used_question_library"));
                businessConfigMap.put("predefinedQuestionLibrary", businessConfigMap.remove("predefined_question_library"));
                businessConfigMap.put("customThemes", businessConfigMap.remove("custom_themes"));
                businessConfigMap.put("auditProgram", businessConfigMap.remove("audit_program"));
                businessConfigMap.put("customDashboard", businessConfigMap.remove("custom_dashboard"));
                businessConfigMap.put("rCoefficient", businessConfigMap.remove("r_coefficient"));
                businessConfigMap.put("closeLoop", businessConfigMap.remove("close_loop"));
                businessConfigMap.put("programThrottling", businessConfigMap.remove("program_throttling"));
                businessConfigMap.put("submissionDelay", businessConfigMap.remove("submission_delay"));
                businessConfigMap.put("refreshKioskQr", businessConfigMap.remove("refresh_kiosk_qr"));
                businessConfigMap.put("aiSurveysLimit", businessConfigMap.remove("ai_surveys_limit"));
                businessConfigMap.put("taskManager", businessConfigMap.remove("task_manager"));
                //DTV-12899
                businessConfigMap.put("aiSurveys", businessConfigMap.remove("ai_surveys"));
                businessConfigMap.put("ltiConfig", businessConfigMap.remove("lti_config"));
                //DTV-13080
                businessConfigMap.put("responseQuota", businessConfigMap.remove("response_quota"));
                //DTV-13383
                businessConfigMap.put("customDashboardBuilder", businessConfigMap.remove("custom_dashboard_builder"));

                // Text Analytics Config
                int textAnalyticsConfigId = (businessConfigMap.containsKey("text_analytics_config_id") &&
                        businessConfigMap.get("text_analytics_config_id") != null) ?
                        Integer.parseInt(businessConfigMap.get("text_analytics_config_id").toString()) : 0;

                Map<String, Object> txtAnalyticsConfigMap = new HashMap<>();
                if (textAnalyticsConfigId > 0) {
                    txtAnalyticsConfigMap = constructTextAnalyticsConfigMap(businessConfigMap.get("textAnalytics"),
                            businessConfigMap.get("intentAnalysis"),
                            businessConfigMap.get("categoryAnalysis"),
                            textAnalyticsConfigId);
                }
                businessConfigMap.put("textAnalyticsConfig", txtAnalyticsConfigMap);
                businessConfigMap.remove("text_analytics_config_id");
                businessConfigMap.put("textAnalyticsConfigId", textAnalyticsConfigId);

                businessConfigMap.put("contractStartDate", businessMap.getOrDefault("contract_start_date", ""));
                businessConfigMap.put("contractEndDate", (Utils.isNotNull(businessMap.get("contract_end_date"))) ?
                        Utils.convertToEndOfDay(businessMap.get("contract_end_date").toString()) : "");

                businessConfigurationList.add(businessConfigMap);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        logger.info("end retrieving configs by business id");
        return result;
    }

    /**
     * function to construct textAnalyticsConfigMap by business id and text_analytics_config_id
     *
     * @param textAnalytics
     * @return
     */
    public Map<String, Object> constructTextAnalyticsConfigMap(Object textAnalytics, Object intentAnalysis,
                                                               Object categoryAnalysis, int textAnalyticsConfigId) {

        Map<String, Object> txtAnalyticsConfigMap = new HashMap<String, Object>();
        try {
            TextAnalyticsConfigBean textAnalyticsConfigBean = textAnalyticsConfigurationService.getTextAnalyticsConfig(textAnalyticsConfigId);
            Map<String, Object> textAnalyticsConfig = mapper.readValue(textAnalyticsConfigBean.getConfigs().toString(),
                    new TypeReference<Map<String, Object>>() {
                    });
            List<String> surveyIdCount = textAnalyticsConfig.containsKey("surveyId") && Utils.isNotNull(textAnalyticsConfig.get("surveyId")) &&
                    Utils.notEmptyString(textAnalyticsConfig.get("surveyId").toString())
                    ? (List) textAnalyticsConfig.get("surveyId") : new ArrayList<>();
            List<String> surveyIdIntentCount = new ArrayList<>();
            try {
                surveyIdIntentCount = textAnalyticsConfig.containsKey("surveyIdIntent") && Utils.isNotNull(textAnalyticsConfig.get("surveyIdIntent")) &&
                        Utils.notEmptyString(textAnalyticsConfig.get("surveyIdIntent").toString())
                        ? (List) textAnalyticsConfig.get("surveyIdIntent") : new ArrayList<>();
            } catch (Exception e) {
                logger.error(e.getMessage());
            }

            txtAnalyticsConfigMap.put("textAnalytics", textAnalytics);
            if (textAnalyticsConfigBean.getTextAnalyticsPlan().equals("advanced")) {
                txtAnalyticsConfigMap.put("textAnalyticsPlan", "Advanced Text analytics");
                txtAnalyticsConfigMap.put("categoryAndSentimentAnalysis", categoryAnalysis);
                txtAnalyticsConfigMap.put("intentAnalysis", intentAnalysis);
                if (intentAnalysis.toString().equals("1")) {
                    txtAnalyticsConfigMap.put("intentAnalysisSurveyCount", surveyIdIntentCount.size());
                }
            } else {
                txtAnalyticsConfigMap.put("textAnalyticsPlan", "Basic Text analytics");
            }
            txtAnalyticsConfigMap.put("surveyIdCount", surveyIdCount.size());

            int dataDuration = (Utils.isNotNull(textAnalyticsConfig.get("dataDurationId")) && Utils.notEmptyString(textAnalyticsConfig.get("dataDurationId").toString()))
                    ? Integer.parseInt(textAnalyticsConfig.get("dataDurationId").toString()) : 0;

            if (dataDuration == 1) {
                txtAnalyticsConfigMap.put("dataDuration", "Historical data");
            } else if (dataDuration == 2) {
                txtAnalyticsConfigMap.put("dataDuration", "Upcoming data");
            } else if (dataDuration == 3) {
                txtAnalyticsConfigMap.put("dataDuration", "Demo/dry run data");
                String startDate = textAnalyticsConfig.containsKey("startDate") && Utils.isNotNull(textAnalyticsConfig.get("startDate")) &&
                        Utils.notEmptyString(textAnalyticsConfig.get("startDate").toString())
                        ? textAnalyticsConfig.get("startDate").toString() : "";

                String endDate = textAnalyticsConfig.containsKey("endDate") && Utils.isNotNull(textAnalyticsConfig.get("endDate")) &&
                        Utils.notEmptyString(textAnalyticsConfig.get("endDate").toString())
                        ? textAnalyticsConfig.get("endDate").toString() : "";
                txtAnalyticsConfigMap.put("startDate", startDate);
                txtAnalyticsConfigMap.put("endDate", endDate);
            } else if (dataDuration == 4) {
                txtAnalyticsConfigMap.put("dataDuration", "Historical + Upcoming data");
            }
        } catch (Exception e) {
            logger.error("Error in constructTextAnalyticsConfigMap() : " + e.getMessage());
        }

        return txtAnalyticsConfigMap;
    }

    /**
     * function to update business configuration by business id
     *
     * @param businessConfigurationsBean
     * @return
     */
    public int updateBusinessConfigurations(BusinessConfigurationsBean businessConfigurationsBean, String businessUUID,
                                            Map oldBusinessConfigurationMap) {
        int updateStatus = 0;
        boolean publishEliza = false;
        String EMAIL = "email";
        String SMS = "sms";
        String QRCODE = "qr";
        String LINK = "link";
        String KIOSK = "kiosk";
        String WHATSAPP = "whatsapp";
        String EXPORTLINKS = "uniqueLinks";
        int oldApiKeyVal = 0;
        int newApiKeyVal = 0;
        //DTV-6355
        int oldTAVal = 0;
        int newTAVal = 0;
        //SE-1010
        int oldKioskCount = 0;
        int newKioskCount = 0;
        //DTV-7515
        int oldMfa = 0;
        int newMfa = 0;

        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            ArrayList<Object> objectArrayList = new ArrayList<Object>();
            Map channelValueMap = new HashMap();
            Map usageLimitMap = new HashMap();

            String sql = "update business_configurations set ";

            if (businessConfigurationsBean.getEmail() != null) {
                logger.info("business email config is present");
                objectArrayList.add(businessConfigurationsBean.getEmail());
                if (Integer.parseInt(businessConfigurationsBean.getEmail()) == 1) {
                    channelValueMap.put(EMAIL, true);
                } else {
                    channelValueMap.put(EMAIL, false);
                }
                sql = sql + "email = ?, ";
            }

            if (businessConfigurationsBean.getSms() != null) {
                logger.info("business sms config is present");
                objectArrayList.add(businessConfigurationsBean.getSms());
                if (Integer.parseInt(businessConfigurationsBean.getSms()) == 1) {
                    channelValueMap.put(SMS, true);
                } else {
                    channelValueMap.put(SMS, false);
                }
                sql = sql + "sms = ?, ";
            }

            if (businessConfigurationsBean.getTaskManager() != null) {
                logger.info("business task manager config is present");
                objectArrayList.add(businessConfigurationsBean.getTaskManager());
                sql = sql + "task_manager = ?, ";
            }

            if (businessConfigurationsBean.getWhatsapp() != null) {
                logger.info("business whatsapp config is present");
                objectArrayList.add(businessConfigurationsBean.getWhatsapp());
                if (Integer.parseInt(businessConfigurationsBean.getWhatsapp()) == 1) {
                    channelValueMap.put(WHATSAPP, true);
                } else {
                    channelValueMap.put(WHATSAPP, false);
                }
                sql = sql + "whatsapp = ?, ";
            }


            if (businessConfigurationsBean.getQrcode() != null) {
                logger.info("business qr config is present");
                objectArrayList.add(businessConfigurationsBean.getQrcode());
                if (Integer.parseInt(businessConfigurationsBean.getQrcode()) == 1) {
                    channelValueMap.put(QRCODE, true);
                } else {
                    channelValueMap.put(QRCODE, false);
                }
                sql = sql + "qrcode = ?, ";
            }

            if (businessConfigurationsBean.getShareableLink() != null) {
                logger.info("business shareable link config is present");
                objectArrayList.add(businessConfigurationsBean.getShareableLink());
                if (Integer.parseInt(businessConfigurationsBean.getShareableLink()) == 1) {
                    channelValueMap.put(LINK, true);
                } else {
                    channelValueMap.put(LINK, false);
                }
                sql = sql + "shareable_link = ?, ";
            }

            if (businessConfigurationsBean.getTextAnalytics() != null) {
                logger.info("business text analytics config is present");
                objectArrayList.add(businessConfigurationsBean.getTextAnalytics());
                sql = sql + "text_analytics = ?, ";
                publishEliza = (Integer.parseInt(businessConfigurationsBean.getTextAnalytics().toString()) == 1) ? true : false;
                oldTAVal = (Utils.isNotNull(oldBusinessConfigurationMap.get("textAnalytics")) && Utils.notEmptyString(oldBusinessConfigurationMap.get("textAnalytics").toString())) ? Integer.parseInt(oldBusinessConfigurationMap.get("textAnalytics").toString()) : 0;
                newTAVal = Utils.notEmptyString(businessConfigurationsBean.getTextAnalytics()) ? Integer.parseInt(businessConfigurationsBean.getTextAnalytics()) : 0;
            }

            if (businessConfigurationsBean.getLogic() != null) {
                logger.info("business logic config is present");
                objectArrayList.add(businessConfigurationsBean.getLogic());
                sql = sql + "logic = ?, ";
            }

            if (businessConfigurationsBean.getRespondent() != null) {
                logger.info("business respondent config is present");
                objectArrayList.add(businessConfigurationsBean.getRespondent());
                sql = sql + "respondent = ?, ";
            }

            if (businessConfigurationsBean.getSegment() != null) {
                logger.info("business segment config is present");
                objectArrayList.add(businessConfigurationsBean.getSegment());
                sql = sql + "segment = ?, ";
            }

            if (businessConfigurationsBean.getKiosk() != null) {
                logger.info("business kiosk config is present");
                objectArrayList.add(businessConfigurationsBean.getKiosk());
                if (Integer.parseInt(businessConfigurationsBean.getKiosk()) == 1) {
                    channelValueMap.put(KIOSK, true);
                } else {
                    channelValueMap.put(KIOSK, false);
                }
                sql = sql + "kiosk = ?, ";
            }

            if (businessConfigurationsBean.getMetadataQuestion() != null) {
                logger.info("business settings metadataQuestion is present");
                objectArrayList.add(businessConfigurationsBean.getMetadataQuestion());
                sql = sql + "metadata_question = ?, ";
            }

            if (businessConfigurationsBean.getMobileApp() != null) {
                logger.info("business settings mobile app is present");
                objectArrayList.add(businessConfigurationsBean.getMobileApp());
                sql = sql + "mobile_app = ?, ";
            }

            if (businessConfigurationsBean.getEmotionAnalysis() != null) {
                logger.info("business settings emotion analysis is present");
                objectArrayList.add(businessConfigurationsBean.getEmotionAnalysis());
                sql = sql + "emotion_analysis = ?, ";
            }

            if (businessConfigurationsBean.getIntentAnalysis() != null) {
                logger.info("business settings intent analysis is present");
                objectArrayList.add(businessConfigurationsBean.getIntentAnalysis());
                sql = sql + "intent_analysis = ?, ";
            }

            if (businessConfigurationsBean.getTrigger() != null) {
                logger.info("business settings trigger config is present");
                objectArrayList.add(businessConfigurationsBean.getTrigger());
                sql = sql + "trigger_toggle = ?, ";
            }

            if (businessConfigurationsBean.getAdvancedSchedule() != null) {
                logger.info("business settings advanced schedule is present");
                objectArrayList.add(businessConfigurationsBean.getAdvancedSchedule());
                sql = sql + "advanced_schedule = ?, ";
            }

            if (businessConfigurationsBean.getEnglish() != null) {
                logger.info("business settings english is present");
                objectArrayList.add(businessConfigurationsBean.getEnglish());
                sql = sql + "english_toggle = ?, ";
            }

            if (businessConfigurationsBean.getArabic() != null) {
                logger.info("business settings arabic is present");
                objectArrayList.add(businessConfigurationsBean.getArabic());
                sql = sql + "arabic_toggle = ?, ";
            }

            if (businessConfigurationsBean.getMultiSurveys() != null) {
                logger.info("business settings multi surveys is present");
                objectArrayList.add(businessConfigurationsBean.getMultiSurveys());
                sql = sql + "multi_surveys = ?, ";
            }

            if (businessConfigurationsBean.getBitly() != null) {
                logger.info("business settings bitly is present");
                objectArrayList.add(businessConfigurationsBean.getBitly());
                sql = sql + "bitly = ?, ";
            }

            if (businessConfigurationsBean.getWebHooks() != null) {
                logger.info("business settings web hooks is present");
                objectArrayList.add(businessConfigurationsBean.getWebHooks());
                sql = sql + "web_hooks = ?, ";
            }

            if (businessConfigurationsBean.getPreferredMetric() != null) {
                logger.info("business settings preferred metric is present");
                objectArrayList.add(businessConfigurationsBean.getPreferredMetric());
                sql = sql + "preferred_metric = ?, ";
            }

            if (businessConfigurationsBean.getCategoryAnalysis() != null) {
                logger.info("business settings category analysis is present");
                objectArrayList.add(businessConfigurationsBean.getCategoryAnalysis());
                sql = sql + "category_analysis = ?, ";
            }

            if (businessConfigurationsBean.getAdvancedTextAnalytics() != null) {
                logger.info("business settings advanced text analytics is present");
                objectArrayList.add(businessConfigurationsBean.getAdvancedTextAnalytics());
                sql = sql + "advanced_text_analytics = ?, ";
            }

            if (businessConfigurationsBean.getNoOfUsers() != null) {
                logger.info("business settings no of users is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfUsers());
                sql = sql + "no_of_users = ?, ";
                usageLimitMap.put("users", businessConfigurationsBean.getNoOfUsers());
            }

            if (businessConfigurationsBean.getNoOfActivePrograms() != null) {
                logger.info("business settings no of active programs is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfActivePrograms());
                sql = sql + "no_of_active_programs = ?, ";
                usageLimitMap.put("programs", businessConfigurationsBean.getNoOfActivePrograms());
            }

            if (businessConfigurationsBean.getNoOfResponses() != null) {
                logger.info("business settings no of responses is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfResponses());
                sql = sql + "no_of_responses = ?, ";
                usageLimitMap.put("responses", businessConfigurationsBean.getNoOfResponses());

                int oldLimit = (Utils.isNotNull(oldBusinessConfigurationMap.get("noOfResponses")) && Utils.notEmptyString(oldBusinessConfigurationMap.get("noOfResponses").toString())) ? Integer.parseInt(oldBusinessConfigurationMap.get("noOfResponses").toString()) : -1;
                int newLimit = businessConfigurationsBean.getNoOfResponses();
                this.reprocessNewResponses(businessUUID, oldLimit, newLimit, "responses");
            }

            if (businessConfigurationsBean.getNoOfMetrics() != null) {
                logger.info("business settings no of metrics is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfMetrics());
                sql = sql + "no_of_metrics = ?, ";
                usageLimitMap.put("metrics", businessConfigurationsBean.getNoOfMetrics());
            }

            if (businessConfigurationsBean.getNoOfEmailsSent() != null) {
                logger.info("business settings no of emails sent is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfEmailsSent());
                sql = sql + "no_of_emails_sent = ?, ";
                usageLimitMap.put("emails", businessConfigurationsBean.getNoOfEmailsSent());
            }

            if (businessConfigurationsBean.getNoOfSms() != null) {
                logger.info("business settings no of sms sent is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfSms());
                sql = sql + "no_of_sms = ?, ";
                usageLimitMap.put("sms", businessConfigurationsBean.getNoOfSms());
            }

            if (businessConfigurationsBean.getNoOfWhatsapp() != null) {
                logger.info("business settings no of whatsapp sent is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfWhatsapp());
                sql = sql + "no_of_whatsapp = ?, ";
                usageLimitMap.put("whatsapp", businessConfigurationsBean.getNoOfWhatsapp());
            }

            if (businessConfigurationsBean.getRCoefficient() != null) {
                logger.info("business settings r coefficient is present");
                objectArrayList.add(businessConfigurationsBean.getRCoefficient());
                sql = sql + "r_coefficient = ?,";
            }

            if (businessConfigurationsBean.getNoOfApi() != null) {
                logger.info("business settings no of api sent is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfApi());
                sql = sql + "no_of_api = ?, ";
                usageLimitMap.put("api", businessConfigurationsBean.getNoOfApi());
            }

            if (businessConfigurationsBean.getNoOfFilters() != null) {
                logger.info("business settings no of filters sent is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfFilters());
                sql = sql + "no_of_filters = ?, ";
                usageLimitMap.put("filters", businessConfigurationsBean.getNoOfFilters());
            }

            if (businessConfigurationsBean.getNoOfTriggers() != null) {
                logger.info("business settings no of triggers sent is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfTriggers());
                sql = sql + "no_of_triggers = ?, ";
                usageLimitMap.put("triggers", businessConfigurationsBean.getNoOfTriggers());
            }

            if (businessConfigurationsBean.getDataRetention() != null) {
                logger.info("business settings data retention sent is present");
                objectArrayList.add(businessConfigurationsBean.getDataRetention());
                sql = sql + "data_retention = ?, ";
            }

            if (businessConfigurationsBean.getNoOfLists() != null) {
                logger.info("business settings no of lists sent is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfLists());
                sql = sql + "no_of_lists = ?, ";
                usageLimitMap.put("lists", businessConfigurationsBean.getNoOfLists());
            }

            if (businessConfigurationsBean.getNoOfRecipients() != null) {
                logger.info("business settings no of recipients sent is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfRecipients());
                sql = sql + "no_of_recipients = ?, ";
                usageLimitMap.put("recipients", businessConfigurationsBean.getNoOfRecipients());

                int oldLimit = (Utils.isNotNull(oldBusinessConfigurationMap.get("noOfRecipients")) && Utils.notEmptyString(oldBusinessConfigurationMap.get("noOfRecipients").toString())) ? Integer.parseInt(oldBusinessConfigurationMap.get("noOfRecipients").toString()) : -1;
                int newLimit = businessConfigurationsBean.getNoOfRecipients();
                logger.info("oldLimit" + oldLimit);
                logger.info("newLimit" + newLimit);
                logger.info("businessUUID" + businessUUID);
                this.reprocessNewResponses(businessUUID, oldLimit, newLimit, "recipients");
            }

            if (businessConfigurationsBean.getModifiedBy() != null) {
                logger.info("user id is present");
                objectArrayList.add(businessConfigurationsBean.getModifiedBy());
                sql = sql + "modified_by = ?,";
            }

            if (businessConfigurationsBean.getHippa() != null) {
                logger.info("business hippa config is present");
                objectArrayList.add(businessConfigurationsBean.getHippa());
                sql = sql + "hippa = ?, ";
            }

            if (businessConfigurationsBean.getCohortAnalysis() != null) {
                logger.info("business cohort analysis config is present");
                objectArrayList.add(businessConfigurationsBean.getCohortAnalysis());
                sql = sql + "cohort_analysis = ?, ";
            }

            if (businessConfigurationsBean.getAdvancedTemplates() != null) {
                logger.info("business advanced templates config is present");
                objectArrayList.add(businessConfigurationsBean.getAdvancedTemplates());
                sql = sql + "advanced_templates = ?, ";
                if (businessConfigurationsBean.getAdvancedTemplates().equals("0")) {
                    //when the advanced_templates is turned off  for a client, then advance template mapping for the business has to removed: DTV-4730
                    this.removeBusinessFromTemplates(businessConfigurationsBean.getBusinessId());
                }
            }

            if (businessConfigurationsBean.getMetricCorrelation() != null) {
                logger.info("business metric correlation config is present");
                objectArrayList.add(businessConfigurationsBean.getMetricCorrelation());
                sql = sql + "metric_correlation = ?, ";
            }

            if (businessConfigurationsBean.getApiKey() != null) {
                logger.info("business api key config is present");
                objectArrayList.add(businessConfigurationsBean.getApiKey());
                sql = sql + "api_key = ?, ";
                oldApiKeyVal = (Utils.isNotNull(oldBusinessConfigurationMap.get("apiKey")) && Utils.notEmptyString(oldBusinessConfigurationMap.get("apiKey").toString())) ? Integer.parseInt(oldBusinessConfigurationMap.get("apiKey").toString()) : 0;
                newApiKeyVal = Utils.notEmptyString(businessConfigurationsBean.getApiKey()) ? Integer.parseInt(businessConfigurationsBean.getApiKey()) : 0;
            }

            if (businessConfigurationsBean.getWhatfix() != null) {
                logger.info("business whatfix config is present");
                objectArrayList.add(businessConfigurationsBean.getWhatfix());
                sql = sql + "whatfix = ?, ";
            }

            if (businessConfigurationsBean.getMultipleSublink() != null) {
                logger.info("business multiple sublink config is present");
                objectArrayList.add(businessConfigurationsBean.getMultipleSublink());
                sql = sql + "multiple_sublink = ?, ";
            }

            if (businessConfigurationsBean.getAdvancedAnalysis() != null) {
                logger.info("business advanced analysis config is present");
                objectArrayList.add(businessConfigurationsBean.getAdvancedAnalysis());
                sql = sql + "advanced_analysis = ?, ";
            }

            if (businessConfigurationsBean.getExportLinks() != null) {
                logger.info("business export links config is present");
                objectArrayList.add(businessConfigurationsBean.getExportLinks());
                if (Integer.parseInt(businessConfigurationsBean.getExportLinks()) == 1) {
                    channelValueMap.put(EXPORTLINKS, true);
                } else {
                    channelValueMap.put(EXPORTLINKS, false);
                }
                sql = sql + "export_links = ?, ";
            }

            if (businessConfigurationsBean.getRespondentTracker() != null) {
                logger.info("business respondent tracker config is present");
                objectArrayList.add(businessConfigurationsBean.getRespondentTracker());
                sql = sql + "respondent_tracker = ?, ";
            }

            if (businessConfigurationsBean.getFromCustomization() != null) {
                logger.info("business from customization config is present");
                objectArrayList.add(businessConfigurationsBean.getFromCustomization());
                sql = sql + "from_customization = ?, ";
            }

            if (businessConfigurationsBean.getIntegrations() != null) {
                logger.info("business settings integrations config is present");
                objectArrayList.add(businessConfigurationsBean.getIntegrations());
                sql = sql + "integrations = ?, ";
            }

            if (businessConfigurationsBean.getNoOfIntegrations() != null) {
                logger.info("business settings no of integrations is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfIntegrations());
                sql = sql + "no_of_integrations = ?, ";
                usageLimitMap.put("integrations", businessConfigurationsBean.getNoOfIntegrations());
            }

            if (businessConfigurationsBean.getNoOfKiosks() != null) {
                logger.info("business settings no of kiosks is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfKiosks());
                sql = sql + "no_of_kiosks = ?, ";
                usageLimitMap.put("kiosks", businessConfigurationsBean.getNoOfKiosks());
                oldKioskCount = (Utils.isNotNull(oldBusinessConfigurationMap.get("noOfKiosks")) && Utils.notEmptyString(oldBusinessConfigurationMap.get("noOfKiosks").toString())) ? Integer.parseInt(oldBusinessConfigurationMap.get("noOfKiosks").toString()) : -1;
                newKioskCount = businessConfigurationsBean.getNoOfKiosks();
            }

            if (businessConfigurationsBean.getDynamicLinks() != null) {
                logger.info("business settings dynamicLinks config is present");
                objectArrayList.add(businessConfigurationsBean.getDynamicLinks());
                sql = sql + "dynamic_links = ?, ";
            }

            if (businessConfigurationsBean.getNoOfShareableSublink() != null) {
                logger.info("business settings no of shareable sublink is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfShareableSublink());
                sql = sql + "no_of_shareable_sublink = ?, ";
                //usageLimitMap.put("shareableSublink", businessConfigurationsBean.getNoOfShareableSublink());
            }

            if (businessConfigurationsBean.getNoOfMultipleStaticQr() != null) {
                logger.info("business settings no of multiple static qr is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfMultipleStaticQr());
                sql = sql + "no_of_multiple_static_qr = ?, ";
                //usageLimitMap.put("multipleStaticQr", businessConfigurationsBean.getNoOfMultipleStaticQr());
            }

            if (businessConfigurationsBean.getNoOfDynamicSwitchQr() != null) {
                logger.info("business settings no of dynamic switch qr is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfDynamicSwitchQr());
                sql = sql + "no_of_dynamic_switch_qr = ?, ";
                //usageLimitMap.put("dynamicSwitchQr", businessConfigurationsBean.getNoOfDynamicSwitchQr());
            }

            if (businessConfigurationsBean.getNoOfDynamicGroupQr() != null) {
                logger.info("business settings no of dynamic group qr is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfDynamicGroupQr());
                sql = sql + "no_of_dynamic_group_qr = ?, ";
                //usageLimitMap.put("dynamicGroupQr", businessConfigurationsBean.getNoOfDynamicGroupQr());
            }

            if (businessConfigurationsBean.getMfa() != null) {
                logger.info("business mfa config is present");
                objectArrayList.add(businessConfigurationsBean.getMfa());
                oldMfa = (Utils.isNotNull(oldBusinessConfigurationMap.get("mfa")) && Utils.notEmptyString(oldBusinessConfigurationMap.get("mfa").toString())) ? Integer.parseInt(oldBusinessConfigurationMap.get("mfa").toString()) : 0;
                newMfa = Utils.notEmptyString(businessConfigurationsBean.getMfa()) ? Integer.parseInt(businessConfigurationsBean.getMfa()) : 0;
                sql = sql + "mfa = ?, ";
            }

            if (businessConfigurationsBean.getFocusMetric() != null) {
                logger.info("business settings focus metric is present");
                Map focusMap = new HashMap();
                focusMap.put("focusMetric", businessConfigurationsBean.getFocusMetric());
                focusMap.put("defaultRecommendation", businessConfigurationsBean.getDefaultRecommendation() != null ? businessConfigurationsBean.getDefaultRecommendation() : "0");
                focusMap.put("userDefinedRecommendation", businessConfigurationsBean.getUserDefinedRecommendation() != null ? businessConfigurationsBean.getUserDefinedRecommendation() : "0");
                objectArrayList.add(new JSONObject(focusMap).toString());
                sql = sql + "focus_metric = ?, ";
            }

            if (businessConfigurationsBean.getUniqueLinks() != null) {
                logger.info("business settings unique links is present");
                Map uniqueMap = new HashMap();
                uniqueMap.put("uniqueLinkEmail", businessConfigurationsBean.getUniqueLinkEmail() != null ? businessConfigurationsBean.getUniqueLinkEmail() : "0");
                uniqueMap.put("uniqueLinkSms", businessConfigurationsBean.getUniqueLinkSms() != null ? businessConfigurationsBean.getUniqueLinkSms() : "0");
                uniqueMap.put("uniqueLinkWhatsapp", businessConfigurationsBean.getUniqueLinkWhatsapp() != null ? businessConfigurationsBean.getUniqueLinkWhatsapp() : "0");
                objectArrayList.add(new JSONObject(uniqueMap).toString());
                sql = sql + "unique_links = ?, ";
            }

            if (businessConfigurationsBean.getSso() != null) {
                logger.info("business settings sso is present");
                Map ssoMap = new HashMap();
                ssoMap.put("googleSSO", businessConfigurationsBean.getGoogleSSO() != null ? businessConfigurationsBean.getGoogleSSO() : "0");
                ssoMap.put("microsoftSSO", businessConfigurationsBean.getMicrosoftSSO() != null ? businessConfigurationsBean.getMicrosoftSSO() : "0");
                ssoMap.put("appleSSO", businessConfigurationsBean.getAppleSSO() != null ? businessConfigurationsBean.getAppleSSO() : "0");
                objectArrayList.add(new JSONObject(ssoMap).toString());
                sql = sql + "sso = ?, ";
            }

            if (businessConfigurationsBean.getProgramFeatures() != null) {
                logger.info("business settings program features is present");
                Map programMap = new HashMap();
                programMap.put("programOverview", businessConfigurationsBean.getProgramOverview() != null ? businessConfigurationsBean.getProgramOverview() : "0");
                programMap.put("programMetric", businessConfigurationsBean.getProgramMetric() != null ? businessConfigurationsBean.getProgramMetric() : "0");
                objectArrayList.add(new JSONObject(programMap).toString());
                sql = sql + "program_features = ?, ";
            }

            if (businessConfigurationsBean.getAdvancedReport() != null) {
                logger.info("business settings Advanced Report is present");
                objectArrayList.add(businessConfigurationsBean.getAdvancedReport());
                sql = sql + "advanced_report = ?, ";
            }

            if (businessConfigurationsBean.getGamePlan() != null) {
                logger.info("business settings game plan is present");
                objectArrayList.add(businessConfigurationsBean.getGamePlan());
                sql = sql + "game_plan = ?, ";
            }

            if (businessConfigurationsBean.getPreviouslyUsedQuestionLibrary() != null) {
                logger.info("business settings previously used question library is present");
                objectArrayList.add(businessConfigurationsBean.getPreviouslyUsedQuestionLibrary());
                sql = sql + "previously_used_question_library = ?,";
            }

            if (businessConfigurationsBean.getPredefinedQuestionLibrary() != null) {
                logger.info("business settings predefined question library is present");
                objectArrayList.add(businessConfigurationsBean.getPredefinedQuestionLibrary());
                sql = sql + "predefined_question_library = ?,";
            }

            if (businessConfigurationsBean.getCustomThemes() != null) {
                logger.info("business custom themes config is present");
                objectArrayList.add(businessConfigurationsBean.getCustomThemes());
                sql = sql + "custom_themes = ?, ";
                if (businessConfigurationsBean.getCustomThemes().equals("0")) {
                    //test below function
                    this.removeBusinessFromCustomThemes(businessConfigurationsBean.getBusinessId());
                }
            }

            //DTV-9037
            //chatbot, audit_program, collaboration, custom_dashboard
            if (businessConfigurationsBean.getChatbot() != null) {
                logger.info("business chatbot config is present");
                objectArrayList.add(businessConfigurationsBean.getChatbot());
                sql = sql + " chatbot = ?, ";
            }

            if (businessConfigurationsBean.getAuditProgram() != null) {
                logger.info("business auditProgram config is present");
                objectArrayList.add(businessConfigurationsBean.getAuditProgram());
                sql = sql + " audit_program = ?, ";

                //collaboration is sub feature of audit program
                if (businessConfigurationsBean.getCollaboration() != null) {
                    logger.info("business collaboration config is present");
                    objectArrayList.add(businessConfigurationsBean.getCollaboration());
                    sql = sql + " collaboration = ?, ";
                }
            }

            if (businessConfigurationsBean.getCustomDashboard() != null) {
                logger.info("business customDashboard config is present");
                objectArrayList.add(businessConfigurationsBean.getCustomDashboard());
                sql = sql + " custom_dashboard = ?, ";
            }

            if (businessConfigurationsBean.getCloseLoop() != null) {
                logger.info("business settings close loop is present");
                objectArrayList.add(businessConfigurationsBean.getCloseLoop());
                sql = sql + "close_loop = ?,";
            }

            if (businessConfigurationsBean.getProgramThrottling() != null) {
                logger.info("business settings program throttling is present");
                objectArrayList.add(businessConfigurationsBean.getProgramThrottling());
                sql = sql + "program_throttling = ?,";
            }

            if (businessConfigurationsBean.getNotes() != null) {
                logger.info("business settings for notes is present");
                objectArrayList.add(businessConfigurationsBean.getNotes());
                sql = sql + "notes = ?, ";
            }

            //DTV-9719
            if (businessConfigurationsBean.getTextAnalyticsConfigId() != null) {
                logger.info("business settings text analytics config id is present");
                objectArrayList.add(businessConfigurationsBean.getTextAnalyticsConfigId());
                sql = sql + "text_analytics_config_id = ?, ";
            }
            //DTV-11388
            if (businessConfigurationsBean.getSubmissionDelay() != null) {
                logger.info("business settings submission_delay present");
                objectArrayList.add(businessConfigurationsBean.getSubmissionDelay());
                sql = sql + "submission_delay = ?, ";
            }

            //DTV-12899
            if (businessConfigurationsBean.getAiSurveys() != null) {
                logger.info("business settings ai surveys present");
                objectArrayList.add(businessConfigurationsBean.getAiSurveys());
                sql = sql + " ai_surveys = ?, ";
            }


            if (!objectArrayList.isEmpty()) {

                objectArrayList.add(timestamp);
                sql = sql + "modified_time = ? ";

                if (businessConfigurationsBean.getBusinessId() != null) {

                    sql = sql + "where business_id = ? ";
                    logger.info("sql " + sql);
                    objectArrayList.add(businessConfigurationsBean.getBusinessId());
                    if (publishEliza) {
                        /**
                         * commenting this part to save time. If necessary the api can be invoked manually
                         */
                        //clientService.updateClientConfigEliza(businessUUID);
                    }
                    logger.info("Executing the query");
                    updateStatus = jdbcTemplate.update(sql, objectArrayList.toArray());
                    logger.info("update status " + updateStatus);
                }
            }


            // update business_email_alert in business_global_settings table
            String globalSettingsSql = "update business_global_settings set ";
            List<Object> settingsObjects = new ArrayList<Object>();

            if (businessConfigurationsBean.getNotification() != null) {
                logger.info("business notification config is present");
                settingsObjects.add(businessConfigurationsBean.getNotification());
                globalSettingsSql = globalSettingsSql + "business_email_alert = ?, ";
            }

            if (settingsObjects.size() > 0) {

                globalSettingsSql = globalSettingsSql + "modified_time = ?";
                settingsObjects.add(timestamp);

                globalSettingsSql = globalSettingsSql + "where business_id = ? ";
                logger.info("globalSettingsSql " + globalSettingsSql);
                settingsObjects.add(businessConfigurationsBean.getBusinessId());

                logger.info("Executing the business gloabl settings query");
                updateStatus = jdbcTemplate.update(globalSettingsSql, settingsObjects.toArray());

                logger.info("update status " + updateStatus);

                //insert tracker config
                trackConfigParameters(businessConfigurationsBean.getBusinessId());

            }

            if (updateStatus > 0) {
                //Get business configurations for the the business
                Map mailParam = new HashMap();
                //send email
                Map kingUserMap = clientService.getKingUserByBusinessId(businessConfigurationsBean.getBusinessId());
                mailParam.put("toEmail", kingUserMap.containsKey("user_email") && kingUserMap.get("user_email") != null ? kingUserMap.get("user_email").toString() : "");
                mailParam.put("recipientName", kingUserMap.containsKey("full_name") && kingUserMap.get("full_name") != null ? kingUserMap.get("full_name").toString() : "");
                String encodedPDf = pdfService.generateSettingsAttachmentPDF(businessUUID);
                logger.info("encodedPDF  " + encodedPDf);
                mailParam.put("attachment", encodedPDf);
                mailParam.put("attachmentType", "pdf");
                mailParam.put("businessUUID", businessUUID);
                mailParam.put("EnvironmentInfo", EmailTemplateUtility.getEnvironmentEmailContent(dtpDomain, false));
                logger.info("dtpDomain : " + dtpDomain);
                notificationService.sendBusinessSettingsReminder(mailParam, businessConfigurationsBean.getBusinessId(), false);
            }

            // to update the channels for active surveys in survey schedule for given business account
            if (channelValueMap.size() > 0 && businessConfigurationsBean.getBusinessId() != null) {
                logger.info("begin update the channels in survey schedule");
                Map resultMap = new HashMap();
                String CHANNEL = "channel";
                int businessId = businessConfigurationsBean.getBusinessId();
                try {
                    Map inputMap = new HashMap();
                    final String uri = dtScheduleUrl + "" + businessId + "/" + CHANNEL;
                    logger.info("dtScheduleUrl  {}", dtScheduleUrl);
                    logger.info("uri {}", uri);

                    businessConfigurationsBean.setChannels(channelValueMap);
                    //convert bean to Map
                    inputMap = mapper.convertValue(businessConfigurationsBean, new TypeReference<Map>() {
                    });
                    resultMap = callRestApi(inputMap, uri, HttpMethod.PUT);
                    logger.info("End update the channels in survey schedule");

                } catch (Exception e) {
                    logger.error(e.getMessage());
                    //logger.error(e.getMessage());
                }
            }

            //to update the usage_mail_flag when limits are changed
            if (usageLimitMap.size() > 0) {
                businessUsageService.updateUsageMailFlagWithNewLimitsByBusinessId(businessConfigurationsBean.getBusinessId(), usageLimitMap);
            }

            /**any change to apiKey from configurations
             updating apiKey value in api_key table
             if apiKey value is 1 --> creating or enabling apiKey by businessId
             if apiKey value is 0 --> disabling apiKey by businessId
             **/
            logger.info("oldApiKeyVal  " + oldApiKeyVal);
            logger.info("newApiKeyVal  " + newApiKeyVal);
            if (oldApiKeyVal != newApiKeyVal) {
                this.updateApiKeyByBusinessId(businessConfigurationsBean.getBusinessId(), newApiKeyVal);
            }

            /**
             * DTV-6355
             * Calling Text analytics settings API only when TA is modified
             **/
            logger.info("oldTAVal  " + oldTAVal);
            logger.info("newTAVal  " + newTAVal);
            if (oldTAVal != newTAVal) {
                this.updateElizaTextAnalyticsStatusByBusinessId(businessUUID, businessConfigurationsBean.getBusinessId(), newTAVal);
            }

            /**
             * DTV-7175
             * Calling insights api for mfa enable/disable
             */
            logger.info("old mfa val = " + oldMfa);
            logger.info("new mfa val = " + newMfa);
            if (oldMfa != newMfa) {
                String urlToCallMfa = idmOKTAUrl + "okta/users/" + businessUUID + "/factors";
                if (newMfa == 1 || newMfa == 2) {
                    restTemplateService.callInsightsApiForMfa(urlToCallMfa, HttpMethod.POST);
                } else if (newMfa == 0) {
                    restTemplateService.callInsightsApiForMfa(urlToCallMfa, HttpMethod.DELETE);
                }

            }

            //SE-1010
            if (oldKioskCount != newKioskCount) {
                int existNumberKiosk = oldKioskCount;
                int updateNumberKiosk = newKioskCount;
                int businessId = businessConfigurationsBean.getBusinessId();
                String tableName = "device_registration";
                String registeredNumSql = "select count(*) from " + tableName + " where business_id = ?";
                Integer registeredCount = jdbcTemplate.queryForObject(registeredNumSql, new Object[]{businessId}, Integer.class);
                //if no. of kiosk < registered_devices
                //
                //      call the block api
                //
                //if the no. of kiosk> registered devices and the (prev no.of kiosks < registered devices)
                //
                //     call the unblock api
                String hostName = dtpDomain;
                Map keyMap = deviceService.computeUriandApiKey(hostName);
                String urlToCall = keyMap.get("url").toString() + businessUUID + "/block";
                String kioskKey = keyMap.get("kioskKey").toString();
                if (updateNumberKiosk < registeredCount && updateNumberKiosk != -1) {
                    Map inputMap = new HashMap();
                    inputMap.put("blockType", "expire");//"blockType": "expire", // required, this represent the reason for the blocking, can use expire for now.
                    inputMap.put("timestamp", System.currentTimeMillis());//"timestamp": "1651348576" // expire data's timestamp
                    logger.info("long timestamp = " + System.currentTimeMillis());
                    deviceService.callRestApi(inputMap, urlToCall, HttpMethod.POST, kioskKey);
                } else if ((updateNumberKiosk >= registeredCount && existNumberKiosk <= registeredCount) || updateNumberKiosk == -1) {
                    Map inputMap = new HashMap();
                    deviceService.callRestApi(inputMap, urlToCall, HttpMethod.DELETE, kioskKey);
                }
                deviceService.updateDeactivatedKiosks(businessUUID, existNumberKiosk, updateNumberKiosk);
                //SE-1010
                deviceService.insertBusinessTrackConfig(businessId, "noOfKiosk");
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return updateStatus;
    }


    /**
     * fucntion to perform REST callback to external application
     *
     * @param inputMap
     * @param uri
     * @param method
     * @return
     */
    private Map callRestApi(Map inputMap, String uri, HttpMethod method) {
        Map respMap = new HashMap();
        JSONObject json = new JSONObject(inputMap);
        String inputJson = json.toString();
        logger.info(inputJson);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);

        try {
            ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, method, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info(responseEntity.getBody().toString());
                    respMap = (Map) responseEntity.getBody();
                }
            }

        } catch (ResourceAccessException | HttpClientErrorException ex) {
            logger.error("schedule service refused to connect");
            respMap.put("success", false);
            respMap.put("message", "Connection refused");
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return respMap;
    }


    /**
     * Function to create business configurations
     *
     * @param businessConfigurationsBean
     * @return
     */
    public int createBusinessConfigurations(BusinessConfigurationsBean businessConfigurationsBean, boolean fromPlan) {
        int configurationsId = 0;
        logger.info("START createBusinessConfigurations");
        try {
            int businessId = Utils.getNotNullOrDefault(businessConfigurationsBean.getBusinessId(), Constants.ZERO_INT);
            String hippaValue = businessConfigurationsBean.getHippa();

            Map businessMap = clientService.retrieveBusinessById(businessId);
            String businessUUID =  businessMap.containsKey(Constants.BUSINESS_ID) ? businessMap.get(Constants.BUSINESS_ID).toString() : Constants.EMPTY_STRING;
            String businessUniqueId = businessMap.containsKey(Constants.BUSINESS_UUID) ? businessMap.get(Constants.BUSINESS_UUID).toString() : Constants.EMPTY_STRING;
            if(Utils.isNull(hippaValue)){
                hippaValue = businessMap.get(Constants.HIPPA).toString();
                businessConfigurationsBean.setHippa(hippaValue);
            }

            configurationsId = businessConfigurationsDao.createBusinessConfigurations(businessConfigurationsBean, fromPlan);
            if (configurationsId > 0) {
                udpateBusinessConfigBasedAccess(businessId, businessConfigurationsBean, businessUUID, businessUniqueId);
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        logger.info("End createBusinessConfigurations");
        return configurationsId;
    }

    /**
     * Function to update business configurations
     * <p>
     * * @return
     */
    private void udpateBusinessConfigBasedAccess(int businessId, BusinessConfigurationsBean bean, String businessUUID, String businessUniqueId) {
        try {
            final String apiKey = Utils.isNotNull(bean.getApiKey()) ? bean.getApiKey() : Constants.ZERO_STRING;
            final String mfa = Utils.isNotNull(bean.getMfa()) ? bean.getMfa() : Constants.ZERO_STRING;
            final String notification = Utils.isNotNull(bean.getNotification()) ? bean.getNotification() : Constants.ZERO_STRING;
            final String closeLoop = Utils.isNotNull(bean.getCloseLoop()) ? bean.getCloseLoop() : Constants.EMPTY_STRING;
            final String integrations = Utils.isNotNull(bean.getIntegrations()) ? bean.getIntegrations() : Constants.EMPTY_STRING;
            final String sso = Utils.isNotNull(bean.getSso()) ? bean.getSso() : Constants.EMPTY_STRING;
            final String googleSSO = Utils.isNotNull(bean.getGoogleSSO()) ? bean.getGoogleSSO() : Constants.EMPTY_STRING;
            final String microsoftSSO = Utils.isNotNull(bean.getMicrosoftSSO()) ? bean.getMicrosoftSSO() : Constants.EMPTY_STRING;
            final String appleSSO = Utils.isNotNull(bean.getAppleSSO()) ? bean.getAppleSSO() : Constants.EMPTY_STRING;

            //To track created time of various configs
            trackConfigParameters(businessId);

            if (notification.equals(Constants.ONE_STRING)) {
                int updateNotification = businessConfigurationsDao.updateGlobalSettings(businessId, notification);
            }
            //DTV-7715
            if (Utils.notEmptyString(apiKey) && apiKey.equals(Constants.ONE_STRING)) {
                updateApiKeyByBusinessId(businessId, 1);
            }
            //CSUP-940
            if (Utils.notEmptyString(mfa) && mfa.equals(Constants.ONE_STRING) || Utils.notEmptyString(mfa) && mfa.equals("2")) {
                String urlToCallMfa = idmOKTAUrl + "okta/users/" + businessUUID + "/factors";
                restTemplateService.callInsightsApiForMfa(urlToCallMfa, HttpMethod.POST);
            }
            //DTV-12612
           // Map<String, Object> inputMap = new HashMap<>();
            AuthRoutingRequest inputMap = new AuthRoutingRequest();
            //set the idp name (by default not necessary for authType idP)
            if (sso.equals("1")) {
                //set the auth type
                /*inputMap.put("authType", "IdP");
                inputMap.put("idpName", "");*/
                inputMap.setAuthType(AuthRoutingType.IdP);
                inputMap.setIdpName("");
                //set the idp type
                if (appleSSO.equals("1")) {
                    inputMap.setIdpType(AuthIdPType.APPLE);
                } else if (googleSSO.equals("1")) {
                    inputMap.setIdpType(AuthIdPType.GOOGLE);
                } else if (microsoftSSO.equals("1")) {
                    inputMap.setIdpType(AuthIdPType.MICROSOFT);
                }
            } else {
                inputMap.setAuthType(AuthRoutingType.PASSWORD);
            }
            //call the okta api to enable sso for the business
            if (Utils.isNotNull(inputMap.getAuthType())) {
                String urlToCallSSO = idmOKTAUrl + "okta/users/" + businessUniqueId + "/auth";
                logger.info("urlToCallSSO {}", urlToCallSSO);
                restTemplateService.callSSORestApi(inputMap, urlToCallSSO, HttpMethod.POST);
            }
            logger.info("updateSSO ended");
            //end DTV-12612
            integrationAppService.createBusinessInDTWorks(integrations, closeLoop, businessUUID);
        } catch (Exception e) {
            logger.error("exception at udpateBusinessConfigBasedAccess. Msg {}", e.getMessage());
        }
    }


    /**
     * Delete business configurations by business config uuid
     *
     * @param businessConfigUUID
     * @return
     */
    public int deleteBusinessConfigurationsByConfigId(String businessConfigUUID) {
        return businessConfigurationsDao.deleteBusinessConfigurationsByConfigId(businessConfigUUID);
    }


    /**
     * Delete business configurations by business id
     *
     * @param businessId
     * @return
     */
    public int deleteBusinessConfigurationsByBusinessId(int businessId) {
        return businessConfigurationsDao.deleteBusinessConfigurationsByBusinessId(businessId);
    }

    /**
     * Return the access settings mail parameters
     *
     * @param businessUUID
     * @return
     */
    public HashMap getBusinessConfigMailParams(String businessUUID) {
        HashMap paramMap = new HashMap();
        try {
            List businessConfigurations = this.getBusinessConfigurationsByBusinessId(businessUUID);
            if (businessConfigurations.size() > 0) {
                Map businessConfigurationMap = (Map) businessConfigurations.get(0);
                paramMap.put("emailOptions", Utils.isNotNull(businessConfigurationMap.get("email")) && Integer.parseInt(businessConfigurationMap.get("email").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                paramMap.put("smsOptions", Utils.isNotNull(businessConfigurationMap.get("sms")) && Integer.parseInt(businessConfigurationMap.get("sms").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                paramMap.put("sharableLink", Utils.isNotNull(businessConfigurationMap.get("sharableLink")) && Integer.parseInt(businessConfigurationMap.get("sharableLink").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                paramMap.put("qrCodeOptions", Utils.isNotNull(businessConfigurationMap.get("qrcode")) && Integer.parseInt(businessConfigurationMap.get("qrcode").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                paramMap.put("kioskOptions", Utils.isNotNull(businessConfigurationMap.get("kiosk").toString()) && Integer.parseInt(businessConfigurationMap.get("kiosk").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                paramMap.put("whatsappOptions", Utils.isNotNull(businessConfigurationMap.get("whatsapp")) && Integer.parseInt(businessConfigurationMap.get("whatsapp").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return paramMap;
    }



    /**
     * Return the access settings mail parameters
     *
     * @param businessUUID
     * @return
     */
    public HashMap getBusinessConfigurationForPDF(String businessUUID) {
        HashMap paramMap = new HashMap();
        try {
            Map publishOptions = new LinkedHashMap();
            Map featureAccess = new LinkedHashMap();
            List businessConfigurations = this.getBusinessConfigurationsByBusinessId(businessUUID);
            Integer businessId = clientService.retrieveBusinessIDByUUID(businessUUID);
            if (businessConfigurations.size() > 0) {
                Map businessConfigurationMap = (Map) businessConfigurations.get(0);

                publishOptions.put("Email", Utils.isNotNull(businessConfigurationMap.get("email")) && Integer.parseInt(businessConfigurationMap.get("email").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                publishOptions.put("SMS", Utils.isNotNull(businessConfigurationMap.get("sms")) && Integer.parseInt(businessConfigurationMap.get("sms").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                publishOptions.put("Sharable Link", Utils.isNotNull(businessConfigurationMap.get("sharableLink")) && Integer.parseInt(businessConfigurationMap.get("sharableLink").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                publishOptions.put("QR Code", Utils.isNotNull(businessConfigurationMap.get("qrcode")) && Integer.parseInt(businessConfigurationMap.get("qrcode").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                publishOptions.put("KIOSK", Utils.isNotNull(businessConfigurationMap.get("kiosk")) && Integer.parseInt(businessConfigurationMap.get("kiosk").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                publishOptions.put("WhatsApp", Utils.isNotNull(businessConfigurationMap.get("whatsapp")) && Integer.parseInt(businessConfigurationMap.get("whatsapp").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Text Analytics", Utils.isNotNull(businessConfigurationMap.get("textAnalytics")) &&
                        Integer.parseInt(businessConfigurationMap.get("textAnalytics").toString()) == 1
                        ? Constants.ACTIVATED : Constants.DEACTIVATED);

                //DTV-9719 include txtAnalyticsConfigMap
                Map<String, Object> txtAnalyticsConfigMap = new HashMap<>();
                int textAnalyticsConfigId = (Utils.isNotNull(businessConfigurationMap.get("textAnalyticsConfigId")) &&
                        Utils.notEmptyString(businessConfigurationMap.get("textAnalyticsConfigId").toString()))
                        ? Integer.parseInt(businessConfigurationMap.get("textAnalyticsConfigId").toString()) : 0;
                if (textAnalyticsConfigId > 0) {
                    int categoryAnalysis = Utils.isNotNull(businessConfigurationMap.get("categoryAnalysis")) ?
                            Integer.parseInt(businessConfigurationMap.get("categoryAnalysis").toString()) : 0;
                    int intentAnalysis = Utils.isNotNull(businessConfigurationMap.get("intentAnalysis")) ?
                            Integer.parseInt(businessConfigurationMap.get("intentAnalysis").toString()) : 0;
                    int textAnalytics = Utils.isNotNull(businessConfigurationMap.get("textAnalytics")) ?
                            Integer.parseInt(businessConfigurationMap.get("textAnalytics").toString()) : 0;
                    txtAnalyticsConfigMap = constructTextAnalyticsConfigMap(textAnalytics,
                            intentAnalysis, categoryAnalysis, textAnalyticsConfigId);
                }
                featureAccess.put("Text Analytics Config", txtAnalyticsConfigMap);


                featureAccess.put("Logic Options", Utils.isNotNull(businessConfigurationMap.get("logic")) && Integer.parseInt(businessConfigurationMap.get("logic").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Segment Options", Utils.isNotNull(businessConfigurationMap.get("segment")) && Integer.parseInt(businessConfigurationMap.get("segment").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Advanced Scheduling", Utils.isNotNull(businessConfigurationMap.get("advancedSchedule")) && Integer.parseInt(businessConfigurationMap.get("advancedSchedule").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Multilingual Surveys", Utils.isNotNull(businessConfigurationMap.get("multiSurveys")) && Integer.parseInt(businessConfigurationMap.get("multiSurveys").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Web Dashboard", Utils.isNotNull(businessConfigurationMap.get("english")) && Integer.parseInt(businessConfigurationMap.get("english").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Mobile Dashboard", Utils.isNotNull(businessConfigurationMap.get("mobileApp")) && Integer.parseInt(businessConfigurationMap.get("mobileApp").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Arabic Dashboard", Utils.isNotNull(businessConfigurationMap.get("arabic")) && Integer.parseInt(businessConfigurationMap.get("arabic").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Notification Center", Utils.isNotNull(businessConfigurationMap.get("notification")) && Integer.parseInt(businessConfigurationMap.get("notification").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Advanced Downstream Trigger", Utils.isNotNull(businessConfigurationMap.get("trigger")) && Integer.parseInt(businessConfigurationMap.get("trigger").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Metadata", Utils.isNotNull(businessConfigurationMap.get("metadataQuestion")) && Integer.parseInt(businessConfigurationMap.get("metadataQuestion").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("URL Shortener", Utils.isNotNull(businessConfigurationMap.get("bitly")) && Integer.parseInt(businessConfigurationMap.get("bitly").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Preferred Metric", Utils.isNotNull(businessConfigurationMap.get("preferredMetric")) && Integer.parseInt(businessConfigurationMap.get("preferredMetric").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Cohort Analysis", Utils.isNotNull(businessConfigurationMap.get("cohortAnalysis")) && Integer.parseInt(businessConfigurationMap.get("cohortAnalysis").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Advanced Templates", Utils.isNotNull(businessConfigurationMap.get("advancedTemplates")) && Integer.parseInt(businessConfigurationMap.get("advancedTemplates").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Metric Cross Tabulation", Utils.isNotNull(businessConfigurationMap.get("metricCorrelation")) && Integer.parseInt(businessConfigurationMap.get("metricCorrelation").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Onboarding", Utils.isNotNull(businessConfigurationMap.get("whatfix")) && Integer.parseInt(businessConfigurationMap.get("whatfix").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("API Key", Utils.isNotNull(businessConfigurationMap.get("apiKey")) && Integer.parseInt(businessConfigurationMap.get("apiKey").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Multiple Sublinks", Utils.isNotNull(businessConfigurationMap.get("multipleSublink")) && Integer.parseInt(businessConfigurationMap.get("multipleSublink").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Advanced Analysis", Utils.isNotNull(businessConfigurationMap.get("advancedAnalysis")) && Integer.parseInt(businessConfigurationMap.get("advancedAnalysis").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Unique link for each contact", Utils.isNotNull(businessConfigurationMap.get("exportLinks")) && Integer.parseInt(businessConfigurationMap.get("exportLinks").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Global Respondents Tracker", Utils.isNotNull(businessConfigurationMap.get("respondentTracker")) && Integer.parseInt(businessConfigurationMap.get("respondentTracker").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("From Email & Name settings", Utils.isNotNull(businessConfigurationMap.get("fromCustomization")) && Integer.parseInt(businessConfigurationMap.get("fromCustomization").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Integrations", Utils.isNotNull(businessConfigurationMap.get("integrations")) && Integer.parseInt(businessConfigurationMap.get("integrations").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Dynamic QR codes", Utils.isNotNull(businessConfigurationMap.get("dynamicLinks")) && Integer.parseInt(businessConfigurationMap.get("dynamicLinks").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Multi-Factor Authentication", Utils.isNotNull(businessConfigurationMap.get("mfa")) && Integer.parseInt(businessConfigurationMap.get("mfa").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Advanced Report", Utils.isNotNull(businessConfigurationMap.get("advancedReport")) && Integer.parseInt(businessConfigurationMap.get("advancedReport").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Calculate Correlation value", Utils.isNotNull(businessConfigurationMap.get("rCoefficient")) &&
                        Integer.parseInt(businessConfigurationMap.get("rCoefficient").toString()) == 1 ?
                        Constants.ACTIVATED : Constants.DEACTIVATED);

                //adding action plans
                /**
                 * {"focusMetric": "1", "defaultRecommendation": "1", "userDefinedRecommendation": "1"}
                 */
                Map actionPlans = null;
                try {
                    actionPlans = mapper.readValue(businessConfigurationMap.getOrDefault("focusMetric", "").toString(), HashMap.class);
                } catch (Exception e) {
                    logger.error("Error while parsing focusMetric value {}", e.getMessage());
                }
                if (Utils.isNotNull(actionPlans)) {
                    int focusMetric = Utils.isNotNull(actionPlans.get("focusMetric")) ? Integer.parseInt(actionPlans.get("focusMetric").toString()) : 0;
                    featureAccess.put("Action Plans", focusMetric == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                    if (focusMetric == 1) {
                        /*featureAccess.put("System Defined Recommendations",
                                Utils.isNotNull(actionPlans.get("defaultRecommendation")) &&
                                    Integer.parseInt(actionPlans.getOrDefault("defaultRecommendation","0").toString()) > 0
                                    ? Constants.ACTIVATED : Constants.DEACTIVATED);
                        featureAccess.put("User Defined Recommendations",
                                Utils.isNotNull(actionPlans.get("userDefinedRecommendation")) &&
                                    Integer.parseInt(actionPlans.getOrDefault("userDefinedRecommendation","0").toString()) > 0
                                    ? Constants.ACTIVATED : Constants.DEACTIVATED);*/
                        featureAccess.put("System Defined Recommendations",
                                Utils.isNotNull(actionPlans.get("defaultRecommendation")) &&
                                        Integer.parseInt(actionPlans.getOrDefault("defaultRecommendation", "0").toString()) > 0
                                        ? Integer.parseInt(actionPlans.getOrDefault("defaultRecommendation", "0").toString()) : 0);
                        featureAccess.put("User Defined Recommendations",
                                Utils.isNotNull(actionPlans.get("userDefinedRecommendation")) &&
                                        Integer.parseInt(actionPlans.getOrDefault("userDefinedRecommendation", "0").toString()) > 0
                                        ? Integer.parseInt(actionPlans.getOrDefault("userDefinedRecommendation", "0").toString()) : 0);
                    }
                }

                Map uniqueLinks = null;
                try {
                    uniqueLinks = mapper.readValue(businessConfigurationMap.getOrDefault("uniqueLinks", "").toString(), HashMap.class);
                } catch (Exception e) {
                    logger.error("Error while parsing uniqueLinks value {}", e.getMessage());
                }
                if (Utils.isNotNull(uniqueLinks)) {
                    int uniqueLinkEmail = Utils.isNotNull(uniqueLinks.get("uniqueLinkEmail")) ? Integer.parseInt(uniqueLinks.get("uniqueLinkEmail").toString()) : 0;
                    featureAccess.put("Unique Link Email", uniqueLinkEmail == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                    int uniqueLinkSms = Utils.isNotNull(uniqueLinks.get("uniqueLinkSms")) ? Integer.parseInt(uniqueLinks.get("uniqueLinkSms").toString()) : 0;
                    featureAccess.put("Unique Link Sms", uniqueLinkSms == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                    int uniqueLinkWhatsapp = Utils.isNotNull(uniqueLinks.get("uniqueLinkWhatsapp")) ? Integer.parseInt(uniqueLinks.get("uniqueLinkWhatsapp").toString()) : 0;
                    featureAccess.put("Unique Link Whatsapp", uniqueLinkWhatsapp == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                }

                Map sso = null;
                try {
                    sso = mapper.readValue(businessConfigurationMap.getOrDefault("sso", "").toString(), HashMap.class);
                } catch (Exception e) {
                    logger.error("Error while parsing uniqueLinks value {}", e.getMessage());
                }
                if (Utils.isNotNull(sso)) {
                    int googleSSO = Utils.isNotNull(sso.get("googleSSO")) ? Integer.parseInt(sso.get("googleSSO").toString()) : 0;
                    featureAccess.put("google SSO", googleSSO == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                    int microsoftSSO = Utils.isNotNull(sso.get("microsoftSSO")) ? Integer.parseInt(sso.get("microsoftSSO").toString()) : 0;
                    featureAccess.put("microsoft SSO", microsoftSSO == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                    int appleSSO = Utils.isNotNull(sso.get("appleSSO")) ? Integer.parseInt(sso.get("appleSSO").toString()) : 0;
                    featureAccess.put("apple SSO", appleSSO == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                }

                Map programFeatures = null;
                try {
                    programFeatures = mapper.readValue(businessConfigurationMap.getOrDefault("programFeatures", "").toString(), HashMap.class);
                } catch (Exception e) {
                    logger.error("Error while parsing programFeatures value {}", e.getMessage());
                }
                if (Utils.isNotNull(programFeatures)) {
                    int programOverview = Utils.isNotNull(programFeatures.get("programOverview")) ? Integer.parseInt(programFeatures.get("programOverview").toString()) : 0;
                    featureAccess.put("Program Overview", programOverview == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                    int programMetric = Utils.isNotNull(programFeatures.get("programMetric")) ? Integer.parseInt(programFeatures.get("programMetric").toString()) : 0;
                    featureAccess.put("Program Metric", programMetric == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                }

                featureAccess.put("Predefined Question Library", Utils.isNotNull(businessConfigurationMap.get("predefinedQuestionLibrary")) && Integer.parseInt(businessConfigurationMap.get("predefinedQuestionLibrary").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Previously Used Question Library", Utils.isNotNull(businessConfigurationMap.get("previouslyUsedQuestionLibrary")) && Integer.parseInt(businessConfigurationMap.get("previouslyUsedQuestionLibrary").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                //TODO to uncomment below line after UI work
                //featureAccess.put("Custom EUX theme", Utils.isNotNull(businessConfigurationMap.get("customThemes")) && Integer.parseInt(businessConfigurationMap.get("customThemes").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                //chatbot, audit_program, collaboration, custom_dashboard
                featureAccess.put("Custom Report", Utils.isNotNull(businessConfigurationMap.get("customDashboard")) && Integer.parseInt(businessConfigurationMap.get("customDashboard").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Audit Programs", Utils.isNotNull(businessConfigurationMap.get("auditProgram")) && Integer.parseInt(businessConfigurationMap.get("auditProgram").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Collaboration", Utils.isNotNull(businessConfigurationMap.get("collaboration")) && Integer.parseInt(businessConfigurationMap.get("collaboration").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Chatbot", Utils.isNotNull(businessConfigurationMap.get("chatbot")) && Integer.parseInt(businessConfigurationMap.get("chatbot").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Custom EUX Theme", Utils.isNotNull(businessConfigurationMap.get("customThemes")) && Integer.parseInt(businessConfigurationMap.get("customThemes").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("DT works - Case management", Utils.isNotNull(businessConfigurationMap.get("closeLoop")) &&
                        Integer.parseInt(businessConfigurationMap.get("closeLoop").toString()) == 1 ?
                        Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Program Throttling", Utils.isNotNull(businessConfigurationMap.get("programThrottling")) &&
                        Integer.parseInt(businessConfigurationMap.get("programThrottling").toString()) == 1 ?
                        Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Gameplans", Utils.isNotNull(businessConfigurationMap.get("gamePlan")) && Integer.parseInt(businessConfigurationMap.get("gamePlan").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Notes", Utils.isNotNull(businessConfigurationMap.get("notes")) && Integer.parseInt(businessConfigurationMap.get("notes").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Spam avoidance", Utils.isNotNull(businessConfigurationMap.get("submissionDelay")) && Integer.parseInt(businessConfigurationMap.get("submissionDelay").toString()) == 1 ? Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Task Manager", Utils.isNotNull(businessConfigurationMap.get("taskManager")) &&
                        Integer.parseInt(businessConfigurationMap.get("taskManager").toString()) == 1 ?
                        Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("AI Surveys", Utils.isNotNull(businessConfigurationMap.get("aiSurveys")) &&
                        Integer.parseInt(businessConfigurationMap.get("aiSurveys").toString()) == 1 ?
                        Constants.ACTIVATED : Constants.DEACTIVATED);
                //DTV-12386, 13080
                featureAccess.put("Response Quota", Utils.isNotNull(businessConfigurationMap.get("responseQuota")) &&
                        Integer.parseInt(businessConfigurationMap.get("responseQuota").toString()) == 1 ?
                        Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Recurrence", Utils.isNotNull(businessConfigurationMap.get("recurrence")) &&
                        Integer.parseInt(businessConfigurationMap.get("recurrence").toString()) == 1 ?
                        Constants.ACTIVATED : Constants.DEACTIVATED);
                featureAccess.put("Custom Dashboard Builder", Utils.isNotNull(businessConfigurationMap.get("customDashboardBuilder")) &&
                        Integer.parseInt(businessConfigurationMap.get("customDashboardBuilder").toString()) == 1 ?
                        Constants.ACTIVATED : Constants.DEACTIVATED);
                paramMap.put("publishOptions", publishOptions);
                paramMap.put("featureAccess", featureAccess);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return paramMap;
    }


    /**
     * update business_email_alert in global settings table
     *
     * @param businessUUID
     * @param oldLimit
     * @param newLimit
     * @return
     */
    public void reprocessNewResponses(String businessUUID, int oldLimit, int newLimit, String source) {
        int result = 0;
        logger.info("Start reprocess excess feedback");
        Map inputMap = new HashMap();
        try {
            if (Utils.notEmptyString(businessUUID)) {
                Map reprocessMap = new HashMap();

                //If newlimit is -1 (max), need to process all the responses in message_queue for the given businessUUID if present.
                if (newLimit == -1) {
                    reprocessMap.put("businessUUID", businessUUID);
                    reprocessMap.put("type", source);
                } else {
                    //calculate new feedbacks count to be processed.
                    if (newLimit > oldLimit) {
                        int newFeedbackCount = oldLimit == -1 ? newLimit : newLimit - oldLimit;
                        reprocessMap.put("businessUUID", businessUUID);
                        reprocessMap.put("limit", newFeedbackCount);
                        reprocessMap.put("type", source);
                    }
                }

                if (reprocessMap.size() > 0) {
                    inputMap.put("business", reprocessMap);

                    // uri = https://${dt.event.url}/api/v1/event/reprocess
                    final String uri = dtEventUrl + "reprocess";
                    logger.info("uri {}", uri);

                    Map resultMap = callRestApi(inputMap, uri, HttpMethod.POST);
                    logger.info("End reprocess excess feedback ");
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

    }

    /**
     * @param data
     * @param businessUUID
     * @return
     */
    public Map upsertTextAnalytics(String data, String businessUUID) {
        Map resultMap = new HashMap();
        try {
            String urlToCall = dtUpsertUrl + businessUUID;
            logger.info("urltocall {}", urlToCall);
            logger.info("data {}", data);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> entity = new HttpEntity<String>(data, headers);

            ResponseEntity<Object> responseEntity = restTemplate.postForEntity(urlToCall, entity, Object.class);

            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info("upsertTextAnalytics request sent successfully");
                    logger.info(responseEntity.getBody().toString());
                    resultMap = (Map) responseEntity.getBody();
                }
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return resultMap;
    }

    /**
     * retrieve text analytics mapping for the client
     *
     * @param businessUUID
     * @return
     */
    public Map retriveTextAnalyticsMapping(String businessUUID) {
        Map responseMap = new HashMap();
        logger.info("begin retrieve txt mapping");
        try {

            String urlToCall = dtUpsertUrl + businessUUID;
            logger.info("urltocall {}", urlToCall);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> entity = new HttpEntity<String>(headers);

            ResponseEntity<Object> responseEntity = restTemplate.exchange(urlToCall, HttpMethod.GET, entity, Object.class);

            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info(" request sent successfully");
                    logger.info(responseEntity.getBody().toString());
                    responseMap = (Map) responseEntity.getBody();
                }
            }

            //DTV-4635 adding pending approval status in text analytics config.
            if (responseMap.size() > 0) {
                int businessId = clientService.retrieveBusinessIDByUUID(businessUUID);
                TextAnalyticsConfigBean textAnalyticsConfigBean = textAnalyticsConfigurationService.getLatestTextAnalyticsConfigByBusinessId(businessId);
                String approvalState = (Utils.isNotNull(textAnalyticsConfigBean)) ? textAnalyticsConfigBean.getApprovalStatus() : "";
                if (approvalState.equalsIgnoreCase("pending")) {
                    Map pendingConfig = Utils.isNotNull(textAnalyticsConfigBean.getConfigs()) && Utils.notEmptyString(textAnalyticsConfigBean.getConfigs()) ? mapper.readValue(textAnalyticsConfigBean.getConfigs(), HashMap.class) : new HashMap();
                    responseMap.put("pendingApproval", pendingConfig);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        logger.info("end retrieve txt mapping");
        return responseMap;
    }

    /**
     * update apiKey by businessId in api_key table.
     *
     * @param businessId
     * @param enabled
     * @return
     */
    public void updateApiKeyByBusinessId(int businessId, int enabled) {
        int result = 0;
        logger.info("Start updating apiKey by businessId in api_key table");
        Map inputMap = new HashMap();
        try {
            if (businessId > 0) {

                Map apiKeyMap = this.getApiKeyByBusinessId(businessId);
                //check entry for given business in api_key table.
                boolean isApiKeyPresent = apiKeyMap.containsKey("business_id") && Utils.isNotNull(apiKeyMap.get("business_id"));

                //apiKey is enabled --> create new apiKey if no entry is present in api_key table for businessId
                //                  --> update the enabled status to 1 if entry is present in api_key table for businessId

                //already apikey exists for given business.
                if (isApiKeyPresent) {
                    String updateSql = "update api_key set enabled = ? where business_id = ?";
                    int val = (enabled == 1) ? 1 : 0;
                    jdbcTemplate.update(updateSql, new Object[]{val, businessId});
                }
                //no apiKey is present in api_key table,
                //if enabled == 1 --> create new apiKey by businessId
                else if (enabled == 1) {
                    String apiKey = "DT." + tokenUtility.generatealphaNumericRandomString(40);

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    String createdTime = simpleDateFormat.format(timestamp);

                    Map businessMap = clientService.getBusinessContractDates(businessId);
                    String contractStartDate = businessMap.containsKey("contract_start_date") && businessMap.get("contract_start_date") != null ? businessMap.get("contract_start_date").toString() : "";
                    String expirationTime = businessMap.containsKey("contract_end_date") && Utils.isNotNull(businessMap.get("contract_end_date")) ? Utils.convertToEndOfDay(businessMap.get("contract_end_date").toString()) : "";


                    /**
                     * Token expiration time will be business contract_end_date
                     * If contract_end_date is not present  --> adding 1 year to contract start date.
                     */
                    if (Utils.emptyString(expirationTime)) {
                        Calendar cal = Calendar.getInstance();
                        Date startDate = simpleDateFormat.parse(contractStartDate);
                        cal.setTime(startDate);
                        cal.add(Calendar.YEAR, 1);
                        expirationTime = simpleDateFormat.format(cal.getTime());
                    }

                    if (Utils.notEmptyString(expirationTime) && expirationTime.length() > 19) {
                        expirationTime = expirationTime.substring(0, 19);
                    }

                    String insertSql = "insert into api_key (business_id, api_key, created_time, expiration_time, enabled) values (?, ?, ?, ?, ?)";
                    jdbcTemplate.update(insertSql, new Object[]{businessId, apiKey, createdTime, expirationTime, enabled});
                }
            }
            logger.info("End updating apiKey by businessId in api_key table");
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * Get apiKey entry by businessId in api_key table.
     *
     * @param businessId
     * @return
     */
    public Map getApiKeyByBusinessId(int businessId) {
        logger.info("start get apiKey by businessId in api_key table");
        Map resultMap = new HashMap();
        try {
            if (businessId > 0) {
                String sql = "select id, business_id, api_key, created_time, expiration_time, enabled from api_key where business_id = ? and current_timestamp < expiration_time";
                List result = jdbcTemplate.queryForList(sql, new Object[]{businessId});
                if (result.size() > 0) {
                    resultMap = (Map) result.get(0);
                }
            }
            logger.info("end get apiKey by businessId in api_key table");
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return resultMap;
    }


    /**
     * deactivate api key when contract is expired
     *
     * @param businessId
     */
    public void deactivateApiKeyBusinessId(int businessId) {
        logger.info("begin deactivate api key setting after contract expiration");
        try {
            String sql = "update business_configurations set api_key = 0 where business_id = ?";
            int updateResult = jdbcTemplate.update(sql);
            logger.info("end deactivate api key setting after contract expiration");
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }


    /**
     * remove all the advanced template mapping for a business
     *
     * @param businessId
     */
    public void removeBusinessFromTemplates(int businessId) {
        logger.info("begin removeBusinessFromTemplates");
        try {
            String sql = "UPDATE templates SET business_ids = JSON_REMOVE(business_ids, JSON_UNQUOTE(JSON_SEARCH(business_ids, 'one', '" + businessId + "')))" +
                    "WHERE  json_contains(business_ids, '\"" + businessId + "\"');";
            int updateResult = jdbcTemplate.update(sql);
            logger.info("end removeBusinessFromTemplates");
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * @param textAnalyticsConfigBean
     * @return
     */
    public int updateLatestBusinessConfigurationWithAdvancedTextAnalyticsValue(TextAnalyticsConfigBean textAnalyticsConfigBean) {

        int status = 1;
        try {
            String textAnalyticsPlanId = (textAnalyticsConfigBean.getTextAnalyticsPlan().equals("basic")) ? "0" : "1";
            String businessUUID = textAnalyticsConfigBean.getBuinessUniqueId();
            int businessId = clientService.getBusinessIdByUUID(businessUUID);
            String updateBusinessConfigSql = "update business_configurations set advanced_text_analytics = ? where business_id = ?";
            int updateResult = jdbcTemplate.update(updateBusinessConfigSql, new Object[]{textAnalyticsPlanId, businessId});
            logger.info("update business config advanced text analytics result {}", updateResult);
            String maxIdQuery = "select max(business_config_version_id) from business_configurations_version where business_id = ?";
            Object maxIdObject = jdbcTemplate.queryForObject(maxIdQuery, Object.class, new Object[]{businessId});
            int maxId = (Utils.isNotNull(maxIdObject) && Utils.isStringNumber(maxIdObject.toString())) ? Integer.parseInt(maxIdObject.toString()) : 0;
            String updateConfigVersionsSQL = "update business_configurations_version set advanced_text_analytics = ? where business_config_version_id = ?";
            int updateConfigResult = jdbcTemplate.update(updateConfigVersionsSQL, new Object[]{textAnalyticsPlanId, maxId});
            logger.info("update business config version advanced text analytics result {}", updateConfigResult);
        } catch (Exception e) {
            e.printStackTrace();
            status = 0;
        }
        return status;
    }


    /**
     * DTV-6355
     * Calling Text Analytics Eliza settings API when TA value is modified from portal (business configurations).
     *
     * @param businessId
     * @param enabled
     * @return
     */
    public void updateElizaTextAnalyticsStatusByBusinessId(String businessUUID, int businessId, int enabled) {
        int result = 0;
        logger.info("Start updating Text Analytics setting enabled status by businessId");
        Map inputMap = new HashMap();
        try {
            if (businessId > 0) {
                //Fetching the latest used TA config in previous_ta_config table
                TextAnalyticsConfigBean prevTextAnalyticsConfigBean = textAnalyticsConfigurationService.getLatestPreviousTextAnalyticsConfigByBusinessId(businessId);

                /**
                 * Case 1 : Text Analytics is turned off from portal
                 * When TA is turned off, calling eliza settings API with enabled = false;
                 * Storing the last used TA config in previous_ta_config table to update eliza settings API when TA is turned on
                 */
                if (enabled == 0 && businessId > 0) {
                    Map configMap = new HashMap();
                    configMap.put("enabled", false);
                    //calling Eliza settings API
                    this.upsertTextAnalytics(new JSONObject(configMap).toString(), businessUUID);

                    //Storing the last used TA config in previous_ta_config table to update eliza settings API when TA is turned on
                    TextAnalyticsConfigBean textAnalyticsConfigBean = textAnalyticsConfigurationService.getLatestTextAnalyticsConfigByBusinessId(businessId);

                    if (Utils.isNotNull(textAnalyticsConfigBean)) {

                        //For first time prevTextAnalyticsConfigBean will be null
                        if (prevTextAnalyticsConfigBean == null) {
                            prevTextAnalyticsConfigBean = new TextAnalyticsConfigBean();
                            prevTextAnalyticsConfigBean.setConfigId(-1);
                        }
                        //If prev TA config & new TA config is different then update with new TA Config
                        if (prevTextAnalyticsConfigBean.getConfigId() != textAnalyticsConfigBean.getConfigId()) {

                            String deleteSql = "delete from previous_ta_config where business_id = ?";
                            jdbcTemplate.update(deleteSql, new Object[]{businessId});

                            String configs = Utils.isNotNull(textAnalyticsConfigBean.getConfigs()) && Utils.notEmptyString(textAnalyticsConfigBean.getConfigs()) ? new JSONObject(textAnalyticsConfigBean.getConfigs()).toString() : "";
                            if (configs.length() > 0) {
                                String insertSql = "insert into previous_ta_config (config_id, configs, business_id, created_time) values (?, ?, ?, ?)";
                                jdbcTemplate.update(insertSql, new Object[]{textAnalyticsConfigBean.getConfigId(), configs, businessId, new Timestamp(System.currentTimeMillis())});
                            }
                        }
                    }
                }
                /**
                 * Case 1 : Text Analytics is turned on from portal
                 * When TA is turned on, calling eliza settings API with enabled = true;
                 * Storing the last used TA config in previous_ta_config table to update eliza settings API when TA is turned on
                 *
                 * "dataDurationId", "textAnalyticsPlanId", enabled is required when TA is turned on
                 *
                 */
                else if (enabled == 1 && businessId > 0 && Utils.isNotNull(prevTextAnalyticsConfigBean)) {
                    Map configMap = Utils.isNotNull(prevTextAnalyticsConfigBean.getConfigs()) && Utils.notEmptyString(prevTextAnalyticsConfigBean.getConfigs()) ? mapper.readValue(prevTextAnalyticsConfigBean.getConfigs(), HashMap.class) : new HashMap();
                    if (configMap.containsKey("dataDurationId") && configMap.containsKey("textAnalyticsPlanId")) {
                        configMap.put("enabled", true);
                        //calling Eliza settings API
                        this.upsertTextAnalytics(new JSONObject(configMap).toString(), businessUUID);
                    }
                }
            }

            logger.info("End updating Text Analytics setting enabled status by businessId");
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

    }

    /**
     * track the business config parameters
     *
     * @param businessId
     */
    public void trackConfigParameters(int businessId) {
        logger.info("begin track config parameters");
        try {
            List businessConfigurations = jdbcTemplate.queryForList(Constants.GET_BUSINESS_CONFIG_BY_BUSINESS_ID, businessId);
            int configSize = businessConfigurations.size();
            int noOfKiosk = 0;

            if (configSize > 0) {
                Map configMap = (Map) businessConfigurations.get(configSize - 1);
                noOfKiosk = configMap.containsKey("no_of_kiosks") ? (int) configMap.get("no_of_kiosks") : 0;
                String parameter = "noOfKiosk";
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());

                //track the no. of kiosk
                if (noOfKiosk > 0) {
                    String uniqueId = tokenUtility.generateUUID().toString();
                    KeyHolder keyHolder = new GeneratedKeyHolder();
                    jdbcTemplate.update(new PreparedStatementCreator() {
                        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                            PreparedStatement ps = connection.prepareStatement(Constants.CREATE_TRACKER_CONFIG, Statement.RETURN_GENERATED_KEYS);
                            ps.setInt(1, businessId);
                            ps.setString(2, parameter);
                            ps.setTimestamp(3, timestamp);
                            ps.setString(4, uniqueId);
                            return ps;
                        }
                    }, keyHolder);
                    int trackerId = keyHolder.getKey().intValue();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("end track config parameters");
    }


    public void updateDeactivatedKiosks(String businessUUID, int existNumberKiosk, int updatedNumberKiosk) {
        try {
            int differenceNumber = Math.abs(existNumberKiosk - updatedNumberKiosk);
            int business_id = clientService.getBusinessIdByUUID(businessUUID);
            String tableName = "deactivate_kiosks";
            Timestamp currentTimeToInsert = new Timestamp(System.currentTimeMillis());
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(insertDeactivatedKiosk, Statement.RETURN_GENERATED_KEYS);
                    ps.setInt(1, business_id);
                    ps.setInt(2, updatedNumberKiosk);
                    ps.setInt(3, differenceNumber);
                    ps.setTimestamp(4, currentTimeToInsert);
                    return ps;
                }
            }, keyHolder);
            logger.info("insert into deactivate kiosk done");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * @param businessId
     * @param trackParam
     * @return
     */
    public String getKioskLatestConfigDate(int businessId, String trackParam) {
        return businessConfigurationsDao.getKioskLatestConfigDate(businessId, trackParam);
    }


    /**
     * function to update business configuration by business id
     *
     * @param businessConfigurationsBean
     * @return
     */
    public int updateBusinessConfigurationsV1(BusinessConfigurationsBean businessConfigurationsBean,
                                              String businessUUID, Map oldBusinessConfigurationMap) {
        int updateStatus = 0;
        boolean publishEliza = false;
        logger.info("updateBusinessConfigurationsV1 - start");
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            ArrayList<Object> objectArrayList = new ArrayList<Object>();
            BusinessIntegratedBean businessIntegratedBean = new BusinessIntegratedBean();

            Map channelValueMap = setDefaultChannelValues();
            Map usageLimitMap = new HashMap();

            StringBuilder sqlBuilder = new StringBuilder("update business_configurations set ");

            if (businessConfigurationsBean.getEmail() != null) {
                logger.info("business email config is present");
                objectArrayList.add(businessConfigurationsBean.getEmail());
                if (Integer.parseInt(businessConfigurationsBean.getEmail()) == 1) {
                    channelValueMap.put(Constants.EMAIL, true);
                }
                sqlBuilder.append("email = ?, ");
            }

            if (businessConfigurationsBean.getSms() != null) {
                logger.info("business sms config is present");
                objectArrayList.add(businessConfigurationsBean.getSms());
                if (Integer.parseInt(businessConfigurationsBean.getSms()) == 1) {
                    channelValueMap.put(Constants.SMS, true);
                }
                sqlBuilder.append("sms = ?, ");
            }
            if (businessConfigurationsBean.getTaskManager() != null) {
                logger.info("business taskManager config is present");
                objectArrayList.add(businessConfigurationsBean.getTaskManager());
                sqlBuilder.append("task_manager = ?, ");
            }

            if (businessConfigurationsBean.getWhatsapp() != null) {
                logger.info("business whatsapp config is present");
                objectArrayList.add(businessConfigurationsBean.getWhatsapp());
                if (Integer.parseInt(businessConfigurationsBean.getWhatsapp()) == 1) {
                    channelValueMap.put(Constants.WHATSAPP, true);
                }
                sqlBuilder.append("whatsapp = ?, ");
            }

            if (businessConfigurationsBean.getQrcode() != null) {
                logger.info("business qr config is present");
                objectArrayList.add(businessConfigurationsBean.getQrcode());
                if (Integer.parseInt(businessConfigurationsBean.getQrcode()) == 1) {
                    channelValueMap.put(Constants.QRCODE, true);
                }
                sqlBuilder.append("qrcode = ?, ");
            }

            if (businessConfigurationsBean.getShareableLink() != null) {
                logger.info("business shareable link config is present");
                objectArrayList.add(businessConfigurationsBean.getShareableLink());
                if (Integer.parseInt(businessConfigurationsBean.getShareableLink()) == 1) {
                    channelValueMap.put(Constants.LINK, true);
                }
                sqlBuilder.append("shareable_link = ?, ");
            }

            if (businessConfigurationsBean.getTextAnalytics() != null) {
                logger.info("business text analytics config is present");
                objectArrayList.add(businessConfigurationsBean.getTextAnalytics());
                sqlBuilder.append("text_analytics = ?, ");
                publishEliza = (Integer.parseInt(businessConfigurationsBean.getTextAnalytics().toString()) == 1) ? true : false;
                if (publishEliza)
                    businessIntegratedBean.setPublishToEliza(true);
                businessIntegratedBean.setTAChanged(true);
            }

            if (businessConfigurationsBean.getLogic() != null) {
                logger.info("business logic config is present");
                objectArrayList.add(businessConfigurationsBean.getLogic());
                sqlBuilder.append("logic = ?, ");
            }

            if (businessConfigurationsBean.getRespondent() != null) {
                logger.info("business respondent config is present");
                objectArrayList.add(businessConfigurationsBean.getRespondent());
                sqlBuilder.append("respondent = ?, ");
            }

            if (businessConfigurationsBean.getSegment() != null) {
                logger.info("business segment config is present");
                objectArrayList.add(businessConfigurationsBean.getSegment());
                sqlBuilder.append("segment = ?, ");
            }

            if (businessConfigurationsBean.getKiosk() != null) {
                logger.info("business kiosk config is present");
                objectArrayList.add(businessConfigurationsBean.getKiosk());
                if (Integer.parseInt(businessConfigurationsBean.getKiosk()) == 1) {
                    channelValueMap.put(Constants.KIOSK, true);
                }
                sqlBuilder.append("kiosk = ?, ");
            }

            if (businessConfigurationsBean.getMetadataQuestion() != null) {
                logger.info("business settings metadataQuestion is present");
                objectArrayList.add(businessConfigurationsBean.getMetadataQuestion());
                sqlBuilder.append("metadata_question = ?, ");
            }

            if (businessConfigurationsBean.getMobileApp() != null) {
                logger.info("business settings mobile app is present");
                objectArrayList.add(businessConfigurationsBean.getMobileApp());
                sqlBuilder.append("mobile_app = ?, ");
            }

            if (businessConfigurationsBean.getEmotionAnalysis() != null) {
                logger.info("business settings emotion analysis is present");
                objectArrayList.add(businessConfigurationsBean.getEmotionAnalysis());
                sqlBuilder.append("emotion_analysis = ?, ");
            }

            if (businessConfigurationsBean.getIntentAnalysis() != null) {
                logger.info("business settings intent analysis is present");
                objectArrayList.add(businessConfigurationsBean.getIntentAnalysis());
                sqlBuilder.append("intent_analysis = ?, ");
            }

            if (businessConfigurationsBean.getTrigger() != null) {
                logger.info("business settings trigger config is present");
                objectArrayList.add(businessConfigurationsBean.getTrigger());
                sqlBuilder.append("trigger_toggle = ?, ");
            }

            if (businessConfigurationsBean.getAdvancedSchedule() != null) {
                logger.info("business settings advanced schedule is present");
                objectArrayList.add(businessConfigurationsBean.getAdvancedSchedule());
                sqlBuilder.append("advanced_schedule = ?, ");
            }

            if (businessConfigurationsBean.getEnglish() != null) {
                logger.info("business settings english is present");
                objectArrayList.add(businessConfigurationsBean.getEnglish());
                sqlBuilder.append("english_toggle = ?, ");
            }

            if (businessConfigurationsBean.getArabic() != null) {
                logger.info("business settings arabic is present");
                objectArrayList.add(businessConfigurationsBean.getArabic());
                sqlBuilder.append("arabic_toggle = ?, ");
            }

            if (businessConfigurationsBean.getMultiSurveys() != null) {
                logger.info("business settings multi surveys is present");
                objectArrayList.add(businessConfigurationsBean.getMultiSurveys());
                sqlBuilder.append("multi_surveys = ?, ");
            }

            if (businessConfigurationsBean.getBitly() != null) {
                logger.info("business settings bitly is present");
                objectArrayList.add(businessConfigurationsBean.getBitly());
                sqlBuilder.append("bitly = ?, ");
            }

            if (businessConfigurationsBean.getWebHooks() != null) {
                logger.info("business settings web hooks is present");
                objectArrayList.add(businessConfigurationsBean.getWebHooks());
                sqlBuilder.append("web_hooks = ?, ");
            }

            if (businessConfigurationsBean.getPreferredMetric() != null) {
                logger.info("business settings preferred metric is present");
                objectArrayList.add(businessConfigurationsBean.getPreferredMetric());
                sqlBuilder.append("preferred_metric = ?, ");
            }

            if (businessConfigurationsBean.getCategoryAnalysis() != null) {
                logger.info("business settings category analysis is present");
                objectArrayList.add(businessConfigurationsBean.getCategoryAnalysis());
                sqlBuilder.append("category_analysis = ?, ");
            }

            if (businessConfigurationsBean.getAdvancedTextAnalytics() != null) {
                logger.info("business settings advanced text analytics is present");
                objectArrayList.add(businessConfigurationsBean.getAdvancedTextAnalytics());
                sqlBuilder.append("advanced_text_analytics = ?, ");
            }

            if (businessConfigurationsBean.getNoOfUsers() != null) {
                logger.info("business settings no of users is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfUsers());
                sqlBuilder.append("no_of_users = ?, ");
                usageLimitMap.put("users", businessConfigurationsBean.getNoOfUsers());
            }

            if (businessConfigurationsBean.getNoOfActivePrograms() != null) {
                logger.info("business settings no of active programs is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfActivePrograms());
                sqlBuilder.append("no_of_active_programs = ?, ");
                usageLimitMap.put("programs", businessConfigurationsBean.getNoOfActivePrograms());
            }

            if (businessConfigurationsBean.getNoOfResponses() != null) {
                logger.info("business settings no of responses is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfResponses());

                sqlBuilder.append("no_of_responses = ?, ");
                usageLimitMap.put("responses", businessConfigurationsBean.getNoOfResponses());
                businessIntegratedBean.setResponseChanged(true);
            }

            if (businessConfigurationsBean.getNoOfMetrics() != null) {
                logger.info("business settings no of metrics is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfMetrics());
                sqlBuilder.append("no_of_metrics = ?, ");
                usageLimitMap.put("metrics", businessConfigurationsBean.getNoOfMetrics());
            }

            if (businessConfigurationsBean.getNoOfEmailsSent() != null) {
                logger.info("business settings no of emails sent is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfEmailsSent());
                sqlBuilder.append("no_of_emails_sent = ?, ");
                usageLimitMap.put("emails", businessConfigurationsBean.getNoOfEmailsSent());
            }

            if (businessConfigurationsBean.getNoOfSms() != null) {
                logger.info("business settings no of sms sent is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfSms());
                sqlBuilder.append("no_of_sms = ?, ");
                usageLimitMap.put("sms", businessConfigurationsBean.getNoOfSms());
            }

            if (businessConfigurationsBean.getNoOfWhatsapp() != null) {
                logger.info("business settings no of whatsapp sent is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfWhatsapp());
                sqlBuilder.append("no_of_whatsapp = ?, ");
                usageLimitMap.put("whatsapp", businessConfigurationsBean.getNoOfWhatsapp());
            }

            if (businessConfigurationsBean.getRCoefficient() != null) {
                logger.info("business settings r coefficient is present");
                objectArrayList.add(businessConfigurationsBean.getRCoefficient());
                sqlBuilder.append("r_coefficient = ?, ");
            }

            if (businessConfigurationsBean.getNoOfApi() != null) {
                logger.info("business settings no of api sent is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfApi());
                sqlBuilder.append("no_of_api = ?, ");
                usageLimitMap.put("api", businessConfigurationsBean.getNoOfApi());
            }

            if (businessConfigurationsBean.getNoOfFilters() != null) {
                logger.info("business settings no of filters sent is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfFilters());
                sqlBuilder.append("no_of_filters = ?, ");
                usageLimitMap.put("filters", businessConfigurationsBean.getNoOfFilters());
            }

            if (businessConfigurationsBean.getNoOfTriggers() != null) {
                logger.info("business settings no of triggers sent is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfTriggers());
                sqlBuilder.append("no_of_triggers = ?, ");
                usageLimitMap.put("triggers", businessConfigurationsBean.getNoOfTriggers());
            }

            if (businessConfigurationsBean.getDataRetention() != null) {
                logger.info("business settings data retention sent is present");
                objectArrayList.add(businessConfigurationsBean.getDataRetention());
                sqlBuilder.append("data_retention = ?, ");
            }

            if (businessConfigurationsBean.getNoOfLists() != null) {
                logger.info("business settings no of lists sent is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfLists());
                sqlBuilder.append("no_of_lists = ?, ");
                usageLimitMap.put("lists", businessConfigurationsBean.getNoOfLists());
            }

            if (businessConfigurationsBean.getNoOfRecipients() != null) {
                logger.info("business settings no of recipients sent is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfRecipients());
                sqlBuilder.append("no_of_recipients = ?, ");
                usageLimitMap.put("recipients", businessConfigurationsBean.getNoOfRecipients());
                businessIntegratedBean.setRecipientsChanged(true);
            }

            if (businessConfigurationsBean.getModifiedBy() != null) {
                logger.info("user id is present");
                objectArrayList.add(businessConfigurationsBean.getModifiedBy());
                sqlBuilder.append("modified_by = ?, ");
            }

            if (businessConfigurationsBean.getHippa() != null) {
                logger.info("business hippa config is present");
                objectArrayList.add(businessConfigurationsBean.getHippa());
                sqlBuilder.append("hippa = ?, ");
            }

            if (businessConfigurationsBean.getCohortAnalysis() != null) {
                logger.info("business cohort analysis config is present");
                objectArrayList.add(businessConfigurationsBean.getCohortAnalysis());
                sqlBuilder.append("cohort_analysis = ?, ");
            }

            if (businessConfigurationsBean.getAdvancedTemplates() != null) {
                logger.info("business advanced templates config is present");
                objectArrayList.add(businessConfigurationsBean.getAdvancedTemplates());
                sqlBuilder.append("advanced_templates = ?, ");
                if (businessConfigurationsBean.getAdvancedTemplates().equals("0")) {
                    businessIntegratedBean.setTemplateChanged(true);
                }
            }

            if (businessConfigurationsBean.getMetricCorrelation() != null) {
                logger.info("business metric correlation config is present");
                objectArrayList.add(businessConfigurationsBean.getMetricCorrelation());
                sqlBuilder.append("metric_correlation = ?, ");
            }

            if (businessConfigurationsBean.getApiKey() != null) {
                logger.info("business api key config is present");
                objectArrayList.add(businessConfigurationsBean.getApiKey());
                sqlBuilder.append("api_key = ?, ");
                businessIntegratedBean.setAPIKeyChanged(true);
            }

            if (businessConfigurationsBean.getWhatfix() != null) {
                logger.info("business whatfix config is present");
                objectArrayList.add(businessConfigurationsBean.getWhatfix());
                sqlBuilder.append("whatfix = ?, ");
            }

            if (businessConfigurationsBean.getMultipleSublink() != null) {
                logger.info("business multiple sublink config is present");
                objectArrayList.add(businessConfigurationsBean.getMultipleSublink());
                sqlBuilder.append("multiple_sublink = ?, ");
            }

            if (businessConfigurationsBean.getAdvancedAnalysis() != null) {
                logger.info("business advanced analysis config is present");
                objectArrayList.add(businessConfigurationsBean.getAdvancedAnalysis());
                sqlBuilder.append("advanced_analysis = ?, ");
            }

            if (businessConfigurationsBean.getExportLinks() != null) {
                logger.info("business export links config is present");
                objectArrayList.add(businessConfigurationsBean.getExportLinks());
                if (Integer.parseInt(businessConfigurationsBean.getExportLinks()) == 1) {
                    channelValueMap.put(Constants.EXPORTLINKS, true);
                }
                sqlBuilder.append("export_links = ?, ");
            }

            if (businessConfigurationsBean.getRespondentTracker() != null) {
                logger.info("business respondent tracker config is present");
                objectArrayList.add(businessConfigurationsBean.getRespondentTracker());
                sqlBuilder.append("respondent_tracker = ?, ");
            }

            if (businessConfigurationsBean.getFromCustomization() != null) {
                logger.info("business from customization config is present");
                objectArrayList.add(businessConfigurationsBean.getFromCustomization());
                sqlBuilder.append("from_customization = ?, ");
            }

            if (businessConfigurationsBean.getIntegrations() != null) {
                logger.info("business settings integrations config is present");
                objectArrayList.add(businessConfigurationsBean.getIntegrations());
                sqlBuilder.append("integrations = ?, ");
            }

            if (businessConfigurationsBean.getNoOfIntegrations() != null) {
                logger.info("business settings no of integrations is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfIntegrations());
                sqlBuilder.append("no_of_integrations = ?, ");
                usageLimitMap.put("integrations", businessConfigurationsBean.getNoOfIntegrations());
            }

            if (businessConfigurationsBean.getNoOfKiosks() != null) {
                logger.info("business settings no of kiosks is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfKiosks());
                sqlBuilder.append("no_of_kiosks = ?, ");
                usageLimitMap.put("kiosks", businessConfigurationsBean.getNoOfKiosks());
                businessIntegratedBean.setKioskChanged(true);
            }

            if (businessConfigurationsBean.getDynamicLinks() != null) {
                logger.info("business settings dynamicLinks config is present");
                objectArrayList.add(businessConfigurationsBean.getDynamicLinks());
                sqlBuilder.append("dynamic_links = ?, ");
            }

            if (businessConfigurationsBean.getNoOfShareableSublink() != null) {
                logger.info("business settings no of shareable sublink is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfShareableSublink());
                sqlBuilder.append("no_of_shareable_sublink = ?, ");
            }

            if (businessConfigurationsBean.getNoOfMultipleStaticQr() != null) {
                logger.info("business settings no of multiple static qr is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfMultipleStaticQr());
                sqlBuilder.append("no_of_multiple_static_qr = ?, ");
            }

            if (businessConfigurationsBean.getNoOfDynamicSwitchQr() != null) {
                logger.info("business settings no of dynamic switch qr is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfDynamicSwitchQr());
                sqlBuilder.append("no_of_dynamic_switch_qr = ?, ");
            }

            if (businessConfigurationsBean.getNoOfDynamicGroupQr() != null) {
                logger.info("business settings no of dynamic group qr is present");
                objectArrayList.add(businessConfigurationsBean.getNoOfDynamicGroupQr());
                sqlBuilder.append("no_of_dynamic_group_qr = ?, ");
            }

            if (businessConfigurationsBean.getMfa() != null) {
                logger.info("business mfa config is present");
                objectArrayList.add(businessConfigurationsBean.getMfa());
                sqlBuilder.append("mfa = ?, ");
                businessIntegratedBean.setMFAChanged(true);
            }

            if (businessConfigurationsBean.getFocusMetric() != null) {
                logger.info("business settings focus metric is present");
                JSONObject focusJson = new JSONObject();
                focusJson.put("focusMetric", businessConfigurationsBean.getFocusMetric());
                focusJson.put("defaultRecommendation", Utils.getNotNullOrDefault(businessConfigurationsBean.getDefaultRecommendation(), Constants.ZERO_STRING));
                focusJson.put("userDefinedRecommendation", Utils.getNotNullOrDefault(businessConfigurationsBean.getUserDefinedRecommendation(), Constants.ZERO_STRING));

                objectArrayList.add(focusJson.toString());
                sqlBuilder.append("focus_metric = ?, ");
            }

            if (businessConfigurationsBean.getUniqueLinks() != null) {
                logger.info("business settings unique links is present");

                JSONObject uniqueMapJson = new JSONObject();
                uniqueMapJson.put("uniqueLinkEmail", Utils.getNotNullOrDefault(businessConfigurationsBean.getUniqueLinkEmail(), Constants.ZERO_STRING));
                uniqueMapJson.put("uniqueLinkSms", Utils.getNotNullOrDefault(businessConfigurationsBean.getUniqueLinkSms(), Constants.ZERO_STRING));
                uniqueMapJson.put("uniqueLinkWhatsapp", Utils.getNotNullOrDefault(businessConfigurationsBean.getUniqueLinkWhatsapp(), Constants.ZERO_STRING));

                objectArrayList.add(uniqueMapJson.toString());
                sqlBuilder.append("unique_links = ?, ");
            }

            if (businessConfigurationsBean.getSso() != null) {
                logger.info("business settings sso is present");

                JSONObject ssoMapJson = new JSONObject();
                ssoMapJson.put("googleSSO", Utils.getNotNullOrDefault(businessConfigurationsBean.getGoogleSSO(), Constants.ZERO_STRING));
                ssoMapJson.put("microsoftSSO", Utils.getNotNullOrDefault(businessConfigurationsBean.getMicrosoftSSO(), Constants.ZERO_STRING));
                ssoMapJson.put("appleSSO", Utils.getNotNullOrDefault(businessConfigurationsBean.getAppleSSO(), Constants.ZERO_STRING));

                objectArrayList.add(ssoMapJson.toString());
                sqlBuilder.append("sso = ?, ");
                businessIntegratedBean.setSSOChanged(true);
            }

            if (businessConfigurationsBean.getProgramFeatures() != null) {
                logger.info("business settings program features is present");

                JSONObject programMapJson = new JSONObject();
                programMapJson.put("programOverview", Utils.getNotNullOrDefault(businessConfigurationsBean.getProgramOverview(), Constants.ZERO_STRING));
                programMapJson.put("programMetric", Utils.getNotNullOrDefault(businessConfigurationsBean.getProgramMetric(), Constants.ZERO_STRING));

                objectArrayList.add(programMapJson.toString());
                sqlBuilder.append("program_features = ?, ");
            }

            if (businessConfigurationsBean.getAdvancedReport() != null) {
                logger.info("business settings Advanced Report is present");
                objectArrayList.add(businessConfigurationsBean.getAdvancedReport());
                sqlBuilder.append("advanced_report = ?, ");
            }

            if (businessConfigurationsBean.getGamePlan() != null) {
                logger.info("business settings game plan is present");
                objectArrayList.add(businessConfigurationsBean.getGamePlan());
                sqlBuilder.append("game_plan = ?, ");
            }

            if (businessConfigurationsBean.getPreviouslyUsedQuestionLibrary() != null) {
                logger.info("business settings previously used question library is present");
                objectArrayList.add(businessConfigurationsBean.getPreviouslyUsedQuestionLibrary());
                sqlBuilder.append("previously_used_question_library = ?, ");
            }

            if (businessConfigurationsBean.getPredefinedQuestionLibrary() != null) {
                logger.info("business settings predefined question library is present");
                objectArrayList.add(businessConfigurationsBean.getPredefinedQuestionLibrary());
                sqlBuilder.append("predefined_question_library = ?, ");
            }

            if (businessConfigurationsBean.getCustomThemes() != null) {
                logger.info("business custom theme config is present");
                objectArrayList.add(businessConfigurationsBean.getCustomThemes());
                sqlBuilder.append("custom_themes = ?, ");
                if (businessConfigurationsBean.getCustomThemes().equals("0")) {
                    businessIntegratedBean.setCustomThemeChanged(true);
                }
            }

            //DTV-9037
            //chatbot, audit_program, collaboration, custom_dashboard
            if (businessConfigurationsBean.getChatbot() != null) {
                logger.info("business chatbot config is present");
                objectArrayList.add(businessConfigurationsBean.getChatbot());
                sqlBuilder.append(" chatbot = ?, ");
            }

            if (businessConfigurationsBean.getAuditProgram() != null) {
                logger.info("business auditProgram config is present");
                objectArrayList.add(businessConfigurationsBean.getAuditProgram());
                sqlBuilder.append(" audit_program = ?, ");

                //collaboration is sub feature of audit program
                if (businessConfigurationsBean.getCollaboration() != null) {
                    logger.info("business collaboration config is present");
                    objectArrayList.add(businessConfigurationsBean.getCollaboration());
                    sqlBuilder.append(" collaboration = ?, ");
                }
            }

            if (businessConfigurationsBean.getCustomDashboard() != null) {
                logger.info("business customDashboard config is present");
                objectArrayList.add(businessConfigurationsBean.getCustomDashboard());
                sqlBuilder.append(" custom_dashboard = ?, ");
            }

            if (businessConfigurationsBean.getCloseLoop() != null) {
                logger.info("business settings close loop is present");
                objectArrayList.add(businessConfigurationsBean.getCloseLoop());
                //DTV-11826 check if role_permissions_{business_uuid} table has the role "DTWorks". If not, insert the role
                integrationAppService.createDTWorksRoleInDT(businessConfigurationsBean.getCloseLoop(), businessUUID);
                sqlBuilder.append("close_loop = ?, ");
            }

            if (businessConfigurationsBean.getProgramThrottling() != null) {
                logger.info("business settings program throttling is present");
                objectArrayList.add(businessConfigurationsBean.getProgramThrottling());
                sqlBuilder.append("program_throttling = ?, ");
            }

            if (businessConfigurationsBean.getNotes() != null) {
                logger.info("business settings program notes is present");
                objectArrayList.add(businessConfigurationsBean.getNotes());
                sqlBuilder.append("notes = ?, ");
            }

            //DTV-9719
            if (businessConfigurationsBean.getTextAnalyticsConfigId() != null) {
                logger.info("business settings text analytics config id is present");
                objectArrayList.add(businessConfigurationsBean.getTextAnalyticsConfigId());
                sqlBuilder.append("text_analytics_config_id = ?, ");
            }
            //DTV-11388
            if (businessConfigurationsBean.getSubmissionDelay() != null) {
                logger.info("business settings submission_delay present");
                objectArrayList.add(businessConfigurationsBean.getSubmissionDelay());
                sqlBuilder.append("submission_delay = ?, ");
            }

            //DTV-11681
            if (businessConfigurationsBean.getRefreshKioskQr() != null) {
                logger.info("business settings refresh kiosk qr present");
                objectArrayList.add(businessConfigurationsBean.getRefreshKioskQr());
                sqlBuilder.append("refresh_kiosk_qr = ?, ");
            }

            if (businessConfigurationsBean.getAiSurveysLimit() != null) {
                logger.info("business settings ai surveys limit present");
                objectArrayList.add(businessConfigurationsBean.getAiSurveysLimit());
                sqlBuilder.append("ai_surveys_limit = ?, ");
            }

            //DTV-12899
            if (businessConfigurationsBean.getAiSurveys() != null) {
                logger.info("business settings ai surveys present");
                objectArrayList.add(businessConfigurationsBean.getAiSurveys());
                sqlBuilder.append(" ai_surveys = ?, ");
            }

            if(businessConfigurationsBean.getLti() != null) {
                logger.info("business settings lti present");
                businessIntegratedBean.setLtiChanged(true);
                objectArrayList.add(businessConfigurationsBean.getLti());
                sqlBuilder.append("lti = ?, ");
            }

            if(businessConfigurationsBean.getLtiConfig() != null) {
                logger.info("business settings lti config present");
                objectArrayList.add(new JSONObject(businessConfigurationsBean.getLtiConfig()).toString());
                sqlBuilder.append("lti_config = ?, ");
            }

            if(businessConfigurationsBean.getRecurrence() != null) {
                logger.info("business settings recurrence config present");
                objectArrayList.add(businessConfigurationsBean.getRecurrence());
                sqlBuilder.append(" recurrence = ?, ");
            }

            if(businessConfigurationsBean.getResponseQuota() != null) {
                logger.info("business settings response quota config present");
                objectArrayList.add(businessConfigurationsBean.getResponseQuota());
                sqlBuilder.append(" response_quota = ?, ");
            }

            if(businessConfigurationsBean.getCustomDashboardBuilder() != null) {
                logger.info("business settings custom dashboard builder present");
                objectArrayList.add(businessConfigurationsBean.getCustomDashboardBuilder());
                sqlBuilder.append(" custom_dashboard_builder = ?, ");
            }

            if (objectArrayList.size() > 0) {
                objectArrayList.add(timestamp);
                sqlBuilder.append("modified_time = ? ");

                if (businessConfigurationsBean.getBusinessId() != null) {
                    sqlBuilder.append("where business_id = ? ");
                    logger.info("sql " + sqlBuilder);
                    objectArrayList.add(businessConfigurationsBean.getBusinessId());
                    logger.info("Executing the query");
                    updateStatus = businessConfigurationsDao.updateBusinessConfigurations(sqlBuilder.toString(), objectArrayList);
                    logger.info("update status " + updateStatus);
                }
            }
            businessConfigurationsDao.updateBusinessGlobalSettings(businessConfigurationsBean);
            //insert tracker config
            trackConfigParameters(businessConfigurationsBean.getBusinessId());
            businessIntegratedBean.setChannels(channelValueMap);
            businessIntegratedBean.setUsageLimit(usageLimitMap);
            //update internal application configuration changes asynchronously
            this.updateServicesAsynchronously(businessIntegratedBean, oldBusinessConfigurationMap,
                    businessUUID, businessConfigurationsBean, updateStatus);

        } catch (Exception e) {
            logger.error("exception at updateBusinessConfigurationsV1. Msg - {} ", e.getMessage());
        }
        logger.info("updateBusinessConfigurationsV1 - end");
        return updateStatus;
    }

    private Map setDefaultChannelValues() {
        Map channelValueMap = new HashMap();
        channelValueMap.put(Constants.EMAIL, false);
        channelValueMap.put(Constants.SMS, false);
        channelValueMap.put(Constants.QRCODE, false);
        channelValueMap.put(Constants.LINK, false);
        channelValueMap.put(Constants.KIOSK, false);
        channelValueMap.put(Constants.EXPORTLINKS, false);
        channelValueMap.put(Constants.WHATSAPP, false);
        return channelValueMap;
    }

    public void updateServicesAsynchronously(BusinessIntegratedBean businessIntegratedBean,
                                             Map oldBusinessConfigurationMap, String businessUniqueId,
                                             BusinessConfigurationsBean businessConfigurationsBean,
                                             int updateStatus) {
        logger.info("update configurations asynchronously started");
        //send business setting email
        if (updateStatus > 0) {
            logger.info("update status");
            integrationAppService.sendReminderSettingEmail(
                    businessConfigurationsBean.getBusinessId(), businessUniqueId, false, "", "");
            logger.info("update status end");
        }
        //update api key
        if (businessIntegratedBean.isAPIKeyChanged()) {
            logger.info("update api key");
            integrationAppService.updateAPIKey(oldBusinessConfigurationMap, businessConfigurationsBean);
            logger.info("update api key end");
        }
        //update lti changes to dt-edu
        if(businessIntegratedBean.isLtiChanged()) {
            logger.info("update lti");
            integrationAppService.updateLti(businessUniqueId, businessConfigurationsBean);
            logger.info("update lti end");
        }
        //update kiosk count
        if (businessIntegratedBean.isKioskChanged()) {
            logger.info("update kiosk");
            integrationAppService.updateKioskCount(oldBusinessConfigurationMap, businessConfigurationsBean, businessUniqueId);
            logger.info("update kiosk end");
        }
        //update mfa changes to okta
        if (businessIntegratedBean.isMFAChanged()) {
            logger.info("update mfa");
            integrationAppService.upateMFA(oldBusinessConfigurationMap, businessConfigurationsBean, businessUniqueId);
            logger.info("update mfa end");
        }
        //update sso changes to okta
        if (businessIntegratedBean.isSSOChanged()) {
            logger.info("update sso");
            integrationAppService.updateSSO(oldBusinessConfigurationMap, businessConfigurationsBean, businessUniqueId);
            logger.info("update sso end");
        }
        // to update the channels for active surveys in survey schedule for given business account
        Map channelValueMap = businessIntegratedBean.getChannels();
        if (channelValueMap.size() > 0 && businessConfigurationsBean.getBusinessId() != null) {
            logger.info("update channelValueMap");
            businessConfigurationsBean.setChannels(channelValueMap);
            integrationAppService.updateChannelsInScheduledSurveys(channelValueMap, businessConfigurationsBean);
            logger.info("update channelValueMap end");
        }

        //to update the usage_mail_flag when limits are changed
        Map usageLimitMap = businessIntegratedBean.getUsageLimit();
        if (usageLimitMap.size() > 0) {
            logger.info("update updateUsageLimit");
            integrationAppService.updateUsageLimit(businessConfigurationsBean.getBusinessId(), usageLimitMap);
            logger.info("update updateUsageLimit end");
        }

        //Call Text analytics settings API only when TA is modified
        if (businessIntegratedBean.isTAChanged()) {
            logger.info("update ta");
            integrationAppService.updateTASettings(oldBusinessConfigurationMap, businessConfigurationsBean, businessUniqueId);
            logger.info("update ta end");
        }
        if (businessIntegratedBean.isRecipientsChanged()) {
            logger.info("update recipients");
            int oldLimit = (Utils.isNotNull(oldBusinessConfigurationMap.get("noOfRecipients")) &&
                    Utils.notEmptyString(oldBusinessConfigurationMap.get("noOfRecipients").toString())) ? Integer.parseInt(oldBusinessConfigurationMap.get("noOfRecipients").toString()) : -1;
            int newLimit = businessConfigurationsBean.getNoOfRecipients();
            integrationAppService.updateReProcess(businessUniqueId, oldLimit, newLimit, "recipients");
            logger.info("update recipients end");
        }
        if (businessIntegratedBean.isResponseChanged()) {
            logger.info("update response");
            int oldLimit = (Utils.isNotNull(oldBusinessConfigurationMap.get("noOfResponses")) &&
                    Utils.notEmptyString(oldBusinessConfigurationMap.get("noOfResponses").toString())) ?
                    Integer.parseInt(oldBusinessConfigurationMap.get("noOfResponses").toString()) : -1;
            int newLimit = businessConfigurationsBean.getNoOfResponses();
            integrationAppService.updateReProcess(businessUniqueId, oldLimit, newLimit, "responses");
            logger.info("update response end");
        }
        //remove Templates from business
        if (businessIntegratedBean.isTemplateChanged()) {
            logger.info("update template");
            this.removeBusinessFromTemplates(businessConfigurationsBean.getBusinessId());
            logger.info("update template end");
        }
        //remove Templates from business
        if (businessIntegratedBean.isCustomThemeChanged()) {
            logger.info("update theme");
            //TODO test below function
            this.removeBusinessFromCustomThemes(businessConfigurationsBean.getBusinessId());
            logger.info("update theme end");
        }
        if (businessIntegratedBean.isPublishToEliza()) {
            /**
             * commenting this part to save time. If necessary the api can be invoked manually
             */
            //clientService.updateClientConfigEliza(businessUUID);//TODO have to check and uncomment it
        }
        logger.info("abc");
    }

    /**
     * remove all the custom themes mapping for a business
     *
     * @param businessId
     */
    public void removeBusinessFromCustomThemes(int businessId) {
        logger.info("begin removeBusinessFromCustomThemes");
        try {
            String sql = "UPDATE custom_eux_themes SET business_id = JSON_REMOVE(business_id, JSON_UNQUOTE(JSON_SEARCH(business_id, 'one', '" + businessId + "')))" +
                    " WHERE  json_contains(business_id, '\"" + businessId + "\"');";
            int updateResult = jdbcTemplate.update(sql);
            logger.info("end removeBusinessFromCustomThemes");
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * Validate atleast one access settings enabled
     *
     * @return
     */
    public boolean validateConfiguration(BusinessConfigurationsBean businessConfigurationsBean) {
        boolean status = false;

        if (businessConfigurationsBean.getEmail() != null) {
            if (businessConfigurationsBean.getEmail().equals(Constants.BUSINESS_CONFIG_ACTIVE_STATUS)) {
                return true;
            }
        }

        if (businessConfigurationsBean.getSms() != null) {
            if (businessConfigurationsBean.getSms().equals(Constants.BUSINESS_CONFIG_ACTIVE_STATUS)) {
                return true;
            }
        }

        if (businessConfigurationsBean.getWhatsapp() != null) {
            if (businessConfigurationsBean.getWhatsapp().equals(Constants.BUSINESS_CONFIG_ACTIVE_STATUS)) {
                return true;
            }
        }


        if (businessConfigurationsBean.getQrcode() != null) {
            if (businessConfigurationsBean.getQrcode().equals(Constants.BUSINESS_CONFIG_ACTIVE_STATUS)) {
                return true;
            }
        }

        if (businessConfigurationsBean.getShareableLink() != null) {
            if (businessConfigurationsBean.getShareableLink().equals(Constants.BUSINESS_CONFIG_ACTIVE_STATUS)) {
                return true;
            }
        }

        if (businessConfigurationsBean.getKiosk() != null) {
            if (businessConfigurationsBean.getKiosk().equals(Constants.BUSINESS_CONFIG_ACTIVE_STATUS)) {
                return true;
            }
        }
        return status;
    }

    /**
     * Get business configuration by business uuid
     *
     * @param businessId
     * @return
     */
    public List getBusinessConfigurationsByBusinessId(int businessId) {
        String businessUUID = clientService.getBusinessUUIDByID(businessId);
        return getBusinessConfigurationsByBusinessId(businessUUID);
    }

    /**
     *
     * @param businessId
     * @return
     */
    public String getSSOConfigByBusinessId(int businessId) {
        logger.info("getSSOConfigByBusinessId - start");
        String configString =  "";
        String ssoConfig = businessConfigurationsDao.getSSOConfigByBusinessId(businessId);
        try{
            JSONObject jsonObject = new JSONObject(ssoConfig);
            if(jsonObject.has("googleSSO") && Utils.isNotNull(jsonObject.get("googleSSO")) && jsonObject.get("googleSSO").equals("1")){
                configString = "google";
            }else if(jsonObject.has("microsoftSSO") && Utils.isNotNull(jsonObject.get("microsoftSSO")) && jsonObject.get("microsoftSSO").equals("1")){
                configString = "microsoft";
            }else if(jsonObject.has("appleSSO") && Utils.isNotNull(jsonObject.get("appleSSO")) && jsonObject.get("appleSSO").equals("1")){
                configString = "apple";
            }

        }catch (Exception e){
            logger.error("Error in getSSOConfigByBusinessId : "+e.getMessage());
            configString = "";
        }
        logger.info("getSSOConfigByBusinessId - end");
        return configString;
    }

}
