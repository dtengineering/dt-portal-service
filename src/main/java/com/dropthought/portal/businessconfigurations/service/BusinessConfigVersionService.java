package com.dropthought.portal.businessconfigurations.service;

import com.dropthought.portal.businessconfigurations.dao.BusinessConfigVersionDao;
import com.dropthought.portal.businessconfigurations.dto.BusinessConfigurationVersionBean;
import com.dropthought.portal.businessconfigurations.dto.BusinessConfigurationsBean;
import com.dropthought.portal.service.BaseService;
import com.dropthought.portal.service.ClientService;
import com.dropthought.portal.service.TextAnalyticsConfigurationService;
import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.TokenUtility;
import com.dropthought.portal.util.Utils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class BusinessConfigVersionService extends BaseService {

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * JdbcTemplate for DB Connectivity.
     */
    @Autowired
    @Qualifier("jdbcDt")
    JdbcTemplate jdbcTemplate;

   /* @Autowired
    @Qualifier("jdbcNamedDt")
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;*/

    @Autowired
    TokenUtility tokenUtility;

    @Autowired
    ClientService clientService;

    @Autowired
    TextAnalyticsConfigurationService textAnalyticsConfigService;

    @Autowired
    BusinessConfigurationsService businessConfigurationsService;

    @Autowired
    BusinessConfigVersionDao businessConfigVersionDao;

    /*Page*/
    private Integer upperLimit = 0;
    private Integer length = 10;

    /**
     * Function to create business version configurations
     *
     * @param businessConfigurationVersionBean
     * @return
     */
    public int createBusinessVersionConfigurations(BusinessConfigurationVersionBean businessConfigurationVersionBean) {
        return businessConfigVersionDao.createBusinessVersionConfigurations(businessConfigurationVersionBean);
    }

    /**
     * Get businessuuid based on businessid
     *
     * @param businessId
     * @return
     */
    public String retrieveBusinessById(int businessId) {
        String businesUUID = "";
        try {
            String SELECT_BUSINESSUUID_BY_ID = "select business_uuid from business where business_id=?";
            logger.debug("begin retrieving bussinessUUID by business id");
            Map businessMap = jdbcTemplate.queryForMap(SELECT_BUSINESSUUID_BY_ID , new Object[]{businessId});
            if(businessMap.size() > 0){
                businesUUID = businessMap.containsKey("business_uuid") ? (String)businessMap.get("business_uuid") : "";
            }
            logger.debug("end retrieving bussinessUUID by business id {}", businessId);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return businesUUID;
    }

    /**
     * Get all business configuration by business uuid
     *
     * @param businessId
     * @return
     */
    public List getAllBusinessConfigurationsVersionByBusinessId(int businessId){
        List responses = new ArrayList();
        try{
            responses = jdbcTemplate.queryForList(Constants.GET_BUSINESS_VERSION_ALL_CONFIG, businessId);

        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return responses;
    }

    /**
     * Get all business configuration by business uuid
     *
     * @param businessId
     * @return
     */
    public int getAllBusinessConfigurationsVersionsCountByBusinessId(int businessId){
        int size = 0;
        try {
            size = Optional.ofNullable(
                    jdbcTemplate.queryForObject(
                            Constants.GET_BUSINESS_VERSION_ALL_CONFIG_COUNT,
                            new Object[]{businessId},
                            Integer.class
                    )
            ).orElse(0);
        } catch (Exception e) {
            logger.error("Error fetching business configuration versions count for business UUID: {}", e.getMessage());
        }
        return size;
    }

    /**
     * Get business configuration by business uuid
     *
     * @param businessId
     * @return
     */
    public List getBusinessConfigurationsVersionByBusinessId(int pageNo, String timezone, int businessId) {
        List result;
        List businessConfigurationVersionList = new ArrayList();
        logger.info("START getBusinessConfigurationsVersionByBusinessId");
        try {
            //pagination
            if (pageNo > 1) {
                upperLimit = (pageNo - 1) * length;
            } else {
                upperLimit = 0;
            }

            result = businessConfigVersionDao.getBusinessConfigurationsVersionByBusinessId(businessId, upperLimit, length);
            Map businessMap = clientService.getBusinessContractDates(businessId);
            Iterator<Map> iterator = result.iterator();
            while (iterator.hasNext()) {
                Map businessConfigMap = iterator.next();

                sanitiseBusinessConfigVersionParams(businessConfigMap, businessMap);

                String createdTime = businessConfigMap.containsKey("createdTime") ? businessConfigMap.get("createdTime").toString() : "";
                createdTime = Utils.getDateAsStringByTimezone(createdTime, timezone);
                businessConfigMap.remove("createdTime");
                businessConfigMap.put("createdTime", createdTime);

                //DTV-9719 include txtAnalyticsConfigMap
                Map<String, Object> txtAnalyticsConfigMap = new HashMap<>();
                int textAnalyticsConfigId = (Utils.isNotNull(businessConfigMap.get("text_analytics_config_id")) && Utils.notEmptyString(businessConfigMap.get("text_analytics_config_id").toString()))
                        ? Integer.parseInt(businessConfigMap.get("text_analytics_config_id").toString()) : 0;
                if (textAnalyticsConfigId > 0) {
                    txtAnalyticsConfigMap = businessConfigurationsService.constructTextAnalyticsConfigMap(businessConfigMap.getOrDefault("textAnalytics", null),
                            businessConfigMap.getOrDefault("intentAnalysis", null), businessConfigMap.getOrDefault("categoryAnalysis", null), textAnalyticsConfigId);
                }
                businessConfigMap.put("textAnalyticsConfig", txtAnalyticsConfigMap);
                businessConfigMap.remove("text_analytics_config_id");
                businessConfigMap.put("textAnalyticsConfigId", textAnalyticsConfigId);

                businessConfigurationVersionList.add(businessConfigMap);
            }
        } catch (Exception e) {
//            logger.error(e.getMessage());
            logger.error("Exception at getBusinessConfigurationsVersionByBusinessId. Msg {}", e.getMessage());
        }
        logger.info("END getBusinessConfigurationsVersionByBusinessId");
        return businessConfigurationVersionList;
    }

    /**
     * function to retrieve a business by businessUUID
     *
     * @param businessUUID
     * @return
     */
    public int retrieveBusinessByUUID(String businessUUID) {
        String SELECT_BUSINESS_BY_UUID = "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid from business where business_uuid=?";

        logger.debug("begin retrieving retrieveBusinessIDByUUID by business uuid");
        int businesId = 0;
        Map businessMap = jdbcTemplate.queryForMap(SELECT_BUSINESS_BY_UUID , new Object[]{businessUUID});
        if(businessMap.size() > 0){
            businesId = businessMap.containsKey("business_id") ? (int)businessMap.get("business_id") : 0;
        }
        logger.debug("end retrieving retrieveBusinessIDByUUID by business uuid {}", businesId);
        return businesId;
    }


    /**
     * Function to create business version configurations
     *
     * @param businessConfigurationBean
     * @return
     */
    public int createBusinessVersionConfigurations(BusinessConfigurationsBean businessConfigurationBean, boolean fromPlan) {
        //convert businessConfigurationBean to BusinessConfigurationVersionBean
        BusinessConfigurationVersionBean requestBean = new BusinessConfigurationVersionBean();
        try{
            requestBean.setCreatedBy( (Utils.isNotNull(businessConfigurationBean.getCreatedBy())) ? businessConfigurationBean.getCreatedBy() : (Utils.isNotNull(businessConfigurationBean.getModifiedBy()) ? businessConfigurationBean.getModifiedBy() : 0 ));
            requestBean.setWhatsapp(Utils.getNotNullOrDefault(businessConfigurationBean.getWhatsapp(), Constants.ZERO_STRING));
            requestBean.setNoOfWhatsapp(Utils.getNotNullOrDefault(businessConfigurationBean.getNoOfWhatsapp(), Constants.ZERO_INT));
            requestBean.setBusinessId(Utils.getNotNullOrDefault(businessConfigurationBean.getBusinessId(), Constants.ZERO_INT));
            requestBean.setEmail(Utils.getNotNullOrDefault(businessConfigurationBean.getEmail(), Constants.ZERO_STRING));
            requestBean.setSms(Utils.getNotNullOrDefault(businessConfigurationBean.getSms(), Constants.ZERO_STRING));
            requestBean.setQrcode(Utils.getNotNullOrDefault(businessConfigurationBean.getQrcode(), Constants.ZERO_STRING));
            requestBean.setShareableLink(Utils.getNotNullOrDefault(businessConfigurationBean.getShareableLink(), Constants.ZERO_STRING));
            requestBean.setTextAnalytics(Utils.getNotNullOrDefault(businessConfigurationBean.getTextAnalytics(), Constants.ZERO_STRING));
            requestBean.setLogic(Utils.getNotNullOrDefault(businessConfigurationBean.getLogic(), Constants.ZERO_STRING));
            requestBean.setRespondent(Utils.getNotNullOrDefault(businessConfigurationBean.getRespondent(), Constants.ZERO_STRING));
            requestBean.setSegment(Utils.getNotNullOrDefault(businessConfigurationBean.getSegment(), Constants.ZERO_STRING));
            requestBean.setKiosk(Utils.getNotNullOrDefault(businessConfigurationBean.getKiosk(), Constants.ZERO_STRING));
            requestBean.setMetadataQuestion(Utils.getNotNullOrDefault(businessConfigurationBean.getMetadataQuestion(), Constants.ZERO_STRING));
            requestBean.setMobileApp(Utils.getNotNullOrDefault(businessConfigurationBean.getMobileApp(), Constants.ZERO_STRING));
            requestBean.setEmotionAnalysis(Utils.getNotNullOrDefault(businessConfigurationBean.getEmotionAnalysis(), Constants.ZERO_STRING));
            requestBean.setIntentAnalysis(Utils.getNotNullOrDefault(businessConfigurationBean.getIntentAnalysis(), Constants.ZERO_STRING));
            requestBean.setTrigger(Utils.getNotNullOrDefault(businessConfigurationBean.getTrigger(), Constants.ZERO_STRING));
            requestBean.setAdvancedSchedule(Utils.getNotNullOrDefault(businessConfigurationBean.getAdvancedSchedule(), Constants.ZERO_STRING));
            requestBean.setEnglish(Utils.getNotNullOrDefault(businessConfigurationBean.getEnglish(), Constants.ZERO_STRING));
            requestBean.setArabic(Utils.getNotNullOrDefault(businessConfigurationBean.getArabic(), Constants.ZERO_STRING));
            requestBean.setMultiSurveys(Utils.getNotNullOrDefault(businessConfigurationBean.getMultiSurveys(), Constants.ZERO_STRING));
            requestBean.setBitly(Utils.getNotNullOrDefault(businessConfigurationBean.getBitly(), Constants.ZERO_STRING));
            requestBean.setWebHooks(Utils.getNotNullOrDefault(businessConfigurationBean.getWebHooks(), Constants.ZERO_STRING));
            requestBean.setPreferredMetric(Utils.getNotNullOrDefault(businessConfigurationBean.getPreferredMetric(), Constants.ZERO_STRING));
            requestBean.setNotification(Utils.getNotNullOrDefault(businessConfigurationBean.getNotification(), Constants.ZERO_STRING));
            requestBean.setCategoryAnalysis(Utils.getNotNullOrDefault(businessConfigurationBean.getCategoryAnalysis(), Constants.ZERO_STRING));
            requestBean.setAdvancedTextAnalytics(Utils.getNotNullOrDefault(businessConfigurationBean.getAdvancedTextAnalytics(), Constants.ZERO_STRING));
            requestBean.setNoOfUsers(Utils.getNotNullOrDefault(businessConfigurationBean.getNoOfUsers(), Constants.ZERO_INT));
            requestBean.setNoOfActivePrograms(Utils.getNotNullOrDefault(businessConfigurationBean.getNoOfActivePrograms(), Constants.ZERO_INT));
            requestBean.setNoOfResponses(Utils.getNotNullOrDefault(businessConfigurationBean.getNoOfResponses(), Constants.ZERO_INT));
            requestBean.setNoOfMetrics(Utils.getNotNullOrDefault(businessConfigurationBean.getNoOfMetrics(), Constants.ZERO_INT));
            requestBean.setNoOfEmailsSent(Utils.getNotNullOrDefault(businessConfigurationBean.getNoOfEmailsSent(), Constants.ZERO_INT));
            requestBean.setNoOfSms(Utils.getNotNullOrDefault(businessConfigurationBean.getNoOfSms(), Constants.ZERO_INT));
            requestBean.setNoOfApi(Utils.getNotNullOrDefault(businessConfigurationBean.getNoOfApi(), Constants.ZERO_INT));
            requestBean.setNoOfFilters(Utils.getNotNullOrDefault(businessConfigurationBean.getNoOfFilters(), Constants.ZERO_INT));
            requestBean.setNoOfTriggers(Utils.getNotNullOrDefault(businessConfigurationBean.getNoOfTriggers(), Constants.ZERO_INT));
            requestBean.setDataRetention(Utils.getNotNullOrDefault(businessConfigurationBean.getDataRetention(), Constants.ZERO_INT));
            requestBean.setNoOfLists(Utils.getNotNullOrDefault(businessConfigurationBean.getNoOfLists(), Constants.ZERO_INT));
            requestBean.setNoOfRecipients(Utils.getNotNullOrDefault(businessConfigurationBean.getNoOfRecipients(), Constants.ZERO_INT));
            requestBean.setCohortAnalysis(Utils.getNotNullOrDefault(businessConfigurationBean.getCohortAnalysis(), Constants.ZERO_STRING));
            requestBean.setAdvancedTemplates(Utils.getNotNullOrDefault(businessConfigurationBean.getAdvancedTemplates(), Constants.ZERO_STRING));
            requestBean.setMetricCorrelation(Utils.getNotNullOrDefault(businessConfigurationBean.getMetricCorrelation(), Constants.ZERO_STRING));
            requestBean.setApiKey(Utils.getNotNullOrDefault(businessConfigurationBean.getApiKey(), Constants.ZERO_STRING));
            requestBean.setWhatfix(Utils.getNotNullOrDefault(businessConfigurationBean.getWhatfix(), Constants.ZERO_STRING));
            requestBean.setMultipleSublink(Utils.getNotNullOrDefault(businessConfigurationBean.getMultipleSublink(), Constants.ZERO_STRING));
            requestBean.setAdvancedAnalysis(Utils.getNotNullOrDefault(businessConfigurationBean.getAdvancedAnalysis(), Constants.ZERO_STRING));
            requestBean.setExportLinks(Utils.getNotNullOrDefault(businessConfigurationBean.getExportLinks(), Constants.ZERO_STRING));
            requestBean.setHippa(Utils.getNotNullOrDefault(businessConfigurationBean.getHippa(), Constants.ZERO_STRING));
            requestBean.setRespondentTracker(Utils.getNotNullOrDefault(businessConfigurationBean.getRespondentTracker(), Constants.ZERO_STRING));
            requestBean.setFromCustomization(Utils.getNotNullOrDefault(businessConfigurationBean.getFromCustomization(), Constants.ZERO_STRING));
            requestBean.setIntegrations(Utils.getNotNullOrDefault(businessConfigurationBean.getIntegrations(), Constants.ZERO_STRING));
            requestBean.setNoOfIntegrations(Utils.getNotNullOrDefault(businessConfigurationBean.getNoOfIntegrations(), Constants.ZERO_INT));
            requestBean.setNoOfKiosks(Utils.getNotNullOrDefault(businessConfigurationBean.getNoOfKiosks(), Constants.ZERO_INT));
            requestBean.setDynamicLinks(Utils.getNotNullOrDefault(businessConfigurationBean.getDynamicLinks(), Constants.ZERO_STRING));
            requestBean.setNoOfShareableSublink(Utils.getNotNullOrDefault(businessConfigurationBean.getNoOfShareableSublink(), Constants.ZERO_INT));
            requestBean.setNoOfMultipleStaticQr(Utils.getNotNullOrDefault(businessConfigurationBean.getNoOfMultipleStaticQr(), Constants.ZERO_INT));
            requestBean.setNoOfDynamicSwitchQr(Utils.getNotNullOrDefault(businessConfigurationBean.getNoOfDynamicSwitchQr(), Constants.ZERO_INT));
            requestBean.setNoOfDynamicGroupQr(Utils.getNotNullOrDefault(businessConfigurationBean.getNoOfDynamicGroupQr(), Constants.ZERO_INT));
            requestBean.setMfa(Utils.getNotNullOrDefault(businessConfigurationBean.getMfa(), Constants.ZERO_STRING));
            requestBean.setAdvancedReport(Utils.getNotNullOrDefault(businessConfigurationBean.getAdvancedReport(), Constants.ZERO_STRING));
            requestBean.setGamePlan(Utils.getNotNullOrDefault(businessConfigurationBean.getGamePlan(), Constants.ZERO_STRING));
            requestBean.setPreviouslyUsedQuestionLibrary(Utils.getNotNullOrDefault(businessConfigurationBean.getPreviouslyUsedQuestionLibrary(), Constants.ZERO_STRING));
            requestBean.setPredefinedQuestionLibrary(Utils.getNotNullOrDefault(businessConfigurationBean.getPredefinedQuestionLibrary(), Constants.ZERO_STRING));
            requestBean.setCustomThemes(Utils.getNotNullOrDefault(businessConfigurationBean.getCustomThemes(), Constants.ZERO_STRING));
            requestBean.setChatbot(Utils.getNotNullOrDefault(businessConfigurationBean.getChatbot(), Constants.ZERO_STRING));
            requestBean.setAuditProgram(Utils.getNotNullOrDefault(businessConfigurationBean.getAuditProgram(), Constants.ZERO_STRING));
            requestBean.setCollaboration(Utils.getNotNullOrDefault(businessConfigurationBean.getCollaboration(), Constants.ZERO_STRING));
            requestBean.setCustomDashboard(Utils.getNotNullOrDefault(businessConfigurationBean.getCustomDashboard(), Constants.ZERO_STRING));
            requestBean.setRCoefficient(Utils.getNotNullOrDefault(businessConfigurationBean.getRCoefficient(), Constants.ZERO_STRING));
            requestBean.setCloseLoop(Utils.getNotNullOrDefault(businessConfigurationBean.getCloseLoop(), Constants.ZERO_STRING));
            requestBean.setProgramThrottling(Utils.getNotNullOrDefault(businessConfigurationBean.getProgramThrottling(), Constants.ZERO_STRING));
            requestBean.setNotes(Utils.getNotNullOrDefault(businessConfigurationBean.getNotes(), Constants.ZERO_STRING));
            requestBean.setSubmissionDelay(Utils.getNotNullOrDefault(businessConfigurationBean.getSubmissionDelay(), Constants.ZERO_STRING));
            requestBean.setRefreshKioskQr(Utils.getNotNullOrDefault(businessConfigurationBean.getRefreshKioskQr(), Constants.ZERO_STRING));
            requestBean.setAiSurveysLimit(Utils.getNotNullOrDefault(businessConfigurationBean.getAiSurveysLimit(), Constants.AISURVEYS_LIMIT));
            requestBean.setTaskManager(Utils.getNotNullOrDefault(businessConfigurationBean.getTaskManager(), Constants.ZERO_STRING));
            //DTV-12899
            requestBean.setAiSurveys(Utils.getNotNullOrDefault(businessConfigurationBean.getAiSurveys(), Constants.ZERO_STRING));
            requestBean.setLti(Utils.getNotNullOrDefault(businessConfigurationBean.getLti(), Constants.ZERO_STRING));

            // Retrieve values based on 'fromPlan' parameter
            String focusMetric = fromPlan ? new JSONObject(Utils.getNotNullOrDefault(businessConfigurationBean.getFocusMetric(), "{}")).toString() :
                    new JSONObject()
                            .put("focusMetric", Utils.getNotNullOrDefault(businessConfigurationBean.getFocusMetric(), Constants.ZERO_STRING))
                            .put("defaultRecommendation", Utils.getNotNullOrDefault(businessConfigurationBean.getDefaultRecommendation(), Constants.ZERO_STRING))
                            .put("userDefinedRecommendation", Utils.getNotNullOrDefault(businessConfigurationBean.getUserDefinedRecommendation(), Constants.ZERO_STRING))
                            .toString();

            String uniqueLinks = fromPlan ? new JSONObject(Utils.getNotNullOrDefault(businessConfigurationBean.getUniqueLinks(), "{}")).toString() :
                    new JSONObject()
                            .put("uniqueLinkEmail", Utils.getNotNullOrDefault(businessConfigurationBean.getUniqueLinkEmail(), Constants.ZERO_STRING))
                            .put("uniqueLinkSms", Utils.getNotNullOrDefault(businessConfigurationBean.getUniqueLinkSms(), Constants.ZERO_STRING))
                            .put("uniqueLinkWhatsapp", Utils.getNotNullOrDefault(businessConfigurationBean.getUniqueLinkWhatsapp(), Constants.ZERO_STRING))
                            .toString();

            String programFeatures = fromPlan ? new JSONObject(Utils.getNotNullOrDefault(businessConfigurationBean.getProgramFeatures(), "{}")).toString() :
                    new JSONObject()
                            .put("programOverview", Utils.getNotNullOrDefault(businessConfigurationBean.getProgramOverview(), Constants.ZERO_STRING))
                            .put("programMetric", Utils.getNotNullOrDefault(businessConfigurationBean.getProgramMetric(), Constants.ZERO_STRING))
                            .toString();

            String sso = fromPlan ? new JSONObject(Utils.getNotNullOrDefault(businessConfigurationBean.getSso(), "{}")).toString() :
                    new JSONObject()
                            .put("googleSSO", Utils.getNotNullOrDefault(businessConfigurationBean.getGoogleSSO(), Constants.ZERO_STRING))
                            .put("microsoftSSO", Utils.getNotNullOrDefault(businessConfigurationBean.getMicrosoftSSO(), Constants.ZERO_STRING))
                            .put("appleSSO", Utils.getNotNullOrDefault(businessConfigurationBean.getAppleSSO(), Constants.ZERO_STRING))
                            .toString();

            String ltiConfig = fromPlan ? new JSONObject(Utils.getNotNullOrDefault(businessConfigurationBean.getLtiConfig(), null)).toString() :
                    new JSONObject(businessConfigurationBean.getLtiConfig())
                            .toString();

            requestBean.setFocusMetric(focusMetric);
            requestBean.setUniqueLinks(uniqueLinks);
            requestBean.setProgramFeatures(programFeatures);
            requestBean.setSso(sso);
            requestBean.setLtiConfig(mapper.readValue(ltiConfig, HashMap.class));
            //DTV-12326, 13080
            requestBean.setResponseQuota(Utils.getNotNullOrDefault(businessConfigurationBean.getResponseQuota(), Constants.ZERO_STRING));
            requestBean.setRecurrence(Utils.getNotNullOrDefault(businessConfigurationBean.getRecurrence(), Constants.ZERO_STRING));
            requestBean.setCustomDashboardBuilder(Utils.getNotNullOrDefault(businessConfigurationBean.getCustomDashboardBuilder(), Constants.ZERO_STRING));

            requestBean.setTextAnalyticsConfigId(Utils.isNotNull(businessConfigurationBean.getTextAnalyticsConfigId()) ? businessConfigurationBean.getTextAnalyticsConfigId()
                    : textAnalyticsConfigService.getLatestConfigId(businessConfigurationBean.getBusinessId()));

        }catch (Exception e){
            e.printStackTrace();
        }
        //call create method
        int result = createBusinessVersionConfigurations(requestBean);
        return result;
    }

    /**
     * Get business configuration by business uuid and version uuid
     *
     * @param businessUUID
     * @return
     */
    public List getBusinessConfigurationsVersionByVersionId(String businessUUID, String versionUniqueId) {
        List result;
        List businessConfigurationVersionList = new ArrayList();
        logger.info("START getBusinessConfigurationsVersionByVersionId");
        try {
            int businessId = retrieveBusinessByUUID(businessUUID);
            result = businessConfigVersionDao.getBusinessConfigurationsVersionByVersionId(businessId, versionUniqueId);
            Map businessMap = clientService.getBusinessContractDates(businessId);

            Iterator<Map> iterator = result.iterator();
            while (iterator.hasNext()) {
                Map businessConfigMap = iterator.next();
                sanitiseBusinessConfigVersionParams(businessConfigMap, businessMap);
                businessConfigurationVersionList.add(businessConfigMap);
            }
        } catch (Exception e) {
            logger.error("Exception at getBusinessConfigurationsVersionByVersionId. Msg {}", e.getMessage());
        }
        logger.info("END getBusinessConfigurationsVersionByVersionId");
        return businessConfigurationVersionList;
    }

    /**
     * Get business configuration by business uuid
     *
     * @param versionId
     * @return
     */
    public Map getBusinessConfigurationsVersionById(int versionId, int businessId) {
        logger.info("START getBusinessConfigurationsVersionById");
        Map businessConfigMap = new HashMap();
        try {
            businessConfigMap = businessConfigVersionDao.getBusinessConfigurationsVersionById(versionId);
            Map businessMap = clientService.getBusinessContractDates(businessId);
            sanitiseBusinessConfigVersionParams(businessConfigMap, businessMap);

        } catch (Exception e) {
            logger.error("Exception at getBusinessConfigurationsVersionById. Msg {}", e.getMessage());
        }
        logger.info("END getBusinessConfigurationsVersionById");
        return businessConfigMap;
    }

    private void sanitiseBusinessConfigVersionParams(Map businessConfigMap, Map businessMap){
        logger.info("START sanitiseBusinessConfigVersionParams");
        try{
            businessConfigMap.put("businessConfigVersionId", businessConfigMap.remove("business_config_version_uuid"));
            businessConfigMap.put("businessId", businessConfigMap.remove("business_id"));
            businessConfigMap.put("shareableLink", businessConfigMap.remove("shareable_link"));
            businessConfigMap.put("textAnalytics", businessConfigMap.remove("text_analytics"));
            businessConfigMap.put("metadataQuestion",businessConfigMap.remove("metadata_question"));
            businessConfigMap.put("contractStartDate", businessMap.containsKey("contract_start_date") && businessMap.get("contract_start_date") != null ? businessMap.get("contract_start_date").toString() : Constants.EMPTY_STRING);
            businessConfigMap.put("contractEndDate",businessMap.containsKey("contract_end_date") &&  businessMap.get("contract_end_date") != null ? businessMap.get("contract_end_date").toString() : Constants.EMPTY_STRING);
            businessConfigMap.put("mobileApp", businessConfigMap.remove("mobile_app"));
            businessConfigMap.put("emotionAnalysis", businessConfigMap.remove("emotion_analysis"));
            businessConfigMap.put("intentAnalysis", businessConfigMap.remove("intent_analysis"));
            businessConfigMap.put("trigger", businessConfigMap.remove("trigger_toggle"));
            businessConfigMap.put("advancedSchedule", businessConfigMap.remove("advanced_schedule"));
            businessConfigMap.put("english", businessConfigMap.remove("english_toggle"));
            businessConfigMap.put("arabic", businessConfigMap.remove("arabic_toggle"));
            businessConfigMap.put("multiSurveys", businessConfigMap.remove("multi_surveys"));
            businessConfigMap.put("webHooks", businessConfigMap.remove("web_hooks"));
            businessConfigMap.put("preferredMetric", businessConfigMap.remove("preferred_metric"));
            businessConfigMap.put("notification", businessConfigMap.remove("notification"));
            businessConfigMap.put("categoryAnalysis", businessConfigMap.remove("category_analysis"));
            businessConfigMap.put("advancedTextAnalytics", businessConfigMap.remove("advanced_text_analytics"));
            businessConfigMap.put("noOfUsers", businessConfigMap.remove("no_of_users"));
            businessConfigMap.put("noOfActivePrograms", businessConfigMap.remove("no_of_active_programs"));
            businessConfigMap.put("noOfResponses", businessConfigMap.remove("no_of_responses"));
            businessConfigMap.put("noOfMetrics", businessConfigMap.remove("no_of_metrics"));
            businessConfigMap.put("noOfEmailsSent", businessConfigMap.remove("no_of_emails_sent"));
            businessConfigMap.put("noOfSms", businessConfigMap.remove("no_of_sms"));
            businessConfigMap.put("noOfWhatsapp", businessConfigMap.remove("no_of_whatsapp"));
            businessConfigMap.put("noOfApi", businessConfigMap.remove("no_of_api"));
            businessConfigMap.put("noOfFilters", businessConfigMap.remove("no_of_filters"));
            businessConfigMap.put("noOfTriggers", businessConfigMap.remove("no_of_triggers"));
            businessConfigMap.put("noOfLists", businessConfigMap.remove("no_of_lists"));
            businessConfigMap.put("noOfRecipients", businessConfigMap.remove("no_of_recipients"));

            businessConfigMap.put("dataRetention", businessConfigMap.remove("data_retention"));
            businessConfigMap.put("cohortAnalysis", businessConfigMap.remove("cohort_analysis"));
            businessConfigMap.put("advancedTemplates", businessConfigMap.remove("advanced_templates"));
            businessConfigMap.put("metricCorrelation", businessConfigMap.remove("metric_correlation"));

            businessConfigMap.put("apiKey", businessConfigMap.remove("api_key"));
            businessConfigMap.put("multipleSublink", businessConfigMap.remove("multiple_sublink"));
            businessConfigMap.put("advancedAnalysis", businessConfigMap.remove("advanced_analysis"));
            businessConfigMap.put("exportLinks", businessConfigMap.remove("export_links"));

            businessConfigMap.put("respondentTracker", Utils.getNotNullOrDefault(businessConfigMap.remove("respondent_tracker"), 0));
            businessConfigMap.put("fromCustomization", Utils.getNotNullOrDefault(businessConfigMap.remove("from_customization"), 0));
            businessConfigMap.put("noOfIntegrations", Utils.getNotNullOrDefault(businessConfigMap.remove("no_of_integrations"), 0));
            businessConfigMap.put("noOfKiosks", businessConfigMap.remove("no_of_kiosks"));

            businessConfigMap.put("dynamicLinks", businessConfigMap.remove("dynamic_links"));
            businessConfigMap.put("noOfShareableSublink", businessConfigMap.remove("no_of_shareable_sublink"));
            businessConfigMap.put("noOfMultipleStaticQr", businessConfigMap.remove("no_of_multiple_static_qr"));
            businessConfigMap.put("noOfDynamicSwitchQr", businessConfigMap.remove("no_of_dynamic_switch_qr"));
            businessConfigMap.put("noOfDynamicGroupQr", businessConfigMap.remove("no_of_dynamic_group_qr"));


            businessConfigMap.put("focusMetric", Utils.handleNullValueAndReplace(businessConfigMap, "focus_metric", "\\\\\\\"", "\\\""));
            businessConfigMap.put("uniqueLinks", Utils.handleNullValueAndReplace(businessConfigMap, "unique_links", "\\\\\\\"", "\\\""));
            businessConfigMap.put("programFeatures", Utils.handleNullValueAndReplace(businessConfigMap, "program_features", "\\\\\\\"", "\\\""));
            businessConfigMap.put("sso", Utils.handleNullValueAndReplace(businessConfigMap, "sso", "\\\\\\\"", "\\\""));


            businessConfigMap.put("advancedReport", businessConfigMap.remove("advanced_report"));
            businessConfigMap.put("gamePlan", businessConfigMap.remove("game_plan"));
            businessConfigMap.put("previouslyUsedQuestionLibrary", businessConfigMap.remove("previously_used_question_library"));
            businessConfigMap.put("predefinedQuestionLibrary", businessConfigMap.remove("predefined_question_library"));
            businessConfigMap.put("customThemes", businessConfigMap.remove("custom_themes"));


            businessConfigMap.put("auditProgram", businessConfigMap.remove("audit_program"));
            businessConfigMap.put("customDashboard", businessConfigMap.remove("custom_dashboard"));
            businessConfigMap.put("rCoefficient", businessConfigMap.remove("r_coefficient"));
            businessConfigMap.put("closeLoop", businessConfigMap.remove("close_loop"));
            businessConfigMap.put("programThrottling", businessConfigMap.remove("program_throttling"));
            businessConfigMap.put("textAnalyticsConfigId", businessConfigMap.remove("text_analytics_config_id"));
            businessConfigMap.put("submissionDelay", businessConfigMap.remove("submission_delay"));
            businessConfigMap.put("refreshKioskQr", businessConfigMap.remove("refresh_kiosk_qr"));
            businessConfigMap.put("aiSurveysLimit", businessConfigMap.remove("ai_surveys_limit"));
            //DTV-12899
            businessConfigMap.put("aiSurveys", businessConfigMap.remove("ai_surveys"));
            businessConfigMap.put("ltiConfig", businessConfigMap.remove("lti_config"));
            //DTV-12386, 13080
            businessConfigMap.put("responseQuota", businessConfigMap.remove("response_quota"));
            businessConfigMap.put("recurrence", businessConfigMap.remove("recurrence"));
            businessConfigMap.put("customDashboardBuilder", businessConfigMap.remove("custom_dashboard_builder"));
        }catch (Exception e){
         logger.error("Exception at sanitiseBusinessConfigVersionParams. Msg {}", e.getMessage());
        }
        logger.info("END sanitiseBusinessConfigVersionParams");
    }

}
