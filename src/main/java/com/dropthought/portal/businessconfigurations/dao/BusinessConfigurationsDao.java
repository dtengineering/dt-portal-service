package com.dropthought.portal.businessconfigurations.dao;

import com.dropthought.portal.businessconfigurations.dto.BusinessConfigurationsBean;
import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.TokenUtility;
import com.dropthought.portal.util.Utils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class BusinessConfigurationsDao {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());


    /**
     * JdbcTemplate for DB Connectivity.
     */
    @Autowired
    @Qualifier("jdbcDt")
    JdbcTemplate jdbcTemplate;

    @Autowired
    TokenUtility tokenUtility;

    /**
     * Function to create business configurations
     *
     * @param businessConfigurationsBean
     * @return
     */
    public int createBusinessConfigurations(BusinessConfigurationsBean businessConfigurationsBean, boolean fromPlan) {
        int configurationsId = 0;
        logger.info("START createBusinessConfigurations");
        try {
            final Integer userId = Utils.getNotNullOrDefault(businessConfigurationsBean.getCreatedBy(), Constants.ZERO_INT);
            final String whatsapp = Utils.getNotNullOrDefault(businessConfigurationsBean.getWhatsapp(), Constants.ZERO_STRING);
            final Integer businessId = Utils.getNotNullOrDefault(businessConfigurationsBean.getBusinessId(), Constants.ZERO_INT);
            final String email = Utils.getNotNullOrDefault(businessConfigurationsBean.getEmail(), Constants.ZERO_STRING);
            final String sms = Utils.getNotNullOrDefault(businessConfigurationsBean.getSms(), Constants.ZERO_STRING);
            final String qrCode = Utils.getNotNullOrDefault(businessConfigurationsBean.getQrcode(), Constants.ZERO_STRING);
            final String shareableLink = Utils.getNotNullOrDefault(businessConfigurationsBean.getShareableLink(), Constants.ZERO_STRING);
            final String textAnalytics = Utils.getNotNullOrDefault(businessConfigurationsBean.getTextAnalytics(), Constants.ZERO_STRING);
            final String logic = Utils.getNotNullOrDefault(businessConfigurationsBean.getLogic(), Constants.ZERO_STRING);
            final String respondent = Utils.getNotNullOrDefault(businessConfigurationsBean.getRespondent(), Constants.ZERO_STRING);
            final String segment = Utils.getNotNullOrDefault(businessConfigurationsBean.getSegment(), Constants.ZERO_STRING);
            final String kiosk = Utils.getNotNullOrDefault(businessConfigurationsBean.getKiosk(), Constants.ZERO_STRING);
            final String metadataQuestion = Utils.getNotNullOrDefault(businessConfigurationsBean.getMetadataQuestion(), Constants.ZERO_STRING);
            final String mobileApp = Utils.getNotNullOrDefault(businessConfigurationsBean.getMobileApp(), Constants.ZERO_STRING);
            final String emotionAnalysis = Utils.getNotNullOrDefault(businessConfigurationsBean.getEmotionAnalysis(), Constants.ZERO_STRING);
            final String intentAnalysis = Utils.getNotNullOrDefault(businessConfigurationsBean.getIntentAnalysis(), Constants.ZERO_STRING);
            final String trigger = Utils.getNotNullOrDefault(businessConfigurationsBean.getTrigger(), Constants.ZERO_STRING);
            final String advancedSchedule = Utils.getNotNullOrDefault(businessConfigurationsBean.getAdvancedSchedule(), Constants.ZERO_STRING);
            final String english = Utils.getNotNullOrDefault(businessConfigurationsBean.getEnglish(), Constants.ZERO_STRING);
            final String arabic = Utils.getNotNullOrDefault(businessConfigurationsBean.getArabic(), Constants.ZERO_STRING);
            final String multiSurveys = Utils.getNotNullOrDefault(businessConfigurationsBean.getMultiSurveys(), Constants.ZERO_STRING);
            final String bitly = Utils.getNotNullOrDefault(businessConfigurationsBean.getBitly(), Constants.ZERO_STRING);
            final String webHooks = Utils.getNotNullOrDefault(businessConfigurationsBean.getWebHooks(), Constants.ZERO_STRING);
            final String preferredMetric = Utils.getNotNullOrDefault(businessConfigurationsBean.getPreferredMetric(), Constants.ZERO_STRING);
            final String notification = Utils.getNotNullOrDefault(businessConfigurationsBean.getNotification(), Constants.ZERO_STRING);
            final String categoryAnalysis = Utils.getNotNullOrDefault(businessConfigurationsBean.getCategoryAnalysis(), Constants.ZERO_STRING);
            final Integer noOfUsers = Utils.getNotNullOrDefault(businessConfigurationsBean.getNoOfUsers(), Constants.LIMIT_UNLIMITED);
            final Integer noOfActivePrograms = Utils.getNotNullOrDefault(businessConfigurationsBean.getNoOfActivePrograms(), Constants.LIMIT_UNLIMITED);
            final Integer noOfResponses = Utils.getNotNullOrDefault(businessConfigurationsBean.getNoOfResponses(), Constants.LIMIT_UNLIMITED);
            final Integer noOfMetrics = Utils.getNotNullOrDefault(businessConfigurationsBean.getNoOfMetrics(), Constants.LIMIT_UNLIMITED);
            final Integer noOfEmailsSent = Utils.getNotNullOrDefault(businessConfigurationsBean.getNoOfEmailsSent(), Constants.LIMIT_UNLIMITED);
            final Integer noOfSms = Utils.getNotNullOrDefault(businessConfigurationsBean.getNoOfSms(), Constants.LIMIT_UNLIMITED);
            final Integer noOfWhatsapp = Utils.getNotNullOrDefault(businessConfigurationsBean.getNoOfWhatsapp(), Constants.LIMIT_UNLIMITED);
            final Integer noOfApi = Utils.getNotNullOrDefault(businessConfigurationsBean.getNoOfApi(), Constants.LIMIT_UNLIMITED);
            final Integer noOfFilters = Utils.getNotNullOrDefault(businessConfigurationsBean.getNoOfFilters(), Constants.LIMIT_UNLIMITED);
            final Integer noOfTriggers = Utils.getNotNullOrDefault(businessConfigurationsBean.getNoOfTriggers(), Constants.LIMIT_UNLIMITED);
            final Integer dataRetention = Utils.getNotNullOrDefault(businessConfigurationsBean.getDataRetention(), Constants.LIMIT_UNLIMITED);
            final Integer noOfLists = Utils.getNotNullOrDefault(businessConfigurationsBean.getNoOfLists(), Constants.LIMIT_UNLIMITED);
            final Integer noOfRecipients = Utils.getNotNullOrDefault(businessConfigurationsBean.getNoOfRecipients(), Constants.LIMIT_UNLIMITED);
            final String rCoefficient = Utils.getNotNullOrDefault(businessConfigurationsBean.getRCoefficient(), Constants.ZERO_STRING);
            final String closeLoop = Utils.getNotNullOrDefault(businessConfigurationsBean.getCloseLoop(), Constants.ZERO_STRING);
            final String programThrottling = Utils.getNotNullOrDefault(businessConfigurationsBean.getProgramThrottling(), Constants.ZERO_STRING);
            final String advancedTextAnalytics = Utils.getNotNullOrDefault(businessConfigurationsBean.getAdvancedTextAnalytics(), Constants.ZERO_STRING);
            final String customThemes = Utils.getNotNullOrDefault(businessConfigurationsBean.getCustomThemes(), Constants.ZERO_STRING);
            final String cohortAnalysis = Utils.getNotNullOrDefault(businessConfigurationsBean.getCohortAnalysis(), Constants.ZERO_STRING);
            final String advancedTemplates = Utils.getNotNullOrDefault(businessConfigurationsBean.getAdvancedTemplates(), Constants.ZERO_STRING);
            final String metricCorrelation = Utils.getNotNullOrDefault(businessConfigurationsBean.getMetricCorrelation(), Constants.ZERO_STRING);
            final String apiKey = Utils.getNotNullOrDefault(businessConfigurationsBean.getApiKey(), Constants.ZERO_STRING);
            final String whatfix = Utils.getNotNullOrDefault(businessConfigurationsBean.getWhatfix(), Constants.ZERO_STRING);
            final String multipleSublink = Utils.getNotNullOrDefault(businessConfigurationsBean.getMultipleSublink(), Constants.ZERO_STRING);
            final String advancedAnalysis = Utils.getNotNullOrDefault(businessConfigurationsBean.getAdvancedAnalysis(), Constants.ZERO_STRING);
            final String exportLinks = Utils.getNotNullOrDefault(businessConfigurationsBean.getExportLinks(), Constants.ZERO_STRING);
            final String respondentTracker = Utils.getNotNullOrDefault(businessConfigurationsBean.getRespondentTracker(), Constants.ZERO_STRING);
            final String fromCustomization = Utils.getNotNullOrDefault(businessConfigurationsBean.getFromCustomization(), Constants.ZERO_STRING);
            final String integrations = Utils.getNotNullOrDefault(businessConfigurationsBean.getIntegrations(), Constants.ZERO_STRING);
            final Integer noOfIntegrations = Utils.getNotNullOrDefault(businessConfigurationsBean.getNoOfIntegrations(), Constants.LIMIT_UNLIMITED);
            final Integer noOfKiosks = Utils.getNotNullOrDefault(businessConfigurationsBean.getNoOfKiosks(),Constants.LIMIT_UNLIMITED);
            final String dynamicLinks = Utils.getNotNullOrDefault(businessConfigurationsBean.getDynamicLinks(), Constants.ZERO_STRING);
            final Integer noOfShareableSublink = Utils.getNotNullOrDefault(businessConfigurationsBean.getNoOfShareableSublink(), Constants.LIMIT_UNLIMITED);
            final Integer noOfMultipleStaticQr = Utils.getNotNullOrDefault(businessConfigurationsBean.getNoOfMultipleStaticQr(), Constants.LIMIT_UNLIMITED);
            final Integer noOfDynamicSwitchQr = Utils.getNotNullOrDefault(businessConfigurationsBean.getNoOfDynamicSwitchQr(), Constants.LIMIT_UNLIMITED);
            final Integer noOfDynamicGroupQr = Utils.getNotNullOrDefault(businessConfigurationsBean.getNoOfDynamicGroupQr(),  Constants.LIMIT_UNLIMITED);
            final String mfa = Utils.getNotNullOrDefault(businessConfigurationsBean.getMfa(), Constants.ZERO_STRING);
            final String advancedReport = Utils.getNotNullOrDefault(businessConfigurationsBean.getAdvancedReport(), Constants.ZERO_STRING);
            final String gamePlan = Utils.getNotNullOrDefault(businessConfigurationsBean.getGamePlan(), Constants.ZERO_STRING);
            final String previouslyUsedQuestionLibrary = Utils.getNotNullOrDefault(businessConfigurationsBean.getPreviouslyUsedQuestionLibrary(), Constants.ZERO_STRING);
            final String predefinedQuestionLibrary = Utils.getNotNullOrDefault(businessConfigurationsBean.getPredefinedQuestionLibrary(), Constants.ZERO_STRING);
            final String chatbot = Utils.getNotNullOrDefault(businessConfigurationsBean.getChatbot(), Constants.ZERO_STRING);
            final String auditProgram = Utils.getNotNullOrDefault(businessConfigurationsBean.getAuditProgram(), Constants.ZERO_STRING);
            final String collaboration = Utils.getNotNullOrDefault(businessConfigurationsBean.getCollaboration(), Constants.ZERO_STRING);
            final String customDashboard = Utils.getNotNullOrDefault(businessConfigurationsBean.getCustomDashboard(), Constants.ZERO_STRING);
            final String notes = Utils.getNotNullOrDefault(businessConfigurationsBean.getNotes(), Constants.ZERO_STRING);
            final String submissionDelay = Utils.getNotNullOrDefault(businessConfigurationsBean.getSubmissionDelay(), Constants.ZERO_STRING);
            final String refreshKioskQr = Utils.getNotNullOrDefault(businessConfigurationsBean.getRefreshKioskQr(), Constants.ZERO_STRING);
            final String aiSurveysLimit = Utils.getNotNullOrDefault(businessConfigurationsBean.getAiSurveysLimit(), Constants.AISURVEYS_LIMIT);
            final String hippa = Utils.getNotNullOrDefault(businessConfigurationsBean.getHippa(), Constants.ZERO_STRING);
            final String taskManager = Utils.getNotNullOrDefault(businessConfigurationsBean.getTaskManager(), Constants.ZERO_STRING);
            //DTV-12899
            final String aiSurveys = Utils.getNotNullOrDefault(businessConfigurationsBean.getAiSurveys(), Constants.ZERO_STRING);
            final String lti = Utils.getNotNullOrDefault(businessConfigurationsBean.getLti(), Constants.ZERO_STRING);
            final Map ltiConfig = Utils.getNotNullOrDefault(businessConfigurationsBean.getLtiConfig(), null);
            final String responseQuota = Utils.getNotNullOrDefault(businessConfigurationsBean.getResponseQuota(), Constants.ZERO_STRING);
            final String recurrence = Utils.getNotNullOrDefault(businessConfigurationsBean.getRecurrence(), Constants.ZERO_STRING);
            final String customDashboardBuilder = Utils.getNotNullOrDefault(businessConfigurationsBean.getCustomDashboardBuilder(), Constants.ZERO_STRING);

            // Retrieve values based on 'fromPlan' parameter
            String focusMetric = fromPlan ? new JSONObject(Utils.getNotNullOrDefault(businessConfigurationsBean.getFocusMetric(), Constants.EMPTY_OBJECT)).toString() :
                    new JSONObject()
                            .put("focusMetric", Utils.getNotNullOrDefault(businessConfigurationsBean.getFocusMetric(), Constants.ZERO_STRING))
                            .put("defaultRecommendation", Utils.getNotNullOrDefault(businessConfigurationsBean.getDefaultRecommendation(), Constants.ZERO_STRING))
                            .put("userDefinedRecommendation", Utils.getNotNullOrDefault(businessConfigurationsBean.getUserDefinedRecommendation(), Constants.ZERO_STRING))
                            .toString();

            String uniqueLinks = fromPlan ? new JSONObject(Utils.getNotNullOrDefault(businessConfigurationsBean.getUniqueLinks(), Constants.EMPTY_OBJECT)).toString() :
                    new JSONObject()
                            .put("uniqueLinkEmail", Utils.getNotNullOrDefault(businessConfigurationsBean.getUniqueLinkEmail(), Constants.ZERO_STRING))
                            .put("uniqueLinkSms", Utils.getNotNullOrDefault(businessConfigurationsBean.getUniqueLinkSms(), Constants.ZERO_STRING))
                            .put("uniqueLinkWhatsapp", Utils.getNotNullOrDefault(businessConfigurationsBean.getUniqueLinkWhatsapp(), Constants.ZERO_STRING))
                            .toString();

            String programFeatures = fromPlan ? new JSONObject(Utils.getNotNullOrDefault(businessConfigurationsBean.getProgramFeatures(), Constants.EMPTY_OBJECT)).toString() :
                    new JSONObject()
                            .put("programOverview", Utils.getNotNullOrDefault(businessConfigurationsBean.getProgramOverview(), Constants.ZERO_STRING))
                            .put("programMetric", Utils.getNotNullOrDefault(businessConfigurationsBean.getProgramMetric(), Constants.ZERO_STRING))
                            .toString();

            String sso = fromPlan ? new JSONObject(Utils.getNotNullOrDefault(businessConfigurationsBean.getSso(), Constants.EMPTY_OBJECT)).toString() :
                    new JSONObject()
                            .put("googleSSO", Utils.getNotNullOrDefault(businessConfigurationsBean.getGoogleSSO(), Constants.ZERO_STRING))
                            .put("microsoftSSO", Utils.getNotNullOrDefault(businessConfigurationsBean.getMicrosoftSSO(), Constants.ZERO_STRING))
                            .put("appleSSO", Utils.getNotNullOrDefault(businessConfigurationsBean.getAppleSSO(), Constants.ZERO_STRING))
                            .toString();

            final Integer textAnalyticsConfigId = Utils.getNotNullOrDefault(businessConfigurationsBean.getTextAnalyticsConfigId(), Constants.ZERO_INT);
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(Constants.CREATE_BUSINESS_CONFIG, Statement.RETURN_GENERATED_KEYS);
                ps.setInt(1, businessId);
                ps.setString(2, email);
                ps.setString(3, sms);
                ps.setString(4, qrCode);
                ps.setString(5, shareableLink);
                ps.setString(6, textAnalytics);
                ps.setString(7, logic);
                ps.setString(8, respondent);
                ps.setInt(9, userId);
                ps.setTimestamp(10, new Timestamp(System.currentTimeMillis()));
                ps.setString(11, tokenUtility.generateUUID());
                ps.setString(12, segment);
                ps.setString(13, kiosk);
                ps.setString(14, metadataQuestion);
                ps.setString(15, mobileApp);
                ps.setString(16, emotionAnalysis);
                ps.setString(17, intentAnalysis);
                ps.setString(18, trigger);
                ps.setString(19, advancedSchedule);
                ps.setString(20, english);
                ps.setString(21, arabic);
                ps.setString(22, multiSurveys);
                ps.setString(23, bitly);
                ps.setString(24, webHooks);
                ps.setString(25, preferredMetric);
                ps.setString(26, categoryAnalysis);
                ps.setString(27, advancedTextAnalytics);
                ps.setInt(28, noOfUsers);
                ps.setInt(29, noOfActivePrograms);
                ps.setInt(30, noOfResponses);
                ps.setInt(31, noOfMetrics);
                ps.setInt(32, noOfEmailsSent);
                ps.setInt(33, noOfSms);
                ps.setInt(34, noOfApi);
                ps.setInt(35, noOfFilters);
                ps.setInt(36, noOfTriggers);
                ps.setInt(37, dataRetention);
                ps.setInt(38, noOfLists);
                ps.setInt(39, noOfRecipients);
                ps.setString(40, hippa);
                ps.setString(41, cohortAnalysis);
                ps.setString(42, advancedTemplates);
                ps.setString(43, metricCorrelation);
                ps.setString(44, apiKey);
                ps.setString(45, whatfix);
                ps.setString(46, multipleSublink);
                ps.setString(47, advancedAnalysis);
                ps.setString(48, exportLinks);
                ps.setString(49, respondentTracker);
                ps.setString(50, fromCustomization);
                ps.setString(51, integrations);
                ps.setInt(52, noOfIntegrations);
                ps.setInt(53, noOfKiosks);
                ps.setString(54, dynamicLinks);
                ps.setInt(55, noOfShareableSublink);
                ps.setInt(56, noOfMultipleStaticQr);
                ps.setInt(57, noOfDynamicSwitchQr);
                ps.setInt(58, noOfDynamicGroupQr);
                ps.setString(59, mfa);
                ps.setString(60, focusMetric);
                ps.setString(61, advancedReport);
                ps.setString(62, previouslyUsedQuestionLibrary);
                ps.setString(63, predefinedQuestionLibrary);
                ps.setString(64, customThemes);
                ps.setString(65, chatbot);
                ps.setString(66, auditProgram);
                ps.setString(67, collaboration);
                ps.setString(68, customDashboard);
                ps.setString(69, whatsapp);
                ps.setInt(70, noOfWhatsapp);
                ps.setString(71, rCoefficient);
                ps.setString(72, closeLoop);
                ps.setString(73, programThrottling);
                ps.setInt(74, textAnalyticsConfigId);
                ps.setString(75, gamePlan);
                ps.setString(76, notes);
                ps.setString(77, notification);
                ps.setString(78, submissionDelay);
                ps.setString(79, uniqueLinks);
                ps.setString(80, programFeatures);
                ps.setString(81, refreshKioskQr);
                ps.setString(82, aiSurveysLimit);
                ps.setString(83, taskManager);
                ps.setString(84, aiSurveys);
                ps.setString(85, lti);
                ps.setString(86, new JSONObject(ltiConfig).toString());
                ps.setString(87, sso);
                ps.setString(88, responseQuota);
                ps.setString(89, recurrence);
                ps.setString(90, customDashboardBuilder);
                return ps;
            }, keyHolder);
            configurationsId = keyHolder.getKey().intValue();

        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        logger.info("END createBusinessConfigurations");
        return configurationsId;
    }

    public List getBusinessConfigurationsByBusinessId(int businessId){
        return jdbcTemplate.queryForList(Constants.GET_BUSINESS_CONFIGURATIONS_BY_BUSINESS_ID, businessId);
    }

    /**
     *
     * @param businessId
     * @param trackParam
     * @return
     */
    public String getKioskLatestConfigDate(int businessId, String trackParam) {
        logger.info("Start  getKioskLatestConfigDate");
        String lastConfigTime = Constants.EMPTY_STRING;
        try{
            List resultList = jdbcTemplate.queryForList(Constants.GET_TACKER_CONFIG, businessId, trackParam);
            if(!resultList.isEmpty()){
                Map resultMap = (Map) resultList.get(0);
                lastConfigTime = resultMap.getOrDefault(Constants.CREATED_TIME, Constants.EMPTY_STRING).toString();
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        logger.info("End  getKioskLatestConfigDate");
        return lastConfigTime;
    }

    /**
     * Get business configurations by business id
     *
     * @param businessId
     * @return
     */
    public String getSSOConfigByBusinessId(int businessId) {
        logger.info("Start  getSSOConfigByBusinessId");
        String ssoConfig = Constants.EMPTY_STRING;
        try {
            List resultList = jdbcTemplate.queryForList(Constants.GET_SSO_CONFIG, businessId);
            if (!resultList.isEmpty()) {
                Map resultMap = (Map) resultList.get(0);
                ssoConfig = resultMap.getOrDefault(Constants.SSO, Constants.EMPTY_STRING).toString();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("End  getSSOConfigByBusinessId");
        return ssoConfig;
    }

    /**
     * Delete business configurations by business config uuid
     *
     * @param businessConfigUUID
     * @return
     */
    public int deleteBusinessConfigurationsByConfigId(String businessConfigUUID) {
        int deleteStatus = 0;
        logger.info("START  deleteBusinessConfigurationsByConfigId");
        try {
            deleteStatus = jdbcTemplate.update(Constants.DELETE_CONFIG_BY_CONFIG_UUID, businessConfigUUID);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("END  deleteBusinessConfigurationsByConfigId");
        return deleteStatus;
    }

    /**
     * Delete business configurations by business id
     *
     * @param businessId
     * @return
     */
    public int deleteBusinessConfigurationsByBusinessId(int businessId) {
        int deleteStatus = 0;
        logger.info("START  deleteBusinessConfigurationsByBusinessId");
        try {
            deleteStatus = jdbcTemplate.update(Constants.DELETE_CONFIG_BY_BUSINESSID, businessId);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("END  deleteBusinessConfigurationsByBusinessId");
        return deleteStatus;
    }

    /**
     * update business_email_alert in global settings table
     *
     * @param businessId
     * @param notification
     * @return
     */
    public int updateGlobalSettings(int businessId, String notification) {
        int result = 0;
        logger.info("START updateGlobalSettings");
        try {
            result = jdbcTemplate.update(Constants.UPDATE_BUSINESS_GLOBAL_SETTING_EMAIL_ALERT, notification, businessId);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("END updateGlobalSettings");
        return result;
    }

    /**
     * update business_configurations
     *
     * @return
     */
    public int updateBusinessConfigurations(String sqlBuilder, ArrayList<Object> params) {
        int result = 0;
        logger.info("START updateBusinessConfigurations");
        try {
            result = jdbcTemplate.update(sqlBuilder, params.toArray());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("END updateBusinessConfigurations");
        return result;
    }

    /**
     * update business_configurations
     *
     * @return
     */
    public int updateBusinessGlobalSettings(BusinessConfigurationsBean businessConfigurationsBean) {
        int result = 0;
        logger.info("START updateBusinessGlobalSettings");
        try {
            // Update business_email_alert in business_global_settings table
            StringBuilder globalSettingsSql = new StringBuilder("update business_global_settings set ");
            List<Object> settingsObjects = new ArrayList<>();

            if (businessConfigurationsBean.getNotification() != null) {
                logger.info("business notification config is present");
                globalSettingsSql.append("business_email_alert = ?, ");
                settingsObjects.add(businessConfigurationsBean.getNotification());
            }

            if (!settingsObjects.isEmpty()) {
                globalSettingsSql.append("modified_time = ? where business_id = ?");
                settingsObjects.add(new Timestamp(System.currentTimeMillis()));
                settingsObjects.add(businessConfigurationsBean.getBusinessId());
                logger.info("globalSettingsSql: " + globalSettingsSql);

                int updateStatus = jdbcTemplate.update(globalSettingsSql.toString(), settingsObjects.toArray());
                logger.info("update status: " + updateStatus);
            } else {
                logger.info("No business notification config present, nothing to update.");
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("END updateBusinessGlobalSettings");
        return result;
    }

}
