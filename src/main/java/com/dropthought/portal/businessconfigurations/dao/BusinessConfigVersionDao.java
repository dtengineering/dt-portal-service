package com.dropthought.portal.businessconfigurations.dao;

import com.dropthought.portal.businessconfigurations.dto.BusinessConfigurationVersionBean;
import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.TokenUtility;
import com.dropthought.portal.util.Utils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.*;

@Component
public class BusinessConfigVersionDao {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());


    /**
     * JdbcTemplate for DB Connectivity.
     */
    @Autowired
    @Qualifier("jdbcDt")
    JdbcTemplate jdbcTemplate;

    @Autowired
    TokenUtility tokenUtility;

    /**
     * Function to create business version configurations
     *
     * @return businessConfigVersionId
     */
    public int createBusinessVersionConfigurations(final BusinessConfigurationVersionBean businessConfigurationVersionBean) {
        logger.info("START createBusinessVersionConfigurations");
        int configurationsVersionId = 0;
        try {

            Integer userId = businessConfigurationVersionBean.getCreatedBy();
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            Integer businessId = businessConfigurationVersionBean.getBusinessId();
            String email = businessConfigurationVersionBean.getEmail();
            String sms = businessConfigurationVersionBean.getSms();
            String whatsapp = businessConfigurationVersionBean.getWhatsapp();
            String qrCode = businessConfigurationVersionBean.getQrcode();
            String shareableLink = businessConfigurationVersionBean.getShareableLink();
            String textAnalytics = businessConfigurationVersionBean.getTextAnalytics();
            String logic = businessConfigurationVersionBean.getLogic();
            String respondent = businessConfigurationVersionBean.getRespondent();
            String businessConfigVersionUUID = tokenUtility.generateUUID();
            String segment = businessConfigurationVersionBean.getSegment();
            String kiosk = businessConfigurationVersionBean.getKiosk();
            String metadataQuestion = businessConfigurationVersionBean.getMetadataQuestion();
            String mobileApp = businessConfigurationVersionBean.getMobileApp();
            String emotionAnalysis = businessConfigurationVersionBean.getEmotionAnalysis();
            String intentAnalysis = businessConfigurationVersionBean.getIntentAnalysis();
            String trigger = businessConfigurationVersionBean.getTrigger();
            String advancedSchedule = businessConfigurationVersionBean.getAdvancedSchedule();
            String english = businessConfigurationVersionBean.getEnglish();
            String arabic = businessConfigurationVersionBean.getArabic();
            String multiSurveys = businessConfigurationVersionBean.getMultiSurveys();
            String bitly = businessConfigurationVersionBean.getBitly();
            String webHooks = businessConfigurationVersionBean.getWebHooks();
            String preferredMetric = businessConfigurationVersionBean.getPreferredMetric();
            String notification = businessConfigurationVersionBean.getNotification();
            String categoryAnalysis = Utils.isNotNull(businessConfigurationVersionBean.getCategoryAnalysis()) ? businessConfigurationVersionBean.getCategoryAnalysis() : Constants.LIMIT_DEFAULT;
            String advancedTextAnalytics = Utils.isNotNull(businessConfigurationVersionBean.getAdvancedTextAnalytics()) ? businessConfigurationVersionBean.getAdvancedTextAnalytics() : Constants.LIMIT_DEFAULT;
            Integer noOfUsers = Utils.isNotNull(businessConfigurationVersionBean.getNoOfUsers()) ? businessConfigurationVersionBean.getNoOfUsers() : Constants.LIMIT_UNLIMITED;
            Integer noOfActivePrograms = Utils.isNotNull(businessConfigurationVersionBean.getNoOfActivePrograms()) ? businessConfigurationVersionBean.getNoOfActivePrograms() : Constants.LIMIT_UNLIMITED;
            Integer noOfResponses = Utils.isNotNull(businessConfigurationVersionBean.getNoOfResponses()) ? businessConfigurationVersionBean.getNoOfResponses() : Constants.LIMIT_UNLIMITED;
            Integer noOfMetrics = Utils.isNotNull(businessConfigurationVersionBean.getNoOfMetrics()) ? businessConfigurationVersionBean.getNoOfMetrics() : Constants.LIMIT_UNLIMITED;
            Integer noOfEmailsSent = Utils.isNotNull(businessConfigurationVersionBean.getNoOfEmailsSent()) ? businessConfigurationVersionBean.getNoOfEmailsSent() : Constants.LIMIT_UNLIMITED;
            Integer noOfSms = Utils.isNotNull(businessConfigurationVersionBean.getNoOfSms()) ? businessConfigurationVersionBean.getNoOfSms() : Constants.LIMIT_UNLIMITED;
            Integer noOfWhatsapp = Utils.isNotNull(businessConfigurationVersionBean.getNoOfWhatsapp()) ? businessConfigurationVersionBean.getNoOfWhatsapp() : Constants.LIMIT_UNLIMITED;
            Integer noOfApi = Utils.isNotNull(businessConfigurationVersionBean.getNoOfApi()) ? businessConfigurationVersionBean.getNoOfApi() : Constants.LIMIT_UNLIMITED;
            Integer noOfFilters = Utils.isNotNull(businessConfigurationVersionBean.getNoOfFilters()) ? businessConfigurationVersionBean.getNoOfFilters() : Constants.LIMIT_UNLIMITED;
            Integer noOfTriggers = Utils.isNotNull(businessConfigurationVersionBean.getNoOfTriggers()) ? businessConfigurationVersionBean.getNoOfTriggers() : Constants.LIMIT_UNLIMITED;
            Integer dataRetention = Utils.isNotNull(businessConfigurationVersionBean.getDataRetention()) ? businessConfigurationVersionBean.getDataRetention() : Constants.LIMIT_UNLIMITED;
            Integer noOfLists = Utils.isNotNull(businessConfigurationVersionBean.getNoOfLists()) ? businessConfigurationVersionBean.getNoOfLists() : Constants.LIMIT_UNLIMITED;
            Integer noOfRecipients = Utils.isNotNull(businessConfigurationVersionBean.getNoOfRecipients()) ? businessConfigurationVersionBean.getNoOfRecipients() : Constants.LIMIT_UNLIMITED;
            String hippa = businessConfigurationVersionBean.getHippa();
            String cohortAnalysis = businessConfigurationVersionBean.getCohortAnalysis();
            String advancedTemplates = businessConfigurationVersionBean.getAdvancedTemplates();
            String metricCorrelation = businessConfigurationVersionBean.getMetricCorrelation();
            String apiKey = businessConfigurationVersionBean.getApiKey();
            String whatfix = businessConfigurationVersionBean.getWhatfix();
            String multipleSublink = businessConfigurationVersionBean.getMultipleSublink();
            String advancedAnalysis = businessConfigurationVersionBean.getAdvancedAnalysis();
            String exportLinks = businessConfigurationVersionBean.getExportLinks();
            String respondentTracker = businessConfigurationVersionBean.getRespondentTracker();
            String fromCustomization = businessConfigurationVersionBean.getFromCustomization();
            String integrations = businessConfigurationVersionBean.getIntegrations();
            Integer noOfIntegrations = Utils.isNotNull(businessConfigurationVersionBean.getNoOfIntegrations()) ? businessConfigurationVersionBean.getNoOfIntegrations() : Constants.LIMIT_UNLIMITED;
            Integer noOfKiosks = Utils.isNotNull(businessConfigurationVersionBean.getNoOfKiosks()) ? businessConfigurationVersionBean.getNoOfKiosks() : Constants.LIMIT_UNLIMITED;
            String dynamicLinks = businessConfigurationVersionBean.getDynamicLinks();
            Integer noOfShareableSublink = Utils.isNotNull(businessConfigurationVersionBean.getNoOfShareableSublink()) ? businessConfigurationVersionBean.getNoOfShareableSublink() : Constants.LIMIT_UNLIMITED;
            Integer noOfMultipleStaticQr = Utils.isNotNull(businessConfigurationVersionBean.getNoOfMultipleStaticQr()) ? businessConfigurationVersionBean.getNoOfMultipleStaticQr() : Constants.LIMIT_UNLIMITED;
            Integer noOfDynamicSwitchQr = Utils.isNotNull(businessConfigurationVersionBean.getNoOfDynamicSwitchQr()) ? businessConfigurationVersionBean.getNoOfDynamicSwitchQr() : Constants.LIMIT_UNLIMITED;
            Integer noOfDynamicGroupQr = Utils.isNotNull(businessConfigurationVersionBean.getNoOfDynamicGroupQr()) ? businessConfigurationVersionBean.getNoOfDynamicGroupQr() : Constants.LIMIT_UNLIMITED;
            String mfa = businessConfigurationVersionBean.getMfa();
            String advancedReport = businessConfigurationVersionBean.getAdvancedReport();
            String gamePlan = businessConfigurationVersionBean.getGamePlan();
            String previouslyUsedQuestionLibrary = businessConfigurationVersionBean.getPreviouslyUsedQuestionLibrary();
            String predefinedQuestionLibrary = businessConfigurationVersionBean.getPredefinedQuestionLibrary();
            String customThemes = businessConfigurationVersionBean.getCustomThemes();
            String chatbot = businessConfigurationVersionBean.getChatbot();
            String auditProgram = businessConfigurationVersionBean.getAuditProgram();
            String collaboration = businessConfigurationVersionBean.getCollaboration();
            String customDashboard = businessConfigurationVersionBean.getCustomDashboard();
            String rCoefficient = Utils.isNotNull(businessConfigurationVersionBean.getRCoefficient()) ? businessConfigurationVersionBean.getRCoefficient() : Constants.ZERO_STRING;
            String focusMetric = Utils.isNotNull(businessConfigurationVersionBean.getFocusMetric()) ? new JSONObject(businessConfigurationVersionBean.getFocusMetric()).toString() : new JSONObject().toString();
            String uniqueLinks = Utils.isNotNull(businessConfigurationVersionBean.getUniqueLinks()) ? new JSONObject(businessConfigurationVersionBean.getUniqueLinks()).toString() : new JSONObject().toString();
            String programFeatures = Utils.isNotNull(businessConfigurationVersionBean.getProgramFeatures()) ? new JSONObject(businessConfigurationVersionBean.getProgramFeatures()).toString() : new JSONObject().toString();
            String sso = Utils.isNotNull(businessConfigurationVersionBean.getSso()) ? new JSONObject(businessConfigurationVersionBean.getSso()).toString() : new JSONObject().toString();
            String closeLoop = businessConfigurationVersionBean.getCloseLoop();
            String programThrottling = businessConfigurationVersionBean.getProgramThrottling();
            String notes = businessConfigurationVersionBean.getNotes();
            String submissionDelay = businessConfigurationVersionBean.getSubmissionDelay();
            String refreshKioskQr = businessConfigurationVersionBean.getRefreshKioskQr();
            String aiSurveysLimit = businessConfigurationVersionBean.getAiSurveysLimit();
            String taskManager = businessConfigurationVersionBean.getTaskManager();
            //DTV-12899
            String aiSurveys = businessConfigurationVersionBean.getAiSurveys();
            String lti = businessConfigurationVersionBean.getLti();
            String ltiConfig = Utils.isNotNull(businessConfigurationVersionBean.getLtiConfig()) ? new JSONObject(businessConfigurationVersionBean.getLtiConfig()).toString() : null;
            //DTV-13080, 12386
            String responseQuota = businessConfigurationVersionBean.getResponseQuota();
            String recurrence = businessConfigurationVersionBean.getRecurrence();
            String customDashboardBuilder = businessConfigurationVersionBean.getCustomDashboardBuilder();

            //  int textAnalyticsConfigId = textAnalyticsConfigService.getLatestConfigId(businessId);
            int textAnalyticsConfigId = (Utils.isNotNull(businessConfigurationVersionBean.getTextAnalyticsConfigId())) ? businessConfigurationVersionBean.getTextAnalyticsConfigId() : 0;

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(Constants.CREATE_BUSINESS_VERSION_CONFIG, Statement.RETURN_GENERATED_KEYS);
                ps.setInt(1, businessId);
                ps.setString(2, email);
                ps.setString(3, sms);
                ps.setString(4, qrCode);
                ps.setString(5, shareableLink);
                ps.setString(6, textAnalytics);
                ps.setString(7, logic);
                ps.setString(8, respondent);
                ps.setInt(9, userId);
                ps.setTimestamp(10, timestamp);
                ps.setString(11, businessConfigVersionUUID);
                ps.setString(12, segment);
                ps.setString(13, kiosk);
                ps.setString(14, metadataQuestion);
                ps.setString(15, mobileApp);
                ps.setString(16, emotionAnalysis);
                ps.setString(17, intentAnalysis);
                ps.setString(18, trigger);
                ps.setString(19, advancedSchedule);
                ps.setString(20, english);
                ps.setString(21, arabic);
                ps.setString(22, multiSurveys);
                ps.setString(23, bitly);
                ps.setString(24, webHooks);
                ps.setString(25, preferredMetric);
                ps.setString(26, notification);
                ps.setString(27, categoryAnalysis);
                ps.setString(28, advancedTextAnalytics);
                ps.setInt(29, noOfUsers);
                ps.setInt(30, noOfActivePrograms);
                ps.setInt(31, noOfResponses);
                ps.setInt(32, noOfMetrics);
                ps.setInt(33, noOfEmailsSent);
                ps.setInt(34, noOfSms);
                ps.setInt(35, noOfApi);
                ps.setInt(36, noOfFilters);
                ps.setInt(37, noOfTriggers);
                ps.setInt(38, dataRetention);
                ps.setInt(39, noOfLists);
                ps.setInt(40, noOfRecipients);
                ps.setString(41, hippa);
                ps.setString(42, cohortAnalysis);
                ps.setString(43, advancedTemplates);
                ps.setString(44, metricCorrelation);
                ps.setString(45, apiKey);
                ps.setString(46, whatfix);
                ps.setString(47, multipleSublink);
                ps.setString(48, advancedAnalysis);
                ps.setString(49, exportLinks);
                ps.setString(50, respondentTracker);
                ps.setString(51, fromCustomization);
                ps.setString(52, integrations);
                ps.setInt(53, noOfIntegrations);
                ps.setInt(54, noOfKiosks);
                ps.setString(55, dynamicLinks);
                ps.setInt(56, noOfShareableSublink);
                ps.setInt(57, noOfMultipleStaticQr);
                ps.setInt(58, noOfDynamicSwitchQr);
                ps.setInt(59, noOfDynamicGroupQr);
                ps.setString(60, mfa);
                ps.setString(61, focusMetric);
                ps.setString(62, advancedReport);
                ps.setString(63, previouslyUsedQuestionLibrary);
                ps.setString(64, predefinedQuestionLibrary);
                ps.setString(65, customThemes);
                ps.setString(66, chatbot);
                ps.setString(67, auditProgram);
                ps.setString(68, collaboration);
                ps.setString(69, customDashboard);
                ps.setString(70, whatsapp);
                ps.setInt(71, noOfWhatsapp);
                ps.setString(72, rCoefficient);
                ps.setString(73, closeLoop);
                ps.setString(74, programThrottling);
                ps.setInt(75, textAnalyticsConfigId);
                ps.setString(76, gamePlan);
                ps.setString(77, notes);
                ps.setString(78, submissionDelay);
                ps.setString(79, uniqueLinks);
                ps.setString(80, programFeatures);
                ps.setString(81, refreshKioskQr);
                ps.setString(82, aiSurveysLimit);
                ps.setString(83, taskManager);
                ps.setString(84, aiSurveys);
                ps.setString(85, lti);
                ps.setString(86, ltiConfig);
                ps.setString(87, sso);
                ps.setString(88, responseQuota);
                ps.setString(89, recurrence);
                ps.setString(90, customDashboardBuilder);
                return ps;
            }, keyHolder);
            configurationsVersionId = Objects.requireNonNull(keyHolder.getKey()).intValue();

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("END createBusinessVersionConfigurations");
        return configurationsVersionId;
    }

    /**
     * Get business configuration by business uuid
     *
     * @param versionId
     * @return
     */
    public Map getBusinessConfigurationsVersionById(int versionId) {
        logger.info("begin retrieving configs version by primary id");
        Map businessConfigMap = new HashMap();
        try {
            businessConfigMap = jdbcTemplate.queryForMap(Constants.GET_BUSINESS_VERSION_CONFIG_BY_ID, versionId);
        } catch (EmptyResultDataAccessException emptyResultDataAccessException) {
            return businessConfigMap;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("end retrieving configs version by primary id");
        return businessConfigMap;
    }

    /**
     * Get business configuration by business uuid
     *
     * @return
     */
    public List getBusinessConfigurationsVersionByBusinessId(int businessId,int upperLimit,int length) {
        logger.info("START getBusinessConfigurationsVersionByBusinessId - DAO");
        List result = new ArrayList();
        try {
            result = jdbcTemplate.queryForList(Constants.GET_BUSINESS_VERSION_CONFIG, businessId, upperLimit, length);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("END getBusinessConfigurationsVersionByBusinessId - DAO");
        return result;
    }

    /**
     * Get business configuration by business uuid
     *
     * @return
     */
    public List getBusinessConfigurationsVersionByVersionId(int businessId, String versionUniqueId) {
        logger.info("START getBusinessConfigurationsVersionByVersionId - DAO");
        List result = new ArrayList();
        try {
            result = jdbcTemplate.queryForList(Constants.GET_BUSINESS_VERSION_CONFIG_BY_VERSIONID, businessId, versionUniqueId);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("END getBusinessConfigurationsVersionByVersionId - DAO");
        return result;
    }
}
