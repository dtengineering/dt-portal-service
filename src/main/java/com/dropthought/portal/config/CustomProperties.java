package com.dropthought.portal.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;


@Configuration
@Component
public class CustomProperties {

    @Value("${rest.uri.disallow.characters}")
    private String uriVulnerablities;

    @Value("${rest.payload.disallow.characters}")
    private String payloadVulnerablities;

    public String getUriVulnerablities() {
        return uriVulnerablities;
    }

    public void setUriVulnerablities(String uriVulnerablities) {
        this.uriVulnerablities = uriVulnerablities;
    }

    public String getPayloadVulnerablities() {
        return payloadVulnerablities;
    }

    public void setPayloadVulnerablities(String payloadVulnerablities) {
        this.payloadVulnerablities = payloadVulnerablities;
    }

}
