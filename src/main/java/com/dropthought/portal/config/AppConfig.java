package com.dropthought.portal.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import javax.sql.DataSource;

@Configuration
public class AppConfig {

    /**
     * DT database config
     *
     */
    @Bean(name = "dsDt")
    @ConfigurationProperties(prefix = "spring.dt")
    public DataSource prodDataSource() {
        return DataSourceBuilder.create().build();
    }


    @Bean(name = "jdbcDt")
    @Autowired
    public JdbcTemplate prodJdbcTemplate(@Qualifier("dsDt") DataSource datasource) {
        return new JdbcTemplate(datasource);
    }


    @Bean(name = "jdbcNamedDt")
    @Autowired
    public NamedParameterJdbcTemplate prodJdbcNamedTemplate(@Qualifier("dsDt") DataSource datasource) {
        return new NamedParameterJdbcTemplate(datasource);
    }

    /**
     * portal database config
     *
     */

    @Bean(name = "dsPortal")
    @ConfigurationProperties(prefix = "spring.portal")
    public DataSource prodPortalDataSource() {
        return DataSourceBuilder.create().build();
    }


    @Bean(name = "jdbcPortal")
    @Autowired
    public JdbcTemplate prodPortalJdbcTemplate(@Qualifier("dsPortal") DataSource datasource) {
        return new JdbcTemplate(datasource);
    }

    @Bean(name = "jdbcNamedPortal")
    @Autowired
    public NamedParameterJdbcTemplate prodPortalJdbcNamedTemplate(@Qualifier("dsPortal") DataSource datasource) {
        return new NamedParameterJdbcTemplate(datasource);
    }


    @Bean(name = "dsWeb")
    @ConfigurationProperties(prefix = "spring.web")
    public DataSource webDataSource() {
        return DataSourceBuilder.create().build();
    }


    @Bean(name = "jdbcWeb")
    @Autowired
    public JdbcTemplate webJdbcTemplate(@Qualifier("dsWeb") DataSource datasource) {
        return new JdbcTemplate(datasource);
    }

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }


}
