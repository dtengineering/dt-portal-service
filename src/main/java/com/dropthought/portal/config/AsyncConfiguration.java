package com.dropthought.portal.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Configuration
@EnableAsync
public class AsyncConfiguration  {
    @Bean
    public Executor asyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        //sets the number of core threads in the thread pool. This is the number of threads that will always be active, even if there are no tasks to execute.
        executor.setCorePoolSize(2);
        //sets the maximum number of threads that can be in the thread pool. If the number of tasks exceeds the number of core threads, additional threads will be created up to this maximum number.
        executor.setMaxPoolSize(8);
        executor.setThreadNamePrefix("Executor::");
        executor.initialize();
        return executor;
    }
}
