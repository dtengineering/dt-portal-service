package com.dropthought.portal.config;


import com.dropthought.portal.model.DTAppResponse;
import com.dropthought.portal.util.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@Component
@Order(1)
public class CustomAPIFilter implements Filter {

    private static final Logger logger = LoggerFactory.getLogger(CustomAPIFilter.class);

    private ObjectMapper mapper = new ObjectMapper();

    private static final String INVALID_REQUEST = "Invalid request";

    private static final String ERROR_IN_SERVER = "Error in server";

    private static final String VULNERABLE_CHARACTER_IN_URL = "Found vulnerable character in uri";

    private static final String VULNERABLE_CHARACTER_IN_REQUEST_PARAMETERS = "Found vulnerable character in request parameter";

    private static final String VULNERABLE_CHARACTER_IN_PAYLOAD = "Found vulnerable character in payload";

    private static final String VULNERABLE_KEY_IN_URL = "rest.uri.disallow.characters";

    private static final String VULNERABLE_KEY_IN_PAYLOAD = "rest.payload.disallow.characters";

    //private static final String ALLOWABLE_CHARACTER_IN_PAYLOAD = "rest.payload.allow.characters";

    private String uriVulnerablities;

    private String payloadVulnerablities;

    //private String payloadAllowHtmlTags;

    @Autowired
    public CustomAPIFilter(Environment env) {
        String payloadDisallowChars = env.getRequiredProperty(VULNERABLE_KEY_IN_PAYLOAD);
        String uriDisallowChars = env.getRequiredProperty(VULNERABLE_KEY_IN_URL);
        //String payloadAllowHtmlTags = env.getRequiredProperty(ALLOWABLE_CHARACTER_IN_PAYLOAD);
        this.payloadVulnerablities = payloadDisallowChars;
        this.uriVulnerablities = uriDisallowChars;
        //this.payloadAllowHtmlTags = payloadAllowHtmlTags;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        /**
         * 1.Forced convertion of original request / response object
         */
        boolean validRequest;
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        Optional<String> vulnerableCharacter = null;
        List<String> errors = new ArrayList<>();
        String originRequestHeader = null;
        String authorizationRequestHeader = null;

        try {

            String uri = UriUtils.decode(httpServletRequest.getRequestURI(), "UTF-8");
            String uriContextPath = httpServletRequest.getContextPath();
            Map<String, String[]> paramMap = httpServletRequest.getParameterMap();
            String contentType = Utils.notEmptyString(httpServletRequest.getContentType()) ?
                    httpServletRequest.getContentType() : MediaType.APPLICATION_JSON_VALUE;
            originRequestHeader = httpServletRequest.getHeader("Origin");
            authorizationRequestHeader = httpServletRequest.getHeader("Authorization");
            //Read request.getBody() as many time you need
            request = new CustomRequestWrapper(httpServletRequest);
            //chain.doFilter(request, response);
            String originalRequestBody = IOUtils.toString(request.getInputStream());
            List<String> vulnerableListInPayload = Arrays.asList(payloadVulnerablities.split(","));
            List<String> vulnerableListInURI = Arrays.asList(uriVulnerablities.split(","));
            //List<String> allowCharsInPayload = Arrays.asList(payloadAllowHtmlTags.split(","));

            //Validate Vulnerabilities in the Path Parameters & URL
            vulnerableCharacter = vulnerableListInURI.stream().filter(e -> uri.contains(e)).findFirst();
            validRequest = vulnerableCharacter.isPresent() ? false : true;

            if (!validRequest) {
                errors.add(VULNERABLE_CHARACTER_IN_URL + " : " + vulnerableCharacter.get() );
            }

            //Validate Vulnerabilities in the Request Parameters
            if (!paramMap.isEmpty() && contentType.contains(MediaType.APPLICATION_JSON_VALUE)) {

                for (Map.Entry<String,String[]> entry : paramMap.entrySet()) {

                    vulnerableCharacter = vulnerableListInPayload.stream()
                            .filter(e -> Arrays.toString(entry.getValue()).contains(e))
                            .findFirst();
                    validRequest = vulnerableCharacter.isPresent() ? false : true;
                    if (!validRequest) {
                        errors.add(VULNERABLE_CHARACTER_IN_REQUEST_PARAMETERS + " " + entry.getKey() + " : " + vulnerableCharacter.get() );
                    }
                }
            }

            //Validate Vulnerabilities in the request payload
            if (Utils.notEmptyString(originalRequestBody) && contentType.contains(MediaType.APPLICATION_JSON_VALUE)) {

                vulnerableCharacter = vulnerableListInPayload.stream()
                        .filter(e -> originalRequestBody.contains(e))
                        .findFirst();
                validRequest = vulnerableCharacter.isPresent() ? false : true;

                if (!validRequest) {
                    errors.add(VULNERABLE_CHARACTER_IN_PAYLOAD + " : " + vulnerableCharacter.get() );
                }

            }

            if (!errors.isEmpty()) {
                DTAppResponse errorResponse = new DTAppResponse(false, INVALID_REQUEST, errors);
                resp.reset();
                resp.setStatus(HttpStatus.BAD_REQUEST.value());
                resp.addHeader(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");
                resp.addHeader(HttpHeaders.ACCEPT, "application/json, text/plain, */*");
                if (Utils.notEmptyString(authorizationRequestHeader) ) {
                    resp.addHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");
                    resp.addHeader(HttpHeaders.AUTHORIZATION, authorizationRequestHeader);
                }
                if (Utils.notEmptyString(originRequestHeader)) {
                    resp.addHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, originRequestHeader);
                }
                resp.addHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, "Access-Control-Allow-Origin, " +
                        "Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Access-Control-Allow-Methods");
                resp.getWriter().write(mapper.writeValueAsString(errorResponse));
                return;
            }

            chain.doFilter(request, response);
        } catch (Exception ex) {
            logger.error("Custom API Filter error : " + ex.getMessage());
            ex.printStackTrace();
            DTAppResponse errorResponse = new DTAppResponse(false, ERROR_IN_SERVER);
            resp.reset();
            resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            resp.addHeader(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");
            resp.addHeader(HttpHeaders.ACCEPT, "application/json, text/plain, */*");
            if (Utils.notEmptyString(authorizationRequestHeader) ) {
                resp.addHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");
                resp.addHeader(HttpHeaders.AUTHORIZATION, authorizationRequestHeader);
            }
            if (Utils.notEmptyString(originRequestHeader)) {
                resp.addHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, originRequestHeader);
            }
            resp.addHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, "Access-Control-Allow-Origin, " +
                    "Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Access-Control-Allow-Methods");
            resp.getWriter().write(mapper.writeValueAsString(errorResponse));
            return;
        }

    }

}
