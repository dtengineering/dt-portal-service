package com.dropthought.portal.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    protected final Logger logger = LoggerFactory.getLogger(SecurityConfig.class);

    @Autowired
    Environment env;

    @Value("${cors}")
    private String allowedHosts;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        String allowedDomains = allowedHosts.replace(",", " ");
        http
                .addFilterBefore(new CustomAPIFilter(env), UsernamePasswordAuthenticationFilter.class)
                .csrf().disable()
                .headers()
                .xssProtection()
                .and()
                .contentSecurityPolicy("script-src 'self'" + allowedDomains + "; default-src https: " + allowedDomains);

        http.cors().configurationSource(corsFilter());
    }

    @Bean
    public CorsConfigurationSource corsFilter() {

        logger.info("enabling cors configuration from security config");
        logger.info("allowed hosts {}", allowedHosts);
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList(allowedHosts.split(","))); // Replace this with your actual frontend URL
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS", "PATCH", "HEAD"));
        configuration.setAllowedHeaders(Arrays.asList("Content-Type", "X-Requested-With", "Accept", "Origin", "Authorization",
                "Access-Control-Request-Method", "Access-Control-Request-Headers", "Access-Control-Allow-Origin"));
        configuration.addExposedHeader("Access-Control-Allow-Origin");
        configuration.addExposedHeader("Access-Control-Allow-Headers");
        configuration.addExposedHeader("Access-Control-Allow-Methods");
        logger.info("cors configuration {}", configuration.getAllowedOrigins());
        configuration.setMaxAge(3600L);
        configuration.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);

        return source;
    }
}
