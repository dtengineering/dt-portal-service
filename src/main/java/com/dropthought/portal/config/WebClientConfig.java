package com.dropthought.portal.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

@Configuration
public class WebClientConfig {

    @Value("${idm.okta.url}")
    private String idmOKTAUrl;

    @Value("${dt.edu.url}")
    private String dtEduUrl;

    @Bean("oktaClient")
    @Primary
    public WebClient getIdmOktaWebClient() {
        try {
            return WebClient.builder()
                    .baseUrl(idmOKTAUrl)
                    .clientConnector(new ReactorClientHttpConnector(HttpClient.create()))
                    .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .build();
        } catch (Exception e) {
            return null;
        }
    }

    @Bean("ltiClient")
    @Primary
    public WebClient getLTIWebClient() {
        try {
            return WebClient.builder()
                    .baseUrl(dtEduUrl)
                    .clientConnector(new ReactorClientHttpConnector(HttpClient.create()))
                    .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .build();
        } catch (Exception e) {
            return null;
        }
    }
}
