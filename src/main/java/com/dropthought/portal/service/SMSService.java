package com.dropthought.portal.service;

import com.dropthought.portal.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class SMSService {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RestTemplateService restTemplateService;

    @Autowired
    @Qualifier("jdbcDt")
    private JdbcTemplate jdbcTemplate;

    /**
     * retrieve the business id based upon phone number
     * @param phoneNumber
     * @return
     */
    public Map<String, Object> getBusinessByPhoneNumber(String phoneNumber) {
        logger.info("begin getBusinessByPhoneNumber {}",phoneNumber);
        Map<String, Object> businessMap = new HashMap<>();
        try {
            phoneNumber = Utils.retriveOnlyNumbersPhone(phoneNumber);
            String sql = "select a.account_business_id as business_id, b.business_uuid as business_uuid, u.user_uuid as user_uuid from account a inner join users u on u.user_id = a.account_user_id inner join business b on b.business_id = a.account_business_id where u.phone= ? ";
            List records = jdbcTemplate.queryForList(sql, new Object[]{phoneNumber});
            if (records.size() > 0) {
                businessMap = (Map<String, Object>) records.get(0);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return businessMap;
    }

    /**
     * check if the phone number is a valid DT customer
     * @param phoneNumber
     * @return
     */
    public boolean isValidCustomerByPhoneNumber(String phoneNumber) {
        logger.info("begin isValidCustomerByPhoneNumber {}",phoneNumber);
        boolean isValid = false;
        try {
            phoneNumber = Utils.retriveOnlyNumbersPhone(phoneNumber);
            String sql = "SELECT COUNT(*) FROM users WHERE phone = ?";
            int count = jdbcTemplate.queryForObject(sql, new Object[]{phoneNumber}, Integer.class);
            if (count > 0) {
                isValid = true;
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return isValid;
    }

    /**
     * make a POST request to the Eliza chatbot API
     * @param query
     * @return a map containing the response body
     */
    public String callEliza(String query, String phoneNumber, String uri) {
        Map inputMap = new HashMap();
//        Map<String, Object> businessVals = this.getBusinessByPhoneNumber(phoneNumber);
        String response = "";
        try{
            inputMap.put("program_uuid", "");
            Map<String, Object> businessVals = this.getBusinessByPhoneNumber(phoneNumber);
            if (businessVals.size() > 0 && businessVals.containsKey("business_uuid")) {
                inputMap.put("business_uuid", businessVals.get("business_uuid"));
            }
            inputMap.put("accessToken", "");
            if (businessVals.size() > 0 && businessVals.containsKey("business_id")) {
                inputMap.put("business_id", businessVals.get("business_id"));
            }
            inputMap.put("query", query);


//            try {
//                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//                String currentTimeVar = simpleDateFormat.format(timestamp);
//
//                inputMap.put("timestamp", currentTimeVar);
//            } catch (Exception e){
//                logger.info(e.getMessage());
//            }

            String uuid = (String) businessVals.get("user_uuid");
            String finalUri = uri + uuid;
            logger.info("final uri {}", finalUri);
            Map<String, Object> output = restTemplateService.callRestApi(inputMap, finalUri, HttpMethod.POST);
            if(output.containsKey("response"))
                response = (String)output.get("response");


        }catch (Exception e) {
            logger.info(e.getMessage());
            response = e.getMessage();
        }
        return response;

    }
}