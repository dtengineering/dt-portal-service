package com.dropthought.portal.service;

import com.dropthought.portal.model.Pricing;
import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.TokenUtility;
import com.dropthought.portal.util.Utils;
import com.stripe.StripeClient;
import com.stripe.exception.CardException;
import com.stripe.exception.StripeException;
import com.stripe.model.PaymentMethod;
import com.stripe.model.PaymentIntent;
import com.stripe.model.StripeCollection;
import com.stripe.param.PaymentIntentCreateParams;
import com.stripe.param.PaymentMethodListParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class PaymentService extends BaseService {

    // initialize logger
    private static final Logger logger = LoggerFactory.getLogger(PaymentService.class);

    /**
     * JdbcTemplate for DB Connectivity.
     */
    @Autowired
    @Qualifier("jdbcDt")
    JdbcTemplate jdbcTemplate;

    @Autowired
    private TokenUtility tokenUtility;

    @Value("${stripe.secret.key.live}")
    private String stripePrivateKeyLive;

    @Value("${stripe.secret.key.test}")
    private String stripePrivateKeyTest;

    @Value("${stripe.mode}")
    private String stripeMode;

    private StripeClient client = null;

    public StripeClient getStripeClient(){
        logger.info("getStripeClient() - starts");
        if(this.client == null){
            logger.info("client is null");
            this.client = stripeMode.equals("live") ? new StripeClient(stripePrivateKeyLive) : new StripeClient(stripePrivateKeyTest);
        }else{
            logger.info("client is not null");
        }
        return this.client;
    }

    /**
     * Function to generate the payment details for the business
     * This function will run once in a month to add the upcoming payment details for the business
     * in balance_{businessuuid} table
     */
    public void generatePayment() {

        logger.info("Generating payment details for the business - starts");

        Map<Integer, Object> pricingMap = new HashMap<>();
        Map<Integer, String> planNameMap = new HashMap<>();
       try {
           // identify the business which has plan_config_id > 0 and is active
           String query = "select * from business where plan_config_id > 0 and state = '1'";
           List<Map<String, Object>> businesses = jdbcTemplate.queryForList(query);
           Set<Integer> planConfigIds = new HashSet<>();
           planConfigIds = businesses.stream().map(business -> (int) business.get("plan_config_id")).collect(Collectors.toSet());
           // get the pricing details for the plan_config_ids
           if (!planConfigIds.isEmpty()) {
                String planConfigQuery = "select plan_config_id, pricing, plan_name from plan_configurations where plan_config_id in" +
                        " (" + planConfigIds.stream().map(String::valueOf).collect(Collectors.joining(",")) + ")";
                List<Map<String, Object>> planConfigs = jdbcTemplate.queryForList(planConfigQuery);
                for (Map<String, Object> planConfig : planConfigs) {
                     int planConfigId = (int) planConfig.get("plan_config_id");
                     String planName = (String) planConfig.get("plan_name");
                     String pricingStr = Utils.isNotNull(planConfig.get("pricing")) &&
                             !planConfig.get("pricing").toString().isEmpty() &&
                             !planConfig.get("pricing").toString().equals("0") ? planConfig.get("pricing").toString() : "";
                        if (!pricingStr.isEmpty()) {
                            Pricing pricing = mapper.readValue(pricingStr, Pricing.class);
                            pricingMap.put(planConfigId, pricing);
                            planNameMap.put(planConfigId, planName);
                        }
                }
           }

           for (Map<String, Object> eachBusinessMap : businesses) {
               // get the business uuid
               String businessUuid = (String) eachBusinessMap.get("business_uuid");
               int businessId = (int) eachBusinessMap.get("business_id");
               int planConfigId = (int) eachBusinessMap.get("plan_config_id");
               String contractStartDateStr = eachBusinessMap.get("contract_start_date").toString();
               LocalDate nextPaymentDate = computeNextPaymentDate(businessUuid, contractStartDateStr, "Yearly");

               // filter business those who are not paid for the current payment interval.
               // Verify the status and last transaction in the transaction_{businessuuid} table
               boolean transactionMade = getLastTransactionWithinPaymentInterval(businessUuid, contractStartDateStr,
                       nextPaymentDate.toString());

               if (transactionMade) {
                   continue;
               }

               Map<String, Object> balanceMap = new HashMap<>();
               if (!pricingMap.isEmpty() && pricingMap.containsKey(planConfigId) && planNameMap.containsKey(planConfigId)) {

                   String planName = planNameMap.get(planConfigId);

                   // function to calculate the payment details
                    Pricing pricing = (Pricing) pricingMap.get(planConfigId);
                    int totalAmount = pricing.getPrice();
                    totalAmount = totalAmount * 12;// total amount should be calculated yearly and based on no of users count in the business configuration
                    // if plan id dt lite then we need to calculate the total amount based on the user count
                    if (planName.equals(Constants.DT_LITE_PLAN_NAME)) {

                        String businessConfigQuery = "select no_of_users from business_configurations " +
                                "where business_id = " + businessId + " order by created_time desc limit 1";
                        int userCount = jdbcTemplate.queryForObject(businessConfigQuery, Integer.class);
                        if (userCount != -1 && userCount > 0) {
                            totalAmount = totalAmount * userCount;
                        }
                    }
                    balanceMap.put("amount", totalAmount);
                    balanceMap.put("currency", pricing.getCurrencyCode());
                    balanceMap.put("balance_uuid", tokenUtility.generateUUID());
                    balanceMap.put("next_payment_date", nextPaymentDate);
               }

               if (balanceMap.size() > 0) {
                   // insert the payment details into the balance_{businessuuid} table for the business
                   // where there is no data present within the payment interval
                    String replacedBusinessUuid = businessUuid.replace("-", "_");
                    String balanceTableName = "balance_" + replacedBusinessUuid;
                    String insertQuery = "insert into " + balanceTableName +
                            " (date, amount, currency, created_time, balance_uuid, next_payment_date, status) values (?, ?, ?, ?, ?,?,?)";
                    jdbcTemplate.update(insertQuery, new Timestamp(System.currentTimeMillis()), balanceMap.get("amount"), balanceMap.get("currency"),
                            new Timestamp(System.currentTimeMillis()), balanceMap.get("balance_uuid"), balanceMap.get("next_payment_date"), 0);
               }
           }
       } catch (Exception e) {
           logger.error("Error in generating payment details", e);
       }
        logger.info("Generating payment details for the business - ends");
    }

    /**
     * Function to verify the last transaction for the business within the payment interval
     * @param businessUUID
     * @param contractEndDate
     * @param nextPaymentDate
     * @return
     */
    private boolean getLastTransactionWithinPaymentInterval(String businessUUID,
                                                                      String contractEndDate, String nextPaymentDate) {

        boolean isTransactionPresent = false;
        try {
            String replacedBusinessUuid = businessUUID.replace("-", "_");
            String transactionTableName = "transaction_" + replacedBusinessUuid;
            String query = "select * from " + transactionTableName + " where status = 1 " +
                    "AND date BETWEEN ? and ?";
            List<Map<String, Object>> transactionList = jdbcTemplate.queryForList(query, contractEndDate, nextPaymentDate);
            if (!transactionList.isEmpty()) {
                isTransactionPresent = true;
            }
        } catch (Exception e) {
            logger.error("Error in filtering business by last transaction", e);
        }

        return isTransactionPresent;
    }

    /**
     * Function to compute the next payment date, if any transaction is already present for the business
     * then get the last transaction date and calculate the next payment date
     * else calculate the next payment date from the contract start date
     *
     * @param businessUuid
     * @param contractStartDateStr
     * @return
     */
    private LocalDate computeNextPaymentDate(String businessUuid, String contractStartDateStr, String paymentCycle) {

        LocalDate nextPaymentDate = null;

        try {
            String replacedBusinessUuid = businessUuid.replace("-", "_");
            String transactionTableName = "transaction_" + replacedBusinessUuid;
            String query = "select * from " + transactionTableName + " where status = 1 order by created_time desc limit 1";
            Map<String, Object> transactionMap = jdbcTemplate.queryForMap(query);
            if (!transactionMap.isEmpty()) {
                String lastTransactionDateStr = transactionMap.get("created_time").toString();
                Date lastTransactionDate = Utils.convertStringToDate(lastTransactionDateStr, "yyyy-MM-dd");
                LocalDate lastTransactionDateLocal = Utils.convertDateToLocalDate(lastTransactionDate);
                // If the payment cycle is monthly then add 1 month and if the payment cycle is yearly then add 1 year
                if (paymentCycle.equals("Monthly")) {
                    nextPaymentDate = lastTransactionDateLocal.plusMonths(1);
                } else if (paymentCycle.equals("Yearly")) {
                    nextPaymentDate = lastTransactionDateLocal.plusYears(1);
                }
            } else {
                Date contractStartDate = Utils.convertStringToDate(contractStartDateStr, "yyyy-MM-dd");
                LocalDate contractStartDateLocal = Utils.convertDateToLocalDate(contractStartDate);
                // If the payment cycle is monthly then add 1 month and if the payment cycle is yearly then add 1 year
                if (paymentCycle.equals("Monthly")) {
                    nextPaymentDate = contractStartDateLocal.plusMonths(1);
                } else if (paymentCycle.equals("Yearly")) {
                    nextPaymentDate = contractStartDateLocal.plusYears(1);
                }
            }
        } catch (Exception e) {
            logger.error("Error in computing next payment date", e);
            return null;
        }

        return nextPaymentDate;
    }

    /**
     * Method to verify the payment status for the business in transaction table
     * @param businessUUID
     * @return
     */
    public boolean verifyPaymentStatus(String businessUUID) {

        boolean isPaymentDone = false;
        try {
            String replacedBusinessUuid = businessUUID.replace("-", "_");
            String transactionTableName = "transaction_" + replacedBusinessUuid;
            String query = "select * from " + transactionTableName + " where status = 1 order by created_time desc limit 1";
            Map<String, Object> transactionMap = jdbcTemplate.queryForMap(query);
            if (!transactionMap.isEmpty()) {
                isPaymentDone = true;
                logger.info("Payment is done for the business : {}", businessUUID);
            }
        } catch (Exception e) {
            logger.error("Error in verifying payment status", e);
        }

        return isPaymentDone;
    }

    /**
     * Method to identify business to make payment
     * identify business from the business_renewal_schedule table which has new_start_time is less than current time
     * and renewal status is empty and payment status is "InComplete"
     * @return
     */
    public List<Map<String, Object>> identifyBusinessToMakePayment() {

        List<Map<String, Object>> businessList = new ArrayList<>();
        try {
            String query = "select * from business_renewal_schedule where renewal_time < now() and renewal_status = '' and payment_status = 'InComplete'";
            businessList = jdbcTemplate.queryForList(query);
        } catch (Exception e) {
            logger.error("Error in identifying business to make payment", e);
        }

        return businessList;
    }

    /**
     * Method to create a customer in stripe
     * @param businessId
     * @return
     */
    public String returnCustomerIdByBusinessId(int businessId) {
        String customerId = "";
        try {
            String query = "select customer_id from dtlite_business_customer_map where business_id = ?";
            customerId = jdbcTemplate.queryForObject(query, new Object[]{businessId}, String.class);
            logger.info("Customer Id for the business : {} is : {}", businessId, customerId);
        } catch (Exception e) {
            logger.error("Error in returnCustomerIdByBusinessId() : " + e.getMessage());
        }

        return customerId;
    }

    /**
     * Method to get all payment methods for the customer
     * @param customerId
     * @return
     */
    public List<PaymentMethod> getPaymentMethods(String customerId) {
        List<PaymentMethod> paymentmethods = new ArrayList<>();
        try {
            StripeClient client = this.getStripeClient();

            PaymentMethodListParams params = PaymentMethodListParams.builder()
                    .setCustomer(customerId)
                    .setType(PaymentMethodListParams.Type.CARD)
                    .build();
            StripeCollection<PaymentMethod> paymentMethodCollection = client.paymentMethods().list(params);
            paymentmethods = paymentMethodCollection.getData();
            logger.info("Payment methods for the customerId : " + customerId + " : " + paymentmethods.size());
        } catch (StripeException e) {
            logger.error("Error in getting payment methods for the customerId : " + customerId + " : " + e.getMessage());
        }

        return paymentmethods;
    }

    /**
     * Method to get last four digits of the card for the business
     * @param businessId
     * @return
     */
    public String getCardLastFourDigits(int businessId) {
        String lastFourDigits = "";
        try {
            String customerId = this.returnCustomerIdByBusinessId(businessId);
            if (Utils.notEmptyString(customerId)) {
                logger.info("Customer Id for the business : " + businessId + " : " + customerId);
                List<PaymentMethod> paymentMethods = this.getPaymentMethods(customerId);
                if (paymentMethods.size() > 0) {
                    PaymentMethod paymentMethod = paymentMethods.get(0);
                    lastFourDigits = paymentMethod.getCard().getLast4();
                }
            }
        } catch (Exception e) {
            logger.error("Error in getting last four digits of the card for the business : " + businessId + " : " + e.getMessage());
        }

        return lastFourDigits;
    }

    /**
     * Method to get the first successful transaction for the business
     * @param businessId
     * @return
     */
    public boolean isTransactionsMadeForBusiness(int businessId, String businessUUID) {

        boolean isTransactionsMade = false;
        try {
            String replacedBusinessUuid = businessUUID.replace("-", "_");
            String tableName = "transaction_" + replacedBusinessUuid;
            String sql = "select count(*) from " + tableName + " where status = 1";
            int count = jdbcTemplate.queryForObject(sql, new Object[]{businessId}, Integer.class);
            if (count > 0) {
                isTransactionsMade = true;
            }
        } catch (Exception e) {
            logger.error("Error in isTransactionsMadeForBusiness() : " + e.getMessage());
        }
        return isTransactionsMade;
    }

    /**
     * create payment using paymentMethodId and customerId
     * @param customerId
     * @param paymentMethodId
     * @param amount
     * @param currency
     * @return
     */
    public String createPayment(String customerId, String paymentMethodId, long amount, String currency) {

        String paymentStatus = "";
        try {
            amount = amount * 100; // convert amount to cents
            StripeClient client = this.getStripeClient();
            PaymentIntentCreateParams params =
                    PaymentIntentCreateParams.builder()
                            .setAmount(amount)
                            .setCurrency(currency)
                            .setCustomer(customerId)
                            .setAutomaticPaymentMethods(
                                    PaymentIntentCreateParams.AutomaticPaymentMethods.builder().setEnabled(true).build()
                            )
                            .setCustomer(customerId)
                            .setPaymentMethod(paymentMethodId)
                            .setConfirm(true)
                            .setOffSession(true)
                            .build();
            try {
                client.paymentIntents().create(params);
                paymentStatus = "Payment Successful";
            } catch (CardException e) {
                // Error code will be authentication_required if authentication is needed
                String paymentIntentId = e.getStripeError().getPaymentIntent().getId();
                PaymentIntent paymentIntent = PaymentIntent.retrieve(paymentIntentId);
                paymentStatus = "Payment Failed";
            } catch (StripeException e) {
                logger.error("Error in creating payment : " + e.getMessage());
                paymentStatus = "Payment Failed";
            }
        } catch (StripeException e) {
            throw new RuntimeException(e);
        }

        return paymentStatus;
    }

    /**
     *Method to get the latest renewal schedule by business id
     * @param businessId
     * @return
     */
    public Map<String, Object> getBusinessRenewalSchedule(int businessId) {

        Map<String, Object> scheduleMap = new HashMap<>();
        try {
            String sql = "select * from business_renewal_schedule where business_id = ? and payment_status = ? order by id desc";
            List schedules = jdbcTemplate.queryForList(sql, new Object[]{businessId, "InComplete"});
            if (schedules.size() > 0) {
                scheduleMap = (Map) schedules.get(0);
            }
        } catch (EmptyResultDataAccessException e) {
            logger.error("Error in getBusinessRenewalSchedule() : " + e.getMessage());
        } catch (Exception e) {
            logger.error("Error in getBusinessRenewalSchedule() : " + e.getMessage());
        }
        return scheduleMap;
    }

    /**
     * Method to get the currency value from the currencies table
     * @param currencyCode
     * @return
     */
    public Long getCurrencyValue(String currencyCode) {
        Long currencyValue = 1L;
        try {
            String sql = "select value from currencies where currency_code = ?";
            currencyValue = jdbcTemplate.queryForObject(sql, new Object[]{currencyCode}, Long.class);
        } catch (EmptyResultDataAccessException e) {
            logger.error("Error in getCurrencyValue() : " + e.getMessage());
        } catch (Exception e) {
            logger.error("Error in getCurrencyValue() : " + e.getMessage());
        }
        return currencyValue;
    }

    /**
     * Method to create business renewal schedule
     * @param businessId
     * @param paymentIntentId
     * @param contractEndDate
     * @param amount
     * @param currency
     * @param paymentStatus
     * @param renewalStatus
     * @return
     */
    public int createBusinessRenewalSchedule(int businessId, String paymentIntentId, String contractEndDate, String newStartDate,
                                             long amount, String currency, String paymentStatus, String renewalStatus) {

        int result = 0;
        try {
            String sql = "INSERT INTO business_renewal_schedule (business_id, renewal_time, renewal_status, " +
                    "new_start_time, amount, currency, payment_status, payment_succeeded_amount, payment_intent_id) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            result = jdbcTemplate.update(sql, businessId, contractEndDate, renewalStatus, newStartDate, amount, currency,
                    paymentStatus, 0, paymentIntentId);
        } catch (Exception e) {
            logger.error("Error in createBusinessRenewalSchedule() : " + e.getMessage());
        }

        return result;
    }

    /**
     * Method to create payment intent using default currency USD and amount 1
     * @param eachBusinessId
     * @param amount
     * @param currencyCode
     * @return
     */
    public Map<String, String> createPaymentIntent(int eachBusinessId, long amount, String currencyCode) {

        Map<String, String> paymentIntentMap = new HashMap<>();
        try {
            String paymentIntentId = "";
            String customerId = this.returnCustomerIdByBusinessId(eachBusinessId);
            if (Utils.emptyString(customerId)) {
                logger.error("Customer Id is empty for business_id: {}", eachBusinessId);
                return paymentIntentMap;
            }

            StripeClient client = this.getStripeClient();
            PaymentIntentCreateParams params =
                    PaymentIntentCreateParams.builder()
                            .setAmount(amount)
                            .setCurrency(currencyCode)
                            .setCustomer(customerId)
                            .setSetupFutureUsage(PaymentIntentCreateParams.SetupFutureUsage.OFF_SESSION)
                            .setAutomaticPaymentMethods(
                                    PaymentIntentCreateParams.AutomaticPaymentMethods.builder().setEnabled(true).build()
                            )
                            .build();
            try {
                PaymentIntent paymentIntent = client.paymentIntents().create(params);
                paymentIntentId = paymentIntent.getId();
                paymentIntentMap.put("paymentIntentId", paymentIntentId);
                paymentIntentMap.put("clientSecret", paymentIntent.getClientSecret());
            } catch (StripeException e) {
                logger.error("Error in creating payment intent : " + e.getMessage());
            }
        } catch (Exception e) {
            logger.error("Error in createPaymentIntent() : " + e.getMessage());
        }

        return paymentIntentMap;
    }
}
