package com.dropthought.portal.service.shorturl;


import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.Utils;
import org.springframework.stereotype.Component;

@Component
public class ShortUrlBuilderFactory {

    public ShortUrlService createShortUrlService(String shortUrlType) {

        if (Utils.emptyString(shortUrlType))
            return null;
        switch (shortUrlType) {
            case (Constants.YASS):
                return new YassServiceImpl();
            case (Constants.BITLY):
                return new BitlyServiceImpl();
            default:
                throw new IllegalArgumentException("Unknown Short Url Type :  " + shortUrlType);
        }
    }

}
