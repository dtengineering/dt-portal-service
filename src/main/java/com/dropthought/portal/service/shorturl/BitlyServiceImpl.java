package com.dropthought.portal.service.shorturl;

import com.dropthought.portal.util.BitlyIntegrationUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class BitlyServiceImpl implements ShortUrlService {

    @Override
    public String generateShortUrl(String longUrl, String domain, String bitlykey, String bitlyAPI) {
        return BitlyIntegrationUtil.generateBitlyLink(longUrl, domain, bitlykey, bitlyAPI);
    }

    @Override
    public Map<String, String> constructPayload(String longUrl, String ttl) {
        return null;
    }

    @Override
    public boolean callShortUrlEndpoint(List shortUrlPayloadList, String apiLink) {
        return false;
    }
}
