package com.dropthought.portal.service.shorturl;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public interface ShortUrlService {

    String generateShortUrl(String longUrl, String domain, String apiKey, String apiLink);

    Map<String, String> constructPayload(String longUrl, String ttl);

    boolean callShortUrlEndpoint(List shortUrlPayloadList, String apiLink);
}
