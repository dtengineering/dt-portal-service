package com.dropthought.portal.service.shorturl;

import com.dropthought.portal.util.TokenUtility;
import com.dropthought.portal.util.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Component
public class YassServiceImpl implements ShortUrlService {

    private static final Logger logger = LogManager.getLogger(YassServiceImpl.class);

    @Autowired
    protected RestTemplate restTemplate;

    private String shortUrl;

    @Override
    public String generateShortUrl(String longUrl, String domain, String apiKey, String apiLink) {

        String randomString = TokenUtility.generateRandomShortUrl();
        this.shortUrl = randomString;

        return apiLink + randomString;
    }

    /**
     * construct request payload to call Yass endpoint
     * @param longUrl
     * @return
     */
    @Override
    public Map constructPayload(String longUrl, String ttl){

        Map shortUrlMap = new HashMap();
        try {
            shortUrlMap.put("longUrl", longUrl);
            shortUrlMap.put("shortUrl", shortUrl);
            shortUrlMap.put("ttl", ttl);
            logger.debug("time to live {}",ttl);
        }catch (Exception e){
            logger.error("Error in constructPayloadForYass() : " + e);
        }

        return shortUrlMap;
    }

    /**
     * Rest api to call yass short url service
     * @param requestList
     * @param method
     * @return
     */
    private List callYassAPI(List requestList, HttpMethod method, String apiLink) {

        logger.info("call yass short url service");
        List responseList = new ArrayList();
        JSONArray json = new JSONArray(requestList);
        String inputJson = json.toString();
        logger.info(inputJson);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);
        try {
            logger.debug("Yass endpoint", apiLink);
            ResponseEntity<Object> responseEntity = restTemplate.exchange(apiLink, method, entity, Object.class);
            if (Utils.isNotNull(responseEntity) && Utils.isNotNull(responseEntity.getBody())){
                logger.debug("Yass short URL request has been sent successfully!");
                responseList = (List) responseEntity.getBody();
            }
        } catch (Exception e) {
            logger.error("Error in callYassAPI() : " + e);
        }
        return responseList;
    }

    /**
     * method to call Yass endpoint to get short urls for given list shortUrlList
     *
     * @param shortUrlList
     * @return
     */
    @Override
    public boolean callShortUrlEndpoint(List shortUrlList, String apiLink) {

        boolean isSuccess = false;
        List sucessResponseList = new ArrayList();
        List rePublishList = new ArrayList();
        try {
            List resultList = this.callYassAPI(shortUrlList, HttpMethod.POST, apiLink);
            Iterator itr = resultList.iterator();
            int i = 0;
            while (itr.hasNext()) {
                Map resultMap = (Map) itr.next();
                if (resultMap.containsKey("status")) { //if true - random string already exists in the system
                    //set the request of shortUrl
                    Map failedShortUrlRequest = (Map) shortUrlList.get(i);
                    failedShortUrlRequest.remove("shortUrl");
                    failedShortUrlRequest.put("shortUrl", TokenUtility.generateRandomShortUrl());
                    rePublishList.add(failedShortUrlRequest);
                } else {
                    sucessResponseList.add(resultMap);
                }
                i++;
            }
            if(!rePublishList.isEmpty() && sucessResponseList.size() != shortUrlList.size()) {
                List reList = this.callYassAPI(rePublishList, HttpMethod.POST, apiLink);
                Iterator reListItr = resultList.iterator();
                while (reListItr.hasNext()) {
                    Map resultMap = (Map) reListItr.next();
                    //assume all republish request have successfully posted to yass
                    if (!resultMap.containsKey("status")) { //if true - random string already exists in the system
                        sucessResponseList.add(resultMap);
                    }
                }
            }

            if (sucessResponseList.size() == shortUrlList.size()) {
                isSuccess = true;
            }
        } catch (Exception e) {
            logger.error("Error in callShortUrlEndpoint() :", e.getMessage());
        }

        return isSuccess;
    }
}
