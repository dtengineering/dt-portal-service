package com.dropthought.portal.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;


public class BaseService {

    @Autowired
    protected RestTemplate restTemplate;

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
}
