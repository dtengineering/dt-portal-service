package com.dropthought.portal.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component
public class IdmService  extends BaseService{
    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();


    @Value("${idm.url}")
    private String idmKeyUrl;

    @Value("${idm.okta.url}")
    private String idmOktaUrl;

    @Value("${onprem}")
    private boolean onprem;

    @Autowired
    OKTAService oktaService;

    private String idmUrl;
    @PostConstruct
    public void getIdmLink()  {
        if(!onprem) {
            idmUrl = idmOktaUrl + "/okta";
            logger.info(" onprem is false :: url {}", idmUrl);
        }else{
            idmUrl = idmKeyUrl;
            logger.info(" onprem is true :: url {}", idmUrl);
        }
    }

    /**
     * Function to update user password idm
     *
     * @param userId
     * @return
     */
    public void resetUserPassword(String userId, String pwd, String businessUUID) {
        try {
            // call api to update user password by userId in idm service
            Map inputMap = new HashMap();
            inputMap.put("password", pwd);
            inputMap.put("confirmation", pwd);
            String url = idmUrl+"/user/id/"+userId+"/reset";

            Map userMap = this.callRestApi(inputMap, url, HttpMethod.PUT);
            oktaService.createUserUpdateResponse(inputMap, userMap, userId, businessUUID, "passwordReset");

            logger.info("reset pwd response : {}", new JSONObject(userMap).toString());
        }catch (Exception e){
            logger.error("exception at resetUserPassword. Msg {}", e.getMessage());
        }
    }

    /**
     * Function to update user via keycloak
     *
     * @param state
     * @return
     */
    public Map updateUser(final String state, String userUniqueID, String businessUniqueId) {
        logger.info("update user start");
        Map inputMap = new HashMap();
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            inputMap.put("enabled", true);
            //inputMap.put("role", "admin");
            inputMap.put("role", "super admin"); // hence king user as super admin. so updated the role in OKTA

            Map attributes = new HashMap();
            attributes.put("modified", Collections.singletonList(timestamp));
            attributes.put("confirmation", Collections.singletonList(state));
            inputMap.put("attributes", attributes);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        logger.info("update user from idm service");
        logger.info("update user idm reqeust {}", new JSONObject(inputMap).toString());
        Map responseMap = this.callRestApi(inputMap, idmUrl + "/user/id/" + userUniqueID, HttpMethod.PUT);
        //store request and response in okta temp table
        oktaService.createUserUpdateResponse(inputMap, responseMap, userUniqueID, businessUniqueId, "confirmationUpdate");

        logger.info("update user end");
        return responseMap;
    }

    /**
     * Function to deactivate user via keycloak/Okta
     *
     * @param userUniqueID
     * @param businessUniqueId
     * @return
     */
    public Map deactivateUser(String userUniqueID, String businessUniqueId) {
        logger.info("deactivate user start");
        Map responseMap = new HashMap();
        try {
            Map inputMap = new HashMap();
            inputMap.put("enabled", false);

            Map attributes = new HashMap();
            attributes.put("modified", Collections.singletonList(new Timestamp(System.currentTimeMillis())));
            inputMap.put("attributes", attributes);
            logger.info("deactivate user idm reqeust {}", new JSONObject(inputMap).toString());
            responseMap = this.callRestApi(inputMap, idmUrl + "/user/id/" + userUniqueID, HttpMethod.PUT);
            //store request and response in okta temp table
            oktaService.createUserUpdateResponse(inputMap, responseMap, userUniqueID, businessUniqueId, "deactivateUser");
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        logger.info("deactivate user end");
        return responseMap;
    }

    /**
     * Function to get user details by usrer unique from keycloack
     *
     * @param userId
     * @return
     */
    public Map getUserById(String userId) {
        // call get user details by email api from idm service
        Map userMap = this.callRestApi(new HashMap(), idmUrl + "/user/id/"+userId, HttpMethod.GET);
        return userMap;
    }

    /**
     * method to call a service using rest template
     * @param inputMap
     * @param uri
     */
    public Map callRestApi(Map inputMap, String uri, HttpMethod method){

        Map respMap = new HashMap();
        JSONObject json = new JSONObject(inputMap);
        String inputJson = json.toString();
        logger.info("uri {}",uri);
        logger.info("input json {}", inputJson);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);

        try {
            ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, method, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info(responseEntity.getBody().toString());
                    respMap = (Map) responseEntity.getBody();
                }
            }
        }
        catch (ResourceAccessException ex){
            ex.printStackTrace();
            logger.error("service refused to connect. uri {}", uri);
            respMap.put("success",false);
            respMap.put("message", "Connection refused");
        }
        catch (Exception e){
            respMap.put("success", false);
            respMap.put("message", "internal error");
            logger.error(e.getMessage());
        }
        return  respMap;
    }


    /**
     * Function to get user details by usrer unique from keycloack
     *
     * @param token
     * @return
     */
    public Map renewUserToken(String token) {
        // call get user details by email api from idm service
       return  this.callRestApi(new HashMap(), idmUrl + "/renew/" + token, HttpMethod.POST);
    }

}
