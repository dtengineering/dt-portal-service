package com.dropthought.portal.service;

import com.dropthought.portal.businessconfigurations.dto.BusinessConfigurationsBean;
import com.dropthought.portal.businessconfigurations.service.BusinessConfigVersionService;
import com.dropthought.portal.businessconfigurations.service.BusinessConfigurationsService;
import com.dropthought.portal.model.PlanConfigurationsBean;
import com.dropthought.portal.model.Pricing;
import com.dropthought.portal.model.TextAnalyticsConfigBean;
import com.dropthought.portal.model.TextAnalyticsRequestBean;
import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.TokenUtility;
import com.dropthought.portal.util.Utils;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class PlanService extends BaseService {

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper()
            .enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT)
            .enable(DeserializationFeature.FAIL_ON_READING_DUP_TREE_KEY );


    /**
     * JdbcTemplate for DB Connectivity.
     */
    @Autowired
    @Qualifier("jdbcDt")
    JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("jdbcNamedDt")
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    BusinessConfigurationsService businessConfigurationsService;

    @Autowired
    BusinessConfigVersionService businessConfigVersionService;

    @Autowired
    IntegrationAppService integrationAppService;

    @Autowired
    TokenUtility tokenUtility;

    @Autowired
    TextAnalyticsConfigurationService taService;

    private static final String GET_BUSINESS_UUID_BY_ID = "select business_uuid from business where business_id = ?";

    private static final String GET_BUSINESS_ID_BY_UUID = "select business_id from business where business_uuid = ?";

    public static String INSERT_TA_CONFIG = "insert into text_analytics_configs (business_id, text_analytics, text_analytics_plan_id, configs, approved_by, config_uuid, approvers, created_by, approval_status) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";


    public static final Map planFieldNames = new HashMap();

    static {
        planFieldNames.put("email","Email");
        planFieldNames.put("sms", "SMS");
        planFieldNames.put("qrcode", "QR Code");
        planFieldNames.put("shareable_link", "Shareable Link");
        planFieldNames.put("text_analytics", "Text Analytics");
        planFieldNames.put("logic", "Logic Options");
        planFieldNames.put("segment", "Segment Options");
        planFieldNames.put("advanced_schedule", "Advanced Scheduling");
        planFieldNames.put("multi_surveys", "Multilingual Surveys");
        planFieldNames.put("english_toggle", "Web Dashboard");
        planFieldNames.put("mobile_app", "Mobile Dashboard");
        planFieldNames.put("arabic_toggle", "Arabic Dashboard");
        planFieldNames.put("trigger_toggle", "Advanced Downstream Triggers");
        planFieldNames.put("metadata_question", "Metadata");
        planFieldNames.put("bitly", "URL Shortener");
        planFieldNames.put("preferred_metric", "Preferred Metric");
        planFieldNames.put("cohort_analysis", "Cohort Analysis");
        planFieldNames.put("advanced_templates", "Advanced Templates");
        planFieldNames.put("respondent_tracker", "Global Respondents Tracker");
        planFieldNames.put("notification", "Notification Center");
        planFieldNames.put("api_key", "API Key");
        planFieldNames.put("multiple_sublink", "Multiple Sublinks");
        planFieldNames.put("advanced_analysis", "Advanced Analysis");
        planFieldNames.put("export_links", "Unique link for each contact");
        planFieldNames.put("metric_correlation", "Metric Cross Tabulation");
        planFieldNames.put("from_customization", "From Email & Name settings");
        planFieldNames.put("integrations", "Integrations");
        planFieldNames.put("dynamic_links", "Dynamic QR Codes");
        planFieldNames.put("mfa", "Multi Factor Authentication");
        planFieldNames.put("advanced_report", "Advanced Report");
        planFieldNames.put("r_coefficient","Calculate correlation value");
        planFieldNames.put("focus_metric", "Action Plans");
        planFieldNames.put("predefined_question_library", "Predefined Question Library");
        planFieldNames.put("previously_used_question_library", "Previously Used Question Library");
        planFieldNames.put("custom_dashboard", "Custom Dashboard");
        planFieldNames.put("audit_program", "Audit Programs");
        planFieldNames.put("chatbot", "Chatbot");
        planFieldNames.put("custom_themes", "Custom EUX Theme");
        planFieldNames.put("program_throttling", "Program Throttling");
        planFieldNames.put("close_loop", "Close loop");
        planFieldNames.put("game_plan", "Gameplans");
        planFieldNames.put("notes", "Notes");
        planFieldNames.put("no_of_whatsapp", "No. of Whatsapp Invite sent");
        planFieldNames.put("no_of_users", "No. of Users");
        planFieldNames.put("no_of_active_programs", "No. of Active Programs");
        planFieldNames.put("no_of_responses", "No. of Responses for the account");
        planFieldNames.put("no_of_metrics", "No. of Metrics");
        planFieldNames.put("no_of_emails_sent", "No. of Email Invite sent");
        planFieldNames.put("no_of_sms", "No. of SMS Invite sent");
        planFieldNames.put("no_of_api", "No. of API requests");
        planFieldNames.put("no_of_filters", "No. of Filters saved");
        planFieldNames.put("no_of_triggers", "No. of Active Triggers");
        planFieldNames.put("data_retention", "Data Retention");
        planFieldNames.put("no_of_lists", "No. of Lists");
        planFieldNames.put("no_of_recipients", "No. of Total Recipients");
        planFieldNames.put("no_of_integrations", "No. of Integrated Apps");
        planFieldNames.put("no_of_kiosks", "No. of Kiosk");
        planFieldNames.put("unique_links", "Generate unique survey links for client's Email/Sms/Whatsapp distribution systems");
        planFieldNames.put("program_features", "overview/metric tab in program reports");
        planFieldNames.put("plan_properties", "Collect and analyze feedback with dt-lite in minutes");
    }

    /**
     * Function to get  planId by planUUID
     * @param planUUID
     * @return
     */
    public int getPlanIdByUUID(String planUUID) {
        Integer result = 0;
        try {
            String SELECT_ID_BY_BUSINESS_UUID = "select plan_config_id from plan_configurations where plan_config_uuid = ?";
            result = jdbcTemplate.queryForObject(SELECT_ID_BY_BUSINESS_UUID,new Object[]{planUUID.trim()},Integer.class);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.info("plan id not found for the given plan uuid");
        }
        return result;
    }

    /**
     * Function to get  plan configurations by planId
     * @param planUUID
     * @return
     */
    public List getPlanConfigurationsByPlanUUID(String planUUID) {
        List result = new ArrayList();
        logger.info("begin retrieving configs by business id");
        try {
            String GET_PLAN_CONFIGURATIONS_BY_PLANID = "select * from plan_configurations where plan_config_uuid = ?";
            result = jdbcTemplate.queryForList(GET_PLAN_CONFIGURATIONS_BY_PLANID, new Object[]{planUUID});

            List planConfigurationList = new ArrayList();
            Iterator<Map> iterator = result.iterator();
            while (iterator.hasNext()) {
                Map planConfigMap = iterator.next();
                if(Integer.parseInt(planConfigMap.get("email").toString())==0 && Integer.parseInt(planConfigMap.get("sms").toString())==0 && Integer.parseInt(planConfigMap.get("qrcode").toString())==0 && Integer.parseInt(planConfigMap.get("shareable_link").toString())==0){
                    planConfigMap.put("default",true);
                    result = new ArrayList();
                    result.add(planConfigMap);
                }

                Object planConfigUUID = planConfigMap.remove("plan_config_uuid");
                planConfigMap.put("planConfigUUID", planConfigUUID);
                Object planName = planConfigMap.remove("plan_name");
                planConfigMap.put("planName", planName);
                Object planConfigId = planConfigMap.remove("plan_config_id");
                planConfigMap.put("planConfigId", planConfigId);
                Object sharableLink = planConfigMap.remove("shareable_link");
                planConfigMap.put("sharableLink", sharableLink);
                Object textAnalytics = planConfigMap.remove("text_analytics");
                planConfigMap.put("textAnalytics", textAnalytics);
                Object metadataQuestion = planConfigMap.remove("metadata_question");
                planConfigMap.put("metadataQuestion",metadataQuestion);
                Object mobileApp = planConfigMap.remove("mobile_app");
                planConfigMap.put("mobileApp", mobileApp);
                Object emotionAnalysis = planConfigMap.remove("emotion_analysis");
                planConfigMap.put("emotionAnalysis", emotionAnalysis);
                Object intentAnalysis = planConfigMap.remove("intent_analysis");
                planConfigMap.put("intentAnalysis", intentAnalysis);
                Object trigger = planConfigMap.remove("trigger_toggle");
                planConfigMap.put("trigger", trigger);
                Object advancedSchedule = planConfigMap.remove("advanced_schedule");
                planConfigMap.put("advancedSchedule", advancedSchedule);
                Object english = planConfigMap.remove("english_toggle");
                planConfigMap.put("english", english);
                Object arabic = planConfigMap.remove("arabic_toggle");
                planConfigMap.put("arabic", arabic);
                Object multiSurveys = planConfigMap.remove("multi_surveys");
                planConfigMap.put("multiSurveys", multiSurveys);
                Object webHooks = planConfigMap.remove("web_hooks");
                planConfigMap.put("webHooks", webHooks);
                Object preferredMetric = planConfigMap.remove("preferred_metric");
                planConfigMap.put("preferredMetric", preferredMetric);
                Object categoryAnalysis = planConfigMap.remove("category_analysis");
                planConfigMap.put("categoryAnalysis", categoryAnalysis);
                Object advancedTextAnalytics = planConfigMap.remove("advanced_text_analytics");
                planConfigMap.put("advancedTextAnalytics", advancedTextAnalytics);
                Object noOfUsers = planConfigMap.remove("no_of_users");
                planConfigMap.put("noOfUsers", noOfUsers);
                Object noOfActivePrograms = planConfigMap.remove("no_of_active_programs");
                planConfigMap.put("noOfActivePrograms", noOfActivePrograms);
                Object noOfResponses = planConfigMap.remove("no_of_responses");
                planConfigMap.put("noOfResponses", noOfResponses);
                Object noOfMetrics = planConfigMap.remove("no_of_metrics");
                planConfigMap.put("noOfMetrics", noOfMetrics);
                Object noOfEmailsSent = planConfigMap.remove("no_of_emails_sent");
                planConfigMap.put("noOfEmailsSent", noOfEmailsSent);
                Object noOfSms = planConfigMap.remove("no_of_sms");
                planConfigMap.put("noOfSms", noOfSms);
                Object noOfApi = planConfigMap.remove("no_of_api");
                planConfigMap.put("noOfApi", noOfApi);
                Object noOfFilters = planConfigMap.remove("no_of_filters");
                planConfigMap.put("noOfFilters", noOfFilters);
                Object noOfTriggers = planConfigMap.remove("no_of_triggers");
                planConfigMap.put("noOfTriggers", noOfTriggers);
                Object dataRetention = planConfigMap.remove("data_retention");
                planConfigMap.put("dataRetention", dataRetention);
                Object noOfLists = planConfigMap.remove("no_of_lists");
                planConfigMap.put("noOfLists", noOfLists);
                Object noOfRecipients = planConfigMap.remove("no_of_recipients");
                planConfigMap.put("noOfRecipients", noOfRecipients);
                Object cohortAnalysis = planConfigMap.remove("cohort_analysis");
                planConfigMap.put("cohortAnalysis", cohortAnalysis);
                Object advancedTemplates = planConfigMap.remove("advanced_templates");
                planConfigMap.put("advancedTemplates", advancedTemplates);
                Object metricCorrelation = planConfigMap.remove("metric_correlation");
                planConfigMap.put("metricCorrelation", metricCorrelation);
                Object apiKey = planConfigMap.remove("api_key");
                planConfigMap.put("apiKey", apiKey);
                Object multipleSublink = planConfigMap.remove("multiple_sublink");
                planConfigMap.put("multipleSublink", multipleSublink);
                Object advancedAnalysis = planConfigMap.remove("advanced_analysis");
                planConfigMap.put("advancedAnalysis", advancedAnalysis);
                Object exportLinks = planConfigMap.remove("export_links");
                planConfigMap.put("exportLinks", exportLinks);
                Object respondentTracker = planConfigMap.remove("respondent_tracker");
                planConfigMap.put("respondentTracker", respondentTracker);
                Object fromCustomization = planConfigMap.remove("from_customization");
                planConfigMap.put("fromCustomization", fromCustomization);
                Object noOfIntegrations = planConfigMap.remove("no_of_integrations");
                planConfigMap.put("noOfIntegrations", noOfIntegrations);
                Object noOfKiosks = planConfigMap.remove("no_of_kiosks");
                planConfigMap.put("noOfKiosks", noOfKiosks);
                Object dynamicLinks = planConfigMap.remove("dynamic_links");
                planConfigMap.put("dynamicLinks", dynamicLinks);
                Object noOfShareableSublink = planConfigMap.remove("no_of_shareable_sublink");
                planConfigMap.put("noOfShareableSublink", noOfShareableSublink);
                Object noOfMultipleStaticQr = planConfigMap.remove("no_of_multiple_static_qr");
                planConfigMap.put("noOfMultipleStaticQr", noOfMultipleStaticQr);
                Object noOfDynamicSwitchQr = planConfigMap.remove("no_of_dynamic_switch_qr");
                planConfigMap.put("noOfDynamicSwitchQr", noOfDynamicSwitchQr);
                Object noOfDynamicGroupQr = planConfigMap.remove("no_of_dynamic_group_qr");
                planConfigMap.put("noOfDynamicGroupQr", noOfDynamicGroupQr);
                Object focusMetric = planConfigMap.remove("focus_metric");
                planConfigMap.put("focusMetric", focusMetric);
                Object uniqueLinks = planConfigMap.remove("unique_links");
                planConfigMap.put("uniqueLinks", uniqueLinks);
                Object programFeatures = planConfigMap.remove("program_features");
                planConfigMap.put("programFeatures", programFeatures);
                Object advancedReport = planConfigMap.remove("advanced_report");
                planConfigMap.put("advancedReport", advancedReport);
                Object gamePlan = planConfigMap.remove("game_plan");
                planConfigMap.put("gamePlan",gamePlan);
                Object previouslyUsedQuestionLibrary = planConfigMap.remove("previously_used_question_library");
                planConfigMap.put("previouslyUsedQuestionLibrary",previouslyUsedQuestionLibrary);
                Object predefinedQuestionLibrary = planConfigMap.remove("predefined_question_library");
                planConfigMap.put("predefinedQuestionLibrary",predefinedQuestionLibrary);
                Object customThemes = planConfigMap.remove("custom_themes");
                planConfigMap.put("customThemes", customThemes);
                Object auditProgram = planConfigMap.remove("audit_program");
                planConfigMap.put("auditProgram", auditProgram);
                Object customDashboard = planConfigMap.remove("custom_dashboard");
                planConfigMap.put("customDashboard", customDashboard);
                Object noOfWhatsapp = planConfigMap.remove("no_of_whatsapp");
                planConfigMap.put("noOfWhatsapp", noOfWhatsapp);
                Object rCoefficient = planConfigMap.remove("r_coefficient");
                planConfigMap.put("rCoefficient",rCoefficient);
                Object closeLoop = planConfigMap.remove("close_loop");
                planConfigMap.put("closeLoop",closeLoop);
                Object programThrottling = planConfigMap.remove("program_throttling");
                planConfigMap.put("programThrottling",programThrottling);
                Object trialDays = planConfigMap.remove("trial_days");
                planConfigMap.put("trialDays",trialDays);
                Object pricing = planConfigMap.remove("pricing");
                planConfigMap.put("pricing",pricing);
                Object planProperties = planConfigMap.remove("plan_properties");
                planConfigMap.put("planProperties", planProperties);
                Object taskManager = planConfigMap.remove("task_manager");
                planConfigMap.put("taskManager", taskManager);
                //DTV-12899
                planConfigMap.put("aiSurveys", planConfigMap.remove("ai_surveys"));
                Object ltiConfig = planConfigMap.remove("lti_config");
                planConfigMap.put("ltiConfig", ltiConfig);
                //DTV-13080
                planConfigMap.put("responseQuota", planConfigMap.remove("response_quota"));
                planConfigMap.put("customDashboardBuilder", planConfigMap.remove("custom_dashboard_builder"));
                planConfigurationList.add(planConfigMap);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("end retrieving configs by plan id");
        return result;
    }

    /**
     * Get all business using a plan uuid
     *
     * @param planUUID
     * @return
     */
    public List getAllBusinessByPlanUUID(String planUUID){
        List responses = new ArrayList();
        try{
            int planConfigId = getPlanIdByUUID(planUUID);
            String getAllBusinessUsingPlanConfig = "SELECT business_id FROM business WHERE plan_config_id = ?";
            List result = jdbcTemplate.queryForList(getAllBusinessUsingPlanConfig, new Object[]{planConfigId});

            for(int i=0; i<result.size(); i++)
            {
                Map businessMap = (Map) result.get(i);
                int businessId = Integer.parseInt(businessMap.get("business_id").toString());
                String businessUUID = retrieveBusinessUUIDById(businessId);
                responses.add(businessUUID);
            }
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return responses;
    }

    /**
     * function to retrieve a businessUUID by businessId
     *
     * @param businessId
     * @return
     */

    public String retrieveBusinessUUIDById(Integer businessId) {
        String businessUUID = "";
        try {
            businessUUID = jdbcTemplate.queryForObject(GET_BUSINESS_UUID_BY_ID, new Object[]{businessId}, String.class);
            logger.debug("end retrieving business by business uuid");
        }catch (Exception e){
            logger.info("No businessUUID found");
        }
        return businessUUID;
    }

    /**
     * function to update plan configuration by plan id
     * @param planConfigurationsBean
     * @return
     */
    public int updatePlanConfigurations(PlanConfigurationsBean planConfigurationsBean) {
        int updateStatus = 0;
        boolean publishEliza = false;
        logger.info("updatePlanConfigurations - start");
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            ArrayList<Object> objectArrayList = new ArrayList<Object>();

            Map usageLimitMap = new HashMap();

            String sql = "update plan_configurations set ";

            if (planConfigurationsBean.getPlanName() != null) {
                logger.info("plan name config is present");
                objectArrayList.add(planConfigurationsBean.getPlanName());
                sql = sql + "plan_name = ?, ";
            }

            if (planConfigurationsBean.getEmail() != null) {
                logger.info("business email config is present");
                objectArrayList.add(planConfigurationsBean.getEmail());
                sql = sql + "email = ?, ";
            }

            if (planConfigurationsBean.getSms() != null) {
                logger.info("business sms config is present");
                objectArrayList.add(planConfigurationsBean.getSms());
                sql = sql + "sms = ?, ";
            }

            if (planConfigurationsBean.getQrcode() != null) {
                logger.info("business qr config is present");
                objectArrayList.add(planConfigurationsBean.getQrcode());
                sql = sql + "qrcode = ?, ";
            }

            if (planConfigurationsBean.getShareableLink() != null) {
                logger.info("business shareable link config is present");
                objectArrayList.add(planConfigurationsBean.getShareableLink());
                sql = sql + "shareable_link = ?, ";
            }

            if (planConfigurationsBean.getTextAnalytics() != null) {
                logger.info("business text analytics config is present");
                objectArrayList.add(planConfigurationsBean.getTextAnalytics());
                sql = sql + "text_analytics = ?, ";
            }

            if (planConfigurationsBean.getLogic() != null) {
                logger.info("business logic config is present");
                objectArrayList.add(planConfigurationsBean.getLogic());
                sql = sql + "logic = ?, ";
            }

            if (planConfigurationsBean.getRespondent() != null) {
                logger.info("business respondent config is present");
                objectArrayList.add(planConfigurationsBean.getRespondent());
                sql = sql + "respondent = ?, ";
            }

            if (planConfigurationsBean.getSegment() != null) {
                logger.info("business segment config is present");
                objectArrayList.add(planConfigurationsBean.getSegment());
                sql = sql + "segment = ?, ";
            }

            if (planConfigurationsBean.getKiosk() != null) {
                logger.info("business kiosk config is present");
                objectArrayList.add(planConfigurationsBean.getKiosk());
                sql = sql + "kiosk = ?, ";
            }

            if (planConfigurationsBean.getMetadataQuestion() != null) {
                logger.info("business settings metadataQuestion is present");
                objectArrayList.add(planConfigurationsBean.getMetadataQuestion());
                sql = sql + "metadata_question = ?, ";
            }

            if(planConfigurationsBean.getMobileApp() !=null) {
                logger.info("business settings mobile app is present");
                objectArrayList.add(planConfigurationsBean.getMobileApp());
                sql = sql + "mobile_app = ?, ";
            }

            if(planConfigurationsBean.getEmotionAnalysis() !=null) {
                logger.info("business settings emotion analysis is present");
                objectArrayList.add(planConfigurationsBean.getEmotionAnalysis());
                sql = sql + "emotion_analysis = ?, ";
            }

            if(planConfigurationsBean.getIntentAnalysis() !=null) {
                logger.info("business settings intent analysis is present");
                objectArrayList.add(planConfigurationsBean.getIntentAnalysis());
                sql = sql + "intent_analysis = ?, ";
            }

            if(planConfigurationsBean.getTrigger() !=null) {
                logger.info("business settings trigger config is present");
                objectArrayList.add(planConfigurationsBean.getTrigger());
                sql = sql + "trigger_toggle = ?, ";
            }

            if(planConfigurationsBean.getAdvancedSchedule() != null) {
                logger.info("business settings advanced schedule is present");
                objectArrayList.add(planConfigurationsBean.getAdvancedSchedule());
                sql = sql + "advanced_schedule = ?, ";
            }

            if(planConfigurationsBean.getEnglish() != null) {
                logger.info("business settings english is present");
                objectArrayList.add(planConfigurationsBean.getEnglish());
                sql = sql + "english_toggle = ?, ";
            }

            if(planConfigurationsBean.getArabic() != null) {
                logger.info("business settings arabic is present");
                objectArrayList.add(planConfigurationsBean.getArabic());
                sql = sql + "arabic_toggle = ?, ";
            }

            if(planConfigurationsBean.getMultiSurveys() != null) {
                logger.info("business settings multi surveys is present");
                objectArrayList.add(planConfigurationsBean.getMultiSurveys());
                sql = sql + "multi_surveys = ?, ";
            }

            if(planConfigurationsBean.getBitly() != null) {
                logger.info("business settings bitly is present");
                objectArrayList.add(planConfigurationsBean.getBitly());
                sql = sql + "bitly = ?, ";
            }

            if(planConfigurationsBean.getWebHooks() != null) {
                logger.info("business settings web hooks is present");
                objectArrayList.add(planConfigurationsBean.getWebHooks());
                sql = sql + "web_hooks = ?, ";
            }

            if(planConfigurationsBean.getPreferredMetric() != null) {
                logger.info("business settings preferred metric is present");
                objectArrayList.add(planConfigurationsBean.getPreferredMetric());
                sql = sql + "preferred_metric = ?, ";
            }

            if(planConfigurationsBean.getCategoryAnalysis() != null) {
                logger.info("business settings category analysis is present");
                objectArrayList.add(planConfigurationsBean.getCategoryAnalysis());
                sql = sql + "category_analysis = ?, ";
            }

            if(planConfigurationsBean.getAdvancedTextAnalytics() != null) {
                logger.info("business settings advanced text analytics is present");
                objectArrayList.add(planConfigurationsBean.getAdvancedTextAnalytics());
                sql = sql + "advanced_text_analytics = ?, ";
            }

            if(planConfigurationsBean.getNoOfUsers() != null) {
                logger.info("business settings no of users is present");
                objectArrayList.add(planConfigurationsBean.getNoOfUsers());
                sql = sql + "no_of_users = ?, ";
                usageLimitMap.put("users", planConfigurationsBean.getNoOfUsers());
            }

            if(planConfigurationsBean.getNoOfActivePrograms() != null) {
                logger.info("business settings no of active programs is present");
                objectArrayList.add(planConfigurationsBean.getNoOfActivePrograms());
                sql = sql + "no_of_active_programs = ?, ";
                usageLimitMap.put("programs", planConfigurationsBean.getNoOfActivePrograms());
            }

            if(planConfigurationsBean.getNoOfResponses() != null) {
                logger.info("business settings no of responses is present");
                objectArrayList.add(planConfigurationsBean.getNoOfResponses());
                sql = sql + "no_of_responses = ?, ";
                usageLimitMap.put("responses", planConfigurationsBean.getNoOfResponses());
            }

            if(planConfigurationsBean.getNoOfMetrics() != null) {
                logger.info("business settings no of metrics is present");
                objectArrayList.add(planConfigurationsBean.getNoOfMetrics());
                sql = sql + "no_of_metrics = ?, ";
                usageLimitMap.put("metrics", planConfigurationsBean.getNoOfMetrics());
            }

            if(planConfigurationsBean.getNoOfEmailsSent() != null) {
                logger.info("business settings no of emails sent is present");
                objectArrayList.add(planConfigurationsBean.getNoOfEmailsSent());
                sql = sql + "no_of_emails_sent = ?, ";
                usageLimitMap.put("emails", planConfigurationsBean.getNoOfEmailsSent());
            }

            if(planConfigurationsBean.getNoOfSms() != null) {
                logger.info("business settings no of sms sent is present");
                objectArrayList.add(planConfigurationsBean.getNoOfSms());
                sql = sql + "no_of_sms = ?, ";
                usageLimitMap.put("sms", planConfigurationsBean.getNoOfSms());
            }

            if(planConfigurationsBean.getNoOfApi() != null) {
                logger.info("business settings no of api sent is present");
                objectArrayList.add(planConfigurationsBean.getNoOfApi());
                sql = sql + "no_of_api = ?, ";
                usageLimitMap.put("api", planConfigurationsBean.getNoOfApi());
            }

            if(planConfigurationsBean.getNoOfFilters() != null) {
                logger.info("business settings no of filters sent is present");
                objectArrayList.add(planConfigurationsBean.getNoOfFilters());
                sql = sql + "no_of_filters = ?, ";
                usageLimitMap.put("filters", planConfigurationsBean.getNoOfFilters());
            }

            if(planConfigurationsBean.getNoOfTriggers() != null) {
                logger.info("business settings no of triggers sent is present");
                objectArrayList.add(planConfigurationsBean.getNoOfTriggers());
                sql = sql + "no_of_triggers = ?, ";
                usageLimitMap.put("triggers", planConfigurationsBean.getNoOfTriggers());
            }

            if(planConfigurationsBean.getDataRetention() != null) {
                logger.info("business settings data retention sent is present");
                objectArrayList.add(planConfigurationsBean.getDataRetention());
                sql = sql + "data_retention = ?, ";
            }

            if(planConfigurationsBean.getNoOfLists() != null) {
                logger.info("business settings no of lists sent is present");
                objectArrayList.add(planConfigurationsBean.getNoOfLists());
                sql = sql + "no_of_lists = ?, ";
                usageLimitMap.put("lists", planConfigurationsBean.getNoOfLists());
            }

            if(planConfigurationsBean.getNoOfRecipients() != null) {
                logger.info("business settings no of recipients sent is present");
                objectArrayList.add(planConfigurationsBean.getNoOfRecipients());
                sql = sql + "no_of_recipients = ?, ";
                usageLimitMap.put("recipients", planConfigurationsBean.getNoOfRecipients());
            }

            if (planConfigurationsBean.getModifiedBy() != null) {
                logger.info("user id is present");
                objectArrayList.add(planConfigurationsBean.getModifiedBy());
                sql = sql + "modified_by = ?,";
            }

            if (planConfigurationsBean.getHippa() != null) {
                logger.info("business hippa config is present");
                objectArrayList.add(planConfigurationsBean.getHippa());
                sql = sql + "hippa = ?, ";
            }

            if (planConfigurationsBean.getCohortAnalysis() != null) {
                logger.info("business cohort analysis config is present");
                objectArrayList.add(planConfigurationsBean.getCohortAnalysis());
                sql = sql + "cohort_analysis = ?, ";
            }

            if (planConfigurationsBean.getAdvancedTemplates() != null) {
                logger.info("business advanced templates config is present");
                objectArrayList.add(planConfigurationsBean.getAdvancedTemplates());
                sql = sql + "advanced_templates = ?, ";
            }

            if(planConfigurationsBean.getMetricCorrelation() != null) {
                logger.info("business metric correlation config is present");
                objectArrayList.add(planConfigurationsBean.getMetricCorrelation());
                sql = sql + "metric_correlation = ?, ";
            }

            if(planConfigurationsBean.getApiKey() != null) {
                logger.info("business api key config is present");
                objectArrayList.add(planConfigurationsBean.getApiKey());
                sql = sql + "api_key = ?, ";
            }

            if(planConfigurationsBean.getWhatfix() != null) {
                logger.info("business whatfix config is present");
                objectArrayList.add(planConfigurationsBean.getWhatfix());
                sql = sql + "whatfix = ?, ";
            }

            if(planConfigurationsBean.getMultipleSublink() != null) {
                logger.info("business multiple sublink config is present");
                objectArrayList.add(planConfigurationsBean.getMultipleSublink());
                sql = sql + "multiple_sublink = ?, ";
            }

            if(planConfigurationsBean.getAdvancedAnalysis() != null) {
                logger.info("business advanced analysis config is present");
                objectArrayList.add(planConfigurationsBean.getAdvancedAnalysis());
                sql = sql + "advanced_analysis = ?, ";
            }

            if(planConfigurationsBean.getExportLinks() != null) {
                logger.info("business export links config is present");
                objectArrayList.add(planConfigurationsBean.getExportLinks());
                sql = sql + "export_links = ?, ";
            }

            if(planConfigurationsBean.getRespondentTracker() != null){
                logger.info("business respondent tracker config is present");
                objectArrayList.add(planConfigurationsBean.getRespondentTracker());
                sql = sql + "respondent_tracker = ?, ";
            }

            if(planConfigurationsBean.getFromCustomization() != null){
                logger.info("business from customization config is present");
                objectArrayList.add(planConfigurationsBean.getFromCustomization());
                sql = sql + "from_customization = ?, ";
            }

            if(planConfigurationsBean.getIntegrations() !=null) {
                logger.info("business settings integrations config is present");
                objectArrayList.add(planConfigurationsBean.getIntegrations());
                sql = sql + "integrations = ?, ";
            }

            if(planConfigurationsBean.getNoOfIntegrations() != null) {
                logger.info("business settings no of integrations is present");
                objectArrayList.add(planConfigurationsBean.getNoOfIntegrations());
                sql = sql + "no_of_integrations = ?, ";
                usageLimitMap.put("integrations", planConfigurationsBean.getNoOfIntegrations());
            }

            if(planConfigurationsBean.getNoOfKiosks() != null) {
                logger.info("business settings no of kiosks is present");
                objectArrayList.add(planConfigurationsBean.getNoOfKiosks());
                sql = sql + "no_of_kiosks = ?, ";
                usageLimitMap.put("kiosks", planConfigurationsBean.getNoOfKiosks());
            }

            if(planConfigurationsBean.getDynamicLinks() !=null) {
                logger.info("business settings dynamicLinks config is present");
                objectArrayList.add(planConfigurationsBean.getDynamicLinks());
                sql = sql + "dynamic_links = ?, ";
            }

            if(planConfigurationsBean.getNoOfShareableSublink() != null) {
                logger.info("business settings no of shareable sublink is present");
                objectArrayList.add(planConfigurationsBean.getNoOfShareableSublink());
                sql = sql + "no_of_shareable_sublink = ?, ";
            }

            if(planConfigurationsBean.getNoOfMultipleStaticQr() != null) {
                logger.info("business settings no of multiple static qr is present");
                objectArrayList.add(planConfigurationsBean.getNoOfMultipleStaticQr());
                sql = sql + "no_of_multiple_static_qr = ?, ";
            }

            if(planConfigurationsBean.getNoOfDynamicSwitchQr() != null) {
                logger.info("business settings no of dynamic switch qr is present");
                objectArrayList.add(planConfigurationsBean.getNoOfDynamicSwitchQr());
                sql = sql + "no_of_dynamic_switch_qr = ?, ";
            }

            if(planConfigurationsBean.getNoOfDynamicGroupQr() != null) {
                logger.info("business settings no of dynamic group qr is present");
                objectArrayList.add(planConfigurationsBean.getNoOfDynamicGroupQr());
                sql = sql + "no_of_dynamic_group_qr = ?, ";
            }

            if (planConfigurationsBean.getMfa() != null) {
                logger.info("business mfa config is present");
                objectArrayList.add(planConfigurationsBean.getMfa());
                sql = sql + "mfa = ?, ";
            }

            if(planConfigurationsBean.getFocusMetric() != null) {
                logger.info("business settings focus metric is present");
                Map focusMap = new HashMap();
                focusMap.put("focusMetric",planConfigurationsBean.getFocusMetric());
                focusMap.put("defaultRecommendation",planConfigurationsBean.getDefaultRecommendation()!=null ? planConfigurationsBean.getDefaultRecommendation() : "0");
                focusMap.put("userDefinedRecommendation",planConfigurationsBean.getUserDefinedRecommendation()!=null ? planConfigurationsBean.getUserDefinedRecommendation() : "0");
                objectArrayList.add(new JSONObject(focusMap).toString());
                sql = sql + "focus_metric = ?, ";
            }

            if(Utils.isNotNull(planConfigurationsBean.getSSO())) {
                logger.info("business sso config is present");
                Map ssoMap = new HashMap();
                ssoMap.put("googleSSO",planConfigurationsBean.getGoogleSSO()!=null ? planConfigurationsBean.getGoogleSSO() : "0");
                ssoMap.put("microsoftSSO",planConfigurationsBean.getMicrosoftSSO()!=null ? planConfigurationsBean.getMicrosoftSSO() : "0");
                ssoMap.put("appleSSO",planConfigurationsBean.getAppleSSO()!=null ? planConfigurationsBean.getAppleSSO() : "0");
                objectArrayList.add(new JSONObject(ssoMap).toString());
                sql = sql + "sso = ?, ";
            }

            if(planConfigurationsBean.getUniqueLinks() != null) {
                logger.info("business settings unique links is present");
                Map uniqueMap = new HashMap();
                uniqueMap.put("uniqueLinkEmail",planConfigurationsBean.getUniqueLinkEmail()!=null ? planConfigurationsBean.getUniqueLinkEmail() : "0");
                uniqueMap.put("uniqueLinkSms",planConfigurationsBean.getUniqueLinkSms()!=null ? planConfigurationsBean.getUniqueLinkSms() : "0");
                uniqueMap.put("uniqueLinkWhatsapp",planConfigurationsBean.getUniqueLinkWhatsapp()!=null ? planConfigurationsBean.getUniqueLinkWhatsapp() : "0");
                objectArrayList.add(new JSONObject(uniqueMap).toString());
                sql = sql + "unique_links = ?, ";
            }

            if(planConfigurationsBean.getProgramFeatures() != null) {
                logger.info("business settings program features is present");
                Map programMap = new HashMap();
                programMap.put("programOverview",planConfigurationsBean.getProgramOverview()!=null ? planConfigurationsBean.getProgramOverview() : "0");
                programMap.put("programMetric",planConfigurationsBean.getProgramMetric()!=null ? planConfigurationsBean.getProgramMetric() : "0");
                objectArrayList.add(new JSONObject(programMap).toString());
                sql = sql + "program_features = ?, ";
            }

            if(planConfigurationsBean.getAdvancedReport() != null) {
                logger.info("business settings Advanced Report is present");
                objectArrayList.add(planConfigurationsBean.getAdvancedReport());
                sql = sql + "advanced_report = ?, ";
            }

            if(planConfigurationsBean.getGamePlan() != null) {
                logger.info("business settings game plan is present");
                objectArrayList.add(planConfigurationsBean.getGamePlan());
                sql = sql + "game_plan = ?, ";
            }

            if(planConfigurationsBean.getPreviouslyUsedQuestionLibrary() != null) {
                logger.info("business settings previously used question library is present");
                objectArrayList.add(planConfigurationsBean.getPreviouslyUsedQuestionLibrary());
                sql = sql + "previously_used_question_library = ?,";
            }

            if(planConfigurationsBean.getPredefinedQuestionLibrary() != null) {
                logger.info("business settings predefined question library is present");
                objectArrayList.add(planConfigurationsBean.getPredefinedQuestionLibrary());
                sql = sql + "predefined_question_library = ?,";
            }

            if (planConfigurationsBean.getCustomThemes() != null) {
                logger.info("business custom theme config is present");
                objectArrayList.add(planConfigurationsBean.getCustomThemes());
                sql = sql + "custom_themes = ?, ";
            }

            if (planConfigurationsBean.getChatbot() != null) {
                logger.info("business chatbot config is present");
                objectArrayList.add(planConfigurationsBean.getChatbot());
                sql = sql + " chatbot = ?, ";
            }

            if (planConfigurationsBean.getAuditProgram() != null) {
                logger.info("business auditProgram config is present");
                objectArrayList.add(planConfigurationsBean.getAuditProgram());
                sql = sql + " audit_program = ?, ";

                //collaboration is sub feature of audit program
                if (planConfigurationsBean.getCollaboration() != null) {
                    logger.info("business collaboration config is present");
                    objectArrayList.add(planConfigurationsBean.getCollaboration());
                    sql = sql + " collaboration = ?, ";
                }
            }

            if (planConfigurationsBean.getCustomDashboard() != null) {
                logger.info("business customDashboard config is present");
                objectArrayList.add(planConfigurationsBean.getCustomDashboard());
                sql = sql + " custom_dashboard = ?, ";
            }

            if (planConfigurationsBean.getWhatsapp() != null) {
                logger.info("business whatsapp config is present");
                objectArrayList.add(planConfigurationsBean.getWhatsapp());
                sql = sql + "whatsapp = ?, ";
            }

            if(planConfigurationsBean.getNoOfWhatsapp() != null) {
                logger.info("business no of whatsapp config is present");
                objectArrayList.add(planConfigurationsBean.getNoOfWhatsapp());
                sql = sql + "no_of_whatsapp = ?, ";
            }

            if(planConfigurationsBean.getRCoefficient() != null) {
                logger.info("business r coefficient config is present");
                objectArrayList.add(planConfigurationsBean.getRCoefficient());
                sql = sql + "r_coefficient = ?, ";
            }

            if(Utils.isNotNull(planConfigurationsBean.getActive())) {
                logger.info("business active config is present");
                objectArrayList.add(planConfigurationsBean.getActive());
                sql = sql + "active = ?, ";
            }

            if(Utils.isNotNull(planConfigurationsBean.getPricing())) {
                logger.info("business pricing config is present");
                //convert planConfigurationsBean.getPricing() to json string
                String pricingJson = new Gson().toJson(planConfigurationsBean.getPricing());
                objectArrayList.add(pricingJson);
                sql = sql + "pricing = ?, ";
            }

            if(Utils.isNotNull(planConfigurationsBean.getPlanProperties())) {
                logger.info("business plan_properties config is present");
                //convert planConfigurationsBean.getPlanProperties() to json string
                //JSONArray questionDataJsonArray = new JSONArray(questionData);
                List planPropertiesList = planConfigurationsBean.getPlanProperties();
                JSONArray planPropertiesJson = new JSONArray(planPropertiesList);
                objectArrayList.add(planPropertiesJson.toString());
                sql = sql + "plan_properties = ?, ";
            }

            // Added missing configuration while plan update : DTV11271
            if(Utils.isNotNull(planConfigurationsBean.getCloseLoop())) {
                logger.info("business close loop config is present");
                objectArrayList.add(planConfigurationsBean.getCloseLoop());
                sql = sql + "close_loop = ?, ";
            }

            if(Utils.isNotNull(planConfigurationsBean.getNotification())) {
                logger.info("business notification config is present");
                objectArrayList.add(planConfigurationsBean.getNotification());
                sql = sql + "notification = ?, ";
            }

            if(Utils.isNotNull(planConfigurationsBean.getProgramThrottling())) {
                logger.info("business program throttling config is present");
                objectArrayList.add(planConfigurationsBean.getProgramThrottling());
                sql = sql + "program_throttling = ?, ";
            }

            //DTV-12899
            if(Utils.isNotNull(planConfigurationsBean.getAiSurveys())) {
                logger.info("business ai surveys setting is present");
                objectArrayList.add(planConfigurationsBean.getAiSurveys());
                sql = sql + " ai_surveys = ?, ";
            }

            if(Utils.isNotNull(planConfigurationsBean.getLti())) {
                logger.info("business lti is present");
                objectArrayList.add(planConfigurationsBean.getLti());
                sql = sql + "lti = ?, ";
            }

            if(Utils.isNotNull(planConfigurationsBean.getLtiConfig())) {
                logger.info("business lti config is present");
                objectArrayList.add(new JSONObject(planConfigurationsBean.getLtiConfig()).toString());
                sql = sql + "lti_config = ?, ";
            }

            if(Utils.isNotNull(planConfigurationsBean.getResponseQuota())) {
                logger.info("business response quota present");
                objectArrayList.add(planConfigurationsBean.getResponseQuota());
                sql = sql + " response_quota = ?, ";
            }

            if(Utils.isNotNull(planConfigurationsBean.getRecurrence())) {
                logger.info("business recurrence config is present");
                objectArrayList.add(new JSONObject(planConfigurationsBean.getRecurrence()).toString());
                sql = sql + " recurrence = ?, ";
            }

            if(Utils.isNotNull(planConfigurationsBean.getCustomDashboardBuilder())) {
                logger.info("business custom dashboard builder config is present");
                objectArrayList.add(planConfigurationsBean.getCustomDashboardBuilder());
                sql = sql + " custom_dashboard_builder = ?, ";
            }


            // Not a part of plan configuration as of now
           /* if(Utils.isNotNull(planConfigurationsBean.getTrialDays())) {
                logger.info("business trial days config is present");
                objectArrayList.add(planConfigurationsBean.getTrialDays());
                sql = sql + "trial_days = ?, ";
            }*/

            if (objectArrayList.size() > 0) {
                objectArrayList.add(timestamp);
                sql = sql + "modified_time = ? ";

                if (planConfigurationsBean.getPlanConfigUUID() != null) {
                    sql = sql + "where plan_config_uuid = ? ";
                    logger.info("sql " + sql);
                    objectArrayList.add(planConfigurationsBean.getPlanConfigUUID());
                    logger.info("Executing the query");
                    updateStatus = jdbcTemplate.update(sql, objectArrayList.toArray());
                    logger.info("update status " + updateStatus);
                }
            }

        } catch (Exception e) {
            logger.error("exception at updatePlanConfigurations. Msg - {} ",e.getMessage());
            e.printStackTrace();
        }
        logger.info("updatePlanCOnfigurations - end");
        return updateStatus;
    }

    /**
     * Function to create plan configurations
     *
     * @param planConfigurationsBean
     * @return
     */
    public Map createPlanConfigurations(PlanConfigurationsBean planConfigurationsBean) {
        int configurationsId = 0;
        Map returnMap = new HashMap();
        try {
            final Integer createdBy = Utils.isNotNull(planConfigurationsBean.getCreatedBy()) ? planConfigurationsBean.getCreatedBy() : 0;
            final Integer modifiedBy = Utils.isNotNull(planConfigurationsBean.getModifiedBy()) ? planConfigurationsBean.getModifiedBy() : 0;
            final String planName = Utils.isNotNull(planConfigurationsBean.getPlanName()) ? planConfigurationsBean.getPlanName() : "";
            final Timestamp createdTime = Utils.isNotNull(planConfigurationsBean.getCreatedTime()) ? planConfigurationsBean.getCreatedTime() : new Timestamp(System.currentTimeMillis());
            final Timestamp modifiedTime = Utils.isNotNull(planConfigurationsBean.getModifiedTime()) ? planConfigurationsBean.getModifiedTime() : new Timestamp(System.currentTimeMillis());
            final String email = Utils.isNotNull(planConfigurationsBean.getEmail()) ? planConfigurationsBean.getEmail() : "0";
            final String sms = Utils.isNotNull(planConfigurationsBean.getSms()) ? planConfigurationsBean.getSms() : "0";
            final String qrCode = Utils.isNotNull(planConfigurationsBean.getQrcode()) ? planConfigurationsBean.getQrcode() : "0";
            final String shareableLink = Utils.isNotNull(planConfigurationsBean.getShareableLink()) ? planConfigurationsBean.getShareableLink() : "0";
            final String textAnalytics = Utils.isNotNull(planConfigurationsBean.getTextAnalytics()) ? planConfigurationsBean.getTextAnalytics() : "0";
            final String logic = Utils.isNotNull(planConfigurationsBean.getLogic()) ? planConfigurationsBean.getLogic() : "0";
            final String respondent = Utils.isNotNull(planConfigurationsBean.getRespondent()) ? planConfigurationsBean.getRespondent() : "0";
            final String planConfigUUID = tokenUtility.generateUUID();
            final String segment = Utils.isNotNull(planConfigurationsBean.getSegment()) ? planConfigurationsBean.getSegment() : "0";
            final String kiosk = Utils.isNotNull(planConfigurationsBean.getKiosk()) ? planConfigurationsBean.getKiosk() : "0";
            final String metadataQuestion = Utils.isNotNull(planConfigurationsBean.getMetadataQuestion()) ? planConfigurationsBean.getMetadataQuestion() : "0";
            final String mobileApp = Utils.isNotNull(planConfigurationsBean.getMobileApp()) ? planConfigurationsBean.getMobileApp() : "0";
            final String emotionAnalysis = Utils.isNotNull(planConfigurationsBean.getEmotionAnalysis()) ? planConfigurationsBean.getEmotionAnalysis() : "0";
            final String intentAnalysis = Utils.isNotNull(planConfigurationsBean.getIntentAnalysis()) ? planConfigurationsBean.getIntentAnalysis() : "0";
            final String trigger = Utils.isNotNull(planConfigurationsBean.getTrigger()) ? planConfigurationsBean.getTrigger() : "0";
            final String advancedSchedule = Utils.isNotNull(planConfigurationsBean.getAdvancedSchedule()) ? planConfigurationsBean.getAdvancedSchedule() : "0";
            final String english = Utils.isNotNull(planConfigurationsBean.getEnglish()) ? planConfigurationsBean.getEnglish() : "0";
            final String arabic = Utils.isNotNull(planConfigurationsBean.getArabic()) ? planConfigurationsBean.getArabic() : "0";
            final String multiSurveys = Utils.isNotNull(planConfigurationsBean.getMultiSurveys()) ? planConfigurationsBean.getMultiSurveys() : "0";
            final String bitly = Utils.isNotNull(planConfigurationsBean.getBitly()) ? planConfigurationsBean.getBitly() : "0";
            final String webHooks = Utils.isNotNull(planConfigurationsBean.getBitly()) ? planConfigurationsBean.getWebHooks() : "0";
            final String preferredMetric = Utils.isNotNull(planConfigurationsBean.getPreferredMetric()) ? planConfigurationsBean.getPreferredMetric() : "0";
            final String notification = Utils.isNotNull(planConfigurationsBean.getNotification()) ? planConfigurationsBean.getNotification() : "0";
            final String categoryAnalysis = Utils.isNotNull(planConfigurationsBean.getCategoryAnalysis()) ? planConfigurationsBean.getCategoryAnalysis() : "0";
            final String advancedTextAnalytics = Utils.isNotNull(planConfigurationsBean.getAdvancedTextAnalytics()) ? planConfigurationsBean.getAdvancedTextAnalytics() : "0";
            final Integer noOfUsers = Utils.isNotNull(planConfigurationsBean.getNoOfUsers()) ? planConfigurationsBean.getNoOfUsers() : Constants.LIMIT_UNLIMITED;
            final Integer noOfActivePrograms = Utils.isNotNull(planConfigurationsBean.getNoOfActivePrograms()) ? planConfigurationsBean.getNoOfActivePrograms() : Constants.LIMIT_UNLIMITED;
            final Integer noOfResponses = Utils.isNotNull(planConfigurationsBean.getNoOfResponses()) ? planConfigurationsBean.getNoOfResponses() : Constants.LIMIT_UNLIMITED;
            final Integer noOfMetrics = Utils.isNotNull(planConfigurationsBean.getNoOfMetrics()) ? planConfigurationsBean.getNoOfMetrics() : Constants.LIMIT_UNLIMITED;
            final Integer noOfEmailsSent = Utils.isNotNull(planConfigurationsBean.getNoOfEmailsSent()) ? planConfigurationsBean.getNoOfEmailsSent() : Constants.LIMIT_UNLIMITED;
            final Integer noOfSms = Utils.isNotNull(planConfigurationsBean.getNoOfSms()) ? planConfigurationsBean.getNoOfSms() : Constants.LIMIT_UNLIMITED;
            final Integer noOfApi = Utils.isNotNull(planConfigurationsBean.getNoOfApi()) ? planConfigurationsBean.getNoOfApi() : Constants.LIMIT_UNLIMITED;
            final Integer noOfFilters = Utils.isNotNull(planConfigurationsBean.getNoOfFilters()) ? planConfigurationsBean.getNoOfFilters() : Constants.LIMIT_UNLIMITED;
            final Integer noOfTriggers = Utils.isNotNull(planConfigurationsBean.getNoOfTriggers()) ? planConfigurationsBean.getNoOfTriggers() : Constants.LIMIT_UNLIMITED;
            final Integer dataRetention = Utils.isNotNull(planConfigurationsBean.getDataRetention()) ? planConfigurationsBean.getDataRetention() : Constants.LIMIT_UNLIMITED;
            final Integer noOfLists = Utils.isNotNull(planConfigurationsBean.getNoOfLists()) ? planConfigurationsBean.getNoOfLists() : Constants.LIMIT_UNLIMITED;
            final Integer noOfRecipients = Utils.isNotNull(planConfigurationsBean.getNoOfRecipients()) ? planConfigurationsBean.getNoOfRecipients() : Constants.LIMIT_UNLIMITED;
            final String hippa = Utils.isNotNull(planConfigurationsBean.getHippa()) ? planConfigurationsBean.getHippa() : "0";
            final String customThemes = Utils.isNotNull(planConfigurationsBean.getCustomThemes()) ? planConfigurationsBean.getCustomThemes() : "0";
            final String cohortAnalysis = Utils.isNotNull(planConfigurationsBean.getCohortAnalysis()) ? planConfigurationsBean.getCohortAnalysis() : "0";
            final String advancedTemplates = Utils.isNotNull(planConfigurationsBean.getAdvancedTemplates()) ? planConfigurationsBean.getAdvancedTemplates() : "0";
            final String metricCorrelation = Utils.isNotNull(planConfigurationsBean.getMetricCorrelation()) ? planConfigurationsBean.getMetricCorrelation() : "0";
            final String apiKey = Utils.isNotNull(planConfigurationsBean.getApiKey()) ? planConfigurationsBean.getApiKey() : "0";
            final String whatfix = Utils.isNotNull(planConfigurationsBean.getWhatfix()) ? planConfigurationsBean.getWhatfix() : "0";
            final String multipleSublink = Utils.isNotNull(planConfigurationsBean.getMultipleSublink()) ? planConfigurationsBean.getMultipleSublink() : "0";
            final String advancedAnalysis = Utils.isNotNull(planConfigurationsBean.getAdvancedAnalysis()) ? planConfigurationsBean.getAdvancedAnalysis() : "0";
            final String exportLinks = Utils.isNotNull(planConfigurationsBean.getExportLinks()) ? planConfigurationsBean.getExportLinks() : "0";
            final String respondentTracker = Utils.isNotNull(planConfigurationsBean.getRespondentTracker()) ? planConfigurationsBean.getRespondentTracker() : "0";
            final String fromCustomization = Utils.isNotNull(planConfigurationsBean.getFromCustomization()) ? planConfigurationsBean.getFromCustomization() : "0";
            final String integrations = Utils.isNotNull(planConfigurationsBean.getIntegrations()) ? planConfigurationsBean.getIntegrations() : "0";
            final Integer noOfIntegrations = Utils.isNotNull(planConfigurationsBean.getNoOfIntegrations()) ? planConfigurationsBean.getNoOfIntegrations() : Constants.LIMIT_UNLIMITED;
            final Integer noOfKiosks = Utils.isNotNull(planConfigurationsBean.getNoOfKiosks()) ? planConfigurationsBean.getNoOfKiosks() : Constants.LIMIT_UNLIMITED;
            final String dynamicLinks = Utils.isNotNull(planConfigurationsBean.getDynamicLinks()) ? planConfigurationsBean.getDynamicLinks() : "0";
            final Integer noOfShareableSublink = Utils.isNotNull(planConfigurationsBean.getNoOfShareableSublink()) ? planConfigurationsBean.getNoOfShareableSublink() : Constants.LIMIT_UNLIMITED;
            final Integer noOfMultipleStaticQr = Utils.isNotNull(planConfigurationsBean.getNoOfMultipleStaticQr()) ? planConfigurationsBean.getNoOfMultipleStaticQr() : Constants.LIMIT_UNLIMITED;
            final Integer noOfDynamicSwitchQr = Utils.isNotNull(planConfigurationsBean.getNoOfDynamicSwitchQr()) ? planConfigurationsBean.getNoOfDynamicSwitchQr() : Constants.LIMIT_UNLIMITED;
            final Integer noOfDynamicGroupQr = Utils.isNotNull(planConfigurationsBean.getNoOfDynamicGroupQr()) ? planConfigurationsBean.getNoOfDynamicGroupQr() : Constants.LIMIT_UNLIMITED;
            final String mfa = Utils.isNotNull(planConfigurationsBean.getMfa()) ? planConfigurationsBean.getMfa() : "0";
            final String advancedReport = Utils.isNotNull(planConfigurationsBean.getAdvancedReport()) ? planConfigurationsBean.getAdvancedReport() : "0";
            final String gamePlan = Utils.isNotNull(planConfigurationsBean.getGamePlan()) ? planConfigurationsBean.getGamePlan() : "0";
            final String previouslyUsedQuestionLibrary = Utils.isNotNull(planConfigurationsBean.getPreviouslyUsedQuestionLibrary()) ? planConfigurationsBean.getPreviouslyUsedQuestionLibrary() : "0";
            final String predefinedQuestionLibrary = Utils.isNotNull(planConfigurationsBean.getPredefinedQuestionLibrary()) ? planConfigurationsBean.getPredefinedQuestionLibrary() : "0";
            final String chatbot = Utils.isNotNull(planConfigurationsBean.getChatbot()) ? planConfigurationsBean.getChatbot() : "0";
            final String auditProgram = Utils.isNotNull(planConfigurationsBean.getAuditProgram()) ? planConfigurationsBean.getAuditProgram() : "0";
            final String collaboration = Utils.isNotNull(planConfigurationsBean.getCollaboration()) ? planConfigurationsBean.getCollaboration() : "0";
            final String customDashboard = Utils.isNotNull(planConfigurationsBean.getCustomDashboard()) ? planConfigurationsBean.getCustomDashboard() : "0";
            final String whatsapp = Utils.isNotNull(planConfigurationsBean.getWhatsapp()) ? planConfigurationsBean.getWhatsapp() : "0";
            final Integer noOfWhatsapp = Utils.isNotNull(planConfigurationsBean.getNoOfWhatsapp()) ? planConfigurationsBean.getNoOfWhatsapp() : Constants.LIMIT_UNLIMITED;
            final String rCoefficient = Utils.isNotNull(planConfigurationsBean.getRCoefficient()) ? planConfigurationsBean.getRCoefficient() : "0";
            final String notes = Utils.isNotNull(planConfigurationsBean.getNotes()) ? planConfigurationsBean.getNotes() : "";
            final String active = Utils.isNotNull(planConfigurationsBean.getActive()) ? planConfigurationsBean.getActive() : "0";
            final String taskManager = Utils.isNotNull(planConfigurationsBean.getTaskManager()) ? planConfigurationsBean.getTaskManager() : "0";
            final String aiSurveys = Utils.isNotNull(planConfigurationsBean.getAiSurveys()) ? planConfigurationsBean.getAiSurveys() : Constants.ZERO_STRING;
            Pricing pricing = Utils.isNotNull(planConfigurationsBean.getPricing()) ? planConfigurationsBean.getPricing() : null;
            String pricingJson = null;
            if (pricing != null) {
                pricingJson = new Gson().toJson(pricing);
            }
            List planProperties = Utils.isNotNull(planConfigurationsBean.getPlanProperties()) ? planConfigurationsBean.getPlanProperties() : new ArrayList();

            JSONArray finalPlanPropertiesJson = new JSONArray(planProperties);
            String finalPlanPropertiesJsonStr = finalPlanPropertiesJson.toString();
//            String planPropertiesJson = null;
//            if(planProperties != null) {
//                planPropertiesJson = new Gson().toJson(planProperties);
//            }

            final Integer trialDays = Utils.isNotNull(planConfigurationsBean.getTrialDays()) ? planConfigurationsBean.getTrialDays() : Constants.TRIAL_DAYS;
            Map focusMap = new HashMap();
            focusMap.put("focusMetric",Utils.isNotNull(planConfigurationsBean.getFocusMetric()) ? planConfigurationsBean.getFocusMetric() : "0");
            focusMap.put("defaultRecommendation",Utils.isNotNull(planConfigurationsBean.getDefaultRecommendation()) ? planConfigurationsBean.getDefaultRecommendation() : "0");
            focusMap.put("userDefinedRecommendation",Utils.isNotNull(planConfigurationsBean.getUserDefinedRecommendation()) ? planConfigurationsBean.getUserDefinedRecommendation() : "0");
            final String focusMetric = Utils.isNotNull(focusMap) ? new JSONObject(focusMap).toString() : new JSONObject().toString();
            //unique links
            Map uniqueMap = new HashMap();
            uniqueMap.put("uniqueLinkEmail",Utils.isNotNull(planConfigurationsBean.getUniqueLinkEmail()) ? planConfigurationsBean.getUniqueLinkEmail() : "0");
            uniqueMap.put("uniqueLinkSms",Utils.isNotNull(planConfigurationsBean.getUniqueLinkSms()) ? planConfigurationsBean.getUniqueLinkSms() : "0");
            uniqueMap.put("uniqueLinkWhatsapp",Utils.isNotNull(planConfigurationsBean.getUniqueLinkWhatsapp()) ? planConfigurationsBean.getUniqueLinkWhatsapp() : "0");
            final String uniqueLinks = Utils.isNotNull(uniqueMap) ? new JSONObject(uniqueMap).toString() : new JSONObject().toString();
            //program features
            Map programMap = new HashMap();
            programMap.put("programOverview",Utils.isNotNull(planConfigurationsBean.getProgramOverview()) ? planConfigurationsBean.getProgramOverview() : "0");
            programMap.put("programMetric",Utils.isNotNull(planConfigurationsBean.getProgramMetric()) ? planConfigurationsBean.getProgramMetric() : "0");
            final String programFeatures = Utils.isNotNull(programMap) ? new JSONObject(programMap).toString() : new JSONObject().toString();
            //sso
            Map ssoMap = new HashMap();
            ssoMap.put("googleSSO",Utils.isNotNull(planConfigurationsBean.getGoogleSSO()) ? planConfigurationsBean.getGoogleSSO() : "0");
            ssoMap.put("microsoftSSO",Utils.isNotNull(planConfigurationsBean.getMicrosoftSSO()) ? planConfigurationsBean.getMicrosoftSSO() : "0");
            ssoMap.put("appleSSO",Utils.isNotNull(planConfigurationsBean.getAppleSSO()) ? planConfigurationsBean.getAppleSSO() : "0");
            final String sso = Utils.isNotNull(ssoMap) ? new JSONObject(ssoMap).toString() : new JSONObject().toString();

            final String closeLoop = Utils.isNotNull(planConfigurationsBean.getCloseLoop()) ?
                    planConfigurationsBean.getCloseLoop() : "0";
            final String programThrottling = Utils.isNotNull(planConfigurationsBean.getProgramThrottling()) ?
                    planConfigurationsBean.getProgramThrottling() : "0";
            final String lti = Utils.isNotNull(planConfigurationsBean.getLti()) ?
                    planConfigurationsBean.getLti() : "0";
            final String ltiConfig = Utils.isNotNull(planConfigurationsBean.getLtiConfig()) ? new JSONObject(planConfigurationsBean.getLtiConfig()).toString() : new JSONObject().toString();
            //DTV-13080, 12326 response_quota, recurrence
            final String responseQuota = Utils.isNotNull(planConfigurationsBean.getResponseQuota()) ? planConfigurationsBean.getResponseQuota() : Constants.ZERO_STRING;
            final String recurrence = Utils.isNotNull(planConfigurationsBean.getRecurrence()) ? planConfigurationsBean.getRecurrence() : Constants.ZERO_STRING;
            final String customDashboardBuilder = Utils.isNotNull(planConfigurationsBean.getCustomDashboardBuilder()) ? planConfigurationsBean.getCustomDashboardBuilder() : Constants.ZERO_STRING;


            KeyHolder keyHolder = new GeneratedKeyHolder();
            String finalPricingJson = pricingJson;
            jdbcTemplate.update(new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(Constants.CREATE_PLAN_CONFIG, Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, planName);
                    ps.setString(2, email);
                    ps.setString(3, sms);
                    ps.setString(4, qrCode);
                    ps.setString(5, shareableLink);
                    ps.setString(6, textAnalytics);
                    ps.setInt(7, createdBy);
                    ps.setInt(8, modifiedBy);
                    ps.setTimestamp(9, createdTime);
                    ps.setTimestamp(10, modifiedTime);
                    ps.setString(11, planConfigUUID);
                    ps.setString(12, logic);
                    ps.setString(13, respondent);
                    ps.setString(14, segment);
                    ps.setString(15, kiosk);
                    ps.setString(16, metadataQuestion);
                    ps.setString(17, mobileApp);
                    ps.setString(18, emotionAnalysis);
                    ps.setString(19, intentAnalysis);
                    ps.setString(20, trigger);
                    ps.setString(21, advancedSchedule);
                    ps.setString(22, english);
                    ps.setString(23, arabic);
                    ps.setString(24, multiSurveys);
                    ps.setString(25, bitly);
                    ps.setString(26, webHooks);
                    ps.setString(27, preferredMetric);
                    ps.setString(28, categoryAnalysis);
                    ps.setString(29, advancedTextAnalytics);
                    ps.setInt(30, noOfUsers);
                    ps.setInt(31, noOfActivePrograms);
                    ps.setInt(32, noOfResponses);
                    ps.setInt(33, noOfMetrics);
                    ps.setInt(34, noOfSms);
                    ps.setInt(35, noOfApi);
                    ps.setInt(36, noOfFilters);
                    ps.setInt(37, noOfTriggers);
                    ps.setInt(38, dataRetention);
                    ps.setInt(39, noOfLists);
                    ps.setInt(40, noOfRecipients);
                    ps.setString(41,hippa);
                    ps.setString(42,cohortAnalysis);
                    ps.setString(43,advancedTemplates);
                    ps.setString(44, metricCorrelation);
                    ps.setString(45, apiKey);
                    ps.setString(46, whatfix);
                    ps.setString(47, multipleSublink);
                    ps.setString(48, advancedAnalysis);
                    ps.setString(49, exportLinks);
                    ps.setString(50, respondentTracker);
                    ps.setString(51, fromCustomization);
                    ps.setString(52, integrations);
                    ps.setInt(53, noOfIntegrations);
                    ps.setInt(54, noOfKiosks);
                    ps.setString(55, dynamicLinks);
                    ps.setInt(56, noOfShareableSublink);
                    ps.setInt(57, noOfMultipleStaticQr);
                    ps.setInt(58, noOfDynamicSwitchQr);
                    ps.setInt(59, noOfDynamicGroupQr);
                    ps.setString(60,mfa);
                    ps.setString(61,focusMetric);
                    ps.setString(62,advancedReport);
                    ps.setString(63,previouslyUsedQuestionLibrary);
                    ps.setString(64,predefinedQuestionLibrary);
                    ps.setString(65, customThemes);
                    ps.setString(66, chatbot);
                    ps.setString(67, auditProgram);
                    ps.setString(68, collaboration);
                    ps.setString(69, customDashboard);
                    ps.setString(70, whatsapp);
                    ps.setInt(71, noOfWhatsapp);
                    ps.setString(72,rCoefficient);
                    ps.setString(73,closeLoop);
                    ps.setString(74,programThrottling);
                    ps.setString(75,gamePlan);
                    ps.setString(76,notes);
                    ps.setString(77,active);
                    ps.setString(78, finalPricingJson);
                    ps.setInt(79,trialDays);
                    ps.setString(80,notification);
                    ps.setInt(81, noOfEmailsSent);
                    ps.setString(82, uniqueLinks);
                    ps.setString(83, programFeatures);
                    ps.setString(84, finalPlanPropertiesJsonStr);
                    ps.setString(85, taskManager);
                    ps.setString(86, aiSurveys);
                    ps.setString(87, lti);
                    ps.setString(88, ltiConfig);
                    ps.setString(89, sso);
                    ps.setString(90, responseQuota);
                    ps.setString(91, recurrence);
                    ps.setString(92, customDashboardBuilder);
                    return ps;
                }
            }, keyHolder);
            configurationsId = keyHolder.getKey().intValue();
            returnMap.put("insertStatus",configurationsId);
            returnMap.put("planConfigUUID",planConfigUUID);
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return returnMap;
    }

    /**
     * This method is used to delete plan configurations by plan id
     * @param planUUID
     * @return
     */
    public int deletePlanConfigByPlanId(String planUUID, int planId, boolean isDeleteByClient, String businessUUID) {
        int deleteStatus = 0;
        try {

            if (!isDeleteByClient) {
                // Need to re visit this case. If Some clients are in the middle of the plan, then we should not delete the plan.
                String DELETE_PLAN_BY_PLAN_UUID  = "delete from plan_configurations where plan_config_uuid = ?";
                //int planId = getPlanIdByUUID(planUUID);
                deleteStatus = jdbcTemplate.update(DELETE_PLAN_BY_PLAN_UUID, new Object[]{planUUID});
                if(deleteStatus > 0) {
                    logger.info("plan configurations deleted successfully");
                    logger.info("remove plan id from business table");
                    String UPDATE_BUSINESS_PLAN_ID = "update business set plan_config_id = 0 where plan_config_id = ?";
                    int updateStatus = jdbcTemplate.update(UPDATE_BUSINESS_PLAN_ID, new Object[]{planId});
            } else {
                    // update plan_config_id to 0 in business table for the particular client
                    String UPDATE_BUSINESS_PLAN_ID = "update business set plan_config_id = 0 where business_uuid = ?";
                    int updateStatus = jdbcTemplate.update(UPDATE_BUSINESS_PLAN_ID, new Object[]{businessUUID});
            }

                // method to sent mail to client for plan deletion
                sendMailForPlanUpdateOrDelete(planId, "delete", isDeleteByClient, businessUUID);

            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        return deleteStatus;
    }

    /**
     * This method is used to send mail to client for plan update or delete
     * @param planId
     * @param emailType
     */
    private void sendMailForPlanUpdateOrDelete(int planId, String emailType, boolean isUpdateOrDeleteByClient, String businessUUID) {
        try {
            List<Map<String, Object>>  businessList = new ArrayList<>();
                    String query = "select * from business where plan_config_id = ?";

            if (isUpdateOrDeleteByClient) {
                query = query + " and business_uuid = ?";
                businessList = jdbcTemplate.queryForList(query, new Object[]{planId, businessUUID});
            } else {
                businessList = jdbcTemplate.queryForList(query, new Object[]{planId});
            }

            // iterate the business list and send mail to each client
            for (Map<String, Object> businessMap : businessList) {
                String eachBusinessUUID = businessMap.containsKey("business_uuid") ? businessMap.get("business_uuid").toString() : "";
                int eachBusinessId = businessMap.containsKey("business_id") ? Integer.parseInt(businessMap.get("business_id").toString()) : 0;
                int planConfigId = businessMap.containsKey("plan_config_id") ? Integer.parseInt(businessMap.get("plan_config_id").toString()) : 0;
                String contractEndDate = businessMap.containsKey("contract_end_date") ? businessMap.get("contract_end_date").toString() : "";
                if (emailType.equalsIgnoreCase("update")) {

                    String planName = getPlanNameByPlanId(planConfigId);
                    contractEndDate = Utils.getFormattedDate(contractEndDate, "MMM dd yyyy");
                    integrationAppService.sendReminderSettingEmail(eachBusinessId, eachBusinessUUID, true,
                            planName, contractEndDate);
                } else if (emailType.equalsIgnoreCase("delete")) {
                    // This use case wont be there. As we are not deleting the plan from the system.
                    // We are just updating the plan configurations and the customer can either upgrade / down grade the plan.
                }
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }

    }

    /**
     * This method is used to retrieve plan id by plan uuid
     * @param configUUID
     * @return
     */
    public int retrievePlanIdByPlanUUID(String configUUID) {
        int planId = 0;
        try {
            String SELECT_CONFIGID_BY_UUID = "select plan_config_id from plan_configurations where plan_config_uuid=?";
            logger.info("begin retrieving plan id by uuid");
            Map planMap = jdbcTemplate.queryForMap(SELECT_CONFIGID_BY_UUID, new Object[]{configUUID});
            if (planMap.size() > 0) {
                planId = planMap.containsKey("plan_config_id") ? Integer.parseInt(planMap.get("plan_config_id").toString()) : 0;
            }
            logger.info("end retrieving plan id by uuid {}", configUUID);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return planId;
    }

    /**
     * This method is used to retrieve plan id by plan uuid
     * @param configID
     * @return
     */
    public String retrievePlanUUIDyPlanID(int configID) {
        String planUUId = "";
        try {
            String SELECT_CONFIGID_BY_ID = "select plan_config_uuid from plan_configurations where plan_config_id=?";
            logger.info("begin retrieving plan id by uuid");
            Map planMap = jdbcTemplate.queryForMap(SELECT_CONFIGID_BY_ID, new Object[]{configID});
            if (planMap.size() > 0) {
                planUUId = planMap.containsKey("plan_config_uuid") ? planMap.get("plan_config_uuid").toString() : "";
            }
            logger.info("end retrieving plan id by uuid {}", configID);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return planUUId;
    }

    /**
     * This method is used to convert and apply plan configurations to business configurations.
     * @param planConfigurationsBean
     */
    public void convertAndApplyPlanConfigurationsToBusinessConfigurations(PlanConfigurationsBean planConfigurationsBean,
                                                                          int businessId, String startDate, String endDate,
                                                                          String businessUUID) {
        BusinessConfigurationsBean returnBean = new BusinessConfigurationsBean();
        try {
            //TODO: optimize this code or think of better way to handle this.
            List businessList = new ArrayList();
            String planConfigUUID = planConfigurationsBean.getPlanConfigUUID();
            int planConfigId = retrievePlanIdByPlanUUID(planConfigUUID);
            List businessWithPlanUUID = jdbcTemplate.queryForList("select business_id from business where plan_config_id=?", new Object[]{planConfigId});
            logger.info("businesswithPlanUUID = " + businessWithPlanUUID);
            if (businessId == 0 && businessWithPlanUUID.size() > 0) {
                for (int it = 0; it < businessWithPlanUUID.size(); it++) {
                    Map businessMap = (Map) businessWithPlanUUID.get(it);
                    int businessIdforList = businessMap.containsKey("business_id") ? Integer.parseInt(businessMap.get("business_id").toString()) : 0;
                    businessList.add(businessIdforList);
                }
            } else {
                businessList.add(businessId);
            }
            logger.info("businessList = " + businessList);
            for (int i = 0; i < businessList.size(); i++) {
                returnBean.setBusinessId(Integer.parseInt(businessList.get(i).toString()));
                returnBean.setEmail(Utils.isNotNull(planConfigurationsBean.getEmail()) ? planConfigurationsBean.getEmail() : "0");
                returnBean.setSms(Utils.isNotNull(planConfigurationsBean.getSms()) ? planConfigurationsBean.getSms() : "0");
                returnBean.setQrcode(Utils.isNotNull(planConfigurationsBean.getQrcode()) ? planConfigurationsBean.getQrcode() : "0");
                returnBean.setShareableLink(Utils.isNotNull(planConfigurationsBean.getShareableLink()) ? planConfigurationsBean.getShareableLink() : "0");
                returnBean.setTextAnalytics(Utils.isNotNull(planConfigurationsBean.getTextAnalytics()) ? planConfigurationsBean.getTextAnalytics() : "0");
                returnBean.setLogic(Utils.isNotNull(planConfigurationsBean.getLogic()) ? planConfigurationsBean.getLogic() : "0");
                returnBean.setRespondent(Utils.isNotNull(planConfigurationsBean.getRespondent()) ? planConfigurationsBean.getRespondent() : "0");
                returnBean.setCreatedBy(Utils.isNotNull(planConfigurationsBean.getCreatedBy()) ? planConfigurationsBean.getCreatedBy() : 0);
                returnBean.setSegment(Utils.isNotNull(planConfigurationsBean.getSegment()) ? planConfigurationsBean.getSegment() : "0");
                returnBean.setKiosk(Utils.isNotNull(planConfigurationsBean.getKiosk()) ? planConfigurationsBean.getKiosk() : "0");
                returnBean.setMetadataQuestion(Utils.isNotNull(planConfigurationsBean.getMetadataQuestion()) ? planConfigurationsBean.getMetadataQuestion() : "0");
                returnBean.setMobileApp(Utils.isNotNull(planConfigurationsBean.getMobileApp()) ? planConfigurationsBean.getMobileApp() : "0");
                returnBean.setEmotionAnalysis(Utils.isNotNull(planConfigurationsBean.getEmotionAnalysis()) ? planConfigurationsBean.getEmotionAnalysis() : "0");
                returnBean.setIntentAnalysis(Utils.isNotNull(planConfigurationsBean.getIntentAnalysis()) ? planConfigurationsBean.getIntentAnalysis() : "0");
                returnBean.setTrigger(Utils.isNotNull(planConfigurationsBean.getTrigger()) ? planConfigurationsBean.getTrigger() : "0");
                returnBean.setAdvancedSchedule(Utils.isNotNull(planConfigurationsBean.getAdvancedSchedule()) ? planConfigurationsBean.getAdvancedSchedule() : "0");
                returnBean.setEnglish(Utils.isNotNull(planConfigurationsBean.getEnglish()) ? planConfigurationsBean.getEnglish() : "0");
                returnBean.setArabic(Utils.isNotNull(planConfigurationsBean.getArabic()) ? planConfigurationsBean.getArabic() : "0");
                returnBean.setMultiSurveys(Utils.isNotNull(planConfigurationsBean.getMultiSurveys()) ? planConfigurationsBean.getMultiSurveys() : "0");
                returnBean.setBitly(Utils.isNotNull(planConfigurationsBean.getBitly()) ? planConfigurationsBean.getBitly() : "0");
                returnBean.setWebHooks(Utils.isNotNull(planConfigurationsBean.getWebHooks()) ? planConfigurationsBean.getWebHooks() : "0");
                returnBean.setPreferredMetric(Utils.isNotNull(planConfigurationsBean.getPreferredMetric()) ? planConfigurationsBean.getPreferredMetric() : "0");
                returnBean.setCategoryAnalysis(Utils.isNotNull(planConfigurationsBean.getCategoryAnalysis()) ? planConfigurationsBean.getCategoryAnalysis() : "0");
                returnBean.setAdvancedTextAnalytics(Utils.isNotNull(planConfigurationsBean.getAdvancedTextAnalytics()) ? planConfigurationsBean.getAdvancedTextAnalytics() : "0");
                returnBean.setNoOfUsers(Utils.isNotNull(planConfigurationsBean.getNoOfUsers()) ? planConfigurationsBean.getNoOfUsers() : 0);
                returnBean.setNoOfActivePrograms(Utils.isNotNull(planConfigurationsBean.getNoOfActivePrograms()) ? planConfigurationsBean.getNoOfActivePrograms() : 0);
                returnBean.setNoOfResponses(Utils.isNotNull(planConfigurationsBean.getNoOfResponses()) ? planConfigurationsBean.getNoOfResponses() : 0);
                returnBean.setNoOfMetrics(Utils.isNotNull(planConfigurationsBean.getNoOfMetrics()) ? planConfigurationsBean.getNoOfMetrics() : 0);
                returnBean.setNoOfEmailsSent(Utils.isNotNull(planConfigurationsBean.getNoOfEmailsSent()) ? planConfigurationsBean.getNoOfEmailsSent() : 0);
                returnBean.setNoOfSms(Utils.isNotNull(planConfigurationsBean.getNoOfSms()) ? planConfigurationsBean.getNoOfSms() : 0);
                returnBean.setNoOfApi(Utils.isNotNull(planConfigurationsBean.getNoOfApi()) ? planConfigurationsBean.getNoOfApi() : 0);
                returnBean.setNoOfFilters(Utils.isNotNull(planConfigurationsBean.getNoOfFilters()) ? planConfigurationsBean.getNoOfFilters() : 0);
                returnBean.setNoOfTriggers(Utils.isNotNull(planConfigurationsBean.getNoOfTriggers()) ? planConfigurationsBean.getNoOfTriggers() : 0);
                returnBean.setDataRetention(Utils.isNotNull(planConfigurationsBean.getDataRetention()) ? planConfigurationsBean.getDataRetention() : 0);
                returnBean.setNoOfLists(Utils.isNotNull(planConfigurationsBean.getNoOfLists()) ? planConfigurationsBean.getNoOfLists() : 0);
                returnBean.setNoOfRecipients(Utils.isNotNull(planConfigurationsBean.getNoOfRecipients()) ? planConfigurationsBean.getNoOfRecipients() : 0);
                returnBean.setHippa(Utils.isNotNull(planConfigurationsBean.getHippa()) ? planConfigurationsBean.getHippa() : "0");
                returnBean.setCohortAnalysis(Utils.isNotNull(planConfigurationsBean.getCohortAnalysis()) ? planConfigurationsBean.getCohortAnalysis() : "0");
                returnBean.setAdvancedTemplates(Utils.isNotNull(planConfigurationsBean.getAdvancedTemplates()) ? planConfigurationsBean.getAdvancedTemplates() : "0");
                returnBean.setMetricCorrelation(Utils.isNotNull(planConfigurationsBean.getMetricCorrelation()) ? planConfigurationsBean.getMetricCorrelation() : "0");
                returnBean.setApiKey(Utils.isNotNull(planConfigurationsBean.getApiKey()) ? planConfigurationsBean.getApiKey() : "0");
                returnBean.setWhatfix(Utils.isNotNull(planConfigurationsBean.getWhatfix()) ? planConfigurationsBean.getWhatfix() : "0");
                returnBean.setMultipleSublink(Utils.isNotNull(planConfigurationsBean.getMultipleSublink()) ? planConfigurationsBean.getMultipleSublink() : "0");
                returnBean.setAdvancedAnalysis(Utils.isNotNull(planConfigurationsBean.getAdvancedAnalysis()) ? planConfigurationsBean.getAdvancedAnalysis() : "0");
                returnBean.setExportLinks(Utils.isNotNull(planConfigurationsBean.getExportLinks()) ? planConfigurationsBean.getExportLinks() : "0");
                returnBean.setRespondentTracker(Utils.isNotNull(planConfigurationsBean.getRespondentTracker()) ? planConfigurationsBean.getRespondentTracker() : "0");
                returnBean.setFromCustomization(Utils.isNotNull(planConfigurationsBean.getFromCustomization()) ? planConfigurationsBean.getFromCustomization() : "0");
                returnBean.setIntegrations(Utils.isNotNull(planConfigurationsBean.getIntegrations()) ? planConfigurationsBean.getIntegrations() : "0");
                returnBean.setNoOfIntegrations(Utils.isNotNull(planConfigurationsBean.getNoOfIntegrations()) ? planConfigurationsBean.getNoOfIntegrations() : 0);
                returnBean.setNoOfKiosks(Utils.isNotNull(planConfigurationsBean.getNoOfKiosks()) ? planConfigurationsBean.getNoOfKiosks() : 0);
                returnBean.setDynamicLinks(Utils.isNotNull(planConfigurationsBean.getDynamicLinks()) ? planConfigurationsBean.getDynamicLinks() : "0");
                returnBean.setNoOfShareableSublink(Utils.isNotNull(planConfigurationsBean.getNoOfShareableSublink()) ? planConfigurationsBean.getNoOfShareableSublink() : 0);
                returnBean.setNoOfMultipleStaticQr(Utils.isNotNull(planConfigurationsBean.getNoOfMultipleStaticQr()) ? planConfigurationsBean.getNoOfMultipleStaticQr() : 0);
                returnBean.setNoOfDynamicSwitchQr(Utils.isNotNull(planConfigurationsBean.getNoOfDynamicSwitchQr()) ? planConfigurationsBean.getNoOfDynamicSwitchQr() : 0);
                returnBean.setNoOfDynamicGroupQr(Utils.isNotNull(planConfigurationsBean.getNoOfDynamicGroupQr()) ? planConfigurationsBean.getNoOfDynamicGroupQr() : 0);
                returnBean.setMfa(Utils.isNotNull(planConfigurationsBean.getMfa()) ? planConfigurationsBean.getMfa() : "0");
                returnBean.setFocusMetric(Utils.isNotNull(planConfigurationsBean.getFocusMetric()) ? planConfigurationsBean.getFocusMetric() : "0");
                returnBean.setUniqueLinks(Utils.isNotNull(planConfigurationsBean.getUniqueLinks()) ? planConfigurationsBean.getUniqueLinks() : "0");
                returnBean.setProgramFeatures(Utils.isNotNull(planConfigurationsBean.getProgramFeatures()) ? planConfigurationsBean.getProgramFeatures() : "0");
                returnBean.setAdvancedReport(Utils.isNotNull(planConfigurationsBean.getAdvancedReport()) ? planConfigurationsBean.getAdvancedReport() : "0");
                returnBean.setGamePlan(Utils.isNotNull(planConfigurationsBean.getGamePlan()) ? planConfigurationsBean.getGamePlan() : "0");
                returnBean.setPreviouslyUsedQuestionLibrary(Utils.isNotNull(planConfigurationsBean.getPreviouslyUsedQuestionLibrary()) ? planConfigurationsBean.getPreviouslyUsedQuestionLibrary() : "0");
                returnBean.setPredefinedQuestionLibrary(Utils.isNotNull(planConfigurationsBean.getPredefinedQuestionLibrary()) ? planConfigurationsBean.getPredefinedQuestionLibrary() : "0");
                returnBean.setCustomThemes(Utils.isNotNull(planConfigurationsBean.getCustomThemes()) ? planConfigurationsBean.getCustomThemes() : "0");
                returnBean.setChatbot(Utils.isNotNull(planConfigurationsBean.getChatbot()) ? planConfigurationsBean.getChatbot() : "0");
                returnBean.setAuditProgram(Utils.isNotNull(planConfigurationsBean.getAuditProgram()) ? planConfigurationsBean.getAuditProgram() : "0");
                returnBean.setCollaboration(Utils.isNotNull(planConfigurationsBean.getCollaboration()) ? planConfigurationsBean.getCollaboration() : "0");
                returnBean.setCustomDashboard(Utils.isNotNull(planConfigurationsBean.getCustomDashboard()) ? planConfigurationsBean.getCustomDashboard() : "0");
                returnBean.setWhatsapp(Utils.isNotNull(planConfigurationsBean.getWhatsapp()) ? planConfigurationsBean.getWhatsapp() : "0");
                returnBean.setNoOfWhatsapp(Utils.isNotNull(planConfigurationsBean.getNoOfWhatsapp()) ? planConfigurationsBean.getNoOfWhatsapp() : 0);
                returnBean.setRCoefficient(Utils.isNotNull(planConfigurationsBean.getRCoefficient()) ? planConfigurationsBean.getRCoefficient() : "0");
                returnBean.setNotes(Utils.isNotNull(planConfigurationsBean.getNotes()) ? planConfigurationsBean.getNotes() : "0");
                returnBean.setTrialDays(Utils.isNotNull(planConfigurationsBean.getTrialDays()) ? planConfigurationsBean.getTrialDays() : Constants.TRIAL_DAYS);
                returnBean.setNotification(Utils.isNotNull(planConfigurationsBean.getNotification()) ? planConfigurationsBean.getNotification() : "0");
                returnBean.setCloseLoop(Utils.isNotNull(planConfigurationsBean.getCloseLoop()) ? planConfigurationsBean.getCloseLoop() : "0");
                returnBean.setProgramThrottling(Utils.isNotNull(planConfigurationsBean.getProgramThrottling()) ? planConfigurationsBean.getProgramThrottling() : "0");
                returnBean.setSso(Utils.isNotNull(planConfigurationsBean.getSSO()) ? planConfigurationsBean.getSSO() : null);
                returnBean.setAiSurveysLimit(Utils.isNotNull(planConfigurationsBean.getAiSurveysLimit()) ? planConfigurationsBean.getAiSurveysLimit() : Constants.AISURVEYS_LIMIT);
                returnBean.setAiSurveys(Utils.isNotNull(planConfigurationsBean.getAiSurveys()) ? planConfigurationsBean.getAiSurveys() : "0");
                returnBean.setLti(Utils.isNotNull(planConfigurationsBean.getLti()) ? planConfigurationsBean.getLti() : "0");
                returnBean.setLtiConfig(Utils.isNotNull(planConfigurationsBean.getLtiConfig()) ? planConfigurationsBean.getLtiConfig() : null);
                //DTV-12386, 13080
                returnBean.setResponseQuota(Utils.isNotNull(planConfigurationsBean.getResponseQuota()) ? planConfigurationsBean.getResponseQuota() : Constants.ZERO_STRING);
                returnBean.setRecurrence(Utils.isNotNull(planConfigurationsBean.getRecurrence()) ? planConfigurationsBean.getRecurrence() : Constants.ZERO_STRING);
                returnBean.setCustomDashboardBuilder(Utils.isNotNull(planConfigurationsBean.getCustomDashboardBuilder()) ? planConfigurationsBean.getCustomDashboardBuilder() : Constants.ZERO_STRING);
                //DTV-12525. For dt lite customers, text analytics plan is 1 by default
                if (businessId != 0 && planConfigId == 3 && Utils.notEmptyString(businessUUID)) {
                    String textAnalyticsConfigUUID = tokenUtility.generateUUID();
                    int txtAnalyticsConfigId = insertTextAnalyticsConfigurations(businessId, startDate, endDate, textAnalyticsConfigUUID);
                    returnBean.setTextAnalyticsConfigId(txtAnalyticsConfigId);
                    returnBean.setTextAnalytics("1");
                    TextAnalyticsConfigBean bean = taService.getTextAnalyticsConfig(textAnalyticsConfigUUID);
                    Map pendingConfig = Utils.isNotNull(bean.getConfigs()) && Utils.notEmptyString(bean.getConfigs()) ? mapper.readValue(bean.getConfigs(), HashMap.class) : new HashMap();
                    businessConfigurationsService.upsertTextAnalytics(new JSONObject(pendingConfig).toString(), businessUUID);
                }

                businessConfigurationsService.createBusinessConfigurations(returnBean, true);
                businessConfigVersionService.createBusinessVersionConfigurations(returnBean, true);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * This method is used to insert text analytics configurations for dt lite customers
     * @param businessId
     * @return
     */
    private int insertTextAnalyticsConfigurations(int businessId, String startDate, String endDate, String configUniqueId) {

        logger.info("begin createTextAnalyticsConfig");
        int textAnalyticsConfigId = 0;
        try {

            int plan = 1; // for dt lite customers plan is 1 by default. ie, basic text analytics
            int bId = businessId;
            int createdBy = 0;
            TextAnalyticsRequestBean bean = new TextAnalyticsRequestBean();
            Map<String, Object> configMap = new HashMap<>();
            // Populate the map with the required data
            configMap.put("categoryMlId", 0);
            configMap.put("wordCloud", new ArrayList<String>()); // Empty list
            configMap.put("dataDurationId", 2);
            configMap.put("emotionMlId", "");
            configMap.put("enabled", true);
            configMap.put("endDate", startDate);
            configMap.put("intentMlId", "");
            configMap.put("lastUpdated", "");
            configMap.put("sentimentMlId", new ArrayList<String>()); // Empty list
            configMap.put("startDate", endDate);
            configMap.put("textAnalyticsPlanId", 1);

            bean.setPlanConfigs(configMap);

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(INSERT_TA_CONFIG, Statement.RETURN_GENERATED_KEYS);
                    ps.setInt(1, bId);
                    ps.setString(2, bean.getTextAnalytics());
                    ps.setInt(3, plan);
                    ps.setString(4, new JSONObject(bean.getPlanConfigs()).toString());
                    ps.setString(5, new JSONArray().toString());
                    ps.setString(6, configUniqueId);
                    ps.setString(7, new JSONArray().toString());
                    ps.setInt(8, createdBy);
                    ps.setString(9, "1");
                    return ps;
                }
            }, keyHolder);
            textAnalyticsConfigId = keyHolder.getKey().intValue();

        } catch (Exception e) {
            logger.error("Exception at crateTextAnalyticsConfig. Msg {}", e.getMessage());
        }
        logger.info("end createTextAnalyticsConfig");

        return  textAnalyticsConfigId;
    }

    public PlanConfigurationsBean getPlanConfigByPlanConfigUUID(String planUUID)
    {
        PlanConfigurationsBean planConfigurationsBean = new PlanConfigurationsBean();
        try{
            List result = new ArrayList();
            logger.info("begin retrieving configs by business id");
            String GET_PLAN_CONFIGURATIONS_BY_PLANID = "select * from plan_configurations where plan_config_uuid = ?";
            result = jdbcTemplate.queryForList(GET_PLAN_CONFIGURATIONS_BY_PLANID, new Object[]{planUUID});

            Iterator<Map> iterator = result.iterator();
            while (iterator.hasNext()) {
                Map planConfigMap = iterator.next();
                logger.info("planCOnfigmap = "+planConfigMap);
                planConfigurationsBean.setAdvancedAnalysis(Utils.isNotNull(planConfigMap.get("advanced_analysis")) ? planConfigMap.get("advanced_analysis").toString() : "0");
                planConfigurationsBean.setAdvancedReport(planConfigMap.get("advanced_report").toString());
                planConfigurationsBean.setGamePlan(planConfigMap.get("game_plan").toString());
                planConfigurationsBean.setArabic(planConfigMap.get("arabic_toggle").toString());
                planConfigurationsBean.setApiKey(planConfigMap.get("api_key").toString());
                planConfigurationsBean.setAdvancedSchedule(planConfigMap.get("advanced_schedule").toString());
                planConfigurationsBean.setAdvancedTemplates(planConfigMap.get("advanced_templates").toString());
                planConfigurationsBean.setAdvancedTextAnalytics(planConfigMap.get("advanced_text_analytics").toString());
                planConfigurationsBean.setAuditProgram(planConfigMap.get("audit_program").toString());
                planConfigurationsBean.setBitly(planConfigMap.get("bitly").toString());
                planConfigurationsBean.setChatbot(planConfigMap.get("chatbot").toString());
                planConfigurationsBean.setCollaboration(planConfigMap.get("collaboration").toString());
                planConfigurationsBean.setCategoryAnalysis(planConfigMap.get("category_analysis").toString());
                planConfigurationsBean.setCohortAnalysis(planConfigMap.get("cohort_analysis").toString());
                planConfigurationsBean.setCreatedBy(Integer.parseInt(planConfigMap.get("created_by").toString()));
                planConfigurationsBean.setCustomDashboard(planConfigMap.get("custom_dashboard").toString());
                planConfigurationsBean.setCustomThemes(planConfigMap.get("custom_themes").toString());
                planConfigurationsBean.setDataRetention(Integer.parseInt(planConfigMap.get("data_retention").toString()));
                planConfigurationsBean.setDynamicLinks(planConfigMap.get("dynamic_links").toString());
                planConfigurationsBean.setEmail(planConfigMap.get("email").toString());
                planConfigurationsBean.setEnglish(planConfigMap.get("english_toggle").toString());
                planConfigurationsBean.setEmotionAnalysis(planConfigMap.get("emotion_analysis").toString());
                planConfigurationsBean.setExportLinks(planConfigMap.get("export_links").toString());
                planConfigurationsBean.setFocusMetric(planConfigMap.get("focus_metric").toString());
                planConfigurationsBean.setProgramFeatures(planConfigMap.get("program_features").toString());
                planConfigurationsBean.setUniqueLinks(planConfigMap.get("unique_links").toString());
                planConfigurationsBean.setSSO(Utils.isNotNull(planConfigMap.get("sso")) ? planConfigMap.get("sso").toString() : null);
                planConfigurationsBean.setFromCustomization(planConfigMap.get("from_customization").toString());
                planConfigurationsBean.setHippa(planConfigMap.get("hippa").toString());
                planConfigurationsBean.setIntegrations(planConfigMap.get("integrations").toString());
                planConfigurationsBean.setKiosk(planConfigMap.get("kiosk").toString());
                planConfigurationsBean.setLogic(planConfigMap.get("logic").toString());
                planConfigurationsBean.setMfa(planConfigMap.get("mfa").toString());
                planConfigurationsBean.setMetadataQuestion(planConfigMap.get("metadata_question").toString());
                planConfigurationsBean.setMetricCorrelation(planConfigMap.get("metric_correlation").toString());
                planConfigurationsBean.setMobileApp(planConfigMap.get("mobile_app").toString());
                planConfigurationsBean.setModifiedBy(Utils.isNotNull(planConfigMap.get("modified_by")) ? Integer.parseInt(planConfigMap.get("modified_by").toString()) : 0);
                planConfigurationsBean.setMultipleSublink(planConfigMap.get("multiple_sublink").toString());
                planConfigurationsBean.setMultiSurveys(planConfigMap.get("multi_surveys").toString());
                planConfigurationsBean.setNoOfLists(Integer.parseInt(planConfigMap.get("no_of_lists").toString()));
                planConfigurationsBean.setNoOfResponses(Integer.parseInt(planConfigMap.get("no_of_responses").toString()));
                planConfigurationsBean.setNoOfMetrics(Integer.parseInt(planConfigMap.get("no_of_metrics").toString()));
                planConfigurationsBean.setNoOfFilters(Integer.parseInt(planConfigMap.get("no_of_filters").toString()));
                planConfigurationsBean.setNoOfMultipleStaticQr(Integer.parseInt(planConfigMap.get("no_of_multiple_static_qr").toString()));
                planConfigurationsBean.setNoOfEmailsSent(Integer.parseInt(planConfigMap.get("no_of_emails_sent").toString()));
                planConfigurationsBean.setNoOfApi(Integer.parseInt(planConfigMap.get("no_of_api").toString()));
                planConfigurationsBean.setNoOfUsers(Integer.parseInt(planConfigMap.get("no_of_users").toString()));
                planConfigurationsBean.setNoOfActivePrograms(Integer.parseInt(planConfigMap.get("no_of_active_programs").toString()));
                planConfigurationsBean.setNoOfDynamicGroupQr(Integer.parseInt(planConfigMap.get("no_of_dynamic_group_qr").toString()));
                planConfigurationsBean.setNoOfDynamicSwitchQr(Integer.parseInt(planConfigMap.get("no_of_dynamic_switch_qr").toString()));
                planConfigurationsBean.setNotification(Utils.isNotNull(planConfigMap.get("notification")) ? planConfigMap.get("notification").toString() : "0");
                planConfigurationsBean.setNoOfTriggers(Integer.parseInt(planConfigMap.get("no_of_triggers").toString()));
                planConfigurationsBean.setNoOfShareableSublink(Integer.parseInt(planConfigMap.get("no_of_shareable_sublink").toString()));
                planConfigurationsBean.setNoOfRecipients(Integer.parseInt(planConfigMap.get("no_of_recipients").toString()));
                planConfigurationsBean.setNoOfSms(Integer.parseInt(planConfigMap.get("no_of_sms").toString()));
                planConfigurationsBean.setNoOfIntegrations(Integer.parseInt(planConfigMap.get("no_of_integrations").toString()));
                planConfigurationsBean.setPlanName(planConfigMap.get("plan_name").toString());
                planConfigurationsBean.setPlanConfigId(Integer.parseInt(planConfigMap.get("plan_config_id").toString()));
                planConfigurationsBean.setPlanConfigUUID(planConfigMap.get("plan_config_uuid").toString());
                planConfigurationsBean.setPreviouslyUsedQuestionLibrary(planConfigMap.get("previously_used_question_library").toString());
                planConfigurationsBean.setPredefinedQuestionLibrary(planConfigMap.get("predefined_question_library").toString());
                planConfigurationsBean.setPreferredMetric(planConfigMap.get("preferred_metric").toString());
                planConfigurationsBean.setQrcode(planConfigMap.get("qrcode").toString());
                planConfigurationsBean.setRespondentTracker(planConfigMap.get("respondent_tracker").toString());
                planConfigurationsBean.setRespondent(planConfigMap.get("respondent").toString());
                planConfigurationsBean.setTextAnalytics(planConfigMap.get("text_analytics").toString());
                planConfigurationsBean.setTrigger(planConfigMap.get("trigger_toggle").toString());
                planConfigurationsBean.setSegment(planConfigMap.get("segment").toString());
                planConfigurationsBean.setSms(planConfigMap.get("sms").toString());
                planConfigurationsBean.setShareableLink(planConfigMap.get("shareable_link").toString());
                planConfigurationsBean.setWhatfix(planConfigMap.get("whatfix").toString());
                planConfigurationsBean.setWebHooks(planConfigMap.get("web_hooks").toString());
                planConfigurationsBean.setWhatsapp(planConfigMap.get("whatsapp").toString());
                planConfigurationsBean.setNoOfWhatsapp(Integer.parseInt(planConfigMap.get("no_of_whatsapp").toString()));
                planConfigurationsBean.setRCoefficient(planConfigMap.get("r_coefficient").toString());
                planConfigurationsBean.setNotes(planConfigMap.get("notes").toString());
                planConfigurationsBean.setActive(planConfigMap.get("active").toString());
                planConfigurationsBean.setAiSurveys(planConfigMap.get("ai_surveys").toString());
                planConfigurationsBean.setTaskManager(planConfigMap.get("task_manager").toString());
                planConfigurationsBean.setAiSurveysLimit(planConfigMap.get("ai_surveys_limit").toString());
                planConfigurationsBean.setLti(planConfigMap.get("lti").toString());
                planConfigurationsBean.setLtiConfig(Utils.isNotNull(planConfigMap.get("lti_config")) ? mapper.readValue(planConfigMap.get("lti_config").toString(), HashMap.class) : null);
                String pricingStr = Utils.isNotNull(planConfigMap.get("pricing")) && planConfigMap.get("pricing") != null
                        ? planConfigMap.get("pricing").toString() : "";
                if (pricingStr != null && !pricingStr.isEmpty() && !pricingStr.equals("0")) {
                    ObjectMapper mapper = new ObjectMapper();
                    Pricing pricing = null;
                    try {
                        pricing = mapper.readValue(pricingStr, Pricing.class);
                    } catch (Exception e) {
                        logger.error("Error in parsing pricing json string");
                    }
                    planConfigurationsBean.setPricing(pricing);
                }
                // dt-lite fix : java.lang.String cannot be cast to java.util.List
                //List planProperties = Utils.isNotNull(planConfigMap.get("plan_properties")) ? (List) planConfigMap.get("plan_properties") : new ArrayList<>();
                String eachPlanProperties = planConfigMap.containsKey("plan_properties") && Utils.isNotNull(planConfigMap.get("plan_properties"))
                        ? planConfigMap.get("plan_properties").toString() : "";
                List planProperties = Utils.notEmptyString(eachPlanProperties) ?
                        mapper.readValue(eachPlanProperties, ArrayList.class) : new ArrayList<>();
                planConfigurationsBean.setPlanProperties(planProperties);
                planConfigurationsBean.setTrialDays(Integer.parseInt(planConfigMap.get("trial_days").toString()));
                planConfigurationsBean.setCloseLoop(Utils.isNotNull(planConfigMap.get("close_loop")) ? planConfigMap.get("close_loop").toString() : "0");
                planConfigurationsBean.setProgramThrottling(Utils.isNotNull(planConfigMap.get("program_throttling")) ? planConfigMap.get("program_throttling").toString() : "0");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return planConfigurationsBean;
    }

    public int updatePlanIdinBusinessTable(String businessUUID, String planUUID)
    {
        int result = 0;
        try{
            String tableName = "business";
            int planConfigId = retrievePlanIdByPlanUUID(planUUID);
            String updateSql = "update "+tableName+" set plan_config_id = ? where business_uuid = ?";
            result = jdbcTemplate.update(updateSql, new Object[]{planConfigId, businessUUID});

            // create balance and transaction tables for the client if not exists
            createBalanceAndTransactionTables(businessUUID);

            // sent mail to client for plan update with the list of features attached
            sendMailForPlanUpdateOrDelete(planConfigId, "update", true, businessUUID);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * This method is used to create balance and transaction tables for the client if not exists
     * @param businessUUID
     * @return
     */
    private int createBalanceAndTransactionTables(String businessUUID) {

        int tableCreated = 0;

        String replacedBusinessUuid = businessUUID.replace("-", "_");
        String tableName = "balance_" + replacedBusinessUuid;
        logger.debug("begin creating balance table ", tableName);
        try {
            String createSql = "CREATE TABLE IF NOT EXISTS " + tableName + " (`id` int(10) NOT NULL AUTO_INCREMENT," +
                    "`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, " +
                    "`amount` int(10) NOT NULL, " +
                    "`currency` varchar(10) NOT NULL, " +
                    "`created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, " +
                    "`balance_uuid` varchar(50) NOT NULL, " +
                    "`next_payment_date` timestamp NOT NULL, " +
                    "`status` int(1) NOT NULL, " +
                    " PRIMARY KEY (`id`)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";
            tableCreated = jdbcTemplate.update(createSql);
            logger.debug("Create table result {}", tableCreated);

        } catch (Exception e) {
            logger.error("Error in createBalanceTable() : " + e.getMessage());
        }
        logger.debug("end creating balance table");

        // create transaction table
        String transactionTableName = "transaction_" + replacedBusinessUuid;
        logger.debug("begin creating transaction table ", transactionTableName);
        try {
            String createSql = "CREATE TABLE IF NOT EXISTS " + transactionTableName + " (`id` int(10) NOT NULL AUTO_INCREMENT," +
                    "`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, " +
                    "`amount` int(10) NOT NULL, " +
                    "`currency` varchar(10) NOT NULL, " +
                    "`created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, " +
                    "`checkout_session_id` varchar(50) NULL DEFAULT NULL, " +
                    "`status` int(1) NOT NULL, " +
                    "`plan_id` int(10) NOT NULL, " +
                    " PRIMARY KEY (`id`)" +
                    //" KEY `balance_id` (`balance_id`), " +
                    //" CONSTRAINT `balance_id` FOREIGN KEY (`balance_id`) REFERENCES `" + balanceTableName + "` (`id`)" +
                    ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";
            tableCreated = jdbcTemplate.update(createSql);
            logger.debug("Create table result {}", tableCreated);

        } catch (Exception e) {
            logger.error("Error in createTransactionTable() : " + e.getMessage());
        }
        logger.debug("end creating transaction table");

        return tableCreated;
    }

    public Map<String, Object> getBusinessIdHippaByUUID(String businessUUID) {
        Map<String, Object> resultMap = new HashMap<>();
        try {
            String sql = "select business_id, hippa, contract_start_date, contract_end_date from business where business_uuid = ?";
            resultMap = jdbcTemplate.queryForMap(sql, new Object[]{businessUUID});
        } catch (Exception e) {
            logger.info("business id not found for the given business uuid");
        }
        return resultMap;
    }

    public List getActivePlans()
    {
        List returnList = new ArrayList();
        List result = new ArrayList();
        try{
            String tableName = "plan_configurations";
            String sql = "SELECT plan_config_id,plan_config_uuid, plan_name, pricing, plan_properties FROM "+ tableName +" WHERE ACTIVE = '1'";
            returnList = jdbcTemplate.queryForList(sql);
            for(int i=0; i< returnList.size();i++)
            {
                Map eachMap = (Map)returnList.get(i);
                String eachPlanUUID = eachMap.get("plan_config_uuid").toString();
                int eachPlanId = Integer.parseInt(eachMap.get("plan_config_id").toString());
                String eachPlanName = eachMap.get("plan_name").toString();
//                String eachPriceString = eachMap.containsKey("pricing") && Utils.isNotNull(eachMap.get("pricing")) ? eachMap.get("pricing").toString() : "";
//                Map eachPriceMap = mapper.readValue(eachPriceString, HashMap.class);
                List priceList = getCurrencyList();
                String eachPlanProperties = eachMap.containsKey("plan_properties") && Utils.isNotNull(eachMap.get("plan_properties")) ? eachMap.get("plan_properties").toString() : "";
                List eachPlanPropertiesList = Utils.notEmptyString(eachPlanProperties) ? mapper.readValue(eachPlanProperties, ArrayList.class) : new ArrayList<>();
                Map eachPlanMap = getActivePlanConfigurationsByPlanUUID(eachPlanUUID);
                List distributionList = (List) eachPlanMap.get("distribution");
                List featureAccessList = (List) eachPlanMap.get("feature_access");
                List limitationList = (List) eachPlanMap.get("limitation");
                Map planMap = new HashMap();
                planMap.put("planId", eachPlanId);
                planMap.put("planName", eachPlanName);
                planMap.put("pricing", priceList);
                planMap.put("planProperties", eachPlanPropertiesList);
                planMap.put("planUUID",eachPlanUUID);
                planMap.put("plan",eachPlanMap);
                planMap.put("planDataDistribution",distributionList);
                planMap.put("planDataFeatureAccess", featureAccessList);
                planMap.put("planDataLimitation",limitationList);

                result.add(planMap);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Function to get  plan configurations by planId
     * @param planUUID
     * @return
     */
    public Map getActivePlanConfigurationsByPlanUUID(String planUUID) {
        List result = new ArrayList();
        Map returnMap = new HashMap();
        List planConfigurationListFeatureAccess = new ArrayList();
        List planConfigurationListLimitation = new ArrayList();
        List planConfigurationListDistribution = new ArrayList();
        logger.info("begin retrieving configs by business id");
        try {
            String GET_PLAN_CONFIGURATIONS_BY_PLANID = "select * from plan_configurations where plan_config_uuid = ?";
            result = jdbcTemplate.queryForList(GET_PLAN_CONFIGURATIONS_BY_PLANID, new Object[]{planUUID});


            Iterator<Map> iterator = result.iterator();
            while (iterator.hasNext()) {
                Map<String,Object> planConfigMap = iterator.next();
                for (String name : planConfigMap.keySet()) {
                    Map eachKeyMap = new HashMap();
                    String value = Utils.isNotNull(planConfigMap.get(name)) ? planConfigMap.get(name).toString() : "";
                    String nameChanged = StringUtils.capitalize(name.replace("_"," ").toString());
                    if(!name.equalsIgnoreCase("plan_config_id")&&!name.equalsIgnoreCase("plan_name")&&!name.equalsIgnoreCase("pricing")&&!name.equalsIgnoreCase("plan_config_uuid")
                      &&!name.equalsIgnoreCase("modified_time")&&!name.equalsIgnoreCase("created_time")&&!name.equalsIgnoreCase("created_by")&&!name.equalsIgnoreCase("modified_by")&&!name.equalsIgnoreCase("plan_properties"))
                    {
                        eachKeyMap.put("id",name);
                        eachKeyMap.put("value",value);
                        eachKeyMap.put("name",nameChanged);
                        eachKeyMap.put("displayName", planFieldNames.containsKey(name) ? planFieldNames.get(name) : nameChanged);
                        if(name.equalsIgnoreCase("Email")||name.equalsIgnoreCase("Sms")||name.equalsIgnoreCase("qrcode")||name.equalsIgnoreCase("shareable_link")
                        ||name.equalsIgnoreCase("kiosk")||name.equalsIgnoreCase("whatsapp"))
                        {
                            planConfigurationListDistribution.add(eachKeyMap);
                        }
                        else if(name.startsWith("no_of_")||name.equalsIgnoreCase("data_retention"))
                        {
                            planConfigurationListLimitation.add(eachKeyMap);
                        }
                        else {
                            planConfigurationListFeatureAccess.add(eachKeyMap);
                        }
                    }
                }
                returnMap.put("feature_access",planConfigurationListFeatureAccess);
                returnMap.put("distribution",planConfigurationListDistribution);
                returnMap.put("limitation",planConfigurationListLimitation);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("end retrieving configs by plan id");
        return returnMap;
    }


    /**
     * This method is used to get the plan name map for the given business list
     * @param businessList
     * @return
     */
    public Map<Integer, String> getPlanNameMap(List<Map<String, Object>> businessList) {

        Map<Integer, String> planNameMap = new HashMap<>();
        try {
            Set<Integer> planConfigIds = businessList.stream().map(business -> (int) business.get("plan_config_id"))
                    .collect(Collectors.toSet());
            // get the plan name details for the plan_config_ids
            if (!planConfigIds.isEmpty()) {
                String planConfigQuery = "select plan_config_id, plan_name from plan_configurations where plan_config_id in" +
                        " (" + planConfigIds.stream().map(String::valueOf).collect(Collectors.joining(",")) + ")";
                List<Map<String, Object>> planConfigs = jdbcTemplate.queryForList(planConfigQuery);
                for (Map<String, Object> planConfig : planConfigs) {
                    int planConfigId = (int) planConfig.get("plan_config_id");
                    String planName = planConfig.get("plan_name").toString();
                    planNameMap.put(planConfigId, planName);
                }
            }
        } catch (Exception e) {
            logger.error("Error in getPlanNameMap() : " + e.getMessage());
        }

        return  planNameMap;
    }

    /**
     *  This method is used to get the plan config id for the given business id
     * @param eachBusinessId
     * @return
     */
    public int getPlanIdByBusinessId(int eachBusinessId) {
        int planId = 0;
        try {
            String SELECT_PLAN_ID_BY_BUSINESS_ID = "select plan_config_id from business where business_id = ?";
            logger.info("begin retrieving plan id by business id");
            Map planMap = jdbcTemplate.queryForMap(SELECT_PLAN_ID_BY_BUSINESS_ID, new Object[]{eachBusinessId});
            if (planMap.size() > 0) {
                planId = planMap.containsKey("plan_config_id") ? Integer.parseInt(planMap.get("plan_config_id").toString()) : 0;
            }
            logger.info("end retrieving plan id by business id {}", eachBusinessId);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return planId;
    }

    public String getPlanNameByPlanId(int planId) {

        String planName = "";
        try {
            String SELECT_PLAN_NAME_BY_PLAN_ID = "select plan_name from plan_configurations where plan_config_id = ?";
            logger.info("begin retrieving plan name by plan id");
            Map planMap = jdbcTemplate.queryForMap(SELECT_PLAN_NAME_BY_PLAN_ID, planId);
            if (planMap.size() > 0) {
                planName = planMap.containsKey("plan_name") ? planMap.get("plan_name").toString() : "";
            }
            logger.info("end retrieving plan name by plan id {}", planId);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return planName;
    }

    /**
     * This method is used to get the plan details for the given business id
     * @param eachBusinessId
     * @return
     */
    public Map<String, Object> getPlanByBusinessId(int eachBusinessId) {

        Map<String, Object> planMap = new HashMap<>();
        try {
            String SELECT_PLAN_BY_BUSINESS_ID = "select * from business where business_id = ?";
            logger.info("begin retrieving plan details by business id");
            planMap = jdbcTemplate.queryForMap(SELECT_PLAN_BY_BUSINESS_ID, new Object[]{eachBusinessId});
            logger.info("end retrieving plan details by business id {}", eachBusinessId);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return planMap;
    }

    public Map<String, Object> getPlanDetailsById(int planId) {

        Map<String, Object> planMap = new HashMap<>();
        try {
            String sql = "select * from plan_configurations where plan_config_id = ?";
            logger.info("begin retrieving plan details by business id");
            planMap = jdbcTemplate.queryForMap(sql, planId);
            logger.info("end retrieving plan details by business id {}", planId);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return planMap;
    }

    /**
     * This method is used to get the plan name map for the given business list
     * @return
     */
    public Map<Integer, String> getAllPlanIdAndNameMap() {
        Map<Integer, String> planNameMap = new HashMap<>();
        try {
            String planConfigQuery = "select plan_config_id, plan_name from plan_configurations";
            List<Map<String, Object>> planConfigs = jdbcTemplate.queryForList(planConfigQuery);
            planConfigs
                    .forEach(planConfig -> {
                        int planConfigId = (int) planConfig.getOrDefault("plan_config_id", 0);
                        String planName = planConfig.getOrDefault("plan_name", Constants.EMPTY_STRING).toString();
                        planNameMap.put(planConfigId, planName);
                    });
        } catch (Exception e) {
            logger.error("Error in getAllPlanIdAndNameMap {} : ", e.getMessage());
        }
        return  planNameMap;
    }

    public List getCurrencyList()
    {
        String priceSql = "select * from currencies";
        List currencyList = jdbcTemplate.queryForList(priceSql);
        return currencyList;
    }
}
