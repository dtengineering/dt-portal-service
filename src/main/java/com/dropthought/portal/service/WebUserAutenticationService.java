package com.dropthought.portal.service;

import com.dropthought.portal.model.UserBean;
import com.dropthought.portal.util.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class WebUserAutenticationService {

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();

    public String  select_user_by_email="select user_id, full_name, first_name, last_name, username, user_email, password, phone, confirmation_status, created_by, created_time, modified_time, modified_by, user_uuid from users where user_email = ?";

    public String select_uuid_by_userid="select user_uuid from users where user_id = ?";

    /* Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("jdbcWeb")
    private JdbcTemplate webJdbcTemplate;


    public List authenticateUser(String email, String password) {
        logger.info("begin authenticating user");
        List returnList = new ArrayList();
        int userStatus = 0;
        int confirmationStatus = 0;
        int usrID = 0;
        String select_user_by_email="select * from users where user_email =?";
        try {

            List<Map<String, Object>> result = webJdbcTemplate.queryForList(select_user_by_email, new Object[]{email});
            if (result.size() > 0) {
                Map userMap = result.get(0);
                UserBean userBean = new UserBean();

                if (userMap.containsKey("user_id")) {
                    userBean.setUserId((int) userMap.get("user_id"));
                    usrID = (int) userMap.get("user_id");
                }
                if (userMap.containsKey("user_status")) {
                    userBean.setUserStatus(userMap.get("user_status").toString());
                    userStatus = Integer.parseInt(userMap.get("user_status").toString());
                }
                if (userMap.containsKey("user_email")) {
                    userBean.setUserEmail(userMap.get("user_email").toString());
                }
                if (userMap.containsKey("password")) {
                    userBean.setPassword(userMap.get("password").toString());
                }

                if (userMap.containsKey("user_uuid")) {
                    String userUUID = userMap.get("user_uuid").toString();
                    userBean.setUserUUID(userUUID);

                }
                String userPassword = userBean.getPassword();
                String userId = userBean.getUserId().toString();


                MessageDigest digest = MessageDigest.getInstance("SHA-256");
                digest.update(password.getBytes());
                byte byteData[] = digest.digest();
                StringBuffer hexString = new StringBuffer();
                for (int i = 0; i < byteData.length; i++) {
                    String hex = Integer.toHexString(0xff & byteData[i]);
                    if (hex.length() == 1) hexString.append('0');
                    hexString.append(hex);
                }
                String stringHashPassword = hexString.toString();
                if (userPassword.equals(stringHashPassword)) {
                    returnList.add(0, userBean);
                }

                // to add last login time
                if (userStatus == 1 && usrID > 0) {
                    String currentDateTime = Utils.getCurrentUTCDateAsString();
                    String updateSql = "update users set last_login_time = ? where user_id = ?";
                    webJdbcTemplate.update(updateSql,new Object[]{currentDateTime, usrID});

                }
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("End authenticating user");
        return returnList;
    }




}
