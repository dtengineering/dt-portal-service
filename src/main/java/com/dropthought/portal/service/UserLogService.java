package com.dropthought.portal.service;

import com.dropthought.portal.model.UserLogBean;
import com.dropthought.portal.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.*;

@Component
public class UserLogService extends BaseService {
    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * JdbcTemplate for DB Connectivity.
     */
    @Autowired
    @Qualifier("jdbcDt")
    JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("jdbcNamedDt")
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    private ClientService clientService;

    public static final String create_activity = "insert into user_activity (business_id, username, user_email, location, ip_address,role, activity, created_by, created_time) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

    /**
     * method to save logs to db
     *
     * @param userLogBean
     * @return
     */
    public Map saveLogs(UserLogBean userLogBean) {
        Map resultMap = new HashMap();
        try {
            final int businessId = userLogBean.getBusinessId();
            final String username = userLogBean.getUsername();
            final String userEmail = userLogBean.getUserEmail();
            final String location = userLogBean.getLocation();
            final String ipAddress = userLogBean.getIpAddress();
            final String role = userLogBean.getRole();
            final String activity = userLogBean.getActivity();
            final int createdBy = userLogBean.getCreatedBy();
            final Timestamp createdTime = new Timestamp(System.currentTimeMillis());

            KeyHolder keyHolder = new GeneratedKeyHolder();
            if (businessId != 0) {
                jdbcTemplate.update(new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps = connection.prepareStatement(create_activity, Statement.RETURN_GENERATED_KEYS);
                        ps.setInt(1, businessId);
                        ps.setString(2, username);
                        ps.setString(3, userEmail);
                        ps.setString(4, location);
                        ps.setString(5, ipAddress);
                        ps.setString(6, role);
                        ps.setString(7, activity);
                        ps.setInt(8, createdBy);
                        ps.setTimestamp(9, createdTime);
                        return ps;
                    }
                }, keyHolder);
            }
            int result = keyHolder.getKey().intValue();
            if (result > 0) {
                resultMap.put("success", true);
                resultMap.put("message", "activity has been logged");
            } else {
                resultMap.put("success", false);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return resultMap;
    }

    /**
     * method to get user activity log
     *
     * @param clientId
     * @param role
     * @param userId
     * @param location
     * @param activity
     * @param fromDate
     * @param toDate
     * @param pageId
     * @param timeZone
     * @return
     */
    public List getLogs(String clientId, List role, List userId, List location, List activity, List userEmail, List username, String fromDate, String toDate, int pageId, String timeZone) {
        List resultList = new ArrayList();
        try {
            //fetch business id from business uuid
            int businessId = clientService.getBusinessIdByUUID(clientId);
            //convert from and to dates to UTC
            if (Utils.notEmptyString(fromDate) && Utils.notEmptyString(toDate)) {
                fromDate = Utils.getUTCDateAsString(fromDate, timeZone);
                toDate = Utils.getUTCDateAsString(toDate, timeZone);
            }
            resultList = getSortedLogs(businessId, role, userId, location, activity, userEmail, username, fromDate, toDate, pageId, timeZone);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return resultList;
    }

    /**
     * method to get user activity log based on filter/ sorting criterias
     *
     * @param businessId
     * @param role
     * @param userId
     * @param location
     * @param activity
     * @param fromDate
     * @param toDate
     * @param pageId
     * @param timeZone
     * @return
     */
    public List getSortedLogs(int businessId, List role, List userId, List location, List activity, List userEmail, List username, String fromDate, String toDate, int pageId, String timeZone) {
        List userActivity = new ArrayList();
        int upperLimit = 0;
        int length = 10;
        try {
            StringBuilder sqlSort = new StringBuilder();
            sqlSort.append("SELECT created_by, role, activity, ip_address, user_email, username, location, created_time FROM user_activity WHERE business_id = :businessId AND (");

            if (Utils.notEmptyString(fromDate) && Utils.notEmptyString(toDate)) {
                sqlSort.append("created_time BETWEEN :fromDate AND :toDate");
            }

            if (Utils.isNotNull(location) && location.size() > 0) {
                sqlSort.append(" AND location in (:location)");
            }

            if (Utils.isNotNull(role) && role.size() > 0) {
                sqlSort.append(" AND role in (:role)");
            }

            if (Utils.isNotNull(activity) && activity.size() > 0) {
                sqlSort.append(" AND activity in (:activity)");
            }

            if (Utils.isNotNull(userEmail) && userEmail.size() > 0) {
                sqlSort.append(" AND user_email in (:userEmail)");
            }

            if (Utils.isNotNull(username) && username.size() > 0) {
                sqlSort.append(" AND username in (:username)");
            }

            if (Utils.isNotNull(userId) && userId.size() > 0) {
                sqlSort.append(" AND created_by in (:userId)");
            }

            if (Utils.notEmptyString(fromDate) || Utils.notEmptyString(toDate)) {
                sqlSort.append(")");
            }

            sqlSort.append(" ORDER BY created_time DESC");

            if (pageId > -1) {
                if (pageId > -1) {
                    upperLimit = (pageId - 1) * length;
                    sqlSort.append(" limit :upperLimit, :length");
                } else {
                    sqlSort.append(" limit 1000");
                }
            }

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("businessId", businessId);
            parameterSource.addValue("role", role);
            parameterSource.addValue("location", location);
            parameterSource.addValue("activity", activity);
            parameterSource.addValue("userEmail", userEmail);
            parameterSource.addValue("username", username);
            parameterSource.addValue("fromDate", fromDate);
            parameterSource.addValue("toDate", toDate);
            parameterSource.addValue("userId", userId);
            parameterSource.addValue("toDate", toDate);
            parameterSource.addValue("upperLimit", upperLimit);
            parameterSource.addValue("length", length);

            List logs = namedParameterJdbcTemplate.queryForList(sqlSort.toString(), parameterSource);
            logger.info("sqlSort {}", sqlSort.toString());

            if (logs.size() > 0) {
                Iterator userLogIterator = logs.iterator();
                while (userLogIterator.hasNext()) {
                    Map eachUserLog = (Map) userLogIterator.next();
                    String createdTime = eachUserLog.containsKey("created_time") ? eachUserLog.get("created_time").toString() : "";
                    //convert timestamp to local timezone
                    createdTime = Utils.notEmptyString(createdTime) ? Utils.getDateAsStringByTimezone(createdTime, timeZone) : "";
                    eachUserLog.remove("created_time");
                    eachUserLog.put("createdTime", Utils.notEmptyString(createdTime) ? Utils.getFormattedDate(createdTime) : "");
                    Object usersEmail = eachUserLog.remove("user_email");
                    eachUserLog.put("userEmail", usersEmail);
                    Object ipAddress = eachUserLog.remove("ip_address");
                    eachUserLog.put("ipAddress", ipAddress);
                    userActivity.add(eachUserLog);
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return userActivity;
    }

    /**
     * method to get user log filter dropdown values
     *
     * @param clientId
     * @return
     */
    public Map getFilterDropdown(String clientId) {
        Map filterDropdown = new HashMap();
        try {
            //fetch business id from business uuid
            int businessId = clientService.getBusinessIdByUUID(clientId);
            try {
                //fetch location and activity values
                String userLogSql = "select location, activity from user_activity where business_id = ?";
                List resultListUser = jdbcTemplate.queryForList(userLogSql, new Object[]{businessId});
                if (resultListUser.size() > 0) {
                    List location = new ArrayList();
                    List activity = new ArrayList();
                    Iterator userListItr = resultListUser.iterator();
                    while (userListItr.hasNext()) {
                        Map eachUserMap = (Map) userListItr.next();
                        String eachLocation = eachUserMap.containsKey("location") ? eachUserMap.get("location").toString() : "";
                        String eachActivity = eachUserMap.containsKey("activity") ? eachUserMap.get("activity").toString() : "";
                        if (Utils.notEmptyString(eachLocation)) {
                            location.add(eachLocation);
                        }
                        if (Utils.notEmptyString(eachActivity)) {
                            activity.add(eachActivity);
                        }
                    }
                    Set userLocation = new HashSet(location);
                    Set userActvitiy = new HashSet(activity);
                    filterDropdown.put("location", userLocation);
                    filterDropdown.put("activity", userActvitiy);
                }
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
            try {
                //fetch userids, username, emails, and role values
                String businessUserSql = "select user_id, full_name, user_email, user_role, account_role_id from DT.users inner join DT.account on account.account_user_id = users.user_id where user_status = \"1\" and account_business_id = ?";
                List resultListUsers = jdbcTemplate.queryForList(businessUserSql, new Object[]{businessId});
                if (resultListUsers.size() > 0) {
                    List username = new ArrayList();
                    List userEmail = new ArrayList();
                    List roleId = new ArrayList();
                    List userRole = new ArrayList();
                    List userRoles = new ArrayList();
                    Iterator resultListUsersItr = resultListUsers.iterator();
                    while (resultListUsersItr.hasNext()) {
                        Map eachUserMap = (Map) resultListUsersItr.next();
                        String eachUsername = eachUserMap.containsKey("full_name") ? eachUserMap.get("full_name").toString() : "";
                        String eachUserEmail = eachUserMap.containsKey("user_email") ? eachUserMap.get("user_email").toString() : "";
                        int eachUserRole = eachUserMap.containsKey("user_role") ? (int) eachUserMap.get("user_role") : null;
                        int eachAccountRole = eachUserMap.containsKey("account_role_id") ? (int) eachUserMap.get("account_role_id") : null;

                        if (Utils.notEmptyString(eachUsername)) {
                            username.add(eachUsername);
                        }
                        if (Utils.notEmptyString(eachUserEmail)) {
                            userEmail.add(eachUserEmail);
                        }
                        if (Utils.isNotNull(eachUserRole)) {
                            userRole.add(eachUserRole);
                        }
                        if (Utils.isNotNull(eachAccountRole)) {
                            roleId.add(eachAccountRole);
                        }
                    }
                    //add role titles based on the roles available for the business
                    if (roleId.size() > 0 || userRole.size() > 0) {
                        if (userRole.contains(1)) {
                            userRoles.add("King");
                        }
                        if (roleId.contains(1)) {
                            userRoles.add("Admin");
                        }
                        if (roleId.contains(2)) {
                            userRoles.add("Read only");
                        }
                        if (roleId.contains(3)) {
                            userRoles.add("Read write");
                        }
                        if (userRole.contains(2)) {
                            userRoles.add("CS user");
                        }
                    }
                    filterDropdown.put("username", username);
                    filterDropdown.put("userEmail", userEmail);
                    filterDropdown.put("role", userRoles);
                }
            } catch (Exception e) {
                logger.error(e.getMessage());
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return filterDropdown;
    }
}
