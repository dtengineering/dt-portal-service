package com.dropthought.portal.service;

import com.dropthought.portal.businessconfigurations.service.BusinessConfigurationsService;
import com.dropthought.portal.util.Utils;
import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.colors.DeviceCmyk;
import com.itextpdf.kernel.colors.DeviceGray;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import java.io.*;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;



@Component
public class PdfService {

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    BusinessConfigurationsService businessConfigurationsService;

    public static  final  String DEFAULT_LOGO = "https://dt-program-banner.s3.us-west-1.amazonaws.com/a597f131-1cfe-4382-9780-2189425da87d-image.png";

    /**
     * Return the access settings mail parameters
     *
     * @param businessUUID
     * @return
     */
    public String generateSettingsAttachmentPDF(String businessUUID) {
        String encodedPDF = "";
        logger.info("Generate settings pdf service started");
        try {

            String pdfName = businessUUID+".pdf";
            Path settingsPath = Paths.get("src/main/resources/portalsettingspdf");
            File dir = new File(String.valueOf(settingsPath));
            if(!dir.exists())
                dir.mkdirs();

            File pdfFile = new File(settingsPath+"/"+pdfName);
            logger.info("pdfFile  "+pdfFile.toString());

            //This will create file if not exists
            //IF file exists in path it will modify the existing doc
            PdfDocument pdfDoc = new com.itextpdf.kernel.pdf.PdfDocument(new PdfWriter(settingsPath+"/"+pdfName));
            Document document = new Document(pdfDoc);
            this.addDTLogo(document);
            this.addContent(document, businessUUID);
            document.close();
            encodedPDF = this.getBase64FromPDF(businessUUID);

        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error("Exception at generateSettingsAttachmentPDF. Msg {}", e.getMessage());
        }
        return encodedPDF;
    }



    public void addContent(Document document, String businessUUID) {
        try {
            Paragraph settings = new Paragraph();
            // We add one empty line
            addEmptyLine(settings, 1);
            // Lets write a big header
            PdfFont f = PdfFontFactory.createFont(StandardFonts.HELVETICA);
            settings.add(new Paragraph("Settings"));
            settings.setFont(f);
            settings.setFontSize(18);
            settings.setFontColor(DeviceCmyk.BLACK);

            document.add(settings);


            //adding publish options table
            Map configurationsMap = businessConfigurationsService.getBusinessConfigurationForPDF(businessUUID);

            float[] columnWidths = {15, 15};
            Table table = new Table(UnitValue.createPercentArray(columnWidths));


            /*PdfPCell headerCell = new PdfPHeaderCell();
            headerCell.addElement(new Paragraph("Publish Options"));
            headerCell.setBackgroundColor(BaseColor.BLACK);
            headerCell.setTe*/
            Cell headerCell = new Cell(1, 2)
                    .add(new com.itextpdf.layout.element.Paragraph("Publish Options"))
                    .setFont(f)
                    .setFontSize(13)
                    .setFontColor(DeviceGray.WHITE)
                    .setBackgroundColor(new DeviceCmyk(0.80f,0,0,0))
                    .setTextAlignment(TextAlignment.CENTER);
            table.addHeaderCell(headerCell);

            //adding second row as channel, status
            Cell[] secondRow = new Cell[]{
                    new Cell().setBackgroundColor(new DeviceGray(0.60f)).add(new com.itextpdf.layout.element.Paragraph("Channel")).setTextAlignment(TextAlignment.CENTER),
                    new Cell().setBackgroundColor(new DeviceGray(0.60f)).add(new com.itextpdf.layout.element.Paragraph("Status").setTextAlignment(TextAlignment.CENTER))
            };
            table.addHeaderCell(secondRow[0]);
            table.addHeaderCell(secondRow[1]);

            Map publishOptions =  configurationsMap.containsKey("publishOptions") && Utils.isNotNull(configurationsMap.get("publishOptions")) ? (Map) configurationsMap.get("publishOptions") : new HashMap();
            Set keys = publishOptions.keySet();

            for(Object key : keys){

                table.addCell(new Cell().setBackgroundColor(new DeviceGray(0.9f)).setTextAlignment(TextAlignment.CENTER)
                            .add(new Paragraph(String.valueOf(key))));

                table.addCell(new Cell().setBackgroundColor(new DeviceGray(0.9f)).setTextAlignment(TextAlignment.CENTER)
                        .add(new Paragraph(publishOptions.get(key).toString())));
            }
            document.add(table);


            Paragraph newLine = new Paragraph();
            // We add one empty line
            addEmptyLine(newLine, 2);
            document.add(newLine);


            float[] columnWidths1 = {20, 20};
            Table table1 = new Table(UnitValue.createPercentArray(columnWidths1));

            Cell headerCell1 = new Cell(1, 2)
                    .add(new com.itextpdf.layout.element.Paragraph("Feature Access"))
                    .setFont(f)
                    .setFontSize(13)
                    .setFontColor(DeviceGray.WHITE)
                    .setBackgroundColor(new DeviceCmyk(0.80f,0,0,0))
                    .setTextAlignment(TextAlignment.CENTER);
            table1.addHeaderCell(headerCell1);

            //adding second row as channel, status
            Cell[] secondRow1 = new Cell[]{
                    new Cell().setBackgroundColor(new DeviceGray(0.60f)).add(new com.itextpdf.layout.element.Paragraph("Feature")).setTextAlignment(TextAlignment.CENTER),
                    new Cell().setBackgroundColor(new DeviceGray(0.60f)).add(new com.itextpdf.layout.element.Paragraph("Status").setTextAlignment(TextAlignment.CENTER))
            };
            table1.addHeaderCell(secondRow1[0]);
            table1.addHeaderCell(secondRow1[1]);

            Map featureAccess =  configurationsMap.containsKey("featureAccess") && Utils.isNotNull(configurationsMap.get("featureAccess")) ? (Map) configurationsMap.get("featureAccess") : new HashMap();
            Set faKeys = featureAccess.keySet();

            //Construct paragraph content for Action Plan
            Paragraph actionPlanParagraph = new Paragraph();
            if (featureAccess.containsKey("Action Plans") && (featureAccess.containsKey("System Defined Recommendations") &&
                    Integer.valueOf(featureAccess.get("System Defined Recommendations").toString()) > 0) ||
                    (featureAccess.containsKey("User Defined Recommendations") &&
                    Integer.valueOf(featureAccess.get("User Defined Recommendations").toString()) > 0)) {
                actionPlanParagraph.add("Action Plans \n");

                if (featureAccess.containsKey("System Defined Recommendations") &&
                        Integer.valueOf(featureAccess.get("System Defined Recommendations").toString()) == 1) {
                    actionPlanParagraph.add("-System defined recommendation: Score-based recommendations \n");
                } else if (featureAccess.containsKey("System Defined Recommendations") &&
                        Integer.valueOf(featureAccess.get("System Defined Recommendations").toString()) == 2) {
                    actionPlanParagraph.add("-System defined recommendation: Score + Impact based recommendations \n");
                }
                if (featureAccess.containsKey("User Defined Recommendations") &&
                        Integer.valueOf(featureAccess.get("User Defined Recommendations").toString()) > 0) {
                    actionPlanParagraph.add("-User defined recommendation: Score-based recommendations");
                }
            } else {
                actionPlanParagraph.add("Action Plans \n");
            }

            //DTV-9719 Construct paragraph content for Text Analytics
            Paragraph txtAnalyticsParagraph = new Paragraph();
            Map<String, Object> txtConfig = featureAccess.containsKey("Text Analytics Config") && ((Map) featureAccess.get("Text Analytics Config")).size() > 0
                    ? (Map) featureAccess.get("Text Analytics Config") : new HashMap();
            int surveyCount = txtConfig.containsKey("surveyIdCount") ? Integer.valueOf(txtConfig.get("surveyIdCount").toString()) : 0;
            int surveyIdIntentCount = txtConfig.containsKey("intentAnalysisSurveyCount")
                    ? Integer.valueOf(txtConfig.get("intentAnalysisSurveyCount").toString()) : 0;

            if (txtConfig.size() > 0) {
                txtAnalyticsParagraph.add("Text Analytics \n");
                if (txtConfig.get("textAnalyticsPlan").toString().contains("Basic")) {
                    txtAnalyticsParagraph.add("-Basic sentiment " +
                            (surveyCount == 0 ? "(All Programs) \n" :  "(" + surveyCount  + " Programs selected \n"));
                } else {
                    txtAnalyticsParagraph.add("-Advanced Text analytics \n");
                    if (txtConfig.containsKey("categoryAndSentimentAnalysis") &&
                            txtConfig.get("categoryAndSentimentAnalysis").toString().equals("1")) {
                        txtAnalyticsParagraph.add("-Category and Sentiment Analysis " +
                                (surveyCount == 0 ? "(All Programs) \n" :  "(" + surveyCount  + " Programs selected \n"));
                    }

                    if (txtConfig.containsKey("intentAnalysis") &&
                            txtConfig.get("intentAnalysis").toString().equals("1")) {
                        txtAnalyticsParagraph.add("-Intent Analysis " +
                                (surveyIdIntentCount == 0 ? "(All Programs) \n" :  "(" + surveyIdIntentCount  + " Programs selected) \n"));
                    }
                }
                if (txtConfig.containsKey("dataDuration") && txtConfig.get("dataDuration").toString().contains("Demo")) {
                    txtAnalyticsParagraph.add("-Duration: " + txtConfig.get("dataDuration").toString() + "\n");
                    txtAnalyticsParagraph.add(
                                "(" + txtConfig.get("startDate").toString() + " - " + txtConfig.get("endDate").toString() + ")");
                } else {
                    txtAnalyticsParagraph.add("-Duration: " + txtConfig.get("dataDuration").toString());
                }

            } else {
                txtAnalyticsParagraph.add("Text Analytics \n");
            }

            for(Object key : faKeys){

                if (String.valueOf(key).equals("System Defined Recommendations") ||
                        String.valueOf(key).equals("User Defined Recommendations") ||
                        String.valueOf(key).equals("Text Analytics Config")) {
                    continue;
                }
                if (String.valueOf(key).equals("Action Plans")) {
                    table1.addCell(new Cell().setBackgroundColor(new DeviceGray(0.9f)).setTextAlignment(TextAlignment.CENTER)
                            .add(actionPlanParagraph));
                    table1.addCell(new Cell().setBackgroundColor(new DeviceGray(0.9f)).setTextAlignment(TextAlignment.CENTER)
                            .add(new Paragraph(featureAccess.get(key).toString())));

                } else if (String.valueOf(key).equals("Text Analytics")) {
                    table1.addCell(new Cell().setBackgroundColor(new DeviceGray(0.9f)).setTextAlignment(TextAlignment.CENTER)
                            .add(txtAnalyticsParagraph));
                    table1.addCell(new Cell().setBackgroundColor(new DeviceGray(0.9f)).setTextAlignment(TextAlignment.CENTER)
                            .add(new Paragraph(featureAccess.get(key).toString())));

                } else {
                    table1.addCell(new Cell().setBackgroundColor(new DeviceGray(0.9f)).setTextAlignment(TextAlignment.CENTER)
                            .add(new Paragraph(String.valueOf(key))));
                    table1.addCell(new Cell().setBackgroundColor(new DeviceGray(0.9f)).setTextAlignment(TextAlignment.CENTER)
                            .add(new Paragraph(featureAccess.get(key).toString())));
                }
            }

            document.add(table1);

        }catch (Exception e){
            logger.error(e.getMessage());
            logger.error("Exception at addContent. Msg {}", e.getMessage());
        }

    }


    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }


    public String getBase64FromPDF(String businessUUID) {
        String encodedString = "";
        logger.info("begin reading the bytes from the file");
        //BASE64Encoder encoder = new BASE64Encoder();
        try{
            String pdfName = businessUUID+".pdf";
            Path settingsPath = Paths.get("src/main/resources/portalsettingspdf");
            File file = new File(settingsPath +"/"+pdfName);
            logger.info("file name  "+file.toString());

            FileInputStream fileInputStream = new FileInputStream(file);
            byte[] bytes = new byte[(int)file.length()];
            fileInputStream.read(bytes);
            byte[] encodedBytes = Base64Utils.encode(bytes);

            encodedString = new String(encodedBytes);
            encodedString = encodedString.replace("\n","");
        }catch (Exception e){
            logger.error(e.getMessage());
            logger.error("Exception at getBase64FromPDF. Msg {}", e.getMessage());
        }
        logger.info("end reading the bytes from the file");
        return encodedString;
    }

    /**
     * Method to get image bytes from url
     * @param document
     * @return
     */
    public void addDTLogo(Document document) {
        try{
            Paragraph para = new Paragraph();
            // We add one empty line
            addEmptyLine(para, 1);
            document.add(para);
            byte[] bytes = getBusinessImageFromURL(DEFAULT_LOGO);
            Image image = new Image(ImageDataFactory.create(bytes));
            //Top center position
            image.setFixedPosition((PageSize.A4.getWidth() - image.getImageScaledWidth())/2, (PageSize.A4.getHeight() - image.getImageScaledHeight()-10));
            document.add(image);
        }catch (Exception e){
            logger.error(e.getMessage());
            logger.error("Exception at addDTLogo. Msg {}", e.getMessage());
        }
    }

    /**
     * Method to get image bytes from url
     * @param imageUrl
     * @return
     */
    private byte[] getBusinessImageFromURL(String imageUrl) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream is = null;
        URL url = null;
        try {
            url = new URL(imageUrl);
            is = url.openStream();
            byte[] byteChunk = new byte[4096]; // Or whatever size you want to read in at a time.
            int n;
            while ((n = is.read(byteChunk)) > 0) {
                baos.write(byteChunk, 0, n);
            }
        } catch (Exception e) {
            logger.error("Failed while reading bytes from %s: %s", url.toExternalForm(), e.getMessage());
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    logger.error(e.getMessage());
                }
            }
        }
        return baos.toByteArray();
    }
}
