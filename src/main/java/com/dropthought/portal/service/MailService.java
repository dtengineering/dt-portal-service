package com.dropthought.portal.service;


import com.dropthought.portal.util.Constants;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by Prabhakar Thakur on 13/7/2020.
 */

@Component
public class MailService extends BaseService {

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /*@Value("${sendgrid}")
    private String sendgridKey;*/

    @Value("${dt.email.url}")
    private String emailUrl;

    @Value("${dtp.domain}")
    private String domain;

    @Autowired
    private NotificationService notificationService;

    /**
     * Send Html emails from SendGrid
     * Equivalent of htmlEmailSender
     *
     * @param recipientEmail
     * @param recipientName
     * @param subject
     * @param buttonVerbiage
     * @param link
     * @param messages
     * @param template
     */
    public int sendGridHtmlEmailSender(String recipientEmail, String recipientName, String subject, String buttonVerbiage, String link, Map messages, String template) {

        int result = 0;
        try {

            String type = null;
            logger.info(recipientEmail);
            recipientEmail = recipientEmail.trim();

            /*Logic without calling CallEmailRestApi*/
            //Email from = new Email("no-reply@dropthought.com", "No-reply");
            //Personalization personalization = new Personalization();
            //Email to = new Email(recipientEmail);
            //personalization.addTo(to);

            URL url = this.getClass().getClassLoader().getResource("email/" + template);
            String htmlContent = new String(Files.readAllBytes(Paths.get(url.toURI())));

            HashMap eachParameterMap = new HashMap();
            eachParameterMap.put("subject", subject);
            eachParameterMap.put("recipientName", recipientName);
            eachParameterMap.put("link", link);
            if (messages.containsKey("lineOne")) {
                eachParameterMap.put("lineOne", messages.get("lineOne"));
            }

            if (messages.containsKey("lineTwo")) {
                eachParameterMap.put("lineTwo", messages.get("lineTwo"));
            }

            if (messages.containsKey("userName")) {
                eachParameterMap.put("userName", messages.get("userName"));
            }

            if (messages.containsKey("password")) {
                eachParameterMap.put("password", messages.get("password"));
            }

            eachParameterMap.put("buttonVerbiage", buttonVerbiage);

            logger.info("The value in eachparametermap = " + eachParameterMap);
            StrSubstitutor sub = new StrSubstitutor(eachParameterMap);
            String resolvedString = sub.replace(htmlContent);
            //logger.info("The value in resolvedString = "+resolvedString);
            Map emailMap = new HashMap();
            emailMap.put("from", "no-reply@dropthought.com");
            emailMap.put("to", recipientEmail);
            emailMap.put("subject", subject);
            emailMap.put("body", resolvedString);
            emailMap.put("name", "No-reply");
            Map resultMap = callEmailRestApi(emailMap, HttpMethod.POST);
            logger.info("email sent status {} for {}", resultMap.get("STATUS").toString(), recipientEmail);
            if (resultMap.get("STATUS").toString().equalsIgnoreCase("SUCCESS")) {
                result = 1;
            }
            /*Logic without calling CallEmailRestApi*/
            /*Content content = new Content("text/html", resolvedString);
            Mail mail = new Mail(from, subject, to, content);
            mail.setFrom(from);
            mail.setSubject(subject);
            mail.addPersonalization(personalization);
            mail.addContent(content);

            logger.info("SENGRID _ KEY " + sendgridKey);
            SendGrid sendGrid = new SendGrid(sendgridKey);
            Request request = new Request();
            try {
                request.setMethod(Method.POST);
                request.setEndpoint("mail/send");
                request.setBody(mail.build());
                Response response = sendGrid.api(request);
            } catch (IOException e) {
                logger.error(e.getMessage());
            }*/

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return result;
    }

    private Map callEmailRestApi(Map inputMap, HttpMethod method) {

        Map respMap = new HashMap();
        JSONObject json = new JSONObject(inputMap);
        String inputJson = json.toString();
        logger.info(inputJson);

//        String uri ="send";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);

        try {
            String uri = emailUrl;
//            uri = Utils.notEmptyString(uri) ? baseEmailUrl + uri : baseEmailUrl;
            logger.info("uri{}", uri);
            restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
            ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, method, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info(responseEntity.getBody().toString());
                    respMap = (Map) responseEntity.getBody();

                }
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return respMap;
    }

    /**
     * method to send TA status email notification to CS user
     * @param status
     * @param userMap
     * @param businessName
     */
    public void sendTAStatusEmailToCSUser(String status, Map userMap, String businessName){
        logger.info("begin sendTAStatusEmailToCSUser");

        try{
            List ccList = new ArrayList();
            String ccEmail = "cs@dropthought.com";
            if(domain.contains("test")||domain.contains("stage")||domain.contains("automation")){
                ccEmail = "cs@yopmail.com";
            }
            ccList.add(ccEmail);

            String toEmail =  userMap.get("user_email").toString();
            Map emailMap = new HashMap();
            emailMap.put("from", Constants.FROM_CUSTOMER_SUCCESS_EMAIL);
            emailMap.put("name", Constants.FROM_CUSTOMER_SUCCESS_NAME);
            emailMap.put("to", toEmail);
            emailMap.put("cc", ccList);
            emailMap.put("subject", "Approval Request for "+businessName+" text analytics has been "+status);
            emailMap.put("body", String.format(Constants.TA_STATUS_NOFICATION_MSG, businessName, status));
            Map resultMap = callEmailRestApi(emailMap, HttpMethod.POST);
            logger.info("email sent status {} for {}", resultMap.get("STATUS").toString(), toEmail);
        }catch (Exception e) {
            logger.error("exception at sendTAStatusEmailToCSUser. Msg {}", e.getMessage());
        }
        logger.info("end sendTAStatusEmailToCSUser");
    }

    /**
     * Send Html emails from SendGrid
     * Equivalent of htmlEmailSender
     *
     * @param recipientEmail
     * @param recipientName
     * @param subject
     * @param messages
     * @param template
     */
    public int sendTextAnalyticsRequestEmail(String recipientEmail, String recipientName, String subject, Map messages, String template) {

        int result = 0;
        try {

            String type = null;
            logger.info(recipientEmail);
            recipientEmail = recipientEmail.trim();

            /*Logic without calling CallEmailRestApi*/
            //Email from = new Email("no-reply@dropthought.com", "No-reply");
            //Personalization personalization = new Personalization();
            //Email to = new Email(recipientEmail);
            //personalization.addTo(to);
            /*logger.info("template {}",template);
            URL url = this.getClass().getClassLoader().getResource("email/"+template);
            logger.info("url {}",url);
            String htmlContent = new String(Files.readAllBytes(Paths.get(url.toURI())));*/

            String htmlContent = notificationService.getFileContent(template);

            HashMap eachParameterMap = new HashMap();
            eachParameterMap.put("subject", subject);
            eachParameterMap.put("recipientName", recipientName);
            if (messages.containsKey("businessName")) {
                eachParameterMap.put("businessName", messages.get("businessName"));
            }
            if (messages.containsKey("approvalLink")) {
                eachParameterMap.put("approvalLink", messages.get("approvalLink"));
            }

            if (messages.containsKey("denyLink")) {
                eachParameterMap.put("denyLink", messages.get("denyLink"));
            }

            if (messages.containsKey("dataDuration")) {
                eachParameterMap.put("dataDuration", messages.get("dataDuration"));
            }

            if (messages.containsKey("plan")) {
                eachParameterMap.put("plan", messages.get("plan"));
            }

            if (messages.containsKey("surveyNames")) {
                List surveyNames = (List) messages.get("surveyNames");
                StringBuffer surveyNameStr = new StringBuffer();

                Iterator iterator = surveyNames.iterator();
                while (iterator.hasNext()) {
                    String eachSurveyName = (String) iterator.next();
                    surveyNameStr.append("<li style='list-style-type: decimal;margin:0 0 10px 0;padding:0 0 0 8px;'><p class='lead' style='color: #000000; font-family: \"Avenir LT W01_85 Heavy1475544\",-apple-system,system-ui,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 21px; margin: 0 0 5px 0; padding: 0; text-align: left;'>");
                    surveyNameStr.append(eachSurveyName);
                    surveyNameStr.append("</p></li>");
                }
                eachParameterMap.put("surveyNames", surveyNameStr.toString());
            }else {
                eachParameterMap.put("surveyNames", "");
            }

            eachParameterMap.put("EnvironmentInfo", messages.get("EnvironmentInfo"));

            logger.info("The value in eachparametermap = " + eachParameterMap);
            StrSubstitutor sub = new StrSubstitutor(eachParameterMap);
            String resolvedString = sub.replace(htmlContent);
            //logger.info("The value in resolvedString = "+resolvedString);
            Map emailMap = new HashMap();
            emailMap.put("from", "no-reply@dropthought.com");
            emailMap.put("to", recipientEmail);
            emailMap.put("subject", subject);
            emailMap.put("body", resolvedString);
            emailMap.put("name", "No-reply");
            Map resultMap = callEmailRestApi(emailMap, HttpMethod.POST);
            logger.info("email sent status {} for {}", resultMap.get("STATUS").toString(), recipientEmail);
            if (resultMap.get("STATUS").toString().equalsIgnoreCase("SUCCESS")) {
                result = 1;
            }
            /*Logic without calling CallEmailRestApi*/
            /*Content content = new Content("text/html", resolvedString);
            Mail mail = new Mail(from, subject, to, content);
            mail.setFrom(from);
            mail.setSubject(subject);
            mail.addPersonalization(personalization);
            mail.addContent(content);

            logger.info("SENGRID _ KEY " + sendgridKey);
            SendGrid sendGrid = new SendGrid(sendgridKey);
            Request request = new Request();
            try {
                request.setMethod(Method.POST);
                request.setEndpoint("mail/send");
                request.setBody(mail.build());
                Response response = sendGrid.api(request);
            } catch (IOException e) {
                logger.error(e.getMessage());
            }*/

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return result;
    }

}

