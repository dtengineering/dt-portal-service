package com.dropthought.portal.service;

import com.dropthought.portal.businessconfigurations.dto.BusinessConfigurationsBean;
import com.dropthought.portal.businessconfigurations.service.BusinessConfigurationsService;
import com.dropthought.portal.businessusage.BusinessUsageService;
import com.dropthought.portal.model.AuthIdPType;
import com.dropthought.portal.model.AuthRoutingRequest;
import com.dropthought.portal.model.AuthRoutingType;
import com.dropthought.portal.model.LTIBean;
import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.EmailTemplateUtility;
import com.dropthought.portal.util.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

@Service
public class IntegrationAppService extends BaseService {
    @Autowired
    BusinessConfigurationsService businessConfigurationsService;
    @Autowired
    DeviceService deviceService;
    @Autowired
    RestTemplateService restTemplateService;

    @Autowired
    ClientService clientService;
    @Autowired
    NotificationService notificationService;
    @Autowired
    BusinessUsageService businessUsageService;

    @Autowired
    PdfService pdfService;
    @Value("${dt.schedule.url}")
    private String dtScheduleUrl;
    @Value("${dtp.domain}")
    private String dtpDomain;
    @Value("${idm.okta.url}")
    private String idmOKTAUrl;
    @Value("${dtp.url}")
    private String dtAppUrl;
    @Value("${dt.edu.url}")
    private String dtEduUrl;

    @Qualifier("oktaClient")
    @Autowired
    private WebClient oktaWebClient;

    @Qualifier("ltiClient")
    @Autowired
    private WebClient ltiWebClient;

   /* @Async
    public void updateServicesAsynchronously(BusinessIntegratedBean businessIntegratedBean,
                                             Map oldBusinessConfigurationMap, String businessUniqueId,
                                             BusinessConfigurationsBean businessConfigurationsBean,
                                             int updateStatus) {
        logger.info("update configurations asynchronously started");
        //send business setting email
        if (updateStatus > 0) {
            sendReminderSettingEmail(businessConfigurationsBean.getBusinessId(), businessUniqueId);
        }
        //remove Templates from business
        if (businessIntegratedBean.isTemplateChanged()) {
            businessConfigurationsService.removeBusinessFromTemplates(businessConfigurationsBean.getBusinessId());
        }
        //update api key
        if (businessIntegratedBean.isAPIKeyChanged()) {
           updateAPIKey(oldBusinessConfigurationMap, businessConfigurationsBean);
        }
        //update kiosk count
        if (businessIntegratedBean.isKioskChanged()) {
           updateKioskCount(oldBusinessConfigurationMap, businessConfigurationsBean, businessUniqueId);
        }
        //update mfa changes to okta
        if (businessIntegratedBean.isMFAChanged()) {
           upateMFA(oldBusinessConfigurationMap, businessConfigurationsBean, businessUniqueId);
        }
        // to update the channels for active surveys in survey schedule for given business account
        Map channelValueMap = businessIntegratedBean.getChannels();
        if (channelValueMap.size() > 0 && businessConfigurationsBean.getBusinessId() != null) {
            businessConfigurationsBean.setChannels(channelValueMap);
            updateChannelsInScheduledSurveys(channelValueMap, businessConfigurationsBean);
        }

        //to update the usage_mail_flag when limits are changed
        Map usageLimitMap = businessIntegratedBean.getUsageLimit();
        if (usageLimitMap.size() > 0) {
            updateUsageLimit(businessConfigurationsBean.getBusinessId(), usageLimitMap);
        }

        //Call Text analytics settings API only when TA is modified
        if (businessIntegratedBean.isTAChanged()) {
            updateTASettings(oldBusinessConfigurationMap, businessConfigurationsBean, businessUniqueId);
        }
        if(businessIntegratedBean.isRecipientsChanged()){
            int oldLimit = (Utils.isNotNull(oldBusinessConfigurationMap.get("noOfRecipients")) &&
                    Utils.notEmptyString(oldBusinessConfigurationMap.get("noOfRecipients").toString())) ? Integer.parseInt(oldBusinessConfigurationMap.get("noOfRecipients").toString()) : -1;
            int newLimit = businessConfigurationsBean.getNoOfRecipients();
            updateReProcess(businessUniqueId, oldLimit, newLimit, "recipients");
        }
        if(businessIntegratedBean.isResponseChanged()){
            int oldLimit = (Utils.isNotNull(oldBusinessConfigurationMap.get("noOfResponses")) &&
                    Utils.notEmptyString(oldBusinessConfigurationMap.get("noOfResponses").toString())) ?
                    Integer.parseInt(oldBusinessConfigurationMap.get("noOfResponses").toString()) : -1;
            int newLimit = businessConfigurationsBean.getNoOfResponses();
            updateReProcess(businessUniqueId, oldLimit, newLimit, "responses");
        }
        if(businessIntegratedBean.isPublishToEliza()){
            *//**
             * commenting this part to save time. If necessary the api can be invoked manually
             *//*
            //clientService.updateClientConfigEliza(businessUUID);//TODO have to check and uncomment it
        }

        logger.info("abc");
    }*/



    @Async
    public void updateUsageLimit(int businessId, Map usageLimitMap){
        businessUsageService.updateUsageMailFlagWithNewLimitsByBusinessId(businessId,
                usageLimitMap);
    }

    @Async
    public void updateReProcess(String businessUniqueId, int oldLimit, int newLimit, String processStr){
        businessConfigurationsService.reprocessNewResponses(businessUniqueId, oldLimit, newLimit, processStr);
    }
    @Async
    public void upateMFA(Map oldBusinessConfigurationMap, BusinessConfigurationsBean businessConfigurationsBean,
                         String businessUniqueId) {
        try {
            int oldMfa = (Utils.isNotNull(oldBusinessConfigurationMap.get("mfa")) &&
                    Utils.notEmptyString(oldBusinessConfigurationMap.get("mfa").toString())) ?
                    Integer.parseInt(oldBusinessConfigurationMap.get("mfa").toString()) : 0;
            int newMfa = Utils.notEmptyString(businessConfigurationsBean.getMfa()) ?
                    Integer.parseInt(businessConfigurationsBean.getMfa()) : 0;
            logger.info("oldMfa {} newMfa {}", oldBusinessConfigurationMap.get("mfa"), businessConfigurationsBean.getMfa());
            if (oldMfa != newMfa) {
                String urlToCallMfa = idmOKTAUrl + "okta/users/" + businessUniqueId + "/factors";
                logger.info("update mfa url {}", urlToCallMfa);
                if (newMfa == 1 || newMfa==2)
                    restTemplateService.callInsightsApiForMfa(urlToCallMfa, HttpMethod.POST);
                else
                    restTemplateService.callInsightsApiForMfa(urlToCallMfa, HttpMethod.DELETE);
            }else{
                logger.info("MFA is not changed.");
            }
        } catch (Exception e) {
            logger.error("exception at upateMFA. Msg {}", e.getMessage());
        }
    }

    @Async
    public void updateKioskCount(Map oldBusinessConfigurationMap, BusinessConfigurationsBean businessConfigurationsBean,
                                 String businessUniqueId){
        int oldKioskCount = (Utils.isNotNull(oldBusinessConfigurationMap.get("noOfKiosks")) &&
                Utils.notEmptyString(oldBusinessConfigurationMap.get("noOfKiosks").toString())) ?
                Integer.parseInt(oldBusinessConfigurationMap.get("noOfKiosks").toString()) : -1;
        int newKioskCount = businessConfigurationsBean.getNoOfKiosks();
        updateKiosk(businessConfigurationsBean.getBusinessId(), oldKioskCount, newKioskCount,
                businessUniqueId);
    }
    @Async
    public void updateTASettings(Map oldBusinessConfigurationMap, BusinessConfigurationsBean businessConfigurationsBean,
                                 String businessUniqueId){
        int oldTAVal = (Utils.isNotNull(oldBusinessConfigurationMap.get("textAnalytics")) &&
                Utils.notEmptyString(oldBusinessConfigurationMap.get("textAnalytics").toString())) ?
                Integer.parseInt(oldBusinessConfigurationMap.get("textAnalytics").toString()) : 0;
        int newTAVal = Utils.notEmptyString(businessConfigurationsBean.getTextAnalytics()) ?
                Integer.parseInt(businessConfigurationsBean.getTextAnalytics()) : 0;
        if (oldTAVal != newTAVal) {
            businessConfigurationsService.updateElizaTextAnalyticsStatusByBusinessId(businessUniqueId,
                    businessConfigurationsBean.getBusinessId(), newTAVal);
        }
    }
    @Async
    public void updateAPIKey(Map oldBusinessConfigurationMap, BusinessConfigurationsBean  businessConfigurationsBean ){
        int oldApiKeyVal = (Utils.isNotNull(oldBusinessConfigurationMap.get("apiKey")) &&
                Utils.notEmptyString(oldBusinessConfigurationMap.get("apiKey").toString())) ?
                Integer.parseInt(oldBusinessConfigurationMap.get("apiKey").toString()) : 0;
        int newApiKeyVal = Utils.notEmptyString(businessConfigurationsBean.getApiKey()) ?
                Integer.parseInt(businessConfigurationsBean.getApiKey()) : 0;
        if (oldApiKeyVal != newApiKeyVal) {
            businessConfigurationsService.updateApiKeyByBusinessId(businessConfigurationsBean.getBusinessId(),
                    newApiKeyVal);
        }
    }

    @Async
    public void sendReminderSettingEmail(int businessId, String businessUniqueId, boolean isPlanUpdate,
                                         String planName, String contractEndDate) {
        try {
            //Get business configurations for the business
            Map mailParam = new HashMap();
            //send email
            Map kingUserMap = clientService.getKingUserByBusinessId(businessId);
            mailParam.put("toEmail", kingUserMap.containsKey("user_email") && kingUserMap.get("user_email") != null ?
                    kingUserMap.get("user_email").toString() : "");
            mailParam.put("recipientName", kingUserMap.containsKey("full_name") && kingUserMap.get("full_name") != null ?
                    kingUserMap.get("full_name").toString() : "");
            String encodedPDf = pdfService.generateSettingsAttachmentPDF(businessUniqueId);
            logger.info("encodedPDF  " + encodedPDf);
            mailParam.put("attachment", encodedPDf);
            mailParam.put("attachmentType", "pdf");
            mailParam.put("businessUUID", businessUniqueId);
            mailParam.put("EnvironmentInfo", EmailTemplateUtility.getEnvironmentEmailContent(dtpDomain, false));

            if (isPlanUpdate) {
                //get contract end date and plan name for the business
                mailParam.put("planName", planName);
                mailParam.put("contractEndDate", contractEndDate);

            }
            notificationService.sendBusinessSettingsReminder(mailParam, businessId, isPlanUpdate);
        } catch (Exception e) {
            logger.error("exception at sendReminderSettingEmail. Msg {} ", e.getMessage());
        }
    }

    /**
     * method to update the channels for active surveys in survey schedule for given business account
     * @param channelValueMap
     * @param businessConfigurationsBean
     */
    @Async
    public void updateChannelsInScheduledSurveys(Map channelValueMap,
                                                 BusinessConfigurationsBean businessConfigurationsBean){
        if (channelValueMap.size() > 0 && businessConfigurationsBean.getBusinessId() != null) {
            logger.info("begin update the channels in survey schedule");
            String CHANNEL = "channel";

            try {
                Map inputMap = new HashMap();
                final String uri =  dtScheduleUrl+""+businessConfigurationsBean.getBusinessId()+"/"+CHANNEL;
                //convert bean to Map
                inputMap = mapper.convertValue(businessConfigurationsBean, new TypeReference<Map>() {
                });
                Map resultMap = restTemplateService.callRestApi(inputMap, uri, HttpMethod.PUT);
                logger.info("End update the channels in survey schedule");

            } catch (Exception e) {
                logger.error("exception at updateChannelsInSchduledSurveys. Msg {}",e.getMessage());
            }
        }
    }

    /**
     * method to update kiosk registeration count
     * @param businessId
     * @param oldKioskCount
     * @param newKioskCount
     * @param businessUUID
     */
    public void updateKiosk(int businessId, int oldKioskCount, int newKioskCount, String businessUUID) {
        try {
            if (oldKioskCount != newKioskCount) {
                Integer registeredCount = deviceService.getKiosKRegistrationCount(businessId);
                /**if no. of kiosk < registered_devices
                 * call the block api
                 * if the no. of kiosk> registered devices and the (prev no.of kiosks < registered devices
                 * call the unblock api*/
                String hostName = dtpDomain;
                Map keyMap = deviceService.computeUriandApiKey(hostName);
                String urlToCall = keyMap.get("url").toString() + businessUUID + "/block";
                String kioskKey = keyMap.get("kioskKey").toString();
                if (newKioskCount < registeredCount && newKioskCount != -1) {
                    Map inputMap = new HashMap();
                    inputMap.put("blockType", "expire");//"blockType": "expire", // required, this represent the reason for the blocking, can use expire for now.
                    inputMap.put("timestamp", System.currentTimeMillis());//"timestamp": "1651348576" // expire data's timestamp
                    logger.info("long timestamp = " + System.currentTimeMillis());
                    deviceService.callRestApi(inputMap, urlToCall, HttpMethod.POST, kioskKey);
                } else if ((newKioskCount >= registeredCount && oldKioskCount <= registeredCount) || newKioskCount == -1) {
                    Map inputMap = new HashMap();
                    deviceService.callRestApi(inputMap, urlToCall, HttpMethod.DELETE, kioskKey);
                }
                //update the deactivate koisk
                deviceService.updateDeactivatedKiosks(businessUUID, oldKioskCount, newKioskCount);
                //insert the business details to track
                deviceService.insertBusinessTrackConfig(businessId, "noOfKiosk");
            }
        } catch (Exception e) {
            logger.error("exception at updateKiosk. Msg {}", e.getMessage());
        }
    }

    @Async
    public void createBusinessInDTWorks(String integrations, String closeLoop, String businessUUID) {
        if (Utils.notEmptyString(integrations) && integrations.equals("1") && Utils.notEmptyString(closeLoop) && closeLoop.equals("1")) {
            final String uri = dtAppUrl + "migrate/dtworks/business?businessId=" + businessUUID;
            logger.info("uri {}", uri);
            restTemplateService.callRestApi(null, uri, HttpMethod.POST);
        }
    }

    /**
     * method to create/update DTWorks role in DT
     * @param closeLoop
     * @param businessUUID
     */
    public void createDTWorksRoleInDT(String closeLoop, String businessUUID) {

        try {
            if (Utils.notEmptyString(closeLoop) && closeLoop.equals("1")) {
                //get DT api key for the business
                String apiKey = clientService.getApiKeyByClientId(businessUUID);
                if (Utils.notEmptyString(apiKey)) {
                    //call migrate api to insert/update DTWorks role in DT
                    String uri = dtAppUrl + "migrate/updaterolepermissions?businessUUID=" + businessUUID + "&roleName=dtWorks";
                    logger.info("uri {}", uri);
                    //set headers
                    HttpHeaders httpHeaders = new HttpHeaders();
                    httpHeaders.set("X-DT-API-KEY", apiKey);
                    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
                    //set request entity
                    HttpEntity<String> requestEntity = new HttpEntity<>(null, httpHeaders);
                    //call api
                    ResponseEntity<String> responseEntity = restTemplate.exchange(uri, HttpMethod.POST, requestEntity, String.class);
                    if (responseEntity != null && (responseEntity.getStatusCode() == HttpStatus.OK)) {
                        logger.info("DTWorks role added/updated successfully");
                    }
                }
            }
        } catch (Exception e) {
            logger.error("exception at createDTWorksRoleInDT. Msg {}", e.getMessage());
        }
    }

    /**
     * method to update sso for the business
     *
     * @param oldBusinessConfigurationMap
     * @param businessConfigurationsBean
     * @param businessUniqueId
     */
    @Async
    public void updateSSO(Map oldBusinessConfigurationMap, BusinessConfigurationsBean businessConfigurationsBean,
                          String businessUniqueId) {
        logger.info("updateSSO started");
        try {
            //get the sso value from the business configurations
            String ssoString = businessConfigurationsBean.getSso();
            //get the sso value for apple from the business configurations
            String appleSSO = businessConfigurationsBean.getAppleSSO();
            //get the sso value for google from the business configurations
            String googleSSO = businessConfigurationsBean.getGoogleSSO();
            //get the sso value for microsoft from the business configurations
            String microsoftSSO = businessConfigurationsBean.getMicrosoftSSO();

            //if sso is enabled, call the okta api to enable sso for the business
            //form payload
            //DTV-13160, DTV-12612
           // Map<String, Object> inputMap = new HashMap<>();
            AuthRoutingRequest inputMap = AuthRoutingRequest.builder().build();
            //set the idp name (by default not necessary for authType idP)
            if (ssoString.equals("1")) {
                //set the auth type
                /*inputMap.put("authType", "IdP");
                inputMap.put("idpName", "");*/
                inputMap.setAuthType(AuthRoutingType.IdP);
                inputMap.setIdpName("");
                //set the idp type
                if (appleSSO.equals("1")) {
                    inputMap.setIdpType(AuthIdPType.APPLE);
                } else if (googleSSO.equals("1")) {
                    inputMap.setIdpType(AuthIdPType.GOOGLE);
                } else if (microsoftSSO.equals("1")) {
                    inputMap.setIdpType(AuthIdPType.MICROSOFT);
                }
            } else {
                inputMap.setAuthType(AuthRoutingType.PASSWORD);
            }
                //call the okta api to enable sso for the business
                if (Utils.isNotNull(inputMap.getAuthType())) {
                    //String urlToCallSSO = idmOKTAUrl + "okta/users/" + businessUniqueId + "/auth";
                    String urlToCallSSO = "/okta/users/" + businessUniqueId + "/auth";
                    logger.info("urlToCallSSO {}", urlToCallSSO);
                    //restTemplateService.callSSORestApi(inputMap, urlToCallSSO, HttpMethod.POST);
                    callOKTAWebClientPOST(inputMap, urlToCallSSO)
                    .subscribe(
                            null, // No need to handle the body since it's Void
                            error -> logger.error("Error occurred: " + error.getMessage()),
                            () -> logger.info("IDM Okta Service SSO Request completed successfully")
                    );
                    //restTemplateService.callInsightsApiForMfa(urlToCallSSO, HttpMethod.POST);
                }
            logger.info("updateSSO ended");
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("exception at upateMFA. Msg {}", e.getMessage());
        }
    }

    /**
     * method to update lti for the business
     *
     * @param businessConfigurationsBean
     */
    public void updateLti(String businessUUID, BusinessConfigurationsBean businessConfigurationsBean) {
        try {
            //get the lti value from the business configurations
            Map ltiConfigMap = businessConfigurationsBean.getLtiConfig();
            String lti = businessConfigurationsBean.getLti();
            //if lti is enabled, call the dt-edu api to enable lti for the business
            if (lti.equals("1")) {
                //get DT api key for the business
                String apiKey = clientService.getApiKeyByClientId(businessUUID);
                //form payload
                /*ltiConfigMap.put("accessKey", apiKey);
                ltiConfigMap.put("businessUUID", businessUUID);*/
                //DTV-13160, DTV-12612
                LTIBean ltiBean = LTIBean.builder()
                                .accessKey(apiKey)
                                .businessUUID(businessUUID)
                                .developerId(ltiConfigMap.containsKey("developerId") ? ltiConfigMap.get("developerId").toString() : "")
                                .developerKey(ltiConfigMap.containsKey("developerKey") ? ltiConfigMap.get("developerKey").toString() : "")
                                .domain(ltiConfigMap.containsKey("domain") ? ltiConfigMap.get("domain").toString() : "")
                                .issuer(ltiConfigMap.containsKey("issuer") ? ltiConfigMap.get("issuer").toString() : "")
                                .apiKey(ltiConfigMap.containsKey("apiKey") ? ltiConfigMap.get("apiKey").toString() : "")
                                .build();
                //call the api to enable lti for the business
                /*String urlToCallLTI = dtEduUrl;
                restTemplateService.callRestApiDtEdu(ltiBean, urlToCallLTI, HttpMethod.POST);*/
                String response = callLTIWebClientPOST(ltiBean, "");
                logger.info("response from LTI {}", response);

            }
        } catch (Exception e) {
            logger.error("exception at updateLti. Msg {}", e.getMessage());
        }
    }

    /**
     * Use WebClient to make a reactive HTTP request.
     * Handle the Mono<Void> response appropriately, either subscribing to it or handling it in a reactive manner.
     * @param requestBody
     * @param url
     * @return
     */
    public Mono<Void> callOKTAWebClientPOST(AuthRoutingRequest requestBody, String url) {
        return oktaWebClient.post()
                .uri(url) // specify the endpoint
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE) // set content type
                .bodyValue(requestBody) // set the request body
                .retrieve() // initiate the request
                .onStatus(
                        status -> status.value() != 200, // check for non-200 status codes
                        clientResponse -> Mono.error(new RuntimeException("Failed with HTTP code: " + clientResponse.statusCode()))
                )
                .bodyToMono(Void.class) // expect a Void response
                .doOnSuccess(aVoid -> logger.info("Request successful with status code 200"))
                .doOnError(error -> logger.error("Error occurred: " + error.getMessage()));
    }

    /**
     * Use WebClient to make a reactive HTTP request.
     * Handle the Mono<Void> response appropriately, either subscribing to it or handling it in a reactive manner.
     * @param requestBody
     * @param url
     * @return
     */
    public String callLTIWebClientPOST(LTIBean requestBody, String url) {

        try {
            return ltiWebClient.post()
                    .uri(url) // specify the endpoint
                    .headers(headers -> {
                        headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
                        headers.set("X-DT-EDU-ACCESS", "dropthought.com");
                    }) // set content type
                    .bodyValue(requestBody) // set the request body
                    .retrieve() // initiate the request
                    .bodyToMono(String.class) // expect a String response
                    .block();
        } catch(WebClientResponseException e) {
            logger.error("LTI Edu webclient request error occurred with status code: {}", e.getRawStatusCode());
            return e.getResponseBodyAsString();
        }
    }
}
