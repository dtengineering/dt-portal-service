package com.dropthought.portal.service;


import com.dropthought.portal.businessconfigurations.service.BusinessConfigurationsService;
import com.dropthought.portal.model.BusinessSchedule;
import com.dropthought.portal.model.CustomizeQRCodeBean;
import com.dropthought.portal.model.QrCodeSurveyTokenBean;
import com.dropthought.portal.service.shorturl.ShortUrlBuilderFactory;
import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.CryptoUtility;
import com.dropthought.portal.util.TokenUtility;
import com.dropthought.portal.util.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.BarcodeQRCode;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ParameterizedPreparedStatementSetter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


@Component
public class SchedulerService extends  BaseService {

    /**
     * Logger Factory Initialized
     */
//    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${schedule.routing.key}")
    private String routingKey;

    @Value("${dt.schedule.url}")
    private String dtScheduleBaseUrl;

    @Value("${dtp.domain}")
    private String dtpDomain;

    @Value("${dtp.eux.domain}")
    private String euxDomain;

    @Value("${dt.dt360.url}")
    private String dt360Url;

    @Value("${dt.dt360.url.r}")
    private String dt360UrlRead;

    @Value("${dt.event.url}")
    private String dtEventUrl;

    @Value("${short.url.type}")
    private String shortUrlType;

    /*@Value("${bitly.domain}")
    private String domain;

    @Value("${bitly.apikey}")
    private String bitlykey;

    @Value("${bitly.url}")
    private String bitlyAPI;*/

    @Value("${onprem}")
    private boolean onprem;
    @Value("${dtp.url}")
    private String dtAppUrl;

    /**
     * JdbcTemplate for DB Connectivity.
     */
    @Autowired
    @Qualifier("jdbcDt")
    JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("jdbcNamedDt")
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    NotificationService notificationService;

    @Autowired
    BusinessConfigurationsService businessConfigurationsService;

    @Autowired
    TokenUtility tokenUtility;

    @Autowired
    CryptoUtility cryptoUtility;

    @Autowired
    PdfService pdfService;

    @Autowired
    IdmService idmService;

    @Autowired
    ShortUrlBuilderFactory factory;

    @Autowired
    QRCustomizationService qrCustomizationService;

    @Autowired
    RestTemplateService restTemplateService;

    @Autowired
    PaymentService paymentService;

    /**
     * Method to insert a new record into business_schedule
     * @param businessSchedule
     */
    public int createBusinessSchedule(BusinessSchedule businessSchedule){
        logger.info("create business schedule starts");
        int result = 0;
        String INSERT_SCHEDULED_BUSINESSS = "INSERT INTO business_schedule (`business_id`,`state`,`start_date`,`end_date`,`precursor_date`, timezone) values(?,?,?,?,?,?)";
        try{
            String startDate = businessSchedule.getStartDate();
            String endDate =  businessSchedule.getEndDate();
            String timezone = Utils.notEmptyString(businessSchedule.getTimezone()) ? businessSchedule.getTimezone() : Constants.TIMEZONE_AMERICA_LOSANGELES;
            String eightyPercentDate = computeEightyPercentDate(startDate, endDate);
            result = jdbcTemplate.update(INSERT_SCHEDULED_BUSINESSS, new Object[]{businessSchedule.getBusinessId(), businessSchedule.getState(), businessSchedule.getStartDate(), businessSchedule.getEndDate(), eightyPercentDate, timezone});
        }catch (Exception e){
            logger.error(e.getMessage());
            result = -1;
        }
        logger.info("create business schedule ends");
        return result;
    }

    /**
     * Method to update record into business_schedule
     * @param businessSchedule
     */
    public int updateBusinessSchedule(BusinessSchedule businessSchedule){
        logger.info("update business schedule starts");
        int result = 0;
        String UPDATE_SCHEDULED_BUSINESSS = "update business_schedule set start_date=?, end_date=?, precursor_date=?, modified_time=? where business_id=?";
        try{
            String startDate = businessSchedule.getStartDate();
            String endDate =  businessSchedule.getEndDate();
            String eightyPercentDate = computeEightyPercentDate(startDate, endDate);
            result = jdbcTemplate.update(UPDATE_SCHEDULED_BUSINESSS, new Object[]{businessSchedule.getStartDate(), businessSchedule.getEndDate(), eightyPercentDate,new Timestamp(System.currentTimeMillis()), businessSchedule.getBusinessId()});
        }catch (Exception e){
            logger.error(e.getMessage());
            result = -1;
        }
        logger.info("update business schedule ends");
        return result;
    }

    /**
     * Method to check if business is exists by businessid from business_schedule
     * @param businessId
     */
    public boolean checkIfBusinessExistsInSchedule(int businessId){
        String GET_SCHEDULED_BUSINESSS = "select  id from  business_schedule where business_id = ?";
        try{
            List result = jdbcTemplate.queryForList(GET_SCHEDULED_BUSINESSS, new Object[]{businessId});
            if(Utils.isNotNull(result) && result.size() >0){
                return true;
            }
        }catch (Exception e){
            logger.error(e.getMessage());
            return false;
        }
        return false;
    }

    /**
     * method to compute 80% of contract date to send out email for only remaining 20% of contract date
     * @param startDateStr
     * @param endDateStr
     * @return
     */
    public String computeEightyPercentDate(String startDateStr, String endDateStr){
        String dateStr = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date startDate = sdf.parse(startDateStr);
            Date endDate = sdf.parse(endDateStr);
            long diff = endDate.getTime() - startDate.getTime();
            long totalDaysDiff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            long eightyPerDays =  (totalDaysDiff * 80 ) / 100 ;
            logger.info("total days "+totalDaysDiff);
            logger.info("80 per days "+eightyPerDays);

            Calendar ex = Calendar.getInstance();
            ex.setTime(startDate); // set calendar with contract startDate
            ex.add(Calendar.DATE, (int)eightyPerDays); // add eighty percent days diff to startdate
            Date dateVal = ex.getTime();
            logger.info("start date "+ startDate);
            logger.info("end date "+ endDate);

            dateStr = sdf.format(dateVal);
            logger.info("eighty percent date "+ dateStr);

        } catch (ParseException e) {
            logger.error(e.getMessage());
        }
        return dateStr;
    }

    /**
     * Method to get scheduled business by status
     * state = 0  inactive, state = 1 active
     * @return
     */
    public List getScheduledBusinessByStatus(String state) {
        String orderBy = state.equals(Constants.CONTRACT_STATE_INACTIVE) ? "start_date" : "end_date";
        //Return business details if business configurations also available for the business.
        String GET_ACTIVE_SCHEDULED_BUSINESSS = "select bs.`id`, bs.`business_id`,bs.`state`,bs.`start_date`, " +
                "bs.end_date,bs.`precursor_date`, bs.`timezone` from business_schedule bs " +
                "right join DT.business_configurations bc on bc.business_id=bs.business_id where state = ? order by ?";
        List<Map<String, Object>> dbMaps = new ArrayList<Map<String, Object>>();
        try{
           dbMaps = jdbcTemplate.queryForList(GET_ACTIVE_SCHEDULED_BUSINESSS, new Object[]{state, orderBy});
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return dbMaps;
    }

    /**
     * method to update state of scheduled business
     * @param scheduled_id
     * 0-inactive, 1-active, 2 - partially(80percent) Expired, 3 expired
     */
    public int updateScheduledBusinessState(int scheduled_id, String state){
        int result = 0;
        String UPDATE_SCEDULED_BUSINESS_STATE = "update  business_schedule set state = ?, modified_time = ?  where  id = ?";
        try{
            result = jdbcTemplate.update(UPDATE_SCEDULED_BUSINESS_STATE, new Object[]{state, new Timestamp(System.currentTimeMillis()), scheduled_id});
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return  result;
    }

    /**
     * method to get kings profile like name and email by using business id
     * @param businessId
     * @return
     */
    public Map getKingsProfileForBusiness(int businessId){
        Map profileMap = new HashMap();
        String GET_KING_PROFILE = "select u.full_name, u.first_name, u.last_name, u.username, u.user_email, u.password, u.user_status, a.account_business_id, a.account_role_id\n" +
                " from users u left join account a on u. user_id = a.account_user_id where a.account_business_id = ? and u.user_role='1'";
        try{
            profileMap = jdbcTemplate.queryForMap(GET_KING_PROFILE, new Object[]{businessId});
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return profileMap;
    }

    /**
     * Send mail to business for business remainder or contract termination
     *
     * @param rabbitQueueParam
     */
    public void sendBusinessEmail(Map rabbitQueueParam) {
        logger.info("begin send business email");
        try {
            String templateType = rabbitQueueParam.get("templateType").toString();
            Integer eachBusinessId = (Integer) rabbitQueueParam.get("eachBusinessId");

            if (Utils.notEmptyString(templateType) && Utils.isNotNull(eachBusinessId)) {
                String contractExpiryDate = rabbitQueueParam.containsKey("contractExpiryDate") ?
                        rabbitQueueParam.get("contractExpiryDate").toString() : "";
                String timezone = rabbitQueueParam.containsKey("timezone") ? rabbitQueueParam.get("timezone").toString() : "";
                String businessUUID = rabbitQueueParam.containsKey("businessUUID") ? rabbitQueueParam.get("businessUUID").toString() : "";
                int planId = rabbitQueueParam.containsKey("planId") ? (int) rabbitQueueParam.get("planId") : 0;
                String dtToken = rabbitQueueParam.containsKey("dtToken") ? rabbitQueueParam.get("dtToken").toString() : "";

                Map profileMap = getKingsProfileForBusiness(eachBusinessId);
                String localContractDate = Utils.notEmptyString(contractExpiryDate) && Utils.notEmptyString(timezone) ?
                        Utils.getDateAsStringByTimezone(contractExpiryDate, timezone) : "";
                String endDate = rabbitQueueParam.containsKey("endDate") ? rabbitQueueParam.get("endDate").toString() : "";
                Map mailParam = new HashMap();
                mailParam.put("contractExpiryDate", localContractDate);
                mailParam.put("toEmail", profileMap.containsKey("user_email")? profileMap.get("user_email").toString() : "");
                mailParam.put("recipientName", profileMap.containsKey("full_name")? profileMap.get("full_name").toString() : "");
                mailParam.put("endDate", endDate);
                mailParam.put("planName", rabbitQueueParam.containsKey("planName")? rabbitQueueParam.get("planName").toString() : "");
                mailParam.put("userUUID", rabbitQueueParam.containsKey("userUUID")? rabbitQueueParam.get("userUUID").toString() : "");
                mailParam.put("businessUUID", businessUUID);
                if (planId > 0) {

                    mailParam.put("planId", planId);
                    mailParam.put("dtToken", dtToken);
                    mailParam.put("trialStatus", rabbitQueueParam.containsKey("trialStatus")
                            ? rabbitQueueParam.get("trialStatus").toString() : "0");
                    mailParam.put("trialReminderEndDate", rabbitQueueParam.containsKey("trialReminderEndDate")
                            ? rabbitQueueParam.get("trialReminderEndDate").toString() : endDate);
                    mailParam.put("subscriptionReminderEndDate", rabbitQueueParam.containsKey("subscriptionReminderEndDate")
                            ? rabbitQueueParam.get("subscriptionReminderEndDate").toString() : endDate);
                    mailParam.put("card", paymentService.getCardLastFourDigits(eachBusinessId));
                    boolean isTransactionsMadeAlready = paymentService.isTransactionsMadeForBusiness(eachBusinessId, businessUUID);
                    mailParam.put("isTransactionsMadeAlready", isTransactionsMadeAlready);
                    mailParam.put("link", rabbitQueueParam.containsKey("link") ? rabbitQueueParam.get("link").toString() : "");
                }
                notificationService.sendBusinessContractEmail(mailParam, templateType, eachBusinessId);
                logger.info("Email has sent to business id {}",eachBusinessId);

                if (templateType.equals(Constants.DEACTIVATE) || templateType.equals(Constants.PLAN_EXPIRED)) {
                    //deactivate the business for contract termination
                    this.updateBusinessState(eachBusinessId, Constants.CONTRACT_EXPIRED);
                    //deactivate the users from the business
                    //commenting the below line to stop users from being deactivated when contract expired.
                    //this.deactivateUsersByBusiness(eachBusinessId);
                }
            }
        }
        catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("end send business email");
    }

    /**
     * method to deactivate users in okta for a given expired business
     * @param businessId
     */
    public String deactivateUsersByBusiness(Integer businessId) {
        int deactivatedUserCnt = 0;
        int totalUsers =0;
        String getExpiredBusinessUsers = "SELECT user_id, user_uuid, user_email, business_uuid FROM account a, business b, users c WHERE " +
                "  a.account_business_id = ? AND a.account_business_id = b.business_id and b.state= '3' AND a.account_user_id = c.user_id";
        try {
            List expiredBusinessUsersList = jdbcTemplate.queryForList(getExpiredBusinessUsers, new Object[]{businessId});
            totalUsers = expiredBusinessUsersList!= null ?  expiredBusinessUsersList.size() : 0;
            Iterator itr = expiredBusinessUsersList.iterator();
            while (itr.hasNext()) {
                Map userMap = (Map) itr.next();
                String userUniqueId = userMap.get("user_uuid").toString();
                String businessUniqueId = userMap.get("business_uuid").toString();
                idmService.deactivateUser(userUniqueId, businessUniqueId);
                deactivatedUserCnt++;
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return "Total users: " +totalUsers + ". Deactivated users: "+deactivatedUserCnt;
    }

    /**
     * method to update state of scheduled business
     * @param businessId
     * 0-inactive, 1-active, 2 - partially(80percent) Expired, 3 expired
     */
    public int updateBusinessState(int businessId, String state){
        int result = 0;
        String UPDATE_BUSINESS_STATE = "update business set state = ?, modified_time = ?  where  business_id = ?";
        try{
            result = jdbcTemplate.update(UPDATE_BUSINESS_STATE, new Object[]{state, new Timestamp(System.currentTimeMillis()), businessId});
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return  result;
    }

    /**
     * delete's business details if it has been expired for more than 6 months
     * DTV-2756
     */
    public void deleteBusiness(){
        List expiredBusiness = new ArrayList();
        String getExpiredBusiness = "SELECT business_id, business_uuid, contract_end_date FROM business WHERE state = ?";
        try {
            expiredBusiness = jdbcTemplate.queryForList(getExpiredBusiness, new Object[]{Constants.CONTRACT_EXPIRED});
        }catch (Exception e){
           logger.error(e.getMessage());
        }
        if(expiredBusiness.size()>0){
            Iterator expiredBusinessIterator = expiredBusiness.iterator();
            while (expiredBusinessIterator.hasNext()){
                try {
                    Map eachExpiredBuinsess = (Map) expiredBusinessIterator.next();
                    int businessId = eachExpiredBuinsess.containsKey("business_id") ? (int) eachExpiredBuinsess.get("business_id") : 0;
                    String businessUUID = eachExpiredBuinsess.containsKey("business_uuid") ? eachExpiredBuinsess.get("business_uuid").toString() : "";
                    String contractEndDate = eachExpiredBuinsess.containsKey("contract_end_date") ? eachExpiredBuinsess.get("contract_end_date").toString() : "";
                    String currentDate = Utils.getCurrentUTCDateAsString();
                    double differenceInDays = Utils.differenceDays(contractEndDate, currentDate);
                    if (differenceInDays >= Constants.EXPIRY_MONTHS) {
                        logger.info("Deleting the business {} as it's expired for 6 months", businessUUID);
                        String businessHyphenReplaced = businessUUID.replace("-", "_");
                        //to delete events related the business
                        this.deleteEvents(businessHyphenReplaced);
                        //to backup business related tables before deleting.
                        this.backupBusinessTables(businessHyphenReplaced);
                        //to delete business related tables
                        this.deleteBusinessRelatedTables(businessHyphenReplaced);
                        // to delete users related tp the business
                        this.deleteUsers(businessUUID);
                        // to update state of business to inactive in business table
                        this.updateBusinessState(businessId, Constants.STATE_0_DRAFT);
                    }
                    logger.info("End deleting business {} and its details", businessUUID);
                }catch (Exception e){
                    logger.error(e.getMessage());
                }
            }
        }
    }

    /**
     * method to delete business related tables
     * @param businessUUID
     */
    public void deleteBusinessRelatedTables(String businessUUID){
        logger.info("Begin deleting business related tables");
        /** business related tables**/

        String surveysTable = String.format("surveys_%s", businessUUID.replaceAll("-", "_"));
        String userSurveyMapTable = String.format("user_survey_map_%s", businessUUID.replaceAll("-", "_"));
        String questionsTable = String.format("questions_%s", businessUUID.replaceAll("-", "_"));
        String keyMetricTable = String.format("key_metric_%s", businessUUID.replaceAll("-", "_"));
        String multiChoiceOptionsTable = String.format("multiple_choice_options_%s", businessUUID.replaceAll("-", "_"));
        String surveyFiltersTable = String.format("survey_filters_%s", businessUUID.replaceAll("-", "_"));
        String userSurveyFiltersTable = String.format("user_survey_filters_%s", businessUUID.replaceAll("-", "_"));
        String userAlertsTable = String.format("user_alerts_%s", businessUUID.replaceAll("-", "_"));
        String notificationTable = String.format("notifications_%s", businessUUID.replaceAll("-", "_"));
        String tokenTable = String.format("token_%s", businessUUID.replaceAll("-", "_"));
        String staticTokenTable = String.format("static_token_%s", businessUUID.replaceAll("-", "_"));
        String qrCodeTable = String.format("qrcode_%s", businessUUID.replaceAll("-", "_"));
        String surveyTempalteTable = String.format("templates_%s", businessUUID.replaceAll("-", "_"));

        String businessTables = surveysTable +","+ userSurveyMapTable +","+ questionsTable +","+ keyMetricTable +","+ multiChoiceOptionsTable +
                ","+ surveyFiltersTable +","+ userSurveyFiltersTable +","+ userAlertsTable +","+ notificationTable +","+ tokenTable +","+ staticTokenTable +
                ","+ qrCodeTable +","+ surveyTempalteTable;

        String deleteSurveyTable = "DROP TABLE IF EXISTS " + businessTables;
        try {
            int deleteSurvey = jdbcTemplate.update(deleteSurveyTable);
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        logger.info("End deleting business related tables");
    }

    /**
     * method to delete events related to a business
     * @param businessUUID
     */
    public void deleteEvents(String businessUUID){
        logger.info("Begin deleting events");
        List surveysList = new ArrayList();
        String surveysTable = String.format("surveys_%s", businessUUID.replaceAll("-", "_"));
        String surveysListSql = "SELECT survey_uuid FROM " + surveysTable;
        try {
            surveysList = jdbcTemplate.queryForList(surveysListSql);
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        if(surveysList.size()>0){
            Iterator surveyListIterator = surveysList.iterator();
            while (surveyListIterator.hasNext()){
                try {
                    Map surveyMap = (Map) surveyListIterator.next();
                    String eachSurveyUUID = surveyMap.containsKey("survey_uuid") ? surveyMap.get("survey_uuid").toString() : "";
                    //String eventBackupTableName = "event_bcup"+eachSurveyUUID.replace("-","_");
                    String eventBackupTableName = String.format("event_bcup%s", eachSurveyUUID.replace("-", "_"));
                    //String showSql = "show tables like `event_"+eachSurveyUUID.replace("-","_")+"`"
                    String showSql = "SELECT * FROM information_schema.tables WHERE table_schema = 'DT' AND table_name = `event_"+eachSurveyUUID.replace("-","_")+"`";
                    List eventList = jdbcTemplate.queryForList(showSql);
                    String eventTableName = String.format("event_%s", eachSurveyUUID.replace("-", "_"));
                    if(eventList.size()>0)
                    {
                        String eventBackupSql = "insert into "+eventBackupTableName+" select * from " + eventTableName;
                        int backupResult = jdbcTemplate.update(eventBackupSql);
                    }
                    String deleteEventSql = "DROP TABLE IF EXISTS " + eventTableName;
                    try {
                        int deleteEvent = jdbcTemplate.update(deleteEventSql);
                    } catch (Exception e) {
                        logger.error(e.getMessage());
                    }
                }catch (Exception e){
                    logger.error(e.getMessage());
                }
            }
        }
        logger.info("End deleting events");
    }

    /**
     * method to delete a business's users
     * @param businessUUID
     */
    public void deleteUsers(String businessUUID){
        logger.debug("Begin deleting users");
        List usersList = new ArrayList();
        int businessId = this.getBusinessId(businessUUID);
        String usersListSql = "SELECT u.user_id, u.full_name, u.username,  u.user_email, u.user_uuid, a.account_user_id FROM users AS u LEFT JOIN " +
                "account AS a ON u.user_id = a.account_user_id WHERE account_business_id = ?";
        try {
            usersList = jdbcTemplate.queryForList(usersListSql, new Object[]{businessId});
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        if(usersList.size()>0){
            Iterator usersIterator = usersList.iterator();
            while (usersIterator.hasNext()){
                Map eachUserMap = (Map) usersIterator.next();
                int userId = eachUserMap.containsKey("user_id") ? (int) eachUserMap.get("user_id") : 0;
                //String userUUID = eachUserMap.containsKey("user_uuid") ? eachUserMap.get("user_uuid").toString() : "";
                //delete user from user_uuid_mapping table
                String deleteUUIDMapping = "DELETE FROM user_uuid_mapping WHERE uuid_user_id = ?";
                //delete user from acocunt table
                String deleleInAccount = "DELETE FROM account WHERE account_user_id = ?";
                //delete from user_survey
                String deleteUserSurvey = "DELETE FROM user_survey WHERE user_id = ?";
                //delete user from users table
                String deleteInUsers = "DELETE FROM users WHERE user_id = ?";
                try {
                    int updateMapping = jdbcTemplate.update(deleteUUIDMapping, new Object[]{userId});
                    int updateAccount = jdbcTemplate.update(deleleInAccount, new Object[]{userId});
                    int updateUserSurvey = jdbcTemplate.update(deleteUserSurvey, new Object[]{userId});
                    int updateUsers = jdbcTemplate.update(deleteInUsers, new Object[]{userId});
                }catch (Exception e){
                    logger.error(e.getMessage());
                }
            }
        }
        logger.debug("End deleting users");
    }

    /**
     * get business id from uuid
     * @param businessUUID
     * @return
     */
    public int getBusinessId(String businessUUID){
        int businessId =0;
        String getBusinessSql = "SELECT business_id FROM business WHERE business_uuid = ?";
        try {
            businessId = jdbcTemplate.queryForObject(getBusinessSql, new Object[]{businessUUID}, Integer.class);
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return  businessId;
    }

    /**
     * update confirmation status in users table by user id
     *
     * @param userId
     * @param state
     * @return
     */
    public int updateUserConfirmStatus(int userId, String state){
        int result = 0;
        String UPDATE_USER_CONFIG_STATUS = "UPDATE users SET confirmation_status = ?, modified_time = ?  where  user_id = ?";
        try{
            result = jdbcTemplate.update(UPDATE_USER_CONFIG_STATUS, new Object[]{state, new Timestamp(System.currentTimeMillis()), userId});
        }catch (Exception e){
            logger.error(e.getMessage());
            logger.error(e.getMessage());
        }
        return  result;
    }

    /**
     * Get businessuuid based on businessid
     *
     * @param businessId
     * @return
     */
    public String retrieveBusinessUUIDByID(int businessId) {
        String businesUUID = "";
        try {
            String SELECT_BUSINESSUUID_BY_ID = "select business_uuid from business where business_id=?";
            logger.debug("begin retrieving bussinessUUID by business id");
            Map businessMap = jdbcTemplate.queryForMap(SELECT_BUSINESSUUID_BY_ID , new Object[]{businessId});
            if(businessMap.size() > 0){
                businesUUID = businessMap.containsKey("business_uuid") ? (String)businessMap.get("business_uuid") : "";
            }
            logger.debug("end retrieving bussinessUUID by business id {}", businessId);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return businesUUID;
    }

    /**
     * Send mail for newly created business
     *
     * @param businessId
     */
    public void sendLoginKingMail(int businessId , int confirmationStatus, int userId, String kingUserEmail, String recipientName, String userUUID) {
        try {
            if (confirmationStatus == 0) {
                //update password for user to login
                String password = tokenUtility.generateRandomString(10);
                String randPwd = password;
                String hashedPassword = cryptoUtility.computeSHAHash(password);
                //update user confirmation status and password
                this.updateUserConfirmStatus(userId, Constants.USER_CONFIG_ACTIVE_STATUS);
                this.updateUserPassword(userId, hashedPassword);
                String businessUUID = this.retrieveBusinessUUIDByID(businessId);

                // update status and pwd in keycloak
                idmService.updateUser(Constants.USER_CONFIG_ACTIVE_STATUS, userUUID, businessUUID);
                idmService.resetUserPassword(userUUID, randPwd, businessUUID);

                //get business configurations
                Map mailParam = businessConfigurationsService.getBusinessConfigMailParams(businessUUID);
                mailParam.put("toEmail", kingUserEmail);
                mailParam.put("recipientName", recipientName);
                String language = Constants.LANGUAGE_EN;
                logger.info("dtapp base {}", dtpDomain);
                String host = dtpDomain.contains("localhost") ? "http://localhost:4200" : "https://"+ dtpDomain;
                logger.info("host {}",host);
                String link = host + "/" + language + "/login/";
                logger.info("link {}",link);
                mailParam.put("link", link);
                mailParam.put("userName", kingUserEmail);
                mailParam.put("password", password);
                mailParam.put("buttonVerbiage", Constants.KING_BUTTON_VERBIAGE);

                String encodedPDF = pdfService.generateSettingsAttachmentPDF(businessUUID);
                if(Utils.notEmptyString(encodedPDF)){
                    mailParam.put("attachment", encodedPDF);
                    mailParam.put("attachmentType", "pdf");
                    mailParam.put("attachmentName", businessUUID+".pdf");
                }

                notificationService.sendLoginMail(mailParam);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * update user password by user id
     *
     * @param userId
     * @param password
     * @return
     */
    public int updateUserPassword(int userId, String password){
        logger.info("userId {}",userId);
        int result = 0;
        String UPDATE_USER_PASSWORD = "UPDATE users SET password = ?, modified_time = ?  where  user_id = ?";
        try{
            result = jdbcTemplate.update(UPDATE_USER_PASSWORD, new Object[]{password, new Timestamp(System.currentTimeMillis()), userId});
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return  result;
    }

    /**
     * Get newly created business with user confirmation status as 0
     * Need to get the business which is invoked save and send activation at first time.
     *
     * @param state
     * @return
     */
    public List getNotConfirmedBusinessUsers(String state) {
        String orderBy = state.equals(Constants.CONTRACT_STATE_INACTIVE) ? "start_date" : "end_date";
        String GET_NOT_CONFIRMED_USERS = "select bs.`id`, bs.`business_id`, bs.`state`,bs.`start_date`, bs.end_date,bs.`precursor_date`," +
                " bs.`timezone`, u.confirmation_status, u.user_id, u.user_email, u.full_name, u.first_name, u.last_name, " +
                " u.user_uuid from business_schedule bs " +
                " left join account a on bs.business_id=a.account_business_id " +
                " right join business_configurations bc on bc.business_id=a.account_business_id " +
                " left join users u on u.user_id=a.account_user_id where bs.state = ? and u.user_role='1' and u.confirmation_status='0' " ;
        GET_NOT_CONFIRMED_USERS += !onprem ? " and (u.okta_user_id is not null and u.okta_user_id <> '') order by ? " : " order by ?";
        List<Map<String, Object>> dbMaps = new ArrayList<Map<String, Object>>();
        try{
            dbMaps = jdbcTemplate.queryForList(GET_NOT_CONFIRMED_USERS, new Object[]{state, orderBy});
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return dbMaps;
    }

    /**
     * Method to get business from business usage table, if  current date exceed monthly end date
     * @return
     */
    public List getBusinessesForMonthlyDurationUpdation() {
        List businessUsageList = new ArrayList();
        //Return business usage details if business usage monthly end date is  also available for the business.
        String sql ="select business_usage_id, business_id, users, programs, responses, metrics, filters, triggers, emails, sms, api, lists, recipients, data_retention, current_month_start, current_month_end, usage_email_flag, whatsapp from business_usage where current_timestamp >= current_month_end limit 1000";
        try{
            businessUsageList = jdbcTemplate.queryForList(sql);
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return businessUsageList;
    }

    /**
     * method to update monthly duration and resetting monthly limits
     */
    public void updateBusinessUsageMonthlyDuration() {
        logger.info("starts update business usage monthly duration");
        try {
            //get all business list where current time >= current_month_end
            List businessList = this.getBusinessesForMonthlyDurationUpdation();
            Iterator<Map> iterator = businessList.iterator();
            while (iterator.hasNext()) {
                try {
                    //loop through each business
                    Map eachBusiness = iterator.next();
                    int eachBusinessId = (int) eachBusiness.get(Constants.BUSINESS_ID);
                    /**when current utc time exceeds current_month_end time
                     * current_month_start = current_utc_time
                     * current_month_end =  current_utc_time + 30 days
                     */
                    String currentUTCDate = Utils.getCurrentUTCDateAsString();
                    String fromDate = Utils.convertToStartOfDay(currentUTCDate);
                    //adding 30 days to current date
                    String toDat = Utils.getFormattedDateTimeAddHours(currentUTCDate, 24 * 30);
                    String toDate = Utils.convertToEndOfDay(toDat);

                    logger.info("currentUTCDate" + currentUTCDate);
                    logger.info("fromDate  " + fromDate);
                    logger.info("toDate  " + toDate);

                    String updateSql = "update business_usage set responses = 0, emails = 0, sms = 0, whatsapp = 0, api = 0, current_month_start = ?, current_month_end = ?  where business_id = ?";
                    int result = jdbcTemplate.update(updateSql, fromDate, toDate, eachBusinessId);

                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        } catch (Exception e) {
            logger.error("exception at businessContractReminder. {}", e.getMessage());
            logger.error(e.getMessage());
        }
        logger.info("Ends update business usage monthly duration");
    }
    /**
     * Method to backup business related tables
     * @param businessUUID
     */
    public void backupBusinessTables(String businessUUID)
    {
        /*String surveysTable = "surveys_" + businessUUID;
        String userSurveyMapTable = "user_survey_map_" + businessUUID;
        String questionsTable = "questions_" + businessUUID;
        String keyMetricTable = "key_metric_" + businessUUID;
        String multiChoiceOptionsTable = "multiple_choice_options_" + businessUUID;
        String surveyFiltersTable = "survey_filters_" + businessUUID;
        String userSurveyFiltersTable = "user_survey_filters_" + businessUUID;
        String userAlertsTable = "user_alerts_" + businessUUID;
        String notificationTable = "notifications_" + businessUUID;
        String tokenTable = "token_" + businessUUID;
        String staticTokenTable = "static_token_" + businessUUID;
        String qrCodeTable = "qrcode_" + businessUUID;
        String surveyTempalteTable = "templates_" + businessUUID;*/

        String surveysTable = String.format("surveys_%s", businessUUID.replaceAll("-", "_"));
        String userSurveyMapTable = String.format("user_survey_map_%s", businessUUID.replaceAll("-", "_"));
        String questionsTable = String.format("questions_%s", businessUUID.replaceAll("-", "_"));
        String keyMetricTable = String.format("key_metric_%s", businessUUID.replaceAll("-", "_"));
        String multiChoiceOptionsTable = String.format("multiple_choice_options_%s", businessUUID.replaceAll("-", "_"));
        String surveyFiltersTable = String.format("survey_filters_%s", businessUUID.replaceAll("-", "_"));
        String userSurveyFiltersTable = String.format("user_survey_filters_%s", businessUUID.replaceAll("-", "_"));
        String userAlertsTable = String.format("user_alerts_%s", businessUUID.replaceAll("-", "_"));
        String notificationTable = String.format("notifications_%s", businessUUID.replaceAll("-", "_"));
        String tokenTable = String.format("token_%s", businessUUID.replaceAll("-", "_"));
        String staticTokenTable = String.format("static_token_%s", businessUUID.replaceAll("-", "_"));
        String qrCodeTable = String.format("qrcode_%s", businessUUID.replaceAll("-", "_"));
        String surveyTempalteTable = String.format("templates_%s", businessUUID.replaceAll("-", "_"));

        /**
         * backup tables
         */
        /*String surveysTableBcup = "surveys_bcup" + businessUUID;
        String userSurveyMapTableBcup = "user_survey_map_bcup" + businessUUID;
        String questionsTableBcup = "questions_bcup" + businessUUID;
        String keyMetricTableBcup = "key_metric_bcup" + businessUUID;
        String multiChoiceOptionsTableBcup = "multiple_choice_bcup" + businessUUID;
        String surveyFiltersTableBcup = "survey_filters_bcup" + businessUUID;
        String userSurveyFiltersTableBcup = "user_survey_filters_bcup" + businessUUID;
        String userAlertsTableBcup = "user_alerts_bcup" + businessUUID;
        String notificationTableBcup = "notifications_bcup" + businessUUID;
        String tokenTableBcup = "token_bcup" + businessUUID;
        String staticTokenTableBcup = "static_token_bcup" + businessUUID;
        String qrCodeTableBcup = "qrcode_bcup" + businessUUID;
        String surveyTempalteTableBcup = "templates_bcup" + businessUUID;*/

        String surveysTableBcup = String.format("surveys_bcup%s", businessUUID.replaceAll("-", "_"));
        String userSurveyMapTableBcup = String.format("user_survey_map_bcup%s", businessUUID.replaceAll("-", "_"));
        String questionsTableBcup = String.format("questions_bcup%s", businessUUID.replaceAll("-", "_"));
        String keyMetricTableBcup = String.format("key_metric_bcup%s", businessUUID.replaceAll("-", "_"));
        String multiChoiceOptionsTableBcup = String.format("multiple_choice_bcup%s", businessUUID.replaceAll("-", "_"));
        String surveyFiltersTableBcup = String.format("survey_filters_bcup%s", businessUUID.replaceAll("-", "_"));
        String userSurveyFiltersTableBcup = String.format("user_survey_filters_bcup%s", businessUUID.replaceAll("-", "_"));
        String userAlertsTableBcup = String.format("user_alerts_bcup%s", businessUUID.replaceAll("-", "_"));
        String notificationTableBcup = String.format("notifications_bcup%s", businessUUID.replaceAll("-", "_"));
        String tokenTableBcup = String.format("token_bcup%s", businessUUID.replaceAll("-", "_"));
        String staticTokenTableBcup = String.format("static_token_bcup%s", businessUUID.replaceAll("-", "_"));
        String qrCodeTableBcup = String.format("qrcode_bcup%s", businessUUID.replaceAll("-", "_"));
        String surveyTempalteTableBcup = String.format("templates_bcup%s", businessUUID.replaceAll("-", "_"));

        /**
         * survey table backup
         */
        String checkSurveySql = "SELECT * FROM information_schema.tables WHERE table_schema = 'DT' AND table_name = '"+ surveysTable + "'";
        List surveyList = jdbcTemplate.queryForList(checkSurveySql);
        if(surveyList.size()>0)
        {
            String surveyBackupSql = "insert into "+surveysTableBcup+" select * from "+surveysTable;
            int surveyBackupResult = jdbcTemplate.update(surveyBackupSql);
        }
        /**
         * user_survey_map backup
         */
        String checkUserSurveyMapSql = "SELECT * FROM information_schema.tables WHERE table_schema = 'dt' AND table_name = '"+ userSurveyMapTable + "'";
        List userSurveyMapList = jdbcTemplate.queryForList(checkUserSurveyMapSql);
        if(userSurveyMapList.size()>0)
        {
            String userSurveyMapBackupSql = "insert into "+userSurveyMapTableBcup+" select * from "+ userSurveyMapTable;
            int userSurveyMapBackupResult = jdbcTemplate.update(userSurveyMapBackupSql);
        }
        /**
         * questions backup
         */
        String checkQuestionSql = "SELECT * FROM information_schema.tables WHERE table_schema = 'DT' AND table_name = '"+ questionsTable + "'";
        List questionList = jdbcTemplate.queryForList(checkQuestionSql);
        if(questionList.size()>0)
        {
            String questionBackupSql = "insert into "+questionsTableBcup+" select * from "+questionsTable;
            int questionBackupResult = jdbcTemplate.update(questionBackupSql);
        }
        /**
         * key_metric_backup
         */
        String checkKeyMetricSql = "SELECT * FROM information_schema.tables WHERE table_schema = 'DT' AND table_name = '"+ keyMetricTable + "'";
        List keyMetricList = jdbcTemplate.queryForList(checkKeyMetricSql);
        if(keyMetricList.size()>0)
        {
            String keyMetricBackupSql = "insert into "+keyMetricTableBcup+" select * from "+keyMetricTable;
            int keyMetricBackupResult = jdbcTemplate.update(keyMetricBackupSql);
        }
        /**
         * multiple_choice_options_backup
         */
        String checkMultipleChoiceOptionSql = "SELECT * FROM information_schema.tables WHERE table_schema = 'DT' AND table_name = '"+ multiChoiceOptionsTable + "'";
        List multipleChoiceList = jdbcTemplate.queryForList(checkMultipleChoiceOptionSql);
        if(multipleChoiceList.size()>0)
        {
            String multipleChoiceBackupSql = "insert into "+multiChoiceOptionsTableBcup+" select * from "+multiChoiceOptionsTable;
            int multipleChoiceBackupResult = jdbcTemplate.update(multipleChoiceBackupSql);
        }
        /**
         * survey_filters_backup
         */
        String checkSurveyFilterSql = "SELECT * FROM information_schema.tables WHERE table_schema = 'DT' AND table_name = '"+ surveyFiltersTable + "'";
        List surveyFilterList = jdbcTemplate.queryForList(checkSurveyFilterSql);
        if(surveyFilterList.size()>0)
        {
            String surveyFilterBackupSql = "insert into "+surveyFiltersTableBcup+" select * from "+surveyFiltersTable;
            int surveyFiltersBackupResult = jdbcTemplate.update(surveyFilterBackupSql);
        }
        /**
         * user_survey_filters_backup
         */
        String checkUserSurveyFilterSql = "SELECT * FROM information_schema.tables WHERE table_schema = 'DT' AND table_name = '"+ userSurveyFiltersTable + "'";
        List userSurveyFilterList = jdbcTemplate.queryForList(checkUserSurveyFilterSql);
        if(userSurveyFilterList.size()>0)
        {
            String userSurveyFilterBackupSql = "insert into "+userSurveyFiltersTableBcup+" select * from "+userSurveyFiltersTable;
            int userSurveyFilterBackupResult = jdbcTemplate.update(userSurveyFilterBackupSql);
        }
        /**
         * user_alerts_backup
         */
        String checkUserAlertSql = "SELECT * FROM information_schema.tables WHERE table_schema = 'DT' AND table_name = '"+ userAlertsTable + "'";
        List userAlertList = jdbcTemplate.queryForList(checkUserAlertSql);
        if(userAlertList.size()>0)
        {
            String userAlertBackupSql = "insert into "+userAlertsTableBcup+" select * from "+userAlertsTable;
            int userAlertBackupResult = jdbcTemplate.update(userAlertBackupSql);
        }
        /**
         * notifications_backup
         */
        String checkNotificationSql = "SELECT * FROM information_schema.tables WHERE table_schema = 'DT' AND table_name = '"+ notificationTable + "'";
        List notificationList = jdbcTemplate.queryForList(checkNotificationSql);
        if(notificationList.size()>0)
        {
            String notificationBackupSql = "insert into "+notificationTableBcup+" select * from "+notificationTable;
            int notificationBackupResult = jdbcTemplate.update(notificationBackupSql);
        }
        /**
         * token_backup
         */
        String checkTokenSql = "SELECT * FROM information_schema.tables WHERE table_schema = 'DT' AND table_name = '"+ tokenTable + "'";
        List tokenList = jdbcTemplate.queryForList(checkTokenSql);
        if(tokenList.size()>0)
        {
            String tokenBackupSql = "insert into "+tokenTableBcup+" select * from "+tokenTable;
            int tokenBackupResult = jdbcTemplate.update(tokenBackupSql);
        }
        /**
         * static_token_backup
         */
        String checkStaticTokenSql = "SELECT * FROM information_schema.tables WHERE table_schema = 'DT' AND table_name = '"+ staticTokenTable + "'";
        List staticTokenList = jdbcTemplate.queryForList(checkStaticTokenSql);
        if(staticTokenList.size()>0)
        {
            String staticTokenBackupSql = "insert into "+staticTokenTableBcup+" select * from "+staticTokenTable;
            int staticTokenBackupResult = jdbcTemplate.update(staticTokenBackupSql);
        }
        /**
         * qrcode_backup
         */
        String checkQrCodeSql = "SELECT * FROM information_schema.tables WHERE table_schema = 'DT' AND table_name = '"+ qrCodeTable + "'";
        List qrCodeList = jdbcTemplate.queryForList(checkQrCodeSql);
        if(qrCodeList.size()>0)
        {
            String qrCodeBackupSql = "insert into "+qrCodeTableBcup+" select * from "+qrCodeTable;
            int qrCodeBackupResult = jdbcTemplate.update(qrCodeBackupSql);
        }
        /**
         * templates_backup
         */
        String checkTemplateSql = "SELECT * FROM information_schema.tables WHERE table_schema = 'DT' AND table_name = '"+ surveyTempalteTable + "'";
        List templateList = jdbcTemplate.queryForList(checkTemplateSql);
        if(templateList.size()>0)
        {
            String templateBackupSql = "insert into "+surveyTempalteTableBcup+" select * from "+surveyTempalteTable;
            int templateBackupResult = jdbcTemplate.update(templateBackupSql);
        }
    }


    /**
     * method to delete expired terminated tokens from terminated_session_tokens table
     */
    public void updateTerminatedTokens() {
        logger.info("starts removing terminated tokens hourly basis");
        try {
            //removing  terminated tokens  where current time >= expiration_time
            String deleteSql = "delete from terminated_session_tokens where current_timestamp >= expiration_time";
            jdbcTemplate.update(deleteSql);
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        logger.info("End removing terminated tokens hourly basis");
    }

    /**
     * delete api key when the contract is expired.
     * @param businessId
     */
    public void deleteAPIKeyByBusinessId(int businessId){
        logger.info("starts deleting api key by businessId");
        try {
            if(businessId > 0) {

                String deleteSql = "delete from api_key where business_id = ?";
                jdbcTemplate.update(deleteSql);

            }
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        logger.info("End deleting api key by businessId");
    }

    /**
     * method to deactivate api_key once business is expired & turning off api_key in business_configurations
     *
     */
    public void disableAPIKeyByBusinessId(int businessId) {
        logger.info("starts deleting api key by businessId");
        try {
            if(businessId > 0) {
                //removing  terminated tokens  where current time >= expiration_time
                String apiUpdatesql = "update api_key set enabled = 0 where business_id = ?";
                jdbcTemplate.update(apiUpdatesql, new Object[]{businessId});

                String updateSql = "update business_configurations set api_key = 0 where business_id = ?";
                jdbcTemplate.update(updateSql, new Object[]{businessId});
            }
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        logger.info("End deleting api key by businessId");
    }

    /**
     * method to delete expired terminated tokens from terminated_session_tokens table
     */
   /* public void validateShortUrlLink() {
        logger.info("starts validateShortUrlLink");
        try {
            //get list of generated short url links
            String selectSql = "select short_url, long_url, unique_token, business_id, survey_id, link_group_id, link_id from generated_short_links order by created_time desc limit 1000";
            List resultLit = jdbcTemplate.queryForList(selectSql);
            Iterator itr = resultLit.iterator();
            //iterate each record
            while (itr.hasNext()) {
                try {
                    Map eachMap = (Map) itr.next();
                    String shortUrl = eachMap.get("short_url").toString();
                    String longUrl = eachMap.get("long_url").toString();
                    String token = eachMap.get("unique_token").toString();
                    String businessUniqueId = eachMap.get("business_id").toString();
                    int surveyId = (int) eachMap.get("survey_id");
                    String linkId = eachMap.get("link_id").toString();
                    String linkGroupId = eachMap.get("link_group_id").toString();
                    String existingShortCode = "";
                    //get only shorturl code
                    if(Utils.isNotNull(shortUrl)&&shortUrl.length()>19) {
                        existingShortCode = shortUrl.substring(19);
                    }
                    // call api to create the shorturl link
                    String newShortUrl = generateShortUrl(businessUniqueId, surveyId, longUrl, existingShortCode);
                    // if shorturl code exists already then generate new one and update in static token table
                    //if(Utils.notEmptyString(newShortUrl) && !shortUrl.equals(newShortUrl) && !newShortUrl.equals(longUrl)) {
                    if(Utils.notEmptyString(newShortUrl)) {
                        logger.info("update the short url in the static token");
                        String updateSql = "update static_token_" + businessUniqueId.replaceAll("-", "_") + " set short_url = ? where static_token_id = ? and survey_id = ? and link_group_id = ? and link_id = ?";
                        int updateResult = jdbcTemplate.update(updateSql, new Object[]{newShortUrl, token, surveyId, linkGroupId, linkId});
                        //logger.info("update result {}",updateResult);
                    }

                    //remove the entry once short urls created
                    String deleteSql = "delete from generated_short_links where unique_token = ? and survey_id = ? and business_id = ? and link_id = ?";
                    jdbcTemplate.update(deleteSql, new Object[]{token, surveyId, businessUniqueId, linkId});
                }catch (Exception e){
                    logger.error(e.getMessage());
                    logger.error("exception at validateShortUrlLink. Msg {}", e.getMessage());
                }
            }
        }catch (Exception e){
            logger.error("exception at validateShortUrlLink. Msg {}", e.getMessage());
            logger.error(e.getMessage());
        }
        logger.info("End validateShortUrlLink");
    }*/

    /**
     * method to delete expired terminated tokens from terminated_session_tokens table
     */
    public void validateShortUrlLinkV2() {
        logger.info("starts validateShortUrlLinkV2");
        try {
            //get list of generated short url links
            String selectSql = "select short_url, long_url, unique_token, business_id, survey_id, link_group_id, link_id " +
                    "from generated_short_links where status is null order by created_time desc limit 1000";
            List resultLit = jdbcTemplate.queryForList(selectSql);
            boolean allRecordsSent = false;
            int endIndex =0;
            int startIndex = 0;
            int length = 20;
            while (!allRecordsSent) {
                endIndex = startIndex + length;
                if (endIndex >= resultLit.size()) {
                    endIndex = resultLit.size();
                    allRecordsSent = true;
                }
                List resulstSubList = resultLit.subList(startIndex, endIndex);
                Iterator<Map> itr = resulstSubList.iterator();
                List shortURlList = new ArrayList();
                List tokenList = new ArrayList();
                List invalidTokenList = new ArrayList();

                while (itr.hasNext()) {
                    try {
                        Map eachMap = itr.next();
                        String longUrl = eachMap.get("long_url").toString();
                        String token = eachMap.get("unique_token").toString();
                        String businessUniqueId = eachMap.get("business_id").toString();
                        int surveyId = (int) eachMap.get("survey_id");

                        Map surveyData = this.returnProgramByID(surveyId, businessUniqueId);
                        if (surveyData.containsKey("id")) {
                            String surveyEndDate = surveyData.get("to_date").toString();
                            String ttl = Utils.computeTTL(surveyEndDate);
                            if(Utils.notEmptyString(ttl)) {
                                Map shortUrlMap = this.formShortUrlRequestList(longUrl, ttl, businessUniqueId);
                                shortUrlMap.put("token", token); // add token and bid in the request to utilize later
                                shortUrlMap.put("businessUniqueId", businessUniqueId);
                                shortURlList.add(shortUrlMap);
                            }else{
                                invalidTokenList.add(token);
                            }
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        logger.error("exception at validateShortUrlLinkV2. Msg {}", e.getMessage());
                    }
                }
                startIndex = startIndex + length;

                List responseList = Collections.emptyList();
                if(!shortURlList.isEmpty()) {
                    // call api to create the shorturl link
                    responseList = generateShortUrlV2(shortURlList);
                }

                //iterator the response
                Iterator itr1 = responseList.iterator();
                while(itr1.hasNext()) {
                    try {
                        Map eachResponse = (Map) itr1.next();
                        String bid = eachResponse.get("businessUniqueId").toString().replaceAll("-", "_");
                        String url = eachResponse.get("shortUrl").toString();
                        String token = eachResponse.get("token").toString();
                        //build dynamic query to update the static token table based on buisnessId
                        if (Utils.notEmptyString(url) && url.contains("https://")) { //check portocol is available in the shorturl to fix DTV-5463
                            logger.info("update the short url in the static token");
                            String updateSql= " update static_token_" + bid.replaceAll("-", "_") + " set short_url = '" + url + "' where static_token_id = '" + token + "' ";
                            int updateResult = jdbcTemplate.update(updateSql);
                            tokenList.add(token);
                        }
                    }catch (Exception e){
                        logger.error("exception at eachResponse loop.Msg {]", e.getMessage());
                    }
                }

                /*List<Object[]> batchArgs = new ArrayList<>();

                while (itr1.hasNext()) {
                    try {
                        Map eachResponse = (Map) itr1.next();
                        String bid = eachResponse.get("businessUniqueId").toString().replaceAll("-", "_");
                        String url = eachResponse.get("shortUrl").toString();
                        String token = eachResponse.get("token").toString();

                        // Build dynamic query values if URL is valid
                        if (Utils.notEmptyString(url) && url.contains("https://")) {
                            String tableName = "static_token_" + bid;
                            batchArgs.add(new Object[]{tableName, url, token});
                            tokenList.add(token); // Add token to list for further processing
                        }
                    } catch (Exception e) {
                        logger.error("Exception in eachResponse loop. Msg: {}", e.getMessage());
                    }
                }

                // Execute batch update if there are records to process
                if (!batchArgs.isEmpty()) {
                    String sql = "UPDATE ? SET short_url = ? WHERE static_token_id = ?";
                    jdbcTemplate.batchUpdate(sql, batchArgs, batchArgs.size(),
                            (ps, argument) -> {
                                ps.setString(1, argument[0].toString());
                                ps.setString(2, argument[1].toString());
                                ps.setString(3, argument[2].toString());
                            }
                    );
                    logger.info("Batch update executed for short URLs.");
                }*/


                //remove the entry once short urls created
                if(tokenList.size() > 0) {
                    /*String deleteSql = "delete from generated_short_links where unique_token in (:tokens)";
                    MapSqlParameterSource param = new MapSqlParameterSource();
                    param.addValue("tokens", tokenList);
                    namedParameterJdbcTemplate.update(deleteSql, param);*/
                    deleteGeneratedShortLinks(tokenList);
                }
                if(invalidTokenList.size() > 0){
                    updateInvalidShortLinksStatus(invalidTokenList);
                }
            }
        }catch (Exception e){
            logger.error("exception at validateShortUrlLinkV2. Msg {}", e.getMessage());
            e.printStackTrace();
        }
        logger.info("End validateShortUrlLinkV2");
    }

    /**
     * delete the generated short url links entries from generated_short_links table
     * @param tokenList
     */
    private void deleteGeneratedShortLinks(List<String> tokenList) {
        try {
            if (tokenList.size() > 0) {
                String deleteSql = "delete from generated_short_links where unique_token in (:tokens)";
                MapSqlParameterSource param = new MapSqlParameterSource();
                param.addValue("tokens", tokenList);
                namedParameterJdbcTemplate.update(deleteSql, param);
            }
        } catch (Exception e) {
            logger.error("exception at deleteGeneratedShortLinks. Msg {}", e.getMessage());
        }
    }

    /**
     * Method to update invalid short links ttl values with status as invalid
     * @param invalidTokenList
     */
    private void updateInvalidShortLinksStatus(List<String> invalidTokenList) {
        try {
            if (invalidTokenList.size() > 0) {
                String deleteSql = "update generated_short_links set status= 'invalid' where unique_token in (:tokens)";
                MapSqlParameterSource param = new MapSqlParameterSource();
                param.addValue("tokens", invalidTokenList);
                namedParameterJdbcTemplate.update(deleteSql, param);
            }
        } catch (Exception e) {
            logger.error("exception at updateInvalidShortLinksStatus. Msg {}", e.getMessage());
        }
    }

    /**
     * method to get short url for given suveyId with long url
     *
     * @param businessUniqueId
     * @param surveyId
     * @param longUrl
     * @return
     */
    /*public String generateNewShortUrl(String businessUniqueId, int surveyId, String longUrl) {
        String newShortUrl = "";
        try {
            Map surveyData = this.returnProgramByID(surveyId, businessUniqueId);
            if (surveyData.containsKey("id")) {
                String surveyEndDate = surveyData.get("to_date").toString();
                String ttl = Utils.computeTTL(surveyEndDate);
                boolean stringExists = false;
                Map resultMap = new HashMap();
                Map shortUrlMap = this.formShortUrlRequestList(longUrl, ttl, businessUniqueId);
                List shortUrlList = new ArrayList();
                shortUrlList.add(shortUrlMap);
                String baseUrl = dt360UrlRead;
                String uri = dt360Url;
                while (!stringExists) {
                    List resultList = this.callShortUrlService(shortUrlList, HttpMethod.POST, uri);
                    resultMap = resultList.size() > 0 ? (Map) resultList.get(0) : resultMap;
                    if (resultMap.containsKey("status")) { //if true - random string already exists in the system
                        stringExists = false;
                    } else {
                        stringExists = true;
                    }
                }
                newShortUrl = resultMap.containsKey("shortUrl") && Utils.notEmptyString(resultMap.get("shortUrl").toString()) ? baseUrl + (resultMap.get("shortUrl").toString()) : longUrl;
            }
        } catch (Exception e) {
            logger.error("exception at generateNewShortUrl msg {}", e.getMessage());
        }
        return newShortUrl;
    }*/

    /**
     * method to get short url for given suveyId with long url
     *
     * @param businessUniqueId
     * @param surveyId
     * @param longUrl
     * @return
     */
   /* public String generateShortUrl(String businessUniqueId, int surveyId, String longUrl, String shortUrl) {
        String newShortUrl = "";
        try {
            Map surveyData = this.returnProgramByID(surveyId, businessUniqueId);
            if (surveyData.containsKey("id")) {
                String surveyEndDate = surveyData.get("to_date").toString();
                String ttl = Utils.computeTTL(surveyEndDate);
                boolean stringExists = false;
                Map resultMap = new HashMap();
                Map shortUrlMap = this.formShortUrlRequestList(longUrl, ttl, businessUniqueId);
                List shortUrlList = new ArrayList();
                shortUrlList.add(shortUrlMap);
                String baseUrl = dt360UrlRead;
                String uri = dt360Url;
                while (!stringExists) {
                    List resultList = this.callShortUrlService(shortUrlList, HttpMethod.POST, uri);
                    resultMap = resultList.size() > 0 ? (Map) resultList.get(0) : resultMap;
                    if (resultMap.containsKey("status")) { //if true - random string already exists in the system
                        stringExists = false;
                    } else {
                        stringExists = true;
                    }
                }

                newShortUrl = resultMap.containsKey("shortUrl") && Utils.notEmptyString(resultMap.get("shortUrl").toString()) ? baseUrl + (resultMap.get("shortUrl").toString()) : longUrl;
            }
        } catch (Exception e) {
            logger.error("exception at generateNewShortUrl msg {}", e.getMessage());
        }
        logger.info("new short url {}", newShortUrl);
        return newShortUrl;
    }
*/
    /**
     * method to get short url for given suveyId with long url
     *
     * @param shortUrlList
     * @return
     */
    public List generateShortUrlV2(List shortUrlList) {
        Map newShortUrlMap = new HashMap();
        try {
            String baseUrl = dt360UrlRead;
            String uri = dt360Url;
            List resultList = this.callShortUrlService(shortUrlList, HttpMethod.POST, uri);
            Iterator itr = resultList.iterator();
            int i = 0;
            while (itr.hasNext()) {
                Map resultMap = (Map) itr.next();
                //get each request index
                Map eachShortUrlReq = ((Map) shortUrlList.get(i));
//                String longUrl = eachShortUrlReq.get("longUrl").toString();
                String shortUrl = resultMap.get("shortUrl").toString();
                if (resultMap.containsKey("status")) { //if true - random string already exists in the system
                    boolean isExist = false;
                    //set the request of shortUrl
                    List lst = new ArrayList();
                    lst.add(eachShortUrlReq);
                    while(!isExist) {
                        List reList = this.callShortUrlService(shortUrlList, HttpMethod.POST, uri);
                        Map result = reList.size() > 0 ? (Map) reList.get(0) : resultMap;
                        if (resultMap.containsKey("status")) { //if true - random string already exists in the system
                            isExist = false;
                        } else {
                            String newshortUrl = result.containsKey("shortUrl") && Utils.notEmptyString(result.get("shortUrl").toString()) ? baseUrl + (result.get("shortUrl").toString()) : "";
                            eachShortUrlReq.put("shortUrl", newshortUrl);
                            isExist = true;
                        }
                    }
                } else {
                    String newShortUrl = resultMap.containsKey("shortUrl") && Utils.notEmptyString(resultMap.get("shortUrl").toString()) ? baseUrl + (resultMap.get("shortUrl").toString()) : "";
                    eachShortUrlReq.put("shortUrl", newShortUrl);
                }
                i++;
            }

        } catch (Exception e) {
            logger.error("exception at generateNewShortUrl msg {}", e.getMessage());
        }
        logger.info("new short url {}", new JSONObject(newShortUrlMap).toString());
        return shortUrlList;
    }

    /**
     * generate random string for short url service
     * @param longUrl
     * @return
     */
    public Map formShortUrlRequestList(String longUrl, String ttl, String businessUniqueId){
        Map shortUrlMap = new HashMap();
        try {
            if(Utils.notEmptyString(ttl)) {
                boolean isShortUrlEnabled = this.isShortUrlEnabledForBusiness(businessUniqueId);
                String customUrlMask = isShortUrlEnabled ? this.getUrlMaskByBusiness(businessUniqueId) : "";
                //generate random string
                String generatedString = RandomStringUtils.randomAlphanumeric(10);
                //input map
                shortUrlMap.put("longUrl", longUrl);
                shortUrlMap.put("shortUrl", Utils.notEmptyString(customUrlMask) ? customUrlMask + "/" + generatedString : generatedString);
                shortUrlMap.put("ttl", ttl);
                logger.info("time to live {}", ttl);
            }
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return shortUrlMap;
    }

    /**
     * method to get url mask for business
     * @param businessId
     * @return
     */
    public String getUrlMaskByBusiness(String businessId) {
        String urlMask = "";
        try {
            List businessSettingsList = jdbcTemplate.queryForList("select * from business_global_settings where business_id = (select business_id from business where business_uuid=?)", new Object[]{businessId});
            if (businessSettingsList.size() > 0) {
                Map eachBusinessSettingsMap = (Map) businessSettingsList.get(0);
                urlMask = eachBusinessSettingsMap.containsKey("url_mask") ? (String) eachBusinessSettingsMap.get("url_mask") : "";
            }
        } catch (Exception e) {
            logger.error("exception at getUrlMaskByBusiness, msg {}", e.getMessage());
        }
        return  urlMask;
    }

    /**
     * return a program by programUUID
     *
     * @param programID
     * @param businessUUID
     * @return
     */
    private Map returnProgramByID(int programID, String businessUUID) {
        Map programMap = new HashMap();
        try {
            String businessUUIDParsed = businessUUID.replace("-", "_");
            String tableName = "surveys_" + businessUUIDParsed;
            String surveySql = "select id, from_date, to_date from `" + tableName + "` where id = ?";
            List programs = jdbcTemplate.queryForList(surveySql, new Object[]{programID});
            if (programs.size() > 0) {
                programMap = (Map) programs.get(0);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return programMap;
    }

    /**
     * Rest api to call DT yass short url service
     *
     * @param shortUrlList
     * @param method
     * @return
     */
    private List callShortUrlService(List shortUrlList, HttpMethod method, String uri) {

        List responseMap = new ArrayList();
        JSONArray json = new JSONArray(shortUrlList);
        String inputJson = json.toString();
        logger.debug(inputJson);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);
        try {
            ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, method, entity, Object.class);
            if (responseEntity != null) {
                if (responseEntity.getBody() != null) {
                    //logger.info("Short URL request has been sent successfully!");
                    //logger.debug(responseEntity.getBody().toString());
                    responseMap = (List) responseEntity.getBody();
                }
            }
        } catch (HttpClientErrorException e) {
            logger.error("403 forbidden!");
            //logger.error(e.getMessage());
            //responseMap.put("success",false);
        }
        return responseMap;
    }

    public void insertQrFromTempToMain() {
        logger.info("begin insert qr from temp to main");
        String selectSql = "select * from generated_qrcode_link order by created_time desc";
        List resultLit = jdbcTemplate.queryForList(selectSql);
        Iterator itr = resultLit.iterator();
        //iterate each record
        while (itr.hasNext()) {
            try {
                Map eachMap = (Map) itr.next();
                int width = Integer.parseInt(eachMap.get("width").toString());
                int height = Integer.parseInt(eachMap.get("height").toString());
                String static_token_id = eachMap.get("static_token_id").toString();
                int businessUniqueId = Integer.parseInt(eachMap.get("business_id").toString());
                String qr_file_type = eachMap.get("qr_file_type").toString();
                String tag_link_id = eachMap.get("tag_link_id").toString();
                String qrcode_survey_id = eachMap.get("qrcode_survey_id").toString();
                String link_group_id = eachMap.get("link_group_id").toString();
                String link_id = eachMap.get("link_id").toString();
                //String language = eachMap.get("languages").toString();
                String businessUUID = retrieveBusinessUUIDByID(businessUniqueId);
                logger.debug("qrfiletype = "+qr_file_type);
                logger.debug("statictokenid = "+static_token_id);
                logger.debug("qrcodesurveyid = "+qrcode_survey_id);
                logger.debug("linkgroupid = "+link_group_id);
                logger.debug("linkid = "+link_id);
                logger.debug("taglinkid = "+tag_link_id);
                logger.debug("businessid = "+businessUniqueId);

                CustomizeQRCodeBean bean = qrCustomizationService.getTagLinkQRCustomizationByTagId(tag_link_id, businessUUID);
                // call api to create qrCodeUrl

                byte[] qrcode = null;
                if(bean != null) //customization enabled links
                    qrcode = generateQrCode(businessUUID,"en", width, height,
                                                            qr_file_type, static_token_id, bean.getForeColor(), bean.getBackColor(), bean.getLogoUrl());
                else
                    qrcode = generateQrCode(businessUUID,"en", width, height,
                            qr_file_type, static_token_id, "");


                if(Utils.isNotNull(qrcode)) {
                    logger.debug("update the qrcode in the static token");
                    String updateSql = "update qrcode_" + businessUUID.replaceAll("-", "_") +
                            " set survey_qrcode = ? where qr_file_type = ? and tag_link_id = ? and static_token_id = ? and qrcode_survey_id = ? and link_group_id = ? and link_id = ?";
                    int updateResult = jdbcTemplate.update(updateSql, new Object[]{qrcode, qr_file_type, tag_link_id ,static_token_id, qrcode_survey_id, link_group_id, link_id});
                    //logger.info("update result {}",updateResult);
                }

                //remove the entry once qrcode urls created
                String deleteSql = "delete from generated_qrcode_link where qr_file_type = ? and tag_link_id = ? and static_token_id = ? and qrcode_survey_id = ? and link_group_id = ? and link_id = ?";
                jdbcTemplate.update(deleteSql, new Object[]{qr_file_type, tag_link_id, static_token_id, qrcode_survey_id, link_group_id, link_id});
            }catch (Exception e){
                logger.error("Error in insertQrFromTempToMain() : ", e.getMessage());
            }
        }
        logger.info("end insert qr from temp to main");
    }

    private byte[] generateQrCode(String businessUUID, String language, int width,
                                  int height, String qrFileType, String staticToken, String url) {

        byte[] qrCode = null;

        try {
            String longUrl = "";
            //String staticToken = tokenUtility.generateToken();
            //qrString = url + uniqueToken + "$" + tmpString+"?source=qr";
            longUrl = Utils.notEmptyString(url) ? url : constructLongUrl(businessUUID, staticToken, language, "DT", "qr");
            logger.info("qrUrl  " + longUrl);
            logger.debug("eachQrFile type " + qrFileType);
            qrCode = getQRCodeImage(longUrl, qrFileType, width, height);
        } catch (Exception e) {
            logger.error("Error in generateQrCode() : " + e.getMessage());
        }

        return qrCode;
    }

    private byte[] generateQrCode(String businessUUID, String language, int width,
                                  int height, String qrFileType, String staticToken, String frontColor, String backColor, String logo) {

        byte[] qrCode = null;

        try {
            String longUrl = "";
            longUrl = constructLongUrl(businessUUID, staticToken, language, "DT", "qr");
            logger.info("qrUrl  " + longUrl);
            logger.debug("eachQrFile type " + qrFileType);
            qrCode = getCustomizedQRCodeImage(language, qrFileType, width, height, frontColor, backColor, logo);
        } catch (Exception e) {
            logger.error("Error in generateQrCode() : " + e.getMessage());
        }

        return qrCode;
    }

    /**
     * Get qr code url from token
     * @param businessUUID
     * @return
     */
    public String constructLongUrl(String businessUUID, String token, String language, String cc, String source){
        String longUrl = "";
        try{
            if(Utils.notEmptyString(businessUUID) && Utils.notEmptyString(token)) {
                //token = token + "$" + DatatypeConverter.printBase64Binary(cc.getBytes("UTF-8")).toLowerCase();
                String tmpString = TokenUtility.getTokenTempString(cc);
                //String tmpString = DatatypeConverter.printBase64Binary(cc.getBytes("UTF-8")).toLowerCase();
                token = token + Constants.TOKEN_DELIMITER + tokenUtility.encodeBusinessID(businessUUID) + "$" + tmpString +"?source="+source;

                String hostname = dtpDomain;//request.getHeader("host");
                //SE-1160 Fetching EUX_URL from application.prop
                hostname = hostname.contains("localhost") ? "https://test.dropthought.com" : "https://" + euxDomain; //Utils.computeHostnameEUX(hostname); //http:localhost:4200 is not getting resolved in bitly hence for testing purpose changed to test.dropthought.com
                //DTV-3398, to prevent language as null in long url
                language = (Utils.isNull(language) || Utils.emptyString(language) || language.equalsIgnoreCase("null")) ? "en" : language;
                longUrl = hostname + "/" + language + "/dt/survey/" + token;
            }

        } catch (Exception e){
            logger.error("Error in getQrCodeUrlByToken() : " + e.getMessage());
        }
        return longUrl;
    }

    private byte[] getQRCodeImage(String text, String fileType, int width, int height) {

        byte[] imgData = null;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        //ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            if(fileType.equalsIgnoreCase("pdf"))
            {
                Document document = new Document();
                PdfWriter writer = PdfWriter.getInstance(document, outputStream);
                document.open();
                BarcodeQRCode barcodeQRCode = new BarcodeQRCode(text, 1000, 1000, null);
                Image codeQrImage = barcodeQRCode.getImage();
                codeQrImage.scaleAbsolute(width,height);
                document.add(codeQrImage);
                document.close();
                imgData = outputStream.toByteArray();

            }
            else {
                QRCodeWriter qrCodeWriter = new QRCodeWriter();
                BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
                MatrixToImageWriter.writeToStream(bitMatrix, fileType, outputStream);
                imgData = outputStream.toByteArray();
            }
        } catch (WriterException | IOException | DocumentException e) {
            logger.error(e.getMessage());
        } finally{
            try {
                outputStream.close();
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
        return imgData;
    }

    /**
     * method to call rest api and get the byte array for given qr details
     * @param text
     * @param fileType
     * @param width
     * @param height
     * @return
     */
    private byte[] getCustomizedQRCodeImage(String text, String fileType, int width, int height, String foreColor, String backColor, String logo) {

        byte[] imgData = null;
        try {
            final String uri = dtAppUrl + "customizeqr/bytearray?qrlink" + text + "&fileType=" +
                    fileType + "&width=" + width + "&height=" + height + "&forecolor=" + foreColor +
                    "&backColor=" + backColor + "&logoImg=" + logo;
            logger.info("qr cusomized url  {}", uri);
            Map resultMap = restTemplateService.callRestApi(uri, HttpMethod.GET);
            imgData = resultMap.containsKey("result") ? (byte[]) resultMap.get("result") : null;

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return imgData;
    }



    /**
     * Send password update mail for newly created business
     *
     * @param businessId
     */
    public void sendPasswordUpdateKingMail(int businessId , int confirmationStatus, int userId, String kingUserEmail, String recipientName, String userUUID) {
        try {
            if (confirmationStatus == 0) {
                //update password for user to login
                String password = tokenUtility.generateRandomString(10);
                String randPwd = password;
                String hashedPassword = cryptoUtility.computeSHAHash(password);
                //update user confirmation status and password
                this.updateUserConfirmStatus(userId, Constants.USER_CONFIG_ACTIVE_STATUS);
                //todo : if not needed then we can avoid this password update
                this.updateUserPassword(userId, hashedPassword);
                String businessUUID = this.retrieveBusinessUUIDByID(businessId);
                // update status and pwd in keycloak
                logger.info("calling idm to update user password");
                Map response = idmService.updateUser(Constants.USER_CONFIG_ACTIVE_STATUS, userUUID, businessUUID);
                logger.info("idm update user response {}", new JSONObject(response).toString());

//                idmService.resetUserPassword(userUUID, randPwd, businessUUID); //TODO is this needed?

                //get business configurations
                Map mailParam = businessConfigurationsService.getBusinessConfigMailParams(businessUUID);
                mailParam.put("toEmail", kingUserEmail);
                mailParam.put("recipientName", recipientName);
                String language = Constants.LANGUAGE_EN;
                logger.info("dtapp base {}", dtpDomain);
                String host = dtpDomain.contains("localhost") ? "http://localhost:4200" : "https://"+ dtpDomain;
                logger.info("host {}",host);
                String randomToken = this.saveResetPasswordToken(userUUID, true);
                //String link = host + "/" + language + "/"+Constants.SAVEPASSWORD+"/"+ randomToken;
                String sso = businessConfigurationsService.getSSOConfigByBusinessId(businessId);
                String link = host + "/" + language + (Utils.emptyString(sso) ? "/savepassword/" + randomToken : "");
                logger.info("link {}",link);
                mailParam.put("link", link);
                mailParam.put("userName", kingUserEmail);
                mailParam.put("password", password);
                mailParam.put("buttonVerbiage", Utils.notEmptyString(sso)? Utils.getWelcomeButtonVerbiage(sso) : Constants.SET_PASSWORD);//KING_BUTTON_VERBIAGE);
                //mailParam.put("role", Constants.KING_ROLE);
                mailParam.put("role", Constants.SUPER_ADMIN_ROLE);
                mailParam.put("kickstartMessage", Utils.getWelcomeMsg(sso));

                String encodedPDF = pdfService.generateSettingsAttachmentPDF(businessUUID);
                if(Utils.notEmptyString(encodedPDF)){
                    mailParam.put("attachment", encodedPDF);
                    mailParam.put("attachmentType", "pdf");
                    mailParam.put("attachmentName", businessUUID+".pdf");
                }

                notificationService.sendPasswordUpdateMail(mailParam, businessId, false);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * Store useruuid and token and time stamp for reset password
     *
     * @param userUUID
     * @return
     */
    public String saveResetPasswordToken(String userUUID, boolean welcomeMail) {
        String updatedToken = "";
        try {
            String randomToken = tokenUtility.generateUUID();
            int expiryThreshold = welcomeMail ? Utils.WELCOME_EMAIL_EXPIRY : Utils.RESET_PASS_EXPIRY;
            //insert
            this.createResetPassword(userUUID, randomToken, expiryThreshold);
            updatedToken = randomToken;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return updatedToken;
    }

    /**
     * Insert reset password entry
     *
     * @param userUUID
     * @param randomToken
     * @return
     */
    public int createResetPassword(String userUUID, String randomToken, int expiryThreshold) {
        int result = 0;
        try {
            //insert resetpassword token
            String expiryTime = Utils.addMinutesToDate(expiryThreshold);
            String resetPassSql = "insert into reset_password (user_uuid, token, expiry_time) values (?, ?, ?)";
            result = jdbcTemplate.update(resetPassSql, new Object[]{userUUID, randomToken, expiryTime});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * method to create active feedbacks present in temp_data_feedback_process table
     */
    public void createExcelFeedbacks() {
        logger.info("starts creating active feedbacks");
        String selectSql = "SELECT a.id as feedbackId, a.data, a.header, a.metadata_excel_index, a.question_data, a.question_uuid, a.question_excel_index, a.upload_id, a.timestamp, a.survey_uuid, a.business_uuid, a.active, a.status,\n" +
                "b.id, upload_metadata, upload_questions, excel_headers\n" +
                "FROM temp_data_feedback_process a, temp_data_feedback_upload b\n" +
                "WHERE a.upload_id = b.id AND a.active = '1' AND a.status IS NULL AND a.id > -1 and current_timestamp > a.timestamp LIMIT 100";
        //allowing only feedbacks having timestamp less than current time
        List activeFeedbacks = jdbcTemplate.queryForList(selectSql);
        Map surveyIdDataMap = new HashMap();
        if (Utils.isNotNull(activeFeedbacks)) {
            Iterator iterator = activeFeedbacks.iterator();
            while (iterator.hasNext()) {
                try {
                    Map eachResponseMap = (Map) iterator.next();
                    int feedbackId = eachResponseMap.containsKey(Constants.FEEDBACK_ID) && Utils.isNotNull(eachResponseMap.get(Constants.FEEDBACK_ID)) ? Integer.parseInt(eachResponseMap.get(Constants.FEEDBACK_ID).toString()) : -1;
                    logger.info("feedback id {} ", feedbackId);
                    String surveyUUID = eachResponseMap.containsKey(Constants.SURVEY_UUID) && Utils.isNotNull(eachResponseMap.get(Constants.SURVEY_UUID)) ? (eachResponseMap.get(Constants.SURVEY_UUID).toString()) : Constants.EMPTY;
                    String businessUUID = eachResponseMap.containsKey(Constants.BUSINESS_UUID) && Utils.isNotNull(eachResponseMap.get(Constants.BUSINESS_UUID)) ? (eachResponseMap.get(Constants.BUSINESS_UUID).toString()) : Constants.EMPTY;
                    String createdTime = eachResponseMap.containsKey(Constants.TIMESTAMP) && Utils.isNotNull(eachResponseMap.get(Constants.TIMESTAMP)) ? (eachResponseMap.get(Constants.TIMESTAMP).toString()) : Constants.EMPTY;

                    //CSUP-1367
                    //In order to avoid multiple DB calls to get questionData for same survey multiple times.
                    if(!surveyIdDataMap.containsKey(surveyUUID)) {
                        List surveyQuestionData = this.getSurveyQuestionDataBySurveyId(surveyUUID, businessUUID);
                        if(surveyQuestionData.size() > 0)
                            surveyIdDataMap.put(surveyUUID, surveyQuestionData);
                    }

                    Map eventPayload = this.getEventPayloadByFeedbackId(eachResponseMap, surveyIdDataMap);
                    //proper payload
                    if(Utils.isNotNull(eventPayload) && eventPayload.size() > 0 && Utils.notEmptyString(surveyUUID) && Utils.notEmptyString(businessUUID)){
                        eventPayload.put("programId", surveyUUID);
                        eventPayload.put("companyId", businessUUID);
                        eventPayload.put("createdTime", createdTime);
                        eventPayload.put("timezone", "America/Los_Angeles");
                        logger.info("eventPayload   "+new JSONObject(eventPayload));
                        Map respMap = this.callEventRestApi(eventPayload, dtEventUrl);
                        if(respMap.containsKey("success") && (boolean) respMap.get("success")){
                            //deleting the entry in temp_data_feedback_process table after successful event creation
                            this.updateFeedbackStatus("completed", feedbackId);
                        }else {
                            //updating the entry to failed state by feedbackId
                            this.updateFeedbackStatus("failed", feedbackId);
                        }
                    }else {
                        //updating the entry to failed state by feedbackId
                        this.updateFeedbackStatus("failed", feedbackId);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    logger.error(e.getMessage());
                }
            }
        }
        logger.info("End creating active feedbacks");
    }


    /**
     * method to create active feedbacks present in temp_data_feedback_process table
     */
    public Map getEventPayloadByFeedbackId(Map eachResponseMap, Map surveyIdDataMap) {
        Map eventPayloadMap = new LinkedHashMap();
        logger.info("starts getEventPayloadByFeedbackId");
        try {
            if (Utils.isNotNull(eachResponseMap)) {
                List headerValues = new LinkedList();
                List metadataIndexes = new LinkedList();

                List questionUUIDs = new LinkedList();
                List questionIndexes = new LinkedList();

                Map eventMetaDataMap = new LinkedHashMap();
                List questionDataList = new LinkedList();

                Map uploadMetadata = eachResponseMap.containsKey(Constants.UPLOAD_METADATA) && Utils.isNotNull(eachResponseMap.get(Constants.UPLOAD_METADATA)) ? mapper.readValue(eachResponseMap.get(Constants.UPLOAD_METADATA).toString(), LinkedHashMap.class) : new LinkedHashMap();
                Map uploadQuestions = eachResponseMap.containsKey(Constants.UPLOAD_QUESTIONS) && Utils.isNotNull(eachResponseMap.get(Constants.UPLOAD_QUESTIONS)) ? mapper.readValue(eachResponseMap.get(Constants.UPLOAD_QUESTIONS).toString(), LinkedHashMap.class) : new LinkedHashMap();

                String surveyUUID = eachResponseMap.containsKey(Constants.SURVEY_UUID) && Utils.isNotNull(eachResponseMap.get(Constants.SURVEY_UUID)) ? (eachResponseMap.get(Constants.SURVEY_UUID).toString()) : Constants.EMPTY;
                String businessUUID = eachResponseMap.containsKey(Constants.BUSINESS_UUID) && Utils.isNotNull(eachResponseMap.get(Constants.BUSINESS_UUID)) ? (eachResponseMap.get(Constants.BUSINESS_UUID).toString()) : Constants.EMPTY;

                List surveyQuestionData = new ArrayList();
                if(surveyIdDataMap.containsKey(surveyUUID)) {
                    surveyQuestionData = (List) surveyIdDataMap.get(surveyUUID);
                }else{
                    surveyQuestionData = this.getSurveyQuestionDataBySurveyId(surveyUUID, businessUUID);
                }

                //data --> uploaded metadata data, header --> uploaded metadata headers, metadataExcelIndex --> uploaded response metadata header excel indexes
                List data = eachResponseMap.containsKey(Constants.DATA) && Utils.isNotNull(eachResponseMap.get(Constants.DATA)) ? mapper.readValue(eachResponseMap.get(Constants.DATA).toString(), LinkedList.class) : new LinkedList();
                List header = eachResponseMap.containsKey(Constants.HEADER) && Utils.isNotNull(eachResponseMap.get(Constants.HEADER)) ? mapper.readValue(eachResponseMap.get(Constants.HEADER).toString(), LinkedList.class) : new LinkedList();
                List metadataExcelIndex = eachResponseMap.containsKey(Constants.METADATA_EXCEL_INDEX) && Utils.isNotNull(eachResponseMap.get(Constants.METADATA_EXCEL_INDEX)) ? mapper.readValue(eachResponseMap.get(Constants.METADATA_EXCEL_INDEX).toString(), LinkedList.class) : new LinkedList();

                //questionData --> uploaded question data, header --> uploaded question uuid, metadataExcelIndex --> uploaded response questiion title excel indexes
                List questionData = eachResponseMap.containsKey(Constants.QUESTION_DATA) && Utils.isNotNull(eachResponseMap.get(Constants.QUESTION_DATA)) ? mapper.readValue(eachResponseMap.get(Constants.QUESTION_DATA).toString(), LinkedList.class) : new LinkedList();
                List questionUUID = eachResponseMap.containsKey(Constants.QUESTION_UUID) && Utils.isNotNull(eachResponseMap.get(Constants.QUESTION_UUID)) ? mapper.readValue(eachResponseMap.get(Constants.QUESTION_UUID).toString(), LinkedList.class) : new LinkedList();
                List questionExcelIndex = eachResponseMap.containsKey(Constants.QUESTION_EXCEL_INDEX) && Utils.isNotNull(eachResponseMap.get(Constants.QUESTION_EXCEL_INDEX)) ? mapper.readValue(eachResponseMap.get(Constants.QUESTION_EXCEL_INDEX).toString(), LinkedList.class) : new LinkedList();

                //mappedHeader contain mapppng of excelIndex with metadata header
                // mappedHeader --> key : excelIndex, value : metadata header
                Map mappedHeader = uploadMetadata.containsKey("mappedHeader") && Utils.isNotNull(uploadMetadata.get("mappedHeader")) ? (LinkedHashMap) uploadMetadata.get("mappedHeader") : new LinkedHashMap();
                Map uploadedHeaders = uploadMetadata.containsKey("header") && Utils.isNotNull(uploadMetadata.get("header")) ? (LinkedHashMap) uploadMetadata.get("header") : new LinkedHashMap();
                if(mappedHeader.size() > 0) {
                    metadataIndexes = new LinkedList(mappedHeader.keySet());
                    headerValues = new LinkedList(mappedHeader.values());
                }
                //If no mapping headers found append uploaded headers
                else {
                    metadataIndexes = new LinkedList(uploadedHeaders.keySet());
                    headerValues = new LinkedList(uploadedHeaders.values());
                }

                if(headerValues.size() > 0 && metadataIndexes.size() > 0){
                    for(int i=0; i<headerValues.size(); i++){
                     String eachHeader = headerValues.get(i).toString();
                     int excelIndex = Integer.parseInt(metadataIndexes.get(i).toString());
                     int dataIndex = metadataExcelIndex.contains(excelIndex) ? metadataExcelIndex.indexOf(excelIndex) : -1;
                     String eachData = dataIndex> -1 && data.size() > dataIndex ? data.get(dataIndex).toString() : "";
                     if(Utils.notEmptyString(eachData)){
                         eventMetaDataMap.put(eachHeader, eachData);
                     }
                    }
                }
                logger.info("eventMetaDataMap   "+eventMetaDataMap);

                Map mappedQuestions = uploadQuestions.containsKey("mappedQuestions") && Utils.isNotNull(uploadQuestions.get("mappedQuestions")) ? (LinkedHashMap) uploadQuestions.get("mappedQuestions") : new LinkedHashMap();
                Map uploadedQuestions = uploadQuestions.containsKey("questionUUID") && Utils.isNotNull(uploadQuestions.get("questionUUID")) ? (LinkedHashMap) uploadQuestions.get("questionUUID") : new LinkedHashMap();
                Map questionUUIDTypeMap = uploadQuestions.containsKey("questionType") && Utils.isNotNull(uploadQuestions.get("questionType")) ? (LinkedHashMap) uploadQuestions.get("questionType") : new LinkedHashMap();
                if(mappedQuestions.size() > 0) {
                    questionIndexes = new LinkedList(mappedQuestions.keySet());
                    questionUUIDs = new LinkedList(mappedQuestions.values());
                }
                //If no mapping questions found append uploaded headers
                else {
                    questionIndexes = new LinkedList(uploadedQuestions.keySet());
                    questionUUIDs = new LinkedList(uploadedQuestions.values());
                }

                if(questionUUIDs.size() > 0 && questionIndexes.size() > 0){
                    for(int k=0; k<questionUUIDs.size(); k++){
                        Map eachQuestionEventMap = new LinkedHashMap();
                        String eachQuestionUUID = questionUUIDs.get(k).toString();
                        int excelIndex = Integer.parseInt(questionIndexes.get(k).toString());
                        int questionDataIndex = questionExcelIndex.contains(excelIndex) ? questionExcelIndex.indexOf(excelIndex) : -1;
                        String eachData = questionDataIndex> -1 && questionData.size() >= questionDataIndex ? questionData.get(questionDataIndex).toString() : "";
                        //DTV-8473, adding questionType
                        String questionType = questionUUIDTypeMap.containsKey(eachQuestionUUID) && Utils.isNotNull(questionUUIDTypeMap.get(eachQuestionUUID)) ? questionUUIDTypeMap.get(eachQuestionUUID).toString() : "";

                        //DTV-8473
                        eachQuestionEventMap.put("dataType", questionType);
                        eachQuestionEventMap.put("dataId", eachQuestionUUID);
                        Boolean otherFlag = false;

                        switch(questionType) {
                            case "rating":
                            case "nps":
                            case "ratingSlider":
                            case "pictureChoice"://without other option for pictureChoice
                            case "singleChoice":
                            case "dropdown":
                            case "poll":
                                if(Utils.isNotNull(eachData) && Utils.isStringNumber(eachData.toString()) && eachData.matches("\\d+")){
                                    eachQuestionEventMap.put("dataValue", Collections.singletonList(Integer.parseInt(eachData.toString())));
                                } else {
                                    otherFlag = eachData.length() > 0;
                                    //if question is unanwered then dataValue will be empty.
                                    eachQuestionEventMap.put("dataValue", otherFlag ? Collections.singletonList(eachData) :
                                            "poll".equalsIgnoreCase(questionType) ? Collections.singletonList("-1") : Collections.singletonList(-1));
                                }
                                eachQuestionEventMap.put("otherFlag", otherFlag);
                            break;

                            case "multiChoice":
                            case "ranking":
                            case "matrixRating":
                            case "multipleOpenEnded":
                                /**
                                 * open/ multiple openended only text
                                 *
                                 * open ended check --> from excel sheet
                                 * [open ended check] --> to event prc
                                 * moe1,moe2,moe3 --> from excel sheet
                                 * [moe1,moe2,moe3] --> to event prc
                                 *
                                 *
                                 * Matrix Rating
                                 * 1,2,3 --> from excel sheet
                                 * [1,2,3] --> to event prc
                                 *
                                 * MCQ
                                 * convert this string to list of int & string
                                 * 1,2,3,other MCQ option --> from excel sheet
                                 * [1,2,3,"other MCQ option"] --> to event prc
                                 *
                                 * RANKING
                                 * 2,1,0,N/A --> from excel sheet ranking question
                                 * [2,1,0,"N/A"] --> to event prc
                                 *
                                 */
                                List finalResponses = new ArrayList();
                                String[] eachDataArr = eachData.split(",");
                                //csup-1367 If question is unanswered then have to replace with default values.
                                if (eachDataArr.length == 1 && eachDataArr[0].isEmpty()) {
                                    finalResponses = this.getDefaultValuesForQuestionByQuestionId(questionType, eachQuestionUUID, surveyQuestionData);
                                }
                                else {
                                    for (int i = 0; i < eachDataArr.length; i++) {
                                        if (Utils.isNotNull(eachDataArr[i]) && Utils.isStringNumber(eachDataArr[i].toString()) && eachDataArr[i].matches("\\d+")) {
                                            finalResponses.add(Integer.parseInt(eachDataArr[i].toString()));
                                        } else {
                                            //for ranking questions only "N/A" is allowed
                                            otherFlag = questionType.equals("multiChoice") ? true : false;
                                            finalResponses.add(eachDataArr[i]);
                                        }
                                    }
                                }
                                eachQuestionEventMap.put("dataValue", finalResponses);
                                eachQuestionEventMap.put("otherFlag", otherFlag);
                            break;

                            case "matrixChoice":
                                /**
                                 * [1,2,3],[2,1],[3] --> from excel sheet
                                 * [[1,2,3],[2,1],[3]] --> to event prc
                                 */
                                if(Utils.notEmptyString(eachData)) {
                                    List finalResponsesMatrix = new ArrayList();
                                    try {
                                        StringBuilder sb = new StringBuilder("[").append(eachData).append("]");
                                        finalResponsesMatrix = mapper.readValue(sb.toString(), ArrayList.class);
                                        eachQuestionEventMap.put("dataValue", finalResponsesMatrix);
                                    } catch (JsonProcessingException e) {
                                        eachQuestionEventMap.put("dataValue", Collections.singletonList(eachData));
                                    }
                                }
                                //csup-1367 If question is unanswered then have to replace with default values.
                                else {
                                    eachQuestionEventMap.put("dataValue", this.getDefaultValuesForQuestionByQuestionId(questionType, eachQuestionUUID, surveyQuestionData));
                                }
                                eachQuestionEventMap.put("otherFlag", otherFlag);
                            break;

                            case "open":
                            default:
                                eachQuestionEventMap.put("dataValue", Collections.singletonList(eachData));
                                eachQuestionEventMap.put("otherFlag", otherFlag);
                            break;

                        }

                        questionDataList.add(eachQuestionEventMap);

                        /*//to support single choice, multi option responses with other choice label and ranking questions
                        if(eachData.startsWith("[")) {
                            Boolean otherFlag = false;
                            List finalResponses = new ArrayList();

                            //Example values : [1,2,3,other option label]
                            List values = mapper.readValue(eachData, ArrayList.class);
                            List valuesDup = mapper.readValue(eachData, ArrayList.class);

                            //Example dataValues : [1,2,3]
                            for(int i=0; i<values.size(); i++){
                                if((String.valueOf(values.get(i))).matches("\\d+") || (String.valueOf(values.get(i))).equalsIgnoreCase("N/A")) {
                                    finalResponses.add(String.valueOf(values.get(i)));
                                    valuesDup.remove(values.get(i));
                                }
                            }

                            //Example valuesDup : ["other option label"]
                            //Example finalResponses : [1,2,3,"other option label"]
                            if(valuesDup.size() > 0){
                                otherFlag = true;
                                finalResponses.add(String.valueOf(valuesDup.get(0)));
                            }
                            eachQuestionEventMap.put("dataValue", finalResponses);
                            eachQuestionEventMap.put("otherFlag", otherFlag);
                        }else {
                            //only numeric values --> rating, singleCHoice, multi choice, dropdown, nps, ratingslider
                            if(Utils.isNotNull(eachData) && Utils.isStringNumber(eachData.toString()) && eachData.matches("\\d+")){
                                eachQuestionEventMap.put("dataValue", new ArrayList(Collections.singleton(Integer.parseInt(eachData.toString()))));
                            }else {
                                eachQuestionEventMap.put("dataValue", new ArrayList(Collections.singleton(eachData)));
                            }
                        }*/

                    }
                }

                //to contain atleast one valid response
                if(questionDataList.size() > 0){
                    eventPayloadMap.put("data", questionDataList);
                    eventPayloadMap.put("metaData", eventMetaDataMap);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            logger.error(e.getMessage());
            return null;
        }
        logger.info("End getEventPayloadByFeedbackId");
        return eventPayloadMap;
    }

    private List getSurveyQuestionDataBySurveyId(String surveyUUID, String businessUUID) {
        List surveyQuestionData = new ArrayList();
        try{
            String sql = String.format("select multi_questions from surveys_%s where survey_uuid = ?", businessUUID.replaceAll("-", "_"));
            List surveyData = jdbcTemplate.queryForList(sql, new Object[]{surveyUUID}, new int[]{Types.VARCHAR});
            if(Utils.isNotNull(surveyData) && surveyData.size() > 0){
                Map surveyMap = (Map) surveyData.get(0);
                List<List<Map<String, Object>>> multiQuestions = surveyMap.containsKey("multi_questions") && Utils.isNotNull(surveyMap.get("multi_questions")) ? (List<List<Map<String, Object>>>) mapper.readValue(surveyMap.get("multi_questions").toString(), ArrayList.class) : new ArrayList();
                if(multiQuestions.size() > 0){
                    surveyQuestionData = multiQuestions.get(0);
                }
            }
        }catch (Exception e){
            logger.error("Error in getSurveyQuestionDataBySurveyId() : ", e.getMessage());
        }
        return surveyQuestionData;
    }

    /**
     * This method is return default values for unanswered questions -> multiChoice, ranking, matrixRating, multipleOpenEnded, matrixChoice
     * "rating", "nps" ,"ratingSlider", "pictureChoice", "singleChoice", "dropdown", "poll" -> default values is -1 (covered during processing)
     * @param questionType
     * @return
     */
    private List getDefaultValuesForQuestionByQuestionId(String questionType, String questionUUID, List<Map<String, Object>> surveyQuestionData) {
        List result = new ArrayList();
        try{
            if("multiChoice".equalsIgnoreCase(questionType))
                result = Collections.singletonList(-1);
            else {
                surveyQuestionData.stream()
                        .filter(eachQuestion -> eachQuestion.containsKey("questionId") &&
                                eachQuestion.get("questionId").toString().equalsIgnoreCase(questionUUID))
                        .findFirst()
                        .map(eachQuestion -> {
                            List dataValueList = new ArrayList();

                            if (questionType.equalsIgnoreCase("ranking")) {
                                List<String> options = eachQuestion.containsKey("options") && Utils.isNotNull(eachQuestion.get("options")) ? (List<String>) eachQuestion.get("options") : new ArrayList<>();
                                dataValueList = IntStream.range(0, options.size()).boxed().collect(Collectors.toList());
                            }
                            else if (questionType.equalsIgnoreCase("matrixRating")) {
                                List<String> subQuestionIds = eachQuestion.containsKey("questionIds") && Utils.isNotNull(eachQuestion.get("questionIds")) ? (List<String>) eachQuestion.get("questionIds") : new ArrayList<>();
                                dataValueList = Collections.nCopies(subQuestionIds.size(), -1);
                            }
                            else if (questionType.equalsIgnoreCase("multipleOpenEnded")) {
                                List<String> subQuestionIds = eachQuestion.containsKey("questionIds") && Utils.isNotNull(eachQuestion.get("questionIds")) ? (List<String>) eachQuestion.get("questionIds") : new ArrayList<>();
                                dataValueList = Collections.nCopies(subQuestionIds.size(), "");
                            }
                            else if (questionType.equalsIgnoreCase("matrixChoice")) {
                                List<String> subQuestionIds = eachQuestion.containsKey("questionIds") && Utils.isNotNull(eachQuestion.get("questionIds")) ? (List<String>) eachQuestion.get("questionIds") : new ArrayList<>();
                                dataValueList = Collections.nCopies(subQuestionIds.size(), Collections.singletonList(-1));
                            }
                            return dataValueList;
                        })
                        .ifPresent(result::addAll);
            }

        }catch (Exception e){
            logger.error("Error in getDefaultValuesForQuestionByQuestionId() : ", e.getMessage());
        }
        return result;
    }

    /**
     * method to call event rest api
     * @param inputMap
     * @return
     */
    public  Map callEventRestApi(Map inputMap, String eventUrl){
        Map respMap = new HashMap();
        JSONObject json = new JSONObject(inputMap);
        String inputJson = json.toString();
        logger.info(inputJson);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);

        try {
            String uri = eventUrl;
            logger.info("uri{}",uri);
//            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
            ResponseEntity<Object> responseEntity = restTemplate.exchange(uri+"create", HttpMethod.POST, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info(responseEntity.getBody().toString());
                    respMap = (Map) responseEntity.getBody();
                    logger.info("respMap  "+respMap);
                }
            }

        }
        catch (ResourceAccessException | HttpClientErrorException ex){
            logger.error("event service refused to connect");
            respMap.put("success",false);
            respMap.put("message", "Connection refused");
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return  respMap;
    }

    /**
     * If feedback status is completed , deleting that entry
     * If feedback status is failed, updatethat enty to fail
     *
     * @param status
     * @param feedbackId
     * @return
     */
    public int updateFeedbackStatus(String status, int feedbackId) {
        int result = 0;
        try {
            if(status.equalsIgnoreCase("completed")) {
                //deleting the successfull event responses
                String updateSql = "delete from temp_data_feedback_process where id = ?";
                int updateResult = jdbcTemplate.update(updateSql, new Object[]{feedbackId});
            }else {
                //updating status to failed for that feedbackId
                String updateSql = "update temp_data_feedback_process set status = ? where id = ?";
                int updateResult = jdbcTemplate.update(updateSql, new Object[]{status, feedbackId});
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * method to fetch customized `from email` settings of a business
     *
     * @param businessId
     * @return
     */
    public Map getBusinessEmailSettings(int businessId) {
        Map resultMap = new HashMap();
        try {
            //query to fetch `from email` settings of a business
            String getFromEmailSettingSql = "SELECT business_email, trigger_email, alert_email, notification_email FROM email_settings WHERE business_id = ?";
            resultMap = jdbcTemplate.queryForMap(getFromEmailSettingSql, new Object[]{businessId});

            //sanitize result map
            if(resultMap.size() > 0) {
                Map businessEmail = mapper.readValue(resultMap.get("business_email").toString(), HashMap.class);
                resultMap.remove("business_email");
                resultMap.put("business_email", businessEmail);

                Map triggerEmail = mapper.readValue(resultMap.get("trigger_email").toString(), HashMap.class);
                resultMap.remove("trigger_email");
                resultMap.put("trigger_email", triggerEmail);

                Map alertEmail = mapper.readValue(resultMap.get("alert_email").toString(), HashMap.class);
                resultMap.remove("alert_email");
                resultMap.put("alert_email", alertEmail);

                Map notificationEmail = mapper.readValue(resultMap.get("notification_email").toString(), HashMap.class);
                resultMap.remove("notification_email");
                resultMap.put("notification_email", notificationEmail);
            }
        } catch (Exception e) {
            logger.info("No existing customized `from email` settings found for business {} ", businessId);
            //logger.error("exception at customizeFromEmailForBusiness(). Msg {}", e.getMessage());
        }
        return resultMap;
    }

    public void extentTTLForShortURLAndSMS() {
       // 1. get expired surveys from survey_schedule table
        String currentDateTime = Utils.getCurrentUTCDateAsString();
        String surveyScheduleSQl = "SELECT * FROM survey_schedule WHERE (JSON_CONTAINS(channels, '[\"link\"]') = 1 OR JSON_CONTAINS(channels, '[\"whatsapp\"]') = 1 OR JSON_CONTAINS(channels, '[\"sms\"]') = 1)" +
                " AND survey_expiration = 'expired' AND to_date >= ? ORDER BY created_time desc;";
        try {
            List<Map<String, Object>> surveyScheduleData = jdbcTemplate.queryForList(surveyScheduleSQl, new Object[]{currentDateTime});

            if (!surveyScheduleData.isEmpty()) {

                for (Map<String, Object> surveySchedule : surveyScheduleData) {

                    String channels = Utils.isNotNull(surveySchedule.get("channels")) ? surveySchedule.get("channels").toString() : "";
                    List<String> channelList = (Utils.notEmptyString(channels)) ? mapper.readValue(channels, ArrayList.class) : new ArrayList();
                    if (channelList.contains("link")) {
                        extendTTLForShortURL(surveySchedule, currentDateTime);
                    }
                    if (channelList.contains("sms")) {
                        extendTTLForSMS(surveySchedule, currentDateTime, "sms");
                    }
                    if(channelList.contains("whatsapp")) {
                        extendTTLForSMS(surveySchedule, currentDateTime, "whatsapp");
                    }
                }

            }
        } catch (Exception e) {
            logger.error("Error in extentTTLForShortURLAndSMS() : " + e);
        }

    }

    /**
     *
     * @param surveySchedule
     * @param currentDateTime
     */
    private void extendTTLForShortURL(Map<String, Object> surveySchedule, String currentDateTime) {

        boolean shortUrlEnabled = false;
        Map<String, Object> surveyData = new HashMap<>();
        String baseUrl =  dt360UrlRead ;
        String yaasUri = dt360Url;
        try {
            int businessId = (int) surveySchedule.get("business_id");
            int surveyId = (int) surveySchedule.get("survey_id");
            String host = surveySchedule.get("host").toString();
            String businessUUID = this.retrieveBusinessUUIDByID(businessId);
            List businessConfigurations = businessConfigurationsService.getBusinessConfigurationsByBusinessId(businessUUID);
            if (businessConfigurations.size() > 0) {
                Map businessConfigurationMap = (Map) businessConfigurations.get(0);
                shortUrlEnabled = (Integer.parseInt(businessConfigurationMap.get("bitly").toString()) == 1) ? true : false;
            }

            if (shortUrlEnabled) {

                String surveySql = " SELECT * FROM surveys_" + businessUUID.replaceAll("-", "_") +
                        " WHERE state = 3 AND active = 1 AND to_date >= ? AND id = ?";
                List<Map<String, Object>> surveyDetails = jdbcTemplate.queryForList(surveySql, new Object[] {currentDateTime, surveyId});
                if (surveyDetails.size() > 0) {
                    surveyData = (Map) surveyDetails.get(0);

                    //Get static token from static_token table.
                    String staticTokenSql = "select static_token_id from static_token_" + businessUUID.replaceAll("-","_") +
                            " where meta_data_enabled = 0 and survey_id = ? ";
                    String token = jdbcTemplate.queryForObject(staticTokenSql, new Object[] {surveyId}, String.class);
                    //To do, If static_token_id is null / empty we need to generate new token
                    String tmpString = getTokenTempString("DT");
                    token = token + "~~" + tokenUtility.encodeBusinessID(businessUUID) + "$" + tmpString;

                    List languages = mapper.readValue(surveyData.get("languages").toString(), ArrayList.class);
                    String language = (languages.size() > 0) ? languages.get(0).toString() : "en";
                    String staticUrl = Utils.isNotNull(surveyData.get("static_url")) ? surveyData.get("static_url").toString() : null;
                    String surveyEndDate = surveyData.get("to_date").toString();
                    String ttl = Utils.computeTTL(surveyEndDate);

                    //SE-1160 Fetching EUX_URL from application.prop
                    host = host.contains("localhost") ? "https://test.dropthought.com" : "https://" + euxDomain; //computeHostnameEUX(host);
                    language = (Utils.isNull(language) || Utils.emptyString(language) || language.equalsIgnoreCase("null")) ? "en" : language;
                    String long_url = host + "/" + language + "/dt/survey/" + token;
                    logger.info("long_url{}", long_url);

                    //if the url is not present generate the url - Unique Link
                    if (Utils.isNull(staticUrl) || Utils.emptyString(staticUrl)) {
                        String shortUrl = long_url;
                        //Update the shortened url in surveys_{buuid} table
                        shortUrl = getShortUrlByLongUrl(long_url, ttl, businessUUID);
                        if (Utils.notEmptyString(shortUrl) && !shortUrl.equalsIgnoreCase(long_url)) {
                            String updateStaticUrlSQL = "update surveys_%s set static_url = ? where  id = ?";
                            String updateUrl = String.format(updateStaticUrlSQL, businessUUID.replace("-","_"));
                            int result = jdbcTemplate.update(updateUrl, new Object[]{shortUrl, surveyId});
                        }
                    } else {
                        if (Utils.notEmptyString(token) && Utils.notEmptyString(ttl)) {
                            //begin update
//                            String customUrlMask = this.getUrlMaskByBusiness(businessUUID);
                            String uniqeTokenString = staticUrl.substring(baseUrl.length());
//                            uniqeTokenString = Utils.notEmptyString(customUrlMask) ? customUrlMask + "" + uniqeTokenString : uniqeTokenString

                            //input map
                            Map shortUrlMap = new HashMap();
                            shortUrlMap.put("longUrl", long_url);
                            shortUrlMap.put("shortUrl", uniqeTokenString);
                            shortUrlMap.put("ttl", ttl);
                            List shortUrlList = new ArrayList();
                            shortUrlList.add(shortUrlMap);

                            // Call Yass Get API to verify whether the token exists or not
                            HttpHeaders headers = new HttpHeaders();
                            headers.setContentType(MediaType.APPLICATION_JSON);
                            HttpEntity<String> entity = new HttpEntity<String>(headers);
                            Map responseMap = new HashMap();
                            try {
                                String uri = yaasUri + "/" + (uniqeTokenString);
                                ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, entity, Object.class);
                                if (responseEntity != null) {
                                    if (responseEntity.getBody() != null) {
                                        logger.info("Short URL request has been sent successfully!");
                                        logger.debug(responseEntity.getBody().toString());
                                        responseMap = (Map) responseEntity.getBody();
                                    }
                                }
                            } catch (HttpClientErrorException e) {
                                logger.error("403 forbidden!");
                            }

                            if (responseMap.containsKey("Error")) {
                                callShortUrlService(shortUrlList, HttpMethod.POST, yaasUri);
                            } else {
                                callShortUrlService(shortUrlList, HttpMethod.PUT, yaasUri);
                            }
                            //end update
                        }
                    }
                    //Update short url in static_token table for multiple links
                    String staticTokenSQL = "SELECT * from static_token_" + businessUUID.replaceAll("-", "_") +
                            " where meta_data_enabled = 1 and survey_id = ?";
                    List<Map<String, Object>> staticTokenData = jdbcTemplate.queryForList(staticTokenSQL, new Object[] {surveyId});

                    List shortUrlRequestList = new ArrayList();
                    boolean updateStaticTokenTable = false;
                    for (Map<String, Object> staticTokenRecord : staticTokenData) {

                        String staticTokenId = staticTokenRecord.get("static_token_id").toString();
                        String linkId = staticTokenRecord.get("link_id").toString();
                        String expiredShortUrl = staticTokenRecord.get("short_url").toString();
                        String uniqueTokenString = expiredShortUrl.substring(baseUrl.length());
                        updateStaticTokenTable = Utils.emptyString(uniqueTokenString) ? true : false;
                        uniqueTokenString = Utils.emptyString(uniqueTokenString) ? RandomStringUtils.randomAlphanumeric(10) : uniqueTokenString;
                        //input map
                        if(Utils.notEmptyString(ttl)) {
                            Map shortUrlMap = new HashMap();
                            shortUrlMap.put("longUrl", long_url);
                            shortUrlMap.put("shortUrl", uniqueTokenString);
                            shortUrlMap.put("ttl", ttl);
                            shortUrlRequestList.add(shortUrlMap);

                            if (updateStaticTokenTable) {//This is for old data which was failed due to fixTTL migration issue.
                                String staticTokenUpdateSql = "update static_token_" + businessUUID.replaceAll("-", "_") +
                                        " set short_url = ? where meta_data_enabled = 1 and survey_id = ? and static_token_id = ? and link_id = ? ";
                                jdbcTemplate.update(staticTokenUpdateSql, new Object[]{baseUrl + (uniqueTokenString), surveyId, staticTokenId, linkId});
                            }
                        }
                    }

                    if (shortUrlRequestList.size() > 0 && updateStaticTokenTable) {
                        callShortUrlService(shortUrlRequestList, HttpMethod.POST, yaasUri);
                    } else if (shortUrlRequestList.size() > 0) {
                        List response = callShortUrlService(shortUrlRequestList, HttpMethod.PUT, yaasUri);
                    }

                } else {
                    logger.info("survey is not present");
                }

            } else {
                logger.info("short url is not enabled");
            }
        } catch (Exception e) {
            logger.error("Error in extendTTLForShortURL() : " + e);
        }
    }

    /**
     *
     * @param surveySchedule
     * @param currentDateTime
     */
    private void extendTTLForSMS(Map<String, Object> surveySchedule, String currentDateTime, String source) {

        Map<String, Object> surveyData = new HashMap<>();
        String baseUrl =  dt360UrlRead ;
        String yaasUri = dt360Url;
        try {
            int businessId = (int) surveySchedule.get("business_id");
            int surveyId = (int) surveySchedule.get("survey_id");
            String host = surveySchedule.get("host").toString();
            String businessUUID = this.retrieveBusinessUUIDByID(businessId);

            String surveySql = " SELECT * FROM surveys_" + businessUUID.replaceAll("-", "_") +
                    " WHERE state = 3 AND active = 1 AND to_date >= ? AND id = ?";
            List<Map<String, Object>> surveyDetails = jdbcTemplate.queryForList(surveySql, new Object[] {currentDateTime, surveyId});
            if (surveyDetails.size() > 0) {
                surveyData = (Map) surveyDetails.get(0);

                //Get static token from static_token table.
                String staticTokenSql = "select static_token_id from static_token_" + businessUUID.replaceAll("-","_") +
                        " where meta_data_enabled = 0 and survey_id = ? ";
                String token = jdbcTemplate.queryForObject(staticTokenSql, new Object[] {surveyId}, String.class);
                //To do, If static_token_id is null / empty we need to generate new token
                String tmpString = getTokenTempString("DT");
                token = token + "~~" + tokenUtility.encodeBusinessID(businessUUID) + "$" + tmpString;

                List languages = mapper.readValue(surveyData.get("languages").toString(), ArrayList.class);
                String language = (languages.size() > 0) ? languages.get(0).toString() : "en";
                String staticUrl = Utils.isNotNull(surveyData.get("static_url")) ? surveyData.get("static_url").toString() : null;
                String surveyEndDate = surveyData.get("to_date").toString();
                String ttl = Utils.computeTTL(surveyEndDate);

                //SE-1160 Fetching EUX_URL from application.prop
                host = host.contains("localhost") ? "https://test.dropthought.com" : "https://" + euxDomain; //computeHostnameEUX(host);
                language = (Utils.isNull(language) || Utils.emptyString(language) || language.equalsIgnoreCase("null")) ? "en" : language;
                String long_url = host + "/" + language + "/dt/survey/" + token;
                logger.info("long_url{}", long_url);

                //if the url is not present generate the url - Unique Link
                if (Utils.isNull(staticUrl) || Utils.emptyString(staticUrl)) {
                    String shortUrl = long_url;
                    //Update the shortened url in surveys_{buuid} table
                    shortUrl = getShortUrlByLongUrl(long_url, ttl, businessUUID);
                    if (Utils.notEmptyString(shortUrl) && !shortUrl.equalsIgnoreCase(long_url)) {
                        String updateStaticUrlSQL = "update surveys_%s set static_url = ? where  id = ?";
                        String updateUrl = String.format(updateStaticUrlSQL, businessUUID.replace("-","_"));
                        int result = jdbcTemplate.update(updateUrl, new Object[]{shortUrl, surveyId});
                    }
                } else {
                    if (Utils.notEmptyString(token) && Utils.notEmptyString(ttl)) {
                        //begin update
                        String uniqeTokenString = staticUrl.substring(baseUrl.length());
                        //input map
                        Map shortUrlMap = new HashMap();
                        shortUrlMap.put("longUrl", long_url);
                        shortUrlMap.put("shortUrl", uniqeTokenString);
                        shortUrlMap.put("ttl", ttl);
                        List shortUrlList = new ArrayList();
                        shortUrlList.add(shortUrlMap);

                        // Call Yass Get API to verify whether the token exists or not
                        HttpHeaders headers = new HttpHeaders();
                        headers.setContentType(MediaType.APPLICATION_JSON);
                        HttpEntity<String> entity = new HttpEntity<String>(headers);
                        Map responseMap = new HashMap();
                        try {

                            String uri = yaasUri + "/" +(uniqeTokenString);
                            ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, entity, Object.class);
                            if (responseEntity != null) {
                                if (responseEntity.getBody() != null) {
                                    logger.info("Short URL request has been sent successfully!");
                                    logger.debug(responseEntity.getBody().toString());
                                    responseMap = (Map) responseEntity.getBody();
                                }
                            }
                        } catch (HttpClientErrorException e) {
                            logger.error("403 forbidden!");
                        }

                        if (responseMap.containsKey("Error")) {
                            callShortUrlService(shortUrlList, HttpMethod.POST, yaasUri);
                        } else {
                            callShortUrlService(shortUrlList, HttpMethod.PUT, yaasUri);
                        }
                        //end update
                    }
                }
                //Update ttl for dynamic token ids in token table those are not taking survey via sms/whatsapp
                String tokenSQL = "SELECT dynamic_token_id from token_" + businessUUID.replaceAll("-", "_") +
                        " where survey_id = ? AND last_action != 'completed'";
                List<Map<String, Object>> tokenData = jdbcTemplate.queryForList(tokenSQL, new Object[] {surveyId});

                List shortUrlRequestList = new ArrayList();
                for (Map<String, Object> tokenRecord : tokenData) {

                    String long_urlForSMS = "";
                    String dynamicTokenId = tokenRecord.get("dynamic_token_id").toString();
                    long_urlForSMS = dynamicTokenId + "~~" + tokenUtility.encodeBusinessID(businessUUID) + "$" + tmpString;
                    String sourceString = source.equals("sms") ? "?source=sms" : "?source=whatsapp";
                    long_urlForSMS = host + "/" + language + "/dt/survey/" + long_urlForSMS + sourceString;
                    logger.info("long_urlForSMS{}", long_urlForSMS);
                    if(Utils.notEmptyString(ttl)) {
                        //input map
                        Map shortUrlMap = new HashMap();
                        shortUrlMap.put("longUrl", long_urlForSMS);
                        shortUrlMap.put("shortUrl", dynamicTokenId);
                        shortUrlMap.put("ttl", ttl);
                        shortUrlRequestList.add(shortUrlMap);
                    }
                }

                if (shortUrlRequestList.size() > 0) {
                    callShortUrlService(shortUrlRequestList, HttpMethod.PUT, yaasUri);
                }
            } else {
                logger.info("survey is not present");
            }
        } catch (Exception e) {
            logger.error("Error in extendTTLForSMS() : " + e);
        }
    }

    /**
     * method to get tempString for each business
     * @param cc
     * @return
     */
    public String getTokenTempString(String cc){
        String tmpString = null;
        int equalCount = 0;
        try {
            tmpString = DatatypeConverter.printBase64Binary(cc.getBytes("UTF-8"));
            if (cc.length() > 0) {
                if (cc.length() == 1 || cc.length() == 4) {
                    equalCount = 2;
                }
                if (cc.length() == 2 || cc.length() == 5) {
                    equalCount = 1;
                }
            }
            if (equalCount == 1) {
                tmpString = tmpString.substring(0, tmpString.length() - 1);
                tmpString = tmpString + equalCount;
            }
            if (equalCount == 2) {
                tmpString = tmpString.substring(0, tmpString.length() - 2);
                tmpString = tmpString + equalCount;
            }
            if (equalCount == 0) {
                tmpString = tmpString + equalCount;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return tmpString;
    }

    /**
     * host name to resolve to EUX application
     * @param hostname
     * @return
     */
    public String computeHostnameEUX(String hostname){
        String resolvedHostName = "";
        try{
            if(hostname.contains("test")){
                resolvedHostName = "https://test-eux.dropthought.com";
            }else if(hostname.contains("stage")){
                resolvedHostName = "https://stage-eux.dropthought.com";
            }else if(hostname.contains("automation")){
                resolvedHostName = "https://automation-eux.dropthought.com";
            }else if(hostname.contains("sandbox")){
                resolvedHostName = "https://sandbox-eux.dropthought.com";
            }else{
                resolvedHostName = "https://eux.dropthought.com";;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return resolvedHostName;
    }

    /**
     * Method to get complete short url
     *
     * @return
     */
    public String getShortUrlByLongUrl(String longUrl, String ttl, String businessUniqueId){
        logger.info("get short url by long url");
        String shortUrl = "";
        try{
            if(Utils.notEmptyString(longUrl) && Utils.notEmptyString(ttl)) {
                boolean stringExists = false;
                Map resultMap = new HashMap();
                List response = new ArrayList();
                //get base url for shorturl
                String baseUrl = dt360UrlRead;
                while (!stringExists) {
                    List shortUrList = new ArrayList();
                    Map shortUrlMap = this.formShortUrlRequestList(longUrl, ttl, businessUniqueId);
                    shortUrList.add(shortUrlMap);
                    response = this.callShortUrlService(shortUrList, HttpMethod.POST, baseUrl);
                    logger.info("result Map {} ", response);
                    if (response.contains("status")) {
                        /**
                         * if already exists: "status" : "custom short url already exists in the system"
                         * will be returned in the response
                         */
                        stringExists = false;
                    } else {
                        /**
                         * random string exists. success response.
                         */
                        stringExists = true;
                    }
                }
                shortUrl = resultMap.containsKey("shortUrl") && Utils.notEmptyString(resultMap.get("shortUrl").toString()) ?
                        baseUrl + (resultMap.get("shortUrl").toString()) : longUrl;
            }
        }catch (Exception e){
            logger.error("Error in getShortUrlByLongUrl() : " + e);
        }
        return shortUrl;
    }

    /**
     * Expire short links
     */
    public void expireShortLinks() {

        String surveyScheduleSQl = "SELECT * FROM survey_schedule WHERE JSON_CONTAINS(channels, '[\"sms\"]') = 1" +
                " and type = 'journey' and enabled = '1' ORDER BY created_time desc;";
        try {
            List<Map<String, Object>> surveyScheduleData = jdbcTemplate.queryForList(surveyScheduleSQl);

            if (!surveyScheduleData.isEmpty()) {

                for (Map<String, Object> surveySchedule : surveyScheduleData) {

                    int businessId = (int) surveySchedule.get("business_id");
                    int surveyId = (int) surveySchedule.get("survey_id");
                    String businessUUID = this.retrieveBusinessUUIDByID(businessId);

                    String surveySql = " SELECT id FROM surveys_" + businessUUID.replaceAll("-", "_") +
                            " WHERE state = 3 AND active = 1 AND id = ?";
                    List<Map<String, Object>> surveyDetails = jdbcTemplate.queryForList(surveySql, new Object[] {surveyId});
                    if (surveyDetails.size() > 0) {

                        String channels = Utils.isNotNull(surveySchedule.get("channels")) ? surveySchedule.get("channels").toString() : "";
                        List<String> channelList = (Utils.notEmptyString(channels)) ? mapper.readValue(channels, ArrayList.class) : new ArrayList();

                        /*if (channelList.contains("link")) {
                            expireTTLForShortURL(surveySchedule, businessUUID, surveyId);
                        }*/
                        if (channelList.contains("sms")) {
                            expireTTLForSMS(surveySchedule, businessUUID, surveyId);
                        }
                    } else {
                        logger.info("survey not found");
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error in expireShortLinks() : " + e);
        }
    }

    /**
     *
     * @param surveySchedule
     * @param businessUUID
     * @param surveyId
     */
    private void expireTTLForSMS(Map<String, Object> surveySchedule, String businessUUID, int surveyId) {

        try {
            String type = surveySchedule.get("type").toString();
            String kickoff = surveySchedule.get("kickoff").toString();
            String reminder_1Date = Utils.isNotNull(surveySchedule.get("reminder_1")) ? surveySchedule.get("reminder_1").toString() : null;
            String reminder_2Date = Utils.isNotNull(surveySchedule.get("reminder_2")) ? surveySchedule.get("reminder_2").toString() : null;
            kickoff = Utils.isNotNull(reminder_2Date) ? reminder_2Date : Utils.isNotNull(reminder_1Date) ? reminder_1Date : kickoff;
            Map after_kickoff  = Utils.isNotNull(surveySchedule.get("after_kickoff")) ?
                    mapper.readValue(surveySchedule.get("after_kickoff").toString(), HashMap.class) : new HashMap();
            String language = Utils.isNotNull(surveySchedule.get("language")) ?
                    surveySchedule.get("language").toString() : "en";
            String host = surveySchedule.get("host").toString();
            String currentDate = Utils.getCurrentUTCDateAsString();
            String ttl = null;
            String ttlDate = null;
            String expiryDate = null;
            String metaDataHeader = null;
            boolean isJourneyCaseHavingKickoffConfig = false;
            int expiryInterval = 10;
            String expiryIntervalType = "days";
            int  expireOnTakingSurvey = 0;
          //  if (type.equals("journey")) {

                if (!after_kickoff.isEmpty()) {
                    isJourneyCaseHavingKickoffConfig = true;
                    expiryInterval = after_kickoff.containsKey("expiryInterval") && Utils.isNotNull(after_kickoff.get("expiryInterval")) ?
                            (int) after_kickoff.get("expiryInterval") : 10;
                    expiryIntervalType = after_kickoff.containsKey("expiryIntervalType") && Utils.isNotNull(after_kickoff.get("expiryIntervalType")) ?
                            after_kickoff.get("expiryIntervalType").toString() : "days";
                    expireOnTakingSurvey = after_kickoff.containsKey("expireOnTakingSurvey") && Utils.isNotNull(after_kickoff.get("expireOnTakingSurvey")) ?
                            (int) after_kickoff.get("expireOnTakingSurvey") : 0;
                    metaDataHeader = Utils.isNotNull(after_kickoff.get("metaDataHeader")) ?
                            after_kickoff.get("metaDataHeader").toString() : null;
                    String timezone = Utils.isNotNull(after_kickoff.get("timezone")) ?
                            after_kickoff.get("timezone").toString() : null;
                    String kickoffTime = Utils.isNotNull(after_kickoff.get("kickOffTime")) ?
                            after_kickoff.get("kickOffTime").toString() : null;
                } else {
                    ttlDate = kickoff;
                    expiryDate = Utils.addDaysToDate(Utils.convertStringToDate(kickoff), 10);
                }
           /* } else {
                ttlDate = kickoff;
                expiryDate = Utils.addDaysToDate(Utils.convertStringToDate(kickoff), 10);
            }*/

            if (expireOnTakingSurvey == 0) {


                String tmpString = getTokenTempString("DT");
                //SE-1160 Fetching EUX_URL from application.prop
                host = host.contains("localhost") ? "https://test.dropthought.com" : "https://" + euxDomain; //Utils.computeHostnameEUX(host);
                language = (Utils.isNull(language) || Utils.emptyString(language) || language.equalsIgnoreCase("null")) ? "en" : language;
                //Expire short url in token table for multiple links
                String tokenSQL = "SELECT dynamic_token_id, phone, metadata_time from token_" + businessUUID.replaceAll("-", "_") +
                        " where survey_id = ? ";
                List<Map<String, Object>> tokenData = jdbcTemplate.queryForList(tokenSQL, new Object[]{surveyId});

                List shortUrlRequestList = new ArrayList();
                for (Map<String, Object> staticTokenRecord : tokenData) {

                    String long_urlForSMS = "";
                    String tokenId = staticTokenRecord.get("dynamic_token_id").toString();
                    long_urlForSMS = tokenId + "~~" + tokenUtility.encodeBusinessID(businessUUID) + "$" + tmpString;
                    long_urlForSMS = host + "/" + language + "/dt/survey/" + long_urlForSMS + "?source=sms";
                    if (isJourneyCaseHavingKickoffConfig) {

                        String metaDataTime = Utils.isNotNull(staticTokenRecord.get("phone")) && Utils.isNotNull(staticTokenRecord.get("metadata_time")) ?
                                staticTokenRecord.get("metadata_time").toString() : null;
                        ttlDate = Utils.isNotNull(metaDataTime) ? metaDataTime : kickoff;
                        if (expiryIntervalType.equals("minutes")) {
                            expiryDate = Utils.addMinutesToDate(Utils.convertStringToDate(ttlDate), expiryInterval);
                        } else if (expiryIntervalType.equals("hours")) {
                            expiryDate = Utils.addHoursToDate(Utils.convertStringToDate(ttlDate), expiryInterval);
                        } else if (expiryIntervalType.equals("days")) {
                            expiryDate = Utils.addDaysToDate(Utils.convertStringToDate(ttlDate), expiryInterval);
                        } else if (expiryIntervalType.equals("weeks")) {
                            expiryDate = Utils.addWeeksToDate(Utils.convertStringToDate(ttlDate), expiryInterval);
                        } else if (expiryIntervalType.equals("months")) {
                            expiryDate = Utils.addMonthsToDate(Utils.convertStringToDate(ttlDate), expiryInterval);
                        }
                    }
                    if (Utils.compareDates(currentDate, expiryDate)) {
                        ttl = "1m";
                    } else {
                        ttl = Utils.compareDates(expiryDate, currentDate) ? Utils.computeTTL(currentDate, expiryDate) : Utils.computeTTL(ttlDate, expiryDate);
                    }
                    //input map
                    Map shortUrlMap = new HashMap();
                    shortUrlMap.put("longUrl", long_urlForSMS);
                    shortUrlMap.put("shortUrl", tokenId);
                    shortUrlMap.put("ttl", ttl);
                    shortUrlRequestList.add(shortUrlMap);
                }
                if (shortUrlRequestList.size() > 0) {
                    callShortUrlService(shortUrlRequestList, HttpMethod.POST, dt360Url);
                }
            }
        } catch (Exception e) {
            logger.error("Error in expireTTLForSMS() : " + e);
        }
    }

    /**
     *
     * @param surveySchedule
     * @param businessUUID
     * @param surveyId
     */
    private void expireTTLForShortURL(Map<String, Object> surveySchedule, String businessUUID, int surveyId) {

        try {
            String type = surveySchedule.get("type").toString();
            String kickoff = surveySchedule.get("kickoff").toString();
            String reminder_1Date = Utils.isNotNull(surveySchedule.get("reminder_1")) ? surveySchedule.get("reminder_1").toString() : null;
            String reminder_2Date = Utils.isNotNull(surveySchedule.get("reminder_2")) ? surveySchedule.get("reminder_2").toString() : null;
            kickoff = Utils.isNotNull(reminder_2Date) ? reminder_2Date : Utils.isNotNull(reminder_1Date) ? reminder_1Date : kickoff;
            String kickoffStatus = Utils.isNotNull(surveySchedule.get("kickoff_status")) ?
                    surveySchedule.get("kickoff_status").toString() : null;
            String host = surveySchedule.get("host").toString();
            Map after_kickoff = Utils.isNotNull(surveySchedule.get("after_kickoff")) ?
                    mapper.readValue(surveySchedule.get("after_kickoff").toString(), HashMap.class) : new HashMap();
            String language = Utils.isNotNull(surveySchedule.get("language")) ?
                    surveySchedule.get("language").toString() : "en";
            String currentDate = Utils.getCurrentUTCDateAsString();
            String ttl = null;
            String ttlDate = null;
            String expiryDate = null;
            String metaDataHeader = null;
            boolean isJourneyCase = false;
            int expiryInterval = 10;
            String expiryIntervalType = "days";
            Integer expireOnTakingSurvey = 0;
            if (type.equals("journey")) {

                if (!after_kickoff.isEmpty()) {
                    isJourneyCase = true;
                    expiryInterval = after_kickoff.containsKey("expiryInterval") && Utils.isNotNull(after_kickoff.get("expiryInterval")) ?
                            (int) after_kickoff.get("expiryInterval") : 10;
                    expiryIntervalType = after_kickoff.containsKey("expiryIntervalType") && Utils.isNotNull(after_kickoff.get("expiryIntervalType")) ?
                            after_kickoff.get("expiryIntervalType").toString() : "days";
                    expireOnTakingSurvey = after_kickoff.containsKey("expireOnTakingSurvey") && Utils.isNotNull(after_kickoff.get("expireOnTakingSurvey")) ?
                            (int) after_kickoff.get("expireOnTakingSurvey") : 0;
                    metaDataHeader = Utils.isNotNull(after_kickoff.get("metaDataHeader")) ?
                            after_kickoff.get("metaDataHeader").toString() : null;
                    String timezone = Utils.isNotNull(after_kickoff.get("timezone")) ?
                            after_kickoff.get("timezone").toString() : null;
                    String kickoffTime = Utils.isNotNull(after_kickoff.get("kickOffTime")) ?
                            after_kickoff.get("kickOffTime").toString() : null;
                } else {
                    ttlDate = kickoff;
                    expiryDate = Utils.addDaysToDate(Utils.convertStringToDate(kickoff), 10);
                }
            } else {
                ttlDate = kickoff;
                expiryDate = Utils.addDaysToDate(Utils.convertStringToDate(kickoff), 10);
            }
            //Unique link will be enabled up to the survey enddate.
            String staticTokenSql = "select static_token_id from static_token_" + businessUUID.replaceAll("-", "_") +
                    " where meta_data_enabled = 0 and survey_id = ?  ORDER BY id DESC LIMIT 1 ";
            String token = jdbcTemplate.queryForObject(staticTokenSql, new Object[]{surveyId}, String.class);
            //If static_token_id is null / empty then we can skip the expire logic

            String tmpString = getTokenTempString("DT");
            token = token + "~~" + tokenUtility.encodeBusinessID(businessUUID) + "$" + tmpString;
            //SE-1160 Fetching EUX_URL from application.prop
            host = host.contains("localhost") ? "https://test.dropthought.com" : "https://" + euxDomain; //Utils.computeHostnameEUX(host);
            language = (Utils.isNull(language) || Utils.emptyString(language) || language.equalsIgnoreCase("null")) ? "en" : language;
            String long_url = host + "/" + language + "/dt/survey/" + token;
            //Expire short url in static_token table for multiple links
            String staticTokenSQL = "SELECT meta_data, short_url from static_token_" + businessUUID.replaceAll("-", "_") +
                    " where meta_data_enabled = 1 and survey_id = ?";
            List<Map<String, Object>> staticTokenData = jdbcTemplate.queryForList(staticTokenSQL, new Object[]{surveyId});

            List shortUrlRequestList = new ArrayList();
            for (Map<String, Object> staticTokenRecord : staticTokenData) {

                //String staticTokenId = staticTokenRecord.get("static_token_id").toString();
                //String linkId = staticTokenRecord.get("link_id").toString();
                String expiredShortUrl = staticTokenRecord.get("short_url").toString();
                if (isJourneyCase) {

                    Map metaData = Utils.isNotNull(staticTokenRecord.get("meta_data")) ?
                            mapper.readValue(staticTokenRecord.get("meta_data").toString(), HashMap.class) : new HashMap();
                    String metaDataHeaderValue = Utils.isNotNull(metaData.get(metaDataHeader)) ? metaData.get(metaDataHeader).toString() : null;
                    ttlDate = Utils.isNotNull(metaDataHeaderValue) ? metaDataHeaderValue : kickoff;
                    if (expiryIntervalType.equals("hours")) {
                        expiryDate = Utils.addHoursToDate(Utils.convertStringToDate(ttlDate), expiryInterval);
                    } else if (expiryIntervalType.equals("days")) {
                        expiryDate = Utils.addDaysToDate(Utils.convertStringToDate(ttlDate), expiryInterval);
                    } else if (expiryIntervalType.equals("weeks")) {
                        expiryDate = Utils.addWeeksToDate(Utils.convertStringToDate(ttlDate), expiryInterval);
                    } else if (expiryIntervalType.equals("months")) {
                        expiryDate = Utils.addMonthsToDate(Utils.convertStringToDate(ttlDate), expiryInterval);
                    }
                }
                if (Utils.compareDates(currentDate, expiryDate)) {
                    ttl = "1m";
                } else {
                    ttl = Utils.compareDates(expiryDate, currentDate) ? Utils.computeTTL(currentDate, expiryDate) : Utils.computeTTL(ttlDate, expiryDate);
                }
                String uniqueTokenString = expiredShortUrl.substring(dt360UrlRead.length());
                if (Utils.notEmptyString(uniqueTokenString)) {

                    //input map
                    Map shortUrlMap = new HashMap();
                    shortUrlMap.put("longUrl", long_url);
                    shortUrlMap.put("shortUrl", uniqueTokenString);
                    shortUrlMap.put("ttl", ttl);
                    shortUrlRequestList.add(shortUrlMap);
                }
            }
            if (shortUrlRequestList.size() > 0) {
                callShortUrlService(shortUrlRequestList, HttpMethod.POST, dt360Url);
            }
        } catch (Exception e) {
            logger.error("Error in expireTTLForShortURL() : " + e);
        }
    }

    @Transactional
    public void insertDynamicQrFromTempToMainTable() {
        logger.info("begin insert dynamic qr from temp to main table");
        List<QrCodeSurveyTokenBean> qrCodeUpdateList = new ArrayList<>();
        /*String ttl = "240h";
        List shortUrlRequestList = new ArrayList();
        String shortUrlDomain = null;
        String shortUrlAPIKey = null;
        String shortUrlAPILink = null; */
        try {
            //commented as of now. will be used after upgrating bitly
            /*if (shortUrlType.equals("BITLY")) {
                shortUrlDomain = domain;
                shortUrlAPIKey = bitlykey;
                shortUrlAPILink = bitlyAPI;
            } else {
                shortUrlAPILink = dt360UrlRead;
            }
            ShortUrlService shortUrlService = factory.createShortUrlService(shortUrlType);*/
            String selectSql = "select * from generated_dynamicqrcode_link order by created_time desc";
            List resultLit = jdbcTemplate.queryForList(selectSql);
            Iterator itr = resultLit.iterator();
            int businessId = 0;
            Set<Integer> businessIdsSet = new HashSet<>();
            Set<Integer> surveyIdsSet = new HashSet<>();
            Set<Integer> tempTableIds = new HashSet<>();
            Set<String> longUrlSet = new HashSet<>();
            String businessUUID = null;
            //iterate each record
            String shortUrl = "";
            while (itr.hasNext()) {
                try {
                    Map eachMap = (Map) itr.next();
                    int id = Integer.parseInt(eachMap.get("id").toString());
                    int width = Integer.parseInt(eachMap.get("width").toString());
                    int height = Integer.parseInt(eachMap.get("height").toString());
                    String static_token_id = eachMap.get("static_token_id").toString();
                    businessId = Integer.parseInt(eachMap.get("business_id").toString());
                    if (!businessIdsSet.contains(businessId)) {

                        businessUUID = retrieveBusinessUUIDByID(businessId);
                        businessIdsSet.add(businessId);
                    }

                    String qr_file_type = eachMap.get("qr_file_type").toString();
                    String tag_link_id = eachMap.get("tag_link_id").toString();
                    int qrcode_survey_id = Integer.parseInt(eachMap.get("survey_id").toString());
                    String link_group_id = eachMap.get("link_group_id").toString();
                    String link_id = eachMap.get("link_id").toString();
                    //String language = eachMap.get("languages").toString();
                    logger.debug("qrfiletype = " + qr_file_type);
                    logger.debug("statictokenid = " + static_token_id);
                    logger.debug("qrcodesurveyid = " + qrcode_survey_id);
                    logger.debug("linkgroupid = " + link_group_id);
                    logger.debug("linkid = " + link_id);
                    logger.debug("taglinkid = " + tag_link_id);
                    logger.debug("businessid = " + businessId);
                    String longUrl = constructLongUrl(businessUUID, static_token_id, "en", "DT", "qr");
                    /*if (!longUrlSet.contains(longUrl)) {
                        longUrlSet.add(longUrl);
                        shortUrl = shortUrlService.generateShortUrl(longUrl, shortUrlDomain, shortUrlAPIKey, shortUrlAPILink);
                    }*/
                    if (!surveyIdsSet.contains(qrcode_survey_id) && qrcode_survey_id != 0) {

                        surveyIdsSet.add(qrcode_survey_id);
                        /*Map surveyMap = returnProgramByID(qrcode_survey_id, businessUUID);
                        ttl = Utils.computeTTL(surveyMap.get("to_date").toString());*/
                    }

                    CustomizeQRCodeBean bean = qrCustomizationService.getTagLinkQRCustomizationByTagId(tag_link_id, businessUUID);

                    // byte[] qrCode = getQRCodeImage(shortUrl, qr_file_type, width, height);
                    byte[] qrCode = null;
                    if(bean != null) //customization enabled links
                        qrCode = generateQrCode(businessUUID,"en", width, height,
                                qr_file_type, static_token_id, bean.getForeColor(), bean.getBackColor(), bean.getLogoUrl());
                    else
                        qrCode = generateQrCode(businessUUID,"en", width, height,
                                qr_file_type, static_token_id, "");

                    /*if (shortUrlType.equals("YASS")) {
                        shortUrlRequestList.add(shortUrlService.constructPayload(longUrl, ttl));
                    }*/
                    QrCodeSurveyTokenBean eachQrCodeBean = new QrCodeSurveyTokenBean();
                    eachQrCodeBean.setQrCodeSurveyId(qrcode_survey_id);
                    eachQrCodeBean.setStaticTokenId(static_token_id);
                    eachQrCodeBean.setQrFileType(qr_file_type);
                    eachQrCodeBean.setLinkGroupId(link_group_id);
                    eachQrCodeBean.setLinkId(link_id);
                    eachQrCodeBean.setTagLinkId(tag_link_id);
                    //eachQrCodeBean.setShortUrl(shortUrl);
                    eachQrCodeBean.setSurveyQrCode(qrCode);
                    eachQrCodeBean.setCreatedBy(Utils.isNotNull(eachMap.get("created_by")) && Utils.notEmptyString(eachMap.get("created_by").toString()) ?
                            Integer.parseInt(eachMap.get("created_by").toString()) : 0);
                    eachQrCodeBean.setCreatedTime(Utils.isNotNull(eachMap.get("created_time")) && Utils.notEmptyString(eachMap.get("created_time").toString()) ?
                            Timestamp.valueOf(eachMap.get("created_time").toString()) : new Timestamp(System.currentTimeMillis()));
                    eachQrCodeBean.setMetadata(Utils.isNotNull(eachMap.get("meta_data")) && Utils.notEmptyString(eachMap.get("meta_data").toString()) ?
                            mapper.readValue(eachMap.get("meta_data").toString(), HashMap.class) : new HashMap());
                    eachQrCodeBean.setMetadataEnabled(Utils.isNotNull(eachMap.get("meta_data_enabled")) && Utils.notEmptyString(eachMap.get("meta_data_enabled").toString()) ?
                            Integer.parseInt(eachMap.get("meta_data_enabled").toString()) : 0);
                    eachQrCodeBean.setMetadataType(Utils.isNotNull(eachMap.get("meta_data_type")) && Utils.notEmptyString(eachMap.get("meta_data_type").toString()) ?
                            mapper.readValue(eachMap.get("meta_data_type").toString(), HashMap.class) : new HashMap());
                    eachQrCodeBean.setDownloadCount(Utils.isNotNull(eachMap.get("download_count")) ? eachMap.get("download_count").toString() : "0");

                    qrCodeUpdateList.add(eachQrCodeBean);
                    tempTableIds.add(id);

                    /*if (Utils.isNotNull(qrCodeUrl)) {
                        logger.info("update the qrcode in the dynamic QRcode table");
                        String updateSql = "update dynamic_qrcode_" + businessUUID.replaceAll("-", "_") + " set dynamicqr_code = ? where dynamicqr_filetype = ? and tag_link_id = ? and dynamicqr_tokenid = ? and dynamicqr_surveyid = ? and link_group_id = ? and link_id = ?";
                        int updateResult = jdbcTemplate.update(updateSql, new Object[]{qrCodeUrl, qr_file_type, tag_link_id, static_token_id, qrcode_survey_id, link_group_id, link_id});
                    }*/

                    /*String deleteSql = "delete from generated_dynamicqrcode_link where qr_file_type = ? and tag_link_id = ? " +
                            "and static_token_id = ? and qrcode_survey_id = ? and link_group_id = ? and link_id = ?";
                    jdbcTemplate.update(deleteSql, new Object[]{qr_file_type, tag_link_id, static_token_id, qrcode_survey_id, link_group_id, link_id});*/
                } catch (Exception e) {
                    logger.error("Error in insertDynamicQrFromTempToMainTable() : " + e.getMessage());
                }
            }
            /*if (Constants.YASS.equals(shortUrlType) && !shortUrlService.callShortUrlEndpoint(shortUrlRequestList, dt360Url)) {
                throw new Exception("Error in Short Url Service - " + shortUrlType);
            }*/
            logger.info("qrCodeUpdateList {} ", qrCodeUpdateList);
            updateDynamicQRCodeData(qrCodeUpdateList, businessUUID);
            List<QrCodeSurveyTokenBean> dynamicQRListWithSurveyMapped = qrCodeUpdateList.stream()
                    .filter(e -> e.getQrCodeSurveyId() != 0).collect(Collectors.toList());
            logger.info("dynamicQRListWithSurveyMapped {} ", dynamicQRListWithSurveyMapped);
            if (dynamicQRListWithSurveyMapped.size() > 0) {
                int insertedRowCount = insertDataToQRCodeTable(dynamicQRListWithSurveyMapped, businessUUID, true);
                if (insertedRowCount == dynamicQRListWithSurveyMapped.size()) {
                    logger.info("Dynamic qr codes successfully inserted into qr code table");
                }
            }

            //remove the entry once qrcode urls created
            if (tempTableIds.size() > 0) {
                String deleteSql = "delete from generated_dynamicqrcode_link where id in (:idList)";
                MapSqlParameterSource param = new MapSqlParameterSource().addValue("idList", tempTableIds);
                namedParameterJdbcTemplate.update(deleteSql, param);
            }
        } catch (Exception e) {
            logger.error("Error in insertDynamicQrFromTempToMainTable() : " + e);
        }
        logger.info("end insert dynamic qr from temp to main table");
    }

    /**
     * Method to update dynamic_qrcode_{buuid} table with qrcode and short url
     * @param qrCodeUpdateList
     * @param businessUUID
     */
    @Transactional
    private void updateDynamicQRCodeData(List<QrCodeSurveyTokenBean> qrCodeUpdateList, String businessUUID) {

        /*String updateSql = "update dynamic_qrcode_" + businessUUID.replaceAll("-", "_") +
                " set dynamicqr_code = ?, short_url = ?  where dynamicqr_filetype = ? and tag_link_id = ? and dynamicqr_tokenid = ? " +
                " and dynamicqr_surveyid = ? and link_group_id = ? and link_id = ?";*/
        if (!qrCodeUpdateList.isEmpty()) {

            try {
                String updateSql = "update dynamic_qrcode_" + businessUUID.replaceAll("-", "_") +
                        " set dynamicqr_code = ? where dynamicqr_filetype = ? and tag_link_id = ? and dynamicqr_tokenid = ? " +
                        " and link_group_id = ? and link_id = ? and dynamicqr_code IS NULL";
                int[][] updateCounts = jdbcTemplate.batchUpdate(updateSql, qrCodeUpdateList, 5,
                        new ParameterizedPreparedStatementSetter<QrCodeSurveyTokenBean>() {

                            public void setValues(PreparedStatement ps, QrCodeSurveyTokenBean qrcodeBean)
                                    throws SQLException {
                                ps.setBytes(1, qrcodeBean.getSurveyQrCode());
                                ps.setString(2, qrcodeBean.getQrFileType());
                                ps.setString(3, qrcodeBean.getTagLinkId());
                                ps.setString(4, qrcodeBean.getStaticTokenId());
                                ps.setString(5, qrcodeBean.getLinkGroupId());
                                ps.setString(6, qrcodeBean.getLinkId());
                                //ps.setInt(7, qrcodeBean.getQrCodeSurveyId());
                                //ps.setString(8, qrcodeBean.getShortUrl());
                            }
                        });
            } catch (Exception e) {
                logger.error("Error in updateDynamicQRCodeData() : " + e);
            }
        }
    }

    /**
     * Method to insert survey mapped dynamic qr codes to qr code table
     * @param qrCodeBeansList
     * @param businessUUID
     * @return
     */
    private int insertDataToQRCodeTable(final List qrCodeBeansList, String businessUUID, final boolean isDynamicQRList) {

        logger.debug("begin insert Data To QRCode Table");
        int result[] = null;
        try {
            final String table_name = "qrcode_" + businessUUID.replaceAll("-", "_");

            if (Utils.isNotNull(qrCodeBeansList)) {
                final String query = "insert into " + table_name + " (survey_qrcode, qrcode_survey_id, static_token_id, qr_file_type, created_by, created_time, meta_data_enabled, meta_data, tag_link_id, link_group_id, link_id, meta_data_type, short_url, dynamicqr_list) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

                result = jdbcTemplate.batchUpdate(query, new BatchPreparedStatementSetter() {
                    public void setValues(PreparedStatement statement, int i) throws SQLException {
                        final QrCodeSurveyTokenBean qrCodeSurveyBean1 = (QrCodeSurveyTokenBean) qrCodeBeansList.get(i);
                        statement.setBytes(1, qrCodeSurveyBean1.getSurveyQrCode());
                        statement.setInt(2, qrCodeSurveyBean1.getQrCodeSurveyId());
                        statement.setString(3, qrCodeSurveyBean1.getStaticTokenId());
                        statement.setString(4, qrCodeSurveyBean1.getQrFileType());
                        statement.setInt(5, qrCodeSurveyBean1.getCreatedBy());
                        statement.setTimestamp(6, qrCodeSurveyBean1.getCreatedTime());
                        statement.setInt(7, qrCodeSurveyBean1.getMetadataEnabled());
                        statement.setString(8, Utils.isNotNull(qrCodeSurveyBean1.getMetadata()) ? new JSONObject(qrCodeSurveyBean1.getMetadata()).toString() : new JSONObject().toString());
                        statement.setString(9, qrCodeSurveyBean1.getTagLinkId());
                        statement.setString(10, qrCodeSurveyBean1.getLinkGroupId());
                        statement.setString(11, qrCodeSurveyBean1.getLinkId());
                        statement.setString(12, Utils.isNotNull(qrCodeSurveyBean1.getMetadataType()) ? new JSONObject(qrCodeSurveyBean1.getMetadataType()).toString() : new JSONObject().toString());
                        statement.setString(13, Utils.isNotNull(qrCodeSurveyBean1.getShortUrl()) ? qrCodeSurveyBean1.getShortUrl() : null);
                        statement.setString(14, isDynamicQRList ? "1" : "0");
                    }

                    public int getBatchSize() {
                        return qrCodeBeansList.size();
                    }
                });

            }
            logger.debug("end insert Data To QRCode Table");
        }
        catch (Exception e) {
            logger.error("Error in insertDataToQRCodeTable() : " + e.getMessage());
        }

        return result.length;
    }

    /**
     * method to update qr code short url with url mask
     */
    public void updateQRShortUrl() {
        logger.info("starts updateQRShortUrl");
        try {
            //get list of generated short url links
            String selectSql = "select id, short_url, long_url, unique_token, business_id, survey_id, file_type, width, height, language " +
                    " from generated_qr_short_links order by created_time desc limit 10000";
            List resultLit = jdbcTemplate.queryForList(selectSql);
            boolean allRecordsSent = false;
            int endIndex =0;
            int startIndex = 0;
            int length = 20;
            while (!allRecordsSent) {
                endIndex = startIndex + length;
                if (endIndex >= resultLit.size()) {
                    endIndex = resultLit.size();
                    allRecordsSent = true;
                }
                List resulstSubList = resultLit.subList(startIndex, endIndex);
                Iterator<Map> itr = resulstSubList.iterator();
                List shortURlList = new ArrayList();
                List tokenList = new ArrayList();

                while (itr.hasNext()) {
                    try {
                        Map eachMap = itr.next();
                        String longUrl = eachMap.get("long_url").toString();
                        String token = eachMap.get("unique_token").toString();
                        String businessUniqueId = eachMap.get("business_id").toString();
                        String fileType = Utils.isNotNull(eachMap.get("file_type")) ? eachMap.get("file_type").toString() : "png";
                        String language = eachMap.get("language").toString();
                        int surveyId = (int) eachMap.get("survey_id");
                        int id = (int) eachMap.get("id");
                        /*int width = (int) eachMap.get("width");
                        int height = (int) eachMap.get("height");*/

                        Map surveyData = this.returnProgramByID(surveyId, businessUniqueId);
                        if (surveyData.containsKey("id")) {
                            String surveyEndDate = surveyData.get("to_date").toString();
                            String ttl = Utils.computeTTL(surveyEndDate);
                            if(Utils.notEmptyString(ttl)) {
                                Map shortUrlMap = this.formShortUrlRequestList(longUrl, ttl, businessUniqueId);
                                shortUrlMap.put("token", token); // add token and bid in the request to utilize later
                                shortUrlMap.put("businessUniqueId", businessUniqueId);
                                shortUrlMap.put("fileType", fileType);
                                shortUrlMap.put("language", language);
                                shortUrlMap.put("uniqueId", id);
                                shortURlList.add(shortUrlMap);
                            }
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        logger.error("exception at updateQRShortUrl. Msg {}", e.getMessage());
                    }
                }
                startIndex = startIndex + length;

                List responseList = Collections.emptyList();
                if(!shortURlList.isEmpty()) {
                    // call api to create the shorturl link
                    responseList = generateShortUrlV2(shortURlList);
                }
                //iterator the response
                Iterator itr1 = responseList.iterator();
                while(itr1.hasNext()) {
                    try {
                        Map eachResponse = (Map) itr1.next();
                        String bid = eachResponse.get("businessUniqueId").toString().replaceAll("-", "_");
                        String url = eachResponse.get("shortUrl").toString();
                        String token = eachResponse.get("token").toString();
                        String fileType = eachResponse.get("fileType").toString();
                        String language = eachResponse.get("language").toString();
                        int id = (int) eachResponse.get("uniqueId");
                        byte[] qrcode = generateQrCode(bid,language, 200, 200,
                                fileType, token, url);
                        //build dynamic query to update the static token table based on buisnessId
                        if (Utils.notEmptyString(url) && url.contains("https://")) { //check portocol is available in the shorturl to fix DTV-5463
                            logger.info("update the short url in the static token");
                            String updateSql= " update qrcode_" + bid.replaceAll("-", "_") + " set short_url = ?, survey_qrcode = ? where static_token_id = ? and qr_file_type = ? ";
                            int updateResult = jdbcTemplate.update(updateSql, new Object[]{url, qrcode, token, fileType});
                            tokenList.add(id);
                        }
                    }catch (Exception e){
                        logger.error("exception at  updateQRShortUrl in eachResponse loop.Msg {]", e.getMessage());
                    }
                }
                //remove the entry once short urls created
                if(tokenList.size() > 0) {
                    String deleteSql = "delete from generated_qr_short_links where id in (:tokens)";
                    MapSqlParameterSource param = new MapSqlParameterSource();
                    param.addValue("tokens", tokenList);
                    namedParameterJdbcTemplate.update(deleteSql, param);
                }
            }
        }catch (Exception e){
            logger.error("exception at updateQRShortUrl. Msg {}", e.getMessage());
            e.printStackTrace();
        }
        logger.info("End updateQRShortUrl");
    }

    /**
     * Function to check shorturl is enabled for business
     *
     * @param businessId
     * @return
     */

    public boolean isShortUrlEnabledForBusiness(String businessId) {
        boolean isEnabled = false;
        logger.info("begin isShortUrlEnabledForBusiness");
        try {
            String get_business_configurations_by_business_id = "select bitly from business_configurations where business_id = (select business_id from business where business_uuid=?)";
            List businessConfigList = jdbcTemplate.queryForList(get_business_configurations_by_business_id, new Object[]{businessId});
            if (businessConfigList != null && businessConfigList.size() > 0) {
                Map businessConfigMap = (Map) businessConfigList.get(0);
                isEnabled = businessConfigMap.containsKey("bitly") && Utils.isNotNull(businessConfigMap.get("bitly"))
                        && businessConfigMap.get("bitly").equals("1") ? true : false;
            }
        } catch (Exception e) {
            logger.error("Exception at isShortUrlEnabledForBusiness. Msg {} ", e.getMessage());
        }
        logger.info("end isShortUrlEnabledForBusiness");
        return isEnabled;
    }

    /**
     * Function to get business list which is going to expire in 3 days and it is in trial period
     *
     * @return
     */
    public List getBusinessListForPlanRenewal(int count, String interval) {

        List<Map<String, Object>> businessList = new ArrayList<>();

        try {
            //get all active business list whose plan is going to expire in 3 days
            String businessSql = "SELECT * FROM business WHERE state = '1' " +
                    "AND DATE(contract_end_date) = CURDATE() + INTERVAL " + count + " " + interval +
                    " AND plan_config_id = 3 and trial_status  = '1'";//DTV-12699
            businessList = jdbcTemplate.queryForList(businessSql);
        } catch (Exception e) {
            logger.error("Error in getPlanrenewalBusinessList() : " + e);
        }

        return businessList;
    }

    /**
     * Function to get business list which is going to expire in 7 days
     *
     * @return
     */
    public List getBusinessListForSubscriptionRenewal(int count, String interval) {

        List<Map<String, Object>> businessList = new ArrayList<>();

        try {
            //get all active business list whose plan is going to expire in 3 days
            String businessSql = "SELECT * FROM business WHERE state = '1' " +
                    "AND DATE(contract_end_date) = CURDATE() + INTERVAL " + count + " " + interval +
                    " AND plan_config_id = 3";//DTV-12744
            businessList = jdbcTemplate.queryForList(businessSql);
        } catch (Exception e) {
            logger.error("Error in getBusinessListForSubscriptionRenewal() : " + e);
        }

        return businessList;
    }

    /**
     * Function to get business global settings
     *
     * @param businessId
     * @return
     */
    public Map getBusinessGlobalSettings(int businessId) {

        Map<String, Object> businessGlobalSettingsMap = new HashMap<>();
        try {
            String sql = "SELECT * FROM business_global_settings WHERE business_id = ?";
            businessGlobalSettingsMap =  jdbcTemplate.queryForMap(sql, new Object[]{businessId});
        } catch (Exception e) {
            logger.error("Error in getBusinessGlobalSettings() : " + e);
        }

        return  businessGlobalSettingsMap;
    }


    /**
     * Function to get userUUID by business email
     * @param businessEmail
     * @return
     */
    public String getUserUUIDBYEmail(String businessEmail) {
        String userUUID = "";
        try {
            String sql = "SELECT user_uuid FROM user WHERE user_email = ?";
            userUUID =  jdbcTemplate.queryForObject(sql, new Object[]{businessEmail}, String.class);
        } catch (Exception e) {
            logger.error("Error in getUserUUIDBYEmail() : " + e);
        }

        return  userUUID;
    }

    /**
     * Method to sent welcome Email for self sign-up customers
     * @param businessId
     * @param confirmationStatus
     * @param userId
     * @param kingUserEmail
     * @param recipientName
     * @param userUUID
     */
    public void sendWelcomeMail(int businessId, int confirmationStatus, int userId, String kingUserEmail,
                                String recipientName, String userUUID, String businessUUID) {

        try {
            if (confirmationStatus == 0) {
                //update password for user to login
                String password = tokenUtility.generateRandomString(10);
                String randPwd = password;
                String hashedPassword = cryptoUtility.computeSHAHash(password);
                //update user confirmation status and password
                this.updateUserConfirmStatus(userId, Constants.USER_CONFIG_ACTIVE_STATUS);
                //todo : if not needed then we can avoid this password update
                this.updateUserPassword(userId, hashedPassword);
                // update status and pwd in keycloak
                logger.info("calling idm to update user password");
                Map response = idmService.updateUser(Constants.USER_CONFIG_ACTIVE_STATUS, userUUID, businessUUID);
                logger.info("idm update user response {}", new JSONObject(response).toString());

//                idmService.resetUserPassword(userUUID, randPwd, businessUUID); //TODO is this needed?

                //get business configurations
                Map mailParam = businessConfigurationsService.getBusinessConfigMailParams(businessUUID);
                mailParam.put("toEmail", kingUserEmail);
                mailParam.put("recipientName", recipientName);
                String language = Constants.LANGUAGE_EN;
                logger.info("dtapp base {}", dtpDomain);
                String host = dtpDomain.contains("localhost") ? "http://localhost:4200" : "https://"+ dtpDomain;
                logger.info("host {}",host);
                String randomToken = this.saveResetPasswordToken(userUUID, true);
//                String link = host + "/" + language + "/"+Constants.SAVEPASSWORD+"/"+ randomToken;
                String sso = businessConfigurationsService.getSSOConfigByBusinessId(businessId);
                String link = host + "/" + language + (Utils.emptyString(sso) ? "/savepassword/" + randomToken : "");
                logger.info("link {}",link);
                mailParam.put("link", link);
                mailParam.put("userName", kingUserEmail);
                mailParam.put("password", password);
                mailParam.put("buttonVerbiage", Utils.notEmptyString(sso) ? Utils.getWelcomeButtonVerbiage(sso) : Constants.SET_PASSWORD);//KING_BUTTON_VERBIAGE);
                //mailParam.put("role", Constants.KING_ROLE);
                mailParam.put("role", Constants.SUPER_ADMIN_ROLE);
                mailParam.put("kickstartMessage", Utils.getWelcomeMsg(sso));

                String encodedPDF = pdfService.generateSettingsAttachmentPDF(businessUUID);
                if(Utils.notEmptyString(encodedPDF)){
                    mailParam.put("attachment", encodedPDF);
                    mailParam.put("attachmentType", "pdf");
                    mailParam.put("attachmentName", businessUUID+".pdf");
                }

                notificationService.sendPasswordUpdateMail(mailParam, businessId, true);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * Function to send payment failure mail
     * @param eachBusinessId
     * @param kingUserEmail
     * @param recipientName
     * @param planName
     */
    public void sendPaymentFailureMail(int eachBusinessId, String kingUserEmail,
                                       String recipientName, String planName) {

        try {
            Map<String, Object> emailParams = new HashMap<>();
            emailParams.put("toEmail", kingUserEmail);
            emailParams.put("recipientName", recipientName);
            emailParams.put("planName", planName);
            String link = "";
            emailParams.put("link", link);

            notificationService.sendPaymentFailureMail(emailParams, eachBusinessId);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * Function to get api key by business id
     * @param eachBusinessId
     * @return
     */
    public String getAPIKeyByBusinessId(int eachBusinessId) {

        String apiKey = "";
        try {
            String sql = "SELECT api_key FROM api_key WHERE business_id = ? order by created_time desc limit 1";
            apiKey =  jdbcTemplate.queryForObject(sql, new Object[]{eachBusinessId}, String.class);
        } catch (Exception e) {
            logger.error("Error in getAPIKeyByBusinessId() : " + e);
        }

        return  apiKey;
    }

    /**
     * Function to get business by id
     * @param eachBusinessId
     * @return
     */
    public Map<String, Object> getBusinessById(int eachBusinessId) {

        Map<String, Object> businessMap = new HashMap<>();
        try {
            String sql = "SELECT * FROM business WHERE business_id = ?";
            businessMap =  jdbcTemplate.queryForMap(sql, new Object[]{eachBusinessId});
        } catch (Exception e) {
            logger.error("Error in getBusinessById() : " + e);
        }

        return  businessMap;
    }

    /**
     * Method to get business who is in trial period
     * @return
     */
    public List<Map<String, Object>> getBusinessListForTrialStatus() {

        List<Map<String, Object>> businessList = new ArrayList<>();
        try {
            String sql = "SELECT * FROM business WHERE trial_status = '1' AND plan_config_id = 3";
            businessList =  jdbcTemplate.queryForList(sql);
        } catch (Exception e) {
            logger.error("Error in getBusinessListForTrialStatus() : " + e);
        }

        return  businessList;
    }

    /**
     * Method to update trial status
     * @param eachBusinessId
     */
    public void updateTrialStatus(List<Integer> eachBusinessId) {

        try {
            String sql = "UPDATE business SET trial_status = '0' WHERE business_id = ?";
            jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
                public void setValues(PreparedStatement ps, int i) throws SQLException {
                    ps.setInt(1, eachBusinessId.get(i));
                    logger.info("Updated Trial status as '0' for the business : " + eachBusinessId.get(i));
                }

                public int getBatchSize() {
                    return eachBusinessId.size();
                }
            });
        } catch (Exception e) {
            logger.error("Error in updateTrialStatus() : " + e);
        }
    }

}
