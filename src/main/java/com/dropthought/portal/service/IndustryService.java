package com.dropthought.portal.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Component
public class IndustryService  extends BaseService{

    @Autowired
    @Qualifier("jdbcDt")
    JdbcTemplate jdbcTemplate;

    /**
     * get list of industry
     * @return
     */
    public List getIndustry(){
        List industryList = new ArrayList();
        try{
            String industrySQL = "select industry_id, industry_name from lkp_industry";
            List existingIndustry = jdbcTemplate.queryForList(industrySQL);
            Iterator iterator = existingIndustry.iterator();
            while (iterator.hasNext()){
                try{
                    Map eachIndustry = (Map)iterator.next();
                    String eachIndustryName = eachIndustry.get("industry_name").toString();
                    String templateSql = "select template_names from templates where json_contains(industry,'[\""+eachIndustryName+"\"]') = 1";
                    List templates = jdbcTemplate.queryForList(templateSql);
                    if(templates.size()>0){
                        industryList.add(eachIndustry);
                    }
                }catch (Exception e){
                    logger.error(e.getMessage());
                }

            }

        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return industryList;
    }
}
