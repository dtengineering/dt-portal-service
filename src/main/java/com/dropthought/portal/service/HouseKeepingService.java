package com.dropthought.portal.service;

import com.dropthought.portal.businessconfigurations.dto.BusinessConfigurationsBean;
import com.dropthought.portal.businessconfigurations.service.BusinessConfigVersionService;
import com.dropthought.portal.businessconfigurations.service.BusinessConfigurationsService;
import com.dropthought.portal.model.HouseKeepingBean;
import com.dropthought.portal.util.Utils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public class HouseKeepingService {

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());


    @Autowired
    private BusinessConfigurationsService businessConfigurationsService;

    @Autowired
    private BusinessConfigVersionService businessConfigVersionService;

    public boolean configureTextAnalytics(HouseKeepingBean houseKeepingBean) {
        boolean status = true;
        logger.info("begin configure text analytics");
        try {
            String businessUUID = houseKeepingBean.getBusinessUUID();
            boolean txtAnalyticsValue = houseKeepingBean.getTxtAnalytics();
            logger.info("business uuid {}", businessUUID);
            logger.info("enable value {}", txtAnalyticsValue);

                //begin process text analytics
                logger.info("text analytics value {}",txtAnalyticsValue);
                boolean businessConfigStatus = updateTxtAnalyticsBusinessConfig(businessUUID, txtAnalyticsValue);
                logger.info("business config status {}", businessConfigStatus);


                boolean businessTxtAnalyticsStatus = updateTxtAnalyticsMapping(businessUUID, txtAnalyticsValue);
                logger.info("business text analytics staus {}", businessTxtAnalyticsStatus);

                status = (businessTxtAnalyticsStatus && businessConfigStatus) ? true : false;
                //end process text analytics


        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("end configure text analytics");
        return status;
    }


    public boolean updateTxtAnalyticsMapping(String businessUUID, boolean txtAnalyticsValue) {
        boolean txtAnalyticsStatus = true;
        try {
            Map textAnalyticsMap = businessConfigurationsService.retriveTextAnalyticsMapping(businessUUID);
            Map inputMap = (Map) textAnalyticsMap.get(businessUUID);
            inputMap.remove("enabled");
            Set keySet = inputMap.keySet();
            if(keySet.size()>0){
                inputMap.put("enabled", txtAnalyticsValue);
                JSONObject jsonObject = new JSONObject(inputMap);
                Map txtConfigMap = businessConfigurationsService.upsertTextAnalytics(jsonObject.toString(), businessUUID);
                String status = (txtConfigMap.containsKey("status") && Utils.notEmptyString(txtConfigMap.get("status").toString())) ? txtConfigMap.get("status").toString() : "";
                txtAnalyticsStatus = status.equals("success");
            }




        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return txtAnalyticsStatus;
    }

    /**
     * @param businessUUID
     * @param txtAnalyticsValue
     * @return
     */
    public boolean updateTxtAnalyticsBusinessConfig(String businessUUID, boolean txtAnalyticsValue) {

        boolean businessConfigStatus = true;
        try {

            List response = businessConfigurationsService.getBusinessConfigurationsByBusinessId(businessUUID);
            BusinessConfigurationsBean businessConfigurationsBean = new BusinessConfigurationsBean();
            Map oldBusinessConfigurationMap = new HashMap();
            if (response.size() > 0) {
                Map responseMap = (Map) response.get(0);
                oldBusinessConfigurationMap = (Map) response.get(0);
                Integer businessId = Integer.parseInt(responseMap.get("businessId").toString());
                businessConfigurationsBean.setBusinessId(businessId);
                businessConfigurationsBean.setEmail(responseMap.get("email").toString());
                businessConfigurationsBean.setQrcode(responseMap.get("qrcode").toString());
                businessConfigurationsBean.setShareableLink(responseMap.get("sharableLink").toString());
                businessConfigurationsBean.setSms(responseMap.get("sms").toString());
                businessConfigurationsBean.setTextAnalytics((txtAnalyticsValue) ? "1" : "0");
                businessConfigurationsBean.setLogic(responseMap.get("logic").toString());
                businessConfigurationsBean.setRespondent(responseMap.get("respondent").toString());
                businessConfigurationsBean.setSegment(responseMap.get("segment").toString());
                businessConfigurationsBean.setKiosk(responseMap.get("kiosk").toString());
                businessConfigurationsBean.setAdvancedSchedule(responseMap.get("advancedSchedule").toString());
                businessConfigurationsBean.setEnglish(responseMap.get("english").toString());
                businessConfigurationsBean.setArabic(responseMap.get("arabic").toString());
                businessConfigurationsBean.setMultiSurveys(responseMap.get("multiSurveys").toString());
                businessConfigurationsBean.setMobileApp(responseMap.get("mobileApp").toString());
                businessConfigurationsBean.setNotification(responseMap.get("notification").toString());
                businessConfigurationsBean.setTrigger(responseMap.get("trigger").toString());
                businessConfigurationsBean.setMetadataQuestion(responseMap.get("metadataQuestion").toString());
                businessConfigurationsBean.setBitly(responseMap.get("bitly").toString());
                businessConfigurationsBean.setPreferredMetric(responseMap.get("preferredMetric").toString());
                businessConfigurationsBean.setCohortAnalysis( (Utils.isNotNull(responseMap.get("cohortAnalysis"))) ? responseMap.get("cohortAnalysis").toString(): "0");
                businessConfigurationsBean.setAdvancedTemplates(Utils.isNotNull(responseMap.get("advancedTemplates")) ? responseMap.get("advancedTemplates").toString() : "0");
                businessConfigurationsBean.setMetricCorrelation(Utils.isNotNull(responseMap.get("metricCorrelation")) ? responseMap.get("metricCorrelation").toString() : "1");
                businessConfigurationsBean.setCategoryAnalysis(responseMap.get("categoryAnalysis").toString());
                businessConfigurationsBean.setEmotionAnalysis(responseMap.get("emotionAnalysis").toString());
                businessConfigurationsBean.setIntentAnalysis(responseMap.get("intentAnalysis").toString());
                businessConfigurationsBean.setAdvancedTextAnalytics(responseMap.get("advancedTextAnalytics").toString());
                businessConfigurationsBean.setNoOfUsers(Integer.parseInt(responseMap.get("noOfUsers").toString()));
                businessConfigurationsBean.setNoOfActivePrograms(Integer.parseInt(responseMap.get("noOfActivePrograms").toString()));
                businessConfigurationsBean.setNoOfTriggers(Integer.parseInt(responseMap.get("noOfTriggers").toString()));
                businessConfigurationsBean.setNoOfResponses(Integer.parseInt(responseMap.get("noOfResponses").toString()));
                businessConfigurationsBean.setNoOfMetrics(Integer.parseInt(responseMap.get("noOfMetrics").toString()));
                businessConfigurationsBean.setNoOfEmailsSent(Integer.parseInt(responseMap.get("noOfEmailsSent").toString()));
                businessConfigurationsBean.setNoOfSms(Integer.parseInt(responseMap.get("noOfSms").toString()));
                businessConfigurationsBean.setNoOfApi(Integer.parseInt(responseMap.get("noOfApi").toString()));
                businessConfigurationsBean.setNoOfFilters(Integer.parseInt(responseMap.get("noOfFilters").toString()));
                businessConfigurationsBean.setNoOfLists(Integer.parseInt(responseMap.get("noOfLists").toString()));
                businessConfigurationsBean.setNoOfRecipients(Integer.parseInt(responseMap.get("noOfRecipients").toString()));
                businessConfigurationsBean.setDataRetention(Integer.parseInt(responseMap.get("dataRetention").toString()));

            }

            int updateStatus = businessConfigurationsService.updateBusinessConfigurations(businessConfigurationsBean, businessUUID, oldBusinessConfigurationMap);
            if (updateStatus > 0) {
                businessConfigVersionService.createBusinessVersionConfigurations(businessConfigurationsBean,false);
                response = businessConfigurationsService.getBusinessConfigurationsByBusinessId(businessUUID);
                if (response.size() > 0) {

                    Map responseMap = (Map) response.get(0);
                    businessConfigStatus = true;

                } else {
                    businessConfigStatus = false;

                }
            }
            logger.info("response value {}", businessConfigStatus);


        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        return businessConfigStatus;
    }

}
