package com.dropthought.portal.service;

import com.dropthought.portal.dao.MultiAccountDAO;
import com.dropthought.portal.model.ConnectedAccountBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class MultiAccountService extends  BaseService{

    @Autowired
    MultiAccountDAO multiAccountDAO;

    @Autowired
    IdmService idmService;

    public void renewUserTokens() {
        List<ConnectedAccountBean> updatedTokenAccounts = new ArrayList<>();
        List<ConnectedAccountBean> connectedAccountBeanList = multiAccountDAO.getUserConnectedAccounts();

        for (ConnectedAccountBean connectedAccountBean : connectedAccountBeanList) {
            try {
                String token = connectedAccountBean.getRefreshToken();
                Map renewedTokens = this.renewTokens(token);
                if (renewedTokens.isEmpty()) {
                    connectedAccountBean.setRefreshToken(null);
                    connectedAccountBean.setExpiration(null);
                    connectedAccountBean.setActivation(0);
                } else if (renewedTokens.containsKey("refreshToken")) {
                    connectedAccountBean.setRefreshToken((String) renewedTokens.get("refreshToken"));
                    connectedAccountBean.setExpiration((String) renewedTokens.get("refreshTokenExpiration"));
                    connectedAccountBean.setActivation(1);
                }
                updatedTokenAccounts.add(connectedAccountBean);

            } catch (Exception e) {
                logger.error("Error in renewUserTokens {}", e.getMessage());
            }
        }
        if(updatedTokenAccounts.size() > 0)
            multiAccountDAO.updateUserConnectedAccountsBatch(updatedTokenAccounts);
    }

    public Map renewTokens(String token) {
        Map resultMap = new HashMap();

        Map tokenMap = idmService.renewUserToken(token);
        if (tokenMap.containsKey("refresh_token")) {
            resultMap.put("refreshToken", tokenMap.get("refresh_token"));
            resultMap.put("refreshTokenExpiration", getExpirationTime());
        }
        return resultMap;
    }

    private String getExpirationTime() {
        // Format the date time
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        // get UTC timezone
        ZonedDateTime currentZonedTime = ZonedDateTime.now(ZoneId.of("UTC"));
        ZonedDateTime zonedTimePlusOneHour = currentZonedTime.plusHours(1);// Add one hour to the current time since the token is valid for 1 hour
        return zonedTimePlusOneHour.format(formatter);
    }
}
