package com.dropthought.portal.service;


import com.dropthought.portal.model.UserAuthenticationBean;
import com.dropthought.portal.model.UserBean;
import com.dropthought.portal.util.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Prabhakar Thakur on 10/7/2020.
 */

@Component
public class UserAuthenticationService  {

    @Autowired
    private UserService userService;

    //@Autowired
    //private JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("jdbcDt")
    private JdbcTemplate jdbcTemplate;


    @Autowired
    @Qualifier("jdbcPortal")
    private JdbcTemplate portalJdbcTemplate;

    @Autowired
    @Qualifier("jdbcNamedPortal")
    private NamedParameterJdbcTemplate portalNamedParameterJdbcTemplate;

    /**
     * Autowiring Email utility
     */
    @Autowired
    protected MailService emailUtility;

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();


    public String  select_user_by_email="select user_id, full_name, first_name, last_name, username, user_email, password, phone, confirmation_status, created_by, created_time, modified_time, modified_by, user_uuid from users where user_email = ?";

    public String select_uuid_by_userid="select user_uuid from users where user_id = ?";

     /* Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());


    /**
     * Function to return user information based upon userEmail
     *
     * @param userEmail
     * @return
     */
    public UserBean getUserByEmail(String userEmail) {
        List<UserBean> result = new ArrayList<UserBean>();

        UserBean userBean = new UserBean();

        try {

            List userRows = portalJdbcTemplate.queryForList(select_user_by_email,new Object[]{userEmail});
            if(userRows.size()>0){
                Map userMap = (Map)userRows.get(0);
                if(userMap.containsKey("confirmation_status") && Utils.isStringNumber(userMap.get("confirmation_status").toString()))
                    userBean.setConfirmationStatus(Integer.parseInt(userMap.get("confirmation_status").toString()));
                if(userMap.containsKey("full_name") && Utils.isNotNull(userMap.get("account_country_id")))
                    userBean.setFullName(userMap.get("full_name").toString());
                if(userMap.containsKey("first_name") && Utils.isNotNull(userMap.get("first_name")))
                    userBean.setFirstName(userMap.get("first_name").toString());
                if(userMap.containsKey("last_name") && Utils.isNotNull(userMap.get("last_name")))
                    userBean.setLastName(userMap.get("last_name").toString());
                if(userMap.containsKey("username") && Utils.isNotNull(userMap.get("username")))
                    userBean.setUserName(userMap.get("username").toString());
                if(userMap.containsKey("user_email") && Utils.isNotNull(userMap.get("user_email")))
                    userBean.setUserEmail(userMap.get("user_email").toString());
                if(userMap.containsKey("password") && Utils.isNotNull(userMap.get("password")))
                    userBean.setPassword(userMap.get("password").toString());
                if(userMap.containsKey("phone") && Utils.isNotNull(userMap.get("phone")))
                    userBean.setPhone(userMap.get("phone").toString());

                int createdBy = 0;
                if(userMap.containsKey("created_by") && Utils.isStringNumber(userMap.get("created_by").toString()))
                    createdBy = (int)userMap.get("created_by");
                String createdByUUID = "";
                if (createdBy > 0) {
                    Map createdUserMap = userService.retrieveUserByUserId(createdBy);
                    createdByUUID = createdUserMap.get("user_uuid").toString();

                }
                int modifiedBy = 0;
                if(userMap.containsKey("modified_by") && Utils.isStringNumber(userMap.get("modified_by").toString()))
                    modifiedBy = (int)userMap.get("modified_by");
                String modifiedByUUID = "";
                if (modifiedBy > 0) {
                    Map modifiedUserMap = userService.retrieveUserByUserId(modifiedBy);
                    modifiedByUUID = modifiedUserMap.get("user_uuid").toString();
                }

                userBean.setCreatedBy(createdByUUID);
                userBean.setModifiedBy(modifiedByUUID);
                if(userMap.containsKey("user_id") && Utils.isStringNumber(userMap.get("user_id").toString()))
                    userBean.setUserId((int)userMap.get("user_id"));
                if(userMap.containsKey("user_uuid"))
                    userBean.setUserUUID(userMap.get("user_uuid").toString());
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return userBean;
    }

    public String sendLoginEmail(UserAuthenticationBean userAuthenticationBean) {
        // To send Email to User
        try {

            String result;
            String businessUUID = "";
            String email_status = "not_sent";
            String recipientEmail = userAuthenticationBean.getVerifyEmail();
            String recipientName = userAuthenticationBean.getVerifyName();
            String host = userAuthenticationBean.getHost();
            logger.info("Host " + host);
            result = portalJdbcTemplate.queryForObject(select_uuid_by_userid, new Object[]{userAuthenticationBean.getVerifyUid()}, String.class);
            String subject = "Welcome to CS Portal";
            String buttonVerbiage = "Access Account";
            String link = null;
            link = host + "/login/";
            logger.info("Email link " + link);
            String lineOne = "You have been invited to join CS Portal";
            String lineTwo = "We have already created an account for you. As the first step, click on the 'Access Account' button and use the credentials provided below to login to your account. Once you login, go to account settings and update your password.\n" +
                    "Great to have you onboard!";
            Map messages = new HashMap();
            Map categories = new HashMap();
            categories.put("userUUID", result);
            categories.put("businessUUID", businessUUID);
            messages.put("lineOne", lineOne);
            messages.put("lineTwo", lineTwo);
            logger.info("username " + userAuthenticationBean.getVerifyEmail());
            messages.put("userName", userAuthenticationBean.getVerifyEmail());
            logger.info("Password " + userAuthenticationBean.getPassword());
            messages.put("password", userAuthenticationBean.getPassword());
            messages.put("type", "userManagement");
            messages.put("categories", categories);
            int resultVal = emailUtility.sendGridHtmlEmailSender(recipientEmail, recipientName, subject, buttonVerbiage, link, messages, "loginInstructions.html");
            email_status = "sent";
            return email_status;
        } catch (EmptyResultDataAccessException e) {
            return "not_sent";
        } catch (Exception e) {
            return "not sent";
        }

    }


    /**
     * Function to send resetpassword to given userEmail
     */
    public int sendResetEmail(UserBean userBean, String host, String language) {
        int result = 0;
        try{
            Map userMap = mapper.convertValue(userBean, new TypeReference<Map<String, Object>>() {
            });
            logger.info("userMap"+userMap);
            if (userMap.size() > 0) {
                String recipientEmail = userMap.get("userEmail").toString();
                String recipientName = userMap.get("firstName").toString();
                String userUUID = userMap.get("userUUID").toString();
                String subject = "Password Update - CS Portal";
                String buttonVerbiage ="Update Password";
                String link = null;
                link = host + "/resetpassword" + "/" + userUUID;
                String lineOne = "Forgot your password?";
                String lineTwo = "Do not worry, we got you! Let's get you a new password. Click on the button below to reset your password.";
                Map messages = new HashMap();
                Map categories = new HashMap();
                categories.put("userUUID", userUUID);
                messages.put("lineOne", lineOne);
                messages.put("lineTwo", lineTwo);
                messages.put("type", "resetPassword");
                messages.put("categories", categories);
                if(language.equals("en")){
                    result = emailUtility.sendGridHtmlEmailSender(recipientEmail, recipientName, subject, buttonVerbiage, link, messages, "emailContent.html");
                }
            }

        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return result;
    }

    /**
     * Function to authenticate user
     * Created by Prabhakar Thakur on 17/7/2020
     * @param email
     * @param password
     * @return
     */

    public List authenticateUser(String email, String password) {
        logger.info("Begin authenticating user");
        List returnList = new ArrayList();
        int userStatus = 0;
        int confirmationStatus = 0;
        int usrID = 0;
        String select_user_by_email="select * from users where user_email =?";
        try {

            List<Map<String, Object>> result = portalJdbcTemplate.queryForList(select_user_by_email, new Object[]{email});
            if (result.size() > 0) {
                Map userMap = result.get(0);
                UserBean userBean = new UserBean();

                if (userMap.containsKey("user_id")) {
                    userBean.setUserId((int) userMap.get("user_id"));
                    usrID = (int) userMap.get("user_id");
                }
                if (userMap.containsKey("user_status")) {
                    userBean.setUserStatus(userMap.get("user_status").toString());
                    userStatus = Integer.parseInt(userMap.get("user_status").toString());
                }
                if (userMap.containsKey("user_email")) {
                    userBean.setUserEmail(userMap.get("user_email").toString());
                }
                if (userMap.containsKey("password")) {
                    userBean.setPassword(userMap.get("password").toString());
                }
                if (userMap.containsKey("confirmation_status")) {
                    userBean.setConfirmationStatus(Integer.parseInt(userMap.get("confirmation_status").toString()));
                    confirmationStatus = Integer.parseInt(userMap.get("confirmation_status").toString());
                }
                if (userMap.containsKey("user_uuid")) {
                    String userUUID = userMap.get("user_uuid").toString();
                    userBean.setUserUUID(userUUID);

                }
                String userPassword = userBean.getPassword();
                String userId = userBean.getUserId().toString();


                    MessageDigest digest = MessageDigest.getInstance("SHA-256");
                    digest.update(password.getBytes());
                    byte byteData[] = digest.digest();
                    StringBuffer hexString = new StringBuffer();
                    for (int i = 0; i < byteData.length; i++) {
                        String hex = Integer.toHexString(0xff & byteData[i]);
                        if (hex.length() == 1) hexString.append('0');
                        hexString.append(hex);
                    }
                    String stringHashPassword = hexString.toString();
                    if (userPassword.equals(stringHashPassword)) {
                        returnList.add(0, userBean);
                    }

                // to add last login time
                if (userStatus == 1 && usrID > 0) {
                    if (confirmationStatus == 1) {
                        String currentDateTime = Utils.getCurrentUTCDateAsString();
                        String updateSql = "update users set last_login_time = ? where user_id = ?";
                        jdbcTemplate.update(updateSql,new Object[]{currentDateTime, usrID});
                    }
                }
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("End authenticating user");
        return returnList;
    }
}