package com.dropthought.portal.service;


import com.dropthought.portal.businessconfigurations.service.BusinessConfigurationsService;
import com.dropthought.portal.dao.ClientDAO;
import com.dropthought.portal.model.*;
import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.TokenUtility;
import com.dropthought.portal.util.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.apache.commons.text.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.*;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.context.ContextLoader;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;


@Component
public class ClientService extends BaseService {

    public static final String CLIENT = "client";
    public static final String CSACCESS = "access";
    public static final String CSUSER = "grantaccess";
    public static final String ELIZA = "eliza";
    public static final String UKING = "updateking";
    public static final String MKING = "king";
    public static final String USER = "user";
    public static final String SURVEYTEMPLATE = "/template";
    public static final String SURVEYTEMPLATEBYBUSINESS = "/templates";
    public static final String TEMPLATESUMMARY = "/templates/summary";
    private static final String TEMPLATE_TABLE = "templates";

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper()
            .enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT)
            .enable(DeserializationFeature.FAIL_ON_READING_DUP_TREE_KEY);


    /**
     * JdbcTemplate for DB Connectivity.
     */
    @Autowired
    @Qualifier("jdbcDt")
    JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("jdbcNamedDt")
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Value("${dtp.url}")
    private String dtAppUrl;

    @Value("${storage.url}")
    private String storageUrl;

    @Value("${monkeylearn.usage.url}")
    private String monkeyLearnUsageUrl;

    @Value("${monkeylearn.limit.url}")
    private String monkeyLearnLimitUrl;

    @Autowired
    private UserService userService;

    @Autowired
    private PlanService planService;

    @Autowired
    private BusinessConfigurationsService businessConfigurationsService;

    @Autowired
    private TokenUtility tokenUtility;

    @Autowired
    private ClientDAO clientDAO;

    @Autowired
    private NotificationService notificationService;

    /**
     * Create client business and related king user profile
     *
     * @param clientBean
     * @return
     */
    /*public Map createClient(ClientBean clientBean) {
        logger.debug("Create client business begins");
        Map resultMap = new HashMap();
        try {
            final String uri =  dtAppUrl+""+CLIENT;
            logger.info("uri {}" ,uri);

            //convert bean to Map
            Map inputMap = mapper.convertValue(clientBean, new TypeReference<Map>() {
            });

            resultMap = callRestApi(inputMap, uri, HttpMethod.POST);

        } catch (Exception e) {
            logger.error(e.getMessage());
            //logger.error(e.getMessage());
        }
        logger.debug("Create client business ends");
        return resultMap;
    }*/

    /**
     * @param clientData
     * @return
     */
    public Map createClient(String clientData) {
        logger.info("Create client business begins");
        Map resultMap = new HashMap();
        try {
            final String uri = dtAppUrl + "" + CLIENT;
            //final String uri = "http://localhost:8080/dtapp/api/client";
            logger.info("uri {}", uri);

            //testBeanConversion(clientData);

            //convert bean to Map
//            Map inputMap = mapper.convertValue(clientBean, new TypeReference<Map>() {
//            });

            /*JSONObject jsonObject = new JSONObject(clientData);
            HashMap inputMap = new HashMap();

            inputMap.put("businessName",jsonObject.get("businessName"));
            inputMap.put("email",jsonObject.get("email"));
            inputMap.put("fullName",jsonObject.get("fullName"));
            inputMap.put("noOfEmployees",jsonObject.get("noOfEmployees"));
            inputMap.put("phone",jsonObject.get("phone"));
            inputMap.put("contractStartDate",jsonObject.get("contractStartDate"));
            inputMap.put("contractEndDate",jsonObject.get("contractEndDate"));
            inputMap.put("industry",jsonObject.get("industry"));
            inputMap.put("title",jsonObject.get("title"));
            inputMap.put("country",jsonObject.get("country"));
            inputMap.put("businessTimezone",jsonObject.get("businessTimezone"));*/

            //HashMap inputMap =mapper.readValue(clientData, HashMap.class);
            //resultMap = callRestApi(inputMap, uri, HttpMethod.POST);

            JSONObject jsonObject = new JSONObject(clientData);
            logger.info("json objet {}", jsonObject.toString());

            //resultMap = callRestApiWithJson(jsonObject.toString(), uri, HttpMethod.POST);
            //resultMap = callRestApiWithJsonV1(jsonObject.toString(), uri, HttpMethod.POST);
            resultMap = callRestApiWithJsonV1(clientData, uri, HttpMethod.POST);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            //logger.error(e.getMessage());
        }
        logger.info("Create client business ends");
        return resultMap;
    }

    public int sendMsg(String smsMessage , String number) {
        int result = 0;
        logger.info("Start sending message to the user");
        //Twilio properties
        String accountId = "AC17b899e78476415bbc503bde69ab1bdf";
        String authToken = "7807acb495b691ec06e9ec92051527a6";
        String senderId = "+14083009233";

        try {
            Twilio.init(accountId, authToken);
            logger.info("Send to phone number: " + number);
            if (Utils.notEmptyString(number) && number.length() >= 10) {

                Message message = Message.creator(new PhoneNumber(number),
                        new PhoneNumber(senderId),
                        smsMessage).create();
                result = 1;
            } else {
                logger.info("The number is invalid");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("Finished sending the SMS");
        return result;
    }

    //Get account id
    public String getAccountId() {
        return "";
    }

    //Get user phone number
    public String getPhoneNum() {
        return "";
    }

    //Message to send back
    public String getMsgSend() {
        return "";
    }

    //Get auth token
    public String getAuthToken() {
        return "";
    }


    public int receiveMsg(String name, String number) {
        int result = 0;
        logger.info("Message received; start sending message back to the user");
        try {
            String smsMsg = "Hi! Thank you for your message.";
//            Map senderData =    GET THIS
            String accountId = "AC17b899e78476415bbc503bde69ab1bdf";
            String authToken = "7807acb495b691ec06e9ec92051527a6";
            Twilio.init(accountId, authToken);
            logger.info("Send to phone number: " + number);
            if (Utils.notEmptyString(number) && number.length() >= 10) {
                String senderId = "+14084449119";
                Message message = Message.creator(new PhoneNumber(number),
                        new PhoneNumber(senderId),
                        smsMsg).create();
                result = 1;
            } else {
                logger.info("The number is invalid");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("Finished sending the SMS back to the user");
        return result;
    }


    /**
     * Update client business and account information
     *
     * @param data
     * @return
     */
    public Map updateClient(String businessUUID, String data) {
        logger.debug("Update client business begins");
        Map resultMap = new HashMap();
        try {

            final String uri = dtAppUrl + "" + CLIENT + "/" + businessUUID;
            logger.info("uri {}", uri);

            JSONObject jsonObject = new JSONObject(data);
            //convert bean to Map
            //Map inputMap = mapper.convertValue(clientBean, new TypeReference<Map>() {
            //});

            //resultMap = callRestApi(inputMap, uri, HttpMethod.PUT);

            resultMap = callRestApiWithJsonV1(jsonObject.toString(), uri, HttpMethod.PUT);

        } catch (Exception e) {
            logger.error(e.getMessage());
            //logger.error(e.getMessage());
        }
        logger.debug("Update client business ends");
        return resultMap;
    }

    /**
     * Get all Client business details
     *
     * @return
     */
    public List getAllClient(String timezone, String searchTerm, String planName, int pageId) {

        List result = new ArrayList();
        String GET_ALL_BUSINESS = "select business_id, business_uuid, plan_config_id from business";
        timezone = Utils.isNotNull(timezone) && Utils.notEmptyString(timezone) ? timezone : "America/Los_Angeles";

        //DTV-11025
        int planConfigId = getPlandIdByPlanName(planName);

        if (Utils.notEmptyString(searchTerm) && planConfigId > 0) {
            GET_ALL_BUSINESS += "where business_name LIKE '%" + searchTerm + "%' OR business_email LIKE '%" + searchTerm + "%' " +
                    " and plan_config_id = " + planConfigId;
        } else if (Utils.notEmptyString(searchTerm) && planConfigId == 0) {
            GET_ALL_BUSINESS += "where business_name LIKE '%" + searchTerm + "%' OR business_email LIKE '%" + searchTerm + "%' ";
        } else if (planConfigId > 0 && Utils.emptyString(searchTerm)) {
            GET_ALL_BUSINESS += "where plan_config_id = " + planConfigId;
        }
        int upperLimit = 0;
        int length = 10;
        boolean pageExists = false;
        if (pageId > -1) {
            upperLimit = (pageId - 1) * length;
            GET_ALL_BUSINESS += " limit ?, ?";
            pageExists = true;
        }

        List businessDataAll = jdbcTemplate.queryForList(GET_ALL_BUSINESS, pageExists ? new Object[]{upperLimit, length} : new Object[]{});
        if (businessDataAll.size() > 0) {
            Iterator<Map> businessAllIterator = businessDataAll.iterator();
            while (businessAllIterator.hasNext()) {
                try {
                    Map businessMap = businessAllIterator.next();
                    int businessId = (int) businessMap.get("business_id");
                    String businessUUID = businessMap.get("business_uuid").toString();

                    List buisnessResult = this.getClientByClientId(businessUUID, timezone);
                    if (buisnessResult.size() > 0) {

                        Map<String, Object> eachBusinessMap = (Map) buisnessResult.get(0);

                        String planNameFromDB = "";
                        int planConfigIdFromDB = 0;
                        if (Utils.isNotNull(businessMap.get("plan_config_id")) && Utils.notEmptyString(businessMap.get("plan_config_id").toString()) &&
                                Integer.parseInt(businessMap.get("plan_config_id").toString()) > 0) {
                            planConfigIdFromDB = (int) businessMap.get("plan_config_id");
                            String planNameSql = "SELECT plan_name FROM plan_configurations WHERE plan_config_id = ?";
                            planNameFromDB = jdbcTemplate.queryForObject(planNameSql, new Object[]{planConfigIdFromDB}, String.class);
                        }
                        eachBusinessMap.put("planName", planNameFromDB);
                        eachBusinessMap.put("planConfigId", planConfigIdFromDB);

                        result.add(eachBusinessMap);
                    }

                } catch (Exception e) {
                    logger.info("Error getting business");
                }
            }
        }
        return result;
    }

    /**
     * Function to return information about a particular business client by client Id
     *
     * @param businessUUID
     * @return
     */
    public List getClientByClientId(String businessUUID, String timezone) {
        List result = null;
        try {
            String SELECT_BUSINESS_BY_ID = "select * from business where business_id=?";
            timezone = Utils.isNotNull(timezone) && Utils.notEmptyString(timezone) ? timezone : "America/Los_Angeles";
            String userTimeZoneDate = "";
            String userTimeCsStartDate = "";
            String userTimeCsEndDate = "";
            String userTimeContractStartDate = "";
            String userTimeContractEndDate = "";
            logger.info("begin retrieving client by business uuid");
            int businessId = 0;
            if (businessUUID != null && businessUUID.length() > 0) {
                businessId = this.retrieveBusinessIDByUUID(businessUUID);
                result = jdbcTemplate.queryForList(SELECT_BUSINESS_BY_ID, new Object[]{businessId});

                Map kingUserMap = this.getKingUserByBusinessId(businessId);

                //Get business global settings logo, theme, email alert
                Map businessGlobalSettingMap = this.getBusinessGlobalSettings(businessId);
                String currentDateTime = Utils.getCurrentUTCDateAsString();

                List businessList = new ArrayList();
                Iterator<Map> iterator = result.iterator();
                while (iterator.hasNext()) {
                    Map ClientMap = iterator.next();
                    Object id = ClientMap.remove("business_id");
                    ClientMap.put("businessId", id);
                    Object businessName = ClientMap.remove("business_name");
                    ClientMap.put("businessName", businessName);
                    Object businessTypeId = ClientMap.remove("business_type_id");
                    //ClientMap.put("businessTypeId", businessTypeId);
                    Object businessTimezone = ClientMap.remove("business_timezone");
                    ClientMap.put("businessTimezone", businessTimezone);
                    Object businessAddress = ClientMap.remove("business_address");
                    //ClientMap.put("businessAddress", Utils.isNotNull(businessAddress) ? businessAddress.toString() : "");
                    Object businessEmail = ClientMap.remove("business_email");
                    ClientMap.put("businessEmail", businessEmail);
                    Object businessPhone = ClientMap.remove("business_phone");
                    //ClientMap.put("businessPhone", Utils.isNotNull(businessPhone) ? businessPhone.toString() : "");
                    Object businessUUId = ClientMap.remove("business_uuid");
                    ClientMap.put("businessUUID", businessUUId);
                    Object reason = ClientMap.remove("reason");
                    ClientMap.put("reason", Utils.isNotNull(reason) ? reason.toString() : "");
                    Object state = ClientMap.remove("state");
                    ClientMap.put("state", state);
                    Object csAccess = ClientMap.remove("cs_access");
                    ClientMap.put("csAccess", csAccess);
                    Object createdTime = ClientMap.remove("created_time");
                    ClientMap.put("createdTime", Utils.isNotNull(createdTime) ? createdTime.toString() : createdTime);
                    Object modifiedTime = ClientMap.remove("modified_time");
                    ClientMap.put("modifiedTime", Utils.isNotNull(modifiedTime) ? modifiedTime.toString() : modifiedTime);
                    Object industry = ClientMap.remove("industry");
                    ClientMap.put("industry", Utils.isNotNull(industry) ? industry.toString() : "");
                    Object advancedAnalysisUrl = ClientMap.remove("advanced_analysis_url");
                    Object hippa = ClientMap.remove("hippa");
                    ClientMap.put("hippa", Utils.isNotNull(hippa) ? hippa.toString() : "0");

                    // Return advanced analysis url if config is enabled #DTV-4777
                    List response = businessConfigurationsService.getBusinessConfigurationsByBusinessId(businessUUID);
                    if (response.size() > 0) {
                        Map responseMap = (Map) response.get(0);
                        boolean isAdvanedAnalysisEnabled = Utils.isNotNull(responseMap.get("advancedAnalysis")) && Integer.parseInt(responseMap.get("advancedAnalysis").toString()) == 1;
                        if (isAdvanedAnalysisEnabled && Utils.isNotNull(advancedAnalysisUrl)) {
                            ClientMap.put("advancedAnalysisUrl", advancedAnalysisUrl);
                        }
                    }

                    Object contractStartDate = ClientMap.remove("contract_start_date");
                    String csContractStartDate = Utils.isNotNull(contractStartDate) ? contractStartDate.toString() : "";
                    //convert start and end date to localtimezone DTV-3795
                    if (Utils.notEmptyString(csContractStartDate)) {
                        userTimeContractStartDate = Utils.getDateAsStringByTimezone(csContractStartDate, timezone);
                        logger.info("userTimeContractStartDate   " + userTimeContractStartDate);
                    }
                    ClientMap.put("contractStartDate", Utils.notEmptyString(userTimeContractStartDate) ? userTimeContractStartDate : csContractStartDate);

                    Object contractEndDate = ClientMap.remove("contract_end_date");
                    String csContractEndDate = Utils.isNotNull(contractEndDate) ? contractEndDate.toString() : "";
                    if (Utils.notEmptyString(csContractEndDate)) {
                        userTimeContractEndDate = Utils.getDateAsStringByTimezone(csContractEndDate, timezone);
                        logger.info("userTimeContractEndDate   " + userTimeContractEndDate);
                    }
                    String contractEndDateModified = Utils.isNotNull(userTimeContractEndDate) ? Utils.convertToEndOfDay(userTimeContractEndDate) : csContractEndDate;
                    ClientMap.put("contractEndDate", contractEndDateModified);
                    ClientMap.remove("business_code");
                    ClientMap.remove("business_db");
                    ClientMap.put("kingName", kingUserMap.containsKey("full_name") && kingUserMap.get("full_name") != null ? kingUserMap.get("full_name").toString() : "");
                    ClientMap.put("kingEmail", kingUserMap.containsKey("user_email") && kingUserMap.get("user_email") != null ? kingUserMap.get("user_email").toString() : "");
                    ClientMap.put("kingMobile", kingUserMap.containsKey("phone") && kingUserMap.get("phone") != null ? kingUserMap.get("phone").toString() : "");
                    ClientMap.put("kingTitle", kingUserMap.containsKey("title") && kingUserMap.get("title") != null ? kingUserMap.get("title").toString() : "");
                    ClientMap.put("employeesRange", kingUserMap.containsKey("account_emp_range") && kingUserMap.get("account_emp_range") != null ? kingUserMap.get("account_emp_range").toString() : "");
                    ClientMap.put("countryId", kingUserMap.containsKey("country_id") && kingUserMap.get("country_id") != null ? kingUserMap.get("country_id").toString() : "");
                    ClientMap.put("notification", businessGlobalSettingMap.containsKey("business_email_alert") && businessGlobalSettingMap.get("business_email_alert") != null ? businessGlobalSettingMap.get("business_email_alert").toString() : "");
                    ClientMap.put("kingUserUUID", kingUserMap.containsKey("user_uuid") && kingUserMap.get("user_uuid") != null ? kingUserMap.get("user_uuid").toString() : "");

                    String kingUserStatus = kingUserMap.containsKey("eula_status") && kingUserMap.get("eula_status") != null ? kingUserMap.get("eula_status").toString() : "";
                    if (Utils.notEmptyString(contractEndDateModified) && Utils.compareDates(currentDateTime, contractEndDateModified)) {
                        //#DTV-3776 Since contract end date is expired when compare to current date, making king status to 0
                        logger.info("Contract end date is expired");
                        kingUserStatus = Constants.KING_INACTIVE_STATE;
                    }
                    ClientMap.put("kingUserStatus", kingUserStatus);

                    String csStartDate = kingUserMap.containsKey("cs_start_date") && kingUserMap.get("cs_start_date") != null ? kingUserMap.get("cs_start_date").toString() : "";
                    if (Utils.notEmptyString(csStartDate)) {
                        userTimeCsStartDate = Utils.getDateAsStringByTimezone(csStartDate, timezone);
                        logger.info("userTimeCsStartDate   " + userTimeCsStartDate);
                    }
                    String csEndDate = kingUserMap.containsKey("cs_end_date") && kingUserMap.get("cs_end_date") != null ? kingUserMap.get("cs_end_date").toString() : "";
                    if (Utils.notEmptyString(csEndDate)) {
                        userTimeCsEndDate = Utils.getDateAsStringByTimezone(csEndDate, timezone);
                        logger.info("userTimeCsEndDate   " + userTimeCsEndDate);
                    }
                    String lastLoginTime = kingUserMap.containsKey("last_login_time") && kingUserMap.get("last_login_time") != null ? kingUserMap.get("last_login_time").toString() : "";
                    logger.info("lastLogin Time UTC  " + lastLoginTime);
                    if (Utils.notEmptyString(lastLoginTime)) {
                        userTimeZoneDate = Utils.getDateAsStringByTimezone(lastLoginTime, timezone);
                        logger.info("userTimeZoneDate   " + userTimeZoneDate);
                    }
                    ClientMap.put("csStartDate", Utils.notEmptyString(userTimeCsStartDate) ? userTimeCsStartDate : csStartDate);
                    ClientMap.put("csEndDate", Utils.notEmptyString(userTimeCsEndDate) ? userTimeCsEndDate : csEndDate);
                    ClientMap.put("lastLoginTime", Utils.notEmptyString(userTimeZoneDate) ? userTimeZoneDate : lastLoginTime);
                    ClientMap.put("businessLogo", businessGlobalSettingMap.containsKey("business_logo") && businessGlobalSettingMap.get("business_logo") != null ? businessGlobalSettingMap.get("business_logo").toString() : Constants.DEFAULT_LOGO);
                    ClientMap.put("businessTheme", businessGlobalSettingMap.containsKey("business_theme") && businessGlobalSettingMap.get("business_theme") != null ? businessGlobalSettingMap.get("business_theme").toString() : Constants.DEFAULT_THEME);
                    ClientMap.put("thoughtful", kingUserMap.containsKey("thoughtful") && kingUserMap.get("thoughtful") != null ? kingUserMap.get("thoughtful").toString() : "1");
                    int csAccessState = Integer.parseInt(csAccess.toString());
                    switch (csAccessState) {
                        case 0:
                            if (Utils.notEmptyString(csEndDate) && Utils.compareDates(currentDateTime, csEndDate)) {
                                ClientMap.put("status", "Access period ended");
                            } else {
                                ClientMap.put("status", "Default");
                            }
                            break;
                        case 1:
                            ClientMap.put("status", "Request Sent");
                            break;
                        case 2:
                            ClientMap.put("status", "Access Granted");
                            break;
                        case 3:
                            ClientMap.put("status", "Access Revoked");
                            break;
                    }

                    if (Utils.notEmptyString(csStartDate) && Utils.notEmptyString(csEndDate)) {
                        if (csStartDate.substring(0, 10).equals(contractStartDate.toString().substring(0, 10)) && csEndDate.substring(0, 10).equals(contractEndDate.toString().substring(0, 10))) {
                            ClientMap.put("contractDuration", "contract");
                        } else {
                            ClientMap.put("contractDuration", "custom");
                        }
                    } else {
                        ClientMap.put("contractDuration", "");
                    }

                    //DTV-12337 - Add plan name and plan config id to the response
                    String planName = "";
                    int planConfigId = 0;
                    Object planId = ClientMap.remove("plan_config_id");
                    if (Utils.isNotNull(planId) && Utils.notEmptyString(planId.toString()) && Integer.parseInt(planId.toString()) > 0) {
                        planConfigId = (int) planId;
                        String planNameSql = "SELECT plan_name FROM plan_configurations WHERE plan_config_id = ?";
                        planName = jdbcTemplate.queryForObject(planNameSql, new Object[]{planConfigId}, String.class);
                    }

                    ClientMap.put("planName", planName);
                    ClientMap.put("planConfigId", planConfigId);

                    businessList.add(ClientMap);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }

        return result;
    }

    /**
     * function to retrieve a business by businessUUID
     *
     * @param businessUUID
     * @return
     */
    public int retrieveBusinessIDByUUID(String businessUUID) {
        return clientDAO.retrieveBusinessIDByUUID(businessUUID);
    }

    /**
     * Function to get king user based on businessId
     *
     * @param businessId
     * @return
     */
    public Map getKingUserByBusinessId(Integer businessId) {
        String SELECT_KING_USER_BY_BUSINESS_ID = "select u.user_id, u.country_id, u.full_name, u.first_name, u.last_name, u.username, u.user_db, u.user_email, u.user_uuid, u.password, u.phone, u.trial_flag, u.complete_social_signup, u.confirmation_status, u.password_update_count, u.created_by, u.created_time, u.modified_time, u.modified_by, u.user_status, u.eula_status, u.push_notification, u.thoughtful, u.title, u.cs_start_date, u.cs_end_date, u.user_role, u.alias_email, u.last_login_time, a.account_id, a.account_type, a.signup_type_id, a.account_user_id, a.account_business_id, a.account_role_id, a.account_country_id, a.account_emp_range from users  as u  left join account as a on u.user_id = a.account_user_id where user_status = '1' and user_role = '1' and account_business_id = ?";

        Map userMap = new HashMap();
        try {
            List resultList = jdbcTemplate.queryForList(SELECT_KING_USER_BY_BUSINESS_ID, new Object[]{businessId});
            if (resultList.size() > 0) {
                userMap = (Map) resultList.get(0);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return userMap;
    }

    /**
     * Function to get  businessId by businessUUID
     *
     * @return
     */
    public int getBusinessIdByUUID(String businessUUID) {
        Integer result = 0;
        try {
            //System.out.println("business uuid "+businessUUID);
            String SELECT_ID_BY_BUSINESS_UUID = "select business_id from business where business_uuid = ?";
            //System.out.println(SELECT_ID_BY_BUSINESS_UUID);
            result = jdbcTemplate.queryForObject(SELECT_ID_BY_BUSINESS_UUID, new Object[]{businessUUID.trim()}, Integer.class);
        } catch (Exception e) {
            //e.printStackTrace();
            logger.info("business id not found for the given business uuid");
        }
        return result;
    }


    /**
     * @param data
     * @param uri
     * @param method
     * @return
     */
    private Map callRestApiWithJson(String data, String uri, HttpMethod method) {

        Map respMap = new HashMap();
        logger.info(data);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(data, headers);

        try {
            ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, method, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info(responseEntity.getBody().toString());
                    respMap = (Map) responseEntity.getBody();

                }
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return respMap;
    }

    /**
     * @param data
     * @param uri
     * @param method
     * @return
     */
    private Map callRestApiWithJsonV1(String data, String uri, HttpMethod method) {
        Map respMap = new HashMap();
        logger.info(data);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        HttpEntity<String> entity = new HttpEntity<String>(data, headers);

        try {
            //ResponseEntity<Object> responseEntity = restTemplate.postForEntity(uri, entity, Object.class);
            ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, method, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info(responseEntity.getBody().toString());
                    respMap = (Map) responseEntity.getBody();

                }
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return respMap;
    }

    /**
     * fucntion to perform REST callback to external application
     *
     * @param inputMap
     * @param uri
     * @param method
     * @return
     */
    private Map callRestApi(Map inputMap, String uri, HttpMethod method) throws JsonProcessingException {

        Map<String, Object> respMap = new HashMap();
        JSONObject json = new JSONObject(inputMap);
        String inputJson = json.toString();
        logger.info("Request : " + inputJson);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);
        ResponseEntity<Object> responseEntity = null;
        try {
            responseEntity = restTemplate.exchange(uri, method, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info("Response from " + uri + " " + responseEntity.getBody().toString());
                    respMap = (Map) responseEntity.getBody();

                }
            }
            //DTV-12448 - Handle 400 error from dtapp and showing it to the user instead of empty response.
            //  Handle HttpClientErrorException and ResourceAccessException
        } catch (HttpClientErrorException | ResourceAccessException e) {
            logger.error("Error in calling " + uri + " " + e.getMessage());
            // get the response body from the exception and convert it to a map
            String responseBody = e.getMessage();
            int startIndex = responseBody.indexOf("[");
            int endIndex = responseBody.lastIndexOf("]");

            if (startIndex != -1 && endIndex != -1 && startIndex < endIndex) {
                String result = responseBody.substring(startIndex, endIndex + 1);
                // Deserialize the JSON string into a List of Maps
                List<Map<String, Object>> respList = mapper.readValue(result, List.class);
                if (respList.size() > 0) {
                    respMap = respList.get(0);
                }
            } else {
                System.out.println("No valid brackets found.");
            }
        } catch (Exception ex) {
            logger.error("Error in calling " + uri + " " + ex.getMessage());
            //DTV-12448 - Handle 400 error from dtapp and showing it to the user instead of empty response
            if (Utils.isNotNull(responseEntity) && responseEntity.getStatusCode() != HttpStatus.OK  &&
                    Utils.isNotNull(responseEntity.getBody())) {
                respMap = (Map) responseEntity.getBody();
            }
        }
        return respMap;
    }

    /**
     * Retrieves client based on state 0/1
     *
     * @param state
     * @param searchTerm
     * @param pageId
     * @param exactMatch
     * @param sort
     * @param timezone
     * @return
     */
    public List getClientByState(int state, String searchTerm, int pageId, boolean exactMatch, String sort, String timezone) {
        List resultList = new ArrayList();
        List clientsList = new ArrayList();
        try {
            String search = "";
            String getClientSql = "";
            int upperLimit = 0;
            if (pageId > 1) {
                upperLimit = (pageId - 1) * 12;
            }
            if (Utils.notEmptyString(searchTerm)) {
                search = exactMatch ? searchTerm : "%" + searchTerm + "%";
            }
            logger.info("search  " + search);
            if (Utils.emptyString(search)) {
                if (state > -1) {
                    if (pageId > -1) {
                        logger.info("Need to search all clients based on state");
                        getClientSql = sort.equals("createdTime") ? "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business where state = ? order by created_time desc limit ?, 12" : "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business where state = ? order by modified_time desc limit ?, 12";
                        resultList = jdbcTemplate.queryForList(getClientSql, new Object[]{String.valueOf(state), upperLimit});
                    } else {
                        logger.info("Need to search all clients based on state");
                        getClientSql = sort.equals("createdTime") ? "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business where state = ? order by created_time desc " : "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business where state = ? order by modified_time desc ";
                        resultList = jdbcTemplate.queryForList(getClientSql, new Object[]{String.valueOf(state)});
                    }
                } else {
                    if (pageId > -1) {
                        logger.info("Need to search all clients");
                        getClientSql = sort.equals("createdTime") ? "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business order by created_time desc limit ?, 12" : "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business order by modified_time desc limit ?, 12";
                        resultList = jdbcTemplate.queryForList(getClientSql, new Object[]{upperLimit});
                    } else {
                        logger.info("Need to search all clients");
                        getClientSql = sort.equals("createdTime") ? "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business order by created_time desc " : "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business order by modified_time desc ";
                        resultList = jdbcTemplate.queryForList(getClientSql);
                    }
                }
            } else {
                if (state > -1) {
                    if (pageId > -1) {
                        logger.info("Need to search for clients based on state");
                        if (sort.equals("createdTime")) {
                            getClientSql = exactMatch ? "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business where state = ? and business_name = ? order by created_time desc limit ?, 12" : "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business where state = ? and business_name like ?  order by created_time desc limit ?, 12";
                        } else {
                            getClientSql = exactMatch ? "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business where state = ? and business_name = ? order by modified_time desc limit ?, 12" : "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business where state = ? and business_name like ?  order by modified_time desc limit ?, 12";
                        }
                        resultList = jdbcTemplate.queryForList(getClientSql, new Object[]{String.valueOf(state), search, upperLimit});
                    } else {
                        logger.info("Need to search for clients based on state");
                        if (sort.equals("createdTime")) {
                            getClientSql = exactMatch ? "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business where state = ? and business_name = ? order by created_time desc" : "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business where state = ? and business_name like ?  order by created_time desc ";
                        } else {
                            getClientSql = exactMatch ? "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business where state = ? and business_name = ? order by modified_time desc" : "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business where state = ? and business_name like ?  order by modified_time desc ";
                        }
                        resultList = jdbcTemplate.queryForList(getClientSql, new Object[]{String.valueOf(state), search});
                    }
                } else {
                    if (pageId > -1) {
                        logger.info("Need to search for all clients ");
                        if (sort.equals("createdTime")) {
                            getClientSql = exactMatch ? "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business where business_name = ? order by created_time desc limit ?, 12" : "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business where business_name like ?  order by created_time desc limit ?, 12";
                        } else {
                            getClientSql = exactMatch ? "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business where business_name = ? order by modified_time desc limit ?, 12" : "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business where business_name like ?  order by modified_time desc limit ?, 12";
                        }
                        resultList = jdbcTemplate.queryForList(getClientSql, new Object[]{search, upperLimit});
                    } else {
                        logger.info("Need to search for all clients ");
                        if (sort.equals("createdTime")) {
                            getClientSql = exactMatch ? "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business where business_name = ? order by created_time desc" : "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business where business_name like ?  order by created_time desc ";
                        } else {
                            getClientSql = exactMatch ? "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business where business_name = ? is not null order by modified_time desc" : "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, industry, contract_start_date, contract_end_date, reason, state, cs_access from business where business_name like ?  is not null order by modified_time desc ";
                        }
                        resultList = jdbcTemplate.queryForList(getClientSql, new Object[]{search});
                    }
                }

            }

            Iterator allBusinessIterator = resultList.iterator();
            while (allBusinessIterator.hasNext()) {
                Map eachBusinessMap = (Map) allBusinessIterator.next();
                String eachBusinessUUID = eachBusinessMap.get("business_uuid").toString();
                List eachBusinessList = this.getClientByClientId(eachBusinessUUID, timezone);
                if (eachBusinessList.size() > 0) {
                    clientsList.add((Map) eachBusinessList.get(0));
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return clientsList;
    }

    /**
     * Function to activate or create cs user for given businessId.
     *
     * @param businessUUID
     * @return
     */
    public Map enableCsUserByBusinessId(String businessUUID) {
        logger.info(" cs portal : send cs access request begins");
        Map resultMap = new HashMap();
        try {
            final String uri = dtAppUrl + "" + CLIENT + "/" + businessUUID + "/" + CSACCESS;
            logger.info("uri {}", uri);

            resultMap = callRestApi(uri, HttpMethod.POST);

        } catch (Exception e) {
            logger.error(e.getMessage());
            //logger.error(e.getMessage());
        }
        logger.debug("cs portal : send cs access request ends");
        return resultMap;
    }

    /**
     * Function to deactivating cs user for given businessId.
     *
     * @param businessUUID
     * @return
     */
    public Map disableCsUserByBusinessId(String businessUUID) {
        logger.info(" cs portal : send cs access request begins");
        Map resultMap = new HashMap();
        try {
            final String uri = dtAppUrl + "" + CLIENT + "/" + businessUUID + "/" + CSACCESS;
            logger.info("uri {}", uri);

            resultMap = callRestApi(uri, HttpMethod.DELETE);

        } catch (Exception e) {
            logger.error(e.getMessage());
            //logger.error(e.getMessage());
        }
        logger.debug("cs portal : send cs access request ends");
        return resultMap;
    }


    /**
     * Function to trigger cs access request email to king user
     *
     * @param clientPortalAccessBean
     * @return
     */
    public Map sendCsAccessRequest(ClientPortalAccessBean clientPortalAccessBean) {
        logger.debug(" cs portal : send cs access request begins");
        Map resultMap = new HashMap();
        try {
            final String uri = dtAppUrl + "" + CLIENT + "/csaccess";
            logger.info("uri {}", uri);

            //convert bean to Map
            Map inputMap = mapper.convertValue(clientPortalAccessBean, new TypeReference<Map>() {
            });

            resultMap = callRestApi(inputMap, uri, HttpMethod.POST);

        } catch (Exception e) {
            logger.error(e.getMessage());
            //logger.error(e.getMessage());
        }
        logger.debug("cs portal : send cs access request ends");
        return resultMap;
    }

    /**
     * Updates cs user for business account
     *
     * @param clientPortalAccessBean
     * @return
     */
    public Map updateCsUser(ClientPortalAccessBean clientPortalAccessBean) {
        logger.debug(" cs portal : cs user for business account begins");
        Map resultMap = new HashMap();
        try {
            final String uri = dtAppUrl + "" + CLIENT + "/" + CSUSER;
            logger.info("uri {}", uri);

            //RequestParameters
            Map inputMap = new HashMap();
            inputMap.put("userUUID", clientPortalAccessBean.getUserUUID());
            inputMap.put("timezone", clientPortalAccessBean.getTimeZone());
            inputMap.put("fromDate", clientPortalAccessBean.getFromDate());
            inputMap.put("toDate", clientPortalAccessBean.getToDate());

            resultMap = callRestApi(inputMap, uri, HttpMethod.POST);

        } catch (Exception e) {
            logger.error(e.getMessage());
            //logger.error(e.getMessage());
        }
        logger.debug("cs portal : cs user for business account ends");
        return resultMap;
    }


    /**
     * @param businessId
     * @return
     */
    public Map retrieveBusinessById(int businessId) {
        try {
            logger.debug("begin retrieving client business by business id");
            String selectBusinessByUUID = "select business_id, business_name, business_code, business_type_id, business_timezone, business_address, business_email, business_phone, business_db, created_time, modified_time, business_uuid, hippa from business where business_id=?";

            Map businessMap = jdbcTemplate.queryForMap(selectBusinessByUUID, new Object[]{businessId});
            logger.debug("end retrieving client business by business id");
            return businessMap;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new HashMap();
        }
    }

    /**
     * Method to get business contract start and end date by businessid
     *
     * @param businessId
     * @return
     */
    public Map getBusinessContractDates(Integer businessId) {
        return clientDAO.getBusinessContractDates(businessId);
    }

    /**
     * get business global settings
     *
     * @param businessId
     * @return
     */
    public Map getBusinessGlobalSettings(Integer businessId) {
        logger.info("get business global settings begins");
        Map businessMap = new HashMap();
        try {
            Map businessGlobalSettingMap = jdbcTemplate.queryForMap("select business_logo, business_theme, business_ratings, business_email_alert from business_global_settings where business_id = ?", new Object[]{businessId});
            if (businessGlobalSettingMap.size() > 0) {
                logger.info("business settings {}", businessGlobalSettingMap);
                String hexCode = businessGlobalSettingMap.containsKey("business_theme") ? businessGlobalSettingMap.get("business_theme").toString() : Constants.DEFAULT_THEME;
                String logo = "";
                if (businessGlobalSettingMap.containsKey("business_logo") && (Utils.isNotNull(businessGlobalSettingMap.get("business_logo")))) {
                    logo = businessGlobalSettingMap.get("business_logo").toString();
                    byte[] logoBytes = (byte[]) businessGlobalSettingMap.get("business_logo");
                    String logoUrl = new String(logoBytes);
                    if (logoUrl.contains("https")) {
                        logger.info("aws url");
                        logo = logoUrl;
                    } else if (Utils.notEmptyString(logoUrl)) {
                        logger.info("base 64 to aws url");
                        logo = this.base64toFileconversion(logo);
                    } else {
                        logger.info("default logo");
                        logo = Constants.DEFAULT_LOGO;
                    }
                } else {
                    logger.info("using the default logo");
                    logo = Constants.DEFAULT_LOGO;
                }

                businessMap.put("business_logo", logo);
                businessMap.put("business_theme", hexCode);
                businessMap.put("business_email_alert", businessGlobalSettingMap.containsKey("business_email_alert") && businessGlobalSettingMap.get("business_email_alert") != null ? businessGlobalSettingMap.get("business_email_alert").toString() : "");
                businessMap.put("business_ratings", businessGlobalSettingMap.containsKey("business_ratings") && businessGlobalSettingMap.get("business_ratings") != null ? businessGlobalSettingMap.get("business_ratings").toString() : "");
            }
        } catch (EmptyResultDataAccessException e) {
            //e.printStackTrace();
            //logger.error(e.getMessage());
            logger.error("Exception in getBusinessGlobalSettings for businessId {}  error  {}", businessId, e.getMessage());
        } catch (Exception e) {
            //e.printStackTrace();
            logger.error(e.getMessage());
            logger.error("Exception in getBusinessGlobalSettings for businessId {}  error  {}", businessId, e.getMessage());
        }
        logger.info("get business global settings ends");
        return businessMap;
    }

    /**
     * Update client configuration eliza
     *
     * @param businessUUID
     * @return
     */
    public Map updateClientConfigEliza(String businessUUID) {
        logger.info("Update call for eliza client cofiguration text analytics enabled");
        Map resultMap = new HashMap();
        try {
            final String uri = dtAppUrl + "" + CLIENT + "/" + ELIZA + "/" + businessUUID;
            logger.info("uri {}", uri);

            //convert bean to Map
            Map inputMap = new HashMap();

            resultMap = callRestApi(inputMap, uri, HttpMethod.PUT);

        } catch (Exception e) {
            logger.error(e.getMessage());
            //logger.error(e.getMessage());
        }
        logger.info("Update call for eliza client cofiguration ends");
        return resultMap;
    }

    /**
     * function to convert base64 string to file and get aws url
     **/
    public String base64toFileconversion(String base64) {
        logger.info("begin base64 to file conversion");
        Map responseMap = new HashMap();
        String url = "";
        File outputfile = null;
        try {
            if (!base64.isEmpty()) {
                byte[] imageByte = null;
                //BASE64Decoder decoder = new BASE64Decoder();
                int index = base64.indexOf("base64");
                String name = base64.substring(index + 7);
                logger.info("name{} ", name);

                //imageByte = decoder.decodeBuffer(name);
                imageByte = Base64Utils.decode(name.getBytes());
                logger.info("imageByte{} ", imageByte.toString());

                ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
                logger.info("bis{} ", bis.toString());
                BufferedImage image = ImageIO.read(bis);
                bis.close();
                //logger.info("image{} ", image.toString());


                outputfile = new File("logo.png");
                ImageIO.write(image, "png", outputfile);
                logger.info("outputfile{} ", outputfile);

                // Creating the directory to store file
                //logger.info("Creating the directory to store file");
                File dir = ContextLoader.getCurrentWebApplicationContext().getResource("/loader/").getFile();
                if (!dir.exists())
                    dir.mkdirs();

                File serverFile = ContextLoader.getCurrentWebApplicationContext().getResource("/loader/" + outputfile).getFile();
                //logger.info("*********** {}", serverFile.getAbsolutePath());
                BufferedOutputStream stream = new BufferedOutputStream(
                        new FileOutputStream(serverFile));
                stream.write(imageByte);
                stream.close();
            }

            MultiValueMap<String, Object> body
                    = new LinkedMultiValueMap<>();
            File fileName = ContextLoader.getCurrentWebApplicationContext().getResource("/loader/" + outputfile.toString()).getFile();
            //logger.info("fileName{} ", fileName);
            body.add("picture", new FileSystemResource(fileName));

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);

            HttpEntity<MultiValueMap<String, Object>> requestEntity
                    = new HttpEntity<>(body, headers);

            ResponseEntity<Object> response = restTemplate.postForEntity(storageUrl, requestEntity, Object.class);

            if (Utils.isNotNull(response) && response.getStatusCode() == HttpStatus.OK) {
                Files.deleteIfExists(Paths.get(fileName.getPath()));
                if (Utils.isNotNull(response.getBody())) {
                    String responseString = response.getBody().toString();
                    if (responseString.contains("true")) {
                        int startIndex = responseString.indexOf("=") + 1;
                        int endIndex = responseString.indexOf(",");
                        String extractedUrl = responseString.substring(startIndex, endIndex);
                        responseMap.put("success", true);
                        url = extractedUrl;

                    } else {
                        responseMap.put("success", false);
                    }


                }
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            url = Constants.DEFAULT_LOGO;
            responseMap.put("success", false);
        }
        logger.info("end base 64 to file conversion");
        return url;
    }

    /**
     * Update client king user information
     *
     * @param userUUID
     * @param userBean
     * @return
     */
    /*public Map updateKingUser(String userUUID, DtUserBean userBean) {
        logger.debug("Update client business king user begins");
        Map resultMap = new HashMap();
        try {
            final String uri =  dtAppUrl+""+CLIENT+"/"+USER+"/"+userUUID;
            logger.info("uri {}" ,uri);

            //convert bean to Map
            Map inputMap = mapper.convertValue(userBean, new TypeReference<Map>() {
            });

            resultMap = callRestApi(inputMap, uri, HttpMethod.PUT);

        } catch (Exception e) {
            logger.error(e.getMessage());
            //logger.error(e.getMessage());
        }
        logger.debug("Update client business king user ends");
        return resultMap;
    }*/
    public Map updateKingUser(String userUUID, int userId, String userData) {
        logger.info("Update client business king user begins");
        Map resultMap = new HashMap();
        try {
            final String uri = dtAppUrl + "" + CLIENT + "/" + USER + "/" + userUUID;
            logger.info("uri {}", uri);

            //convert bean to Map
            //Map inputMap = mapper.convertValue(userBean, new TypeReference<Map>() {
            //});

            JSONObject jsonObject = new JSONObject(userData);

            Map inputMap = new HashMap();
            inputMap.put("fullName", jsonObject.get("fullName"));
            inputMap.put("userEmail", jsonObject.get("userEmail"));
            inputMap.put("phone", jsonObject.get("phone"));
            inputMap.put("title", jsonObject.get("title"));

            //HashMap inputMap = mapper.readValue(userData, HashMap.class);
            //May not be working

            inputMap.put("userId", userId);
            resultMap = callRestApi(inputMap, uri, HttpMethod.PUT);

        } catch (Exception e) {
            logger.error("Error in updating king user : " + e.getMessage());
            //logger.error(e.getMessage());
        }
        logger.info("Update client business king user ends");
        return resultMap;
    }


    /**
     * Get user_id by useruuid
     *
     * @param userUUID
     * @return
     */
    public int getUserByUserUUID(String userUUID) {
        Integer result = 0;
        try {
            String SELECT_ID_BY_USER_UUID = "select user_id from users where user_uuid = ?";

            result = jdbcTemplate.queryForObject(SELECT_ID_BY_USER_UUID, new Object[]{userUUID}, Integer.class);
        } catch (Exception e) {
            logger.info("user id not found for the given user uuid");
        }
        return result;
    }

    /**
     * Modify client king user information
     *
     * @param businessUUID
     * @param userBean
     * @return
     */
    /*public Map modifyKingUser(String businessUUID, DtUserBean userBean) {
        logger.debug("Update client business king user begins");
        Map resultMap = new HashMap();
        try {
            final String uri =  dtAppUrl+""+CLIENT+"/"+businessUUID+"/"+MKING;
            logger.info("uri {}" ,uri);

            //convert bean to Map
            Map inputMap = mapper.convertValue(userBean, new TypeReference<Map>() {
            });

            resultMap = callRestApi(inputMap, uri, HttpMethod.PUT);

        } catch (Exception e) {
            logger.error(e.getMessage());
            //logger.error(e.getMessage());
        }
        logger.debug("Update client business king user ends");
        return resultMap;
    }*/
    public Map modifyKingUser(String businessUUID, String userData) {
        logger.debug("Update client business king user begins");
        Map resultMap = new HashMap();
        try {
            final String uri = dtAppUrl + "" + CLIENT + "/" + businessUUID + "/" + MKING;
            logger.info("uri {}", uri);

            //convert bean to Map
            //Map inputMap = mapper.convertValue(userBean, new TypeReference<Map>() {
            //});

            JSONObject jsonObject = new JSONObject(userData);
            //JSONObject jsonObject = mapper.readValue(userData, JSONObject.class); // Not working

            logger.info("json object {}", jsonObject);
            Map inputMap = new HashMap();
            inputMap.put("fullName", jsonObject.get("fullName"));
            inputMap.put("userEmail", jsonObject.get("userEmail"));
            inputMap.put("phone", jsonObject.get("phone"));
            inputMap.put("title", jsonObject.get("title"));
            inputMap.put("modifyUserUUID", jsonObject.get("modifyUserUUID"));

            logger.info("input map {}", jsonObject.toString());

            //HashMap inputMap = mapper.readValue(userData, HashMap.class);

            resultMap = callRestApi(inputMap, uri, HttpMethod.PUT);

        } catch (Exception e) {
            logger.error(e.getMessage());
            //logger.error(e.getMessage());
        }
        logger.debug("Update client business king user ends");
        return resultMap;
    }

    /**
     * Get businessuuid based on businessid
     *
     * @param businessId
     * @return
     */
    public String getBusinessUUIDByID(int businessId) {
        return clientDAO.retrieveBusinessUUIDByID(businessId);
    }

    /**
     * Get business id by business config uuid
     *
     * @param configUUID
     * @return
     */
    public int retrieveBusinessIdByConfigUUID(String configUUID) {
        return clientDAO.retrieveBusinessIdByConfigUUID(configUUID);
    }

    /**
     * Delete client by client id
     *
     * @param businessUUID
     * @return
     */
    public Map deleteClientById(String businessUUID) {
        logger.debug("Delete client business begins");
        Map resultMap = new HashMap();
        try {
            final String uri = dtAppUrl + "" + CLIENT + "/" + businessUUID;
            logger.info("uri {}", uri);
            resultMap = callRestApi(null, uri, HttpMethod.DELETE);

        } catch (Exception e) {
            logger.error(e.getMessage());
            //logger.error(e.getMessage());
        }
        logger.debug("Delete client business ends");
        return resultMap;
    }

    /**
     * Get client usage by clientId from API calculation
     *
     * @param businessUUID
     * @return
     */
    public Map getClientUsageByClientId(String businessUUID, String source) {
        logger.debug("Create client business begins");
        Map resultMap = new HashMap();
        try {
            //uri = https://{env}/client/usage/{businessUUID}
            final StringBuilder uriBuilder = new StringBuilder(dtAppUrl)
                    .append(CLIENT)
                    .append("/usage/")
                    .append(businessUUID)
                    .append("?source=")
                    .append(source);
            logger.info("uri {}", uriBuilder);

            Map response = callRestApi(uriBuilder.toString(), HttpMethod.GET);
            if ((boolean) response.get("success")) {
                resultMap = (response.containsKey("result") && Utils.isNotNull(response.get("result"))) ? (Map) response.get("result") : new HashMap();
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            //logger.error(e.getMessage());
        }
        logger.debug("Create client business ends");
        return resultMap;
    }


    /**
     * call rest api
     *
     * @param urlStr
     * @return
     */
    public Map callRestApi(String urlStr, HttpMethod method) {
        Map respMap = new HashMap();
        // rest api header
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        // rest template instance
        try {
            // rest api call for insights url
            ResponseEntity<Object> responseEntity = restTemplate.exchange(urlStr, method, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info("dtapp response received successfully");
                    logger.info("dtapp response received successfully  " + responseEntity.getBody());
                    respMap = (Map) responseEntity.getBody();
                }
            }
        } catch (ResourceAccessException | HttpClientErrorException ex) {
            logger.error("dtapp service refused to connect");
            respMap.put("success", false);
            respMap.put("message", "Connection refused");
        } catch (Exception e) {
            logger.error("Exception at callRestApi. Msg {} ", e.getMessage());
            respMap.put("success", false);
            respMap.put("message", "Exception occured");
        }
        return respMap;
    }


    /**
     * Used to create default business config
     *
     * @param businessUUID
     * @return
     */
    public Map getUserAndBusinessToCreateBusinessConfig(String businessUUID) {
        Map configMap = new HashMap();
        try {
            Map clientMap = clientDAO.retrieveBusinessByUUID(businessUUID);
            int businessId = (clientMap.containsKey(Constants.BUSINESS_ID) && Utils.isStringNumber(clientMap.get(Constants.BUSINESS_ID).toString())) ? (int) clientMap.get(Constants.BUSINESS_ID) : -1;
            configMap.put("businessId", businessId);
            if (businessId > 9) {
                Map kingUserMap = getKingUserByBusinessId(businessId);
                int userId = (kingUserMap.containsKey("user_id") && Utils.isStringNumber(kingUserMap.get("user_id").toString())) ? (int) kingUserMap.get("user_id") : 0;
                configMap.put("userId", userId);
            }
            //getKingUserByBusinessId()
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return configMap;
    }

    /**
     * Return survey list for business
     *
     * @param businessUUID
     * @param pageId          (should be -1 to retrieve all surveys)
     * @param searchTerm
     * @param sort
     * @param state           (0 - drafts, 3 - active, 4 - expired, 5 - Scheduled, 6 - Inactive,  -1 - all surveys)
     * @param timeZone
     * @param requestLanguage
     * @param exactMatch
     * @return
     */
    public List getSurveysList(String businessUUID, int pageId, String searchTerm, String sort, String state, String timeZone, String requestLanguage, int exactMatch) {
        List surveyList = new ArrayList();
        try {
            String newSearch = searchTerm;//URLDecoder.decode(StringEscapeUtils.unescapeJava(searchTerm), StandardCharsets.UTF_8.toString());
            int numericValueOfState = Integer.parseInt(state);
            List surveysList = new ArrayList();

            int businessId = this.retrieveBusinessIDByUUID(businessUUID);

            String businessUUIDHyphenReplaced = businessUUID.replace("-", "_");
            String surveyTableName = "surveys_" + businessUUIDHyphenReplaced;
            String active = Constants.ACTIVE_STATUS;

            int upperLimit = 0;
            int lowerLimit = 0;
            if (pageId > 1) {
                upperLimit = (pageId - 1) * 12;
            }
            lowerLimit = upperLimit + 12;
            logger.debug("upper limit " + upperLimit);
            logger.debug("lower limit " + lowerLimit);
            boolean retrieveUsersForRegularUser = true;
            String surveySql = "";
            String searchStr = (exactMatch == 1) ? "[\"" + newSearch.toUpperCase() + "\"]" : "%" + newSearch.toUpperCase() + "%";
            logger.info("searchStr " + searchStr);

            if (retrieveUsersForRegularUser) {
                logger.debug("Roles admin");
                if (Utils.emptyString(searchTerm)) {
                    logger.debug("search term empty retrieve all the surveys");
                    logger.info("search term empty retrieve all the surveys");
                    if (numericValueOfState > -1) {
                        if (pageId > -1) {
                            logger.info("Need to retrieve surveys based upon state and page id");
                            surveySql = (sort.equals("created_time")) ? "select * from surveys_%s where state = ? and active = ? order by created_time desc, id desc limit ?, 12" : "select * from surveys_%s where state = ? and active = ? order by modified_time desc, id desc limit ?, 12";
                            surveySql = String.format(surveySql, (businessUUID.replaceAll("-", "_")));
                            surveysList = jdbcTemplate.queryForList(surveySql, new Object[]{state, active, upperLimit});
                        } else {
                            logger.info("Need to retrieve surveys based upon state");
                            surveySql = (sort.equals("created_time")) ? "select * from surveys_%s where state = ? and active = ? order by created_time desc, id desc" : "select * from surveys_%s where state = ? and active = ? order by modified_time desc, id desc";
                            surveySql = String.format(surveySql, (businessUUID.replaceAll("-", "_")));
                            surveysList = jdbcTemplate.queryForList(surveySql, new Object[]{state, active});
                        }

                    } else {
                        logger.info("Need to retrieve all created surveys");
                        if (pageId > -1) {
                            surveySql = (sort.equals("created_time")) ? "select * from surveys_%s where active = ? order by created_time desc, id desc limit ?, 12" : "select * from surveys_%s where active = ?  order by modified_time desc, id desc limit ?, 12";
                            surveySql = String.format(surveySql, (businessUUID.replaceAll("-", "_")));
                            surveysList = jdbcTemplate.queryForList(surveySql, new Object[]{active, upperLimit});
                        } else {
                            surveySql = (sort.equals("created_time")) ? "select * from surveys_%s where active = ? order by created_time desc, id desc limit 1000" : "select * from surveys_%s where active = ? order by modified_time desc, id desc limit 1000";
                            surveySql = String.format(surveySql, (businessUUID.replaceAll("-", "_")));
                            surveysList = jdbcTemplate.queryForList(surveySql, new Object[]{active});
                        }

                    }
                    logger.info("sql  " + surveySql);
                } else {
                    logger.info("search term not empty need to search the surveys");
                    if (numericValueOfState > -1) {
                        logger.info("Need to search surveys based upon state");
                        if (pageId > -1) {
                            if (sort.equals("created_time")) {
                                surveySql = (exactMatch == 1) ? "select * from surveys_%s where upper(json_extract(survey_names,'$[*]')) = ? and state = ? and active = ? order by created_time desc, id desc limit ?,12" : "select * from surveys_%s where upper(json_extract(survey_names,'$[*]')) like ? and state = ? and active = ? order by created_time desc, id desc limit ?,12";
                                surveySql = String.format(surveySql, (businessUUID.replaceAll("-", "_")));
                                surveysList = jdbcTemplate.queryForList(surveySql, new Object[]{searchStr, state, active, upperLimit});
                            } else if (sort.equals("modified_time")) {
                                surveySql = (exactMatch == 1) ? "select * from surveys_%s where upper(json_extract(survey_names,'$[*]')) = ? and state = ? and active = ? order by modified_time desc, id desc limit ?,12" : "select * from surveys_%s where upper(json_extract(survey_names,'$[*]')) like ? and state = ? and active = ? order by modified_time desc, id desc limit ?,12";
                                surveySql = String.format(surveySql, (businessUUID.replaceAll("-", "_")));
                                surveysList = jdbcTemplate.queryForList(surveySql, new Object[]{searchStr, state, active, upperLimit});
                            }
                        } else {
                            if (sort.equals("created_time")) {
                                surveySql = (exactMatch == 1) ? "select * from surveys_%s where upper(json_extract(survey_names,'$[*]')) = ? and state = ? and active = ? order by created_time desc, id desc" : "select * from surveys_%s where upper(json_extract(survey_names,'$[*]')) like ? and state = ? and active = ? order by created_time desc, id desc";
                                surveySql = String.format(surveySql, (businessUUID.replaceAll("-", "_")));
                                surveysList = jdbcTemplate.queryForList(surveySql, new Object[]{searchStr, state, active});
                            } else if (sort.equals("modified_time")) {
                                surveySql = (exactMatch == 1) ? "select * from surveys_%s where upper(json_extract(survey_names,'$[*]')) = ? and state = ? and active = ? order by modified_time desc, id desc" : "select * from surveys_%s where upper(json_extract(survey_names,'$[*]')) like ? and state = ? and active = ? order by modified_time desc, id desc";
                                surveySql = String.format(surveySql, (businessUUID.replaceAll("-", "_")));
                                surveysList = jdbcTemplate.queryForList(surveySql, new Object[]{searchStr, state, active});
                            }
                        }

                    } else {
                        logger.info("Need to search all surveys");
                        if (pageId > -1) {
                            if (sort.equals("created_time")) {
                                surveySql = (exactMatch == 1) ? "select * from surveys_%s where upper(json_extract(survey_names,'$[*]')) = ? and active = ? order by created_time desc, id desc limit ?,12" : "select * from surveys_%s where upper(json_extract(survey_names,'$[*]')) like ? and active = ? order by created_time desc, id desc limit ?,12";
                                surveySql = String.format(surveySql, (businessUUID.replaceAll("-", "_")));
                                surveysList = jdbcTemplate.queryForList(surveySql, new Object[]{searchStr, active, upperLimit});
                            } else if (sort.equals("modified_time")) {
                                surveySql = (exactMatch == 1) ? "select * from surveys_%s where upper(json_extract(survey_names,'$[*]')) = ? and active = ? order by modified_time desc, id desc limit ?,12" : "select * from surveys_%s where upper(json_extract(survey_names,'$[*]')) like ? and active = ? order by modified_time desc, id desc limit ?,12";
                                surveySql = String.format(surveySql, (businessUUID.replaceAll("-", "_")));
                                surveysList = jdbcTemplate.queryForList(surveySql, new Object[]{searchStr, active, upperLimit});
                            }
                        } else {
                            if (sort.equals("created_time")) {
                                surveySql = (exactMatch == 1) ? "select * from surveys_%s where upper(json_extract(survey_names,'$[*]')) = ? and active = ? order by created_time desc, id desc limit 1000" : "select * from surveys_%s where upper(json_extract(survey_names,'$[*]')) like ? and active = ? order by created_time desc, id desc limit 1000";
                                surveySql = String.format(surveySql, (businessUUID.replaceAll("-", "_")));
                                surveysList = jdbcTemplate.queryForList(surveySql, new Object[]{searchStr, active});
                            } else if (sort.equals("modified_time")) {
                                surveySql = (exactMatch == 1) ? "select * from surveys_%s where upper(json_extract(survey_names,'$[*]')) = ? and active = ? order by modified_time desc, id desc limit 1000" : "select * from surveys_%s where upper(json_extract(survey_names,'$[*]')) like ? and active = ? order by modified_time desc, id desc limit 1000";
                                surveySql = String.format(surveySql, (businessUUID.replaceAll("-", "_")));
                                surveysList = jdbcTemplate.queryForList(surveySql, new Object[]{searchStr, active});
                            }
                        }

                    }
                }
                logger.info("Sql main {}", surveySql);
            }

            Iterator<Map> surveysIterator = surveysList.iterator();
            while (surveysIterator.hasNext()) {
                Map eachSurveyMap = surveysIterator.next();
                Map eachSurveyCardMap = new HashMap();
                Map surveyMetaDataMap = new HashMap();
                int surveyId = (int) eachSurveyMap.get("id");
                String surveyUUID = eachSurveyMap.get("survey_uuid").toString();
                List languages = mapper.readValue(eachSurveyMap.get("languages").toString(), ArrayList.class);
                int languageIndex = (languages.indexOf(requestLanguage) > -1) ? languages.indexOf(requestLanguage) : 0;
                List multiSurveys = mapper.readValue(eachSurveyMap.get("multi_surveys").toString(), ArrayList.class);
                surveyMetaDataMap = (Map) multiSurveys.get(languageIndex);
                //comment the startdate and endddate paret from survey_schedule as the dates can be fetched from survey table itself
               /* String survey_schedule_by_survey_id="select * from survey_schedule where survey_id = ? and business_id = ?";
                List surveySchedules = jdbcTemplate.queryForList(survey_schedule_by_survey_id, new Object[]{surveyId, businessId});
                Map eachSurveySchedule = new HashMap();
                if (surveySchedules.size() > 0) {
                    eachSurveySchedule = (Map) surveySchedules.get(surveySchedules.size()-1);
                }*/
                String createdTime = (Utils.isNotNull(eachSurveyMap.get("created_time")) && eachSurveyMap.get("created_time").toString().length() > 0) ? eachSurveyMap.get("created_time").toString() : eachSurveyMap.get("created_time").toString();
//                String startDate = (Utils.isNotNull(eachSurveySchedule.get("from_date")) && eachSurveySchedule.get("from_date").toString().length()>0) ? eachSurveySchedule.get("from_date").toString() : eachSurveyMap.get("from_date").toString();
//                String endDate = (Utils.isNotNull(eachSurveySchedule.get("to_date")) && eachSurveySchedule.get("to_date").toString().length()>0) ? eachSurveySchedule.get("to_date").toString() : eachSurveyMap.get("to_date").toString();

                String startDate = (Utils.isNotNull(eachSurveyMap.get("from_date")) && eachSurveyMap.get("from_date").toString().length() > 0) ? eachSurveyMap.get("from_date").toString() : eachSurveyMap.get("from_date").toString();
                String endDate = (Utils.isNotNull(eachSurveyMap.get("to_date")) && eachSurveyMap.get("to_date").toString().length() > 0) ? eachSurveyMap.get("to_date").toString() : eachSurveyMap.get("to_date").toString();
                eachSurveyCardMap.put("languages", languages);
                eachSurveyCardMap.put("surveyId", surveyId);
                eachSurveyCardMap.put("surveyUUID", surveyUUID);
                String programName = (surveyMetaDataMap.containsKey("surveyName") && Utils.isNotNull(surveyMetaDataMap.get("surveyName"))) ? surveyMetaDataMap.get("surveyName").toString() : "";
                eachSurveyCardMap.put("programName", programName);
                eachSurveyCardMap.put("startDate", Utils.getDateAsStringByTimezone(startDate, timeZone));
                eachSurveyCardMap.put("endDate", Utils.getDateAsStringByTimezone(endDate, timeZone));
                eachSurveyCardMap.put("createdTime", Utils.getDateAsStringByTimezone(createdTime, timeZone));


                int surveyState = (int) eachSurveyMap.get("state");
                String status = "Draft";

                if (surveyState == 0) {
                    status = Constants.DRAFT_STATE;
                } else if (surveyState == 5) {
                    status = Constants.SCHEDULED_STATE;
                } else if (surveyState == 3) {
                    status = Constants.ACTIVE_STATE;
                } else if (surveyState == 4) {
                    status = Constants.EXPIRED_STATE;
                } else if (surveyState == 6) {
                    status = Constants.INACTIVE_STATE;
                }
                eachSurveyCardMap.put("status", status);
                surveyList.add(eachSurveyCardMap);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return surveyList;

    }

    /**
     * send password reset email for cs master user
     *
     * @param businessUUID
     * @return
     */
    public Map sendPasswordResetEmail(String businessUUID) {
        logger.info("begin password reset email");
        Map resultMap = new HashMap();
        try {
            Map businessMap = clientDAO.retrieveBusinessByUUID(businessUUID);
            if (businessMap.containsKey(Constants.BUSINESS_ID)) {
                int businessId = (int) businessMap.get(Constants.BUSINESS_ID);
                List csUsers = userService.getCSUsersByBusinessId(businessId);
                if (csUsers.size() > 0) {
                    Map csUser = (Map) csUsers.get(0);
                    String userEmail = (csUser.containsKey("user_email") && Utils.notEmptyString(csUser.get("user_email").toString())) ? csUser.get("user_email").toString() : "";
                    logger.info("user email {}", userEmail);

                    if (Utils.notEmptyString(userEmail)) {
                        final String uri = dtAppUrl + "password/reset";
                        logger.info("uri {}", uri);
                        Map inputMap = new HashMap();
                        inputMap.put("token", userEmail);
                        inputMap.put("language", "en");
                        resultMap = callRestApi(inputMap, uri, HttpMethod.POST);

                        if (resultMap.containsKey("success")) {
                            if ((boolean) resultMap.get("success")) {
                                resultMap.put("success", true);
                                resultMap.put("message", "Password reset request sent to cs master user");
                                resultMap.remove("result");
                            } else {
                                resultMap.put("success", false);
                                resultMap.put("error", "Password reset request failed");
                            }
                        } else {
                            resultMap.put("success", false);
                            resultMap.put("error", "Password reset request failed");
                        }

                    }


                } else {
                    resultMap.put("success", false);
                    resultMap.put("error", "CS user not found");
                }

            } else {
                resultMap.put("success", false);
                resultMap.put("error", "business not found");
            }


        } catch (Exception e) {
            logger.error(e.getMessage());
            resultMap.put("success", false);
            resultMap.put("error", e.getMessage());
        }
        logger.info("end password reset email");
        return resultMap;
    }

    /**
     * @param templateName
     * @param templateType
     * @param sortString
     * @param pageNo
     * @param exactMatch
     * @param timezone
     * @param theme
     * @param industry
     * @return
     */
    public Map searchTemplateV2(String templateName, String templateType, String sortString, int pageNo, boolean exactMatch, String timezone, String theme, String industry) {
        logger.debug("Update client business begins");
        Map resultMap = new HashMap();
        try {
            //search=a&templateType=all&sortString=createdTime&pageNo=-1&exactMatch=false&language=en&timezone=America%2FLos_Angeles&theme=a&industry=a
            String uri = dtAppUrl + "" + TEMPLATESUMMARY + "?search=" + templateName + "&templateType=" + templateType + "&sortString=" + sortString + "&pageNo=" + pageNo + "&exactMatch=" + exactMatch + "&timezone=" + timezone + "&theme=" + theme + "&industry=" + industry;


            logger.info("uri {}", uri);
            resultMap = callRestApi(uri, HttpMethod.GET);

        } catch (Exception e) {
            logger.error(e.getMessage());
            //logger.error(e.getMessage());
        }
        logger.debug("Update client business ends");
        return resultMap;
    }

    public Map updateTemplates(String businessUUID, TemplateBean dtSurveyBean) {
        logger.debug("Update templates begins");
        Map resultMap = new HashMap();
        try {
            //uri = https://{env}/client/usage/{businessUUID}
            final String uri = dtAppUrl + "" + SURVEYTEMPLATE;
            logger.info("uri {}", uri);
            //program template bean
            Map inputMap = mapper.convertValue(dtSurveyBean, new TypeReference<Map>() {
            });
            Map response = callRestApi(inputMap, uri, HttpMethod.PUT);
            if ((boolean) response.get("success")) {
                resultMap = (response.containsKey("result") && Utils.isNotNull(response.get("result"))) ? (Map) response.get("result") : new HashMap();
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            //logger.error(e.getMessage());
        }
        logger.debug("update templates ends");
        return resultMap;
    }

    public Map getTemplatesByType(String templateType, String language, String timezone, int pageId) {
        logger.debug("Update client business begins");
        Map resultMap = new HashMap();
        try {
            //search=a&templateType=all&sortString=createdTime&pageNo=-1&exactMatch=false&language=en&timezone=America%2FLos_Angeles&theme=a&industry=a
            String uri = dtAppUrl + "" + TEMPLATESUMMARY + "?&templateType=" + templateType + "&pageNo=" + pageId + "&timezone=" + timezone + "&language=" + language;

            logger.info("uri {}", uri);
            resultMap = callRestApi(uri, HttpMethod.GET);

        } catch (Exception e) {
            logger.error(e.getMessage());
            //logger.error(e.getMessage());
        }
        logger.debug("Update client business ends");
        return resultMap;
    }

    public Map updateTemplateByBusiness(String businessUUID, TemplateUpdateBean templateUpdateBean) {
        Map returnMap = new HashMap();
        try {
            String businessIdSql = "select business_id from business where business_uuid = ?";
            String businessId = jdbcTemplate.queryForObject(businessIdSql, new Object[]{businessUUID}, String.class);
            String tableName = TEMPLATE_TABLE;
            logger.info("template update bean = " + templateUpdateBean);
            List templates = templateUpdateBean.getTemplate();
            logger.info("templates value = " + templates);
            Iterator itrTemplates = templates.iterator();
            while (itrTemplates.hasNext()) {
                Map eachTemplate = (Map) itrTemplates.next();
                String modeVal = eachTemplate.containsKey("mode") ? eachTemplate.get("mode").toString() : "";
                String templateVal = eachTemplate.containsKey("id") ? eachTemplate.get("id").toString() : "";
                String getBusinessSql = "select * from templates where template_uuid = ?";
                try {
                    Map templateMap = jdbcTemplate.queryForMap(getBusinessSql, new Object[]{templateVal});
                    logger.info("templateMap = " + templateMap);
                    List existingBusinessIds = Utils.isNotNull(templateMap.get("business_ids")) ? mapper.readValue(templateMap.get("business_ids").toString(), ArrayList.class) : new ArrayList();
                    logger.info("existing businessIds = " + existingBusinessIds);
                    if (modeVal.equals("1") && !existingBusinessIds.contains(businessId)) {
                        existingBusinessIds.add(businessId);
                    } else if (modeVal.equals("0")) {
                        if (existingBusinessIds.contains(businessId)) {
                            existingBusinessIds.remove(businessId);
                        } else {
                            logger.info("template not mapped to businessId {}" + businessId);
                        }
                    }
                    String updateBusinessSql = "update templates set business_ids = ? where template_uuid = ?";
                    int result = jdbcTemplate.update(updateBusinessSql, new Object[]{new JSONArray(existingBusinessIds).toString(), templateVal});
                } catch (Exception e) {
                    logger.error(e.getMessage());
                    logger.info("invalid templateid value");
                }
            }

            returnMap.put("success", true);
            returnMap.put("message", "templates updated successfully");
        } catch (Exception e) {
            returnMap.put("success", false);
            returnMap.put("message", "Exception occured");
            logger.error(e.getMessage());
        }
        return returnMap;
    }

    public Map getTemplatesById(String businessUUID, String templateUUID, String type, String language, String timezone) {
        logger.debug("Update client business begins");
        Map resultMap = new HashMap();
        try {
            //search=a&templateType=all&sortString=createdTime&pageNo=-1&exactMatch=false&language=en&timezone=America%2FLos_Angeles&theme=a&industry=a
            String uri = dtAppUrl + "" + SURVEYTEMPLATE + "/" + templateUUID + "?type=" + type + "&language=" + language + "&timezone=" + timezone;
            logger.info("uri {}", uri);
            resultMap = callRestApi(uri, HttpMethod.GET);

        } catch (Exception e) {
            logger.error(e.getMessage());
            //logger.error(e.getMessage());
        }
        logger.debug("Update client business ends");
        return resultMap;
    }

    /**
     * @param templateType
     * @param sortString
     * @param pageNo
     * @param exactMatch
     * @param timezone
     * @param theme
     * @param industry
     * @return
     */
    public List searchAdvancedTemplateV2(String businessUUID, String searchTerm, String templateType, String sortString, int pageNo, boolean exactMatch, String timezone, String theme, String industry, String advanced, String language) {
        logger.info("searchAdvancedTemplateV2 data api starts");
        List resultList = new ArrayList();
        try {
            //advanced templates are part of dropthought templates
            //by default templateType will be dropthought
            templateType = "dropthought";

            Map tempMap = new HashMap();

            int totalRecords = 0;
            int currentPageRecords = 0;
            int length = 12;
            int upperLimit = 0;

            StringBuilder templateSql = new StringBuilder();
            StringBuilder defaultTemplateSql = new StringBuilder();
            String tableName = "";

            int businessId = this.getBusinessIdByUUID(businessUUID);
            // assign table name based on template type
            if (!templateType.equalsIgnoreCase("dropthought")) {
                tableName = "templates_" + businessUUID.replace("-", "_");
            } else {
                tableName = "templates";
            }

            // get template type id based on its string
            int state = getTemplateTypeVal(templateType);

            // default query
            defaultTemplateSql.append(" select id, active, question_data, question_ids, languages, state, template_names, template_uuid, metrics_map, created_by, created_time, modified_time, theme, industry, count_questions, completion_time, template_type, business_ids, JSON_EXTRACT(template_names, '$[0]') AS names  from dt_template_table_dt where id > 0 "); //id > 0 added to avoid bad sql exception while appending AND in the query;

            if (state != 0) {
                /** template type: company / dropthought
                 * if template is personal get records based on userId esle get all
                 * */
                defaultTemplateSql.append(" and state = ").append(state);
            } else {
                /** template type: ALL
                 * when templateType=all(state=0), company and dropthought should list all templates and personal template should return based on the userID
                 */
                defaultTemplateSql.append(" and (state < 3) "); // in table state values: 1 - dropthought, 2 - company, 3 - personal
            }

            //for basic dropthought templates newly created ones --> template_type = 0,
            //                                    old ones --> template_type = null
            //if advanced == 1, list  advanced templates only
            if (Integer.parseInt(advanced) > -1) {
                defaultTemplateSql.append((Integer.parseInt(advanced) == 0) ? " and (template_type = 0 or template_type IS NULL) " : " and template_type = 1 ");
            }

            // add search condition if available
            if (Utils.notEmptyString(searchTerm)) {
                String searchStr = URLDecoder.decode(StringEscapeUtils.unescapeJava(searchTerm), StandardCharsets.UTF_8.toString());
                /**search template with isExactMatch true, returns exact matching result
                 search template with isExactMatch false, returns all likely match survey name */
                if (exactMatch)
                    defaultTemplateSql.append(" and json_search((template_names), 'all', '" + searchStr + "') is not null ");
                else
                    defaultTemplateSql.append(" and json_search(upper(template_names), 'all', '%" + searchStr.toUpperCase() + "%') is not null ");
            }

            // add Theme string if available
            if (Utils.notEmptyString(theme)) {
                String themeStr = URLDecoder.decode(StringEscapeUtils.unescapeJava(theme), StandardCharsets.UTF_8.toString());
                defaultTemplateSql.append(" and json_search(theme, 'all', '" + themeStr + "') is not null ");
            }
            // add Industry string if available
            if (Utils.notEmptyString(industry)) {
                String industryStr = URLDecoder.decode(StringEscapeUtils.unescapeJava(industry), StandardCharsets.UTF_8.toString());
                defaultTemplateSql.append(" and json_search(industry, 'all', '" + industryStr + "') is not null ");
            }

            if (state == 0) {
                /** template type: ALL
                 *  get records from personal, company and dropthought templates
                 */
                templateSql.append(defaultTemplateSql.toString().replace("dt_template_table_dt", "templates"));
                templateSql.append(" UNION ");
                templateSql.append(defaultTemplateSql.toString().replace("dt_template_table_dt", "templates_" + businessUUID.replace("-", "_")));
            } else {
                templateSql.append(defaultTemplateSql.toString().replace("dt_template_table_dt", tableName));
            }

            String intermediateQuery = templateSql.toString();

            // without pagination to get total count of matched records
            String totalSql = templateSql.toString();

            // pagination
            if (pageNo > 1) {
                upperLimit = (pageNo - 1) * length;
            } else {
                upperLimit = 0;
            }

            // sort by criteria
            List templateList = new ArrayList();
            String sortingOrder = " desc ";
            String sortBy = "";
            switch (sortString) {
                case "createdTime":
                    sortBy = "created_time";
                    // append sort by criteria at the end of query
                    templateSql.append(" order by ").append(sortBy).append(sortingOrder).append(", names asc ");

                    logger.info("template sql::  {}", templateSql);
                    logger.info("total sql::  {}", totalSql);

                    if (pageNo == -1) {
                        // query execution to get list of matched records with out pagination
                        templateList = jdbcTemplate.queryForList(templateSql.toString());
                    } else {
                        // query execution to get list of matched records with pagination
                        templateSql.append(" limit ?,? ");
                        templateList = jdbcTemplate.queryForList(templateSql.toString(), upperLimit, length);
                    }
                    break;

                case "modifiedTime":
                    sortBy = "modified_time";
                    // append sort by criteria at the end of query
                    templateSql.append(" order by ").append(sortBy).append(sortingOrder).append(", template_names asc ");

                    logger.info("template sql::  {}", templateSql);
                    logger.info("total sql::  {}", totalSql);

                    if (pageNo == -1) {
                        // query execution to get list of matched records with out pagination
                        templateList = jdbcTemplate.queryForList(templateSql.toString());
                    } else {
                        // query execution to get list of matched records with pagination
                        templateSql.append(" limit ?,? ");
                        templateList = jdbcTemplate.queryForList(templateSql.toString(), upperLimit, length);
                    }
                    break;

                case "questions":
                    List templateQuestion = jdbcTemplate.queryForList(intermediateQuery);
                    Iterator itr = templateQuestion.iterator();
                    Map responseMap = new HashMap();
                    while (itr.hasNext()) {
                        try {
                            Map templateMap = (Map) itr.next();
                            String templateUUID = templateMap.get("template_uuid").toString();
                            List langList = mapper.readValue(templateMap.get("languages").toString(), ArrayList.class);
                            List tempNames = mapper.readValue(templateMap.get("template_names").toString(), ArrayList.class);
                            int index = 0;
                            if (langList.contains(language)) {
                                index = langList.indexOf(language);
                            }
                            String templateName = tempNames.get(index).toString();
                            int questionCount = (int) templateMap.get("count_questions");
                            responseMap.put(templateName + "~_~" + templateUUID, questionCount);
                        } catch (Exception e) {
                            logger.error(e.getMessage());
                        }
                    }
                    Map<String, Integer> templateQuestionMap = sortByValues(responseMap);
                    List templateSort = new LinkedList(templateQuestionMap.keySet());
                    //Collections.reverse(templateSort);
                    templateSql.append(" order by field(template_uuid, :templateSort) ");
                    MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
                    mapSqlParameterSource.addValue("templateSort", templateSort);

                    logger.info("template sql::  {}", templateSql);
                    logger.info("total sql::  {}", totalSql);

                    if (pageNo == -1) {
                        // query execution to get list of matched records with out pagination
                        templateList = namedParameterJdbcTemplate.queryForList(templateSql.toString(), mapSqlParameterSource);
                    } else {
                        // query execution to get list of matched records with pagination
                        templateSql.append(" limit " + upperLimit + ", " + length + " ");
                        templateList = namedParameterJdbcTemplate.queryForList(templateSql.toString(), mapSqlParameterSource);
                    }
                    break;

                case "completionTime":
                    List templateTime = jdbcTemplate.queryForList(intermediateQuery);
                    Iterator iterator = templateTime.iterator();
                    Map resultMap = new HashMap();
                    while (iterator.hasNext()) {
                        try {
                            Map templateMap = (Map) iterator.next();
                            String templateUUID = templateMap.get("template_uuid").toString();
                            List langList = mapper.readValue(templateMap.get("languages").toString(), ArrayList.class);
                            List tempNames = mapper.readValue(templateMap.get("template_names").toString(), ArrayList.class);
                            int index = 0;
                            if (langList.contains(language)) {
                                index = langList.indexOf(language);
                            }
                            String templateName = tempNames.get(index).toString();
                            String avgTime = templateMap.get("completion_time").toString();
                            avgTime = String.valueOf(avgTime.charAt(0));
                            avgTime = avgTime.equals(">") ? "9" : avgTime;
                            int completionTime = Integer.parseInt(avgTime);
                            resultMap.put(templateName + "~_~" + templateUUID, completionTime);
                        } catch (Exception e) {
                            logger.error(e.getMessage());
                        }
                    }
                    Map<String, Integer> templateTimeMap = sortByValues(resultMap);
                    List templateTimeSort = new LinkedList(templateTimeMap.keySet());

                    templateSql.append(" order by field(template_uuid, :templateSort) ");
                    MapSqlParameterSource mapSqlParameterSourceTime = new MapSqlParameterSource();
                    mapSqlParameterSourceTime.addValue("templateSort", templateTimeSort);

                    logger.info("template sql::  {}", templateSql);
                    logger.info("total sql::  {}", totalSql);

                    if (pageNo == -1) {
                        // query execution to get list of matched records with out pagination
                        templateList = namedParameterJdbcTemplate.queryForList(templateSql.toString(), mapSqlParameterSourceTime);
                    } else {
                        // query execution to get list of matched records with pagination
                        templateSql.append(" limit " + upperLimit + ", " + length + " ");
                        templateList = namedParameterJdbcTemplate.queryForList(templateSql.toString(), mapSqlParameterSourceTime);
                    }
                    break;

                //DTV-4720, Advanced templates-Cards are not listed alphabetically from A to Z
                case "templateName":
                    // append sort by criteria at the end of query
                    templateSql.append(" order by names");

                    logger.info("template sql::  {}", templateSql);
                    logger.info("total sql::  {}", totalSql);

                    if (pageNo == -1) {
                        // query execution to get list of matched records with out pagination
                        templateList = jdbcTemplate.queryForList(templateSql.toString());
                    } else {
                        // query execution to get list of matched records with pagination
                        templateSql.append(" limit ?,? ");
                        templateList = jdbcTemplate.queryForList(templateSql.toString(), upperLimit, length);
                    }
                    break;
            }

            logger.info("templateSql  " + templateSql.toString());
            logger.info("templateList  " + templateList);
            if (Utils.isNotNull(templateList)) {
                resultList = returnTemplateDataV2(templateList, language, businessId);
            }


        } catch (Exception e) {
            logger.error(e.getMessage());
            //logger.error(e.getMessage());
        }
        logger.info("searchAdvancedTemplateV2 data api ends");
        return resultList;
    }

    /**
     * method to get template type
     *
     * @param type
     * @return
     */
    public int getTemplateTypeVal(String type) {
        int templateType = -1;
        if (Utils.isNull(type)) return 1; //if not given set state to default
        switch (type) {
            case "dropthought":
                templateType = 1;
                break;
            case "company":
                templateType = 2;
                break;
            case "personal":
                templateType = 3;
                break;
            case "all":
                templateType = 0;
        }
        return templateType;
    }

    /**
     * @param unsortMap
     * @return
     */
    private Map<String, Object> sortByValues(Map<String, Object> unsortMap) {

        // 1. Convert Map to List of Map
        List<Map.Entry<String, Object>> list = new LinkedList<Map.Entry<String, Object>>(unsortMap.entrySet());

        // 2. Sort list with Collections.sort(), provide a custom Comparator
        Collections.sort(list, new Comparator<Map.Entry<String, Object>>() {
            public int compare(Map.Entry<String, Object> o1,
                               Map.Entry<String, Object> o2) {
                // return (o2.getValue()).compareTo(o1.getValue());
                int cmp1 = 0;
                if (o1.getValue() instanceof Double) {
                    cmp1 = ((Double) o1.getValue()).compareTo((Double) o2.getValue());
                } else if (o1.getValue() instanceof Long) {
                    cmp1 = ((Long) o1.getValue()).compareTo((Long) o2.getValue());
                } else if (o1.getValue() instanceof Integer) {
                    cmp1 = ((Integer) o1.getValue()).compareTo((Integer) o2.getValue());
                }

                if (cmp1 != 0)
                    return cmp1;
                else
                    return o1.getKey().compareToIgnoreCase(o2.getKey());
            }
        });

        // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
        Map<String, Object> sortedMap = new LinkedHashMap<String, Object>();
        for (Map.Entry<String, Object> entry : list) {
            String[] parsedKeyArr = entry.getKey().split("~_~");
            String paresedKey = parsedKeyArr[1];
            sortedMap.put(paresedKey, entry.getValue());
        }
        logger.debug("sortedMap {}", sortedMap);

        return sortedMap;
    }

    /**
     * Method to return the template data.
     *
     * @param templatesList
     * @param language
     * @return
     */
    public List returnTemplateDataV2(List templatesList, String language, int businessId) {
        List resultDataList = new ArrayList();
        try {
            Iterator itr = templatesList.iterator();
            while (itr.hasNext()) {
                Map eachMap = (Map) itr.next();
                eachMap.put("questionsCount", eachMap.get("count_questions"));
                eachMap.put("averageCompletionTime", Utils.isNotNull(eachMap.get("completion_time")) ? eachMap.get("completion_time").toString() + " m" : "");
                eachMap.remove("question_ids");
                eachMap.remove("question_data");
                int temp = (int) eachMap.get("active");
                Object languageList = eachMap.remove("languages");
                eachMap.put("languages", new JSONArray(languageList.toString()));
                Object template_uuid = eachMap.remove("template_uuid");
                eachMap.put("templateUUID", template_uuid);
                eachMap.put("language", language);
                List langList = mapper.readValue(eachMap.get("languages").toString(), ArrayList.class);
                List tempNames = mapper.readValue(eachMap.get("template_names").toString(), ArrayList.class);
                if (langList.contains(language)) {
                    int index = langList.indexOf(language);
                    eachMap.put("templateName", tempNames.get(index));
                } else {
                    eachMap.put("templateName", tempNames.get(0));
                }
                int createdBy = (int) eachMap.get("created_by");
                if (createdBy == 0) {
                    eachMap.put("createdBy", "dropthought");
                }

                eachMap.remove("template_names");
                eachMap.remove("created_by");
                if (temp == 1) {
                    eachMap.put("status", "active");
                    eachMap.put("active", "active");
                } else {
                    eachMap.put("status", "inactive");
                    eachMap.put("active", "inactive");
                }
                int stateNum = (int) eachMap.get("state");
                if (stateNum == 1) {
                    eachMap.put("state", "dropthought");
                } else if (stateNum == 2) {
                    eachMap.put("state", "company");
                } else if (stateNum == 3) {
                    eachMap.put("state", "personal");
                }
                Object createdTimeObject = eachMap.remove("created_time");
                eachMap.put("createdTime", createdTimeObject);

                Object templateId = eachMap.remove("id");
                List businessIdsList = Utils.isNotNull(eachMap.get("business_ids")) ? mapper.readValue(eachMap.get("business_ids").toString(), ArrayList.class) : new ArrayList();

                if (businessIdsList.contains(businessId + "")) {
                    eachMap.put("isEnabled", true);
                } else {
                    eachMap.put("isEnabled", false);
                }
                int templateType = Utils.isNotNull(eachMap.get("template_type")) && eachMap.containsKey("template_type") ? Integer.parseInt(eachMap.get("template_type").toString()) : 0;

                //if template is advanced fetch triggers count for that template.
                if (templateType == 1) {
                    List triggersList = this.getTriggersByTemplateId((Integer) templateId);
                    eachMap.put("triggersCount", triggersList.size());
                    eachMap.put("templateType", "advanced");
                } else {
                    eachMap.put("templateType", "basic");
                }

                Map metricsMap = Utils.isNotNull(eachMap.get("metrics_map")) && eachMap.containsKey("metrics_map") ? mapper.readValue(eachMap.get("metrics_map").toString(), HashMap.class) : new HashMap();
                eachMap.put("metricsCount", metricsMap.size());

                if (Utils.isNull(eachMap.get("theme"))) {
                    eachMap.replace("theme", new ArrayList<>());
                }

                if (Utils.isNull(eachMap.get("industry"))) {
                    eachMap.replace("industry", new ArrayList<>());
                }


                eachMap.remove("modified_time");
                eachMap.remove("count_questions");
                eachMap.remove("completion_time");
                eachMap.remove("metrics_map");
                eachMap.remove("business_ids");
                eachMap.remove("template_type");
                resultDataList.add(eachMap);
                logger.info(" each map   " + eachMap);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return resultDataList;
    }

    /**
     * method to get template type
     *
     * @param templateId
     * @return
     */
    public List getTriggersByTemplateId(int templateId) {
        List result = new ArrayList();
        try {
            if (templateId > 0) {
                String surveySql = "select id, trigger_name, trigger_uuid, template_id, template_name, trigger_condition, status, frequency from trigger_templates where template_id = ?";
                result = jdbcTemplate.queryForList(surveySql, new Object[]{templateId});
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return result;
    }

    /**
     * get api key by clientUUID
     *
     * @param businessUUID
     * @return
     */
    public String getApiKeyByClientId(String businessUUID) {
        String apiKey = "";
        try {
            List clients = this.getClientByClientId(businessUUID, "UTC");
            if (clients.size() > 0) {
                Map eachClient = (Map) clients.get(0);
                int businessId = (eachClient.containsKey("businessId")) ? (int) eachClient.get("businessId") : 0;
                if (businessId > 0) {
                    String apiKeySQL = "select business_id, api_key, created_time, expiration_time, enabled from api_key where business_id = ? and enabled = ?";
                    List apiKeys = jdbcTemplate.queryForList(apiKeySQL, new Object[]{businessId, 1});
                    if (apiKeys.size() > 0) {
                        Map eachApiKey = (Map) apiKeys.get(0);
                        apiKey = eachApiKey.get("api_key").toString();
                    }
                }

            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return apiKey;
    }

    /**
     * Get all Client business details
     *
     * @return
     */
    public List<Map<String, Object>> getAllClientV2(String timezone, String searchTerm, String planName, int pageId) {

        List<Map<String, Object>> result = new ArrayList();
        List<Map<String, Object>> kingList = new ArrayList();
        String GET_ALL_BUSINESS = "SELECT b.business_id as businessId, business_name as businessName, business_timezone as businessTimezone, business_email as businessEmail, b.created_time as createdTime, b.modified_time as modifiedTime, business_uuid as businessUUID, industry, ifnull(contract_start_date,'') as contractStartDate, ifnull(contract_end_date, '') as contractEndDate, reason, state, " +
                " cs_access as csAccess, business_logo AS businessLogo, business_theme AS businessTheme, business_ratings AS businessRatings, ifnull(business_email_alert, '') AS notification, b.plan_config_id as planConfigId from business b left join business_global_settings bgs on b.business_id = bgs.business_id ";

       /* String GET_ALL_BUSINESS = "SELECT b.business_id as businessId, b.business_name as businessName, b.business_timezone as businessTimezone, " +
                "b.business_email as businessEmail, b.created_time as createdTime, b.modified_time as modifiedTime, b.business_uuid as businessUUID, " +
                "b.industry, ifnull(b.contract_start_date,'') as contractStartDate, ifnull(b.contract_end_date, '') as contractEndDate, reason, state, " +
                "cs_access as csAccess, bgs.business_logo AS businessLogo, bgs.business_theme AS businessTheme, bgs.business_ratings AS businessRatings, " +
                "ifnull(u.country_id, '')as countryId, ifnull(u.full_name, '') as kingName, ifnull(u.user_email, '') as kingEmail, " +
                "ifnull(u.user_uuid, '') as kingUserUUID ,  ifnull(a.account_emp_range, '') as employeesRange, ifnull(u.title, '') as kingTitle, " +
                " ifnull(u.phone, '') as kingMobile, ifnull(u.thoughtful, '1') as thoughtful, ifnull(u.eula_status, '') as eula_status, " +
                "  ifnull(u.cs_start_date,'') as csStartDate, ifnull(u.cs_end_date,'') as csEndDate, " +
                "  ifnull(u.last_login_time, '') as lastLoginTime, " +
                "ifnull(business_email_alert, '') AS notification from business b " +
                "left join business_global_settings bgs on b.business_id = bgs.business_id " +
                "left join account as a on a.account_business_id = b.business_id " +
                "left join users as u on u.user_id = a.account_user_id " +
                "WHERE u.user_status = '1' and u.user_role = '1' ";*/
        timezone = Utils.isNotNull(timezone) && Utils.notEmptyString(timezone) ? timezone : "America/Los_Angeles";
        //DTV-11025
        int planConfigId = getPlandIdByPlanName(planName);

        if (Utils.notEmptyString(searchTerm) && planConfigId > 0) {
            GET_ALL_BUSINESS += "where (b.business_name LIKE '%" + searchTerm + "%' OR b.business_email LIKE '%" + searchTerm + "%') " +
                    " and b.plan_config_id = " + planConfigId;
        } else if (Utils.notEmptyString(searchTerm) && planConfigId == 0) {
            GET_ALL_BUSINESS += "where (b.business_name LIKE '%" + searchTerm + "%' OR b.business_email LIKE '%" + searchTerm + "%') ";
        } else if (planConfigId > 0 && Utils.emptyString(searchTerm)) {
            GET_ALL_BUSINESS += "where b.plan_config_id = " + planConfigId;
        }

        int upperLimit = 0;
        int length = 10;
        boolean pageExists = false;
        if (pageId > -1) {
            upperLimit = (pageId - 1) * length;
            GET_ALL_BUSINESS += " limit ?, ?";
            pageExists = true;
        }


        List<Map<String, Object>> businessDataAll = jdbcTemplate.queryForList(GET_ALL_BUSINESS, pageExists ? new Object[]{upperLimit, length} : new Object[]{});
        if (!businessDataAll.isEmpty()) {
            logger.info("total business count {}", businessDataAll.size());
            List<Integer> businessIds = businessDataAll.stream()
                    .map(map -> (Integer) map.get("businessId")) // replace "your_key" with the actual key name
                    .collect(Collectors.toList());
            kingList = this.getKingUserInfoByBusinessIds(businessIds, timezone);
            //Get all plans config & Ids from planConfiguration table
            Map planConfigMap = planService.getAllPlanIdAndNameMap();
            Iterator<Map<String, Object>> businessAllIterator = businessDataAll.iterator();
            while (businessAllIterator.hasNext()) {
                try {
                    Map businessMap = businessAllIterator.next();
                    //King user details
                    int businessId = (int) businessMap.get("businessId");
                    this.getBusinessLogoUrl(businessMap);
                    /*Map kingUserMap = this.getKingUserInfoByBusinessId(businessId, timezone, searchTerm);
                    businessMap.putAll(kingUserMap);*/
                    Optional<Map<String, Object>> eachKingUserMap = kingList.stream()
                            .filter(map -> map.containsKey("account_business_id") && (int) map.get("account_business_id") == businessId)
                            .findFirst();
                    if (eachKingUserMap.isPresent()) {
                        Map<String, Object> eachMap = eachKingUserMap.get();
                        businessMap.putAll(eachMap);
                    }
                    this.getClientInfoDatesAndPlanConfig(businessMap, timezone, planConfigMap);
                    result.add(businessMap);

                } catch (Exception e) {
                    logger.info("Error getting business");
                }
            }
            //Temporary fix to add additional data which satisfies serchTerm in user and account table
            if (Utils.notEmptyString(searchTerm)) {

                String businessSql = "SELECT b.business_id as businessId, business_name as businessName, business_timezone as businessTimezone, business_email as businessEmail, b.created_time as createdTime, b.modified_time as modifiedTime, business_uuid as businessUUID, industry, ifnull(contract_start_date,'') as contractStartDate, ifnull(contract_end_date, '') as contractEndDate, reason, state, " +
                        " cs_access as csAccess, business_logo AS businessLogo, business_theme AS businessTheme, business_ratings AS businessRatings, ifnull(business_email_alert, '') AS notification from business b left join business_global_settings bgs on b.business_id = bgs.business_id WHERE b.business_id = ?";

                List<Map<String, Object>> kingUserList = this.getKingUserInfo(timezone, searchTerm);
                try {
                    for (Map<String, Object> kingUserMap : kingUserList) {

                        int businessId = (int) kingUserMap.get("account_business_id");
                        String eachKingMail = kingUserMap.get("kingEmail").toString();
                        boolean isKingUserThereInBusinessList = result.stream()
                                .anyMatch(map -> map.containsKey("kingEmail") && map.get("kingEmail").equals(eachKingMail));
                        if (!isKingUserThereInBusinessList) {

                            Map<String, Object> businessMap = jdbcTemplate.queryForMap(businessSql, businessId);
                            this.getBusinessLogoUrl(businessMap);
                            businessMap.putAll(kingUserMap);
                            this.getClientInfoDatesAndPlanConfig(businessMap, timezone, planConfigMap);
                            result.add(businessMap);
                        }

                    }
                } catch (Exception e) {
                    logger.info("Error getting business");
                }
            }
        }
        return result;
    }

    /**
     * This function is used to get plan config id by plan name
     *
     * @param planName
     * @return
     */
    private int getPlandIdByPlanName(String planName) {
        int planId = 0;
        try {
            if (Utils.notEmptyString(planName)) {
                String planNameStr = '%' + planName + '%';
                String planIdSql = "SELECT plan_config_id FROM plan_configurations WHERE plan_name like ?";
                planId = jdbcTemplate.queryForObject(planIdSql, new Object[]{planNameStr}, Integer.class);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return planId;
    }

    /**
     * This function is used to retrive king user details by the given business Ids
     *
     * @param businessIds
     * @param timezone
     * @return
     */
    private List<Map<String, Object>> getKingUserInfoByBusinessIds(List<Integer> businessIds, String timezone) {

        logger.debug("begin getKingUsers");
        List<Map<String, Object>> kingUserList = new ArrayList<>();
        try {
            String kingSql = "select ifnull(u.country_id, '')as countryId, ifnull(u.full_name, '') as kingName, " +
                    "ifnull(u.user_email, '') as kingEmail, ifnull(u.user_uuid, '') as kingUserUUID ,  ifnull(a.account_emp_range, '') as employeesRange, " +
                    "ifnull(u.title, '') as kingTitle, ifnull(u.phone, '') as kingMobile, ifnull(u.thoughtful, '1') as thoughtful, " +
                    "ifnull(u.eula_status, '') as eula_status, ifnull(u.cs_start_date,'') as csStartDate, ifnull(u.cs_end_date,'') as csEndDate, " +
                    "ifnull(u.last_login_time, '') as lastLoginTime, a.account_business_id as account_business_id  from users as u  " +
                    "left join account as a on u.user_id = a.account_user_id where u.user_status = '1' and u.user_role = '1' " +
                    " and a.account_business_id IN (:businessIds)";

            MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("businessIds", businessIds);
            List<Map<String, Object>> resultList = namedParameterJdbcTemplate.queryForList(
                    kingSql, mapSqlParameterSource);

            if (resultList.size() > 0) {

                for (Map<String, Object> kingUserMap : resultList) {

                    String userTimeZoneDate = "";
                    String userTimeCsStartDate = "";
                    String userTimeCsEndDate = "";

                    String csStartDate = kingUserMap.get("csStartDate").toString();
                    if (Utils.notEmptyString(csStartDate)) {
                        userTimeCsStartDate = Utils.getDateAsStringByTimezone(csStartDate, timezone);
                    }
                    String csEndDate = kingUserMap.get("csEndDate").toString();
                    if (Utils.notEmptyString(csEndDate)) {
                        userTimeCsEndDate = Utils.getDateAsStringByTimezone(csEndDate, timezone);
                    }
                    String lastLoginTime = kingUserMap.get("lastLoginTime").toString();
                    if (Utils.notEmptyString(lastLoginTime)) {
                        userTimeZoneDate = Utils.getDateAsStringByTimezone(lastLoginTime, timezone);
                    }
                    kingUserMap.put("csStartDate", Utils.notEmptyString(userTimeCsStartDate) ? userTimeCsStartDate : csStartDate);
                    kingUserMap.put("csEndDate", Utils.notEmptyString(userTimeCsEndDate) ? userTimeCsEndDate : csEndDate);
                    kingUserMap.put("lastLoginTime", Utils.notEmptyString(userTimeZoneDate) ? userTimeZoneDate : lastLoginTime);

                    kingUserList.add(kingUserMap);
                }

            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.debug("end getKingUserInfoByBusinessId");

        return kingUserList;
    }

    /**
     * get business logo url
     *
     * @param businessMap
     * @return
     */
    public String getBusinessLogoUrl(Map businessMap) {
        String logo = "";
        try {
            //busienss logo
            Object businessLogo = businessMap.get("businessLogo");
            if (Utils.isNotNull(businessLogo)) {
                logo = businessLogo.toString();
                byte[] logoBytes = (byte[]) businessLogo;
                String logoUrl = new String(logoBytes);
                if (logoUrl.contains("https")) {
                    logger.info("aws url");
                    logo = logoUrl;
                } else if (Utils.notEmptyString(logoUrl)) {
                    logger.info("base 64 to aws url");
                    logo = this.base64toFileconversion(logo);
                } else {
                    logger.info("default logo");
                    logo = Constants.DEFAULT_LOGO;
                }
            } else {
                logger.info("using the default logo");
                logo = Constants.DEFAULT_LOGO;
            }
            businessMap.put("businessLogo", logo);
            // business theme
            Object businessTheme = businessMap.get("businessTheme");
            if (Utils.isNull(businessTheme) || Utils.emptyString(businessTheme.toString()))
                businessMap.put("businessTheme", Constants.DEFAULT_THEME);

        } catch (Exception e) {
            logger.error("exception at getBusinessLogoUrl. Msg {}", e.getMessage());
        }
        return logo;
    }

    /**
     * Function to get king user based on businessId
     *
     * @param businessId
     * @return
     */
    public Map getKingUserInfoByBusinessId(Integer businessId, String timezone, String searchTerm) {
        logger.debug("begin getKingUserInfoByBusinessId");
        Map kingUserMap = new HashMap();
        try {
            String userTimeZoneDate = "";
            String userTimeCsStartDate = "";
            String userTimeCsEndDate = "";

            String SELECT_KING_USER_BY_BUSINESS_ID = "select ifnull(u.country_id, '')as countryId, ifnull(u.full_name, '') as kingName, ifnull(u.user_email, '') as kingEmail, ifnull(u.user_uuid, '') as kingUserUUID ,  ifnull(a.account_emp_range, '') as employeesRange, ifnull(u.title, '') as kingTitle, ifnull(u.phone, '') as kingMobile, ifnull(u.thoughtful, '1') as thoughtful, ifnull(u.eula_status, '') as eula_status, ifnull(u.cs_start_date,'') as csStartDate, ifnull(u.cs_end_date,'') as csEndDate, ifnull(u.last_login_time, '') as lastLoginTime  from users as u  left join account as a on u.user_id = a.account_user_id where user_status = '1' and user_role = '1' and account_business_id = ?";
            if (Utils.notEmptyString(searchTerm)) {
                SELECT_KING_USER_BY_BUSINESS_ID += " AND ( u.full_name LIKE'%" + searchTerm + "%' OR u.user_email LIKE '%" + searchTerm + "%')";
            }

            List resultList = jdbcTemplate.queryForList(SELECT_KING_USER_BY_BUSINESS_ID, new Object[]{businessId});
            if (resultList.size() > 0) {
                kingUserMap = (Map) resultList.get(0);

                String csStartDate = kingUserMap.get("csStartDate").toString();
                if (Utils.notEmptyString(csStartDate)) {
                    userTimeCsStartDate = Utils.getDateAsStringByTimezone(csStartDate, timezone);
                }
                String csEndDate = kingUserMap.get("csEndDate").toString();
                if (Utils.notEmptyString(csEndDate)) {
                    userTimeCsEndDate = Utils.getDateAsStringByTimezone(csEndDate, timezone);
                }
                String lastLoginTime = kingUserMap.get("lastLoginTime").toString();
                if (Utils.notEmptyString(lastLoginTime)) {
                    userTimeZoneDate = Utils.getDateAsStringByTimezone(lastLoginTime, timezone);
                }
                kingUserMap.put("csStartDate", Utils.notEmptyString(userTimeCsStartDate) ? userTimeCsStartDate : csStartDate);
                kingUserMap.put("csEndDate", Utils.notEmptyString(userTimeCsEndDate) ? userTimeCsEndDate : csEndDate);
                kingUserMap.put("lastLoginTime", Utils.notEmptyString(userTimeZoneDate) ? userTimeZoneDate : lastLoginTime);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.debug("end getKingUserInfoByBusinessId");
        return kingUserMap;
    }

    /**
     * Function to get king user based on businessId
     *
     * @param timezone
     * @param searchTerm
     * @return
     */
    public List<Map<String, Object>> getKingUserInfo(String timezone, String searchTerm) {
        logger.debug("begin getKingUser");
        List<Map<String, Object>> kingUserList = new ArrayList<>();
        try {


            String SELECT_KING_USER_BY_BUSINESS_ID = "select ifnull(u.country_id, '')as countryId, ifnull(u.full_name, '') as kingName, " +
                    "ifnull(u.user_email, '') as kingEmail, ifnull(u.user_uuid, '') as kingUserUUID ,  ifnull(a.account_emp_range, '') as employeesRange, " +
                    "ifnull(u.title, '') as kingTitle, ifnull(u.phone, '') as kingMobile, ifnull(u.thoughtful, '1') as thoughtful, " +
                    "ifnull(u.eula_status, '') as eula_status, ifnull(u.cs_start_date,'') as csStartDate, ifnull(u.cs_end_date,'') as csEndDate, " +
                    "ifnull(u.last_login_time, '') as lastLoginTime, a.account_business_id as account_business_id  from users as u  " +
                    "left join account as a on u.user_id = a.account_user_id where user_status = '1' and user_role = '1' ";

            if (Utils.notEmptyString(searchTerm)) {
                SELECT_KING_USER_BY_BUSINESS_ID += " AND ( u.full_name LIKE'%" + searchTerm + "%' OR u.user_email LIKE '%" + searchTerm + "%')";
            }

            List<Map<String, Object>> resultList = jdbcTemplate.queryForList(SELECT_KING_USER_BY_BUSINESS_ID);
            if (resultList.size() > 0) {

                for (Map<String, Object> kingUserMap : resultList) {

                    String userTimeZoneDate = "";
                    String userTimeCsStartDate = "";
                    String userTimeCsEndDate = "";

                    String csStartDate = kingUserMap.get("csStartDate").toString();
                    if (Utils.notEmptyString(csStartDate)) {
                        userTimeCsStartDate = Utils.getDateAsStringByTimezone(csStartDate, timezone);
                    }
                    String csEndDate = kingUserMap.get("csEndDate").toString();
                    if (Utils.notEmptyString(csEndDate)) {
                        userTimeCsEndDate = Utils.getDateAsStringByTimezone(csEndDate, timezone);
                    }
                    String lastLoginTime = kingUserMap.get("lastLoginTime").toString();
                    if (Utils.notEmptyString(lastLoginTime)) {
                        userTimeZoneDate = Utils.getDateAsStringByTimezone(lastLoginTime, timezone);
                    }
                    kingUserMap.put("csStartDate", Utils.notEmptyString(userTimeCsStartDate) ? userTimeCsStartDate : csStartDate);
                    kingUserMap.put("csEndDate", Utils.notEmptyString(userTimeCsEndDate) ? userTimeCsEndDate : csEndDate);
                    kingUserMap.put("lastLoginTime", Utils.notEmptyString(userTimeZoneDate) ? userTimeZoneDate : lastLoginTime);

                    kingUserList.add(kingUserMap);
                }

            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.debug("end getKingUserInfoByBusinessId");
        return kingUserList;
    }

    /**
     * Function to return information about a particular business client by client Id
     *
     * @param businessMap
     * @param planConfigMap
     * @return
     */
    public Map getClientInfoDatesAndPlanConfig(Map businessMap, String timezone, Map<Integer, String> planConfigMap) {
        logger.debug("begin getClientInfoDates");
        try {

            // business details
            String contractStartDate = businessMap.get("contractStartDate").toString(); //null check added in query
            String csContractStartDate = Utils.notEmptyString(contractStartDate) ? Utils.getDateAsStringByTimezone(contractStartDate, timezone) : "";
            businessMap.put("contractStartDate", csContractStartDate);

            String contractEndDate = businessMap.get("contractEndDate").toString(); //null check added in query
            String csContractEndDate = Utils.notEmptyString(contractEndDate) ? Utils.getDateAsStringByTimezone(contractEndDate, timezone) : "";
            String contractEndDateModified = Utils.convertToEndOfDay(csContractEndDate);
            businessMap.put("contractEndDate", contractEndDateModified);

            String currentDateTime = Utils.getCurrentUTCDateAsString();
            if (Utils.notEmptyString(contractEndDateModified) && Utils.compareDates(currentDateTime, contractEndDateModified)) {
                logger.debug("Contract end date is expired");
                businessMap.put("kingUserStatus", Constants.KING_INACTIVE_STATE);
            }

            int csAccessState = Utils.isNotNull(businessMap.get("csAccess")) ? Integer.parseInt(businessMap.get("csAccess").toString()) : 0;
            String csStartDate = businessMap.get("csStartDate").toString();
            String csEndDate = businessMap.get("csEndDate").toString();
            String statusStr = "";
            switch (csAccessState) {
                case 0:
                    if (Utils.notEmptyString(csEndDate) && Utils.compareDates(currentDateTime, csEndDate)) {
                        statusStr = "Access period ended";
                    } else {
                        statusStr = "Default";
                    }
                    break;
                case 1:
                    statusStr = "Request Sent";
                    break;
                case 2:
                    statusStr = "Access Granted";
                    break;
                case 3:
                    statusStr = "Access Revoked";
                    break;
            }
            businessMap.put("status", statusStr);

            String contractDur = "";
            if (Utils.notEmptyString(csStartDate) && Utils.notEmptyString(csEndDate)) {
                if (csStartDate.substring(0, 10).equals(contractStartDate.substring(0, 10)) && csEndDate.substring(0, 10).equals(contractEndDate.substring(0, 10))) {
                    contractDur = "contract";
                } else {
                    contractDur = "custom";
                }
            }
            businessMap.put("contractDuration", contractDur);

            String planName = "";
            int planConfigId = 0;
            if (Utils.isNotNull(businessMap.get("planConfigId")) && Utils.notEmptyString(businessMap.get("planConfigId").toString()) &&
                    Integer.parseInt(businessMap.get("planConfigId").toString()) > 0) {
                planConfigId = (int) businessMap.get("planConfigId");
                planName = planConfigMap.getOrDefault(planConfigId, Constants.EMPTY_STRING);
            }
            businessMap.put("planName", planName);
            businessMap.put("planConfigId", planConfigId);

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.debug("end getClientInfoDates");
        return businessMap;
    }

    /**
     * Method to get limits of text analytics by client id
     *
     * @param businessUUID
     * @return
     */
    public String getClientUsageByClientId(String businessUUID, String tap, String mlModelId) {
        String returnStr = "";
        Map respMap = new HashMap();
        try {
            //GET https://stage-api.dropthought.com/eliza/monkeylearn/usage/api/v1/fb0a129f-df09-4166-9781-8ebcf27a94a6
            //https://stage-api.dropthought.com/eliza/monkeylearn/limit/api/v1
            String urlToCall = monkeyLearnUsageUrl + businessUUID;
            Map inputMap = new HashMap();
            if (Utils.notEmptyString(tap)) {
                inputMap.put("tap", tap);
            }
            if (Utils.notEmptyString(mlModelId)) {
                inputMap.put("mlModelId", mlModelId);
            }
            JSONObject json = new JSONObject(inputMap);
            String inputJson = json.toString();
            logger.info(inputJson);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);
            logger.info("urltocall = " + urlToCall);

            ResponseEntity<Object> responseEntity = restTemplate.exchange(urlToCall, HttpMethod.GET, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info("response = ", responseEntity.getBody().toString());
                    respMap = (Map) responseEntity.getBody();
                }
            }

            String totalConsumed = "";
            String basicConsumed = "";
            String advancedConsumed = "";
            logger.info("respmap = " + respMap);
            List summary = (List) respMap.get("summary");
            logger.info("summary valueeee = " + respMap.get("summary"));
            logger.info("summary = " + summary);
            Iterator itr = summary.iterator();
            while (itr.hasNext()) {
                Map eachMap = (Map) itr.next();
                totalConsumed = eachMap.get("totalConsumed").toString();
                basicConsumed = eachMap.get("totalBasicConsumed").toString();
                advancedConsumed = eachMap.get("totalAdvancedConsumed").toString();
                logger.info("totalConsumed = " + totalConsumed);
                logger.info("advancedConsumed = " + advancedConsumed);
                logger.info("basicConsumed = " + basicConsumed);
            }
            if (Utils.notEmptyString(tap) && tap.equalsIgnoreCase("basic")) {
                returnStr = basicConsumed;
            } else if (Utils.notEmptyString(tap) && tap.equalsIgnoreCase("advanced")) {
                returnStr = advancedConsumed;
            } else {
                returnStr = totalConsumed;
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return returnStr;
    }

    public Map getClientLimitsByClientId(String businessUUID, String tap, String mlModelId) {
        List returnList = new ArrayList();
        Map respMap = new HashMap();
        try {
            // https://stage-api.dropthought.com/eliza/monkeylearn/limit/api/v1
            String urlToCall = monkeyLearnLimitUrl;
            Map inputMap = new HashMap();
            JSONObject json = new JSONObject(inputMap);
            String inputJson = json.toString();
            logger.info(inputJson);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);
            logger.info("urltocall = " + urlToCall);
            List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
//Add the Jackson Message converter
            MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
            // Note: here we are making this converter to process any kind of response,
            // not only application/*json, which is the default behaviour
            converter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));
            messageConverters.add(converter);
            restTemplate.setMessageConverters(messageConverters);
            ResponseEntity<Object> responseEntity = restTemplate.exchange(urlToCall, HttpMethod.GET, entity, Object.class);
            //restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
            //ResponseEntity<Object> responseEntity1 = restTemplate.getForEntity(urlToCall,Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info("response = ", responseEntity.getBody());
                    respMap = (Map) responseEntity.getBody();
                }
            }

            logger.info("respMap = " + respMap);

//            if (responseEntity1 != null && responseEntity1.getStatusCode() == HttpStatus.OK) {
//                if (responseEntity1.getBody() != null) {
//                    logger.info("response = ",responseEntity1.getBody().toString());
//                    respMap = (Map) responseEntity1.getBody();
//                }
//            }
//
//            logger.info("respmap = "+respMap);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return respMap;
    }


    public void testBeanConversion(String data) {
        logger.info("begin bean conversion test");
        logger.info("data {}", data);

        try {
            ClientBean clientBean = mapper.readValue(data, ClientBean.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        logger.info("bean creation from read value ");

        try {
            ClientBean clientBean1 = mapper.convertValue(data, new TypeReference<ClientBean>() {
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("bean creation from convert  value ");


        try {

            JSONObject jsonObject = new JSONObject(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject jsonObject1 = mapper.readValue(data, JSONObject.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        logger.info("JSON object creation from string value ");

        try {
            JSONObject jsonObject2 = mapper.convertValue(data, new TypeReference<JSONObject>() {
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("JSON object creation from read value ");

        try {
            HashMap map1 = mapper.readValue(data, HashMap.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        logger.info("JSON object creation from convert value ");

        try {
            HashMap map2 = mapper.convertValue(data, new TypeReference<HashMap>() {
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        logger.info("Map creation from read value ");

        try {

        } catch (Exception e) {
            e.printStackTrace();
        }

        logger.info("Map creation from convert value ");


        logger.info("end bean conversion test");
    }

    /**
     * Return survey list for business - portal
     *
     * @param businessUUID
     * @return
     */
    public List getSurveysListForPortal(String businessUUID, String model) {
        List surveyList = new ArrayList();
        try {
            String modelCdn = model.equals("historical") ? " and response_count > 0 " : "";
            String surveySql = "select id, survey_uuid, response_count from surveys_%s where state in (3,4) and active = 1 " + modelCdn +
                    " order by created_time desc limit 1000";
            surveySql = String.format(surveySql, (businessUUID.replaceAll("-", "_")));
            List surveysList = jdbcTemplate.queryForList(surveySql, new Object[]{});

            Iterator<Map> surveysIterator = surveysList.iterator();
            while (surveysIterator.hasNext()) {
                Map eachSurveyMap = surveysIterator.next();
                Map eachSurveyCardMap = new HashMap();
                int surveyId = (int) eachSurveyMap.get("id");
                String surveyUUID = eachSurveyMap.get("survey_uuid").toString();
                int feedbackCnt = (int) eachSurveyMap.get("response_count");

                eachSurveyCardMap.put("surveyUUID", surveyUUID);
                eachSurveyCardMap.put("surveyId", surveyId);
                eachSurveyCardMap.put("feedbackCount", feedbackCnt);

                surveyList.add(eachSurveyCardMap);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("exception at getSurveysListForPortal" + e);
        }
        return surveyList;
    }

    /**
     * Return custom themes list
     *
     * @param businessUUID
     * @param searchTerm
     * @param sortString
     * @param pageNo
     * @param exactMatch
     * @param timezone
     * @param language
     * @return
     */
    public List getCustomThemesByBusinessId(String businessUUID, String searchTerm, String sortString, int pageNo, boolean exactMatch, String timezone, String language) {
        logger.info("getCustomThemes() data api starts");
        List resultList = new ArrayList();
        try {
            Map tempMap = new HashMap();

            int totalRecords = 0;
            int currentPageRecords = 0;
            int length = 12;
            int upperLimit = 0;

            StringBuilder defaultThemeSql = new StringBuilder();

            int businessId = this.getBusinessIdByUUID(businessUUID);
            String tableName = "custom_eux_themes";

            //default query
            defaultThemeSql.append(" select business_id, theme_name, theme_uuid, page_theme_style_code, page_theme_bg_img from " + tableName + " where id > 0 ");

            //add search condition if available
            if (Utils.notEmptyString(searchTerm)) {
                String searchStr = URLDecoder.decode(StringEscapeUtils.unescapeJava(searchTerm),
                        StandardCharsets.UTF_8.toString());
                if (exactMatch)
                    defaultThemeSql.append(" and theme_name = '" + searchStr + "' ");
                else
                    defaultThemeSql.append(" and theme_name like %" + searchStr.toUpperCase() + "%");
            }

            // pagination
            if (pageNo > 1) {
                upperLimit = (pageNo - 1) * length;
            } else {
                upperLimit = 0;
            }

            // sort by criteria
            List themeList = new ArrayList();
            String sortingOrder = " desc ";
            String sortBy = "";
            switch (sortString) {
                case "createdTime":
                    sortBy = "created_time";
                    // append sort by criteria at the end of query
                    defaultThemeSql.append(" order by ").append(sortBy).append(sortingOrder);

                    logger.info("themes sql::  {}", defaultThemeSql);

                    if (pageNo == -1) {
                        // query execution to get list of matched records with out pagination
                        themeList = jdbcTemplate.queryForList(defaultThemeSql.toString());
                    } else {
                        // query execution to get list of matched records with pagination
                        defaultThemeSql.append(" limit ?,? ");
                        themeList = jdbcTemplate.queryForList(defaultThemeSql.toString(), upperLimit, length);
                    }
                    break;

                case "modifiedTime":
                    sortBy = "modified_time";
                    // append sort by criteria at the end of query
                    defaultThemeSql.append(" order by ").append(sortBy).append(sortingOrder);

                    logger.info("themes sql::  {}", defaultThemeSql);

                    if (pageNo == -1) {
                        // query execution to get list of matched records with out pagination
                        themeList = jdbcTemplate.queryForList(defaultThemeSql.toString());
                    } else {
                        // query execution to get list of matched records with pagination
                        defaultThemeSql.append(" limit ?,? ");
                        themeList = jdbcTemplate.queryForList(defaultThemeSql.toString(), upperLimit, length);
                    }
                    break;

                case "themeName":
                    // append sort by criteria at the end of query
                    defaultThemeSql.append(" order by theme_name asc ");

                    logger.info("themes sql::  {}", defaultThemeSql);

                    if (pageNo == -1) {
                        // query execution to get list of matched records with out pagination
                        themeList = jdbcTemplate.queryForList(defaultThemeSql.toString());
                    } else {
                        // query execution to get list of matched records with pagination
                        defaultThemeSql.append(" limit ?,? ");
                        themeList = jdbcTemplate.queryForList(defaultThemeSql.toString(), upperLimit, length);
                    }
                    break;
            }

            logger.info("defaultThemeSql  " + defaultThemeSql.toString());

            if (Utils.isNotNull(themeList)) {
                resultList = returnThemeDataV2(themeList, language, businessId);
            }
        } catch (Exception e) {
            e.printStackTrace();
            //logger.error(e.getMessage());
        }
        logger.info("getCustomThemes() data api ends");
        return resultList;
    }

    /**
     * Method to return the theme data.
     *
     * @param templatesList
     * @param language
     * @return
     */
    public List returnThemeDataV2(List templatesList, String language, int businessId) {
        List resultDataList = new ArrayList();
        try {
            Iterator itr = templatesList.iterator();
            while (itr.hasNext()) {
                Map eachMap = (Map) itr.next();
                eachMap.put("id", eachMap.get("theme_uuid"));
                eachMap.put("themeName", eachMap.get("theme_name"));
                eachMap.put("pageThemeStyleCode",
                        Utils.isNotNull(eachMap.get("page_theme_style_code")) ? eachMap.get("page_theme_style_code")
                                .toString() : "");
                eachMap.put("pageThemeBgImg",
                        Utils.isNotNull(eachMap.get("page_theme_bg_img")) ? eachMap.get("page_theme_bg_img")
                                .toString() : "");

                List existingBusinessIds = eachMap.containsKey(Constants.BUSINESS_ID) ? mapper.readValue(eachMap.get(Constants.BUSINESS_ID).toString(), ArrayList.class) : new ArrayList();
                eachMap.put("isThemeApplied", existingBusinessIds.contains(businessId));

                eachMap.remove(Constants.BUSINESS_ID);
                eachMap.remove("theme_uuid");
                eachMap.remove("theme_name");
                eachMap.remove("page_theme_style_code");
                eachMap.remove("page_theme_bg_img");
                resultDataList.add(eachMap);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return resultDataList;
    }

    /**
     * Method to update the themes.
     *
     * @param businessUUID
     * @param themesBean
     * @return
     */
    public Map updateThemesBybusiness(String businessUUID, ThemesBean themesBean) {
        Map returnMap = new HashMap();
        try {
            String businessIdSql = "select business_id from business where business_uuid = ?";
            Integer businessId = jdbcTemplate.queryForObject(businessIdSql, new Object[]{businessUUID}, Integer.class);
            String tableName = "custom_eux_themes";
            List themes = themesBean.getThemes();
            Iterator itrThemes = themes.iterator();
            while (itrThemes.hasNext()) {
                Map eachTheme = (Map) itrThemes.next();
                String modeVal = eachTheme.containsKey("mode") ? eachTheme.get("mode").toString() : "";
                String themeVal = eachTheme.containsKey("id") ? eachTheme.get("id").toString() : "";
                String getThemeSql = "select * from custom_eux_themes where theme_uuid = ?";
                try {
                    Map themeMap = jdbcTemplate.queryForMap(getThemeSql, new Object[]{themeVal});
                    List existingBusinessIds = Utils.isNotNull(themeMap.get(Constants.BUSINESS_ID)) ? mapper.readValue(
                            themeMap.get(Constants.BUSINESS_ID).toString(),
                            ArrayList.class) : new ArrayList();
                    if (modeVal.equals("1") && !existingBusinessIds.contains(businessId)) {
                        existingBusinessIds.add(businessId);
                    } else if (modeVal.equals("0")) {
                        if (existingBusinessIds.contains(businessId)) {
                            existingBusinessIds.remove(businessId);
                        } else {
                            logger.info("template not mapped to businessId {}" + businessId);
                        }
                    }
                    String updateBusinessSql = "update custom_eux_themes set business_id = ? where theme_uuid = ?";
                    int result = jdbcTemplate.update(updateBusinessSql,
                            new Object[]{new JSONArray(existingBusinessIds).toString(), themeVal});
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.info("invalid theme id value");
                }
            }
            returnMap.put("success", true);
            returnMap.put("message", "themes updated successfully");
        } catch (Exception e) {
            returnMap = Utils.failureResponse("Exception occured");
            e.printStackTrace();
        }
        return returnMap;
    }

    public Map getDemoDetails() {
        Map demoMap = new HashMap();
        try {
            String sql = "select * from lkp_demo";
            List demoRecords = jdbcTemplate.queryForList(sql, new Object[]{});
            if (demoRecords.size() > 0) {
                Map demoRecord = (Map) demoRecords.get(0);
                String businessUUID = demoRecord.containsKey("business_uuid") ? demoRecord.get("business_uuid").toString() : "";
                String surveyUUID = demoRecord.containsKey("survey_uuid") ? demoRecord.get("survey_uuid").toString() : "";
                int businessId = getBusinessIdByUUID(businessUUID);
                Map apiKeyMap = businessConfigurationsService.getApiKeyByBusinessId(businessId);
                String apiKey = apiKeyMap.containsKey("api_key") ? apiKeyMap.get("api_key").toString() : "";
                demoMap.put("businessUUID", businessUUID);
                demoMap.put("businessId", businessId);
                demoMap.put("surveyUUID", surveyUUID);
                demoMap.put("apiKey", apiKey);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return demoMap;
    }

    /**
     * update api key if not available when enabled is 1
     *
     * @return
     */
    public Map updateAPIKeyIfNotAvailable() {
        logger.info("update  apiKey  in api_key table start");
        Map resultMap = new HashMap();
        resultMap.put("success", true);
        try {
            String updateSql = "update api_key set api_key = ? where business_id = ?";

            List businessIds = this.getAllBusinessWithoutAPIKey();
            if (businessIds.size() > 0) {
                int[] result = jdbcTemplate.batchUpdate(updateSql, new BatchPreparedStatementSetter() {
                    public void setValues(PreparedStatement statement, int i) throws SQLException {
                        final int businessId = (int) businessIds.get(i);
                        final String apiKey = "DT." + tokenUtility.generatealphaNumericRandomString(40);
                        statement.setString(1, apiKey);
                        statement.setInt(2, businessId);
                    }

                    public int getBatchSize() {
                        return businessIds.size();
                    }
                });
                resultMap.put("message", result.length > 0 ? "apikey updated successfully for " + result.length + " records." : "No business found to update.");
            } else {
                resultMap.put("message", "No business found to update.");
            }
            logger.info("end update  apiKey  in api_key table");
        } catch (Exception e) {
            e.printStackTrace();
            resultMap.put("success", false);
        }
        return resultMap;
    }

    /**
     * method to list of business with apikey enabled but empty apikey
     *
     * @return
     */
    public List getAllBusinessWithoutAPIKey() {
        try {
            String sql = "select json_arrayagg(business_id) businessIds from api_key where enabled = 1 and (api_key is null or api_key = '' ) and current_timestamp < expiration_time";
            List apikeyList = jdbcTemplate.queryForList(sql, new Object[]{});

            Iterator iterator = apikeyList.iterator();
            while (iterator.hasNext()) {
                Map eachApiKeyMap = (Map) iterator.next();
                String businessIdsStr = eachApiKeyMap.containsKey("businessIds") && eachApiKeyMap.get("businessIds") != null ?
                        eachApiKeyMap.get("businessIds").toString() : "";
                return Utils.notEmptyString(businessIdsStr) ? mapper.readValue(businessIdsStr,
                        ArrayList.class) : new ArrayList();
            }
        } catch (Exception e) {
            //e.printStackTrace();
            logger.error(e.getMessage());
        }
        return new ArrayList<>();
    }

    /**
     * Method to update business plan and sent email notification by businessId and accountType
     * If accountType is 'Enterprise', update business plan config id to 0 (Enterprise) and send email notification
     * If accountType is 'DTLite', update business plan config id to 3 (DT Lite), Verify if customer is already mapped for this business,
     *  if not then create a customer in stripe and insert the mapping entry in dtlite_business_customer_map and send email notification
     * @param businessUUID
     * @param accountType
     * @return
     */
    public Map<String, Object> changeAccount(String businessUUID, String accountType) {

        Map<String, Object> resultMap = new HashMap<>();
        boolean result = false;
        try {
            String businessSql = "select business_id, contract_end_date, plan_config_id from business where business_uuid = ?";
            Map businessMap = jdbcTemplate.queryForMap(businessSql, new Object[]{businessUUID});
            if (businessMap.isEmpty()) {
                logger.info("Business not found for businessUUID : " + businessUUID);
                resultMap.put("status", false);
                resultMap.put("message", "Business not found for businessUUID : " + businessUUID);
                return resultMap;
            } else {
                int businessId = businessMap.containsKey("business_id") ? Integer.parseInt(businessMap.get("business_id").toString()) : 0;
                String endDate = businessMap.containsKey("contract_end_date") ? businessMap.get("contract_end_date").toString() : "";
                int businessPlanConfigId = businessMap.containsKey("plan_config_id") ? Integer.parseInt(businessMap.get("plan_config_id").toString()) : 0;
                //get business plan config id
                int planConfigId = 0;
                if (accountType.equals("Enterprise")) { // DTLite to Enterprise
                    if (businessPlanConfigId == 3) {
                        result = changeToEnterpriseAccount(businessId, planConfigId, endDate);
                        resultMap.put("status", true);
                        resultMap.put("message", "Account changed to Enterprise successfully.");
                    } else {
                        resultMap.put("status", true);
                        resultMap.put("message", "No Action required! Account is in Enterprise.");
                        return resultMap;
                    }
                } else if (accountType.equals("DTLite")) { // Enterprise to DTLite
                    //To do waiting for the flow from product to be implemented
                   /* planConfigId = 3;
                    result = changeToDTLiteAccount(businessId, planConfigId, endDate);*/
                }
            }

        } catch (Exception e) {
            logger.error("Error in changeAccount() : " + e.getMessage());
            resultMap.put("status", false);
            resultMap.put("message", "Internal Error");
        }

        return resultMap;
    }

    /**
     * Method to update business from Enterprise to DTLite
     * @param businessId
     * @return
     */
    private boolean changeToDTLiteAccount(int businessId, int planConfigId, String endDate) {

        boolean result = false;
        try {
            //update business plan config id to 3 (DT Lite)
            String sql = "update business set plan_config_id = ?, modified_time = current_timestamp where business_id = ?";
            int updateResult = jdbcTemplate.update(sql, planConfigId, businessId);
            if (updateResult > 0) {
                result = true;
                //verify if customer is already mapped for this business
                String customerSql = "select * from dtlite_business_customer_map where business_id = ?";
                Map<String, Object> customerMap = jdbcTemplate.queryForMap(customerSql, new Object[]{businessId});
                if (!customerMap.isEmpty()) {
                    //send email notification
                    Map<String, Object> kingMap = getKingUserByBusinessId(businessId);
                    Map<String, Object> emailParams = new HashMap<>();
                    emailParams.put("toEmail", kingMap.containsKey("user_email") ? kingMap.get("user_email").toString() : "");
                    emailParams.put("recipientName", kingMap.containsKey("full_name") ? kingMap.get("full_name").toString() : "");
                    emailParams.put("endDate", Utils.getFormattedDateForAccountChangeMail(endDate));
                    notificationService.sendAccountChangeMail(emailParams, businessId);
                }

                //create a customer in stripe
               /* String customerId = paymentService.createCustomer(businessEmail, businessName, businessUUID);

                //insert the mapping entry in dtlite_business_customer_map
                if (Utils.notEmptyString(customerId)) {
                    paymentService.createDTLiteBusinessCustomerMap(businessId, customerId, startDate, 0, endDate);
                }*/
            }

        } catch (Exception e) {
            logger.error("Error in changeToDTLiteAccount() : " + e.getMessage());
        }

        return result;
    }

    private boolean changeToEnterpriseAccount(int businessId, int planConfigId, String endDate) {

        boolean result = false;
        try {
            //update business plan config id to 0 (Enterprise)
            String sql = "update business set plan_config_id = ?, modified_time = current_timestamp where business_id = ?";
            int updateResult = jdbcTemplate.update(sql, planConfigId, businessId);

            //delete the subscription to avoid auto payment for DTLite
            String deleteSubscriptionSql = "DELETE FROM business_renewal_schedule WHERE business_id = ? and payment_status = 'InComplete'";
            jdbcTemplate.update(deleteSubscriptionSql, new Object[]{businessId});
            logger.info("Subscription cancelled successfully for the business : {}", businessId);
            if (updateResult > 0) {
                result = true;
                CompletableFuture.runAsync(() -> {
                    try {
                        //send email notification
                        Map<String, Object> kingMap = getKingUserByBusinessId(businessId);
                        Map<String, Object> emailParams = new HashMap<>();
                        emailParams.put("toEmail", kingMap.containsKey("user_email") ? kingMap.get("user_email").toString() : "");
                        emailParams.put("recipientName", kingMap.containsKey("full_name") ? kingMap.get("full_name").toString() : "");
                        emailParams.put("endDate", Utils.getFormattedDateForAccountChangeMail(endDate));
                        notificationService.sendAccountChangeMail(emailParams, businessId);
                    } catch (Exception e) {
                        logger.error("Error in changeToEnterpriseAccount() : " + e.getMessage());
                    }
                });
            }
        } catch (Exception e) {
            logger.error("Error in changeToEnterpriseAccount() : " + e.getMessage());
        }

        return result;
    }

}
