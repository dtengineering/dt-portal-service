package com.dropthought.portal.service;

import com.dropthought.portal.model.AuthRoutingRequest;
import com.dropthought.portal.model.LTIBean;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class RestTemplateService extends  BaseService{

    public List callInsightsApiForMfa(String urlStr, HttpMethod method){
        List respMap = new ArrayList();
        // rest api header
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        try {
            // rest api call for insights url
            ResponseEntity<Object> responseEntity = restTemplate.exchange(urlStr, method, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info("response received successfully");
                    respMap = (List) responseEntity.getBody();
                }
            }else{
                logger.error("mfa request failed. Status code: {} ", responseEntity != null ? responseEntity.getStatusCode() : "");
            }
        }   catch (ResourceAccessException | HttpClientErrorException ex){
            logger.error("refused to connect");
        }
        catch (Exception e){
            logger.error("Exception at callRestApi. Msg {} ", e.getMessage());
        }
        return respMap;
    }

    /**
     * fucntion to perform REST callback to external application
     *
     * @param inputMap
     * @param uri
     * @param method
     * @return
     */
    public Map callSSORestApi(AuthRoutingRequest inputMap, String uri, HttpMethod method){
        //DTV-13160, DTV-12612
        Map respMap = new HashMap();
        /*JSONObject json = new JSONObject(inputMap);
        String inputJson = json.toString();*/
        logger.info("uri: {}",uri);
        logger.info(inputMap.toString());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AuthRoutingRequest> entity = new HttpEntity<>(inputMap, headers);

        try {
            ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, method, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info("response from auth = "+responseEntity.getBody().toString());
                    respMap = (Map) responseEntity.getBody();
                }
            }

        }
        catch (ResourceAccessException | HttpClientErrorException ex){
            //ex.printStackTrace();
            logger.error("rest template service refused to connect");
            respMap.put("success",false);
            respMap.put("message", "Connection refused");
        }
        catch (Exception e){
            //e.printStackTrace();
            logger.error("Error at callRestApi - {}", e.getMessage());
        }
        return  respMap;
    }

    /**
     * fucntion to perform REST callback to external application
     *
     * @param inputMap
     * @param uri
     * @param method
     * @return
     */
    public Map callRestApi(Map inputMap, String uri, HttpMethod method){
        //DTV-13160, DTV-12612
        Map respMap = new HashMap();
        JSONObject json = new JSONObject(inputMap);
        String inputJson = json.toString();
        logger.info("uri: {}",uri);
        logger.info(inputJson);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);

        try {
            ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, method, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info("response from auth = "+responseEntity.getBody().toString());
                    respMap = (Map) responseEntity.getBody();
                }
            }

        }
        catch (ResourceAccessException | HttpClientErrorException ex){
            //ex.printStackTrace();
            logger.error("rest template service refused to connect");
            respMap.put("success",false);
            respMap.put("message", "Connection refused");
        }
        catch (Exception e){
            //e.printStackTrace();
            logger.error("Error at callRestApi - {}", e.getMessage());
        }
        return  respMap;
    }

    /**
     * call rest api
     * @param urlStr
     * @return
     */
    public Map callRestApi(String urlStr, HttpMethod method){
        Map respMap = new HashMap();
        // rest api header
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        // rest template instance
        try {
            // rest api call for insights url
            ResponseEntity<Object> responseEntity = restTemplate.exchange(urlStr, method, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info("dtapp response received successfully");
                    logger.info("dtapp response received successfully  "+ responseEntity.getBody());
                    respMap = (Map) responseEntity.getBody();
                }
            }
        }   catch (ResourceAccessException | HttpClientErrorException ex){
            logger.error("dtapp service refused to connect");
            respMap.put("success",false);
            respMap.put("message", "Connection refused");
        }
        catch (Exception e){
            logger.error("Exception at callRestApi. Msg {} ", e.getMessage());
            respMap.put("success",false);
            respMap.put("message", "Exception occured");
        }
        return respMap;
    }

    /**
     * fucntion to perform REST callback to external application
     *
     * @param ltiBean
     * @param uri
     * @param method
     * @return
     */
    public Map callRestApiDtEdu(LTIBean ltiBean, String uri, HttpMethod method){

        Map respMap = new HashMap();
        /*JSONObject json = new JSONObject(inputMap);
        String inputJson = json.toString();*/
        logger.info("uri: {}",uri);
        logger.info(ltiBean.toString());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("X-DT-EDU-ACCESS", "dropthought.com");

        HttpEntity<LTIBean> entity = new HttpEntity<>(ltiBean, headers);

        try {
            ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, method, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info("response from auth = "+responseEntity.getBody().toString());
                    respMap = (Map) responseEntity.getBody();
                }
            }

        }
        catch (ResourceAccessException | HttpClientErrorException ex){
            //ex.printStackTrace();
            logger.error("rest template service refused to connect {}", ex.getMessage());
            respMap.put("success",false);
            respMap.put("message", "Connection refused");
        }
        catch (Exception e){
            //e.printStackTrace();
            logger.error("Error at callRestApi - {}", e.getMessage());
        }
        return  respMap;
    }
}
