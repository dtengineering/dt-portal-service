package com.dropthought.portal.service;

import com.dropthought.portal.businessconfigurations.service.BusinessConfigurationsService;
import com.dropthought.portal.model.TextAnalyticsApprovalBean;
import com.dropthought.portal.model.TextAnalyticsApprovedByBean;
import com.dropthought.portal.model.TextAnalyticsConfigBean;
import com.dropthought.portal.model.TextAnalyticsRequestBean;
import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.EmailTemplateUtility;
import com.dropthought.portal.util.TokenUtility;
import com.dropthought.portal.util.Utils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

@Component
public class TextAnalyticsConfigurationService extends BaseService {


    /**
     * JdbcTemplate for DB Connectivity.
     */
    @Autowired
    @Qualifier("jdbcDt")
    JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("jdbcNamedDt")
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    TokenUtility tokenUtility;

    @Autowired
    ClientService clientService;

    @Autowired
    BusinessConfigurationsService businessConfigurationsService;

    @Autowired
    UserService userService;

    @Autowired
    MailService mailService;

    @Value("${dtp.domain}")
    private String domain;

    @Value("${portal.domain}")
    private String portalDomain;

    public static String INSERT_TA_CONFIG = "insert into text_analytics_configs (business_id, text_analytics, text_analytics_plan_id, configs, approved_by, config_uuid, approvers, created_by) values (?, ?, ?, ?, ?, ?, ?, ?)";
    public static String SELETC_TA_CONFIG_BY_UNIQUEID = "select business_id, text_analytics, text_analytics_plan_id, configs, config_uuid, approval_status, approved_by, approvers, created_by from text_analytics_configs where config_uuid = ?";
    public static String SELETC_TA_CONFIG_BY_ID = "select business_id, text_analytics, text_analytics_plan_id, configs, config_uuid, approval_status, approved_by, approvers, created_by from text_analytics_configs where id = ?";
    public static String UPDATE_TA_APPROVAL_STATUS = "update text_analytics_configs set modified_time=current_timestamp, approval_status = ?, approved_by = JSON_ARRAY_APPEND(approved_by, '$',CAST(? AS JSON)), approvers = JSON_ARRAY_APPEND(approvers, '$', ?) where config_uuid = ? and  NOT JSON_CONTAINS(approvers, ?)";
    public static String SELETCT_TA_CONFIG_BY_EMAIID = "select count(1) from text_analytics_configs where config_uuid = ? and Json_contains(approvers, ?)";
    public static String UPDATE_TA_CONFIG_BY_ID = "update text_analytics_configs set text_analytics_plan_id = ?, text_analytics = ? where config_uuid = ?";
    public static String SELETC_TA_CONFIG_BY_BUSINESS_ID = "select id, business_id, text_analytics, text_analytics_plan_id, configs, config_uuid, approval_status, approved_by, approvers, created_time from text_analytics_configs where business_id = ? order by created_time desc";
    public static String SELECT_PREVIOUS_TA_CONFIG_BY_BUSINESS_ID = "select id, configs, business_id, config_id, created_time from previous_ta_config where business_id = ? order by created_time desc";

    /**
     * method to create text analytics configuration
     */
    public int  createTextAnalyticsConfig(TextAnalyticsConfigBean configBean, int businessId) {
        logger.info("begin createTextAnalyticsConfig");
        int textAnalyticsConfigId = 0;
        try {
            if(businessId == 0) {
                businessId = clientService.getBusinessIdByUUID(configBean.getBuinessUniqueId());
            }
            List userList = userService.getUserByUUID(configBean.getCreatedBy());
            int userId = 0;
            if(userList.size() > 0) {
                Map userMap = (Map) userList.get(0);
                userId = (int) userMap.get("user_id");
            }

            final String configUniqueId = tokenUtility.generateUUID();
            int plan = Utils.notEmptyString(configBean.getTextAnalyticsPlan()) && configBean.getTextAnalyticsPlan().equals("basic") ? 0 : 1;
            int bId = businessId;
            int createdBy = userId;

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(INSERT_TA_CONFIG, Statement.RETURN_GENERATED_KEYS);
                    ps.setInt(1, bId);
                    ps.setString(2, configBean.getTextAnalyticsEnabled());
                    ps.setInt(3, plan);
                    ps.setString(4, Utils.notEmptyString(configBean.getConfigs()) ? new JSONObject(configBean.getConfigs()).toString() : new JSONObject().toString());
                    ps.setString(5, Utils.isNotNull(configBean.getApprovedBy()) ? new JSONArray(configBean.getApprovedBy()).toString() : new JSONArray().toString());
                    ps.setString(6, configUniqueId);
                    ps.setString(7, Utils.isNotNull(configBean.getApprovers()) ? new JSONArray(configBean.getApprovers()).toString() : new JSONArray().toString());
                    ps.setInt(8, createdBy);
                    return ps;
                }
            }, keyHolder);
            textAnalyticsConfigId = keyHolder.getKey().intValue();

        } catch (Exception e) {
            logger.error("Exception at crateTextAnalyticsConfig. Msg {}", e.getMessage());
        }
        logger.info("end createTextAnalyticsConfig");
        return  textAnalyticsConfigId;
    }

    /**
     * method to get text analytics configuration
     */
    public TextAnalyticsConfigBean getTextAnalyticsConfig(String configUniqueId) {
        logger.info("begin getTextAnalyticsConfig by uniqueId");
        TextAnalyticsConfigBean configBean = null;
        try {
            List configList = jdbcTemplate.queryForList(SELETC_TA_CONFIG_BY_UNIQUEID, new Object[]{configUniqueId});
            Iterator itr = configList.iterator();
            while (itr.hasNext()) {
                Map configMap = (Map) itr.next();
                int status = configMap.containsKey("approval_status") && Utils.isNotNull(configMap.get("approval_status")) ? Integer.parseInt(configMap.get("approval_status").toString()) : 0;
                //int businessId = configMap.containsKey("businessId") ? (int) configMap.get("businessId") : 0;
                int businessId = configMap.containsKey("business_id") ? (int) configMap.get("business_id") : 0;
                int textAnalyticsPlan = configMap.containsKey("text_analytics_plan_id") && Utils.isNotNull(configMap.get("text_analytics_plan_id")) ? ((int) configMap.get("text_analytics_plan_id")) : 0;
                String configInput = configMap.containsKey("configs") && Utils.isNotNull(configMap.get("configs")) ? configMap.get("configs").toString() : "";
                String businessUniqueId = clientService.getBusinessUUIDByID(businessId);
                List approversList = configMap.containsKey("approvers") && Utils.isNotNull(configMap.get("approvers")) ? mapper.readValue(configMap.get("approvers").toString(), ArrayList.class) : new ArrayList<>();
                List<TextAnalyticsApprovedByBean> approvedByBean = configMap.containsKey("approved_by") && Utils.isNotNull(configMap.get("approved_by")) ? mapper.readValue(configMap.get("approved_by").toString(), ArrayList.class) : new ArrayList();
                configBean = new TextAnalyticsConfigBean();
                configBean.setApprovalStatus(getStatusStr(status));
                configBean.setApprovedBy(approvedByBean);
                configBean.setApprovers(approversList);
                configBean.setConfigUniqueId(configUniqueId);
                configBean.setBuinessUniqueId(businessUniqueId);
                configBean.setTextAnalyticsEnabled(configMap.containsKey("text_analytics") ? (String) configMap.get("text_analytics") : "0");
                configBean.setTextAnalyticsPlan(textAnalyticsPlan == 1 ? "basic" : "advanced");
                configBean.setConfigs(configInput);
            }

        } catch (Exception e) {
            logger.error("Exception at getTextAnalyticsConfig by uniqueId. Msg {}", e.getMessage());
        }
        logger.info("end getTextAnalyticsConfig");
        return configBean;
    }

    /**
     * method to get status in string value
     * @param state
     * @return
     */
    private String getStatusStr(int state){
        String status = "pending";
        switch (state){
            case 1:
                status = "approved";
                break;
            case 2:
                status = "disapproved";
                break;
        }
        return  status;
    }

    /**
     * method to get text analytics configuration
     */
    public TextAnalyticsConfigBean getTextAnalyticsConfig(int configId) {
        logger.info("begin getTextAnalyticsConfig by id");
        TextAnalyticsConfigBean configBean = null;
        try {
            List configList = jdbcTemplate.queryForList(SELETC_TA_CONFIG_BY_ID, new Object[]{configId});
            Iterator itr = configList.iterator();
            while (itr.hasNext()) {
                Map configMap = (Map) itr.next();
                int status = configMap.containsKey("approval_status") && Utils.isNotNull(configMap.get("approval_status")) ? Integer.parseInt(configMap.get("approval_status").toString()) : 0;
                int businessId = configMap.containsKey("businessId") ? (int) configMap.get("businessId") : 0;
                int textAnalyticsPlan = configMap.containsKey("text_analytics_plan_id") && Utils.isNotNull(configMap.get("text_analytics_plan_id")) ? ((int) configMap.get("text_analytics_plan_id")) : 0;
                String configInput = configMap.containsKey("configs") && Utils.isNotNull(configMap.get("configs")) ? configMap.get("configs").toString() : "";

                String businessUniqueId = clientService.getBusinessUUIDByID(businessId);
                List approversList = configMap.containsKey("approvers") && Utils.isNotNull(configMap.get("approvers")) ? mapper.readValue(configMap.get("approvers").toString(), ArrayList.class) : new ArrayList<>();
                List<TextAnalyticsApprovedByBean> approvedByBean = configMap.containsKey("approved_by") && Utils.isNotNull(configMap.get("approved_by")) ? mapper.readValue(configMap.get("approved_by").toString(), ArrayList.class) : new ArrayList<>();
                configBean = new TextAnalyticsConfigBean();
                configBean.setApprovalStatus(getStatusStr(status));
                configBean.setApprovedBy(approvedByBean);
                configBean.setApprovers(approversList);
                configBean.setConfigUniqueId(configMap.containsKey("config_uuid") && Utils.isNotNull(configMap.get("config_uuid")) ? configMap.get("config_uuid").toString() : "");
                configBean.setBuinessUniqueId(businessUniqueId);
                //configBean.setTextAnalyticsEnabled(configMap.containsKey("text_analytics") ? configMap.get("text_analytics").toString() : "0");
                configBean.setTextAnalyticsEnabled("1");
                configBean.setTextAnalyticsPlan(textAnalyticsPlan == 0 ? "basic" : "advanced");
                configBean.setConfigs(configInput);
                configBean.setConfigId(null);
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error("Exception at getTextAnalyticsConfig by id. Msg {}", e.getMessage());
        }
        logger.info("end getTextAnalyticsConfig by id");
        return configBean;
    }

    /**
     * method to update status of approval
     */
    public int updateTAConfigApprovalStatus(TextAnalyticsApprovalBean configBean) {

        logger.info("begin updateTAConfigApprovalStatus");
        int result = 0;
        try {
            Map approveByMap = new HashMap();
            String businessUUID = configBean.getBusinessId();
            String status = getStatusStr(configBean.getApproval());
            approveByMap.put("email", configBean.getApprovedBy());
            approveByMap.put("status", status);
            String apporver = "\""+configBean.getApprovedBy()+"\"";

            /**
             * If one of them clicks deny and the other clicks approve, it is considered as approved.
             * If both of them click deny, then it is denied.
             * If one of them approves but the other does not respond then it is approved
             * If one of them denied but the other does not respond then it is denied.
             */
            boolean isTARequestApproved = isTAConfigApproved(configBean.getId());
            if(isTARequestApproved){
                configBean.setApproval(1);
            }

            if(configBean.getApproval() == 2){
                //only approved email and status will be stored in approved_by column
                String sql = "update text_analytics_configs set approval_status = ?, approvers = JSON_ARRAY_APPEND(approvers, '$', ?) " +
                        "where config_uuid = ? and  NOT JSON_CONTAINS(approvers, ?)";
                result = jdbcTemplate.update(sql, new Object[]{configBean.getApproval().toString(), configBean.getApprovedBy(), configBean.getId(), apporver});

            }else {
                result = jdbcTemplate.update(UPDATE_TA_APPROVAL_STATUS, new Object[]{configBean.getApproval().toString(),
                        new JSONObject(approveByMap).toString(), configBean.getApprovedBy(), configBean.getId(), apporver});
            }


            if(result > 0) {

                int userId = getApprovalRequestedUser(configBean.getId());
                List userList = userService.getUserById(userId);
                if (userList.size() > 0) {
                    Map userMap = (Map) userList.get(0);
                    Map businessMap = businessConfigurationsService.retrieveBusinessByUUID(configBean.getBusinessId());
                    String businessName = businessMap.containsKey("business_name") ? businessMap.get("business_name").toString() : "";
                    mailService.sendTAStatusEmailToCSUser(status, userMap, businessName);
                }

                //upsert text analytics data by clientId after approval
                //check if TA request is already approved or not for given configId
                //If TA approval is 1 update latest config to Text analytics
                if (!isTARequestApproved && (configBean.getApproval() == 1)) {
                    TextAnalyticsConfigBean bean = this.getTextAnalyticsConfig(configBean.getId());
                    Map pendingConfig = Utils.isNotNull(bean.getConfigs()) && Utils.notEmptyString(bean.getConfigs()) ? mapper.readValue(bean.getConfigs(), HashMap.class) : new HashMap();
                    businessConfigurationsService.upsertTextAnalytics(new JSONObject(pendingConfig).toString(), businessUUID);


                    //Update business config and business config version.
                    //Request to TA is sent independent of the business configs
                    //We did not have Basic text analytics at the time of designing this

                    List businessConfigs = businessConfigurationsService.getBusinessConfigurationsByBusinessId(businessUUID);
                    businessConfigurationsService.updateLatestBusinessConfigurationWithAdvancedTextAnalyticsValue(bean);
                }
            }

        } catch (Exception e) {
            //e.printStackTrace();
            logger.error("Exception at updateTAConfigApprovalStatus. Msg {}", e.getMessage());
            result = -1;
        }
        logger.info("end updateTAConfigApprovalStatus");
        return result;
    }

    /**
     * method get TA approval request userId by configUniqueId
     * @param configUniqueId
     * @return
     */
    public int getApprovalRequestedUser(String configUniqueId){
        logger.info("begin getApprovalRequestedUser");
        int userId = 0;
        try {
            Map resultMap = jdbcTemplate.queryForMap(SELETC_TA_CONFIG_BY_UNIQUEID, new Object[]{configUniqueId});
            if(resultMap.containsKey("created_by")){
                userId = Utils.isNotNull(resultMap.get("created_by")) ? Integer.parseInt(resultMap.get("created_by").toString()) : 0;
            }
        } catch (Exception e) {
            logger.error("Exception at getApprovalRequestedUser. Msg {}", e.getMessage());
        }
        logger.info("end getApprovalRequestedUser");
        return  userId;
    }

    /**
     * method to update text analytics plan details
     */
    public int updateTextAnalyticsPlan(int taPlan, String taConfigEnable, String uniqueId) {
        logger.info("begin updateTextAnalyticsConfiguration");
        int result = 0;
        try {
            jdbcTemplate.update(UPDATE_TA_CONFIG_BY_ID, new Object[]{taPlan, taConfigEnable, uniqueId});

        } catch (Exception e) {
            logger.error("Exception at updateTAConfigApprovalStatus. Msg {}", e.getMessage());
        }
        logger.info("end updateTextAnalyticsConfiguration");
        return  result;
    }

    /**
     * method to check whether the approver is already approved/disapproved the TA config request
     * @param configUniqueId
     * @param approverEmail
     * @return
     */
    public boolean getTextAnalyticsConfigStatusByEmail(String configUniqueId, String approverEmail){
        logger.info("begin updateTextAnalyticsConfiguration");
        boolean result = false;
        try {
            result = jdbcTemplate.queryForObject(SELETCT_TA_CONFIG_BY_EMAIID, new Object[]{configUniqueId, "\""+approverEmail+"\""}, Boolean.class);
        } catch (Exception e) {
            logger.error("Exception at updateTAConfigApprovalStatus. Msg {}", e.getMessage());
        }
        logger.info("end updateTextAnalyticsConfiguration");
        return  result;
    }

    public boolean isTAConfigApproved(String configUniqueId){
        logger.info("begin isTAConfigApproved");
        boolean result = false;
        try {
            Map resultMap = jdbcTemplate.queryForMap(SELETC_TA_CONFIG_BY_UNIQUEID, new Object[]{configUniqueId});
            if(resultMap.containsKey("approval_status")){
                int status = Utils.isNotNull(resultMap.get("approval_status")) ? Integer.parseInt(resultMap.get("approval_status").toString()) : 0;
                if(status == 1){
                    return true;
                }
            }
        } catch (Exception e) {
            logger.error("Exception at updateTAConfigApprovalStatus. Msg {}", e.getMessage());
        }
        logger.info("end isTAConfigApproved");
        return  result;
    }


    /**
     * method to create text analytics configuration from text analytics request
     */
    public String createTextAnalyticsRequestConfig(TextAnalyticsRequestBean requestBean) {
        logger.info("begin createTextAnalyticsConfig from TA Request");
        int textAnalyticsConfigId = 0;
        String textAnalyticsConfigUUID = "";
        int businessId = 0;
        try {
            businessId = clientService.getBusinessIdByUUID(requestBean.getBusinessId());
            List userList = userService.getUserByUUID(requestBean.getCreatedBy());
            boolean resend = Utils.isNotNull(requestBean.getResend()) && (requestBean.getResend() == 1);
            int userId = 0;
            if(userList.size() > 0) {
                Map userMap = (Map) userList.get(0);
                userId = (int) userMap.get("user_id");
            }

            if(!resend) {
                final String configUniqueId = tokenUtility.generateUUID();
                int plan = Utils.isNotNull(requestBean.getTextAnalyticsPlanId()) ? requestBean.getTextAnalyticsPlanId() : 0;
                int bId = businessId;
                int createdBy = userId;

                KeyHolder keyHolder = new GeneratedKeyHolder();
                jdbcTemplate.update(new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps = connection.prepareStatement(INSERT_TA_CONFIG, Statement.RETURN_GENERATED_KEYS);
                        ps.setInt(1, bId);
                        ps.setString(2, requestBean.getTextAnalytics());
                        ps.setInt(3, plan);
                        ps.setString(4, Utils.isNotNull(requestBean.getPlanConfigs()) ? new JSONObject(requestBean.getPlanConfigs()).toString() : new JSONObject().toString());
                        ps.setString(5, new JSONArray().toString());
                        ps.setString(6, configUniqueId);
                        ps.setString(7, new JSONArray().toString());
                        ps.setInt(8, createdBy);
                        return ps;
                    }
                }, keyHolder);
                textAnalyticsConfigId = keyHolder.getKey().intValue();

                textAnalyticsConfigUUID = configUniqueId;
                //if request creation entry is completed, sending approve email to managers
                if (textAnalyticsConfigId > 0) {
                    this.sendTAApprovalEmail(businessId, requestBean, configUniqueId);
                }
            }else {
                //resending pending config for approval
                TextAnalyticsConfigBean textAnalyticsConfigBean = this.getLatestTextAnalyticsConfigByBusinessId(businessId);
                String approvalState = textAnalyticsConfigBean.getApprovalStatus();
                if(approvalState.equalsIgnoreCase("pending")){
                    String configUUID = Utils.isNotNull(textAnalyticsConfigBean.getConfigUniqueId()) && Utils.notEmptyString(textAnalyticsConfigBean.getConfigUniqueId()) ? textAnalyticsConfigBean.getConfigUniqueId() : "";
                    this.sendTAApprovalEmail(businessId, requestBean, configUUID);
                    textAnalyticsConfigUUID = configUUID;
                }
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error("Exception at crateTextAnalyticsConfig. Msg {}", e.getMessage());
        }
        logger.info("end createTextAnalyticsConfig");
        return  textAnalyticsConfigUUID;
    }

    /**
     *
     * @param businessId
     * @param textAnalyticsRequestBean
     */
    public void sendTAApprovalEmail(int businessId, TextAnalyticsRequestBean textAnalyticsRequestBean, String configUUID){
        logger.info("begin sending ta approval email");
        boolean result = false;
        try {
            String businessUUID = textAnalyticsRequestBean.getBusinessId();
            List toEmailsList = textAnalyticsRequestBean.getTo();
            String toEmail1 = "deepak@dropthought.com";
            String toEmail2 = "anirudh@dropthought.com";

            if(domain.contains("test")||domain.contains("stage")||domain.contains("automation")){
                toEmail1 = "deepak@yopmail.com";
                toEmail2 = "anirudh@yopmail.com";
            }
            String host = portalDomain;//request.getHeader("host");
            host = host.contains("localhost") ? "http://localhost:4201" : host;


            toEmailsList.add(toEmail1);
            toEmailsList.add(toEmail2);

            if(businessId > 0 && Utils.isNotNull(textAnalyticsRequestBean.getPlanConfigs())){
                Map configs = textAnalyticsRequestBean.getPlanConfigs();

                /**
                 * dataDurationId --> 1 = historical, 2 = upcoming, 3 = demo, 4 = historical + upcoming, for value 3 (startDate and endDate) required
                 *
                 * textAnalyticsPlanId ---> 1 = basic, 2 = advanced, for planId 2 categoryMlId is needed
                 *
                 */
                List surveyNames = new ArrayList();
                int dataDurationId = configs.containsKey("dataDurationId") && Utils.isNotNull(configs.get("dataDurationId")) ? Integer.parseInt(configs.get("dataDurationId").toString()) : 1;
                int textAnalyticsPlanId = configs.containsKey("textAnalyticsPlanId") && Utils.isNotNull(configs.get("textAnalyticsPlanId")) ? Integer.parseInt(configs.get("textAnalyticsPlanId").toString()) : 1;
                String startDate = configs.containsKey("startDate") && Utils.isNotNull(configs.get("startDate")) ? configs.get("startDate").toString() : "";
                String endDate = configs.containsKey("endDate") && Utils.isNotNull(configs.get("endDate")) ? configs.get("endDate").toString() : "";
                List surveyUUIDs = configs.containsKey("surveyId") && Utils.isNotNull(configs.get("surveyId")) ? (List) configs.get("surveyId") : new ArrayList();

                //For mapping model to many surveys present
                //surveIds will contains surveyUUIDs list
                if(surveyUUIDs.size() > 0){
                    surveyNames = this.getSurveyNamesListByUUIDs(surveyUUIDs, businessUUID, "en");
                }

                Map businessMap = businessConfigurationsService.retrieveBusinessByUUID(businessUUID);
                String businessName =  businessMap.containsKey("business_name") ? businessMap.get("business_name").toString() : "";
                String subject = String.format(Constants.TA_REQUEST_APPROVAL_SUBJECT, businessName);
                String dataDuration = this.getDataDurationTypeById(dataDurationId);
                String plan = Constants.TA_REQUEST_APPROVAL_PLAN;
                String environmentInfo = EmailTemplateUtility.getEnvironmentEmailContent(domain, false);

                //if dataduration type is demo/dry need to add start & end date in email body
                if(dataDurationId == 3){
                    dataDuration = dataDuration + " from " + Utils.getDayFormattedDate(startDate) + " to " + Utils.getDayFormattedDate(endDate);
                }

                String textAnalyticsPlan = textAnalyticsPlanId == 1 ? "Basic" : "Advanced";
                //For mapping model to many surveys present, add surveys count in body.
                if(surveyUUIDs.size() > 0){
                    plan = surveyUUIDs.size() == 1 ? String.format(plan, textAnalyticsPlan, "the following <strong> 1 survey </strong>") : String.format(plan, textAnalyticsPlan, "the following <strong>" + surveyUUIDs.size() + " surveys</strong>");
                }else {
                    plan = String.format(plan, textAnalyticsPlan, "<strong> All surveys </strong>");
                }

                for(int i=0; i<toEmailsList.size(); i++){
                    Map messages = new HashMap();
                    String recipientEmail = toEmailsList.get(i).toString();
                    String recipientName = recipientEmail.split("@")[0];
                    recipientName = recipientName.substring(0, 1).toUpperCase() + recipientName.substring(1);
                    String encodedEmail = Utils.encodeString(recipientEmail);

                    //approval , 0 --> pending, 1--> approved , 2 --> disapproved
                    String approvalLink = host + "/approve/1/businessid/"+businessUUID+"/id/"+configUUID+"/"+encodedEmail;
                    String denyLink = host + "/approve/2/businessid/"+businessUUID+"/id/"+configUUID+"/"+encodedEmail;
                    logger.info("approvalLink  "+approvalLink);
                    logger.info("denyLink  "+denyLink);

                    messages.put("dataDuration", dataDuration);
                    messages.put("plan", plan);
                    messages.put("approvalLink", approvalLink);
                    messages.put("denyLink", denyLink);
                    messages.put("businessName", "<strong>"+businessName+"</strong>");
                    messages.put("EnvironmentInfo", environmentInfo);

                    logger.info("TA request Env : ", environmentInfo);
                    if(surveyNames.size() > 0){
                        messages.put("surveyNames", surveyNames);
                    }
                    mailService.sendTextAnalyticsRequestEmail(recipientEmail, recipientName, subject, messages, "TextAnalyticsRequest.html");
                }

            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error("Exception at sendTAApprovalEmail. Msg {}", e.getMessage());
        }
        logger.info("end isTAConfigApproved");
    }

    /**
     * method to get data duration type in string value
     * @param dataDurationId
     * @return
     */
    public String getDataDurationTypeById(int dataDurationId){
        String status = "";
        switch (dataDurationId){
            case 1:
                status = "Historical data";
                break;
            case 2:
                status = "Upcoming data";
                break;
            case 3:
                status = "Demo/dry run data";
                break;
            case 4:
                status = "Historical, Upcoming data";
                break;
        }
        return  status;
    }

    /**
     * method to get surveys list by survey uuids
     * @param surveyIds
     * @return
     */
    private List getSurveyNamesListByUUIDs(List surveyIds, String businessUUID, String language){
        List surveyNames = new ArrayList();
        try{
            if(surveyIds.size() > 0 && Utils.notEmptyString(businessUUID)) {
                List surveys = new ArrayList();
                String businessUUIDHyphenReplaced = businessUUID.replace("-", "_");
                String surveyTableName = "surveys_" + businessUUIDHyphenReplaced;
                String sql = "select * from " + surveyTableName + " where survey_uuid in (:surveyId)";
                if (surveyIds.size() > 0 ) {
                    MapSqlParameterSource surveyParam = new MapSqlParameterSource();
                    surveyParam.addValue("surveyId", surveyIds);
                    surveys = namedParameterJdbcTemplate.queryForList(sql, surveyParam);
                }

                for(int i=0; i<surveys.size(); i++){
                    Map programMap = (Map) surveys.get(i);
                    List languages = mapper.readValue(programMap.get("languages").toString(), ArrayList.class);
                    int languageIndex = languages.indexOf(language);

                    List surveyNamesList = mapper.readValue(programMap.get("survey_names").toString(), ArrayList.class);
                    String surveyName = (surveyNamesList.size() >= languageIndex) ? surveyNamesList.get(languageIndex).toString() : "";
                    surveyNames.add(surveyName);
                }
            }
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return  surveyNames;
    }

    /**
     * Method to get text analytics configuration
     *
     * @param businessId
     * @return
     */
    public TextAnalyticsConfigBean getLatestTextAnalyticsConfigByBusinessId(int businessId) {
        logger.info("begin get latest TextAnalyticsConfig by businessID");
        TextAnalyticsConfigBean configBean = null;
        try {
            if(businessId > 0) {
                List configList = jdbcTemplate.queryForList(SELETC_TA_CONFIG_BY_BUSINESS_ID, new Object[]{businessId});
                if (configList.size() > 0) {
                    Map configMap = (Map) configList.get(0);
                    int configId = configMap.containsKey("id") && Utils.isNotNull(configMap.get("id")) ? Integer.parseInt(configMap.get("id").toString()) : 0;
                    int status = configMap.containsKey("approval_status") && Utils.isNotNull(configMap.get("approval_status")) ? Integer.parseInt(configMap.get("approval_status").toString()) : 0;
                    String configUniqueId = configMap.containsKey("config_uuid") ? configMap.get("config_uuid").toString() : "";
                    int textAnalyticsPlan = configMap.containsKey("text_analytics_plan_id") && Utils.isNotNull(configMap.get("text_analytics_plan_id")) ? ((int) configMap.get("text_analytics_plan_id")) : 0;
                    String configInput = configMap.containsKey("configs") && Utils.isNotNull(configMap.get("configs")) ? configMap.get("configs").toString() : "";

                    String businessUniqueId = clientService.getBusinessUUIDByID(businessId);
                    List approversList = configMap.containsKey("approvers") && Utils.isNotNull(configMap.get("approvers")) ? mapper.readValue(configMap.get("approvers").toString(), ArrayList.class) : new ArrayList<>();
                    List<TextAnalyticsApprovedByBean> approvedByBean = configMap.containsKey("approved_by") && Utils.isNotNull(configMap.get("approved_by")) ? mapper.readValue(configMap.get("approved_by").toString(), ArrayList.class) : new ArrayList();
                    configBean = new TextAnalyticsConfigBean();
                    configBean.setApprovalStatus(status == 0 ? "pending" : status == 1 ? "approved" : "disapproved");
                    configBean.setApprovedBy(approvedByBean);
                    configBean.setApprovers(approversList);
                    configBean.setConfigUniqueId(configUniqueId);
                    configBean.setBuinessUniqueId(businessUniqueId);
                    configBean.setTextAnalyticsEnabled(configMap.containsKey("text_analytics") ? (String) configMap.get("text_analytics") : "0");
                    configBean.setTextAnalyticsPlan(textAnalyticsPlan == 1 ? "basic" : "advanced");
                    configBean.setConfigs(configInput);
                    configBean.setConfigId(configId);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at getTextAnalyticsConfig by businessID. Msg {}", e.getMessage());
        }
        logger.info("end get latest TextAnalyticsConfig by businessID");
        return configBean;
    }

    /**
     * Method to get text analytics configuration by businessUUID
     * @param businessUniqueId
     * @return
     */
    public TextAnalyticsConfigBean getTextAnalyticsConfigByClientUUID(String businessUniqueId) {
        logger.info("begin get latest TextAnalyticsConfig by businessUUID");
        TextAnalyticsConfigBean configBean = null;
        try {
            int businessId = clientService.getBusinessIdByUUID(businessUniqueId);
            //#SE-851 If we need additional parameters then we can write another service function for this.
            configBean = this.getLatestTextAnalyticsConfigByBusinessId(businessId);

        } catch (Exception e) {
            logger.error("Exception at getTextAnalyticsConfig by businessUUID. Msg {}", e.getMessage());
        }
        logger.info("end get latest TextAnalyticsConfig by businessUUID");
        return configBean;
    }


    /**
     * Method to get text analytics previous configuration from previous_ta_config table
     *
     * @param businessId
     * @return
     */
    public TextAnalyticsConfigBean getLatestPreviousTextAnalyticsConfigByBusinessId(int businessId) {
        logger.info("begin get latest previous TextAnalyticsConfig by businessID");
        TextAnalyticsConfigBean configBean = null;
        try {
            if(businessId > 0) {
                List configList = jdbcTemplate.queryForList(SELECT_PREVIOUS_TA_CONFIG_BY_BUSINESS_ID, new Object[]{businessId});
                if (configList.size() > 0) {
                    configBean = new TextAnalyticsConfigBean();
                    Map configMap = (Map) configList.get(0);
                    String configInput = configMap.containsKey("configs") && Utils.isNotNull(configMap.get("configs")) ? configMap.get("configs").toString() : "";
                    int configId = configMap.containsKey("config_id") && Utils.isNotNull(configMap.get("config_id")) ? Integer.parseInt(configMap.get("config_id").toString()) : 0;
                    configBean.setConfigId(configId);
                    configBean.setConfigs(configInput);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at getTextAnalyticsConfig by businessID. Msg {}", e.getMessage());
        }
        logger.info("end get latest previous TextAnalyticsConfig by businessID");
        return configBean;
    }

    /**
     * Method to get text analytics latest configuration id from text_analytics_configs table
     * @param businessId
     * @return
     */
    public int getLatestConfigId(Integer businessId) {

        int configId = 0;
        try {
            String query = "select id from text_analytics_configs where business_id = ?  ORDER BY created_time DESC LIMIT 1";
            configId = jdbcTemplate.queryForObject(query, new Object[]{businessId}, Integer.class);
        } catch (Exception e) {
            logger.error("Exception at getTextAnalyticsConfig by businessID. Msg {}", e.getMessage());
        }

        return  configId;
    }

    /**
     * Method to update business_configurations and business_configurations_version table with text analytics config id
     * @param configId
     * @param buinessUniqueId
     */
    public void updateConfigIdToBusiness(Integer configId, String buinessUniqueId) {

        try {
            int businessId = clientService.getBusinessIdByUUID(buinessUniqueId);
            String query = "update business_configurations set text_analytics_config_id = ? where business_id = ?";
            jdbcTemplate.update(query, new Object[]{configId, businessId});
            String maxIdQuery = "select max(business_config_version_id) from business_configurations_version where business_id = ?";
            int maxId = jdbcTemplate.queryForObject(maxIdQuery, new Object[]{businessId}, Integer.class);
            String businessConfigVersionQuery = "update business_configurations_version set text_analytics_config_id = ? " +
                    " where business_id = ? and business_config_version_id = ?";
            jdbcTemplate.update(query, new Object[]{configId, businessId, maxId});
        } catch (Exception e) {
            logger.error("Exception at getTextAnalyticsConfig by businessID. Msg {}", e.getMessage());
        }
    }
}
