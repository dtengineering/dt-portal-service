package com.dropthought.portal.service;

import com.dropthought.portal.dao.CustomThemeDAO;
import com.dropthought.portal.model.CustomThemeBean;
import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.Utils;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class CustomThemeService extends BaseService {

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper()
            .enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT)
            .enable(DeserializationFeature.FAIL_ON_READING_DUP_TREE_KEY );

    @Autowired
    CustomThemeDAO customThemeDAO;

    @Value("${storage.url}")
    private String storageUrl;

    /**
     * Creating custom eux theme
     * @param customThemeBean
     * @return
     */
    public Map createTheme(CustomThemeBean customThemeBean) {
        Map resultMap = new HashMap();
        try{
            if(customThemeBean != null){
                String themeUUID = customThemeDAO.createTheme(customThemeBean);
                resultMap.put("success", themeUUID.length() > 0 ? true : false);
                resultMap.put(Constants.RESULT, themeUUID);
            }else{
                resultMap = Utils.failureResponse("Error in creating custom theme");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return resultMap;
    }

    /**
     * Get theme by themeUUID
     * @param themeUUID
     * @return
     */
    public CustomThemeBean getThemeByThemeUUID(String themeUUID) {
        CustomThemeBean customThemeBean = new CustomThemeBean();
        try{
            if(Utils.notEmptyString(themeUUID)){
                Map themeMap = customThemeDAO.getThemeByThemeUUID(themeUUID);
                if (themeMap.size() > 0 && themeMap.containsKey("theme_uuid")) {
                    customThemeBean.setThemeName( Utils.isNotNull(themeMap.get("theme_name")) && themeMap.containsKey("theme_name") ? themeMap.get("theme_name").toString() : "");
                    customThemeBean.setThemeUUID(Utils.isNotNull(themeMap.get("theme_uuid")) && themeMap.containsKey("theme_uuid") ? themeMap.get("theme_uuid").toString() : "");
                    customThemeBean.setPageStyleCode(Utils.isNotNull(themeMap.get("page_theme_style_code")) && themeMap.containsKey("page_theme_style_code") ? themeMap.get("page_theme_style_code").toString() : "");
                    customThemeBean.setPageImageUrl(Utils.isNotNull(themeMap.get("page_theme_bg_img")) && themeMap.containsKey("page_theme_bg_img") ? themeMap.get("page_theme_bg_img").toString() : "");
                    customThemeBean.setButtonsStyleCode(Utils.isNotNull(themeMap.get("buttons_style_code")) && themeMap.containsKey("buttons_style_code") ? themeMap.get("buttons_style_code").toString() : "");
                    customThemeBean.setProgressBarStyleCode(Utils.isNotNull(themeMap.get("progress_bar_style_code")) && themeMap.containsKey("progress_bar_style_code") ? themeMap.get("progress_bar_style_code").toString() : "");
                    customThemeBean.setCreatedTime(Utils.isNotNull(themeMap.get("created_time")) && themeMap.containsKey("created_time") ? themeMap.get("created_time").toString() : "");
                    customThemeBean.setSmileyRatingStyleCode(Utils.isNotNull(themeMap.get("smiley_rating_style_code")) && themeMap.containsKey("smiley_rating_style_code") ? themeMap.get("smiley_rating_style_code").toString() : "");
                    customThemeBean.setSmileyRatingImageUrl(Utils.isNotNull(themeMap.get("smiley_rating_bg_img")) && themeMap.containsKey("smiley_rating_bg_img") ? mapper.readValue(themeMap.get("smiley_rating_bg_img").toString(), HashMap.class) : new HashMap());
                    customThemeBean.setStarRatingStyleCode(Utils.isNotNull(themeMap.get("star_rating_style_code")) && themeMap.containsKey("star_rating_style_code") ? themeMap.get("star_rating_style_code").toString() : "");
                    customThemeBean.setStarRatingImageUrl(Utils.isNotNull(themeMap.get("star_rating_bg_img")) && themeMap.containsKey("star_rating_bg_img") ? mapper.readValue(themeMap.get("star_rating_bg_img").toString(), HashMap.class) : new HashMap());
                    customThemeBean.setThumbRatingStyleCode(Utils.isNotNull(themeMap.get("thumb_rating_style_code")) && themeMap.containsKey("thumb_rating_style_code") ? themeMap.get("thumb_rating_style_code").toString() : "");
                    customThemeBean.setThumbRatingImageUrl(Utils.isNotNull(themeMap.get("thumb_rating_bg_img")) && themeMap.containsKey("thumb_rating_bg_img") ? mapper.readValue(themeMap.get("thumb_rating_bg_img").toString(), HashMap.class) : new HashMap());
                    customThemeBean.setSliderRatingStyleCode(Utils.isNotNull(themeMap.get("slider_rating_style_code")) && themeMap.containsKey("slider_rating_style_code") ? themeMap.get("slider_rating_style_code").toString() : "");
                    customThemeBean.setSliderRatingImageUrl(Utils.isNotNull(themeMap.get("slider_rating_bg_img")) && themeMap.containsKey("slider_rating_bg_img") ? themeMap.get("slider_rating_bg_img").toString() : "");
                    customThemeBean.setScaleRatingStyleCode(Utils.isNotNull(themeMap.get("scale_rating_style_code")) && themeMap.containsKey("scale_rating_style_code") ? themeMap.get("scale_rating_style_code").toString() : "");
                    customThemeBean.setScaleRatingImageUrl(Utils.isNotNull(themeMap.get("scale_rating_bg_img")) && themeMap.containsKey("scale_rating_bg_img") ? mapper.readValue(themeMap.get("scale_rating_bg_img").toString(), HashMap.class) : new HashMap());
                    customThemeBean.setMultipleResponseStyleCode(Utils.isNotNull(themeMap.get("multiple_response_style_code")) && themeMap.containsKey("multiple_response_style_code") ? themeMap.get("multiple_response_style_code").toString() : "");
                    customThemeBean.setMultipleResponseImageUrl(Utils.isNotNull(themeMap.get("muliple_response_bg_img")) && themeMap.containsKey("muliple_response_bg_img") ? mapper.readValue(themeMap.get("muliple_response_bg_img").toString(), HashMap.class) : new HashMap());
                    customThemeBean.setSingleResponseStyleCode(Utils.isNotNull(themeMap.get("single_response_style_code")) && themeMap.containsKey("single_response_style_code") ? themeMap.get("single_response_style_code").toString() : "");
                    customThemeBean.setSingleResponseImageUrl(Utils.isNotNull(themeMap.get("single_response_bg_img")) && themeMap.containsKey("single_response_bg_img") ? mapper.readValue(themeMap.get("single_response_bg_img").toString(), HashMap.class) : new HashMap());
                    customThemeBean.setOpenEndedStyleCode(Utils.isNotNull(themeMap.get("open_ended_style_code")) && themeMap.containsKey("open_ended_style_code") ? themeMap.get("open_ended_style_code").toString() : "");
                    customThemeBean.setNpsStyleCode(Utils.isNotNull(themeMap.get("nps_style_code")) && themeMap.containsKey("nps_style_code") ? themeMap.get("nps_style_code").toString() : "");
                    customThemeBean.setNpsImageUrl(Utils.isNotNull(themeMap.get("nps_bg_img")) && themeMap.containsKey("nps_bg_img") ? mapper.readValue(themeMap.get("nps_bg_img").toString(), HashMap.class) : new HashMap());
                    customThemeBean.setRankingStyleCode(Utils.isNotNull(themeMap.get("ranking_style_code")) && themeMap.containsKey("ranking_style_code") ? themeMap.get("ranking_style_code").toString() : "");
                    customThemeBean.setRankingImageUrl(Utils.isNotNull(themeMap.get("ranking_bg_img")) && themeMap.containsKey("ranking_bg_img") ? themeMap.get("ranking_bg_img").toString() : "");
                    customThemeBean.setMatrixRatingStyleCode(Utils.isNotNull(themeMap.get("matrix_rating_style_code")) && themeMap.containsKey("matrix_rating_style_code") ? themeMap.get("matrix_rating_style_code").toString() : "");
                    customThemeBean.setMatrixRatingImageUrl(Utils.isNotNull(themeMap.get("matrix_rating_bg_img")) && themeMap.containsKey("matrix_rating_bg_img") ? mapper.readValue(themeMap.get("matrix_rating_bg_img").toString(), HashMap.class) : new HashMap());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return customThemeBean;
    }

    /**
     * update theme by themeUUID
     * @param customThemeBean
     * @return
     */
    public Map updateThemeById(CustomThemeBean customThemeBean){
        Map resultMap = new HashMap();
        try{
            if(customThemeBean != null && Utils.notEmptyString(customThemeBean.getThemeUUID())){
                int result = customThemeDAO.updateThemeById(customThemeBean);
                if(result > 0) {
                    resultMap = Utils.successResponse();
                    resultMap.put(Constants.RESULT, this.getThemeByThemeUUID(customThemeBean.getThemeUUID()));
                } else if(result == -1){
                    resultMap = Utils.failureResponse("Exception occured while updating theme");
                }
                else{
                    resultMap = Utils.failureResponse();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return resultMap;
    }

    /**
     * getAllThemes
     * @param sortBy
     * @return
     */
    public Map getAllThemes(String search, Boolean exactMatch, String sortBy, int pageNo){
        Map resultMap = new HashMap();
        try{
            List themes = customThemeDAO.getAllThemes(search, exactMatch, sortBy, pageNo);
            resultMap = Utils.successResponse();
            resultMap.put("result", themes);
            resultMap.put("totalCount", pageNo == -1 ? themes.size() : customThemeDAO.getAllThemesCount());
        }catch (Exception e){
            e.printStackTrace();
            resultMap = Utils.failureResponse();
        }
        return resultMap;
    }

    /**
     * Get theme by themeUUID
     * @param themeUUID
     * @return
     */
    public int getThemeIdByThemeUUID(String themeUUID) {
        int themeId = 0;
        try{
            if(Utils.notEmptyString(themeUUID)){
                themeId = customThemeDAO.getThemeIdByThemeUUID(themeUUID);
            }
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
        return themeId;
    }

    /**
     *
     * @param themeUUID
     * @return
     */
    public Map deleteThemeById(String themeUUID){
        Map resultMap = new HashMap();
        try {
            if(Utils.notEmptyString(themeUUID)){
                int result = customThemeDAO.deleteThemeById(themeUUID);
                if(result > 0){
                    resultMap = Utils.successResponse();
                }else {
                    resultMap = Utils.failureResponse();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return resultMap;
    }

    /**
     * Method to upload image to S3 and returns url
     * @param base64String
     * @return
     */
    public Map uploadImageToS3(String base64String) {
        Map resultMap = new HashMap();
        try{
            if(Utils.notEmptyString(base64String)){
                /**
                 * base64 -> byte array -> file -> upload to s3 -> get url
                 */

                // Decode Base64 string to byte array
                byte[] imageBytes = Base64.getDecoder().decode(base64String);

                /**
                 * Create a new temporary file to save the decoded image
                 * createTempFile will append unique string at the end of the file name
                 * to avoid file conflicts in multi instance environment & restrictive access to /tmp folder
                  */
                File imageFile = File.createTempFile("image_", ".png");
                FileOutputStream fos = new FileOutputStream(imageFile);
                fos.write(imageBytes);
                fos.close();

                // Create a new multipart request body
                MultiValueMap<String, Object> requestBody = new LinkedMultiValueMap<>();
                requestBody.add("file", new ByteArrayResource(imageBytes) {
                    @Override
                    public String getFilename() {
                        return imageFile.getName();
                    }
                });

                //uploading to S3
                resultMap = this.callS3RestAPI(requestBody);
                // Delete the temporary file
                if(imageFile.exists())
                    imageFile.delete();

            }else{
                resultMap = Utils.failureResponse(" Error in uploading image to s3 ");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return resultMap;
    }

    /**
     * given a base64 string -> image -> gets aws url for the image
     * Without storing file inside function
     *
     * @param requestbody
     * @return
     * @throws IOException
     */
    public Map callS3RestAPI(MultiValueMap<String, Object> requestbody) throws IOException {
        Map responseMap = new HashMap();

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);

            HttpEntity<MultiValueMap<String, Object>> requestEntity
                    = new HttpEntity<>(requestbody, headers);

            ResponseEntity<Object> response = restTemplate.postForEntity(storageUrl, requestEntity, Object.class);

            if (Utils.isNotNull(response) && response.getStatusCode() == HttpStatus.OK) {
                if (Utils.isNotNull(response.getBody())) {
                    String responseString = response.getBody().toString();
                    if (responseString.contains("true")) {
                        int startIndex = responseString.indexOf("=") + 1;
                        int endIndex = responseString.indexOf(",");
                        String extractedUrl = responseString.substring(startIndex, endIndex);
                        responseMap = Utils.successResponse();
                        responseMap.put("url", extractedUrl);

                    } else {
                        responseMap = Utils.failureResponse();
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            responseMap = Utils.failureResponse();
        }

        return responseMap;
    }


}
