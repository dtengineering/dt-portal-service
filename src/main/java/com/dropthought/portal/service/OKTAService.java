package com.dropthought.portal.service;

import com.dropthought.portal.businessconfigurations.service.BusinessConfigurationsService;
import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.EmailTemplateUtility;
import com.dropthought.portal.util.TokenUtility;
import com.dropthought.portal.util.Utils;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class OKTAService extends BaseService{

    @Autowired
    @Qualifier("jdbcDt")
    JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("jdbcNamedDt")
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    private BusinessConfigurationsService businessConfigurationsService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private PdfService pdfService;

    @Autowired
    private PlanService planService;

    @Autowired
    TokenUtility tokenUtility;

    @Value("${dt.email.url}")
    private String emailUrl;

    @Value("${dtp.domain}")
    private String dtpDomain;


    public static String create_reset_pass="insert into reset_password (user_uuid, token, expiry_time) values (?, ?, ?)";
    public static String update_reset_pass="update reset_password set expiry_time = ? where token = ?";
//    public static String get_reset_pass_by_token="select user_uuid, expiry_time from reset_password where token = ?";
    public static String get_reset_pass_by_useruuid="select token, expiry_time from reset_password where user_uuid = ?";
//    public static String delete_reset_token="delete from reset_password where token = ?";

    public void updateOKTAParameters(Map queueParams) {
        logger.info("updateOKTAParameters begin");
        logger.info("queue parameters {}", new JSONObject(queueParams).toString());

        String userUUID = queueParams.containsKey("id") ? (String) queueParams.get("id") : "";
        String oktaUserId = queueParams.containsKey("oktaUserId") ? (String) queueParams.get("oktaUserId") : "";
        String oktaBusinesId = queueParams.containsKey("oktaBusinessId") ? (String) queueParams.get("oktaBusinessId") : "";
        String userRole =  queueParams.containsKey("role") ? (String) queueParams.get("role") : "";
        String email = queueParams.containsKey("email") ? (String) queueParams.get("email") : "";
        String firstName = queueParams.containsKey("firstname") ? (String) queueParams.get("firstname") : "";
        String lastName = queueParams.containsKey("lastname") ? (String) queueParams.get("lastname") : "";
        String fullName = firstName + " " + lastName;
        Map attributes = queueParams.containsKey("attributes") ? (Map) (queueParams.get("attributes")) : new HashMap();
        String businessId = attributes.containsKey("parent") && Utils.isNotNull(attributes.get("parent")) ?
                ((List) attributes.get("parent")).get(0).toString() : "";

        //update oktaUserId in users
        if (Utils.notEmptyString(userUUID) && Utils.notEmptyString(oktaUserId)) {
            updateUserIdWithOktaUserId(userUUID, oktaUserId);
        }
        //update oktaBusinessId in business
        if (Utils.notEmptyString(oktaBusinesId) && Utils.notEmptyString(oktaBusinesId) && queueParams.containsKey("attributes")) {
            updateBusinessIdWithOktaBusinessId(businessId, oktaBusinesId);
        }

        //DTV-12796 Exclude dt lite users from welcome email
        int businessIdInt = clientService.getBusinessIdByUUID(businessId);
        int planId = planService.getPlanIdByBusinessId(businessIdInt);

        if(planId != Constants.DT_LITE_PLAN_ID && Utils.notEmptyString(userRole) && !isKingUser(userUUID)){ // send welcome onboard email to read write/only and admin users. Excluding king user as it is covered in businessSendLoginMail().
            this.sendPasswordUpdateKingMail(email, fullName, userUUID, businessId, false, userRole);
        }
        logger.info("updateOKTAParameters end");

    }

    /**
     * method to update Okta userId in user by its uniqueId
     * @param userUniqueId
     * @param oktaUserId
     */
    public int updateUserIdWithOktaUserId(String userUniqueId, String oktaUserId){
        logger.info("updateUserIdWithOktaUserId begin");
        int result = 0;
        try {
            String updateUrl = "update users set okta_user_id = ? where user_uuid = ?";
            result = jdbcTemplate.update(updateUrl, new Object[]{oktaUserId, userUniqueId});
        }catch (Exception e){
            logger.info(" exception at updateUserIdWithOktaUserId. Msg {}", e.getMessage());
        }
        logger.info("updateUserIdWithOktaUserId end");
        return result;
    }

    /**
     * method to update Okta busienssId in buisness by businesUUID
     * @param businessUniqueId
     * @param oktaBusinessId
     */
    public int updateBusinessIdWithOktaBusinessId(String businessUniqueId, String oktaBusinessId){
        logger.info("updateBusinessIdWithOktaBusinessId begin");
        int result = 0;
        try {
            String updateUrl = "update business set okta_tenant_id = ? where business_uuid = ?";
            result = jdbcTemplate.update(updateUrl, new Object[]{oktaBusinessId, businessUniqueId});
        }catch (Exception e){
            logger.info(" exception at updateBusinessIdWithOktaBusinessId. Msg {}", e.getMessage());
        }
        logger.info("updateBusinessIdWithOktaBusinessId end");
        return result;
    }

    /**
     * method to store request and response of OKTA user updation
     * @param inputMap
     * @param responseMap
     */
    public void createUserUpdateResponse(Map inputMap, Map responseMap, String userId, String businessId, String action){
        logger.info("create temp user update starts");
        boolean isTableExist = createTempOktaTable();
        if(isTableExist) {
            String INSERT_QUERY = "INSERT INTO temp_okta_user (user_id, business_id, request, response, action) values(?,?,?,?,?)";
            try {
                jdbcTemplate.update(INSERT_QUERY, new Object[]{userId, businessId, new JSONObject(inputMap).toString(),
                        new JSONObject(responseMap).toString(), action});
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }else{
            logger.error("table not exist");
        }
        logger.info("create temp user update ends");
    }

    /**
     * method to check if user is king user
     * @param userUniqueId
     */
    public boolean isKingUser(String userUniqueId){
        logger.info("isKingUser begin");
        try {
            String updateUrl = "select user_role from users where user_uuid = ?";
           int kingRole = jdbcTemplate.queryForObject(updateUrl, new Object[]{userUniqueId}, Integer.class);
           if(kingRole == 1){
               return true;
           }
        }catch (Exception e){
            logger.info(" exception at isKingUser. Msg {}", e.getMessage());
        }
        logger.info("isKingUser end");
        return false;
    }

    /**
     * method to create temp table for okta user
     */
    public boolean createTempOktaTable() {
        int count = jdbcTemplate.queryForObject(
                "select count(1) from information_schema.tables where table_schema = ? AND table_name =?",
                new Object[]{"DT", "temp_okta_user"}, Integer.class);

        if (count == 1) {
            return true;
        } else {
            try {
                logger.info("begin creating okta temp user table");
                String createUserSurveySql = "CREATE TABLE IF NOT EXISTS temp_okta_user ( " +
                        "`id` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY, " +
                        "`user_id` varchar(100) NULL, " +
                        "`business_id` varchar(100) NULL, " +
                        "`request` json DEFAULT NULL, " +
                        "`response` json DEFAULT NULL, " +
                        "`action` varchar(50) NULL, " +
                        "`created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP) " +
                        "ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8 ";
                int createTableResult = jdbcTemplate.update(createUserSurveySql);
                logger.info("Create table result {}", createTableResult);
                logger.info("end creating okta temp user table");
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
    }


    /**
     * Send password update mail for newly created business
     *
     * @param kingUserEmail
     * @param recipientName
     */
    public void sendPasswordUpdateKingMail(String kingUserEmail, String recipientName,
                                           String userUUID, String businessUUID, boolean isKingUser, String role) {
        try {
            Map mailParam = businessConfigurationsService.getBusinessConfigMailParams(businessUUID);
            mailParam.put("toEmail", kingUserEmail);
            mailParam.put("recipientName", recipientName);
            String language = Constants.DEFAULT_LANGUAGE;
            logger.info("dtapp base {}", dtpDomain);
            String host = dtpDomain.contains("localhost") ? "http://localhost:4200" : "https://" + dtpDomain;
            logger.info("host {}", host);
            String randomToken = this.saveResetPasswordToken(userUUID, true);

            int businessId = clientService.getBusinessIdByUUID(businessUUID);
            String sso = businessConfigurationsService.getSSOConfigByBusinessId(businessId);
            String link = host + "/" + language + (Utils.emptyString(sso) ? "/savepassword/" + randomToken : "");

            logger.info("link {}", link);
            mailParam.put("link", link);
            mailParam.put("userName", kingUserEmail);
            mailParam.put("buttonVerbiage", Utils.notEmptyString(sso) ? Utils.getWelcomeButtonVerbiage(sso) : Constants.SET_PASSWORD);//KING_BUTTON_VERBIAGE);
            mailParam.put("oktaRole", role);
            mailParam.put("EnvironmentInfo", EmailTemplateUtility.getEnvironmentEmailContent(host, false));
            mailParam.put("kickstartMessage", Utils.getWelcomeMsg(sso));

            this.sendPasswordUpdateMail(mailParam, isKingUser, businessUUID);

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * send king password update instruction mail
     *
     * @param paramMap
     */
    public void sendPasswordUpdateMail(Map paramMap, boolean isKing, String businessId) {
        try {
            logger.info("begin send king new user password update");
            Map emailSettingsMap = this.getBusinessEmailSettings(businessId);
            Map businessEmailMap = emailSettingsMap.containsKey("business_email") && Utils.notEmptyString(emailSettingsMap.get("business_email").toString()) ? (Map) emailSettingsMap.get("business_email") : new HashMap();
            String passwordUpdateMail = businessEmailMap.containsKey("passwordUpdateMail") && Utils.notEmptyString(businessEmailMap.get("passwordUpdateMail").toString()) ? businessEmailMap.get("passwordUpdateMail").toString() : Constants.SENDER_EMAIL;
            String fromName = businessEmailMap.containsKey("passwordUpdateFromName") && Utils.notEmptyString(businessEmailMap.get("passwordUpdateFromName").toString()) ? businessEmailMap.get("passwordUpdateFromName").toString() : Constants.SENDER_NAME;
            String fromEmailId = passwordUpdateMail;
            String ccEmail = "cs@dropthought.com";
            String toEmailId = paramMap.containsKey("toEmail") ? paramMap.get("toEmail").toString() : "";
            String recipientName = paramMap.containsKey("recipientName") ? paramMap.get("recipientName").toString() : "User";
            String role = paramMap.containsKey("oktaRole") ? paramMap.get("oktaRole").toString() : "viewer";
            String environmentInfo = paramMap.containsKey("EnvironmentInfo") ? paramMap.get("EnvironmentInfo").toString() : "";

            String attachment = "";
            String attachmentType = "";
            String attachmentName = "";
            String template = role.equals("viewer") ? Constants.READ_ONLY_USER_KICKSTART_TEMPLATE : Constants.KING_ADMIN_READWRITE_TEMPLATE;//Constants.PASSWORD_UPDATE_TEMPLATE;
            paramMap.put("role",  role);

            //if (isKing) {
                String encodedPDF = pdfService.generateSettingsAttachmentPDF(businessId);
                if (Utils.notEmptyString(encodedPDF)) {
                    attachment = encodedPDF;
                    attachmentType = "pdf";
                    attachmentName = businessId + ".pdf";
                }
            //}

            if (Utils.notEmptyString(toEmailId)) {
                if (isKing) {
                    template = Constants.KING_ADMIN_READWRITE_TEMPLATE;//KING_NEW_LOGIN_TEMPLATE;
                    //paramMap.put("role", Constants.KING_ROLE);
                    paramMap.put("role", Constants.SUPER_ADMIN_ROLE);
                }
                String subject = Constants.WELCOME_ABOARD_MESSAGE;//Constants.KING_NEW_USER_SUBJECT;
                //get email template file for notification
                String htmlContent = getFileContent(template);
                //set the dynamic param value to replace in template file
                paramMap.put("logo", Constants.DEFAULT_LOGO);
                paramMap.put("recipientName", recipientName);
                paramMap.put("EnvironmentInfo", environmentInfo);

                //substitute variable like link, recipient name, username, password etc in email template with corresponding values
                StrSubstitutor sub = new StrSubstitutor(paramMap);
                String resolvedString = sub.replace(htmlContent);

                //call email utility api
                Map emailMap = new HashMap();
                emailMap.put("from", fromEmailId);
                emailMap.put("to", toEmailId);
                emailMap.put("subject", subject);
                emailMap.put("body", resolvedString);
                emailMap.put("name", fromName);

                //validation to prevent  exceptions in email service
                if (Utils.notEmptyString(attachment) && Utils.notEmptyString(attachmentName)//isKing &&
                        && Utils.notEmptyString(attachmentType)) {
                    emailMap.put("attachment", attachment);
                    emailMap.put("attachmentType", attachmentType);
                    emailMap.put("attachmentName", attachmentName);
                }
                if(Utils.notEmptyString(ccEmail)) {
                    List ccList = new ArrayList();
                    ccList.add(ccEmail);
                    emailMap.put("cc", ccList);
                }
                Map resp = this.callRestApi(emailMap, emailUrl, HttpMethod.POST);
                logger.info("End send king new user email");
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * method to fetch customized `from email` settings of a business
     *
     * @param businessId
     * @return
     */
    public Map getBusinessEmailSettings(String businessId) {
        Map resultMap = new HashMap();
        try {
            //query to fetch `from email` settings of a business
            String getFromEmailSettingSql = "SELECT business_email, trigger_email, alert_email, notification_email FROM email_settings WHERE business_uuid = ?";
            resultMap = jdbcTemplate.queryForMap(getFromEmailSettingSql, new Object[]{businessId});

            //sanitize result map
            if(resultMap.size() > 0) {
                Map businessEmail = mapper.readValue(resultMap.get("business_email").toString(), HashMap.class);
                resultMap.remove("business_email");
                resultMap.put("business_email", businessEmail);

                Map triggerEmail = mapper.readValue(resultMap.get("trigger_email").toString(), HashMap.class);
                resultMap.remove("trigger_email");
                resultMap.put("trigger_email", triggerEmail);

                Map alertEmail = mapper.readValue(resultMap.get("alert_email").toString(), HashMap.class);
                resultMap.remove("alert_email");
                resultMap.put("alert_email", alertEmail);

                Map notificationEmail = mapper.readValue(resultMap.get("notification_email").toString(), HashMap.class);
                resultMap.remove("notification_email");
                resultMap.put("notification_email", notificationEmail);
            }
        } catch (Exception e) {
            logger.info("No existing customized `from email` settings found for business {} ", businessId);
        }
        return resultMap;
    }

    /**
     * method to call rest api for email/sms
     * @param inputMap
     * @param uri
     */
    public  Map callRestApi(Map inputMap, String uri, HttpMethod method){
        Map respMap = new HashMap();

        try {

            JSONObject json = new JSONObject(inputMap);
            String inputJson = json.toString();
            logger.debug("input json {}",inputJson);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);

            logger.debug("uri {}",uri);


            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
            ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, method, entity, Object.class);
            logger.info("response entity {}",responseEntity);

            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info(responseEntity.getBody().toString());
                    respMap = (Map) responseEntity.getBody();

                }
            }
        }
        catch (Exception e){
            //e.printStackTrace();
            logger.error(e.getMessage());
        }
        return  respMap;
    }

    /**
     * method to get email template file in string format
     *
     * @param template
     * @return
     */
    private String getFileContent(String template) {
        StringBuilder result = new StringBuilder();

        try {
            InputStream in = this.getClass().getClassLoader().getResourceAsStream("email/" + template);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            for (String line; (line = reader.readLine()) != null; ) {
                result.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return result.toString();
    }

    /**
     * Store useruuid and token and time stamp for reset password
     *
     * @param userUUID
     * @return
     */
    public String saveResetPasswordToken(String userUUID, boolean welcomeMail) {
        String updatedToken = "";
        try {
            String randomToken = tokenUtility.generateUUID();
            //get by uuid
            Map userData = this.resetUserDataByUser(userUUID);

            //DTV-5712 Increase welcome email expiry time to 48 hrs, Forgot pass keep same 30 min.
            int expiryThreshold = welcomeMail ? Utils.WELCOME_EMAIL_EXPIRY : Utils.RESET_PASS_EXPIRY;

            //within 5 min if your request multiple reset password, we will create new token for reset link.
            //If user has only one token in reset_password table and token also expired, then we will update the same token with expiry time.
            if (userData.size() == 1) {
                boolean tokenExpired = false;
                String token = (userData.containsKey("token")) ? userData.get("token").toString() : "";
                String existExpiryTime = (userData.containsKey("expiry_time")) ? userData.get("expiry_time").toString() : "";

                String currentDateTime = Utils.getCurrentUTCDateAsString();
                int resetExpiryMin = (int) Utils.differenceMinutes(existExpiryTime, currentDateTime);

                tokenExpired = resetExpiryMin >= expiryThreshold;

                if (tokenExpired) {
                    //update
                    this.updateExpiryTokenTime(token, expiryThreshold);
                    updatedToken = token;
                } else {
                    //insert
                    this.createResetPassword(userUUID, randomToken, expiryThreshold);
                    updatedToken = randomToken;
                }
            } else {
                //insert
                this.createResetPassword(userUUID, randomToken, expiryThreshold);
                updatedToken = randomToken;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return updatedToken;
    }

    /**
     * Get reset user data by useruuid
     *
     * @param userUUID
     * @return
     */
    public Map resetUserDataByUser(String userUUID) {
        Map resetUserData = new HashMap();
        try {
            List resetRecords = jdbcTemplate.queryForList(get_reset_pass_by_useruuid, new Object[]{userUUID});
            if (resetRecords.size() > 0) {
                resetUserData = (Map) resetRecords.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resetUserData;
    }

    /**
     * Insert reset password entry
     *
     * @param userUUID
     * @param randomToken
     * @return
     */
    public int createResetPassword(String userUUID, String randomToken, int expiryThreshold) {
        int result = 0;
        try {
            //insert resetpassword token
            String expiryTime = Utils.addMinutesToDate(expiryThreshold);
            result = jdbcTemplate.update(create_reset_pass, new Object[]{userUUID, randomToken, expiryTime});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * update expiry time by token
     *
     * @param randomToken
     * @return
     */
    public int updateExpiryTokenTime(String randomToken, int expiryThreshold) {
        int result = 0;
        try {
            //update resetpassword token
            String expiryTime = Utils.addMinutesToDate(expiryThreshold);
            result = jdbcTemplate.update(update_reset_pass, new Object[]{expiryTime, randomToken});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
