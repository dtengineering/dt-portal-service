package com.dropthought.portal.service;

import com.dropthought.portal.util.TokenUtility;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

@Component
public class DeviceService extends BaseService{

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final String insertDeactivatedKiosk = "insert into deactivate_kiosks(business_id, allowed_number, difference_number, created_time) values(?,?,?,?)";
    private static final String insertTrackConfig = "insert into track_business_config(business_id, parameter, created_time, track_uuid) values(?,?,?,?)";

    @Autowired
    ClientService clientService;

    @Autowired
    TokenUtility tokenUtility;

    /**
     * JdbcTemplate for DB Connectivity.
     */
    @Autowired
    @Qualifier("jdbcDt")
    JdbcTemplate jdbcTemplate;

    @Value("${dt.kioskblock.key}")
    protected String kioskKey;

    @Value("${dtp.domain}")
    private String dtpDomain;

    /**
     * method to call rest api for block kiosk
     * @param inputMap
     * @param uri
     */

    public Map callRestApi(Map inputMap, String uri, HttpMethod method, String key){
        Map respMap = new HashMap();

        try {

            JSONObject json = new JSONObject(inputMap);
            String inputJson = json.toString();
            logger.debug("input json {}",inputJson);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("x-api-key",key);
            HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);

            logger.info("uri {}",uri);


            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
            ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, method, entity, Object.class);
            logger.info("response entity {}",responseEntity);

            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info(responseEntity.getBody().toString());
                    respMap = (Map) responseEntity.getBody();

                }
            }
        }
        catch (Exception e){
            //e.printStackTrace();
            logger.error(e.getMessage());
        }
        return  respMap;
    }

    public void updateDeactivatedKiosks(String businessUUID, int existNumberKiosk, int updatedNumberKiosk)
    {
        try {
            int differenceNumber = Math.abs(existNumberKiosk - updatedNumberKiosk);
            int business_id = clientService.getBusinessIdByUUID(businessUUID);
            String tableName = "deactivate_kiosks";
            Timestamp currentTimeToInsert = new Timestamp(System.currentTimeMillis());
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(insertDeactivatedKiosk, Statement.RETURN_GENERATED_KEYS);
                    ps.setInt(1, business_id);
                    ps.setInt(2, updatedNumberKiosk);
                    ps.setInt(3, differenceNumber);
                    ps.setTimestamp(4, currentTimeToInsert);
                    return ps;
                }
            }, keyHolder);
            logger.info("insert into deactivate kiosk done");
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Insert into track_business_config for trackParam (Ex: noOfKiosk, noOfMetrics)
     */
    public void insertBusinessTrackConfig(int businessId, String trackParam)
    {
        try {
            String tableName = "track_business_config";
            Timestamp currentTimeToInsert = new Timestamp(System.currentTimeMillis());
            final String trackConfigUUID = tokenUtility.generateUUID();
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(insertTrackConfig, Statement.RETURN_GENERATED_KEYS);
                    ps.setInt(1, businessId);
                    ps.setString(2, trackParam);
                    ps.setTimestamp(3, currentTimeToInsert);
                    ps.setString(4, trackConfigUUID);
                    return ps;
                }
            }, keyHolder);
            logger.info("insert into track business config noOfKiosk done");
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    //https://t9kkw4gx2l.execute-api.us-east-1.amazonaws.com/api/business/4276bf86-763a-463c-9214-23ac9f08ae1c/block   stage
    //https://ijx00k49q7.execute-api.us-east-1.amazonaws.com/api/business/4276bf86-763a-463c-9214-23ac9f08ae1c/block   prod
    public Map computeUriandApiKey(String hostname){
        Map returnMap = new HashMap();
        String kioskKey = "";
        String uriKey = "";
        try{
            if(hostname.contains("test")){
                kioskKey = "04mK9GiXUZ8urmMBJLPvN5VNnMzvmzU6fqevdOBb";
                uriKey = "https://t9kkw4gx2l.execute-api.us-east-1.amazonaws.com/api/business/";
            }else if(hostname.contains("stage")){
                kioskKey = "04mK9GiXUZ8urmMBJLPvN5VNnMzvmzU6fqevdOBb";
                uriKey = "https://t9kkw4gx2l.execute-api.us-east-1.amazonaws.com/api/business/";
            }else if(hostname.contains("automation")){
                kioskKey = "04mK9GiXUZ8urmMBJLPvN5VNnMzvmzU6fqevdOBb";
                uriKey = "https://t9kkw4gx2l.execute-api.us-east-1.amazonaws.com/api/business/";
            } else if(hostname.contains("sandbox")){
                kioskKey = "Lm1RnSweIJ70JZdwO1W1S6R591LxTbgF43Zcj8cS";
                uriKey = "https://ijx00k49q7.execute-api.us-east-1.amazonaws.com/api/business/";
            }else{
                kioskKey = "Lm1RnSweIJ70JZdwO1W1S6R591LxTbgF43Zcj8cS";
                uriKey = "https://ijx00k49q7.execute-api.us-east-1.amazonaws.com/api/business/";
            }
            returnMap.put("kioskKey",kioskKey);
            returnMap.put("url",uriKey);
        }catch (Exception e){
            e.printStackTrace();
        }
        return returnMap;
    }

    /**
     * method to kiosk registration count of kiosk
     * @param businessId
     * @return
     */
    public int getKiosKRegistrationCount(int businessId){
        try {
            String registeredNumSql = "select count(*) from device_registration where business_id = ?";
            return jdbcTemplate.queryForObject(registeredNumSql, new Object[]{businessId}, Integer.class);
        }catch (Exception e){
            logger.error("Exception at getKiosKRegistrationCount. Msg {}", e.getMessage());
        }
        return 0;
    }

}
