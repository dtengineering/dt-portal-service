package com.dropthought.portal.service;


import com.dropthought.portal.model.UserAuthenticationBean;
import com.dropthought.portal.model.UserBean;
import com.dropthought.portal.util.CryptoUtility;
import com.dropthought.portal.util.TokenUtility;
import com.dropthought.portal.util.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Created by Prabhakar Thakur on 10/7/2020.
 */
@Component
public class UserService {


    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    protected CryptoUtility cryptoUtility;

    @Autowired
    private TokenUtility tokenUtility;

    //@Autowired
    //private JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("jdbcDt")
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private UserAuthenticationService userAuthenticationService;

    @Autowired
    @Qualifier("jdbcPortal")
    private JdbcTemplate portalJdbcTemplate;

    @Autowired
    @Qualifier("jdbcNamedPortal")
    private NamedParameterJdbcTemplate portalNamedParameterJdbcTemplate;



    public int createUser(final UserBean userBean) {

        logger.info("Create user");
        Integer userId = 0;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        final String createdTimeString = simpleDateFormat.format(timestamp);
        final String hashedPassword = cryptoUtility.computeSHAHash(userBean.getPassword());

        KeyHolder keyHolder = new GeneratedKeyHolder();
        final String userUUID = tokenUtility.generateUUID();
        String create_user="insert into users (full_name, first_name, last_name, username, user_email, password, phone, confirmation_status, created_time, modified_time, user_uuid) values (?,?,?,?,?,?,?,?,?,?,?)";

        try {
            logger.info("execute the sql query to create user");
            portalJdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(create_user, Statement.RETURN_GENERATED_KEYS);

                    ps.setString(1, userBean.getFullName());
                    ps.setString(2, userBean.getFirstName());
                    ps.setString(3, userBean.getLastName());
                    ps.setString(4, userBean.getUserName());
                    ps.setString(5, userBean.getUserEmail());
                    ps.setString(6, hashedPassword);
                    ps.setString(7, userBean.getPhone());
                    ps.setString(8, userBean.getConfirmationStatus().toString());
                    ps.setTimestamp(9, timestamp);
                    ps.setTimestamp(10, timestamp);
                    ps.setString(11, userUUID);

                    return ps;
                }
            }, keyHolder);
            userId = keyHolder.getKey().intValue();
            logger.info("user id " + userId);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return userId;

    }

    /**
     * Function to check the mandatory values of a UserBean are present
     * @author Prabhakar Thakur
     * @param userBean
     * @return
     */
    public Boolean checkMandatoryUserValues(UserBean userBean) {
        Boolean value = false;

        String fullName = userBean.getFullName();
        String userEmail = userBean.getUserEmail();
        String confirmationStatus = userBean.getConfirmationStatus().toString();

        if (Utils.notEmptyString(fullName) && Utils.notEmptyString(userEmail) && Utils.notEmptyString(confirmationStatus)) {
            value = true;
        }

        return value;
    }

    /**
     * Function to check the existing user
     *
     * @param userEmail
     * @author Prabhakar Thakur
     * @retun
     */

    public int checkExistingUser(String userEmail) {
        logger.info("begin check existing user");
        try {

            Integer userId = 0;
            String userSql= "select user_id from users where user_email = ?";
            List userList = portalJdbcTemplate.queryForList(userSql, new Object[]{userEmail}, Integer.class);

            if (userList.size() > 0) {
                userId = (Integer) userList.get(0);
                logger.info("end check existing user");
                return userId;
            } else {
                logger.info("end check existing user");
                return 0;
            }
        } catch (EmptyResultDataAccessException e) {
            return 0;
        }

    }

    /**
     * @param userId
     * @return
     */
    public Map retrieveUserByUserId(int userId) {
        Map userMap = new HashMap();
        //logger.info("begin retrieve user by user id");
        String userQuery="select * from users where user_id = ?";
        try {
                userMap = portalJdbcTemplate.queryForMap(userQuery, new Object[]{userId});
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        //logger.info("end retrieve user by user id");
        logger.debug("end retrieve user by user id");
        return userMap;
    }

    /**
     * Update user by id
     * @param userBean
     * @return
     */
    public int updateUserStatusByEmail(UserBean userBean) {

        String update_user_status_by_email = "update users set user_status=? , phone=? where user_email=?";
        String update_password="update users set password=?,modified_time=? where user_id = ?";
        int result = portalJdbcTemplate.update(update_user_status_by_email,
                new Object[]{"1" , userBean.getPhone(), userBean.getUserEmail()});
        if(result > 0) {
            logger.info("Generating random password for the user");
            String pasword = tokenUtility.generateRandomString(10);
            logger.info("Generated password " + pasword);
            String hashedPassword = cryptoUtility.computeSHAHash(pasword);
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            int updatePassword = portalJdbcTemplate.update(update_password,
                    new Object[]{hashedPassword, timestamp, userBean.getUserId()});
            if(updatePassword > 0) {
                UserAuthenticationBean userAuthenticationBean = new UserAuthenticationBean();
                userAuthenticationBean.setConfirmToken(userBean.getUserUUID());
                userAuthenticationBean.setVerifyEmail(userBean.getUserEmail());
                userAuthenticationBean.setVerifyName(userBean.getFullName());
                userAuthenticationBean.setVerifyUid(userBean.getUserId());
                userAuthenticationBean.setHost(userBean.getHost());
                userAuthenticationBean.setUserName(userBean.getFullName());
                userAuthenticationBean.setPassword(pasword);
                userAuthenticationService.sendLoginEmail(userAuthenticationBean);
            }
        }
        return result;
    }

    /**
     * Function to get all users
     *
     * @return
     */
    public List getUser() {
        List result = null;
        String select_users="select * from users";
        try {
            result = portalJdbcTemplate.queryForList(select_users);
            logger.info("Get user information API");

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        //logger.info(result);
        return result;
    }


    public List getUserByUUID(String userUUID) {
        List result = new ArrayList<UserBean>();
        String select_user_by_uuid ="select user_id,full_name,first_name,last_name,username,user_email,password,phone,confirmation_status,password_update_count," +
                                    "created_by,created_time,modified_time,modified_by,user_uuid from users where user_uuid = ?";
        try {

            result = portalJdbcTemplate.queryForList(select_user_by_uuid, new Object[]{userUUID});

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return result;
    }

    public int countUserId(int userId)
    {
        String userIdCountSql ="select count(user_id) from users where user_id= ?" ;
        int userIdCount=portalJdbcTemplate.queryForObject(userIdCountSql, new Object[]{userId},Integer.class);
        return userIdCount;
    }

    /**
     * Function to delete user based upon userId
     *
     * @param userId
     * @return
     */

    public boolean deleteUser(int userId) {

        boolean result = false;
        if(countUserId(userId)>0) {
            result = true;
        } else {
            result = false;
        }

        String userSql ="delete from users where user_id= ?" ;
        try {
                portalJdbcTemplate.update(userSql, new Object[]{userId});
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return result;

    }

    public List getUserById(Integer userId) {
        List result = new ArrayList<UserBean>();
        String select_user_by_id ="select user_id,full_name,first_name,last_name,username,user_email,password,phone,confirmation_status,password_update_count," +
                "created_by,created_time,modified_time,modified_by,user_uuid from users where user_id = ?";
        try {

            result = portalJdbcTemplate.queryForList(select_user_by_id, new Object[]{userId});

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return result;
    }


    /**
     * Update user by id
     *
     * @param userId
     * @param userBean
     * @return
     */
    public int updateUserById(Integer userId, UserBean userBean) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        int result = 0;
        String sql = "update users set ";
        ArrayList<Object> objectsList = new ArrayList<Object>();
        try {

            if (userBean.getPassword() != null) {
                String hashedPassword = cryptoUtility.computeSHAHash(userBean.getPassword());
                objectsList.add(hashedPassword);
                sql = sql + "password =?, ";
                incrementPasswordUpdateCount(userId);
            }
            if (userBean.getFullName() != null) {
                objectsList.add(userBean.getFullName());
                sql = sql + "full_name =?, ";
            }
            if (userBean.getFirstName() != null) {
                objectsList.add(userBean.getFirstName());
                sql = sql + "first_name =?, ";
            }
            if (userBean.getLastName() != null) {
                objectsList.add(userBean.getLastName());
                sql = sql + "last_name =?, ";
            }
            if (userBean.getUserName() != null) {
                objectsList.add(userBean.getUserName());
                sql = sql + "username =?, ";
            }
            if (userBean.getUserEmail() != null) {
                objectsList.add(userBean.getUserEmail());
                sql = sql + "user_email =?, ";
            }
            if (userBean.getPhone() != null) {
                objectsList.add(userBean.getPhone());
                sql = sql + "phone =?, ";
            }
            if (userBean.getConfirmationStatus() != null) {
                objectsList.add(userBean.getConfirmationStatus().toString());
                sql = sql + "confirmation_status =?, ";
            }

            if (objectsList.size() > 0) {
                //logger.info("Modifying the user");
                String userUUID = userBean.getUserUUID();
                //logger.info("Modifying the user" + userUUID);
                Map userMap = null;
                int modifiedBy=0;
                if (userUUID != null) {
                    userMap = retrieveUserByUserUUID(userUUID);
                    modifiedBy = (int) userMap.get("user_id");
                }
                objectsList.add(modifiedBy);
                sql = sql + "modified_by =?, ";

                objectsList.add(timestamp);
                sql = sql + "modified_time =?, ";

                sql = sql.substring(0, sql.length() - 2);
                sql = sql + " where user_id = ?";
                logger.info("Sql " + sql);
                objectsList.add(userId.toString());
                Object[] objectArray = objectsList.toArray();

                result = portalJdbcTemplate.update(sql, objectArray);
                logger.info("update result " + result);
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return result;
    }


    /**
     * Increment password update count for the user
     *
     * @param userId
     * @return
     */
    public int incrementPasswordUpdateCount(Integer userId) {
        logger.info("begin increment the password update count");
        String increment_password="update users set password_update_count=password_update_count + 1 where user_id = ?";
        logger.info("end increment the password update count");
        return portalJdbcTemplate.update(increment_password, new Object[]{userId});
    }

    /**
     * retrieve user map based upon user uuid
     *
     * @param userUUID
     * @return
     */
    public Map retrieveUserByUserUUID(String userUUID) {
        Map userMap = new HashMap();
        String select_user_by_uuid ="select user_id,full_name,first_name,last_name,username,user_email,password,phone,confirmation_status,password_update_count," +
                "created_by,created_time,modified_time,modified_by,user_uuid from users where user_uuid = ?";
        try {
            logger.info("User uuid " + userUUID);
            userMap = portalJdbcTemplate.queryForMap(select_user_by_uuid, new Object[]{userUUID});
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return userMap;
    }

    /**
     * get cs users by business id
     * @param businessId
     * @return
     */
    public List getCSUsersByBusinessId(int businessId){
        List csUsers = new ArrayList();
        try{
             String csUsersSql = "select u.user_id, u.country_id, u.full_name, u.first_name, u.last_name, u.username, u.user_db, u.user_email, u.user_uuid, u.password, u.phone, u.trial_flag, u.complete_social_signup, u.confirmation_status, u.password_update_count, u.created_by, u.created_time, u.modified_time, u.modified_by, u.user_status, u.eula_status, u.push_notification, u.thoughtful, u.title, u.cs_start_date, u.cs_end_date, u.user_role, u.alias_email, a.account_id, a.account_type, a.signup_type_id, a.account_user_id, a.account_business_id, a.account_role_id, a.account_country_id, a.account_emp_range from users  as u  left join account as a on u.user_id = a.account_user_id where user_role = '2' and account_business_id = ? order by created_time desc";
             csUsers = jdbcTemplate.queryForList(csUsersSql, new Object[]{businessId});
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return csUsers;
    }
}