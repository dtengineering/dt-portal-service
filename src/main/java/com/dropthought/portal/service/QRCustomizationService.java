package com.dropthought.portal.service;

import com.dropthought.portal.dao.QRCustomizationDAO;
import com.dropthought.portal.model.CustomizeQRCodeBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
@Component
public class QRCustomizationService  extends BaseService {

    @Autowired
    QRCustomizationDAO qrCustomizationDAO;
  /**
     * Method to get qr_customization column from tag_links_buuid table using taglinkId and business uuid
     * @param tagLinkUniqueId
     * @param bean
     * @param businessUUID
     */
    public CustomizeQRCodeBean getTagLinkQRCustomizationByTagId(String tagLinkUniqueId, String businessUUID){
        logger.info("getTagLinkQRCustomizationByTagId starts");
        CustomizeQRCodeBean customizeQRCodeBean = null;
        try {
            Map qrCustomizationMap = qrCustomizationDAO.getTagLinkQRCustomizationByTagId(tagLinkUniqueId, businessUUID);
            if(qrCustomizationMap != null) {
                String frontColor = qrCustomizationMap.containsKey("foreColor") ? qrCustomizationMap.get("foreColor").toString() : "";
                String backColor = qrCustomizationMap.containsKey("backColor") ? qrCustomizationMap.get("backColor").toString() : "";
                String logoFile = qrCustomizationMap.containsKey("logoUrl") ? qrCustomizationMap.get("logoUrl").toString() :
                        qrCustomizationMap.containsKey("logoBase64") ? qrCustomizationMap.get("logoBase64").toString() : "";
                customizeQRCodeBean = new CustomizeQRCodeBean();
                customizeQRCodeBean.setBackColor(backColor);
                customizeQRCodeBean.setForeColor(frontColor);
                customizeQRCodeBean.setLogoUrl(logoFile);
            }
            logger.debug("result: " + qrCustomizationMap);
        }
        catch (Exception e){
            logger.error("Error in getTagLinkQRCustomizationByTagId() : " + e.getMessage());
        }
        logger.info("getTagLinkQRCustomizationByTagId ends");
        return customizeQRCodeBean;
    }
}
