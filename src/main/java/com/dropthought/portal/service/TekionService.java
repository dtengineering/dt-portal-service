package com.dropthought.portal.service;

import com.dropthought.portal.dao.TekionDAO;
import com.dropthought.portal.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class TekionService extends BaseService {
    @Value("${dtp.url}")
    private String dtAppUrl;

    @Value("${tekion.url}")
    private String tekionUrl;

    @Autowired
    private SchedulerService schedulerService;

    @Autowired
    private TekionDAO tekionDAO;

    /**
     * Function to update tekion repair orders
     */
    public void updateTekionRepairOrders() {
        try {
            //fetch dealers list
            List<Map<String, Object>> resultList = tekionDAO.getDealersList();
            for (Map<String, Object> eachMap : resultList) {
                String dealerId = eachMap.get("dealer_id").toString();
                int businessId = Integer.valueOf(eachMap.get("business_id").toString());
                long startTime = Utils.isNotNull(eachMap.get("last_executed_time")) ? (long) eachMap.get("last_executed_time") : 0;
                //get repair orders from Tekion
                Map repairOrders = getTekionRepairOrders(dealerId, businessId, startTime);
                if (!repairOrders.isEmpty()) {
                    List<Map<String, Object>> dataList = repairOrders.containsKey("data") && Utils.isNotNull(repairOrders.get("data")) ? (List<Map<String, Object>>) repairOrders.get("data") : new ArrayList<>();
                    if (!dataList.isEmpty()) {
                        //insert repair orders into database
                        int insert = tekionDAO.insertRepairOrderDetails(businessId, dataList);
                        if (insert > 0) {
                            logger.info("Repair orders inserted successfully");
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception at updateTekionRepairOrders() - {} ", e.getMessage());
        }
    }

    /**
     * Function to get repair orders from Tekion
     *
     * @param dealerId
     * @return
     */
    private Map getTekionRepairOrders(String dealerId, int businessId, long startTime) {
        Map resultMap = new HashMap();
        try {
            String uri = tekionUrl;
            //get access token
            Map tokenMap = retrieveTekionAccessToken();
            String accessToken = tokenMap.get("access_token").toString();
            String app_id = tokenMap.get("app_id").toString();
            HttpHeaders headers = new HttpHeaders();
            //set Authorization header
            headers.set("Authorization", "Bearer " + accessToken);
            //set app_id header
            headers.set("app_id", app_id);
            //set dealer_id header
            headers.set("dealer_id", dealerId);
            //set request entity
            HttpEntity requestEntity = new HttpEntity(headers);
            //set start time to yesterday and end time to current time in milliseconds in UTC
            //long startTime = tekionDAO.getPreviouslyExecutedTime(dealerId, businessId, "repairOrders");
            startTime = startTime > 0 ? startTime : Utils.getPreviousDayTimeMillisecondsUTC();
            long endTime = Utils.getCurrentTimeMillisecondsUTC();
            //long startTime = 1709522201000L;
            //long endTime = 1710127001000L;
            logger.info("Start Time {} End Time {}", startTime, endTime);
            //set uri
            uri = uri + "v3.1.0/repair-orders?startTime=" + startTime + "&endTime=" + endTime + "&status=CLOSED";
            //call tekion api
            resultMap = tekionApiCall(uri, requestEntity, HttpMethod.GET);
            logger.info("Tekion repair orders response {}", resultMap);
            //update last executed time
            tekionDAO.updateLastExecutedTime(dealerId, endTime, businessId, "repairOrders");
        } catch (Exception e) {
            logger.error("Exception at getTekionRepairOrders() - {} ", e.getMessage());
        }
        return resultMap;
    }

    /**
     * Function to retrieve tekion access token
     *
     * @return
     */
    private Map retrieveTekionAccessToken() {
        Map tokenMap = new HashMap();
        try {
            //get app_id and app_secret
            Map inputMap = tekionDAO.getTekionKeys();
            String app_id = (String) inputMap.get("app_id");
            String app_secret = (String) inputMap.get("app_secret");
            //set headers
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            //form parameters
            MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
            parameters.add("app_id", app_id);
            parameters.add("secret_key", app_secret);
            //set request entity
            HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(parameters, headers);
            //call tekion api
            String uri = tekionUrl + "public/tokens";
            Map responseMap = tekionApiCall(uri, requestEntity, HttpMethod.POST);
            if (responseMap.containsKey("data")) {
                //get access token
                Map dataMap = (Map) responseMap.get("data");
                String accessToken = dataMap.get("access_token").toString();
                tokenMap.put("access_token", accessToken);
                tokenMap.put("app_id", app_id);
            }
        } catch (Exception e) {
            logger.error("Exception at retrieveTekionAccessToken() - {} ", e.getMessage());
        }
        return tokenMap;
    }

    /**
     * Method to call a service using rest template
     *
     * @param url
     * @param entity
     * @param method
     * @return
     */
    public Map tekionApiCall(String url, HttpEntity entity, HttpMethod method) {
        Map resultMap = new HashMap();
        try {
            try {
                ResponseEntity<String> responseEntity = restTemplate.exchange(url, method, entity, String.class);
                //logger.info("Response Entity {}", responseEntity.toString());
                if (responseEntity != null && (responseEntity.getStatusCode() == HttpStatus.OK || responseEntity.getStatusCode() == HttpStatus.CREATED)) {
                    if (responseEntity.getBody() != null) {
                        resultMap = mapper.readValue(responseEntity.getBody(), HashMap.class);
                        logger.info("Tekion result map {}", resultMap);
                    }
                }
            } catch (Exception e) {
                logger.error("Exception at TekionApiCall() - 1 {} ", e.getMessage());
                //e.printStackTrace();
            }
        } catch (Exception e) {
            logger.error("Exception at TekionApiCall() - 2 {} ", e.getMessage());
        }
        return resultMap;
    }

    /**
     * Function to add repair orders to participant list
     */
    public void updateTekionRepairOrdersToParticipantList() {
        try {
            //get closed repair orders
            List<Map<String, Object>> repairOrders = tekionDAO.getClosedOrders();
            //iterate each repair order
            for (Map<String, Object> repairOrder : repairOrders) {
                //get customer details
                Map<String, Object> customerDetails = mapper.readValue(repairOrder.get("customer_details").toString(), HashMap.class);
                //fet vehicle details
                Map<String, Object> vehicleDetails = mapper.readValue(repairOrder.get("vehicle_details").toString(), HashMap.class);
                //get repair order id
                String repairOrderId = repairOrder.get("repair_order_id").toString();
                //get business id
                int businessId = Integer.valueOf(repairOrder.get("business_id").toString());
                //get business uuid
                String businessUUID = schedulerService.retrieveBusinessUUIDByID(businessId);
                if (Utils.notEmptyString(businessUUID)) {
                    //get DT api key for the business
                    Map resultBusiness = tekionDAO.getApiKeyByBusinessId(businessId);
                    Map resultMap = resultBusiness.containsKey("result") ? (Map) resultBusiness.get("result") : new HashMap();
                    String apiKey = resultMap.containsKey("apiKey") ? resultMap.get("apiKey").toString() : "";
                    //get participant group id
                    String participantGroupId = tekionDAO.getTekionParticipantGroup(businessUUID, "1");
                    if (Utils.notEmptyString(participantGroupId)) {
                        //update participant list with order details
                        int result = updateParticipantList(participantGroupId, customerDetails, vehicleDetails, repairOrderId, apiKey);
                        if (result > 0) {
                            logger.info("Participant list updated successfully");
                            //update order status in repair orders table
                            tekionDAO.updateRepairOrderStatus(repairOrderId);
                        }

                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception at updateTekionRepairOrdersToParticipantList() - {} ", e.getMessage());
        }
    }

    /**
     * Create contact for Tekion orders
     *
     * @param participantGroupId
     * @param customerDetails
     * @param vehicleDetails
     * @param repairOrderId
     */
    private int updateParticipantList(String participantGroupId, Map<String, Object> customerDetails, Map<String, Object> vehicleDetails, String repairOrderId, String apiKey) {
        int update = 0;
        try {
            //form participant data map
            Map participantDataMap = formParticipantMap(customerDetails, vehicleDetails, repairOrderId);
            if (Utils.notEmptyString(apiKey)) {
                ResponseEntity responseEntity = restApiToCreateParticipant(participantGroupId, apiKey, participantDataMap);
                if (responseEntity != null && (responseEntity.getStatusCode() == HttpStatus.OK || responseEntity.getStatusCode() == HttpStatus.CREATED)) {
                    update = 1;
                    logger.info("Participant added successfully");
                }
            }
        } catch (Exception e) {
            logger.error("Exception at updateParticipantList() - {} ", e.getMessage());
        }
        return update;
    }

    /**
     * Function to form participant data map
     *
     * @param customerDetails
     * @param vehicleDetails
     * @param repairOrderId
     * @return
     */
    private Map formParticipantMap(Map<String, Object> customerDetails, Map<String, Object> vehicleDetails, String repairOrderId) {
        Map participantDataMap = new HashMap();
        try {
            //fetch list headers
            List<String> headers = tekionDAO.getTekionListHeaders("Create/update contacts in Dropthought list whenever a servicing is complete on Tekion");
            //set cusotmer data
            List<String> data = new ArrayList();
            //add repair order id
            data.add(repairOrderId);
            //add customer id
            data.add(customerDetails.get("id").toString());
            //add customer type
            data.add(customerDetails.get("customerType").toString());
            //add customer name
            data.add(customerDetails.get("firstName").toString());
            //add customer last name
            data.add(customerDetails.get("lastName").toString());
            //add customer email
            data.add(customerDetails.get("email").toString());
            //add customer phone
            List<Map<String, Object>> phones = (List<Map<String, Object>>) customerDetails.get("phones");
            //iterate phones
            if (phones != null && !phones.isEmpty()) {
                for (Map<String, Object> phone : phones) {
                    String phoneType = phone.get("phoneType").toString();
                    if (phoneType.equals("MOBILE")) {
                        data.add(Utils.isNotNull(phone.get("number")) ? phone.get("number").toString() : "");
                    }
                }
            } else {
                data.add("");
            }
            //add vehicle id
            data.add(vehicleDetails.get("id").toString());
            //add vehicle identification number
            data.add(vehicleDetails.get("vin").toString());
            //add date and time in yyyy-MM-dd HH:mm:ss format in America/Los_Angeles timezone
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles")); //TODO check timezone for tekion and set accordingly
            String date = simpleDateFormat.format(new Date());
            data.add(date);
            //create participant data map
            participantDataMap.put("data", data);
            participantDataMap.put("header", headers);
        } catch (Exception e) {
            logger.error("Exception at formParticipantMap() - {} ", e.getMessage());
        }
        return participantDataMap;
    }

    /**
     * Function to update tekion deals
     */
    public void updateTekionDeals() {
        try {
            List<Map<String, Object>> resultList = tekionDAO.getDealersList();
            for (Map<String, Object> eachMap : resultList) {
                String dealerId = eachMap.get("dealer_id").toString();
                int businessId = Integer.valueOf(eachMap.get("business_id").toString());
                long startTime = Utils.isNotNull(eachMap.get("last_executed_time_deals")) ? (long) eachMap.get("last_executed_time_deals") : 0;
                //get deals from Tekion
                Map deals = getTekionDeals(dealerId, businessId, startTime);
                if (!deals.isEmpty()) {
                    List<Map<String, Object>> dataList = deals.containsKey("data") && Utils.isNotNull(deals.get("data")) ? (List<Map<String, Object>>) deals.get("data") : new ArrayList<>();
                    if (!dataList.isEmpty()) {
                        //insert deals into database
                        int insert = tekionDAO.insertDeals(businessId, dataList);
                        if (insert > 0) {
                            logger.info("Deals inserted successfully");
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception at updateTekionDeals() - {} ", e.getMessage());
        }
    }

    /**
     * Function to get repair orders from Tekion
     *
     * @param dealerId
     * @return
     */
    private Map getTekionDeals(String dealerId, int businessId, long startTime) {
        Map resultMap = new HashMap();
        try {
            String uri = tekionUrl;
            //get access token
            Map tokenMap = retrieveTekionAccessToken();
            String accessToken = tokenMap.get("access_token").toString();
            String app_id = tokenMap.get("app_id").toString();
            HttpHeaders headers = new HttpHeaders();
            //set Authorization header
            headers.set("Authorization", "Bearer " + accessToken);
            //set app_id header
            headers.set("app_id", app_id);
            //set dealer_id header
            headers.set("dealer_id", dealerId);
            //set request entity
            HttpEntity requestEntity = new HttpEntity(headers);
            //set start time to yesterday and end time to current time in milliseconds in UTC
            //startTime = startTime > 0 ? startTime : Utils.getPreviousDayTimeMillisecondsUTC();
            //long endTime = Utils.getCurrentTimeMillisecondsUTC();
            //hard coding start and end time for testing purpose
            startTime = 1722709800000L ;
            long endTime = 1723400999000L;
            logger.info("Start Time {} End Time {}", startTime, endTime);
            //set uri
            uri = uri + "v3.1.0/deals?startTime=" + startTime + "&endTime=" + endTime + "&status=CLOSED_OR_SOLD";
            //call tekion api
            resultMap = tekionApiCall(uri, requestEntity, HttpMethod.GET);
            logger.info("Tekion deals response {}", resultMap);
            //update last executed time
            tekionDAO.updateLastExecutedTime(dealerId, endTime, businessId, "deals");
        } catch (Exception e) {
            logger.error("Exception at getTekionDeals() - {} ", e.getMessage());
        }
        return resultMap;
    }

    /**
     * Function to add repair orders to participant list
     */
    public void updateTekionDealsToParticipantList() {
        try {
            String businessUUID = "";
            String participantGroupId = "";
            //get closed repair orders
            List<Map<String, Object>> deals = tekionDAO.getClosedDeals();
            //iterate each repair order
            for (Map<String, Object> deal : deals) {
                //get customer details
                List<Map<String, Object>> customerDetails = mapper.readValue(deal.get("customer_details").toString(), ArrayList.class);
                //get vehicle details
                List<Map<String, Object>> vehicleDetails = mapper.readValue(deal.get("vehicle_details").toString(), ArrayList.class);
                //get payment details
                Map<String, Object> paymentDetails = mapper.readValue(deal.get("payment_details").toString(), HashMap.class);
                //get trade ins
                List<Map<String, Object>> tradeIns = mapper.readValue(deal.get("trade_ins").toString(), ArrayList.class);
                //get deal order id
                String dealId = deal.get("deal_id").toString();
                //get business id
                int businessId = Integer.valueOf(deal.get("business_id").toString());
                //get DT api key for the business
                Map resultBusiness = tekionDAO.getApiKeyByBusinessId(businessId);
                Map resultMap = resultBusiness.containsKey("result") ? (Map) resultBusiness.get("result") : new HashMap();
                String apiKey = resultMap.containsKey("apiKey") ? resultMap.get("apiKey").toString() : "";
                //get business uuid
                if (Utils.emptyString(businessUUID) && Utils.emptyString(participantGroupId)) {
                    businessUUID = schedulerService.retrieveBusinessUUIDByID(businessId);
                    //get participant group id
                    participantGroupId = tekionDAO.getTekionParticipantGroup(businessUUID, "2");
                }
                if (Utils.notEmptyString(participantGroupId)) {
                    //update participant list with order details
                    int result = updateParticipantListForDeals(participantGroupId, customerDetails, vehicleDetails, paymentDetails, tradeIns, dealId, businessId, apiKey);
                    if (result > 0) {
                        logger.info("Participant list updated successfully");
                        //update deal status in deals table
                        tekionDAO.updateDealStatus(dealId);
                    }

                }
            }
        } catch (Exception e) {
            logger.error("Exception at updateTekionDealsToParticipantList() - {} ", e.getMessage());
        }
    }

    /**
     * Create contact for Tekion orders
     *
     * @param participantGroupId
     * @param customerDetails
     * @param vehicleDetails
     * @param dealId
     * @param businessId
     */
    private int updateParticipantListForDeals(String participantGroupId, List<Map<String, Object>> customerDetails, List<Map<String, Object>> vehicleDetails, Map paymentDetails, List tradeIns, String dealId, int businessId, String apiKey) {
        int update = 0;
        try {
            //form participant data map
            List participantDataList = formParticipantMapForDeals(customerDetails, vehicleDetails, paymentDetails, tradeIns, dealId);
            Iterator itr = participantDataList.iterator();
            while (itr.hasNext()) {
                Map participantDataMap = (Map) itr.next();
                if (Utils.notEmptyString(apiKey)) {
                    ResponseEntity<String> responseEntity = restApiToCreateParticipant(participantGroupId, apiKey, participantDataMap);
                    if (responseEntity != null && (responseEntity.getStatusCode() == HttpStatus.OK || responseEntity.getStatusCode() == HttpStatus.CREATED)) {
                        update = 1;
                        logger.info("Participant added successfully");
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception at updateParticipantListForDeals() - {} ", e.getMessage());
        }
        return update;
    }

    /**
     * Function to call rest api to create participant
     *
     * @param participantGroupId
     * @param apiKey
     * @param participantDataMap
     * @return
     */
    private ResponseEntity<String> restApiToCreateParticipant(String participantGroupId, String apiKey, Map participantDataMap) {
        ResponseEntity<String> responseEntity = null;
        try {
            //call create participant api in dtapp
            String url = dtAppUrl + "/participant/?participantgroupuuid=" + participantGroupId;
            //set headers
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.set("X-DT-API-KEY", apiKey);
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            //set request entity
            HttpEntity<Map> requestEntity = new HttpEntity<>(participantDataMap, httpHeaders);
            //call api
            responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
        } catch (Exception e) {
            logger.error("Exception at restApiToCreateParticipant() - {} ", e.getMessage());
        }
        return responseEntity;
    }

    /**
     * Function to form participant data map
     *
     * @param customerDetails
     * @param vehicleDetails
     * @param dealId
     * @return
     */
    private List formParticipantMapForDeals(List<Map<String, Object>> customerDetails, List<Map<String, Object>> vehicleDetails, Map<String, Object> paymentDetails, List<Map<String, Object>> tradeIns, String dealId) {
        List <Map> participantList = new ArrayList<>();
        try {
            //fetch list headers
            List<String> headers = tekionDAO.getTekionListHeaders("Create a contact in Dropthought whenever a deal is closed on tekion");
            Map<String, Object> vechicleMap = vehicleDetails.get(0);
            //set cusotmer data
            for (int i = 0; i < customerDetails.size(); i++) {
                try {
                    Map participantDataMap = new HashMap();
                    List<String> data = new ArrayList();
                    Map<String, Object> customer = customerDetails.get(i);
                    //add customer name
                    data.add((Utils.notEmptyString(customer.get("firstName").toString()) ? customer.get("firstName").toString() : "") + "" + (Utils.notEmptyString(customer.get("lastName").toString()) ? customer.get("lastName").toString() : ""));
                    //add customer phone
                    List<Map<String, Object>> phones = (List<Map<String, Object>>) customer.get("phones");
                    //iterate phones
                    if (phones != null && !phones.isEmpty()) {
                        for (Map<String, Object> phone : phones) {
                            String phoneType = phone.get("type").toString();
                            if (phoneType.equals("MOBILE") || phoneType.equals("CELL")) {
                                //add phone type
                                data.add(phoneType);
                                //add phone number
                                data.add(Utils.isNotNull(phone.get("number")) ? phone.get("number").toString() : "");
                                break;
                            }
                        }
                    } else {
                        data.add("");
                        data.add("");
                    }
                    //add customer email
                    data.add(Utils.isNotNull(customer.get("email")) ? customer.get("email").toString() : "");
                    //iterate addresses
                    List<Map<String, Object>> addresses = (List<Map<String, Object>>) customer.get("addresses");
                    if (addresses != null && !addresses.isEmpty()) {
                        Map address = addresses.get(0);
                        //add city
                        data.add(Utils.isNotNull(address.get("city")) ? address.get("city").toString() : "");
                        //add state
                        data.add(Utils.isNotNull(address.get("state")) ? address.get("state").toString() : "");
                        //add county
                        data.add(Utils.isNotNull(address.get("county")) ? address.get("county").toString() : "");
                        //add country
                        data.add(Utils.isNotNull(address.get("country")) ? address.get("country").toString() : "");
                    } else {
                        data.add("");
                        data.add("");
                        data.add("");
                        data.add("");
                    }
                    //add vehicle id
                    data.add(Utils.notEmptyString(vechicleMap.get("id").toString()) ? vechicleMap.get("id").toString() : "");
                    //add vehicle year
                    data.add(Utils.notEmptyString(vechicleMap.get("year").toString()) ? vechicleMap.get("year").toString() : "");
                    //add vehicle make
                    data.add(Utils.notEmptyString(vechicleMap.get("make").toString()) ? vechicleMap.get("make").toString() : "");
                    //add vehicle model
                    data.add(Utils.notEmptyString(vechicleMap.get("model").toString()) ? vechicleMap.get("model").toString() : "");
                    //add payment details
                    Map<String, Object> paymentOption = paymentDetails.containsKey("paymentOption") ? (Map<String, Object>) paymentDetails.get("paymentOption") : new HashMap();
                    //add payment frequency
                    data.add(Utils.notEmptyString(paymentOption.get("frequency").toString()) ? paymentOption.get("frequency").toString() : "");
                    //add payment type
                    data.add(Utils.notEmptyString(paymentOption.get("type").toString()) ? paymentOption.get("type").toString() : "");
                    //add payment value
                    data.add(Utils.notEmptyString(paymentOption.get("value").toString()) ? paymentOption.get("value").toString() : "");
                    //add lien details
                    if (tradeIns != null && !tradeIns.isEmpty()) {
                        //add lien holder details
                        Map<String, Object> lienHolder = tradeIns.get(0);
                        data.add(Utils.isNotNull(lienHolder.get("lienHolder").toString()) ? lienHolder.get("lienHolder").toString() : "");
                        //add lien amount
                        Map<String, Object> lienAmount = lienHolder.containsKey("lienAmount") ? (Map<String, Object>) lienHolder.get("lienAmount") : new HashMap();
                        data.add(lienAmount.containsKey("amount") ? lienAmount.get("amount").toString() : "");
                        //add lien currency
                        data.add(lienAmount.containsKey("currency") ? lienAmount.get("currency").toString() : "");
                    } else {
                        data.add("");
                        data.add("");
                        data.add("");
                    }
                    //add pricing
                    List<Map<String, Object>> pricing = (List<Map<String, Object>>) vechicleMap.get("pricing");
                    if (pricing != null && !pricing.isEmpty()) {
                        for (Map<String, Object> price : pricing) {
                            //get price type
                            String priceType = price.get("type").toString();
                            if (priceType.equals("INVOICE_PRICE")) {
                                //add price type
                                data.add(priceType);
                                //add price amount
                                data.add(price.get("amount").toString());
                                //add price currency
                                data.add(price.get("currency").toString());
                                break;
                            }
                        }
                    } else {
                        data.add("");
                        data.add("");
                        data.add("");
                    }
                    //add date and time in yyyy-MM-dd HH:mm:ss format in America/Los_Angeles timezone
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    simpleDateFormat.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles")); //TODO check timezone for tekion and set accordingly
                    String date = simpleDateFormat.format(new Date());
                    data.add(date);
                    //create participant data map
                    participantDataMap.put("data", data);
                    participantDataMap.put("header", headers);
                    participantList.add(participantDataMap);
                } catch (Exception e) {
                    logger.error("Exception at formParticipantMapForDeals() - 1 {} ", e.getMessage());
                }
            }

        } catch (Exception e) {
            logger.error("Exception at formParticipantMapForDeals() - 2 {} ", e.getMessage());
        }
        return participantList;
    }
}
