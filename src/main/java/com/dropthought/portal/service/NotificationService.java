package com.dropthought.portal.service;

import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.EmailTemplateUtility;
import com.dropthought.portal.util.Utils;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class NotificationService extends BaseService {

    @Autowired
    SchedulerService schedulerService;
    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${sender.email}")
    private String emailSender;

    @Value("${sender.name}")
    private String emailSenderName;

    @Value("${dt.email.url}")
    private String dtEmailURL;

    @Value("${dtp.domain}")
    private String dtpDomain;

    @Value("${dt.website.url}")
    private String dtWebsiteURL;

    /**
     * method to get email template file in string format
     *
     * @param template
     * @return
     */
    public String getFileContent(String template) {
        StringBuilder result = new StringBuilder();

        try {
            InputStream in = this.getClass().getClassLoader().getResourceAsStream("email/" + template);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            for (String line; (line = reader.readLine()) != null; ) {
                result.append(line);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getMessage());
        }
        return result.toString();
    }


    /**
     * method to call rest api for email/sms
     *
     * @param inputMap
     * @param uri
     */
    public Map callRestApi(Map inputMap, String uri, HttpMethod method) {
        Map respMap = new HashMap();

        try {

            JSONObject json = new JSONObject(inputMap);
            String inputJson = json.toString();
            logger.info("input json {}", inputJson);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);

            logger.debug("uri {}", uri);


            restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
            ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, method, entity, Object.class);
            logger.info("response entity {}", responseEntity);

            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info(responseEntity.getBody().toString());
                    respMap = (Map) responseEntity.getBody();

                }
            }
        } catch (Exception e) {
            //logger.error(e.getMessage());
            logger.error(e.getMessage());
        }
        return respMap;
    }

    /**
     * method to send emails for activation, reminder and deactivation of contract based on the request
     *
     * @param templateType
     * @param paramMap
     */
    public void sendBusinessContractEmail(Map paramMap, String templateType, int businessId) {
        try {
            logger.debug("begin send business contract email");
            //System.out.println(paramMap);
            boolean auto_renewal = false;
            Map emailSettingsMap = schedulerService.getBusinessEmailSettings(businessId); //DTV-5738 //DTV-6025
            Map businessEmailMap = emailSettingsMap.containsKey("business_email") && Utils.notEmptyString(emailSettingsMap.get("business_email").toString()) ? (Map) emailSettingsMap.get("business_email") : new HashMap();
            String fromEmailId = businessEmailMap.containsKey("businessContractMail") && Utils.notEmptyString(businessEmailMap.get("businessContractMail").toString()) ? businessEmailMap.get("businessContractMail").toString() : Constants.FROM_CUSTOMER_SUCCESS_EMAIL;
            String fromName = businessEmailMap.containsKey("businessContractFromName") && Utils.notEmptyString(businessEmailMap.get("businessContractFromName").toString()) ? businessEmailMap.get("businessContractFromName").toString() : Constants.FROM_CUSTOMER_SUCCESS_NAME;
            String ccEmail = emailSender;
            String toEmailId = paramMap.containsKey("toEmail") ? paramMap.get("toEmail").toString() : "";
            String expiryDate = paramMap.containsKey("contractExpiryDate") ? paramMap.get("contractExpiryDate").toString() : "";
            String recipientName = paramMap.containsKey("recipientName") ? paramMap.get("recipientName").toString() : "User";
            String endDate = paramMap.containsKey("endDate") ? paramMap.get("endDate").toString() : "";
            String trialReminderEndDate = paramMap.containsKey("trialReminderEndDate") ? paramMap.get("trialReminderEndDate").toString() : "";
            String planName = paramMap.containsKey("planName") ? paramMap.get("planName").toString() : "";
            String userUUID = paramMap.containsKey("userUUID") ? paramMap.get("userUUID").toString() : "";
            String businessUUID = paramMap.containsKey("businessUUID") ? paramMap.get("businessUUID").toString() : "";
            String link = paramMap.containsKey("link") ? paramMap.get("link").toString() : "";
            int planId = paramMap.containsKey("planId") ? Integer.parseInt(paramMap.get("planId").toString()) : 0;
            String dtToken = "";
            String trialStatus = "";
            String subscriptionReminderEndDate = "";
            String card = "";
            boolean isTransactionsMadeAlready = false;
            if (planId > 0) {

                dtToken = paramMap.containsKey("dtToken") ? paramMap.get("dtToken").toString() : "";
                trialStatus = paramMap.containsKey("trialStatus") ? paramMap.get("trialStatus").toString() : "1";
                subscriptionReminderEndDate = paramMap.containsKey("subscriptionReminderEndDate") ?
                        paramMap.get("subscriptionReminderEndDate").toString() : "";
                card = paramMap.containsKey("card") ? paramMap.get("card").toString() : "";
                isTransactionsMadeAlready = paramMap.containsKey("isTransactionsMadeAlready") ?
                        Boolean.parseBoolean(paramMap.get("isTransactionsMadeAlready").toString()) : false;
            }

            //get auto_renewal flag from business global settings
            //DTV-12699 Not required as we are not using auto renewal flag
            /*Map businessGlobalSettingsMap = schedulerService.getBusinessGlobalSettings(businessId);

            if (businessGlobalSettingsMap.size() > 0 && businessGlobalSettingsMap.containsKey("auto_renewal")) {
                auto_renewal = businessGlobalSettingsMap.get("auto_renewal").toString().equals("0") ? false : true;
            }*/

            if (Utils.notEmptyString(toEmailId)) {
                String template = "";
                String subject = "";
                if (templateType.equals(Constants.PRECURSOR_DATE) && Utils.emptyString(endDate) && planId == 0) {
                    //template type is eighty pertcent date, then send reminder email template
                    template = Constants.BUSINESS_CONTRACT_REMINDER_TEMPLATE;
                    subject = Constants.BUSINESS_CONTRACT_REMINDER_SUBJECT;

                } else if (templateType.equals(Constants.PLAN_RENEWAL_REMINDER) && trialStatus.equals("1")) {
                    //DTV-12302
                    /*link = dtWebsiteURL + "signup/?planId=" + planId + "&APIToken=" + dtToken + "&businessUUID="
                            + businessUUID + "&renew=true";*/
                    logger.info("dt renew link {}", link);
                    template = Constants.PLAN_RENEWAL_REMINDER_TEMPLATE_FOR_TRIAL;
                    subject = Constants.PLAN_RENEWAL_REMINDER_SUBJECT_FOR_TRIAL;

                } else if (templateType.equals(Constants.PLAN_RENEWAL_REMINDER) && trialStatus.equals("0")) {
                    //DTV-12302
                    /*link = dtWebsiteURL + "signup/?planId=" + planId + "&APIToken=" + dtToken + "&businessUUID="
                            + businessUUID + "&renew=true";*/
                    logger.info("dt renew link {}", link);
                    template = Constants.PLAN_RENEWAL_REMINDER_TEMPLATE_FOR_SUBSCRIPTION;
                    subject = Constants.PLAN_RENEWAL_REMINDER_SUBJECT_FOR_SUBSCRIPTION;

                } else if (templateType.equals(Constants.PLAN_EXPIRED)) {
                    //contract termination template
                    //DTV-12302
                    link = dtWebsiteURL + "signup/?planId=" + planId + "&APIToken=" + dtToken  + "&businessUUID="
                            + businessUUID + "&renew=true";
                    logger.info("dt renew link {}", link);
                    template = isTransactionsMadeAlready ? Constants.PLAN_EXPIRED_TEMPLATE : Constants.PLAN_EXPIRED_AFTER_TRAIL_TEMPLATE;
                    subject = isTransactionsMadeAlready ? Constants.PLAN_EXPIRED_SUBJECT : Constants.PLAN_EXPIRED_AFTER_TRAIL_SUBJECT;
                } else {
                    //contract termination template
                    template = Constants.BUSINESS_CONTRACT_TERMINATION_TEMPLATE;
                    subject = Constants.BUSINESS_CONTRACT_TERMINATION_SUBJECT;
                }
                //get email template file for notification
                String htmlContent = getFileContent(template);
                //set the dynamic param value to replace in template file
                paramMap.put("logo", Constants.DEFAULT_LOGO_URL);
                paramMap.put("contractExpiryDate", expiryDate);
                paramMap.put("recipientName", recipientName);
                paramMap.put("endDate", endDate);
                paramMap.put("planName", planName);
                paramMap.put("link", link);
                paramMap.put("trialReminderEndDate", trialReminderEndDate);
                paramMap.put("subscriptionReminderEndDate", subscriptionReminderEndDate);
                paramMap.put("card", card);

                //substitute variable like logo, lineone, linetwo, link, recipient name etc in email template with corresponding values
                StrSubstitutor sub = new StrSubstitutor(paramMap);
                String resolvedString = sub.replace(htmlContent);

                //call email utility api
                Map emailMap = new HashMap();
                emailMap.put("from", fromEmailId);
                emailMap.put("to", toEmailId);
                emailMap.put("subject", subject);
                emailMap.put("body", resolvedString);
                emailMap.put("name", fromName);
                List ccList = new ArrayList();
                ccList.add(ccEmail);
                emailMap.put("cc", ccList);
                Map resp = callRestApi(emailMap, dtEmailURL, HttpMethod.POST);
                logger.debug("begin send business contract email");
            }

        } catch (Exception e) {
            logger.error("Error in sendBusinessContractEmail {}", e.getMessage());
        }

    }

    /**
     * Send mail to Client king user email when access settings is getting changed
     *
     * @param paramMap
     */
    public void sendBusinessSettingsReminder(Map paramMap, int businessId, boolean planUpdate) {
        try {
            logger.info("begin send business settings reminder email");
            //System.out.println(paramMap);
            Map emailSettingsMap = schedulerService.getBusinessEmailSettings(businessId); //DTV-5738 //DTV-6025
            Map businessEmailMap = emailSettingsMap.containsKey("business_email") && Utils.notEmptyString(emailSettingsMap.get("business_email").toString()) ? (Map) emailSettingsMap.get("business_email") : new HashMap();
            String fromEmailId = businessEmailMap.containsKey("accessSettingsMail") && Utils.notEmptyString(businessEmailMap.get("accessSettingsMail").toString()) ? businessEmailMap.get("accessSettingsMail").toString() : emailSender;
            String fromName = businessEmailMap.containsKey("accessSettingsFromName") && Utils.notEmptyString(businessEmailMap.get("accessSettingsFromName").toString()) ? businessEmailMap.get("accessSettingsFromName").toString() : emailSenderName;
            String ccEmail = "cs@dropthought.com";
            String toEmailId = paramMap.containsKey("toEmail") ? paramMap.get("toEmail").toString() : "";
            String recipientName = paramMap.containsKey("recipientName") ? paramMap.get("recipientName").toString() : "User";
            String encodedPDf = paramMap.containsKey("attachment") ? paramMap.get("attachment").toString() : "";
            String attachmentType = paramMap.containsKey("attachmentType") ? paramMap.get("attachmentType").toString() : "";
            String businessUUID = paramMap.containsKey("businessUUID") ? paramMap.get("businessUUID").toString() : "";
            String environmentInfo = paramMap.containsKey("EnvironmentInfo") ? paramMap.get("EnvironmentInfo").toString() : "";
            String planName = "";
            String endDate = "";
            if (planUpdate) {
                planName = paramMap.containsKey("planName") ? paramMap.get("planName").toString() : "";
                endDate = paramMap.containsKey("contractEndDate") ? paramMap.get("contractEndDate").toString() : "";
            }

            if (Utils.notEmptyString(toEmailId)) {
                String template = planUpdate ? Constants.PLAN_UPDATE_TEMPLATE : Constants.BUSINESS_SETTINGS_REMINDER_TEMPLATE;
                String subject = planUpdate ? Constants.PLAN_UPDATE_SUBJECT : Constants.BUSINESS_SETTINGS_REMINDER_SUBJECT;

                //get email template file for notification
                String htmlContent = getFileContent(template);
                //set the dynamic param value to replace in template file
                paramMap.put("logo", Constants.DEFAULT_LOGO_URL);
                paramMap.put("recipientName", recipientName);
                paramMap.put("planName", planName);
                paramMap.put("endDate", endDate);
                paramMap.put("EnvironmentInfo", environmentInfo);

                //logger.info("EnvironmentInfo {}", environmentInfo);

                //substitute variable like logo, lineone, linetwo, link, recipient name etc in email template with corresponding values
                StrSubstitutor sub = new StrSubstitutor(paramMap);
                String resolvedString = sub.replace(htmlContent);

                //logger.info("resolvedString {}", resolvedString);

                //call email utility api
                Map emailMap = new HashMap();
                emailMap.put("from", fromEmailId);
                emailMap.put("to", toEmailId);
                emailMap.put("subject", subject);
                emailMap.put("body", resolvedString);
                emailMap.put("name", fromName);
                emailMap.put("attachment", encodedPDf);
                emailMap.put("attachmentType", attachmentType);
                emailMap.put("attachmentName", businessUUID + "." + attachmentType); //businessUUID.pdf
                List ccList = new ArrayList();
                ccList.add(ccEmail);
                emailMap.put("cc", ccList);
                Map resp = callRestApi(emailMap, dtEmailURL, HttpMethod.POST);
                logger.debug("begin send business settings reminder email");
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }

    }

    /**
     * send king login instruction mail
     *
     * @param paramMap
     */
    public void sendLoginMail(Map paramMap) {
        try {
            logger.info("begin send king new user email");
            String fromName = emailSenderName;
            String fromEmailId = emailSender;
            String ccEmail = "cs@dropthought.com";
            String toEmailId = paramMap.containsKey("toEmail") ? paramMap.get("toEmail").toString() : "";
            String recipientName = paramMap.containsKey("recipientName") ? paramMap.get("recipientName").toString() : "User";

            String attachment = paramMap.containsKey("attachment") ? paramMap.get("attachment").toString() : "";
            String attachmentType = paramMap.containsKey("attachmentType") ? paramMap.get("attachmentType").toString() : "";
            String attachmentName = paramMap.containsKey("attachmentName") ? paramMap.get("attachmentName").toString() : "";

            if (Utils.notEmptyString(toEmailId)) {
                String template = Constants.KING_NEW_USER_TEMPLATE;
                String subject = Constants.KING_NEW_USER_SUBJECT;
                //get email template file for notification
                String htmlContent = getFileContent(template);
                //set the dynamic param value to replace in template file
                paramMap.put("logo", Constants.DEFAULT_LOGO);
                paramMap.put("recipientName", recipientName);

                //substitute variable like link, recipient name, username, password etc in email template with corresponding values
                StrSubstitutor sub = new StrSubstitutor(paramMap);
                String resolvedString = sub.replace(htmlContent);

                //call email utility api
                Map emailMap = new HashMap();
                emailMap.put("from", fromEmailId);
                emailMap.put("to", toEmailId);
                emailMap.put("subject", subject);
                emailMap.put("body", resolvedString);
                emailMap.put("name", fromName);

                //validation to prevent  exceptions in email service
                if (Utils.notEmptyString(attachment) && Utils.notEmptyString(attachmentName) && Utils.notEmptyString(attachmentType)) {
                    emailMap.put("attachment", attachment);
                    emailMap.put("attachmentType", attachmentType);
                    emailMap.put("attachmentName", attachmentName); //businessUUID.pdf
                }

                List ccList = new ArrayList();
                ccList.add(ccEmail);
                emailMap.put("cc", ccList);
                Map resp = this.callRestApi(emailMap, dtEmailURL, HttpMethod.POST);
                logger.info("End send king new user email");
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }


    /**
     * Send mail to Client king user email when business usage reaches limit
     *
     * @param paramMap
     */
    public void sendBusinessUsageReminderMail(Map paramMap, int businessId) {
        try {
            logger.info("begin send business usage reminder email");
            //System.out.println(paramMap);
            Map emailSettingsMap = schedulerService.getBusinessEmailSettings(businessId); //DTV-5738 //DTV-6025
            Map businessEmailMap = emailSettingsMap.containsKey("business_email") && Utils.notEmptyString(emailSettingsMap.get("business_email").toString()) ? (Map) emailSettingsMap.get("business_email") : new HashMap();
            String fromEmailId = businessEmailMap.containsKey("businessUsageLimitMail") && Utils.notEmptyString(businessEmailMap.get("businessUsageLimitMail").toString()) ? businessEmailMap.get("businessUsageLimitMail").toString() : emailSender;
            String fromName = businessEmailMap.containsKey("businessUsageLimitFromName") && Utils.notEmptyString(businessEmailMap.get("businessUsageLimitFromName").toString()) ? businessEmailMap.get("businessUsageLimitFromName").toString() : emailSenderName;
            String ccEmail = "cs@dropthought.com";
            String toEmailId = paramMap.containsKey("toEmail") ? paramMap.get("toEmail").toString() : "";
            String recipientName = paramMap.containsKey("recipientName") ? paramMap.get("recipientName").toString() : "User";
            String message = paramMap.containsKey("message") ? paramMap.get("message").toString() : "";


            if (Utils.notEmptyString(toEmailId)) {
                String template = Constants.BUSINESS_USAGE_REMINDER_TEMPLATE;
                String subject = Constants.BUSINESS_USAGE_REMINDER_SUBJECT;

                //get email template file for notification
                String htmlContent = getFileContent(template);
                //set the dynamic param value to replace in template file
                paramMap.put("logo", Constants.DEFAULT_LOGO_URL);
                paramMap.put("recipientName", recipientName);
                paramMap.put("message", message);

                //substitute variable like logo, message, recipient name etc in email template with corresponding values
                StrSubstitutor sub = new StrSubstitutor(paramMap);
                String resolvedString = sub.replace(htmlContent);

                //call email utility api
                Map emailMap = new HashMap();
                emailMap.put("from", fromEmailId);
                emailMap.put("to", toEmailId);
                emailMap.put("subject", subject);
                emailMap.put("body", resolvedString);
                emailMap.put("name", fromName);
                List ccList = new ArrayList();
                ccList.add(ccEmail);
                emailMap.put("cc", ccList);
                Map resp = callRestApi(emailMap, dtEmailURL, HttpMethod.POST);
                logger.debug("begin send business settings reminder email");
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }

    }


    /**
     * send king password update instruction mail
     *
     * @param paramMap
     */
    public void sendPasswordUpdateMail(Map paramMap, int businessId, boolean selfSignupCustomer) {
        try {
            logger.info("begin send king new user password update");
            Map emailSettingsMap = schedulerService.getBusinessEmailSettings(businessId); //DTV-5738 //DTV-6025
            Map businessEmailMap = emailSettingsMap.containsKey("business_email") && Utils.notEmptyString(emailSettingsMap.get("business_email").toString()) ? (Map) emailSettingsMap.get("business_email") : new HashMap();
            String fromEmailId = businessEmailMap.containsKey("passwordUpdateMail") && Utils.notEmptyString(businessEmailMap.get("passwordUpdateMail").toString()) ? businessEmailMap.get("passwordUpdateMail").toString() : emailSender;
            String fromName = businessEmailMap.containsKey("passwordUpdateFromName") && Utils.notEmptyString(businessEmailMap.get("passwordUpdateFromName").toString()) ? businessEmailMap.get("passwordUpdateFromName").toString() : emailSenderName;
            String ccEmail = "cs@dropthought.com";
            String toEmailId = paramMap.containsKey("toEmail") ? paramMap.get("toEmail").toString() : "";
            String recipientName = paramMap.containsKey("recipientName") ? paramMap.get("recipientName").toString() : "User";
            String role = paramMap.containsKey("role") ? paramMap.get("role").toString() : Constants.READ_ONLY_ROLE;
            String environmentInfo = EmailTemplateUtility.getEnvironmentEmailContent(dtpDomain, false);

            String attachment = paramMap.containsKey("attachment") ? paramMap.get("attachment").toString() : "";
            String attachmentType = paramMap.containsKey("attachmentType") ? paramMap.get("attachmentType").toString() : "";
            String attachmentName = paramMap.containsKey("attachmentName") ? paramMap.get("attachmentName").toString() : "";

            if (Utils.notEmptyString(toEmailId)) {
                String template = selfSignupCustomer ? Constants.KING_NEW_LOGIN_TEMPLATE_FOR_PLAN
                        : Constants.KING_ADMIN_READWRITE_TEMPLATE;//KING_USER_PASSWORD_UPDATE_TEMPLATE;
                String subject = selfSignupCustomer? Constants.KING_NEW_USER_SUBJECT : Constants.WELCOME_ABOARD_MESSAGE;//KING_USER_PASSWORD_UPDATE_SUBJECT;
                //get email template file for notification
                String htmlContent = getFileContent(template);
                //set the dynamic param value to replace in template file
                paramMap.put("logo", Constants.DEFAULT_LOGO);
                paramMap.put("recipientName", recipientName);
                paramMap.put("role", role);
                paramMap.put("EnvironmentInfo", environmentInfo);

                //substitute variable like link, recipient name, username, password etc in email template with corresponding values
                StrSubstitutor sub = new StrSubstitutor(paramMap);
                String resolvedString = sub.replace(htmlContent);

                //call email utility api
                Map emailMap = new HashMap();
                emailMap.put("from", fromEmailId);
                emailMap.put("to", toEmailId);
                emailMap.put("subject", subject);
                emailMap.put("body", resolvedString);
                emailMap.put("name", fromName);

                //validation to prevent  exceptions in email service
                if (Utils.notEmptyString(attachment) && Utils.notEmptyString(attachmentName) && Utils.notEmptyString(attachmentType)) {
                    emailMap.put("attachment", attachment);
                    emailMap.put("attachmentType", attachmentType);
                    emailMap.put("attachmentName", attachmentName); //businessUUID.pdf
                }

                List ccList = new ArrayList();
                ccList.add(ccEmail);
                emailMap.put("cc", ccList);
                Map resp = this.callRestApi(emailMap, dtEmailURL, HttpMethod.POST);
                logger.info("End send king new user email");
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    public void sendPaymentFailureMail(Map<String, Object> paramMap, int businessId) {

        logger.info("begin send payment failure email");
        try {

            Map emailSettingsMap = schedulerService.getBusinessEmailSettings(businessId); //DTV-5738 //DTV-6025
            Map businessEmailMap = emailSettingsMap.containsKey("business_email") && Utils.notEmptyString(emailSettingsMap.get("business_email").toString()) ? (Map) emailSettingsMap.get("business_email") : new HashMap();
            String fromEmailId = businessEmailMap.containsKey("passwordUpdateMail") && Utils.notEmptyString(businessEmailMap.get("passwordUpdateMail").toString()) ? businessEmailMap.get("passwordUpdateMail").toString() : emailSender;
            String fromName = businessEmailMap.containsKey("passwordUpdateFromName") && Utils.notEmptyString(businessEmailMap.get("passwordUpdateFromName").toString()) ? businessEmailMap.get("passwordUpdateFromName").toString() : emailSenderName;
            String ccEmail = "cs@dropthought.com";
            String toEmailId = paramMap.containsKey("toEmail") ? paramMap.get("toEmail").toString() : "";
            String recipientName = paramMap.containsKey("recipientName") ? paramMap.get("recipientName").toString() : "User";
            String planName = paramMap.containsKey("planName") ? paramMap.get("planName").toString() : "";
            String link = paramMap.containsKey("link") ? paramMap.get("link").toString() : "";
            String environmentInfo = EmailTemplateUtility.getEnvironmentEmailContent(dtpDomain, false);

            if (Utils.notEmptyString(toEmailId)) {
                String template = Constants.PAYMENT_FAILURE_TEMPLATE;
                String subject = Constants.PAYMENT_FAILURE_SUBJECT;
                //get email template file for notification
                String htmlContent = getFileContent(template);
                //set the dynamic param value to replace in template file
                paramMap.put("logo", Constants.DEFAULT_LOGO);
                paramMap.put("recipientName", recipientName);
                paramMap.put("planName", planName);
                paramMap.put("link", link);
                paramMap.put("EnvironmentInfo", environmentInfo);

                //substitute variable like link, recipient name, username, password etc in email template with corresponding values
                StrSubstitutor sub = new StrSubstitutor(paramMap);
                String resolvedString = sub.replace(htmlContent);

                //call email utility api
                Map emailMap = new HashMap();
                emailMap.put("from", fromEmailId);
                emailMap.put("to", toEmailId);
                emailMap.put("subject", subject);
                emailMap.put("body", resolvedString);
                emailMap.put("name", fromName);

                List ccList = new ArrayList();
                ccList.add(ccEmail);
                emailMap.put("cc", ccList);
                Map resp = this.callRestApi(emailMap, dtEmailURL, HttpMethod.POST);
                logger.info("End send payment failure email");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

    }

    public void sendAccountChangeMail(Map<String, Object> paramMap, int businessId) {

        logger.info("begin send account change email");
        try {
            Map emailSettingsMap = schedulerService.getBusinessEmailSettings(businessId); //DTV-5738 //DTV-6025
            Map businessEmailMap = emailSettingsMap.containsKey("business_email") && Utils.notEmptyString(emailSettingsMap.get("business_email").toString()) ? (Map) emailSettingsMap.get("business_email") : new HashMap();
            String fromEmailId = businessEmailMap.containsKey("passwordUpdateMail") && Utils.notEmptyString(businessEmailMap.get("passwordUpdateMail").toString()) ? businessEmailMap.get("passwordUpdateMail").toString() : emailSender;
            String fromName = businessEmailMap.containsKey("passwordUpdateFromName") && Utils.notEmptyString(businessEmailMap.get("passwordUpdateFromName").toString()) ? businessEmailMap.get("passwordUpdateFromName").toString() : emailSenderName;
            String ccEmail = "cs@dropthought.com";
            String toEmailId = paramMap.containsKey("toEmail") ? paramMap.get("toEmail").toString() : "";
            String recipientName = paramMap.containsKey("recipientName") ? paramMap.get("recipientName").toString() : "User";
            String endDate = paramMap.containsKey("endDate") ? paramMap.get("endDate").toString() : "";
            String environmentInfo = EmailTemplateUtility.getEnvironmentEmailContent(dtpDomain, false);

            if (Utils.notEmptyString(toEmailId)) {
                String template = Constants.ACCOUNT_CHANGE_TEMPLATE;
                String subject = Constants.ACCOUNT_CHANGE_SUBJECT;
                //get email template file for notification
                String htmlContent = getFileContent(template);
                //set the dynamic param value to replace in template file
                paramMap.put("logo", Constants.DEFAULT_LOGO);
                paramMap.put("recipientName", recipientName);
                paramMap.put("endDate", endDate);
                paramMap.put("EnvironmentInfo", environmentInfo);

                //substitute variable like link, recipient name, username, password etc in email template with corresponding values
                StrSubstitutor sub = new StrSubstitutor(paramMap);
                String resolvedString = sub.replace(htmlContent);

                //call email utility api
                Map emailMap = new HashMap();
                emailMap.put("from", fromEmailId);
                emailMap.put("to", toEmailId);
                emailMap.put("subject", subject);
                emailMap.put("body", resolvedString);
                emailMap.put("name", fromName);

                List ccList = new ArrayList();
                ccList.add(ccEmail);
                emailMap.put("cc", ccList);
                Map resp = this.callRestApi(emailMap, dtEmailURL, HttpMethod.POST);
            }
        } catch (Exception e) {
            logger.error("Error while sending account change email : " + e.getMessage());
        }
    }
}
