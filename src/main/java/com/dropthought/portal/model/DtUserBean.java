package com.dropthought.portal.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DtUserBean implements Serializable {

    @JsonProperty("businessId")
    private Integer businessId;

    @JsonProperty("businessTypeId")
    private Integer businessTypeId;

    @JsonProperty("empRangeId")
    private Integer empRangeId;

    @JsonProperty("signupTypeId")
    private Integer signupTypeId;

    @JsonProperty("countryId")
    private Integer countryId;

    @JsonProperty("fullName")
    private String fullName;

    @JsonProperty("firstName")
    private String firstName;

    @JsonProperty("lastName")
    private String lastName;

    @JsonProperty("username")
    private String userName;

    @JsonProperty("companyCode")
    private String companyCode;

    @JsonProperty("userDb")
    private String userDB;

    @JsonProperty("userEmail")
    private String userEmail;

    @JsonProperty("password")
    private String password;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("roleId")
    private Integer roleId;

    @JsonProperty("trialFlag")
    private Integer trialFlag;

    @JsonProperty("confirmationStatus")
    private Integer confirmationStatus;

    @JsonProperty("createdBy")
    private String createdBy;

    @JsonProperty("modifiedBy")
    private String modifiedBy;

    @JsonProperty("completeSocialSignup")
    private Integer completeSocialSignup;

    @JsonProperty("userId")
    private Integer userId;

    @JsonProperty("businessName")
    private String businessName;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("refreshToken")
    private String refreshToken;

    @JsonProperty("alreadyExistFlag")
    private Integer alreadyExists;

    @JsonProperty("passwordUpdateCount")
    private Integer passwordUpdateCount;

    @JsonProperty("welcomeEmail")
    private Boolean welcomeEmail;

    @JsonProperty("cc")
    private String currentDB;

    @JsonProperty("host")
    private String host;

    @JsonProperty("userUUID")
    private String userUUID;

    @JsonProperty("user_status")
    private String userStatus;

    @JsonProperty("eulaStatus")
    private String eulaStatus;

    @JsonProperty("language")
    private String language;

    @JsonProperty("pushNotification")
    private Integer pushNotification;

    @JsonProperty(value = "title", defaultValue = "")
    private String title;

    @JsonProperty(value = "csStartDate")
    private String csStartDate;

    @JsonProperty(value = "csEndDate")
    private String csEndDate;

    @JsonProperty(value = "userRole", defaultValue = "0")
    private Integer userRole;

    @JsonProperty(value = "aliasEmail", defaultValue = "")
    private String aliasEmail;

    @JsonProperty(value = "businessStatus")
    private int businessStatus;

    @JsonProperty(value = "lastLoginTime")
    private String lastLoginTime;

    @JsonProperty(value = "modifyUserUUID", defaultValue = "")
    private String modifyUserUUID;

    @JsonProperty(value = "thoughtful", defaultValue = "1")
    private Integer thoughtful;

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getUserUUID() {
        return userUUID;
    }

    public void setUserUUID(String userUUID) {
        this.userUUID = userUUID;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getCurrentDB()
    {
        return currentDB;
    }

    public void setCurrentDB(String currentDB) { this.currentDB = currentDB;}

    public Boolean getWelcomeEmail() {
        return welcomeEmail;
    }

    public void setWelcomeEmail(Boolean welcomeEmail) {
        this.welcomeEmail = welcomeEmail;
    }

    public Integer getPasswordUpdateCount() {
        return passwordUpdateCount;
    }

    public void setPasswordUpdateCount(Integer passwordUpdateCount) {
        this.passwordUpdateCount = passwordUpdateCount;
    }

    public Integer getAlreadyExists() {
        return alreadyExists;
    }

    public void setAlreadyExists(Integer alreadyExists) {
        this.alreadyExists = alreadyExists;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }


    public Integer getCompleteSocialSignup() {
        return completeSocialSignup;
    }

    public void setCompleteSocialSignup(Integer completeSocialSignup) {
        this.completeSocialSignup = completeSocialSignup;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }


    public Integer getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }

    public Integer getEmpRangeId() {
        return empRangeId;
    }

    public void setEmpRangeId(Integer empRangeId) {
        this.empRangeId = empRangeId;
    }

    public Integer getBusinessTypeId() {
        return businessTypeId;
    }

    public void setSignupTypeId(Integer signupTypeId) {
        this.signupTypeId = signupTypeId;
    }

    public Integer getSignupTypeId() {
        return signupTypeId;
    }

    public void setBusinessTypeId(Integer businessTypeId) {
        this.businessTypeId = businessTypeId;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCompanyCode(){ return companyCode; }

    public void setCompanyCode(String companyCode){ this.companyCode = companyCode; }

    public String getUserDB(){ return userDB; }

    public void setUserDB(String userDB){ this.userDB = userDB; }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getTrialFlag() {
        return trialFlag;
    }

    public void setTrialFlag(Integer trialFlag) {
        this.trialFlag = trialFlag;
    }

    public Integer getConfirmationStatus() {
        return this.confirmationStatus;
    }

    public void setConfirmationStatus(Integer confirmationStatus) {
        this.confirmationStatus = confirmationStatus;
    }


    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Integer getRoleId() { return roleId; }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getEulaStatus() {
        return eulaStatus;
    }

    public void setEulaStatus(String eulaStatus) {
        this.eulaStatus = eulaStatus;
    }

    public String getLanguage() { return language; }

    public void setLanguage(String language) { this.language = language; }

    public Integer getPushNotification() { return pushNotification; }

    public void setPushNotification(Integer pushNotification) { this.pushNotification = pushNotification;  }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCsStartDate() { return csStartDate; }

    public void setCsStartDate(String csStartDate) { this.csStartDate = csStartDate; }

    public String getCsEndDate() { return csEndDate; }

    public void setCsEndDate(String csEndDate) { this.csEndDate = csEndDate; }

    public Integer getUserRole() { return userRole; }

    public void setUserRole(Integer userRole) { this.userRole = userRole; }

    public String getAliasEmail() { return aliasEmail; }

    public void setAliasEmail(String aliasEmail) { this.aliasEmail = aliasEmail; }

    public int getBusinessStatus() {
        return businessStatus;
    }

    public void setBusinessStatus(int businessStatus) {
        this.businessStatus = businessStatus;
    }

    public String getLastLoginTime() { return lastLoginTime; }

    public void setLastLoginTime(String lastLoginTime) { this.lastLoginTime = lastLoginTime; }

    public String getModifyUserUUID() {
        return modifyUserUUID;
    }

    public void setModifyUserUUID(String modifyUserUUID) {
        this.modifyUserUUID = modifyUserUUID;
    }

    public Integer getThoughtful() { return thoughtful; }

    public void setThoughtful(Integer thoughtful) { this.thoughtful = thoughtful; }
}
