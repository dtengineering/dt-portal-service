package com.dropthought.portal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

public class PlanConfigurationsBean implements Serializable {

    @JsonProperty("planConfigId")
    private Integer planConfigId;

    @JsonProperty("planName")
    private String planName;

    @JsonProperty("email")
    private String email;

    @JsonProperty("sms")
    private String sms;

    @JsonProperty("qrcode")
    private String qrcode;

    @JsonProperty("kiosk")
    private String kiosk;

    @JsonProperty("shareableLink")
    private String shareableLink;

    @JsonProperty("textAnalytics")
    private String textAnalytics;

    @JsonProperty("planConfigUUID")
    private String planConfigUUID;

    @JsonProperty("logic")
    private String logic;

    @JsonProperty("respondent")
    private String respondent;

    @JsonProperty("segment")
    private String segment;

    @JsonProperty("metadataQuestion")
    private String metadataQuestion;

    @JsonProperty("mobileApp")
    private String mobileApp;

    @JsonProperty("emotionAnalysis")
    private String emotionAnalysis;

    @JsonProperty("intentAnalysis")
    private String intentAnalysis;

    @JsonProperty("trigger")
    private String trigger;

    @JsonProperty("advancedSchedule")
    private String advancedSchedule;

    @JsonProperty("english")
    private String english;

    @JsonProperty("arabic")
    private String arabic;

    @JsonProperty("multiSurveys")
    private String multiSurveys;

    @JsonProperty("notification")
    private String notification;

    @JsonProperty("bitly")
    private String bitly;

    @JsonProperty("webHooks")
    private String webHooks;

    @JsonProperty("preferredMetric")
    private String preferredMetric;

    @JsonProperty("createdBy")
    private Integer createdBy;

    @JsonProperty("modifiedBy")
    private Integer modifiedBy;

    @JsonProperty("createdTime")
    private Timestamp createdTime;

    @JsonProperty("modifiedTime")
    private Timestamp modifiedTime;

    @JsonProperty("channels")
    private Map channels;

    @JsonProperty("categoryAnalysis")
    private String categoryAnalysis;

    @JsonProperty("advancedTextAnalytics")
    private String advancedTextAnalytics;

    @JsonProperty("noOfUsers")
    private Integer noOfUsers;

    @JsonProperty("noOfActivePrograms")
    private Integer noOfActivePrograms;

    @JsonProperty("noOfResponses")
    private Integer noOfResponses;

    @JsonProperty("noOfMetrics")
    private Integer noOfMetrics;

    @JsonProperty("noOfEmailsSent")
    private Integer noOfEmailsSent;

    @JsonProperty("noOfSms")
    private Integer noOfSms;

    @JsonProperty("noOfApi")
    private Integer noOfApi;

    @JsonProperty("noOfFilters")
    private Integer noOfFilters;

    @JsonProperty("noOfTriggers")
    private Integer noOfTriggers;

    @JsonProperty("dataRetention")
    private Integer dataRetention;

    @JsonProperty("noOfLists")
    private Integer noOfLists;

    @JsonProperty("noOfRecipients")
    private Integer noOfRecipients;

    @JsonProperty("hippa")
    private String hippa;

    @JsonProperty("cohortAnalysis")
    private String cohortAnalysis;

    @JsonProperty("advancedTemplates")
    private String advancedTemplates;

    @JsonProperty("metricCorrelation")
    private String metricCorrelation;

    @JsonProperty("apiKey")
    private String apiKey;

    @JsonProperty("whatfix")
    private String whatfix;

    @JsonProperty("multipleSublink")
    private String multipleSublink;

    @JsonProperty("advancedAnalysis")
    private String advancedAnalysis;

    @JsonProperty("exportLinks")
    private String exportLinks;

    @JsonProperty("respondentTracker")
    private String respondentTracker;

    @JsonProperty("fromCustomization")
    private String fromCustomization;

    @JsonProperty("integrations")
    private String integrations;

    @JsonProperty("noOfIntegrations")
    private Integer noOfIntegrations;

    @JsonProperty("noOfKiosks")
    private Integer noOfKiosks;

    @JsonProperty("dynamicLinks")
    private String dynamicLinks;

    @JsonProperty("noOfShareableSublink")
    private Integer noOfShareableSublink;

    @JsonProperty("noOfMultipleStaticQr")
    private Integer noOfMultipleStaticQr;

    @JsonProperty("noOfDynamicSwitchQr")
    private Integer noOfDynamicSwitchQr;

    @JsonProperty("noOfDynamicGroupQr")
    private Integer noOfDynamicGroupQr;

    @JsonProperty("mfa")
    private String mfa;

    @JsonProperty("focusMetric")
    private String focusMetric;

    @JsonProperty("advancedReport")
    private String advancedReport;

    @JsonProperty("previouslyUsedQuestionLibrary")
    private String previouslyUsedQuestionLibrary;

    @JsonProperty("predefinedQuestionLibrary")
    private String predefinedQuestionLibrary;

    @JsonProperty("defaultRecommendation")
    private String defaultRecommendation;

    @JsonProperty("userDefinedRecommendation")
    private String userDefinedRecommendation;

    @JsonProperty("customThemes")
    private String customThemes;

    @JsonProperty("chatbot")
    private String chatbot;

    @JsonProperty("auditProgram")
    private String auditProgram;

    @JsonProperty("collaboration")
    private String collaboration;

    @JsonProperty("customDashboard")
    private String customDashboard;

    @JsonProperty("whatsapp")
    private String whatsapp;

    @JsonProperty("noOfWhatsapp")
    private Integer noOfWhatsapp;

    @JsonProperty("rCoefficient")
    private String rCoefficient;

    @JsonProperty("closeLoop")
    private String closeLoop;

    @JsonProperty("programThrottling")
    private String programThrottling;

    @JsonProperty("gamePlan")
    private String gamePlan;

    @JsonProperty("notes")
    private String notes;

    @JsonProperty("active")
    private String active;

    @JsonProperty("pricing")
    private Pricing pricing;

    @JsonProperty("trialDays")
    private Integer trialDays;

    @JsonProperty("uniqueLinkEmail")
    private String uniqueLinkEmail;

    @JsonProperty("uniqueLinkSms")
    private String uniqueLinkSms;

    @JsonProperty("uniqueLinkWhatsapp")
    private String uniqueLinkWhatsapp;

    @JsonProperty("uniqueLinks")
    private String uniqueLinks;

    @JsonProperty("programFeatures")
    private String programFeatures;

    @JsonProperty("programMetric")
    private String programMetric;

    @JsonProperty("programOverview")
    private String programOverview;

    @JsonProperty("planProperties")
    private List<PlanProperties> planProperties;

    @JsonProperty("sso")
    private String sso;

    @JsonProperty("googleSSO")
    private String googleSSO;

    @JsonProperty("microsoftSSO")
    private String microsoftSSO;

    @JsonProperty("appleSSO")
    private String appleSSO;

    @JsonProperty("aiSurveysLimit")
    private String aiSurveysLimit;

    @JsonProperty("taskManager")
    private String taskManager;

    private String aiSurveys;

    @JsonProperty("lti")
    private String lti;

    @JsonProperty("ltiConfig")
    private Map ltiConfig;

    private String responseQuota;
    private String recurrence;
    private String customDashboardBuilder;

    public String getPlanConfigUUID() {
        return planConfigUUID;
    }

    public void setPlanConfigUUID(String planConfigUUID) {
        this.planConfigUUID = planConfigUUID;
    }



    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSms() {
        return sms;
    }

    public void setSms(String sms) {
        this.sms = sms;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public String getShareableLink() {
        return shareableLink;
    }

    public void setShareableLink(String shareableLink) {
        this.shareableLink = shareableLink;
    }

    public String getTextAnalytics() {
        return textAnalytics;
    }

    public void setTextAnalytics(String textAnalytics) {
        this.textAnalytics = textAnalytics;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getLogic() { return logic; }

    public void setLogic(String logic) { this.logic = logic; }

    public String getRespondent() { return respondent; }

    public void setRespondent(String respondent) { this.respondent = respondent; }

    public String getSegment() { return segment; }

    public void setSegment(String segment) { this.segment = segment; }

    public String getKiosk() { return kiosk; }

    public void setKiosk(String kiosk) { this.kiosk = kiosk; }
    public String getMetadataQuestion() {  return metadataQuestion;     }

    public void setMetadataQuestion(String metadataQuestion) { this.metadataQuestion = metadataQuestion;   }

    public String getMobileApp() { return mobileApp; }

    public void setMobileApp(String mobileApp) { this.mobileApp = mobileApp; }

    public String getEmotionAnalysis() { return emotionAnalysis; }

    public void setEmotionAnalysis(String emotionAnalysis) { this.emotionAnalysis = emotionAnalysis; }

    public String getIntentAnalysis() { return intentAnalysis; }

    public void setIntentAnalysis(String intentAnalysis) { this.intentAnalysis = intentAnalysis; }

    public String getTrigger() { return trigger; }

    public void setTrigger(String trigger) { this.trigger = trigger; }

    public String getAdvancedSchedule() {
        return advancedSchedule;
    }

    public void setAdvancedSchedule(String advancedSchedule) {
        this.advancedSchedule = advancedSchedule;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public String getArabic() {
        return arabic;
    }

    public void setArabic(String arabic) {
        this.arabic = arabic;
    }

    public String getMultiSurveys() {
        return multiSurveys;
    }

    public void setMultiSurveys(String multiSurveys) {
        this.multiSurveys = multiSurveys;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public Map getChannels() { return channels; }

    public void setChannels(Map channels) { this.channels = channels; }

    public String getBitly() { return bitly; }

    public void setBitly(String bitly) { this.bitly = bitly; }

    public String getWebHooks() { return webHooks; }

    public void setWebHooks(String webHooks) { this.webHooks = webHooks; }

    public String getPreferredMetric() { return preferredMetric; }

    public void setPreferredMetric(String preferredMetric) { this.preferredMetric = preferredMetric; }

    public String getCategoryAnalysis() {
        return categoryAnalysis;
    }

    public void setCategoryAnalysis(String categoryAnalysis) {
        this.categoryAnalysis = categoryAnalysis;
    }

    public String getAdvancedTextAnalytics() {
        return advancedTextAnalytics;
    }

    public void setAdvancedTextAnalytics(String advancedTextAnalytics) {
        this.advancedTextAnalytics = advancedTextAnalytics;
    }

    public Integer getNoOfUsers() {
        return noOfUsers;
    }

    public void setNoOfUsers(Integer noOfUsers) {
        this.noOfUsers = noOfUsers;
    }

    public Integer getNoOfActivePrograms() {
        return noOfActivePrograms;
    }

    public void setNoOfActivePrograms(Integer noOfActivePrograms) {
        this.noOfActivePrograms = noOfActivePrograms;
    }

    public Integer getNoOfResponses() {
        return noOfResponses;
    }

    public void setNoOfResponses(Integer noOfResponses) {
        this.noOfResponses = noOfResponses;
    }

    public Integer getNoOfMetrics() {
        return noOfMetrics;
    }

    public void setNoOfMetrics(Integer noOfMetrics) {
        this.noOfMetrics = noOfMetrics;
    }

    public Integer getNoOfEmailsSent() {
        return noOfEmailsSent;
    }

    public void setNoOfEmailsSent(Integer noOfEmailsSent) {
        this.noOfEmailsSent = noOfEmailsSent;
    }

    public Integer getNoOfSms() {
        return noOfSms;
    }

    public void setNoOfSms(Integer noOfSms) {
        this.noOfSms = noOfSms;
    }

    public Integer getNoOfApi() {
        return noOfApi;
    }

    public void setNoOfApi(Integer noOfApi) {
        this.noOfApi = noOfApi;
    }

    public Integer getNoOfFilters() {
        return noOfFilters;
    }

    public void setNoOfFilters(Integer noOfFilters) {
        this.noOfFilters = noOfFilters;
    }

    public Integer getNoOfTriggers() {
        return noOfTriggers;
    }

    public void setNoOfTriggers(Integer noOfTriggers) {
        this.noOfTriggers = noOfTriggers;
    }

    public Integer getDataRetention() {
        return dataRetention;
    }

    public void setDataRetention(Integer dataRetention) {
        this.dataRetention = dataRetention;
    }

    public Integer getNoOfLists() {
        return noOfLists;
    }

    public void setNoOfLists(Integer noOfLists) {
        this.noOfLists = noOfLists;
    }

    public Integer getNoOfRecipients() {
        return noOfRecipients;
    }

    public void setNoOfRecipients(Integer noOfRecipients) {
        this.noOfRecipients = noOfRecipients;
    }

    public String getHippa() {
        return hippa;
    }

    public void setHippa(String hippa) {
        this.hippa = hippa;
    }

    public String getCohortAnalysis() { return cohortAnalysis; }

    public void setCohortAnalysis(String cohortAnalysis) { this.cohortAnalysis = cohortAnalysis; }

    public String getAdvancedTemplates() { return advancedTemplates; }

    public void setAdvancedTemplates(String advancedTemplates) { this.advancedTemplates = advancedTemplates; }

    public String getMetricCorrelation() { return metricCorrelation; }

    public void setMetricCorrelation(String metricCorrelation) { this.metricCorrelation = metricCorrelation; }

    public String getApiKey() { return apiKey; }

    public void setApiKey(String apiKey) { this.apiKey = apiKey; }

    public String getWhatfix() { return whatfix; }

    public void setWhatfix(String whatfix) { this.whatfix = whatfix; }

    public String getMultipleSublink() { return multipleSublink; }

    public void setMultipleSublink(String multipleSublink) { this.multipleSublink = multipleSublink; }

    public String getAdvancedAnalysis() { return advancedAnalysis; }

    public void setAdvancedAnalysis(String advancedAnalysis) { this.advancedAnalysis = advancedAnalysis; }

    public String getExportLinks() { return exportLinks; }

    public void setExportLinks(String exportLinks) { this.exportLinks = exportLinks; }

    public String getRespondentTracker() {
        return respondentTracker;
    }

    public void setRespondentTracker(String respondentTracker) {
        this.respondentTracker = respondentTracker;
    }

    public String getFromCustomization() {
        return fromCustomization;
    }

    public void setFromCustomization(String fromCustomization) {
        this.fromCustomization = fromCustomization;
    }

    public String getIntegrations() {
        return integrations;
    }

    public void setIntegrations(String integrations) {
        this.integrations = integrations;
    }

    public Integer getNoOfIntegrations() {
        return noOfIntegrations;
    }

    public void setNoOfIntegrations(Integer noOfIntegrations) {
        this.noOfIntegrations = noOfIntegrations;
    }

    public Integer getNoOfKiosks() {
        return noOfKiosks;
    }

    public void setNoOfKiosks(Integer noOfKiosks) {
        this.noOfKiosks = noOfKiosks;
    }

    public String getDynamicLinks() {
        return dynamicLinks;
    }

    public void setDynamicLinks(String dynamicLinks) {
        this.dynamicLinks = dynamicLinks;
    }

    public Integer getNoOfShareableSublink() {
        return noOfShareableSublink;
    }

    public void setNoOfShareableSublink(Integer noOfShareableSublink) {
        this.noOfShareableSublink = noOfShareableSublink;
    }

    public Integer getNoOfMultipleStaticQr() {
        return noOfMultipleStaticQr;
    }

    public void setNoOfMultipleStaticQr(Integer noOfMultipleStaticQr) {
        this.noOfMultipleStaticQr = noOfMultipleStaticQr;
    }

    public Integer getNoOfDynamicSwitchQr() {
        return noOfDynamicSwitchQr;
    }

    public void setNoOfDynamicSwitchQr(Integer noOfDynamicSwitchQr) {
        this.noOfDynamicSwitchQr = noOfDynamicSwitchQr;
    }

    public Integer getNoOfDynamicGroupQr() {
        return noOfDynamicGroupQr;
    }

    public void setNoOfDynamicGroupQr(Integer noOfDynamicGroupQr) {
        this.noOfDynamicGroupQr = noOfDynamicGroupQr;
    }

    public String getMfa() {
        return mfa;
    }

    public void setMfa(String mfa) {
        this.mfa = mfa;
    }

    public String getFocusMetric() {
        return focusMetric;
    }

    public void setFocusMetric(String focusMetric) {
        this.focusMetric = focusMetric;
    }

    public String getAdvancedReport() {
        return advancedReport;
    }

    public void setAdvancedReport(String advancedReport) {
        this.advancedReport = advancedReport;
    }

    public void setPreviouslyUsedQuestionLibrary(String previouslyUsedQuestionLibrary) {
        this.previouslyUsedQuestionLibrary = previouslyUsedQuestionLibrary;
    }

    public String getPreviouslyUsedQuestionLibrary() {
        return previouslyUsedQuestionLibrary;
    }

    public String getPredefinedQuestionLibrary() {
        return predefinedQuestionLibrary;
    }

    public void setPredefinedQuestionLibrary(String predefinedQuestionLibrary) {
        this.predefinedQuestionLibrary = predefinedQuestionLibrary;
    }

    public String getDefaultRecommendation() {
        return defaultRecommendation;
    }

    public void setDefaultRecommendation(String defaultRecommendation) {
        this.defaultRecommendation = defaultRecommendation;
    }

    public String getUserDefinedRecommendation() {
        return userDefinedRecommendation;
    }

    public void setUserDefinedRecommendation(String userDefinedRecommendation) {
        this.userDefinedRecommendation = userDefinedRecommendation;
    }

    public String getCustomThemes() {return customThemes;}

    public void setCustomThemes(String customThemes) {this.customThemes = customThemes;}

    public String getChatbot() {return chatbot;}

    public void setChatbot(String chatbot) {this.chatbot = chatbot;}

    public String getAuditProgram() {return auditProgram;}

    public void setAuditProgram(String auditProgram) {this.auditProgram = auditProgram;}

    public String getCollaboration() {return collaboration;}

    public void setCollaboration(String collaboration) {this.collaboration = collaboration;}

    public String getCustomDashboard() {return customDashboard;}

    public void setCustomDashboard(String customDashboard) {this.customDashboard = customDashboard;}

    public Integer getPlanConfigId() {
        return planConfigId;
    }

    public Timestamp getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Timestamp createdTime) {
        this.createdTime = createdTime;
    }

    public Timestamp getModifiedTime() {
        return modifiedTime;
    }

    public String getPlanName() {
        return planName;
    }

    public void setModifiedTime(Timestamp modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public void setPlanConfigId(Integer planConfigId) {
        this.planConfigId = planConfigId;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public Integer getNoOfWhatsapp() {
        return noOfWhatsapp;
    }

    public String getWhatsapp() {
        return whatsapp;
    }

    public void setNoOfWhatsapp(Integer noOfWhatsapp) {
        this.noOfWhatsapp = noOfWhatsapp;
    }

    public void setWhatsapp(String whatsapp) {
        this.whatsapp = whatsapp;
    }

    public String getRCoefficient() {
        return rCoefficient;
    }

    public void setRCoefficient(String rCoefficient) {
        this.rCoefficient = rCoefficient;
    }

    public String getCloseLoop() {
        return closeLoop;
    }

    public void setCloseLoop(String closeLoop) {
        this.closeLoop = closeLoop;
    }

    public String getProgramThrottling() {
        return programThrottling;
    }

    public void setProgramThrottling(String programThrottling) {
        this.programThrottling = programThrottling;
    }

    public String getGamePlan() {
        return gamePlan;
    }

    public void setGamePlan(String gamePlan) {
        this.gamePlan = gamePlan;
    }
    public String getNotes() {
        return notes;
    }
    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Pricing getPricing() {
        return pricing;
    }

    public void setPricing(Pricing pricing) {
        this.pricing = pricing;
    }

    public Integer getTrialDays() {
        return trialDays;
    }

    public void setTrialDays(Integer trialDays) {
        this.trialDays = trialDays;
    }

    public String getUniqueLinkEmail() {
        return uniqueLinkEmail;
    }

    public void setUniqueLinkEmail(String uniqueLinkEmail) {
        this.uniqueLinkEmail = uniqueLinkEmail;
    }

    public String getUniqueLinkSms() {
        return uniqueLinkSms;
    }

    public void setUniqueLinkSms(String uniqueLinkSms) {
        this.uniqueLinkSms = uniqueLinkSms;
    }

    public String getUniqueLinkWhatsapp() {
        return uniqueLinkWhatsapp;
    }

    public void setUniqueLinkWhatsapp(String uniqueLinkWhatsapp) {
        this.uniqueLinkWhatsapp = uniqueLinkWhatsapp;
    }

    public String getProgramFeatures() {
        return programFeatures;
    }

    public void setProgramFeatures(String programFeatures) {
        this.programFeatures = programFeatures;
    }

    public String getUniqueLinks() {
        return uniqueLinks;
    }

    public void setUniqueLinks(String uniqueLinks) {
        this.uniqueLinks = uniqueLinks;
    }

    public String getProgramMetric() {
        return programMetric;
    }

    public void setProgramMetric(String programMetric) {
        this.programMetric = programMetric;
    }

    public String getProgramOverview() {
        return programOverview;
    }

    public void setProgramOverview(String programOverview) {
        this.programOverview = programOverview;
    }

    public void setPlanProperties(List<PlanProperties> planProperties) {
        this.planProperties = planProperties;
    }

    public List<PlanProperties> getPlanProperties() {
        return planProperties;
    }

    public String getGoogleSSO() {
        return googleSSO;
    }

    public String getAppleSSO() {
        return appleSSO;
    }

    public String getMicrosoftSSO() {
        return microsoftSSO;
    }

    public String getSSO() {
        return sso;
    }

    public void setAppleSSO(String appleSSO) {
        this.appleSSO = appleSSO;
    }

    public void setGoogleSSO(String googleSSO) {
        this.googleSSO = googleSSO;
    }

    public void setMicrosoftSSO(String microsoftSSO) {
        this.microsoftSSO = microsoftSSO;
    }

    public void setSSO(String sso) {
        this.sso = sso;
    }

    public String getAiSurveysLimit() {
        return aiSurveysLimit;
    }

    public void setAiSurveysLimit(String aiSurveysLimit) {
        this.aiSurveysLimit = aiSurveysLimit;
    }

    public String getTaskManager() {
        return taskManager;
    }
    public void setTaskManager(String taskManager) {
        this.taskManager = taskManager;
    }

    public String getAiSurveys() {
        return aiSurveys;
    }

    public void setAiSurveys(String aiSurveys) {
        this.aiSurveys = aiSurveys;
    }

    public String getLti() {
        return lti;
    }

    public void setLti(String lti) {
        this.lti = lti;
    }

    public Map getLtiConfig() {
        return ltiConfig;
    }

    public void setLtiConfig(Map ltiConfig) {
        this.ltiConfig = ltiConfig;
    }

    public String getResponseQuota() {
        return responseQuota;
    }

    public void setResponseQuota(String responseQuota) {
        this.responseQuota = responseQuota;
    }

    public String getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(String recurrence) {
        this.recurrence = recurrence;
    }

    public String getCustomDashboardBuilder() {
        return customDashboardBuilder;
    }

    public void setCustomDashboardBuilder(String customDashboardBuilder) {
        this.customDashboardBuilder = customDashboardBuilder;
    }
}

