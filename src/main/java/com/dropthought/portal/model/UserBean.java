package com.dropthought.portal.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * Created by Prabhakar Thakur on 3/7/2020.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserBean implements Serializable {


    @JsonProperty("userId")
    private Integer userId;

    @NotBlank(message = "fullName should not be null or empty")
    @JsonProperty("fullName")
    private String fullName;

    @JsonProperty("firstName")
    private String firstName;

    @JsonProperty("lastName")
    private String lastName;

    @JsonProperty("username")
    private String userName;

    @NotBlank(message = "userEmail should not be null or empty")
    @JsonProperty("userEmail")
    private String userEmail;

    @JsonProperty("password")
    private String password;

    @JsonProperty("phone")
    private String phone;

    @Min(value = 0, message = "confirmationStatus should be valid number")
    @JsonProperty("confirmationStatus")
    private Integer confirmationStatus;

    @JsonProperty("passwordUpdateCount")
    private Integer passwordUpdateCount;

    @JsonProperty("createdBy")
    private String createdBy;

    @JsonProperty("created_time")
    private String created;

    @JsonProperty("modified_time")
    private String modified;

    @JsonProperty("modifiedBy")
    private String modifiedBy;

    @JsonProperty("host")
    private String host;

    @JsonProperty("userUUID")
    private String userUUID;

    @JsonProperty("user_status")
    private String userStatus;

    @JsonProperty("welcomeEmail")
    private Boolean welcomeEmail;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getConfirmationStatus() {
        return confirmationStatus;
    }

    public void setConfirmationStatus(Integer confirmationStatus) {
        this.confirmationStatus = confirmationStatus;
    }

    public Integer getPasswordUpdateCount() {
        return passwordUpdateCount;
    }

    public void setPasswordUpdateCount(Integer passwordUpdateCount) {
        this.passwordUpdateCount = passwordUpdateCount;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUserUUID() {
        return userUUID;
    }

    public void setUserUUID(String userUUID) {
        this.userUUID = userUUID;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public Boolean getWelcomeEmail() {
        return welcomeEmail;
    }

    public void setWelcomeEmail(Boolean welcomeEmail) {
        this.welcomeEmail = welcomeEmail;
    }
}
