package com.dropthought.portal.model;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Modified by Prabhakar Thakur on 14/7/2020.
 */
public class MessageBean implements Serializable {

    private boolean success = true;
    private List<Map> result = new ArrayList<Map>();
    private String status;
    private String message;


    /**
     * Constructor
     */
    public MessageBean() {
    }

    /**
     * Constructor
     *
     * @param success
     */

    public MessageBean(boolean success) {
        this.success = success;
    }

    /**
     * Constructor
     *
     * @param map
     */
    public MessageBean(Map map) {
        this.success = success;
        List<Map> list = new ArrayList<Map>();
        list.add(map);
        this.result = list;
    }

    /**
     * Constructor
     *
     * @param result
     */
    public MessageBean(List<Map> result) {
        this.result = result;
    }

    /**
     * Constructor
     *
     * @param success
     * @param map
     */
    public MessageBean(boolean success, Map map) {
        this.success = success;
        List<Map> list = new ArrayList<Map>();
        list.add(map);
        this.result = list;
    }

    /**
     * Constructor
     *
     * @param success
     * @param result
     */
    public MessageBean(boolean success, List<Map> result) {
        this.success = success;
        //List<Map> list = new ArrayList<Map>();
        //list.add(map);
        this.result = result;
    }

    /**
     * Getter
     *
     * @return
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * Setter
     *
     * @param success
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * Setter
     *
     * @return
     */
    public List<Map> getResult() {
        return result;
    }

    /**
     * Getter
     *
     * @param list
     */
    public void setResult(List<Map> list) {
        this.result = list;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Override toString()
     *
     * @return
     */
    @Override
    public String toString() {
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (com.fasterxml.jackson.core.JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
