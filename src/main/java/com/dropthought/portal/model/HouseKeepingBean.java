package com.dropthought.portal.model;

import com.dropthought.portal.common.DTConstants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Pattern;
import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HouseKeepingBean implements Serializable {

    @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessUUID")
    @JsonProperty("businessId")
    private String businessUUID;

    @JsonProperty("txtAnalytics")
    private Boolean txtAnalytics;

    public String getBusinessUUID() {
        return businessUUID;
    }

    public void setBusinessUUID(String businessUUID) {
        this.businessUUID = businessUUID;
    }

    public Boolean getTxtAnalytics() {
        return txtAnalytics;
    }

    public void setTxtAnalytics(Boolean txtAnalytics) {
        this.txtAnalytics = txtAnalytics;
    }
}
