package com.dropthought.portal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class DTQuestionBean {

    @JsonProperty("questionId")
    private String questionId;

    @JsonProperty("questionTitle")
    private String questionText;

    @JsonProperty("createdTime")
    private String createdTime;

    @JsonProperty("modifiedTime")
    private String modifiedTime;

    @JsonProperty("createdBy")
    private String createdBy;

    @JsonProperty("modifiedBy")
    private String modifiedBy;

    @JsonProperty("category")
    private String metric;

    /*@ApiModelProperty(value = "question metric name")
    @JsonProperty("metricLabel")
    private String metricLabel;*/

    @JsonProperty("options")
    private List<String> options;

    @JsonProperty("optionIds")
    private List<Integer> optionIds;

    @JsonProperty("scale")
    private String scale;

    @JsonProperty("type")
    private String type;

    @JsonProperty("mandatory")
    private boolean mandatory;

    @JsonProperty("mode")
    private String mode;

    @JsonProperty("subType")
    private String ratingType;

    @JsonProperty("cc")
    private String currentDB;

    @JsonProperty("metaDataType")
    private String metaDataType;

    @JsonProperty("questionBrand")
    private String questionBrand;

    @JsonProperty("otherText")
    private String otherText;

    public String getQuestionBrand() {
        return questionBrand;
    }

    public void setQuestionBrand(String questionBrand) {
        this.questionBrand = questionBrand;
    }

    public String getMetaDataType() { return metaDataType; }

    public void setMetaDataType(String metaDataType) { this.metaDataType = metaDataType; }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    /*public String getMetricLabel() {
        return metricLabel;
    }

    public void setMetricLabel(String metricLabel) {
        this.metricLabel = metricLabel;
    }*/

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    public List<Integer> getOptionIds() {
        return optionIds;
    }

    public void setOptionIds(List<Integer> optionIds) {
        this.optionIds = optionIds;
    }

    public String getScale() {
        return scale;
    }

    public void setScale(String scale) {
        this.scale = scale;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getRatingType() {
        return ratingType;
    }

    public void setRatingType(String ratingType) {
        this.ratingType = ratingType;
    }

    public String getCurrentDB() {
        return currentDB;
    }

    public void setCurrentDB(String currentDB) {
        this.currentDB = currentDB;
    }

    public String getOtherText() { return otherText; }

    public void setOtherText(String otherText) { this.otherText = otherText; }
}
