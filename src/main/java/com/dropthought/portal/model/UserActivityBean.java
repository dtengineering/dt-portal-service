package com.dropthought.portal.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserActivityBean implements Serializable {

    @JsonProperty(value = "userEmail")
    private List userEmail;

    @JsonProperty("role")
    private List role;

    @JsonProperty("location")
    private List location;

    @JsonProperty("activity")
    private List activity;

    @JsonProperty("userId")
    private List userId;

    @JsonProperty("fromDate")
    private String fromDate;

    @JsonProperty("toDate")
    private String toDate;

    @JsonProperty(value = "timeZone")
    private String timeZone;

    @JsonProperty(value = "pageId")
    private int pageId;

    @JsonProperty(value = "username")
    private List username;

    public List getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(List userEmail) {
        this.userEmail = userEmail;
    }

    public List getRole() {
        return role;
    }

    public void setRole(List role) {
        this.role = role;
    }

    public List getLocation() {
        return location;
    }

    public void setLocation(List location) {
        this.location = location;
    }

    public List getActivity() {
        return activity;
    }

    public void setActivity(List activity) {
        this.activity = activity;
    }

    public List getUserId() {
        return userId;
    }

    public void setUserId(List userId) {
        this.userId = userId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public int getPageId() {
        return pageId;
    }

    public void setPageId(int pageId) {
        this.pageId = pageId;
    }

    public List getUsername() { return username; }

    public void setUsername(List username) { this.username = username; }
}
