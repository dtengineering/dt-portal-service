package com.dropthought.portal.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConnectedAccountBean implements Serializable {

    private int connectedBusinessId;
    private int connectedUserId;
    private String refreshToken;
    private int activation;
    private String expiration;
    private int userId;
    private int businessId;
}
