package com.dropthought.portal.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Positive;
import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserLogBean implements Serializable {

    @Positive(message = "businessId should be positive")
    @JsonProperty("businessId")
    private Integer businessId;

    @JsonProperty("username")
    private String username;

    @JsonProperty("userEmail")
    private String userEmail;

    @JsonProperty("location")
    private String location;

    @JsonProperty("ipAddress")
    private String ipAddress;

    @JsonProperty("role")
    private String role;

    @JsonProperty("activity")
    private String activity;

    @JsonProperty("createdBy")
    private Integer createdBy;

    @JsonProperty("createdTime")
    private String createdTime;


    public Integer getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

}
