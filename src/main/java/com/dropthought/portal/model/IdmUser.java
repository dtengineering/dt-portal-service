package com.dropthought.portal.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IdmUser implements Serializable {
    private String oktaUserId;
    private String oktaBusinessId;
    private String id;
    private String username;
    private String email;
    private String password; // idm-service -> used only to create user and reset password
    private String confirmation; // idm-service -> used only to reset password
    private String firstname;
    private String lastname;
    private String role;

    @JsonIgnore
    private Set<IdmUserRole> userRoles;
    private boolean enabled; // ACTIVE|INACTIVE

    private int statusCode;
    private String status;
    private Map<String, List<String>> attributes;
}

