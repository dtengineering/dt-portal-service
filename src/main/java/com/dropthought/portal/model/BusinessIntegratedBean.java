package com.dropthought.portal.model;


import java.io.Serializable;
import java.util.Map;

public class BusinessIntegratedBean implements Serializable {

    private boolean isMFAChanged;
    private boolean isTAChanged;
    private boolean isKioskChanged;
    private boolean isAPIKeyChanged;
    private boolean isLimitChanged;
    private boolean isLinkChanged;
    private boolean isSMSChanged;
    private boolean isWhatsappChanged;
    private boolean isQRChanged;
    private boolean isEmailChanged;
    private boolean isTemplateChanged;
    private Map usageLimit;
    private Map channels;
    private boolean isRecipientsChanged;
    private boolean isResponseChanged;
    private boolean isCustomThemeChanged;
    private boolean isSSOChanged;
    private boolean isLtiChanged;

    public boolean isPublishToEliza() {
        return isPublishToEliza;
    }

    public void setPublishToEliza(boolean publishToEliza) {
        isPublishToEliza = publishToEliza;
    }

    private boolean isPublishToEliza;

    public boolean isMFAChanged() {
        return isMFAChanged;
    }

    public void setMFAChanged(boolean MFAChanged) {
        isMFAChanged = MFAChanged;
    }

    public boolean isTAChanged() {
        return isTAChanged;
    }

    public void setTAChanged(boolean TAChanged) {
        isTAChanged = TAChanged;
    }

    public boolean isKioskChanged() {
        return isKioskChanged;
    }

    public void setKioskChanged(boolean kioskChanged) {
        isKioskChanged = kioskChanged;
    }

    public boolean isAPIKeyChanged() {
        return isAPIKeyChanged;
    }

    public void setAPIKeyChanged(boolean APIKeyChanged) {
        isAPIKeyChanged = APIKeyChanged;
    }

    public boolean isLimitChanged() {
        return isLimitChanged;
    }

    public void setLimitChanged(boolean limitChanged) {
        isLimitChanged = limitChanged;
    }

    public boolean isLinkChanged() {
        return isLinkChanged;
    }

    public void setLinkChanged(boolean linkChanged) {
        isLinkChanged = linkChanged;
    }

    public boolean isSMSChanged() {
        return isSMSChanged;
    }

    public void setSMSChanged(boolean SMSChanged) {
        isSMSChanged = SMSChanged;
    }

    public boolean isQRChanged() {
        return isQRChanged;
    }

    public void setQRChanged(boolean QRChanged) {
        isQRChanged = QRChanged;
    }

    public boolean isEmailChanged() {
        return isEmailChanged;
    }

    public void setEmailChanged(boolean emailChanged) {
        isEmailChanged = emailChanged;
    }

    public boolean isTemplateChanged() {
        return isTemplateChanged;
    }

    public void setTemplateChanged(boolean templateChanged) {
        isTemplateChanged = templateChanged;
    }

    public Map getUsageLimit() {
        return usageLimit;
    }

    public void setUsageLimit(Map usageLimit) {
        this.usageLimit = usageLimit;
    }

    public Map getChannels() {
        return channels;
    }

    public void setChannels(Map channels) {
        this.channels = channels;
    }

    public boolean isRecipientsChanged() {
        return isRecipientsChanged;
    }

    public void setRecipientsChanged(boolean recipientsChanged) {
        isRecipientsChanged = recipientsChanged;
    }

    public boolean isResponseChanged() {
        return isResponseChanged;
    }

    public void setResponseChanged(boolean responseChanged) {
        isResponseChanged = responseChanged;
    }

    public boolean isCustomThemeChanged() {return isCustomThemeChanged;}

    public void setCustomThemeChanged(boolean customThemeChanged) {isCustomThemeChanged = customThemeChanged;}

    public boolean isWhatsappChanged() {return isWhatsappChanged;}

    public void setWhatsappChanged(boolean whatsappChanged) {isWhatsappChanged = whatsappChanged;}

    public boolean isSSOChanged() {return isSSOChanged;}

    public void setSSOChanged(boolean SSOChanged) {isSSOChanged = SSOChanged;}

    public boolean isLtiChanged() {return isLtiChanged;}

    public void setLtiChanged(boolean ltiChanged) {isLtiChanged = ltiChanged;}
}
