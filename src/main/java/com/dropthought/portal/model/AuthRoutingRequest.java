package com.dropthought.portal.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
public class AuthRoutingRequest {

    @Builder.Default
    private AuthRoutingType authType = AuthRoutingType.PASSWORD;

    @Builder.Default
    private AuthIdPType idpType = AuthIdPType.OKTA;

    private String idpName;
}
