package com.dropthought.portal.model;

import com.dropthought.portal.common.DTConstants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
public class TextAnalyticsConfigBean  implements Serializable {

    @JsonProperty("textAnalyticsEnabled")
    private String textAnalyticsEnabled;

    @JsonProperty("approvalStatus")
    private String approvalStatus;

    @JsonProperty("approvedBy")
    private List<TextAnalyticsApprovedByBean> approvedBy;

    @JsonProperty("approvers")
    private List approvers;

    @JsonProperty("textAnalyticsPlan")
    private String textAnalyticsPlan;

    @JsonProperty("configs")
    private String configs;

    @JsonProperty("configUniqueId")
    private String configUniqueId;

    @JsonProperty("configId")
    private Integer configId;

    @JsonProperty("createdBy")
    private String createdBy;

   @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessUniqueId")
    @JsonProperty("businessUniqueId")
    private  String buinessUniqueId;

    public String getTextAnalyticsEnabled() {
        return textAnalyticsEnabled;
    }

    public void setTextAnalyticsEnabled(String textAnalyticsEnabled) {
        this.textAnalyticsEnabled = textAnalyticsEnabled;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public List<TextAnalyticsApprovedByBean> getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(List<TextAnalyticsApprovedByBean> approvedBy) {
        this.approvedBy = approvedBy;
    }

    public List getApprovers() {
        return approvers;
    }

    public void setApprovers(List approvers) {
        this.approvers = approvers;
    }

    public String getTextAnalyticsPlan() {
        return textAnalyticsPlan;
    }

    public void setTextAnalyticsPlan(String textAnalyticsPlan) {
        this.textAnalyticsPlan = textAnalyticsPlan;
    }

    public String getConfigs() {
        return configs;
    }

    public void setConfigs(String configs) {
        this.configs = configs;
    }

    public String getConfigUniqueId() {
        return configUniqueId;
    }

    public void setConfigUniqueId(String configUniqueId) {
        this.configUniqueId = configUniqueId;
    }

    public Integer getConfigId() {
        return configId;
    }

    public void setConfigId(Integer configId) {
        this.configId = configId;
    }

    public String getBuinessUniqueId() {
        return buinessUniqueId;
    }

    public void setBuinessUniqueId(String buinessUniqueId) {
        this.buinessUniqueId = buinessUniqueId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
