package com.dropthought.portal.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Prabhakar Thakur on 12/7/2020.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserAuthenticationBean implements Serializable {

    @JsonProperty("verify_email")
    private String verifyEmail;

    @JsonProperty("verify_uid")
    private Integer verifyUid;

    @JsonProperty("verify_name")
    private String verifyName;

    @JsonProperty("confirm_token")
    private String confirmToken;

    @JsonProperty("host")
    private String host;

    @JsonProperty(value = "userName", required = false)
    private String userName;

    @JsonProperty(value = "password", required = false)
    private String password;

//    @JsonProperty("language")
//    private String language;


    public String getVerifyEmail() {
        return verifyEmail;
    }

    public void setVerifyEmail(String verifyEmail) {
        this.verifyEmail = verifyEmail;
    }

    public Integer getVerifyUid() {
        return verifyUid;
    }

    public void setVerifyUid(Integer verifyUid) {
        this.verifyUid = verifyUid;
    }

    public String getVerifyName() {
        return verifyName;
    }

    public void setVerifyName(String verifyName) {
        this.verifyName = verifyName;
    }

    public String getConfirmToken() {
        return confirmToken;
    }

    public void setConfirmToken(String confirmToken) {
        this.confirmToken = confirmToken;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
