package com.dropthought.portal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class DTPageBean {

    @JsonProperty("pageId")
    private String pageId;

    @JsonProperty("surveyId")
    private String surveyId;

    @JsonProperty("pageTitle")
    private String pageTitle;

    @JsonProperty("questions")
    private List<DTQuestionBean> questions;

    @JsonProperty("mode")
    private String mode;

    @JsonProperty("createdBy")
    private Integer createdBy;

    @JsonProperty("modifiedBy")
    private Integer modifiedBy;

    @JsonProperty("cc")
    private String currentDB;

    @JsonProperty("questionsOrder")
    private List questionsOrder;

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public List<DTQuestionBean> getQuestions() {
        return questions;
    }

    public void setQuestions(List<DTQuestionBean> questions) {
        this.questions = questions;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getCurrentDB() {
        return currentDB;
    }

    public void setCurrentDB(String currentDB) {
        this.currentDB = currentDB;
    }

    public List getQuestionsOrder() { return questionsOrder; }

    public void setQuestionsOrder(List questionsOrder) { this.questionsOrder = questionsOrder; }

}
