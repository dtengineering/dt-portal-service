package com.dropthought.portal.model;

import com.dropthought.portal.util.Constants;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CustomizeQRCodeBean implements Serializable {


    @JsonProperty(value = "foreColor", defaultValue = Constants.COLOR_BLACK)
    private String foreColor;

    @JsonProperty(value = "backColor", defaultValue = Constants.COLOR_WHITE)
    private String backColor;

    @JsonProperty("logoUrl")
    private String logoUrl;

    @JsonProperty("logoBase64")
    private String logoBase64;

    @JsonProperty("surveyUniqueId")
    private String surveyUniqueId;

    public String getForeColor() {
        return foreColor;
    }

    public void setForeColor(String foreColor) {
        this.foreColor = foreColor;
    }

    public String getBackColor() {
        return backColor;
    }

    public void setBackColor(String backColor) {
        this.backColor = backColor;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getLogoBase64() {
        return logoBase64;
    }

    public void setLogoBase64(String logoBase64) {
        this.logoBase64 = logoBase64;
    }

    public String getSurveyUniqueId() {
        return surveyUniqueId;
    }

    public void setSurveyUniqueId(String surveyUniqueId) {
        this.surveyUniqueId = surveyUniqueId;
    }
}
