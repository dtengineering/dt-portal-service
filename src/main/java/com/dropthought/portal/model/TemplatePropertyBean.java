package com.dropthought.portal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TemplatePropertyBean {

    @JsonProperty("image")
    private String image;

    @JsonProperty("hexCode")
    private String hexCode;

    public String getHexCode() {
        return hexCode;
    }

    public void setHexCode(String hexCode) {
        this.hexCode = hexCode;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
