package com.dropthought.portal.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientBean implements Serializable {

    @JsonProperty("businessId")
    private Integer businessId;

    @JsonProperty("businessUUID")
    private String businessUUID;

    @JsonProperty("businessTypeId")
    private Integer businessTypeId;

    @JsonProperty("noOfEmployees")
    private String empRangeId;

    @JsonProperty("signupTypeId")
    private Integer signupTypeId;

    @JsonProperty("roleId")
    private Integer roleId;

    @JsonProperty("country")
    private String countryId;

    @JsonProperty("fullName")
    private String fullName;

    @JsonProperty("firstName")
    private String firstName;

    @JsonProperty("lastName")
    private String lastName;

    @JsonProperty("username")
    private String userName;

    @JsonProperty("email")
    private String userEmail;

    @JsonProperty("password")
    private String password;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("trialFlag")
    private Integer trialFlag;

    @JsonProperty("confirmationStatus")
    private Integer userStatus;

    @JsonProperty("createdBy")
    private Integer createdBy;

    @JsonProperty("modifiedBy")
    private Integer modifiedBy;

    @JsonProperty("completeSocialSignup")
    private Integer completeSocialSignup;

    @JsonProperty("userId")
    private Integer userId;

    @JsonProperty("businessName")
    private String businessName;

    @JsonProperty("businessCode")
    private String businessCode;

    @JsonProperty("businessTimezone")
    private String timezone;

    @JsonProperty("businessAddress")
    private String address;

    @JsonProperty("welcomeEmail")
    private Boolean welcomeEmail;

    @JsonProperty("cc")
    private String currentDB;

    @JsonProperty("host")
    private String host;

    @JsonProperty("language")
    private String language;

    @JsonProperty(value = "industry")
    List<Integer> industryIds;

    @JsonProperty(value = "contractStartDate", defaultValue = "")
    private String contractStartDate;

    @JsonProperty(value = "contractEndDate", defaultValue = "")
    private String contractEndDate;

    @JsonProperty(value = "reason", defaultValue = "")
    private String reason;

    @JsonProperty(value = "state", defaultValue = "1")
    private Integer state;

    @JsonProperty(value = "csAccess", defaultValue = "0")
    private Integer csAccess;

    @JsonProperty(value = "title", defaultValue = "")
    private String title;

    @JsonProperty(value = "businessLogo")
    private String businessLogo;

    @JsonProperty(value = "advancedAnalysisUrl")
    private String advancedAnalysisUrl;

    @JsonProperty(value = "hippa")
    private String hippa;


    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getCurrentDB() {
        return currentDB;
    }

    public void setCurrentDB(String currentDB) {
        this.currentDB = currentDB;
    }

    public Boolean getWelcomeEmail() {
        return welcomeEmail;
    }

    public void setWelcomeEmail(Boolean welcomeEmail) {
        this.welcomeEmail = welcomeEmail;
    }

    public Integer getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }

    public String getBusinessUUID() { return businessUUID; }

    public void setBusinessUUID(String businessUUID) { this.businessUUID = businessUUID; }

    public Integer getBusinessTypeId() {
        return businessTypeId;
    }

    public void setBusinessTypeId(Integer businessTypeId) {
        this.businessTypeId = businessTypeId;
    }

    public Integer getSignupTypeId() {
        return signupTypeId;
    }

    public void setSignupTypeId(Integer signupTypeId) {
        this.signupTypeId = signupTypeId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getTrialFlag() {
        return trialFlag;
    }

    public void setTrialFlag(Integer trialFlag) {
        this.trialFlag = trialFlag;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }


    public Integer getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }


    public Integer getCompleteSocialSignup() {
        return completeSocialSignup;
    }

    public void setCompleteSocialSignup(Integer completeSocialSignup) {
        this.completeSocialSignup = completeSocialSignup;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getEmpRangeId() {
        return empRangeId;
    }

    public void setEmpRangeId(String empRangeId) {
        this.empRangeId = empRangeId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getBusinessCode() {
        return businessCode;
    }

    public void setBusinessCode(String businessCode) {
        this.businessCode = businessCode;
    }

    public String getLanguage() { return language; }

    public void setLanguage(String language) { this.language = language; }

    public List<Integer> getIndustryIds() {
        return industryIds;
    }

    public void setIndustryIds(List<Integer> industryIds) {
        this.industryIds = industryIds;
    }

    public String getContractStartDate() {
        return contractStartDate;
    }

    public void setContractStartDate(String contractStartDate) {
        this.contractStartDate = contractStartDate;
    }

    public String getContractEndDate() {
        return contractEndDate;
    }

    public void setContractEndDate(String contractEndDate) {
        this.contractEndDate = contractEndDate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getCsAccess() {
        return csAccess;
    }

    public void setCsAccess(Integer csAccess) {
        this.csAccess = csAccess;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBusinessLogo() {
        return businessLogo;
    }

    public void setBusinessLogo(String businessLogo) {
        this.businessLogo = businessLogo;
    }

    public String getAdvancedAnalysisUrl() {
        return advancedAnalysisUrl;
    }

    public void setAdvancedAnalysisUrl(String advancedAnalysisUrl) {
        this.advancedAnalysisUrl = advancedAnalysisUrl;
    }

    public String getHippa() {
        return hippa;
    }

    public void setHippa(String hippa) {
        this.hippa = hippa;
    }
}
