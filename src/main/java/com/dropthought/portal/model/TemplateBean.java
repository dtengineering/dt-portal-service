package com.dropthought.portal.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TemplateBean {

    @JsonProperty("templateId")
    private String templateId;

    @JsonProperty("templateUUID")
    private String templateUUID;

    @JsonProperty("templateName")
    private String templateName;

    @JsonProperty("createdBy")
    private String createdBy;

    @JsonProperty("modifiedBy")
    private String modifiedBy;

    @JsonProperty("timezone")
    private String timezone;

    @JsonProperty("welcomeText")
    private String welcomeText;

    @JsonProperty("thankYouText")
    private String thankYouText;

    @JsonProperty("businessId")
    private String businessUUID;

    @JsonProperty("pages")
    private List<DTPageBean> page;

    @JsonProperty("anonymous")
    private boolean anonymous;

    @JsonProperty("cc")
    private String currentDB;

    @JsonProperty("templateProperty")
    private TemplatePropertyBean templatePropertyBean;

    @JsonProperty("message")
    private List message;

    @JsonProperty("developerMessage")
    private List developerMessage;

    @JsonProperty("status")
    private String status;

    @JsonProperty("state")
    private String state;

    @JsonProperty("active")
    private String active;

    @JsonProperty("rules")
    private Map rules;

    @JsonProperty("pageOrder")
    private List pageOrder;

    @JsonProperty("language")
    private String language;

    @JsonProperty("languages")
    private List languages;

    @JsonProperty("usedCount")
    private Integer usedCount;

    @JsonProperty("averageCompletionTime")
    private String averageCompletionTime;

    @JsonProperty("questionsCount")
    private int questionsCount;

    @JsonProperty("theme")
    private List theme;

    @JsonProperty("industry")
    private List industry;

    @JsonProperty("welcomeLink")
    private String welcomeLink;

    @JsonProperty("welcomeLinkName")
    private String welcomeLinkName;

    @JsonProperty("surveyDescription")
    private String surveyDescription;

    public List getDeveloperMessage() {
        return developerMessage;
    }

    public void setDeveloperMessage(List developerMessage) {
        this.developerMessage = developerMessage;
    }

    public List getMessage() {
        return message;
    }

    public void setMessage(List message) {
        this.message = message;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getWelcomeText() {
        return welcomeText;
    }

    public void setWelcomeText(String welcomeText) {
        this.welcomeText = welcomeText;
    }

    public String getThankYouText() {
        return thankYouText;
    }

    public void setThankYouText(String thankYouText) {
        this.thankYouText = thankYouText;
    }

    public String getBusinessUUID() {
        return businessUUID;
    }

    public void setBusinessUUID(String businessUUID) {
        this.businessUUID = businessUUID;
    }

    public List<DTPageBean> getPage() {
        return page;
    }

    public void setPage(List<DTPageBean> page) {
        this.page = page;
    }

    public boolean getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(boolean anonymous) {
        this.anonymous = anonymous;
    }

    public String getCurrentDB() {
        return currentDB;
    }

    public void setCurrentDB(String currentDB) {
        this.currentDB = currentDB;
    }

    public boolean isAnonymous() {
        return anonymous;
    }

    public TemplatePropertyBean getTemplateProperty() {
        return templatePropertyBean;
    }

    public void setTemplateProperty(TemplatePropertyBean templateProperty) {
        this.templatePropertyBean = templateProperty;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) { this.state = state; }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Map getRules() {
        return rules;
    }

    public void setRules(Map rules) {
        this.rules = rules;
    }

    public List getPageOrder() {
        return pageOrder;
    }

    public void setPageOrder(List pageOrder) {
        this.pageOrder = pageOrder;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public List getLanguages() {
        return languages;
    }

    public void setLanguages(List languages) {
        this.languages = languages;
    }

    public String getTemplateUUID() {
        return templateUUID;
    }

    public void setTemplateUUID(String templateUUID) {
        this.templateUUID = templateUUID;
    }

    public Integer getUsedCount() {
        return usedCount;
    }

    public void setUsedCount(Integer usedCount) {
        this.usedCount = usedCount;
    }

    public String getAverageCompletionTime() { return averageCompletionTime; }

    public void setAverageCompletionTime(String averageCompletionTime) { this.averageCompletionTime = averageCompletionTime; }

    public int getQuestionsCount() { return questionsCount; }

    public void setQuestionsCount(int questionsCount) { this.questionsCount = questionsCount; }

    public List getTheme() {
        return theme;
    }

    public void setTheme(List themes) {
        this.theme = themes;
    }

    public List getIndustry() {
        return industry;
    }

    public void setIndustry(List industry) {
        this.industry = industry;
    }

    public String getWelcomeLink() {
        return welcomeLink;
    }

    public void setWelcomeLink(String welcomeLink) {
        this.welcomeLink = welcomeLink;
    }

    public String getWelcomeLinkName() {
        return welcomeLinkName;
    }

    public void setWelcomeLinkName(String welcomeLinkName) {
        this.welcomeLinkName = welcomeLinkName;
    }

    public String getSurveyDescription() { return surveyDescription; }

    public void setSurveyDescription(String surveyDescription) { this.surveyDescription = surveyDescription; }

}
