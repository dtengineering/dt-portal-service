package com.dropthought.portal.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)

public class BusinessSchedule implements Serializable {
    @Positive(message = "businessId should be valid number")
    @JsonProperty("businessId")
    private int businessId;

    @NotBlank(message = "startDate should not be null or empty")
    @JsonProperty("startDate")
    private String startDate;

    @NotBlank(message = "endDate should not be null or empty")
    @JsonProperty("endDate")
    private String endDate;

    @NotBlank(message = "state should not be null or empty")
    @JsonProperty("state")
    private String state;
     @JsonProperty("timezone")
     private String timezone;

    public int getBusinessId() {
        return businessId;
    }

    public void setBusinessId(int businessId) {
        this.businessId = businessId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }
}
