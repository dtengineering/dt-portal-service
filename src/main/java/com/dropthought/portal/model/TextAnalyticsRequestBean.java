package com.dropthought.portal.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TextAnalyticsRequestBean implements Serializable {

    @NotBlank(message = "BusinessUniqueId can not be null or empty")
    @JsonProperty("businessId")
    private String businessId;

    @JsonProperty("to")
    private List to;

    @JsonProperty("textAnalytics")
    private String textAnalytics;

    @JsonProperty("textAnalyticsPlanId")
    private Integer textAnalyticsPlanId;

    @JsonProperty("subject")
    private String subject;

    @JsonProperty("body")
    private String body;

    @JsonProperty("createdBy")
    private String createdBy;

    @JsonProperty("planConfigs")
    private Map planConfigs;

    @JsonProperty("resend")
    private Integer resend;

    public String getBusinessId() { return businessId; }

    public void setBusinessId(String businessId) { this.businessId = businessId; }

    public List getTo() { return to; }

    public void setTo(List to) { this.to = to; }

    public Integer getTextAnalyticsPlanId() { return textAnalyticsPlanId; }

    public void setTextAnalyticsPlanId(Integer textAnalyticsPlanId) { this.textAnalyticsPlanId = textAnalyticsPlanId; }

    public String getSubject() { return subject; }

    public void setSubject(String subject) { this.subject = subject; }

    public String getBody() { return body; }

    public void setBody(String body) { this.body = body; }

    public String getCreatedBy() { return createdBy; }

    public void setCreatedBy(String createdBy) { this.createdBy = createdBy; }

    public Map getPlanConfigs() { return planConfigs; }

    public void setPlanConfigs(Map planConfigs) { this.planConfigs = planConfigs; }

    public String getTextAnalytics() { return textAnalytics; }

    public void setTextAnalytics(String textAnalytics) { this.textAnalytics = textAnalytics; }

    public Integer getResend() { return resend; }

    public void setResend(Integer resend) { this.resend = resend; }
}
