package com.dropthought.portal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class PlanProperties implements Serializable {

    @JsonProperty("Title")
    private String title;

    @JsonProperty("Description")
    private String description;

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
