package com.dropthought.portal.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomThemeBean {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("themeName")
    private String themeName;

    @JsonProperty("businessId")
    private List businessId;

    @JsonProperty("themeUUID")
    private String themeUUID;
    @JsonProperty("pageStyleCode")
    private String pageStyleCode;

    @JsonProperty("pageImageUrl")
    private String pageImageUrl;

    @JsonProperty("buttonsStyleCode")
    private String buttonsStyleCode;

    @JsonProperty("progressBarStyleCode")
    private String progressBarStyleCode;

    @JsonProperty("smileyRatingStyleCode")
    private String smileyRatingStyleCode;

    @JsonProperty("smileyRatingImageUrl")
    private Map smileyRatingImageUrl;

    @JsonProperty("starRatingStyleCode")
    private String starRatingStyleCode;

    @JsonProperty("starRatingImageUrl")
    private Map starRatingImageUrl;

    @JsonProperty("thumbRatingStyleCode")
    private String thumbRatingStyleCode;

    @JsonProperty("thumbRatingImageUrl")
    private Map thumbRatingImageUrl;

    @JsonProperty("sliderRatingStyleCode")
    private String sliderRatingStyleCode;

    @JsonProperty("sliderRatingImageUrl")
    private String sliderRatingImageUrl;

    @JsonProperty("scaleRatingStyleCode")
    private String scaleRatingStyleCode;

    @JsonProperty("scaleRatingImageUrl")
    private Map scaleRatingImageUrl;

    @JsonProperty("multipleResponseStyleCode")
    private String multipleResponseStyleCode;

    @JsonProperty("multipleResponseImageUrl")
    private Map multipleResponseImageUrl;

    @JsonProperty("singleResponseStyleCode")
    private String singleResponseStyleCode;

    @JsonProperty("singleResponseImageUrl")
    private Map singleResponseImageUrl;

    @JsonProperty("dropdownStyleCode")
    private String dropdownStyleCode;

    @JsonProperty("openEndedStyleCode")
    private String openEndedStyleCode;

    @JsonProperty("npsStyleCode")
    private String npsStyleCode;

    @JsonProperty("npsImageUrl")
    private Map npsImageUrl;

    @JsonProperty("rankingStyleCode")
    private String rankingStyleCode;

    @JsonProperty("rankingImageUrl")
    private String rankingImageUrl;

    @JsonProperty("matrixRatingStyleCode")
    private String matrixRatingStyleCode;

    @JsonProperty("matrixRatingImageUrl")
    private Map matrixRatingImageUrl;

    @JsonProperty("createdTime")
    private String createdTime;

    public String getThemeName() {
        return themeName;
    }

    public void setThemeName(String themeName) {
        this.themeName = themeName;
    }

    public List getBusinessId() {
        return businessId;
    }

    public void setBusinessId(List businessId) {
        this.businessId = businessId;
    }

    public String getThemeUUID() {
        return themeUUID;
    }

    public void setThemeUUID(String themeUUID) {
        this.themeUUID = themeUUID;
    }

    public String getPageStyleCode() {
        return pageStyleCode;
    }

    public void setPageStyleCode(String pageStyleCode) {
        this.pageStyleCode = pageStyleCode;
    }

    public String getPageImageUrl() {
        return pageImageUrl;
    }

    public void setPageImageUrl(String pageImageUrl) {
        this.pageImageUrl = pageImageUrl;
    }

    public String getButtonsStyleCode() {
        return buttonsStyleCode;
    }

    public void setButtonsStyleCode(String buttonsStyleCode) {
        this.buttonsStyleCode = buttonsStyleCode;
    }

    public String getProgressBarStyleCode() {
        return progressBarStyleCode;
    }

    public void setProgressBarStyleCode(String progressBarStyleCode) {
        this.progressBarStyleCode = progressBarStyleCode;
    }

    public String getSmileyRatingStyleCode() {
        return smileyRatingStyleCode;
    }

    public void setSmileyRatingStyleCode(String smileyRatingStyleCode) {
        this.smileyRatingStyleCode = smileyRatingStyleCode;
    }

    public Map getSmileyRatingImageUrl() {
        return smileyRatingImageUrl;
    }

    public void setSmileyRatingImageUrl(Map smileyRatingImageUrl) {
        this.smileyRatingImageUrl = smileyRatingImageUrl;
    }

    public String getStarRatingStyleCode() {
        return starRatingStyleCode;
    }

    public void setStarRatingStyleCode(String starRatingStyleCode) {
        this.starRatingStyleCode = starRatingStyleCode;
    }

    public Map getStarRatingImageUrl() {
        return starRatingImageUrl;
    }

    public void setStarRatingImageUrl(Map starRatingImageUrl) {
        this.starRatingImageUrl = starRatingImageUrl;
    }

    public String getThumbRatingStyleCode() {
        return thumbRatingStyleCode;
    }

    public void setThumbRatingStyleCode(String thumbRatingStyleCode) {
        this.thumbRatingStyleCode = thumbRatingStyleCode;
    }

    public Map getThumbRatingImageUrl() {
        return thumbRatingImageUrl;
    }

    public void setThumbRatingImageUrl(Map thumbRatingImageUrl) {
        this.thumbRatingImageUrl = thumbRatingImageUrl;
    }

    public String getSliderRatingStyleCode() {
        return sliderRatingStyleCode;
    }

    public void setSliderRatingStyleCode(String sliderRatingStyleCode) {
        this.sliderRatingStyleCode = sliderRatingStyleCode;
    }

    public String getSliderRatingImageUrl() {
        return sliderRatingImageUrl;
    }

    public void setSliderRatingImageUrl(String sliderRatingImageUrl) {
        this.sliderRatingImageUrl = sliderRatingImageUrl;
    }

    public String getScaleRatingStyleCode() {
        return scaleRatingStyleCode;
    }

    public void setScaleRatingStyleCode(String scaleRatingStyleCode) {
        this.scaleRatingStyleCode = scaleRatingStyleCode;
    }

    public Map getScaleRatingImageUrl() {
        return scaleRatingImageUrl;
    }

    public void setScaleRatingImageUrl(Map scaleRatingImageUrl) {
        this.scaleRatingImageUrl = scaleRatingImageUrl;
    }

    public String getMultipleResponseStyleCode() {
        return multipleResponseStyleCode;
    }

    public void setMultipleResponseStyleCode(String multipleResponseStyleCode) {
        this.multipleResponseStyleCode = multipleResponseStyleCode;
    }

    public Map getMultipleResponseImageUrl() {
        return multipleResponseImageUrl;
    }

    public void setMultipleResponseImageUrl(Map multipleResponseImageUrl) {
        this.multipleResponseImageUrl = multipleResponseImageUrl;
    }

    public String getSingleResponseStyleCode() {
        return singleResponseStyleCode;
    }

    public void setSingleResponseStyleCode(String singleResponseStyleCode) {
        this.singleResponseStyleCode = singleResponseStyleCode;
    }

    public Map getSingleResponseImageUrl() {
        return singleResponseImageUrl;
    }

    public void setSingleResponseImageUrl(Map singleResponseImageUrl) {
        this.singleResponseImageUrl = singleResponseImageUrl;
    }

    public String getDropdownStyleCode() {
        return dropdownStyleCode;
    }

    public void setDropdownStyleCode(String dropdownStyleCode) {
        this.dropdownStyleCode = dropdownStyleCode;
    }

    public String getOpenEndedStyleCode() {
        return openEndedStyleCode;
    }

    public void setOpenEndedStyleCode(String openEndedStyleCode) {
        this.openEndedStyleCode = openEndedStyleCode;
    }

    public String getNpsStyleCode() {
        return npsStyleCode;
    }

    public void setNpsStyleCode(String npsStyleCode) {
        this.npsStyleCode = npsStyleCode;
    }

    public Map getNpsImageUrl() {
        return npsImageUrl;
    }

    public void setNpsImageUrl(Map npsImageUrl) {
        this.npsImageUrl = npsImageUrl;
    }

    public String getRankingStyleCode() {
        return rankingStyleCode;
    }

    public void setRankingStyleCode(String rankingStyleCode) {
        this.rankingStyleCode = rankingStyleCode;
    }

    public String getRankingImageUrl() {
        return rankingImageUrl;
    }

    public void setRankingImageUrl(String rankingImageUrl) {
        this.rankingImageUrl = rankingImageUrl;
    }

    public String getMatrixRatingStyleCode() {
        return matrixRatingStyleCode;
    }

    public void setMatrixRatingStyleCode(String matrixRatingStyleCode) {
        this.matrixRatingStyleCode = matrixRatingStyleCode;
    }

    public Map getMatrixRatingImageUrl() {
        return matrixRatingImageUrl;
    }

    public void setMatrixRatingImageUrl(Map matrixRatingImageUrl) {
        this.matrixRatingImageUrl = matrixRatingImageUrl;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
