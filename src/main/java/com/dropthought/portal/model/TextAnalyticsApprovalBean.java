package com.dropthought.portal.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TextAnalyticsApprovalBean implements Serializable {
    @NotBlank(message = "id can not be null or empty")
    @JsonProperty("id")
    private String id;
    @NotBlank(message = "businessId can not be null or empty")
    @JsonProperty("businessId")
    private String businessId;
    @NotBlank(message = "approvedBy can not be null or empty")
    @JsonProperty("approvedBy")
    private String approvedBy;
    @Min(value = 0, message = "approval should be number")
    @JsonProperty("approval")
    private Integer approval;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public Integer getApproval() {
        return approval;
    }

    public void setApproval(Integer approval) {
        this.approval = approval;
    }
}
