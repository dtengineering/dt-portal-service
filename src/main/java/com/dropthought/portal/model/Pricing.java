package com.dropthought.portal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Pricing implements Serializable {

    @JsonProperty("price")
    private int price;

    @JsonProperty("currencyCode")
    private String currencyCode;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
