package com.dropthought.portal.model;

import com.dropthought.portal.common.DTConstants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * Client portal access object
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientPortalAccessBean implements Serializable {

    @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessUUID")
    @JsonProperty("businessUUID")
    private String businessUUID;

    @JsonProperty("fromDate")
    private String fromDate;

    @JsonProperty("toDate")
    private String toDate;

    @JsonProperty("reason")
    private String reason;

    @JsonProperty("timezone")
    private String timeZone;

    @JsonProperty("csAccessFlag")
    private Integer csAccessFlag;

    @JsonProperty("userUUID")
    private String userUUID;

    public String getUserUUID() {
        return userUUID;
    }
    public void setUserUUID(String userUUID) {
        this.userUUID = userUUID;
    }

    public String getFromDate() { return fromDate; }

    public void setFromDate(String fromDate) { this.fromDate = fromDate; }

    public String getToDate() { return toDate; }

    public void setToDate(String toDate) { this.toDate = toDate; }

    public String getReason() { return reason; }

    public void setReason(String reason) { this.reason = reason; }

    public String getBusinessUUID() { return businessUUID; }

    public void setBusinessUUID(String businessUUID) { this.businessUUID = businessUUID; }

    public String getTimeZone() { return timeZone; }

    public void setTimeZone(String timeZone) { this.timeZone = timeZone; }

    public Integer getCsAccessFlag() { return csAccessFlag; }

    public void setCsAccessFlag(Integer csAccessFlag) { this.csAccessFlag = csAccessFlag; }
}
