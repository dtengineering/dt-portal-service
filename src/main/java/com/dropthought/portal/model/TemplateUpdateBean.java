package com.dropthought.portal.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TemplateUpdateBean {

    @JsonProperty("templates")
    private List templates;

    public List getTemplate() { return templates; }

    public void setTemplate(List templates) { this.templates = templates; }


}
