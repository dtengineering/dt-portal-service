package com.dropthought.portal.model;

public enum AuthIdPType {
    OKTA, GOOGLE, MICROSOFT, APPLE, CUSTOM
}
