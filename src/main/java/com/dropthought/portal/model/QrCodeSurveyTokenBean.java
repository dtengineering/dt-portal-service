package com.dropthought.portal.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QrCodeSurveyTokenBean implements Serializable {

    @JsonProperty("surveyQrCode")
    private byte[] surveyQrCode;

    @JsonProperty("qrCodeSurveyId")
    private Integer qrCodeSurveyId;

    @JsonProperty("staticTokenId")
    private String staticTokenId;

    @JsonProperty("qrFileType")
    private String qrFileType;

    @JsonProperty("downloadCount")
    private String downloadCount;

    @JsonProperty("createdTime")
    private Timestamp createdTime;

    @JsonProperty("createdBy")
    private Integer createdBy;

    @JsonProperty("metadataEnabled")
    private Integer metadataEnabled;

    @JsonProperty("meta_data")
    private Map metadata;

    @JsonProperty("linkGroupId")
    private String linkGroupId;

    @JsonProperty("tagLinkId")
    private String tagLinkId;

    @JsonProperty("metadataType")
    private Map metadataType;

    @JsonProperty("linkId")
    private String linkId;

    @JsonProperty("shortUrl")
    private String shortUrl;

/*    @JsonProperty("longUrl")
    private String longUrl;*/

    public byte[] getSurveyQrCode() { return surveyQrCode; }

    public void setSurveyQrCode(byte[] surveyQrCode) { this.surveyQrCode = surveyQrCode; }

    public Integer getQrCodeSurveyId() { return qrCodeSurveyId; }

    public void setQrCodeSurveyId(Integer qrCodeSurveyId) { this.qrCodeSurveyId = qrCodeSurveyId; }

    public String getStaticTokenId() { return staticTokenId; }

    public void setStaticTokenId(String staticTokenId) { this.staticTokenId = staticTokenId; }

    public String getQrFileType() { return qrFileType; }

    public void setQrFileType(String qrFileType) { this.qrFileType = qrFileType; }

    public String getDownloadCount() { return downloadCount; }

    public void setDownloadCount(String downloadCount) { this.downloadCount = downloadCount; }

    public Timestamp getCreatedTime() { return createdTime; }

    public void setCreatedTime(Timestamp createdTime) { this.createdTime = createdTime; }

    public Integer getCreatedBy() { return createdBy; }

    public void setCreatedBy(Integer createdBy) { this.createdBy = createdBy; }

    public Integer getMetadataEnabled() { return metadataEnabled; }

    public void setMetadataEnabled(Integer metadataEnabled) { this.metadataEnabled = metadataEnabled; }

    public Map getMetadata() { return metadata; }

    public void setMetadata(Map metadata) { this.metadata = metadata; }

    public String getLinkGroupId() { return linkGroupId; }

    public void setLinkGroupId(String linkGroupId) { this.linkGroupId = linkGroupId; }

    public String getLinkId() { return linkId; }

    public void setLinkId(String linkId) { this.linkId = linkId; }

    public String getShortUrl() { return shortUrl; }

    public void setShortUrl(String shortUrl) { this.shortUrl = shortUrl; }

    public String getTagLinkId() { return tagLinkId; }

    public void setTagLinkId(String tagLinkId) { this.tagLinkId = tagLinkId; }

    public Map getMetadataType() {
        return metadataType;
    }

    public void setMetadataType(Map metadataType) {
        this.metadataType = metadataType;
    }

    /*public String getLongUrl() {
        return longUrl;
    }

    public void setLongUrl(String longUrl) {
        this.longUrl = longUrl;
    }*/
}
