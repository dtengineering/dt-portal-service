package com.dropthought.portal.runner;

import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;

@Profile("runner")
@Configuration
@EnableScheduling
public class ScheduleRunnerConfiguration {


    @Value("${schedule.topic}")
    private String topicName;

    @Bean
    public TopicExchange senderTopicExchange() {
        //TopicExchange topicExchange = new TopicExchange(topicName);
        //return topicExchange;
        return new TopicExchange(topicName);
    }

    @Bean
    public ScheduleRunner eventPublisher(RabbitTemplate rabbitTemplate, TopicExchange senderTopicExchange) {
        return new ScheduleRunner(rabbitTemplate, senderTopicExchange);
    }
}
