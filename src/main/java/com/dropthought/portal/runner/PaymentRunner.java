package com.dropthought.portal.runner;

import com.dropthought.portal.model.Pricing;
import com.dropthought.portal.service.PaymentService;
import com.dropthought.portal.service.PlanService;
import com.dropthought.portal.util.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stripe.model.PaymentMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Profile("paymentRunner")
@EnableScheduling
@Configuration
public class PaymentRunner {

    private Logger logger = LoggerFactory.getLogger(PaymentRunner.class);

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private PlanService planService;

    @Value("${dtp.domain}")
    private String dtpDomain;

    /**
     * chron to run once in a month to add the upcoming payment details for the business
     * in balance_{businessuuid} table
     */
    //@Scheduled(cron = "0 0 0 1 * *")
    //@Scheduled(cron = "0 */1 * * * *") //for testing purpose
    /*public void generateBill() {
        paymentService.generatePayment();
    }*/

    //cron to every 6 hours to check the business_payment_renewal table and create the payment using customerId and paymentMethodId
    //@Scheduled(cron = "0 0 */6 * * *")
    @Scheduled(cron = "0 */1 * * * *") //for testing purpose
    public void createPayment() {

        //DTV-13784 - Exclude the Job when the dtpDomain is not stage / prod
        if (dtpDomain.contains("test") || dtpDomain.contains("automation") || dtpDomain.contains("sandbox") ) {
            logger.info("Skipping the Payment Job for the domain: {}", dtpDomain);
            return;
        }
        this.identifyBusinessAndCreatePayment();
    }

    //Method to create payment
    public void identifyBusinessAndCreatePayment() {

        String currencyCode = "";
        long amount = 0;
        try {
            int planId = 3; //DT-lite plan Id
            Map<String, Object> planConfigMap = planService.getPlanDetailsById(planId);
            String pricingStr = Utils.isNotNull(planConfigMap.get("pricing")) && planConfigMap.get("pricing") != null
                    ? planConfigMap.get("pricing").toString() : "";
            if (pricingStr != null && !pricingStr.isEmpty() && !pricingStr.equals("0")) {
                ObjectMapper mapper = new ObjectMapper();
                Pricing pricing = null;
                try {
                    pricing = mapper.readValue(pricingStr, Pricing.class);
                } catch (Exception e) {
                    logger.error("Error in parsing pricing json string");
                }

                currencyCode = pricing.getCurrencyCode();
                amount = pricing.getPrice();
                // As of now we are billing Annually. So, we need to convert the price for yearly billing
                amount = amount * 12;
            }

            //identify business from the business_renewal_schedule table which has new_start_time is less than current time
            // and renewal status is empty and payment status is "InComplete"
            // and create payment using customerId and paymentMethodId
            List<Map<String, Object>> businessList = paymentService.identifyBusinessToMakePayment();
            for (Map<String, Object> businessMap : businessList) {
                int business_id = (int) businessMap.get("business_id");
                String customerId = paymentService.returnCustomerIdByBusinessId(business_id);
                if (Utils.emptyString(customerId)) {
                    logger.error("Customer Id is empty for business_id: {}", business_id);
                    continue;
                }
                String subscriptionCurrencyCode = "USD"; //default currency code
                Map<String, Object> businessRenewalSchedule = paymentService.getBusinessRenewalSchedule(business_id);
                if (!businessRenewalSchedule.isEmpty()) {
                    subscriptionCurrencyCode = businessRenewalSchedule.get("currency").toString();
                }
                
                if (!subscriptionCurrencyCode.equalsIgnoreCase("USD")) {

                    // get currency value from currencies table
                    currencyCode = subscriptionCurrencyCode;
                    amount = paymentService.getCurrencyValue(subscriptionCurrencyCode);
                    //DTV-13675 given subscription amount is for a Month, so need to calculate the amount for the Year
                    amount = amount * 12;

                }
                // get the payment method of the customer
                List<PaymentMethod> paymentMethods = paymentService.getPaymentMethods(customerId);
                String paymentMethodId = paymentMethods.size() > 0 ? paymentMethods.get(0).getId() : "";
                if (Utils.notEmptyString(paymentMethodId)) {
                    String paymentStatus = paymentService.createPayment(customerId, paymentMethodId, amount, currencyCode);
                    if (paymentStatus.equalsIgnoreCase("Payment Successful")) {
                        //update the business_payment_renewal table with payment status as "Paid" in dtapp
                        logger.info("Payment Successful for business_id: {}", business_id);
                    }
                }
            }

        } catch (Exception e) {
            logger.error("Error while updating trial status. {}", e.getMessage());
        }
    }
}