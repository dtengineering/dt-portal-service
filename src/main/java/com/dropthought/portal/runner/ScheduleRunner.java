package com.dropthought.portal.runner;


import com.dropthought.portal.businessconfigurations.service.BusinessConfigurationsService;
import com.dropthought.portal.businessusage.BusinessUsageService;
import com.dropthought.portal.model.Pricing;
import com.dropthought.portal.service.*;
import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

//@Configuration
public class ScheduleRunner {

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();
    private final RabbitTemplate rabbitTemplate;
    private final TopicExchange topicExchange;

    @Autowired
    SchedulerService schedulerService;

    @Autowired
    private BusinessConfigurationsService businessConfigurationsService;

    @Autowired
    private PlanService planService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private TekionService tekionService;

    @Autowired
    private BusinessUsageService businessUsageService;

    @Autowired
    private MultiAccountService multiAccountService;

    private Logger logger = LoggerFactory.getLogger(ScheduleRunner.class);

    @Value("${schedule.routing.key}")
    private String routingKey;

    @Value("${dt.website.url}")
    private String dtWebsiteURL;

    @Value("${dtp.domain}")
    private String dtpDomain;

    @Autowired
    public ScheduleRunner(RabbitTemplate rabbitTemplate, TopicExchange topicExchange) {
        this.rabbitTemplate = rabbitTemplate;
        this.topicExchange = topicExchange;
    }

    @Scheduled(fixedDelayString = "${scheduler.fixedDelay.send}", initialDelayString = "${scheduler.intialDelay.send}")
    public void sendMessage() {
        //businessScheduler
        this.businessContractKickoff();
        this.businessSendLoginMail();
        //this.businessContractReminder();
        this.businessContractTermination();
        schedulerService.updateBusinessUsageMonthlyDuration();
        this.verifyAndUpdateTrialStatus();//DTV-12797 - To update the trial status
    }

    /**
     * chron to run once in a day to identify the partially expired business and send remainder emails
     */
    @Scheduled(cron = "0 0 0 * * *")
    //@Scheduled(cron = "0 */2 * * * *") //for testing purpose
    public void sentContractReminderEmails() {
        this.businessContractReminder();
    }

    /**
     * chron to run once in a day to identify the business and send renewal remainder emails
     */
   //@Scheduled(cron = "0 0 0 * * *")
   // @Scheduled(cron = "0 */1 * * * *") //for testing purpose
   /* public void sentPlanRenewalReminderEmails() {
        //this.verifyAndUpdateTrialStatus();
       //DTV-13784 - Exclude the Job when the dtpDomain is not stage / prod
       if (dtpDomain.contains("test") || dtpDomain.contains("automation") || dtpDomain.contains("sandbox") ) {
           return;
       }
        this.sentPlanRenewalReminder();
    }*/

    /**
     * method to verify and update the trial status based on the trial days in the plan configuration
     */
    private void verifyAndUpdateTrialStatus() {
        //DTV-13784 - Exclude the Job when the dtpDomain is not stage / prod
        if (dtpDomain.contains("test") || dtpDomain.contains("automation") || dtpDomain.contains("sandbox") ) {
            return;
        }
        // get business who are in trial period ie, trial_status = "1"
        try {
            List<Map<String, Object>> businessList = schedulerService.getBusinessListForTrialStatus();
            Iterator<Map<String, Object>> iterator = businessList.iterator();
            List<Integer> businessIds = new ArrayList<>();
            while (iterator.hasNext()) {
                try {
                    //loop through each business
                    Map<String, Object> eachBusiness = iterator.next();
                    int eachBusinessId = (int) eachBusiness.get(Constants.BUSINESS_ID);
                    String contractEndDate = eachBusiness.get(Constants.CONTRACT_END_DATE).toString();

                    // Remove the milliseconds part by finding the last occurrence of '.'
                    if (contractEndDate.contains(".")) {
                        contractEndDate = contractEndDate.substring(0, contractEndDate.lastIndexOf('.'));
                    }

                    //if contract end date is reached the current date, update the trial status to "0"
                   /* long diffMinutes = (int) Utils.differenceMinutes(contractEndDate, Utils.getCurrentUTCDateAsString());
                    if (diffMinutes < 0) {
                        businessIds.add(eachBusinessId);
                    }*/
                    // Define the date format
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

                    // Parse the dates
                    LocalDateTime endDate = LocalDateTime.parse(contractEndDate, formatter);
                    LocalDateTime currentDate = LocalDateTime.parse(Utils.getCurrentUTCDateAsString(), formatter);

                    // Check if end date is greater than or equal to current date
                    if (endDate.isEqual(currentDate) || endDate.isBefore(currentDate)) {
                        System.out.println("Contract end date is less than or equal to current date.");
                        businessIds.add(eachBusinessId);
                    } else {
                        System.out.println("Contract end date is less than current date.");
                    }
                } catch (Exception e) {
                    logger.error("Error while updating trial status. {}", e.getMessage());
                }
            }
            if (businessIds.size() > 0) {
                schedulerService.updateTrialStatus(businessIds);
            }

        } catch (Exception e) {
            logger.error("Error while updating trial status. {}", e.getMessage());
        }
    }

    /**
     * Method handles below cases
     * 1. To send reminder emails to business whose trial plan is going to expire 3 days
     * 2. To send reminder emails to business whose plan is going to expire in 7 days
     */
    private void sentPlanRenewalReminder() {

        logger.info("Plan Renewal Reminder - starts");
        List<Map<String, Object>> businessList = new ArrayList<>();
        Map<String, Object> planConfigMap = new HashMap<>();
        try {
            //DTV-12744 Subscription renewal reminder emails (Need to send 7 days before the expiry date)
            List businessListExpireInThreeDays = schedulerService.getBusinessListForPlanRenewal(3, "DAY");
            List businessListExpireInSevenDays = schedulerService.getBusinessListForSubscriptionRenewal(7, "DAY");
            businessList.addAll(businessListExpireInThreeDays);
            businessList.addAll(businessListExpireInSevenDays);
            int planDays = 365;
            String planName = "";
            String renewalStatus = "";
            String currencyCode = "USD"; //default currency code
            long amount = 1L; //default amount
            planConfigMap = planService.getPlanDetailsById(3);

            if (businessList.size() > 0 && planConfigMap.size() > 0) {
                planDays = planConfigMap.containsKey("plan_days") && Utils.isNotNull(planConfigMap.get("plan_days"))
                        ? (int) planConfigMap.get("plan_days") : 365;
                planName = planConfigMap.containsKey("plan_name") && Utils.isNotNull(planConfigMap.get("plan_name"))
                        ? planConfigMap.get("plan_name").toString() : "";
                String pricingStr = Utils.isNotNull(planConfigMap.get("pricing")) && planConfigMap.get("pricing") != null
                        ? planConfigMap.get("pricing").toString() : "";
                if (pricingStr != null && !pricingStr.isEmpty() && !pricingStr.equals("0")) {
                    ObjectMapper mapper = new ObjectMapper();
                    Pricing pricing = null;
                    try {
                        pricing = mapper.readValue(pricingStr, Pricing.class);
                    } catch (Exception e) {
                        logger.error("Error in parsing pricing json string");
                    }

                    currencyCode = pricing.getCurrencyCode();
                    amount = pricing.getPrice();
                    // As of now we are billing Annually. So, we need to convert the price for yearly billing
                    amount = amount * 12;
                }
                // loop through each business and send reminder emails for trial accounts

                Iterator<Map<String, Object>> iterator = businessList.iterator();
                while (iterator.hasNext()) {
                    try {
                        //loop through each business
                        Map eachBusiness = iterator.next();
                        int eachBusinessId = (int) eachBusiness.get(Constants.BUSINESS_ID);
                        String eachBusinessUUID = eachBusiness.get(Constants.BUSINESS_UUID).toString();
                        String endDate = eachBusiness.get(Constants.CONTRACT_END_DATE).toString();
                        int planId = (int) eachBusiness.get(Constants.PLAN_CONFIG_ID);
                        String trialStatus = eachBusiness.get(Constants.TRIAL_STATUS).toString();
                        endDate = Utils.getFormattedDate(endDate, "MMM dd yyyy");
                        String trialReminderEndDate = Utils.getFormattedDate(
                                eachBusiness.get(Constants.CONTRACT_END_DATE).toString(), "MMM dd");
                        String subscriptionReminderEndDate = Utils.getFormattedDateForAccountChangeMail(
                                eachBusiness.get(Constants.CONTRACT_END_DATE).toString());
                        String businessEmail = eachBusiness.get(Constants.BUSINESS_EMAIL).toString();
                        String businessName = eachBusiness.get(Constants.BUSINESS_NAME).toString();
                        //get user uuid by business email
                        String userUUID = schedulerService.getUserUUIDBYEmail(businessEmail);
                        String dtToken = schedulerService.getAPIKeyByBusinessId(eachBusinessId);
                        String link = "";

                        //DTV-12744 - constrict dt-lite update card details link
                        // create business renewal schedule for the newly updated card. By default we will create payment intent for 1 USD
                        Map<String, String> paymentIntentMap = paymentService.createPaymentIntent(eachBusinessId, 1L, currencyCode);
                        if (!paymentIntentMap.isEmpty()) {

                            String newStartDate = Utils.addDaysToDate(Utils.convertStringToDate(
                                    eachBusiness.get(Constants.CONTRACT_END_DATE).toString()), 365);
                            String clientSecret = paymentIntentMap.containsKey("clientSecret") ? paymentIntentMap.get("clientSecret").toString() : "";
                            String paymentIntentId = paymentIntentMap.containsKey("paymentIntentId") ? paymentIntentMap.get("paymentIntentId") : "";
                            paymentService.createBusinessRenewalSchedule(eachBusinessId, paymentIntentId,
                                    endDate, newStartDate, 1L, currencyCode, "InComplete", "UPDATE CARD");
                            link = dtWebsiteURL + "payment/?clientsecret=" + clientSecret + "&paymentintentid=" + paymentIntentId +
                                    "&useremail=" + businessEmail + "&businessname=" + businessName + "&apitoken=" + dtToken;
                        } else {
                            link = dtWebsiteURL + "payment/?clientsecret=&paymentintentid=&useremail=" + businessEmail + "&businessname=" + businessName + "&apitoken=" + dtToken;
                        }
                        Map rabbitQueueParam = new HashMap();
                        rabbitQueueParam.put("eachBusinessId", eachBusinessId);
                        rabbitQueueParam.put("planId", planId);
                        rabbitQueueParam.put("businessUUID", eachBusinessUUID);
                        rabbitQueueParam.put("dtToken", dtToken);
                        rabbitQueueParam.put("endDate", endDate);
                        rabbitQueueParam.put("planName", planName);
                        rabbitQueueParam.put("templateType", Constants.PLAN_RENEWAL_REMINDER);
                        rabbitQueueParam.put("userId", userUUID);
                        rabbitQueueParam.put("trialStatus", trialStatus);
                        rabbitQueueParam.put("trialReminderEndDate", trialReminderEndDate);
                        rabbitQueueParam.put("subscriptionReminderEndDate", subscriptionReminderEndDate);
                        rabbitQueueParam.put("link", link);
                        rabbitTemplate.convertAndSend(topicExchange.getName(), routingKey, rabbitQueueParam);
                        logger.info(" Send to scheduler queue for business reminder {}", rabbitQueueParam);
                    } catch (Exception e) {
                        logger.error(e.getMessage());
                    }
                }
            }
        } catch (Exception e) {
            logger.error("exception at Plan Renewal Reminder. {}", e.getMessage());
            logger.error(e.getMessage());
        }
        logger.info("Plan Renewal Reminder - ends");
    }

    @Scheduled(fixedDelayString = "${scheduler.fixedDelay.expired}", initialDelayString = "${scheduler.intialDelay.expired}")
    public void deleteExpiredBusiness() {
        //schedulerService.deleteBusiness(); //comment this SE-329
    }


    /*********Business Contract Schedule *********/

    /**
     * method to activate business contracts
     */
    public void businessContractKickoff() {
        logger.info("business Contract Kickoff - starts");
        try {
            //insert business contract details into business schedule table //TODO
            //get business list which are in inactive status
            List businessList = schedulerService.getScheduledBusinessByStatus(Constants.CONTRACT_STATE_INACTIVE);
            Iterator<Map> iterator = businessList.iterator();
            while (iterator.hasNext()) {
                try {
                    //loop through each business
                    Map eachBusiness = iterator.next();
                    int eachBusinessId = (int) eachBusiness.get(Constants.BUSINESS_ID);
                    String startDate =  eachBusiness.get(Constants.START_DATE).toString();
                    int id = (int) eachBusiness.get(Constants.ID);
                    //DTV-12796 Exclude dt lite users from welcome email
                    int planId = planService.getPlanIdByBusinessId(eachBusinessId);

                    //compute difference between current date and start date
                    long diffMinutes = (int) Utils.differenceMinutes(startDate, Utils.getCurrentUTCDateAsString());
                    if(diffMinutes >= 0 && planId != Constants.DT_LITE_PLAN_ID){
                        //if current date exceeds start date, activate the business. Activate the business in business schedule
                        schedulerService.updateScheduledBusinessState(id, Constants.CONTRACT_STATE_ACTIVE );
                        logger.info("updated business {} status to active for contract kickoff", eachBusinessId);
                        //activate the business
                        schedulerService.updateBusinessState(id, Constants.CONTRACT_STATE_ACTIVE );
                    }

                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error("exception at businessContractKickoff. {}", e.getMessage());
        }
        logger.info("business Contract Kickoff - ends");
    }

    /**
     * method to send reminder emails to business contracts
     */
    public void businessContractReminder() {
        logger.info("business Contract Reminder - starts");
        try {
            //get all active business list
            List businessList = schedulerService.getScheduledBusinessByStatus(Constants.CONTRACT_STATE_ACTIVE);
            Iterator<Map> iterator = businessList.iterator();
            while (iterator.hasNext()) {
                try {
                    //loop through each business
                    Map eachBusiness = iterator.next();
                    int eachBusinessId = (int) eachBusiness.get(Constants.BUSINESS_ID);
                    String eightyPercentDate =  eachBusiness.get(Constants.PRECURSOR_DATE).toString();
                    String contractExpiryDate = eachBusiness.get(Constants.END_DATE).toString();
                    //get contract date by user timezone
                    String timezone = eachBusiness.get(Constants.TIMEZONE).toString();
                    int id = (int) eachBusiness.get(Constants.ID);

                    long diffMinutes = (int)Utils.differenceMinutes(eightyPercentDate, Utils.getCurrentUTCDateAsString());
                    if(diffMinutes >= 0){

                        Map rabbitQueueParam = new HashMap();
                        rabbitQueueParam.put("eachBusinessId", eachBusinessId);
                        rabbitQueueParam.put("contractExpiryDate", contractExpiryDate);
                        rabbitQueueParam.put("timezone", timezone);
                        rabbitQueueParam.put("id", id);
                        rabbitQueueParam.put("templateType", Constants.PRECURSOR_DATE);
                        schedulerService.updateScheduledBusinessState(id, Constants.CONTRACT_PARTIALLY_EXPIRED);
                        rabbitTemplate.convertAndSend(topicExchange.getName(), routingKey, rabbitQueueParam);
                        logger.info(" Send to scheduler queue for business reminder {}", rabbitQueueParam);
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        } catch (Exception e) {
            logger.error("exception at businessContractReminder. {}", e.getMessage());
            logger.error(e.getMessage());
        }
        logger.info("business Contract Reminder - ends");
    }

    /**
     * method to deactivate business contracts if the end date reached the current date
     */
    public void businessContractTermination() {
        logger.info("business Contract Termination - starts");
        try {
            //get all partially expired business from scheduled business
            List businessList = schedulerService.getScheduledBusinessByStatus(Constants.CONTRACT_PARTIALLY_EXPIRED);
            Iterator<Map> iterator = businessList.iterator();
            while (iterator.hasNext()) {
                try {
                    //loop through each business
                    Map eachBusiness = iterator.next();
                    int eachBusinessId = (int) eachBusiness.get(Constants.BUSINESS_ID);
                    String endDate =  eachBusiness.get(Constants.END_DATE).toString();
                    String contractExpiryDate = eachBusiness.get(Constants.END_DATE).toString();
                    String timezone = eachBusiness.get(Constants.TIMEZONE).toString();
                    int id = (int) eachBusiness.get(Constants.ID);

                    Map<String, Object> businessMap = schedulerService.getBusinessById(eachBusinessId);

                    String eachBusinessUUID = businessMap.get(Constants.BUSINESS_UUID).toString();
                    String contractEndDate =  businessMap.get(Constants.CONTRACT_END_DATE).toString();
                    int planId = (int) businessMap.get(Constants.PLAN_CONFIG_ID);
                    contractEndDate = Utils.getFormattedDate(contractEndDate, "MMM dd yyyy");
                    String dtToken = "";
                    // get plan name by plan id
                    String planName = "";
                    if(planId > 0){
                        planName = planService.getPlanNameByPlanId(planId);
                        dtToken = schedulerService.getAPIKeyByBusinessId(eachBusinessId);
                    }

                    //compute difference between current date and end date
                    long diffMinutes = (int)Utils.differenceMinutes(endDate, Utils.getCurrentUTCDateAsString());
                    if(diffMinutes >= 0){
                        Map rabbitQueueParam = new HashMap();
                        rabbitQueueParam.put("eachBusinessId", eachBusinessId);
                        rabbitQueueParam.put("contractExpiryDate", contractExpiryDate);
                        rabbitQueueParam.put("timezone", timezone);
                        rabbitQueueParam.put("id", id);
                        rabbitQueueParam.put("planId", planId);
                        rabbitQueueParam.put("businessUUID", eachBusinessUUID);
                        rabbitQueueParam.put("dtToken", dtToken);
                        rabbitQueueParam.put("endDate", contractEndDate);
                        rabbitQueueParam.put("planName", planName);
                        rabbitQueueParam.put("templateType", planId > 0 ? Constants.PLAN_EXPIRED : Constants.DEACTIVATE);

                        schedulerService.updateScheduledBusinessState(id, Constants.CONTRACT_EXPIRED);
                        //DTV-4897. Renewing the contract to generate new api key
                        schedulerService.deleteAPIKeyByBusinessId(eachBusinessId);
                        //schedulerService.disableAPIKeyByBusinessId(eachBusinessId);
                        //In the business config deactivate the api key during expiration
                        businessConfigurationsService.deactivateApiKeyBusinessId(eachBusinessId);

                        rabbitTemplate.convertAndSend(topicExchange.getName(), routingKey, rabbitQueueParam);
                        logger.info("Send to scheduler queue for business contract termination {}", rabbitQueueParam);
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        } catch (Exception e) {
            logger.error("exception at businessContractTermination. {}", e.getMessage());
            logger.error(e.getMessage());
        }
        logger.info("business Contract Termination - ends");
    }

    /**
     * Send login mail while creating client based on contract date
     *
     */
    public void businessSendLoginMail() {
        logger.info("business :: login send mail - starts");
        try {
            //get business list which are in inactive status
            List businessList = schedulerService.getNotConfirmedBusinessUsers(Constants.CONTRACT_STATE_ACTIVE);
            Iterator<Map> iterator = businessList.iterator();
            while (iterator.hasNext()) {
                try {
                    //loop through each business
                    Map eachBusiness = iterator.next();
                    int eachBusinessId = (int) eachBusiness.get(Constants.BUSINESS_ID);
                    String businessUUID =  "";
                    String startDate =  eachBusiness.get(Constants.START_DATE).toString();
                    //compute difference between current date and start date
                    long diffMinutes = (int) Utils.differenceMinutes(startDate, Utils.getCurrentUTCDateAsString());
                    if(diffMinutes >= 0) {
                        int confirmationStatus = eachBusiness.containsKey("confirmation_status") && eachBusiness.get("confirmation_status") != null ? Integer.parseInt(eachBusiness.get("confirmation_status").toString()) : 1;
                        if (confirmationStatus == 0) {
                            String kingUserEmail = eachBusiness.containsKey("user_email") && eachBusiness.get("user_email") != null ? eachBusiness.get("user_email").toString() : "";
                            int userId =  Integer.parseInt(eachBusiness.get("user_id").toString());
                            String recipientName =  eachBusiness.containsKey("full_name") && eachBusiness.get("full_name") != null ?
                                    eachBusiness.get("full_name").toString() : "User";
                           /* String firstName = eachBusiness.containsKey("firstname") ? (String) eachBusiness.get("firstname") : "User";
                            String lastName = eachBusiness.containsKey("lastname") ? (String) eachBusiness.get("lastname") : "";
                            String recipientName = firstName + " " + lastName;*/
                            String userUUID =  eachBusiness.containsKey("user_uuid") && eachBusiness.get("user_uuid") != null ? eachBusiness.get("user_uuid").toString() : "";
                            //schedulerService.sendLoginKingMail(eachBusinessId, confirmationStatus, userId, kingUserEmail, recipientName, userUUID);
                            //#5412 password flow changes, while business creation we need to send password reset mail
                            //DTV-11324 If Customer register with any plan in Dt website, then we are sending mail in dtapp itself.
                            // So we need to check the plan and send mail accordingly.
                            int planId = 0;
                            Map<String, Object> planMap = planService.getPlanByBusinessId(eachBusinessId);
                            if (!planMap.isEmpty()) {
                                planId = (int) planMap.get("plan_config_id");
                                businessUUID = planMap.get("business_uuid").toString();
                            }
                            if (planId == 0) {
                                schedulerService.sendPasswordUpdateKingMail(eachBusinessId, confirmationStatus, userId, kingUserEmail, recipientName, userUUID);
                            } else {
                                // verify the payment status and send welcome mail
                                //DTV-12599 As part of trial period, we will send welcome mail to the customer
                                //boolean paymentStatus = paymentService.verifyPaymentStatus(businessUUID);
                                //if (paymentStatus) {
                                    schedulerService.sendWelcomeMail(eachBusinessId, confirmationStatus, userId,
                                            kingUserEmail, recipientName, userUUID, businessUUID);
                               // }
                                //payment failure mail will sent at the time of payment failure
                                /*else {
                                    String planName = planService.getPlanNameByPlanId(planId);
                                    schedulerService.sendPaymentFailureMail(eachBusinessId, kingUserEmail, recipientName, planName);
                                }*/
                            }
                        }
                    }

                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error("exception at business :: login send mail. {}", e.getMessage());
        }
        logger.info("business :: login send mail - ends");
    }


    /**
     * chron to run every 1 hour to remove terminated tokens
     */
    //@Scheduled(cron = "* * * * * *")
    @Scheduled(cron = "0 0 */1 * * *")
    public void updateExecutionTimes() {
        schedulerService.updateTerminatedTokens();
    }

    /**
     * chron to run every 5 minutes to check shorturl mapping
     */
    @Scheduled(cron = "*/2 * * * * *")
    public void verifyShortUrlLink() {
        //Testing purpose
        schedulerService.validateShortUrlLinkV2();
        //To extend the SMS and Short URL TTL after extent the survey end date
        schedulerService.extentTTLForShortURLAndSMS();
    }

    /**
     * chron to run every 5 minutes to check qrcode mapping
     */
    @Scheduled(cron = "*/5 * * * * *")
    public void verifyQrCodeLinks() {
        logger.info("begin verify qr code links");
        //Testing purpose
        schedulerService.insertQrFromTempToMain();
        schedulerService.insertDynamicQrFromTempToMainTable();
        logger.info("end verify qr code links");
    }

    /**
     * chron to run every 5 minutes to create feedbacks for bulk import responses
     */
    @Scheduled(cron = "*/5 * * * * *")
    public void createBulkFeedbacks() {
        //Testing purpose
        //chron job to run active feedbacks from temp_data_feedback_process table
        schedulerService.createExcelFeedbacks();
    }

    @Scheduled(cron = "*/2 * * * * *")
    public void updateQRShortUrl() {
        schedulerService.updateQRShortUrl();
    }

    /**
     * With this cron expression, the method will execute only once a day at 6 PM UTC.
     * 0: Specifies that the task should run at the 0th second of the minute.
     * 0: Specifies that the task should run at the 0th minute of the hour.
     * 18: Specifies the hour (6 PM) when the task should run. This is specified in 24-hour format.
     * *: Specifies that the task should run on every day of the month.
     * *: Specifies that the task should run on every month.
     * *: Specifies that the task should run on every day of the week.
     * With this cron expression, the method will execute only once a day at 6 PM UTC.
     */
    @Scheduled(cron = "0 0 18 * * *")
    public void updateTekionRepairOrders() {
        //logger.info("begin processing tekion repair orders");
        tekionService.updateTekionRepairOrders();
        //logger.info("end processing tekion repair orders");
        tekionService.updateTekionDeals();
    }

   /**
     * With this cron expression, the method will execute only once a day at 6 PM UTC.
     * 0: Specifies that the task should run at the 0th second of the minute.
     * 0: Specifies that the task should run at the 0th minute of the hour.
     * 19: Specifies the hour (7 PM) when the task should run. This is specified in 24-hour format.
     * *: Specifies that the task should run on every day of the month.
     * *: Specifies that the task should run on every month.
     * *: Specifies that the task should run on every day of the week.
     * With this cron expression, the method will execute only once a day at 7 PM UTC.
     */
    @Scheduled(cron = "0 0 19 * * *")
    public void updateTekionRepairOrdersToList() {
        //logger.info("begin processing tekion repair orders to participant list");
        tekionService.updateTekionRepairOrdersToParticipantList();
        //logger.info("end processing tekion repair orders to participant list");
        tekionService.updateTekionDealsToParticipantList();
    }

    /**
     * chron job to run monthly once to update business_usage (ai_surveys_usage)
     */
    @Scheduled(cron = "0 0 0 1 * *")
    public void updateBusinessUsages() {
        businessUsageService.resetAiSurveyUsage();
    }

    /**
     * chron to run every 1 hour to renew tokens in user connected accounts (connected_accounts table)
     */
    @Scheduled(cron = "0 0 */1 * * *")
    public void renewUserTokens() {
        multiAccountService.renewUserTokens();
    }


}
