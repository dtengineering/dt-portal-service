package com.dropthought.portal.common;

public class DTConstants {

    public static final String UUID_REGREX = "^[0-9a-zA-Z]{8}-([0-9a-zA-Z]{4}-){3}[0-9a-zA-Z]{12}+$";

    public static final String ONLY_NUMBERS = "^[0-9]$";

    public static final String ONLY_ALPHABETS = "^[a-zA-Z]$";
}
