package com.dropthought.portal.businessusage;

import com.dropthought.portal.businessconfigurations.service.BusinessConfigurationsService;
import com.dropthought.portal.service.BaseService;
import com.dropthought.portal.service.ClientService;
import com.dropthought.portal.service.NotificationService;
import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BusinessUsageService extends BaseService {

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ClientService clientService;

    @Autowired
    NotificationService notificationService;

    @Autowired
    BusinessConfigurationsService businessConfigurationsService;

    @Autowired
    BusinessUsageDao businessUsageDao;

    /**
     * Get client usage by clientId from business_usage table
     *
     * @param businessId
     * @return
     */
    public Map getClientUsageByClientId(int businessId) {

        Map businessUsageMap = new HashMap();
        logger.info("[START] getClientUsageByClientId");
        try {
            String notification = Constants.ZERO_STRING;

            List usageList = businessUsageDao.retrieveClientUsageByClientId(businessId);
            if (usageList.size() > 0) {
                Map usageMap = (Map) usageList.get(0);
                int users = Utils.isNotNull(usageMap.get("users")) && Utils.notEmptyString(usageMap.get("users").toString()) ? Integer.parseInt(usageMap.get("users").toString()) : 0;
                int programs = Utils.isNotNull(usageMap.get("programs")) && Utils.notEmptyString(usageMap.get("programs").toString()) ? Integer.parseInt(usageMap.get("programs").toString()) : 0;
                int responses = Utils.isNotNull(usageMap.get("responses")) && Utils.notEmptyString(usageMap.get("responses").toString()) ? Integer.parseInt(usageMap.get("responses").toString()) : 0;
                int metrics = Utils.isNotNull(usageMap.get("metrics")) && Utils.notEmptyString(usageMap.get("metrics").toString()) ? Integer.parseInt(usageMap.get("metrics").toString()) : 0;
                int filters = Utils.isNotNull(usageMap.get("filters")) && Utils.notEmptyString(usageMap.get("filters").toString()) ? Integer.parseInt(usageMap.get("filters").toString()) : 0;
                int triggers = Utils.isNotNull(usageMap.get("triggers")) && Utils.notEmptyString(usageMap.get("triggers").toString()) ? Integer.parseInt(usageMap.get("triggers").toString()) : 0;
                int emails = Utils.isNotNull(usageMap.get("emails")) && Utils.notEmptyString(usageMap.get("emails").toString()) ? Integer.parseInt(usageMap.get("emails").toString()) : 0;
                int sms = Utils.isNotNull(usageMap.get("sms")) && Utils.notEmptyString(usageMap.get("sms").toString()) ? Integer.parseInt(usageMap.get("sms").toString()) : 0;
                int whatsapp = Utils.isNotNull(usageMap.get("whatsapp")) && Utils.notEmptyString(usageMap.get("whatsapp").toString()) ? Integer.parseInt(usageMap.get("whatsapp").toString()) : 0;
                int api = Utils.isNotNull(usageMap.get("api")) && Utils.notEmptyString(usageMap.get("api").toString()) ? Integer.parseInt(usageMap.get("api").toString()) : 0;
                int lists = Utils.isNotNull(usageMap.get("lists")) && Utils.notEmptyString(usageMap.get("lists").toString()) ? Integer.parseInt(usageMap.get("lists").toString()) : 0;
                int recipients = Utils.isNotNull(usageMap.get("recipients")) && Utils.notEmptyString(usageMap.get("recipients").toString()) ? Integer.parseInt(usageMap.get("recipients").toString()) : 0;
                int dataRetention = Utils.isNotNull(usageMap.get("data_retention")) && Utils.notEmptyString(usageMap.get("data_retention").toString()) ? Integer.parseInt(usageMap.get("data_retention").toString()) : 0;
                int integrations = Utils.isNotNull(usageMap.get("integrations")) && Utils.notEmptyString(usageMap.get("integrations").toString()) ? Integer.parseInt(usageMap.get("integrations").toString()) : 0;
                int kiosks = Utils.isNotNull(usageMap.get("kiosks")) && Utils.notEmptyString(usageMap.get("kiosks").toString()) ? Integer.parseInt(usageMap.get("kiosks").toString()) : 0;
                int aiSurveysUsage = Utils.isNotNull(usageMap.get("ai_surveys_usage")) && Utils.notEmptyString(usageMap.get("ai_surveys_usage").toString()) ? Integer.parseInt(usageMap.get("ai_surveys_usage").toString()) : 0;

                businessUsageMap.put("usersUsageCount", users);
                businessUsageMap.put("programsUsageCount", programs);
                businessUsageMap.put("responsesUsageCount", responses);
                businessUsageMap.put("metricsUsageCount", metrics);
                businessUsageMap.put("filtersUsageCount", filters);
                businessUsageMap.put("triggersUsageCount", triggers);
                businessUsageMap.put("emailsUsageCount", emails);
                businessUsageMap.put("smsUsageCount", sms);
                businessUsageMap.put("whatsappUsageCount", whatsapp);
                businessUsageMap.put("apiUsageCount", api);
                businessUsageMap.put("listsUsageCount", lists);
                businessUsageMap.put("recipientsUsageCount", recipients);
                businessUsageMap.put("dataRetentionUsageCount", dataRetention);
                businessUsageMap.put("integrationsUsageCount", integrations);
                businessUsageMap.put("kiosksUsageCount", kiosks);
                businessUsageMap.put("aiSurveysUsageCount", aiSurveysUsage);
                logger.info("businessID ---{}", businessId);
                logger.info("kiosks Count ---{}", kiosks);
                logger.info("integration Count -->{}", integrations);
            }


        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("end retrieving configs by business id");
        return businessUsageMap;

    }

    /**
     * Function to get business usage Id by businessUUID
     *
     * @return
     */
    public int getBusinessUsageIdByBusinessId(int businessId) {
        return businessUsageDao.retrieveBusinessUsageIdByBusinessId(businessId);
    }

    /**
     * function to check usage limit reached
     *
     * @param businessId
     * @return
     */

    public boolean checkEightyPercentUsageLimitReached(int businessId, String source, int usageCount) {
        logger.info("Start check usage limit");
        boolean limitReached = false;
        int limit = 0;
        try {
            if (Utils.notEmptyString(source) && usageCount > 0) {
                List configurationsList = businessConfigurationsService.getBusinessConfigurationsByBusinessId(businessId);
                if (configurationsList.size() > 0) {
                    Map limitMap = (Map) configurationsList.get(0);
                    switch (source) {
                        case "users":
                            limit = (limitMap.containsKey("noOfUsers") && Utils.isNotNull(limitMap.get("noOfUsers"))) ? Integer.parseInt(limitMap.get("noOfUsers").toString()) : 0;
                            limitReached = (limit >= 0 && (usageCount >= limit * 0.8));
                            break;
                        case "programs":
                            limit = (limitMap.containsKey("noOfActivePrograms") && Utils.isNotNull(limitMap.get("noOfActivePrograms"))) ? Integer.parseInt(limitMap.get("noOfActivePrograms").toString()) : 0;
                            limitReached = (limit >= 0 && (usageCount >= limit * 0.8));
                            break;
                        case "responses":
                            limit = (limitMap.containsKey("noOfResponses") && Utils.isNotNull(limitMap.get("noOfResponses"))) ? Integer.parseInt(limitMap.get("noOfResponses").toString()) : 0;
                            limitReached = (limit >= 0 && (usageCount >= limit * 0.8));
                            break;
                        case "recipients":
                            limit = (limitMap.containsKey("noOfRecipients") && Utils.isNotNull(limitMap.get("noOfRecipients"))) ? Integer.parseInt(limitMap.get("noOfRecipients").toString()) : 0;
                            limitReached = (limit >= 0 && (usageCount >= limit * 0.8));
                            break;
                        case "lists":
                            limit = (limitMap.containsKey("noOfLists") && Utils.isNotNull(limitMap.get("noOfLists"))) ? Integer.parseInt(limitMap.get("noOfLists").toString()) : 0;
                            limitReached = (limit >= 0 && (usageCount >= limit * 0.8));
                            break;
                        case "emails":
                            limit = (limitMap.containsKey("noOfEmailsSent") && Utils.isNotNull(limitMap.get("noOfEmailsSent"))) ? Integer.parseInt(limitMap.get("noOfEmailsSent").toString()) : 0;
                            limitReached = (limit >= 0 && (usageCount >= limit * 0.8));
                            break;
                        case "sms":
                            limit = (limitMap.containsKey("noOfSms") && Utils.isNotNull(limitMap.get("noOfSms"))) ? Integer.parseInt(limitMap.get("noOfSms").toString()) : 0;
                            limitReached = (limit >= 0 && (usageCount >= limit * 0.8));
                            break;
                        case "whatsapp":
                            limit = (limitMap.containsKey("noOfWhatsapp") && Utils.isNotNull(limitMap.get("noOfWhatsapp"))) ? Integer.parseInt(limitMap.get("noOfWhatsapp").toString()) : 0;
                            limitReached = (limit >= 0 && (usageCount >= limit * 0.8));
                            break;
                        case "triggers":
                            limit = (limitMap.containsKey("noOfTriggers") && Utils.isNotNull(limitMap.get("noOfTriggers"))) ? Integer.parseInt(limitMap.get("noOfTriggers").toString()) : 0;
                            limitReached = (limit >= 0 && (usageCount >= limit * 0.8));
                            break;
                        case "api":
                            limit = (limitMap.containsKey("noOfApi") && Utils.isNotNull(limitMap.get("noOfApi"))) ? Integer.parseInt(limitMap.get("noOfApi").toString()) : 0;
                            limitReached = (limit >= 0 && (usageCount >= limit * 0.8));
                            break;
                        case "filters":
                            limit = (limitMap.containsKey("noOfFilters") && Utils.isNotNull(limitMap.get("noOfFilters"))) ? Integer.parseInt(limitMap.get("noOfFilters").toString()) : 0;
                            limitReached = (limit >= 0 && (usageCount >= limit * 0.8));
                            break;
                        case "metrics":
                            limit = (limitMap.containsKey("noOfMetrics") && Utils.isNotNull(limitMap.get("noOfMetrics"))) ? Integer.parseInt(limitMap.get("noOfMetrics").toString()) : 0;
                            limitReached = (limit >= 0 && (usageCount >= limit * 0.8));
                            break;
                        default:
                            break;
                    }
                }
            }
            logger.info("limit reached  " + limitReached);

        } catch (Exception e) {
            logger.info("No businessUUID found");
        }
        logger.info("End check usage limit");
        return limitReached;
    }

    /**
     * Function to send usage limit mail to king user
     *
     * @param businessId
     * @return
     */
    public Map sendUsageLimitMail(int businessId, String source, int usageCount) {
        logger.info("Start send usage limit mail");
        Map resultMap = new HashMap();
        try {
            Map mailParam = new HashMap();
            if (Utils.notEmptyString(source) && usageCount > 0) {
                String message = "";
                switch (source) {
                    case "users":
                        message = String.format(Constants.USERS_USAGE_MESSAGE, usageCount);
                        break;
                    case "programs":
                        message = String.format(Constants.PROGRAM_USAGE_MESSAGE, usageCount);
                        break;
                    case "responses":
                        message = String.format(Constants.RESPONSE_USAGE_MESSAGE, usageCount);
                        break;
                    case "recipients":
                        message = String.format(Constants.RECIPIENT_USAGE_MESSAGE, usageCount);
                        break;
                    case "lists":
                        message = String.format(Constants.LIST_USAGE_MESSAGE, usageCount);
                        break;
                    case "emails":
                        message = String.format(Constants.EMAIL_USAGE_MESSAGE, usageCount);
                        break;
                    case "sms":
                        message = String.format(Constants.SMS_USAGE_MESSAGE, usageCount);
                        break;
                    case "whatsapp":
                        message = String.format(Constants.WHATSAPP_USAGE_MESSAGE, usageCount);
                        break;
                    case "triggers":
                        message = String.format(Constants.TRIGGER_USAGE_MESSAGE, usageCount);
                        break;
                    case "api":
                        message = String.format(Constants.API_USAGE_MESSAGE, usageCount);
                        break;
                    case "filters":
                        message = String.format(Constants.FILTER_USAGE_MESSAGE, usageCount);
                        break;
                    case "metrics":
                        message = String.format(Constants.METRIC_USAGE_MESSAGE, usageCount);
                        break;
                    case "kiosks":
                        message = String.format(Constants.KIOSKS_USAGE_MESSAGE, usageCount);
                    case "integrations":
                        message = String.format(Constants.INTEGRATIONS_USAGE_MESSAGE, usageCount);
                    default:
                        break;
                }

                if (message.length() > 0) {
                    Map kingUserMap = clientService.getKingUserByBusinessId(businessId);
                    mailParam.put("toEmail", kingUserMap.containsKey("user_email") && kingUserMap.get("user_email") != null ? kingUserMap.get("user_email").toString() : "");
                    mailParam.put("recipientName", kingUserMap.containsKey("full_name") && kingUserMap.get("full_name") != null ? kingUserMap.get("full_name").toString() : "");
                    mailParam.put("message", message);
                    logger.info("message  " + message);
                    notificationService.sendBusinessUsageReminderMail(mailParam, businessId);
                }
            }
            logger.debug("end retrieving business by business uuid");
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("Start send usage limit mail");
        return resultMap;
    }


    /**
     * method to update usage_mail_flag by comparing new limits with usage
     * if usage is less than 80% after new limits updating usage_mail_flag
     *
     * @param businessId
     * @return
     */
    public void updateUsageMailFlagWithNewLimitsByBusinessId(int businessId, Map usageLimitsMap) {

        try {
            if (businessId > 0 && !usageLimitsMap.isEmpty()) {
                Map usageMailFlag = this.getUsageMailFlagByBusinessId(businessId);
                logger.info("usageMailFlag  " + usageMailFlag);
                Map usageMap = this.getClientUsageByClientId(businessId);
                List keys = new ArrayList(usageLimitsMap.keySet());
                logger.info("keys  " + keys);

                for (int i = 0; i < keys.size(); i++) {
                    int usage = 0;
                    String source = keys.get(i).toString();
                    int newLimit = Integer.parseInt(usageLimitsMap.get(source).toString());

                    usage = (usageMap.containsKey(source + "UsageCount") && Utils.isNotNull(usageMailFlag.get(source + "UsageCount"))) ? Integer.parseInt(usageMailFlag.get(source + "UsageCount").toString()) : 0;
                    boolean usageBelowLimit = (usage < (newLimit * 0.8));
                    if (usageBelowLimit) {
                        usageMailFlag.replace(source, 0);
                    }
                }
                logger.info("usageMailFlag  " + usageMailFlag);
                this.updateUsageMailFlagByBusinessId(businessId, usageMailFlag);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }


    /**
     * function to get usage email flag map from business_usage table
     *
     * @param businessId
     * @return
     */
    public Map getUsageMailFlagByBusinessId(int businessId) {
        return businessUsageDao.retrieveUsageMailFlagByBusinessId(businessId);
    }


    /**
     * function to update usage email flag map by businessId
     *
     * @param businessId
     * @param usageMailFlagMap
     * @return
     */
    public void updateUsageMailFlagByBusinessId(int businessId, Map usageMailFlagMap) {
        businessUsageDao.updateUsageMailFlagByBusinessId(businessId, usageMailFlagMap);
    }

    /**
     * Method to update ai_surveys_usage to 0, at the start of every month
     */
    public void resetAiSurveyUsage() {
        businessUsageDao.resetAiSurveyUsage();
    }
}
