package com.dropthought.portal.businessusage;

import com.dropthought.portal.util.TokenUtility;
import com.dropthought.portal.util.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class BusinessUsageDao {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * JdbcTemplate for DB Connectivity.
     */
    @Autowired
    @Qualifier("jdbcDt")
    JdbcTemplate jdbcTemplate;

    @Autowired
    TokenUtility tokenUtility;

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();

    private static final String GET_BUSINESS_USAGE_ID_BY_BUSINESS_ID = "select business_usage_id from business_usage where business_id = ?";
    private static final String GET_BUSINESS_USAGE_BY_BUSINESS_ID = "select business_usage_id, business_id, users, programs, responses, metrics, filters, triggers, emails, sms, api, lists, recipients, data_retention, integrations, kiosks, current_month_start, current_month_end, usage_email_flag, whatsapp, ai_surveys_usage from business_usage where business_id = ?";
    public static final String GET_USAGE_MAIL_FLAG_BY_BUSINESS_ID = "select usage_email_flag from business_usage where business_id = ?";
    public static final String UPDATE_USAGE_MAIL_BY_BUSINESS_ID = "update business_usage set usage_email_flag = ? where business_id = ?";
    public static final String UPDATE_RESET_SURVEYS_USAGE = "update business_usage set ai_surveys_usage= ?";

    /**
     * Function to get business usage Id by businessUUID
     *
     * @return
     */
    public int retrieveBusinessUsageIdByBusinessId(int businessId) {
        Integer result = 0;
        logger.info("[START] retrieveBusinessUsageIdByBusinessId");
        try {
            result = jdbcTemplate.queryForObject(GET_BUSINESS_USAGE_ID_BY_BUSINESS_ID, new Object[]{businessId}, Integer.class);
        } catch (Exception e) {
            logger.info("business usage id not found for the given business uuid");
        }
        logger.info("[END] retrieveBusinessUsageIdByBusinessId");
        return result;
    }

    /**
     * Function to get business usage Id by businessUUID
     *
     * @return
     */
    public List retrieveClientUsageByClientId(int businessId) {
        List result = Collections.emptyList();
        logger.info("[START] retrieveClientUsageByClientId");
        try {
            result = jdbcTemplate.queryForList(GET_BUSINESS_USAGE_BY_BUSINESS_ID, businessId);
        } catch (Exception e) {
            logger.info("[ERROR] retrieveClientUsageByClientId");
        }
        logger.info("[END] retrieveClientUsageByClientId");
        return result;
    }

    /**
     * function to get usage email flag map from business_usage table
     *
     * @param businessId
     * @return
     */
    public Map<String, Object> retrieveUsageMailFlagByBusinessId(int businessId) {
        Map<String, Object> usageMailFlagMap = Collections.emptyMap();
        logger.info("[START] retrieveUsageMailFlagByBusinessId");
        try {
            List usageMailList = jdbcTemplate.queryForList(GET_USAGE_MAIL_FLAG_BY_BUSINESS_ID, businessId);
            if (!usageMailList.isEmpty()) {
                Map usageMap = (Map) usageMailList.get(0);
                usageMailFlagMap = Utils.isNotNull(usageMap.get("usage_email_flag")) ?
                        mapper.readValue(usageMap.get("usage_email_flag").toString(), HashMap.class) : Collections.emptyMap();
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("[END] retrieveUsageMailFlagByBusinessId");
        return usageMailFlagMap;
    }

    /**
     * function to update usage email flag map by businessId
     *
     * @param businessId
     * @param usageMailFlagMap
     * @return
     */
    public void updateUsageMailFlagByBusinessId(int businessId, Map usageMailFlagMap) {
        logger.info("[START] updateUsageMailFlagByBusinessId");
        try {
            jdbcTemplate.update(UPDATE_USAGE_MAIL_BY_BUSINESS_ID, new JSONObject(usageMailFlagMap).toString(),
                    businessId);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("[END] updateUsageMailFlagByBusinessId");
    }

    /**
     * Method to update ai_surveys_usage to 0, at the start of every month
     */
    public void resetAiSurveyUsage() {
        logger.info("[START] resetAiSurveyUsage");
        try {
            jdbcTemplate.update(UPDATE_RESET_SURVEYS_USAGE, 0);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("[END] resetAiSurveyUsage");
    }


}
