package com.dropthought.portal.businessusage;

import com.dropthought.portal.common.DTConstants;
import com.dropthought.portal.service.ClientService;
import com.dropthought.portal.util.Constants;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(path = "/api/v1/portal/usage")
@Validated
public class BusinessUsageController  {

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Http Headers
     */
    protected final HttpHeaders httpHeaders = new HttpHeaders();

    @Autowired
    private BusinessUsageService businessUsageService;

    @Autowired
    private ClientService clientService;

    /**
     *  Get client business information by clientId
     *
     * @param businessUUID
     * @return
     */
    @GetMapping("/client/{clientId}")
    @ResponseBody
    public ResponseEntity<String> getClient(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid clientId") String businessUUID) {
        logger.info("begin rest api to get a business usage by business uuid");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map clientUsageMap = new HashMap();
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        ArrayList<String> messages = new ArrayList<>();
        try {
            int businessId = clientService.getBusinessIdByUUID(businessUUID);
            if(businessId > 0){

                int businessUsageId = businessUsageService.getBusinessUsageIdByBusinessId(businessId);
                if(businessUsageId > 0) {
                    clientUsageMap = businessUsageService.getClientUsageByClientId(businessId);
                    resultMap.put(Constants.SUCCESS, true);
                    resultMap.put(Constants.RESULT, clientUsageMap);
                    responseEntity = new ResponseEntity<>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
                }else {
                    resultMap.put(Constants.SUCCESS, false);
                    resultMap.put("error", "business usage id not found");
                    responseEntity = new ResponseEntity<>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.NOT_FOUND);
                }

            } else {
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.RESULT, "business uuid not found");
                responseEntity = new ResponseEntity<>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.RESULT, "internal server error");
            messages.add(e.getMessage());
            resultMap.put("developerMessages", messages);
            responseEntity = new ResponseEntity<>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        logger.info("end rest api to get a business usage by business id");
        return responseEntity;
    }

    /**
     *  Get client business information by clientId
     *
     * @param businessId
     * @return
     */
    @GetMapping("/mail/client/{clientId}")
    @ResponseBody
    public ResponseEntity<String> sendUsageMail(@PathVariable("clientId") @NotNull(message = "Invalid clientId") int businessId,
                                                @RequestParam(value = "source", defaultValue = "") String source,
                                                @RequestParam(value = "usageCount", defaultValue = "") int usageCount) {
        logger.info("begin rest api to send mail usage by  business uuid");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        ArrayList<String> messages = new ArrayList<>();
        try {
            if(businessId > 0){
                int businessUsageId = businessUsageService.getBusinessUsageIdByBusinessId(businessId);
                if(businessUsageId > 0) {
                    boolean isLimitReached = businessUsageService.checkEightyPercentUsageLimitReached(businessId, source, usageCount);
                    if(isLimitReached){
                        Map result = businessUsageService.sendUsageLimitMail(businessId, source, usageCount);
                        resultMap.put(Constants.SUCCESS, true);
                        resultMap.put(Constants.RESULT, result);
                        responseEntity = new ResponseEntity<>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
                    }else {
                        resultMap.put(Constants.SUCCESS, false);
                        resultMap.put(Constants.RESULT, "Limit not reached");
                        responseEntity = new ResponseEntity<>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.NOT_FOUND);
                    }
                }else {
                    resultMap.put(Constants.SUCCESS, false);
                    resultMap.put(Constants.RESULT, "business usage id not found");
                    responseEntity = new ResponseEntity<>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.NOT_FOUND);
                }
            } else {
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.RESULT, "business id not found");
                responseEntity = new ResponseEntity<>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.RESULT, "internal server error");
            messages.add(e.getMessage());
            resultMap.put("developerMessages", messages);
            responseEntity = new ResponseEntity<>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        logger.info("End rest api to send mail usage by  business uuid");
        return responseEntity;
    }

}
