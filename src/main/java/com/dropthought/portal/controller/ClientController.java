package com.dropthought.portal.controller;


import com.dropthought.portal.common.DTConstants;
import com.dropthought.portal.model.ClientPortalAccessBean;
import com.dropthought.portal.model.MessageBean;
import com.dropthought.portal.model.TemplateUpdateBean;
import com.dropthought.portal.model.ThemesBean;
import com.dropthought.portal.service.ClientService;
import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.Utils;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import java.io.IOException;
import java.util.*;

@RestController
@RequestMapping(path = "/api/v1/portal/client")
@Validated
public class ClientController {

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Http Headers
     */
    protected final HttpHeaders httpHeaders = new HttpHeaders();

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper()
            .enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT)
            .enable(DeserializationFeature.FAIL_ON_READING_DUP_TREE_KEY );

    @Autowired
    private ClientService clientService;

    /**
     * Clint API creation for customer portal
     * Will create business and king user
     *
     * @param clientBean
     * @return
     */
    @PostMapping(headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    //public ResponseEntity<String> createClient(@RequestBody ClientBean clientBean) {
    public ResponseEntity<String> createClient(@RequestBody String clientData) {
        logger.info("begins client business creation");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(clientData)) {
                logger.info("input clientdata {}", clientData);
                resultMap = clientService.createClient(clientData);
                logger.info("response {}", resultMap);
            }
            else{
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide mandatory fields");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
        }
        logger.info("create client business creation ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Update client business information by businessUUID
     *
     * @param clientBean
     * @return
     */
    @PutMapping("{clientId}")
    @ResponseBody
    public ResponseEntity<String> updateClient(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid clientId") String businessUUID,
                                               @RequestBody String data) {
        logger.info("client business update begins");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(businessUUID)) {
                resultMap = clientService.updateClient(businessUUID, data);
                logger.info("response {}", resultMap);
            }
            else{
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide businessUUID mandatory field");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
        }
        logger.info("client business update ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     *  Get client business information by clientId
     *
     * @param businessUUID
     * @return
     */
    @GetMapping("/{clientId}")
    @ResponseBody
    public ResponseEntity<String> getClient(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid clientId") String businessUUID,
                                            @RequestParam(value = "timezone", required = false, defaultValue = "America/Los_Angeles") String timezone) {
        logger.info("get client business details begins");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(businessUUID)) {
                int isTrueUUID = clientService.getBusinessIdByUUID(businessUUID);
                if (isTrueUUID > 0) {
                    List response = clientService.getClientByClientId(businessUUID, timezone);
                    if (response.size() > 0) {
                        resultMap.put(Constants.SUCCESS, true);
                        resultMap.put(Constants.RESULT, response);
                    } else {
                        resultMap.put(Constants.SUCCESS, true);
                        resultMap.put(Constants.RESULT, Collections.emptyList());
                    }
                    logger.info("response {}", resultMap);
                } else {
                    resultMap.put(Constants.SUCCESS, false);
                    resultMap.put(Constants.ERROR, "Provided businessUUID is invalid");
                }

            }
            else{
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide businessUUID mandatory field");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
        }
        logger.info("get client business details ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Get all the client business information
     *
     * @return
     */
    @GetMapping()
    @ResponseBody
    public ResponseEntity<String> getAllClient(@RequestParam(value = "timezone", required = false, defaultValue = "America/Los_Angeles") String timezone,
                                               @RequestParam(defaultValue = "true", required = false) boolean optimized,
                                               @RequestParam(value = "searchTerm", defaultValue = "", required = false) String searchTerm,
                                               @RequestParam(value = "planName", required = false, defaultValue = "") String planName,
                                               @RequestParam(value = "pageId", required = false, defaultValue = "-1") int pageId) {
        logger.info("get all client business details begins");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
            List response = new ArrayList();
            if(!optimized) {
                response = clientService.getAllClient(timezone, searchTerm, planName, pageId);
            }else{
                response = clientService.getAllClientV2(timezone, searchTerm, planName, pageId);
            }
            if (response.size() > 0) {
                resultMap.put(Constants.SUCCESS, true);
                resultMap.put(Constants.RESULT, response);
                resultMap.put("totalCount", response.size());
            }
            else {
                resultMap.put(Constants.SUCCESS, true);
                resultMap.put(Constants.RESULT, Collections.emptyList());
            }
            logger.debug("response {}", resultMap);
        }
        catch (Exception e){
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
        }
        logger.info("get all client business details ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Gets a client business based on state
     *
     * @param state
     * @param searchTerm
     * @param pageId
     * @param timezone
     * @return
     */
    @GetMapping("/summary")
    public ResponseEntity<String> getBusinessByState(@RequestParam(value = "state", required = true, defaultValue = "-1") @Positive(message = "Invalid state") int state,
                                                     @RequestParam(value = "search",required = false, defaultValue = "") String searchTerm,
                                                     @RequestParam(value = "pageId", required = false, defaultValue = "-1") int pageId,
                                                     @RequestParam(value = "exactMatch", required = false, defaultValue = "false") boolean exactMatch,
                                                     @RequestParam(value = "sortCriteria", required = false, defaultValue = "createdTime") String sortString,
                                                     @RequestParam(value = "timezone", required = false, defaultValue = "America/Los_Angeles") String timezone) {
        logger.info("get client business information based on state starts");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
            List businessList = clientService.getClientByState(state, searchTerm, pageId, exactMatch, sortString, timezone);
            logger.info("get client business information based on state ends");
            if (businessList.size() > 0) {
                MessageBean result = new MessageBean(businessList);
                return new ResponseEntity<String>(result.toString(), httpHeaders, HttpStatus.OK);
            } else {
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, "Business not found");
                return new ResponseEntity<String>((new JSONObject(resultMap).toString()), httpHeaders, HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
            return new ResponseEntity<String>((new JSONObject(resultMap).toString()), httpHeaders, HttpStatus.NOT_FOUND);
        }
    }

    /**
     * creating cs user for given businessId if cs user is not present,
     * If cs user is already present activating the cs user.
     *
     * @param businessUUID
     * @return
     */
    @PostMapping("/{clientId}/access")
    @ResponseBody
    public ResponseEntity<String> createCsUserByBusinessId(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid clientId") String businessUUID) {
        logger.info("client portal access request api starts");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        ResponseEntity<String> responseEntity = null;
        try {
            if(Utils.isNotNull(businessUUID) && Utils.notEmptyString(businessUUID)) {
                int isTrueUUID = clientService.getBusinessIdByUUID(businessUUID);
                if (isTrueUUID > 0) {
                    resultMap = clientService.enableCsUserByBusinessId(businessUUID);
                    logger.info("response {}", resultMap);
                } else {
                    resultMap.put(Constants.SUCCESS, false);
                    resultMap.put(Constants.ERROR, "Provided businessUUID is invalid");
                }
            }
            else{
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide businessUUID mandatory field");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            return new ResponseEntity<String>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
        }
        logger.info("end client portal access request api");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }


    /**
     * deactivating cs user for given business.
     *
     * @param businessUUID
     * @return
     */
    @DeleteMapping("/{clientId}/access")
    @ResponseBody
    public ResponseEntity<String> deleteCsUserByBusinessId(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid clientId") String businessUUID) {
        logger.info("disabling cs user by businessId starts");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        ResponseEntity<String> responseEntity = null;
        try {
            if(Utils.isNotNull(businessUUID) && Utils.notEmptyString(businessUUID)) {
                int isTrueUUID = clientService.getBusinessIdByUUID(businessUUID);
                if (isTrueUUID > 0) {
                    resultMap = clientService.disableCsUserByBusinessId(businessUUID);
                    logger.info("response {}", resultMap);
                } else {
                    resultMap.put(Constants.SUCCESS, false);
                    resultMap.put(Constants.ERROR, "Provided businessUUID is invalid");
                }
            }
            else{
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide businessUUID mandatory field");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            return new ResponseEntity<String>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
        }
        logger.info("disabling cs user by businessId ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }


    /**
     * Update client portal access request
     *
     * @param clientPortalAccessBean
     * @return
     */
    @PostMapping("/csaccess")
    @ResponseBody
    public ResponseEntity<String> sendCsAccessRequest(@Valid @RequestBody ClientPortalAccessBean clientPortalAccessBean) {
        logger.info("client portal access request api starts");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        ResponseEntity<String> responseEntity = null;
        try {
            if(Utils.isNotNull(clientPortalAccessBean.getBusinessUUID())) {
                int isTrueUUID = clientService.getBusinessIdByUUID(clientPortalAccessBean.getBusinessUUID());
                if (isTrueUUID > 0) {
                    resultMap = clientService.sendCsAccessRequest(clientPortalAccessBean);
                    logger.info("response {}", resultMap);
                } else {
                    resultMap.put(Constants.SUCCESS, false);
                    resultMap.put(Constants.ERROR, "Provided businessUUID is invalid");
                }
            }
            else{
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide businessUUID mandatory field");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            return new ResponseEntity<String>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
        }
        logger.info("end client portal access request api");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Api to create cs user for given business user(king user) account
     * Updates cs user for business account
     *
     * @param clientPortalAccessBean
     * @return
     */
    @PostMapping("/grantaccess")
    @ResponseBody
    public ResponseEntity<String> createCsUser(@Valid @RequestBody ClientPortalAccessBean clientPortalAccessBean) {
        logger.info("cs user for given business account creation api starts");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        ResponseEntity<String> responseEntity = null;
        try {
            resultMap = clientService.updateCsUser(clientPortalAccessBean);
            JSONObject jsonObject = new JSONObject(resultMap);
            responseEntity = new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            return new ResponseEntity<String>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        logger.info("end cs user for given business account creation api ");
        return responseEntity;
    }

    /**
     * Update client king user information
     *
     * @param userUUID
     * @param userBean
     * @return
     */
    @PutMapping("user/{userId}")
    @ResponseBody
    //public ResponseEntity<String> updateKingUser(@PathVariable("userId") String userUUID, @RequestBody DtUserBean userBean) {
    public ResponseEntity<String> updateKingUser(@PathVariable("userId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid userId") String userUUID,
                                                 @RequestBody String userData) {
        logger.info("client business king user update begins");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(userUUID)) {
                int isUserId = clientService.getUserByUserUUID(userUUID);
                if (isUserId > 0) {
                    //userBean.setUserId(isUserId);
                    resultMap = clientService.updateKingUser(userUUID, isUserId, userData);
                    logger.info("response {}", resultMap);
                }
                else {
                    resultMap.put(Constants.SUCCESS, false);
                    resultMap.put(Constants.ERROR, "Provided userUUID is invalid");
                }
            }
            else {
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide userUUID mandatory field");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
        }
        logger.info("client business king user update ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }


    /**
     * Modify client king user information
     *
     * @param businessUUID
     * @param userBean
     * @return
     */
    @PutMapping("{clientId}/king")
    @ResponseBody
    //public ResponseEntity<String> modifyKingUser(@PathVariable("clientId") String businessUUID, @RequestBody DtUserBean userBean) {
    public ResponseEntity<String> modifyKingUser(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid clientId") String businessUUID,
                                                 @RequestBody String userData) {
        logger.info("client business king user modify begins");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(businessUUID)) {
                int isTrueUUID = clientService.getBusinessIdByUUID(businessUUID);
                //int isTrueUUID = clientService.retrieveBusinessIDByUUID(businessUUID);
                if (isTrueUUID > 0) {
                    resultMap = clientService.modifyKingUser(businessUUID, userData);
                    logger.info("response {}", resultMap);
                }
                else {
                    resultMap.put(Constants.SUCCESS, false);
                    resultMap.put(Constants.ERROR, "Provided businessUUID is invalid");
                }
            }
            else {
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide businessUUID mandatory field");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
        }
        logger.info("client business king user modify ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Delete client business information by businessUUID
     *
     * @param businessUUID
     * @return
     */
    @DeleteMapping("{clientId}")
    public ResponseEntity<String> deleteClient(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid clientId") String businessUUID) {
        logger.info("client business delete begins");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
            if (Utils.isNotNull(businessUUID)) {
                resultMap = clientService.deleteClientById(businessUUID);
                logger.info("response {}", resultMap);
            } else {
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide businessUUID mandatory field");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, "Error occurred");
        }
        logger.info("client business delete ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Return survey list for business
     *
     * @param businessUUID
     * @param state
     * @param pageId
     * @param search
     * @param sort
     * @param timeZone
     * @param language
     * @param exactMatch
     * @return
     * @throws IOException
     */
    @GetMapping("/{clientId}/programs")
    public ResponseEntity<Map<String, Object>> getSurveysList(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid clientId") String businessUUID,
                                                              @RequestParam(value = "state", defaultValue = "-1", required = false) String state,
                                                              @RequestParam(value = "page", defaultValue = "-1", required = false) String pageId,
                                                              @RequestParam(value = "search", defaultValue = "", required = false) String search,
                                                              @RequestParam(value = "sort", defaultValue = "created_time", required = false) String sort,
                                                              @RequestParam(value = "timezone", defaultValue = "America/Los_Angeles", required = false) String timeZone,
                                                              @RequestParam(value = "language", defaultValue = "en", required = false) String language,
                                                              @RequestParam(value = "exactMatch", defaultValue = "0", required = false) int exactMatch) throws IOException {
        logger.info("begin retrieving survey list for business");
        httpHeaders.clear();
        httpHeaders.add("Content-Type", "application/json; charset=utf-8");
        Map resultMap = new HashMap();
        try {
            List surveysList = clientService.getSurveysList(businessUUID, Integer.parseInt(pageId), search, sort, state, timeZone, language, exactMatch);
            resultMap.put(Constants.SUCCESS, Boolean.valueOf(true));
            resultMap.put(Constants.RESULT, surveysList);
            resultMap.put("totalPages", Utils.computeTotalPagesSurveyCard(clientService.getSurveysList(businessUUID, -1, search, sort, state, timeZone, language, exactMatch).size()));
            logger.info("end retrieving the surveys list");
            return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, Boolean.valueOf(false));
            logger.info("end retrieving the surveys list for business");
            return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * send password reset email for master cs user
     * @param businessUUID
     * @return
     */
    @PostMapping("/{clientId}/cspwdreset")
    @ResponseBody
    public ResponseEntity<String> sendResetPasswordEmail(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid clientId") String businessUUID) {
        logger.info("client portal reset password request api starts");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        ResponseEntity<String> responseEntity = null;
        try {
            if(Utils.isNotNull(businessUUID) && Utils.notEmptyString(businessUUID)) {
                int isTrueUUID = clientService.getBusinessIdByUUID(businessUUID);
                if (isTrueUUID > 0) {
                    resultMap = clientService.sendPasswordResetEmail(businessUUID);
                    logger.info("response {}", resultMap);
                } else {
                    resultMap.put(Constants.SUCCESS, false);
                    resultMap.put(Constants.ERROR, "Provided businessUUID is invalid");
                }
            }
            else{
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide businessUUID mandatory field");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            return new ResponseEntity<String>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
        }
        logger.info("end client portal reset password request api");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    /*@GetMapping("/{clientId}/templates")
    public void searchTemplates(@PathVariable("clientId") String businessUUID,HttpServletResponse response,
>>>>>>> sprint-33
                                @RequestParam(value = "search", required = false, defaultValue = "") String templateName,
                                @RequestParam(value = "templateType", required = false, defaultValue = "dropthought") String templateType,
                                @RequestParam(value = "sort", required = false, defaultValue = "created_time") String sortString,
                                @RequestParam(value = "pageNo", required = false, defaultValue = "-1") int pageNo,
                                @RequestParam(value = "exactMatch", required = false, defaultValue = "false") boolean exactMatch,
                                @RequestParam(value = "language", required = false, defaultValue = "en") String language,
                                @RequestParam(value = "timezone", required = false, defaultValue = "America/Los_Angeles") String timezone,
                                @RequestParam(value = "theme", required = false, defaultValue = "") String theme,
                                @RequestParam(value = "industry", required = false, defaultValue = "") String industry) throws Exception {
        logger.info("Search templates data api starts");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        response.addHeader("Content-Type", "application/json; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        ServletOutputStream servletOutputStream = response.getOutputStream();
        Map resultMap = new HashMap();
        try {
            resultMap = clientService.searchTemplateV2(templateName, templateType, sortString, pageNo, exactMatch, timezone, theme, industry);
            byte[] utf8JsonString = new JSONObject(resultMap).toString().getBytes("UTF-8");
            servletOutputStream.write(utf8JsonString);
            servletOutputStream.close();
            logger.info("Search templates data api ends");
        } catch (Exception e) {
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, "false");
            byte[] utf8JsonString = new JSONObject(resultMap).toString().getBytes("UTF-8");
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            servletOutputStream.write(utf8JsonString);
            servletOutputStream.close();
            logger.error(e.getMessage());
        }

    }*/

    /**
     *
     * Listing advanced dropthought templates
     * @param
     * @return
     */
    @GetMapping("/{clientId}/templates")
    public ResponseEntity<String> searchAdvancedTemplates(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid clientId") String businessUUID,
                                                          @RequestParam(value = "search", required = false, defaultValue = "") String templateName,
                                                          @RequestParam(value = "templateType", required = false, defaultValue = "dropthought") String templateType,
                                                          @RequestParam(value = "sortString", required = false, defaultValue = "templateName") String sortString,
                                                          @RequestParam(value = "pageNo", required = false, defaultValue = "-1") int pageNo,
                                                          @RequestParam(value = "exactMatch", required = false, defaultValue = "false") boolean exactMatch,
                                                          @RequestParam(value = "advanced", required = false, defaultValue = "-1") String advanced,
                                                          @RequestParam(value = "language", required = false, defaultValue = "en") String language,
                                                          @RequestParam(value = "timezone", required = false, defaultValue = "America/Los_Angeles") String timezone,
                                                          @RequestParam(value = "theme", required = false, defaultValue = "") String theme,
                                                          @RequestParam(value = "industry", required = false, defaultValue = "") String industry) throws Exception {
        logger.info("Search templates data api starts");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
            List resultList = clientService.searchAdvancedTemplateV2(businessUUID, templateName, templateType, sortString, pageNo, exactMatch, timezone, theme, industry, advanced, language);
            resultMap.put(Constants.SUCCESS, true);
            resultMap.put(Constants.RESULT, resultList);
            JSONObject jsonObject = new JSONObject(resultMap);
            return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, "false");
            JSONObject jsonObject = new JSONObject(resultMap);
            return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    /**
//     * Update templates
//     *
//     * @param
//     * @return
//     */
//    @PutMapping("{clientId}/templates")
//    @ResponseBody
//    public ResponseEntity<String> updateTemplates(@PathVariable("clientId") String businessUUID,
//                                                  @RequestParam(value = "templateUUID", defaultValue = "", required = true) String templateUUID,
//                                                  @RequestBody TemplateBean dtSurveyBean) {
//        logger.info("client business update begins");
//        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
//        ResponseEntity<String> responseEntity = null;
//        Map resultMap = new HashMap();
//        try {
//            if(Utils.isNotNull(businessUUID)) {
//                resultMap = clientService.updateTemplates(businessUUID,dtSurveyBean);
//                logger.info("response {}", resultMap);
//            }
//            else{
//                resultMap.put(Constants.SUCCESS, false);
//                resultMap.put(Constants.ERROR, " Provide businessUUID mandatory field");
//            }
//        }
//        catch (Exception e){
//            logger.error(e.getMessage());
//            logger.error(e.getMessage());
//            resultMap.put(Constants.SUCCESS, false);
//            resultMap.put(Constants.ERROR, ERROR_OCCURED);
//        }
//        logger.info("client business update ends");
//        JSONObject jsonObject = new JSONObject(resultMap);
//        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
//    }



    @GetMapping("/{clientId}/templatesByType")
    public ResponseEntity<String> getTemplatesByType(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid clientId") String businessUUID, HttpServletResponse response, @RequestParam(value = "type", defaultValue = "dropthought") String templateType,
                                                     @RequestParam(value = "language", defaultValue = "en") String language,
                                                     @RequestParam(value = "timezone", defaultValue = "America/Los_Angeles") String timeZone,
                                                     @RequestParam(value = "pageid", defaultValue = "1") int pageId) throws  IOException{
        httpHeaders.clear();
        httpHeaders.add("Content-Type", "application/json; charset=utf-8");
        logger.info("begin fetching the program template by type");

        ResponseEntity responseEntity = null;
        Map resultMap = new HashMap();
        ServletOutputStream servletOutputStream = response.getOutputStream();
        try {
            resultMap = clientService.getTemplatesByType(templateType,language,timeZone,pageId);
            logger.info("response {}", resultMap);


        } catch (Exception e) {
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

        }
        byte[] utf8JsonString = new JSONObject(resultMap).toString().getBytes("UTF-8");
        servletOutputStream.write(utf8JsonString);
        servletOutputStream.close();
        logger.info("end fetching the template by type");
        return responseEntity;

    }
    @GetMapping("/{clientId}/templates/{templateId}")
    public ResponseEntity<String> getProgramTemplateById(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid clientId") String businessUUID,HttpServletResponse response,
                                                         @PathVariable("templateId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid templateId") String templateUUID,
                                                         @RequestParam(value = "type", defaultValue = "dropthought") String type,
                                                         @RequestParam(value = "language", defaultValue = "en") String language,
                                                         @RequestParam(value = "timezone", defaultValue = "America/Los_Angeles") String timeZone) throws IOException {

        httpHeaders.clear();
        httpHeaders.add("Content-Type", "application/json; charset=utf-8");
        logger.info("begin fetching the program template by type");

        ResponseEntity responseEntity = null;
        Map resultMap = new HashMap();
        ServletOutputStream servletOutputStream = response.getOutputStream();
        try {
            resultMap = clientService.getTemplatesById(businessUUID, templateUUID,type,language,timeZone);
            logger.info("response {}", resultMap);


        } catch (Exception e) {
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

        }
        byte[] utf8JsonString = new JSONObject(resultMap).toString().getBytes("UTF-8");
        servletOutputStream.write(utf8JsonString);
        servletOutputStream.close();
        logger.info("end fetching the template by type");
        return responseEntity;

    }

    @PutMapping("/{clientId}/templates")
    public ResponseEntity<String> updateTemplatesByBusiness( @PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid clientId") String businessUUID, HttpServletResponse response,
                                                             @Valid @RequestBody TemplateUpdateBean templateUpdateBean) throws  IOException{
        httpHeaders.clear();
        httpHeaders.add("Content-Type", "application/json; charset=utf-8");
        logger.info("begin fetching the program template by type");

        ResponseEntity responseEntity = null;
        Map resultMap = new HashMap();
        ServletOutputStream servletOutputStream = response.getOutputStream();
        try {
            resultMap = clientService.updateTemplateByBusiness(businessUUID,templateUpdateBean);
            logger.info("response {}", resultMap);


        } catch (Exception e) {
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

        }
        byte[] utf8JsonString = new JSONObject(resultMap).toString().getBytes("UTF-8");
        servletOutputStream.write(utf8JsonString);
        servletOutputStream.close();
        logger.info("end fetching the template by type");
        return responseEntity;

    }

    /**
     * get api key by clientId
     * @param businessUUID
     * @return
     */
    @GetMapping("/{clientId}/key")
    @ResponseBody
    public ResponseEntity<String> getApiKeyByClient(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid clientId") String businessUUID) {
        logger.info("get client business details begins");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(businessUUID)) {
                int isTrueUUID = clientService.getBusinessIdByUUID(businessUUID);
                if (isTrueUUID > 0) {
                    String apiKey = clientService.getApiKeyByClientId(businessUUID);
                    if (Utils.notEmptyString(apiKey)) {
                        resultMap.put(Constants.SUCCESS, true);
                        Map responseMap = new HashMap();
                        responseMap.put("apiKey",apiKey);
                        resultMap.put(Constants.RESULT, responseMap);
                    } else {
                        resultMap.put(Constants.SUCCESS, true);
                        Map responseMap = new HashMap();
                        responseMap.put("message","api key not found");
                        responseMap.put("apiKey",apiKey);
                        resultMap.put(Constants.RESULT, responseMap);
                    }
                    logger.info("response {}", resultMap);
                } else {
                    resultMap.put(Constants.SUCCESS, false);
                    resultMap.put(Constants.ERROR, "Provided businessUUID is invalid");
                }

            }
            else{
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide businessUUID mandatory field");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
        }
        logger.info("get client business details ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * get api key by clientId
     * @param businessUUID
     * @return
     */
    @GetMapping("/{clientId}/ta/limits")
    @ResponseBody
    public ResponseEntity<String> getLimitsByClient(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid clientId") String businessUUID,
                                                    @RequestParam(value = "tap", required = false) String tap,
                                                    @RequestParam(value = "mlModelId", required = false) String mlModelId) {
        logger.info("get client limit details begins");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(businessUUID)) {
                int isTrueUUID = clientService.getBusinessIdByUUID(businessUUID);
                if (isTrueUUID > 0) {
                    Map limitMap = clientService.getClientLimitsByClientId(businessUUID,tap,mlModelId);
                    if (Utils.isNotNull(limitMap)) {
                        resultMap.put(Constants.SUCCESS, true);
                        Map responseMap = new HashMap();
                        String limitAllowed = limitMap.get("queriesAllowed").toString();
                        String limitRemaining = limitMap.get("queriesRemaining").toString();
                        String remainLimitPct = limitMap.get("queriesRemainingPct").toString();
                        Double usedLimitPct = 100-(Double.parseDouble(remainLimitPct.substring(0,remainLimitPct.length()-1)));
                        responseMap.put("limitAllowed",limitAllowed);
                        responseMap.put("limitRemaining",limitRemaining);
                        responseMap.put("limitRemainingPct",remainLimitPct);
                        responseMap.put("usedLimitPct",usedLimitPct+"%");
                        resultMap.put(Constants.RESULT, responseMap);
                    } else {
                        resultMap.put(Constants.SUCCESS, true);
                        Map responseMap = new HashMap();
                        responseMap.put("message","limits not found");
                        resultMap.put(Constants.RESULT, responseMap);
                    }
                    logger.info("response {}", resultMap);
                } else {
                    resultMap.put(Constants.SUCCESS, false);
                    resultMap.put(Constants.ERROR, "Provided businessUUID is invalid");
                }

            }
            else{
                resultMap.put(Constants.SUCCESS, false);
                resultMap.put(Constants.ERROR, " Provide businessUUID mandatory field");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.ERROR, Constants.ERROR_OCCURED);
        }
        logger.info("get client limit details ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Return survey list for business for given model
     *
     * @param businessUUID
     * @return
     * @throws IOException
     */
    @GetMapping("/{clientId}/programs/cs")
    public ResponseEntity<Map<String, Object>> getSurveysListCS(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid clientId") String businessUUID,
                                                              @RequestParam(value = "model", defaultValue = "", required = false) String model) throws IOException {
        logger.info("begin retrieving survey list for portal business");
        httpHeaders.clear();
        httpHeaders.add("Content-Type", "application/json; charset=utf-8");
        Map resultMap = new HashMap();
        try {
            List surveysList = clientService.getSurveysListForPortal(businessUUID, model);
            resultMap.put(Constants.SUCCESS, Boolean.valueOf(true));
            resultMap.put(Constants.RESULT, surveysList);
            resultMap.put("totalRecords", surveysList.size());
            logger.info("end retrieving the surveys list for portal");
            return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, Boolean.valueOf(false));
            logger.info("end retrieving the surveys list for portal business");
            return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Listing custom themes
     *
     * @param
     * @return
     */
    @GetMapping("/{clientId}/themes")
    public ResponseEntity<String> getCustomThemes(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid clientId") String businessUUID,
                                                  @RequestParam(value = "search", required = false, defaultValue = "") String themeName,
                                                  @RequestParam(value = "sortString", required = false, defaultValue = "createdTime") String sortString,
                                                  @RequestParam(value = "pageNo", required = false, defaultValue = "-1") int pageNo,
                                                  @RequestParam(value = "exactMatch", required = false, defaultValue = "false") boolean exactMatch,
                                                  @RequestParam(value = "language", required = false, defaultValue = "en") String language,
                                                  @RequestParam(value = "timezone", required = false, defaultValue = "America/Los_Angeles") String timezone) throws Exception {
        logger.info("Listing custom themes api starts");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
            List resultList = clientService.getCustomThemesByBusinessId(businessUUID,
                    themeName,
                    sortString,
                    pageNo,
                    exactMatch,
                    timezone,
                    language);
            resultMap.put(Constants.SUCCESS, true);
            resultMap.put(Constants.RESULT, resultList);
            JSONObject jsonObject = new JSONObject(resultMap);
            logger.info("Listing custom themes api ends");
            return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, "false");
            JSONObject jsonObject = new JSONObject(resultMap);
            return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update themes by business
     *
     * @param businessUUID
     * @param response
     * @param themesBean
     * @return
     * @throws IOException
     */
    @PutMapping("/{clientId}/themes")
    public ResponseEntity<String> updateThemesByBusiness(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid clientId") String businessUUID, HttpServletResponse response,
                                                         @Valid @RequestBody ThemesBean themesBean) throws IOException {
        httpHeaders.clear();
        httpHeaders.add("Content-Type", "application/json; charset=utf-8");
        logger.info("begin updating themes by business");
        ResponseEntity responseEntity = null;
        Map resultMap = new HashMap();
        ServletOutputStream servletOutputStream = response.getOutputStream();
        try {
            resultMap = clientService.updateThemesBybusiness(businessUUID, themesBean);
            logger.info("end updating themes by business");
        } catch (Exception e) {
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
        byte[] utf8JsonString = new JSONObject(resultMap).toString().getBytes("UTF-8");
        servletOutputStream.write(utf8JsonString);
        servletOutputStream.close();
        return responseEntity;
    }

    /**
     * Update themes by business
     *
     * @param businessUUID
     * @param response
     * @param themesBean
     * @return
     * @throws IOException
     */
    @PutMapping("/apikey")
    public ResponseEntity<String> updateApkeyByBusiness(HttpServletResponse response) throws IOException {
        httpHeaders.clear();
        httpHeaders.add("Content-Type", "application/json; charset=utf-8");
        logger.info("begin updating apikey by business");
        ResponseEntity responseEntity = null;
        Map resultMap = new HashMap();
        ServletOutputStream servletOutputStream = response.getOutputStream();
        try {
            resultMap = clientService.updateAPIKeyIfNotAvailable();
            logger.info("end updating themes by business");
        } catch (Exception e) {
            logger.error(e.getMessage());
            resultMap.put(Constants.SUCCESS, false);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
        byte[] utf8JsonString = new JSONObject(resultMap).toString().getBytes("UTF-8");
        servletOutputStream.write(utf8JsonString);
        servletOutputStream.close();
        return responseEntity;
    }

    //PUT method to change Enterprise user to Dt user and vise versa. accountType should be either "Enterprise" or "DTLite"
    @PutMapping("/{clientId}/changeaccount")
    public ResponseEntity<String> changeAccount(@PathVariable("clientId") String clientId,
                                                @RequestParam(value = "accountType", required = true) String accountType) {

        logger.info("changeAccount() - REST API starts");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        ResponseEntity<String> responseEntity = null;
        try {
            if (Utils.emptyString(clientId)) {
                resultMap.put("status", false);
                resultMap.put("message", "clientId is required");
                responseEntity = new ResponseEntity<>(new JSONObject(resultMap).toString(),
                        httpHeaders,
                        HttpStatus.OK);
                return responseEntity;
            }

            resultMap = clientService.changeAccount(clientId, accountType);
            responseEntity = new ResponseEntity<>(new JSONObject(resultMap).toString(),
                    httpHeaders,
                    HttpStatus.OK);

        } catch (Exception e) {
            logger.error("Exception in changeAccount() - REST API", e);
            resultMap.put("status", false);
            resultMap.put("message", "Internal server error");
            responseEntity = new ResponseEntity<>(new JSONObject(resultMap).toString(),
                    httpHeaders,
                    HttpStatus.OK);
        }
        logger.info("changeAccount() - REST API ends");

        return responseEntity;
    }

}
