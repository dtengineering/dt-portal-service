package com.dropthought.portal.controller;


import com.dropthought.portal.model.BusinessSchedule;
import com.dropthought.portal.service.SchedulerService;
import com.dropthought.portal.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;
import javax.validation.constraints.Positive;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Profile("worker")
@RestController
@RequestMapping("/api/v1/portal/schedule")
@Validated
public class ScheduleController {

    private static final Logger logger = LoggerFactory.getLogger(ScheduleController.class);

    //@Autowired
    //ScheduleRunner scheduleRunner;

    @Autowired
    SchedulerService schedulerService;

    // instantiating a validator factory
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    // instantiating a validator object
    Validator validator = factory.getValidator();


    /**
     * returns the version of the service
     *
     * @return Map(version, major.minor.patch)
     */
    @GetMapping("/version")
    public Map<String, String> version() {
        return Collections.singletonMap("VERSION", "1.0.0");
    }

    @PostMapping("/business")
    @ResponseBody
    public Map createBusinessSchedule(@Valid @RequestBody BusinessSchedule businessSchedule) {
        Map result = new HashMap();
        boolean isValid = true;
        try {
            // validating the event bean via validator object defined in the event bean
            Set<ConstraintViolation<BusinessSchedule>> violations = validator.validate(businessSchedule);
            // iterating all the voilations and send the error message as api response
            for (ConstraintViolation<BusinessSchedule> violation : violations) {
                // assigning failure status & validation error message to result
                result.put(Utils.SUCCESS, false);
                result.put(Utils.ERROR, violation.getMessage());
                // if validations fails assigning false to isValid variable.
                isValid = false;
            }
            if (isValid) {
                int status = 0;
                if(!schedulerService.checkIfBusinessExistsInSchedule(businessSchedule.getBusinessId())) {
                    status = schedulerService.createBusinessSchedule(businessSchedule);
                }else{
                    status = schedulerService.updateBusinessSchedule(businessSchedule);
                }
                if(status > 0) {
                    result.put(Utils.SUCCESS,true);
                }else{
                    result.put(Utils.SUCCESS, false);
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            result.put(Utils.SUCCESS, (false));
        }
        return result;
    }

    /**
     *
     * @param businessSchedule
     * @return
     */
    @PutMapping("/business")
    @ResponseBody
    public Map updateBusinessSchedule(@Valid @RequestBody BusinessSchedule businessSchedule) {
        Map result = new HashMap();
        boolean isValid = true;
        try {
            // validating the event bean via validator object defined in the event bean
            Set<ConstraintViolation<BusinessSchedule>> violations = validator.validate(businessSchedule);
            // iterating all the voilations and send the error message as api response
            for (ConstraintViolation<BusinessSchedule> violation : violations) {
                // assigning failure status & validation error message to result
                result.put(Utils.SUCCESS, false);
                result.put(Utils.ERROR, violation.getMessage());
                // if validations fails assigning false to isValid variable.
                isValid = false;
            }
            if (isValid) {
                int status = schedulerService.updateBusinessSchedule(businessSchedule);
                if(status > 0) {
                    result.put(Utils.SUCCESS,true);
                }else{
                    result.put(Utils.SUCCESS, false);
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            result.put(Utils.SUCCESS, (false));
        }
        return result;
    }

    /**
     * api to deactivete users in keycloak/okta for expired business
     * @param businessId
     * @return
     */
    @PutMapping("/business/{businessId}")
    @ResponseBody
    public Map deactivateUsersByBusiness(@PathVariable @Positive(message = "Invalid businessId") Integer businessId) {
        Map result = new HashMap();
        try {
            if (businessId != null && businessId > 0) {
                String msg = schedulerService.deactivateUsersByBusiness(businessId);
                result.put(Utils.SUCCESS, true);
                result.put("message", msg);
            }else{
                result.put(Utils.SUCCESS, false);
                result.put("error", "businessId is mandatory");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            result.put(Utils.SUCCESS, (false));
        }
        return result;
    }

}
