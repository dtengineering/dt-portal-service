package com.dropthought.portal.controller;

import com.dropthought.portal.model.MessageBean;
import com.dropthought.portal.model.UserBean;
import com.dropthought.portal.service.WebUserAutenticationService;
import com.dropthought.portal.util.UrlPath;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/api/v1/portal/web")
@Validated
public class WebController {
    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Http Headers
     */
    protected final HttpHeaders httpHeaders = new HttpHeaders();

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private WebUserAutenticationService webUserAutenticationService;


    @RequestMapping(value = UrlPath.LOGIN, method = {RequestMethod.POST}, produces = "application/json")
    public ResponseEntity<String> loginUser(@RequestBody String data) {
        List list = new ArrayList();
        UserBean userBean = null;
        Map resultMap = new HashMap();
        try {

            HashMap<String, String> userDataMap = mapper.readValue(data, HashMap.class);
            String email = userDataMap.get("email");
            String password = userDataMap.get("password");
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            logger.info("User authentication rest api starts");
            list = webUserAutenticationService.authenticateUser(email, password);
            if (list.size() > 0) {

                userBean = (UserBean) list.get(0);
                int userStatus;
                String userStatusString = userBean.getUserStatus();
                userStatus = Integer.parseInt(userStatusString);
                if (userStatus == 1) {
                    logger.info("User confirmed");
                    Map userMap = new HashMap();
                    userMap.put("userStatus", userStatusString);
                    userMap.put("userUUID", userBean.getUserUUID());
                    resultMap.put("success", true);
                    resultMap.put("result", userMap);
                    JSONObject jsonObject = new JSONObject(resultMap);
                    logger.info("User authentication rest api ends");
                    return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
                } else {
                    logger.info("User not active");
                    Map responseMap = new HashMap();
                    responseMap.put("success", false);
                    responseMap.put("error", "User not active");
                    JSONObject jsonObject = new JSONObject(responseMap);
                    return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
                }
            } else {
                logger.info("User not found");
                Map responseMap = new HashMap();
                responseMap.put("success", false);
                responseMap.put("message", "Invalid username or password");
                JSONObject jsonObject = new JSONObject(responseMap);
                return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.UNAUTHORIZED);
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity<String>(new MessageBean(false).toString(), httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
