package com.dropthought.portal.controller;

import com.dropthought.portal.common.DTConstants;
import com.dropthought.portal.model.TextAnalyticsApprovalBean;
import com.dropthought.portal.model.TextAnalyticsConfigBean;
import com.dropthought.portal.model.TextAnalyticsRequestBean;
import com.dropthought.portal.service.TextAnalyticsConfigurationService;
import com.dropthought.portal.util.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(path = "/api/v1/portal/textAnalytics")
@Validated
public class TextAnalyticsConfigController {

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Http Headers
     */
    protected final HttpHeaders httpHeaders = new HttpHeaders();
    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();


    @Autowired
    TextAnalyticsConfigurationService textAnalyticsConfigurationService;

    /**
     * Get business text analytics by configUniqueId
     *
     * @param configUniqueId
     * @return
     */
    @GetMapping("{configId}")
    @ResponseBody
    public ResponseEntity<String> getTAConfigByConfigUUID(@PathVariable("configId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid configId") String configUniqueId) {
        logger.info("get client business config details begins");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
            if (Utils.isNotNull(configUniqueId)) {
                TextAnalyticsConfigBean response = textAnalyticsConfigurationService.getTextAnalyticsConfig(configUniqueId);
                if (response != null) {
                    resultMap.put("success", true);
                    resultMap.put("result", response);
                } else {
                    resultMap.put("success", true);
                    resultMap.put("result", new ArrayList<>());
                }
                logger.info("response {}", resultMap);

            } else {
                resultMap.put("success", false);
                resultMap.put("error", " Provide configUniqueId mandatory field");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            resultMap.put("success", false);
            resultMap.put("error", "Error occurred.");
        }
        logger.info("get client business config details ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Create text analytics configurations
     *
     * @param configBean
     * @return
     */
    @PostMapping()
    @ResponseBody
    public ResponseEntity<String> createTextAnalyticsConfig(@Valid @RequestBody TextAnalyticsConfigBean configBean) {
        logger.info("begins client business text analytics config creation");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(configBean)) {
                Integer insertStatus = textAnalyticsConfigurationService.createTextAnalyticsConfig(configBean, 0);
                if (insertStatus > 0) {
                    TextAnalyticsConfigBean response = textAnalyticsConfigurationService.getTextAnalyticsConfig(insertStatus);
                    if (response != null) {
                        //DTV-9719 - update the text analytics config id to business_configurations and business_configurations_version tables
                        textAnalyticsConfigurationService.updateConfigIdToBusiness(insertStatus, configBean.getBuinessUniqueId());
                        resultMap.put("success", true);
                        resultMap.put("result", response);
                    } else {
                        resultMap.put("success", false);
                        resultMap.put("result", new ArrayList<>());
                    }
                }else {
                    resultMap.put("success", true);
                    resultMap.put("result", new ArrayList<>());
                }
            }
            else{
                resultMap.put("success", false);
                resultMap.put("error", " Provide mandatory fields");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put("success", false);
            resultMap.put("error", "Error occurred.");
        }
        logger.info("create client business text analytics config ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Update client business configuration information by businessUUID
     *
     * @param configUniqueId
     * @param configBean
     * @return
     */
    @PutMapping("{configId}")
    @ResponseBody
    public ResponseEntity<String> updateTextAnalyticsConfig(@PathVariable("configId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid configId") String configUniqueId,
                                                            @Valid @RequestBody TextAnalyticsConfigBean configBean) {
        logger.info("client text analytics config update begins");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(configUniqueId)) {
                int updateStatus = textAnalyticsConfigurationService.updateTextAnalyticsPlan(Integer.parseInt(configBean.getTextAnalyticsPlan()), configBean.getTextAnalyticsEnabled(), configUniqueId);
                if (updateStatus > 0) {
                  TextAnalyticsConfigBean  response = textAnalyticsConfigurationService.getTextAnalyticsConfig(configUniqueId);
                    if (response != null) {
                        resultMap.put("success", true);
                        resultMap.put("result", response);
                    } else {
                        resultMap.put("success", false);
                        resultMap.put("result", new ArrayList<>());
                    }
                }
                logger.info("response {}", resultMap);
            }
            else{
                resultMap.put("success", false);
                resultMap.put("error", " Provide ClientUniqueId mandatory field");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put("success", false);
            resultMap.put("error", "Error occurred.");
        }
        logger.info("client text analytics configuration update ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Update client business configuration information by businessUUID
     *
     * @param approvalBean
     * @return
     */
    @PostMapping("approval")
    @ResponseBody
    public ResponseEntity<String> updateTextAnalyticsApproval(@Valid @RequestBody TextAnalyticsApprovalBean approvalBean) {
        logger.info("client text analytics config update begins");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
          /*  // initializing variable to be true
            boolean isValid = true;
            // instantiating a validator factory
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            // instantiating a validator object
            Validator validator = factory.getValidator();
            // validating the TextAnalyticsApprovalBean bean via validator object defined in the TextAnalyticsApprovalBean bean
            Set<ConstraintViolation<TextAnalyticsApprovalBean>> violations = validator.validate(approvalBean);
            // iterating all the voilations and send the error message as api response
            for (ConstraintViolation<TextAnalyticsApprovalBean> violation : violations) {
                // if validations fails assigning false to isValid variable.
                isValid = false;
                // assigning failure status & validation error message to result
                resultMap.put("success", isValid);
                resultMap.put("error", violation.getMessage());
            }

            if (isValid) {*/
                int updateStatus = textAnalyticsConfigurationService.updateTAConfigApprovalStatus(approvalBean);
                if (updateStatus > 0) {
                    resultMap.put("success", true);
                    resultMap.put("result", updateStatus);
                } else {
                    resultMap.put("success", false);
                }
                logger.info("response {}", resultMap);
           /* } else {
                resultMap.put("success", false);
                resultMap.put("error", " Provide ClientUniqueId mandatory field");
            }*/
        }
        catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put("success", false);
            resultMap.put("error", "Error occurred.");
        }
        logger.info("client text analytics configuration update ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Get approver status by configUniqueId and emailId
     *
     * @param configUniqueId
     * @return
     */
    @GetMapping("approval/id/{uuid}/approver/{email}")
    @ResponseBody
    public ResponseEntity<String> getApproverStatusConfigByClintUUID(@PathVariable("uuid") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid uuid") String configUniqueId,
                                                                     @PathVariable("email") @NotBlank(message = "email should be valid value") String approverEmail) {
        logger.info("get approver status for a ta config begins");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            if (Utils.notEmptyString(configUniqueId) && Utils.notEmptyString(approverEmail)) {
                String decodedEmail = Utils.decodeString(approverEmail);
                boolean response = textAnalyticsConfigurationService.getTextAnalyticsConfigStatusByEmail(configUniqueId, decodedEmail);
                resultMap.put("success", response);
                logger.info("response {}", resultMap);
            } else {
                resultMap.put("success", false);
                resultMap.put("error", " Provide configUniqueId mandatory field");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put("success", false);
            resultMap.put("error", "Error occurred.");
        }
        logger.info("get approver status for a ta config ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Create text analytics Service to request for approval
     *
     * @param requestBean
     * @return
     */
    @PostMapping("request")
    @ResponseBody
    public ResponseEntity<String> createTextAnalyticsConfigRequest(@Valid @RequestBody TextAnalyticsRequestBean requestBean) {
        logger.info("begins client business text analytics request for approval");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(requestBean) && Utils.isNotNull(requestBean.getBusinessId())) {
                String configUUID = textAnalyticsConfigurationService.createTextAnalyticsRequestConfig(requestBean);
                if (configUUID.length() > 0) {
                    TextAnalyticsConfigBean response = textAnalyticsConfigurationService.getTextAnalyticsConfig(configUUID);
                    if (response != null) {
                        resultMap.put("success", true);
                        resultMap.put("result", response);
                    } else {
                        resultMap.put("success", false);
                        resultMap.put("result", new ArrayList<>());
                    }
                }else {
                    resultMap.put("success", true);
                    resultMap.put("result", new ArrayList<>());
                }
            }
            else{
                resultMap.put("success", false);
                resultMap.put("error", " Provide mandatory fields");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put("success", false);
            resultMap.put("error", "Error occurred.");
        }
        logger.info("create client business text analytics request ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }


    /**
     * Get business text analytics by clientUniqueId
     *
     * @param clientUniqueId
     * @return
     */
    @GetMapping("request/client/{clientId}")
    @ResponseBody
    public ResponseEntity<String> getTAConfigByClientUniqueId(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid clientId") String clientUniqueId) {
        logger.info("get client business config details begins");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
            if (Utils.isNotNull(clientUniqueId)) {
                TextAnalyticsConfigBean response = textAnalyticsConfigurationService.getTextAnalyticsConfigByClientUUID(clientUniqueId);
                if (response != null) {
                    resultMap.put("success", true);
                    resultMap.put("result", response);
                } else {
                    resultMap.put("success", true);
                    resultMap.put("result", new ArrayList<>());
                }
                logger.info("response {}", resultMap);

            } else {
                resultMap.put("success", false);
                resultMap.put("error", " Provide clientUniqueId mandatory field");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put("success", false);
            resultMap.put("error", "Error occurred.");
        }
        logger.info("get client business config details ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

}
