package com.dropthought.portal.controller;

import com.dropthought.portal.common.DTConstants;
import com.dropthought.portal.model.UserActivityBean;
import com.dropthought.portal.model.UserLogBean;
import com.dropthought.portal.service.UserLogService;
import com.dropthought.portal.util.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/api/v1/portal/log")
@Validated
public class UserLogController {
    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Http Headers
     */
    protected final HttpHeaders httpHeaders = new HttpHeaders();

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private UserLogService userLogService;


    @PostMapping()
    @ResponseBody
    public ResponseEntity<String> createUserActivity(@Valid @RequestBody UserLogBean userLogBean) {
        logger.info("begin user activity creation");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            if (Utils.isNotNull(userLogBean)) {
                resultMap = userLogService.saveLogs(userLogBean);
                logger.info("response {}", resultMap);
            } else {
                resultMap.put("success", false);
                resultMap.put("error", " Provide mandatory fields");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put("success", false);
            resultMap.put("error", "Error occurred.");
        }
        logger.info("create user activity ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    @PostMapping("/client/{clientId}")
    @ResponseBody
    public ResponseEntity<String> getUserActivity(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid clientId") String clientId,
                                                  @Valid @RequestBody UserActivityBean userActivityBean) {
        logger.info("get user activities");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            if (Utils.notEmptyString(clientId)) {
                List role = userActivityBean.getRole();
                List userId = userActivityBean.getUserId();
                List location = userActivityBean.getLocation();
                List activity = userActivityBean.getActivity();
                List userEmail = userActivityBean.getUserEmail();
                List username = userActivityBean.getUsername();
                String fromDate = userActivityBean.getFromDate();
                String toDate = userActivityBean.getToDate();
                int pageId = userActivityBean.getPageId() > 0 ? userActivityBean.getPageId() : -1;
                String timeZone = Utils.isNotNull(userActivityBean.getTimeZone()) ? userActivityBean.getTimeZone() : "America/Los_Angeles";

                List activityLogs = userLogService.getLogs(clientId, role, userId, location, activity, userEmail, username, fromDate, toDate, pageId, timeZone);
                resultMap.put("logs", activityLogs);
                resultMap.put("totalLogs", activityLogs.size());
                resultMap.put("success", true);
            } else {
                resultMap.put("success", false);
                resultMap.put("error", " Provide mandatory fields - client Id");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put("success", false);
            resultMap.put("error", "Error occurred.");
        }
        logger.info("end get user activities");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    @GetMapping("/client/{clientId}/filter")
    @ResponseBody
    public ResponseEntity<String> getUserActivityFilterDropdown(@PathVariable("clientId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid clientId") String clientId) {
        logger.info("get filter dropdown");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            if (Utils.notEmptyString(clientId)) {
                Map filterDropdown = userLogService.getFilterDropdown(clientId);
                resultMap.put("result", filterDropdown);
                resultMap.put("success", true);
            } else {
                resultMap.put("success", false);
                resultMap.put("error", " Provide mandatory fields - client Id");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put("success", false);
            resultMap.put("error", "Error occurred.");
        }
        logger.info("end get user activities");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }


}
