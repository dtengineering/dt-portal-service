package com.dropthought.portal.controller;


import com.dropthought.portal.common.DTConstants;
import com.dropthought.portal.model.MessageBean;
import com.dropthought.portal.model.UserBean;
import com.dropthought.portal.service.UserAuthenticationService;
import com.dropthought.portal.service.UserService;
import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.TokenUtility;
import com.dropthought.portal.util.UrlPath;
import com.dropthought.portal.util.Utils;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Prabhakar Thakur on 3/7/2020.
 */
@RestController
@RequestMapping(path = "/api/v1/portal/user")
@Validated
public class UserController {

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Http Headers
     */
    protected final HttpHeaders httpHeaders = new HttpHeaders();

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private UserService userService;

    @Autowired
    TokenUtility tokenUtility;

    @Autowired
    UserAuthenticationService userAuthenticationService;


    @RequestMapping(method = {RequestMethod.POST}, consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> createUser(@Valid @RequestBody UserBean userBean) {
        List list = new ArrayList();
        try {

            Integer userId = 0;
            //logger.info("Create user rest api start");
            String host = userBean.getHost();
            host = host.contains("localhost") ? "http://localhost:4201/en" : host;
            String fullName = userBean.getFullName();
            logger.debug("Create user rest api start");
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            if (userBean == null) {
                return new ResponseEntity<String>(new MessageBean(false).toString(), httpHeaders, HttpStatus.BAD_REQUEST);
            }
            String userEmail = userBean.getUserEmail();

            if (userService.checkMandatoryUserValues(userBean)) {

                userId = userService.checkExistingUser(userEmail);
                //logger.info("User id " + userId);
                if (userId == 0) {
                    //logger.info("User with this email does not exist");
                    boolean generatePassword = false;
                    if (userBean.getPassword() == null) {
                        generatePassword = true;

                    } else if (userBean.getPassword().length() == 0) {
                        generatePassword = true;
                    }

                    if (generatePassword) {
                        //logger.info("Generating random password for the user");
                        logger.debug("Generating random password for the user");
                        String pasword = tokenUtility.generateRandomString(10);
                        //logger.info("Generated password " + pasword);
                        logger.debug("Generated password " + pasword);
                        userBean.setPassword(pasword);
                    }

                    userId = userService.createUser(userBean);
                    //logger.info("User id " + userId);
                    userBean = userAuthenticationService.getUserByEmail(userEmail);
                    userBean.setHost(host);
                    userBean.setFullName(fullName);
                    userBean.setUserId(userId);
                    if (userBean.getUserId() > 0) {
                        userService.updateUserStatusByEmail(userBean);
                    }
                }
                else {
                    //logger.info("User with this email exist already");
                    MessageBean messageBean=new MessageBean(false);
                    messageBean.setStatus(Constants.FAILED);
                    messageBean.setMessage(Constants.USER_EXIST_ALREADY);
                    return new ResponseEntity<String>(messageBean.toString(), httpHeaders, HttpStatus.BAD_REQUEST);
                }
                list.add(userBean);
                //logger.info("Create user rest api ends");
                MessageBean messageBeanlist=new MessageBean(list);
                messageBeanlist.setStatus(Constants.SUCCESS);
                messageBeanlist.setMessage(Constants.USER_ADDED);
                //return new ResponseEntity<String>(new MessageBean(true, list).toString(), httpHeaders, HttpStatus.OK);
                return new ResponseEntity<String>(messageBeanlist.toString(), httpHeaders, HttpStatus.OK);

            } else {

                throw new Exception("Please check the input");
            }


        }
        catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity<String>(new MessageBean(false).toString(), httpHeaders, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * rest api for listing all Users .
     *
     * @return json response
     */

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public void getUsers(HttpServletResponse response) throws IOException {
        Map resultMap = new HashMap();
        byte[] utf8JsonString = null;
        response.addHeader("Content-Type","application/json; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        ServletOutputStream servletOutputStream = response.getOutputStream();
        try {
            resultMap.put("success",true);
            resultMap.put("result",userService.getUser());
            response.setStatus(HttpServletResponse.SC_OK);

        } catch (Exception e) {
            logger.error(e.getMessage());
            resultMap.put("message","internal server error");
            resultMap.put("success",false);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
        utf8JsonString = new JSONObject(resultMap).toString().getBytes("UTF-8");
        servletOutputStream.write(utf8JsonString);
        servletOutputStream.close();
    }


    @RequestMapping(value = UrlPath.USERUUID, method = RequestMethod.GET, produces = "application/json")
    public void getUserById(HttpServletResponse response, @PathVariable("userUUID") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid userUUID") String userUUID) throws IOException {

        response.addHeader("Content-Type","application/json; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        ServletOutputStream servletOutputStream = response.getOutputStream();
        Map resultMap = new HashMap();
        byte[] utf8JsonString = null;
        try {
            List list = userService.getUserByUUID(userUUID);
            if (list.size() > 0) {
                resultMap.put("success",true);
                resultMap.put("result",list);
                response.setStatus(HttpServletResponse.SC_OK);
            } else {
                resultMap.put("message","not found");
                resultMap.put("success",false);
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            resultMap.put("success",false);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
        utf8JsonString = new JSONObject(resultMap).toString().getBytes("UTF-8");
        servletOutputStream.write(utf8JsonString);
        servletOutputStream.close();
    }

    @RequestMapping(value = UrlPath.USERID, method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<String> deleteUserById(@PathVariable("userId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid userUUId") String userById) {
        boolean status = false;
        ResponseEntity<String> responseEntity = null;
        int userId=Integer.parseInt(userById);
        try {
            status = userService.deleteUser(userId);
            //logger.info("Status is "+status);
            if (status == false) {
                MessageBean messageBeanStatus=new MessageBean(false);
                messageBeanStatus.setStatus(Constants.FAILED);
                messageBeanStatus.setMessage(Constants.USER_DELETED_ALREADY);
                responseEntity = new ResponseEntity<String>(messageBeanStatus.toString(), httpHeaders, HttpStatus.NOT_FOUND);
            } else {
                MessageBean messageBean=new MessageBean();
                messageBean.setStatus(Constants.SUCCESS);
                messageBean.setMessage(Constants.USER_DELETED);
                responseEntity = new ResponseEntity<String>(messageBean.toString(), httpHeaders, HttpStatus.OK);
                //responseEntity = new ResponseEntity<String>(new MessageBean(true).toString(), httpHeaders, HttpStatus.OK);
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity<String>(new MessageBean(false).toString(), httpHeaders, HttpStatus.BAD_REQUEST);
        }

        return responseEntity;


    }

    /**
     * rest api for updating user by userId .
     *
     * @param userById
     * @param data
     * @return json response
     */

    @RequestMapping(value = UrlPath.USERID, method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> updateUserById(@PathVariable("userId") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid userId") String userById, @RequestBody String data) {

        Map resultMap = new HashMap();
        List userList = new ArrayList();
        int userId=Integer.parseInt(userById);

        resultMap.put("success", false);
        try {
            logger.info("update user by ID rest api start");
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            if (data.isEmpty()) {
                return new ResponseEntity<String>(new MessageBean(false).toString(), httpHeaders, HttpStatus.BAD_REQUEST);
            }

            UserBean userBean = mapper.readValue(data, UserBean.class);
            int status = userService.updateUserById(userId, userBean);
            List<Map> list = new ArrayList<Map>();
            ObjectMapper mapper = new ObjectMapper();
            Map o = mapper.readValue(data, new TypeReference<Map<String, String>>() {
            });
            list = userService.getUserById(userId);
            //logger.info("user password is " + userBean.getPassword());

            //logger.info("update user by ID rest api end");
            MessageBean messageBeanSuccesslist=new MessageBean(list);
            messageBeanSuccesslist.setStatus(Constants.SUCCESS);
            messageBeanSuccesslist.setMessage(Constants.USER_UPDATED);
            MessageBean messageBeanFailurelist=new MessageBean(false, list);
            messageBeanFailurelist.setStatus(Constants.FAILED);
            messageBeanFailurelist.setMessage(Constants.USER_NOT_EXIST);
            return ((status == 1) ? new ResponseEntity<String>(messageBeanSuccesslist.toString(), httpHeaders, HttpStatus.OK) : new ResponseEntity<String>(messageBeanFailurelist.toString(), httpHeaders, HttpStatus.BAD_REQUEST));

        } catch (JsonParseException e) {
            //logger.error(e.getMessage());
            resultMap.put("error", "Json parse exception");
            return new ResponseEntity<String>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.BAD_REQUEST);

        } catch (JsonMappingException e) {
            //logger.error(e.getMessage());
            resultMap.put("error", "Json mapping exception");
            return new ResponseEntity<String>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            //logger.error(e.getMessage());
            resultMap.put("error", "IO Exception");
            return new ResponseEntity<String>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            //logger.error(e.getMessage());
            resultMap.put("error", "Please check the input parameters");
            return new ResponseEntity<String>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.BAD_REQUEST);
        }
    }


    /**
     * rest api to send password reset email
     *
     * @param token
     * @return json response
     */
    @RequestMapping(value = UrlPath.RESETPASSWORD, method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> resetPasswordEmail(@RequestParam("token") @NotBlank(message = "token should not be null or empty") String token,
                                                     @RequestParam("host") @NotBlank(message = "host should not be null or empty") String host,
                                                     @RequestParam(value = "language", defaultValue = "en") String language) {
        Map resultMap = new HashMap();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        try {
            logger.info("user reset password begins");
            UserBean userBean = userAuthenticationService.getUserByEmail(token);
            if (Utils.isNull(userBean.getUserId())) {
                resultMap.put("success", false);
                resultMap.put("error", "No user found");
                return new ResponseEntity<String>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
            } else {
                int userId = userBean.getUserId();
                logger.info("userId " + userId);
                logger.info("Reset password Email REST api start");
                logger.info("host" + host);
                int result = userAuthenticationService.sendResetEmail(userBean, host, language);
                if (result > 0) {
                    resultMap.put("success", true);
                    resultMap.put("message", "mail sent successfully");
                    return new ResponseEntity<String>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
                } else {
                    resultMap.put("success", false);
                    resultMap.put("message", "mail not sent successfully");
                    return new ResponseEntity<String>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.NOT_FOUND);
                }
            }
        } catch (Exception e) {
            resultMap.put("success", false);
            resultMap.put("message", "Please check the input parameters");
            resultMap.put("error", "Internal server error");
            return new ResponseEntity<String>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.BAD_REQUEST);
        }
    }


    /**
     * rest api for password reset update
     *
     * @param userBean
     * @return json response
     */
    @RequestMapping(value = UrlPath.RESETPASSWORD, method = {RequestMethod.POST}, consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> updateUser(@Valid @RequestBody UserBean userBean) {
        Map resultMap = new HashMap();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        try {
            logger.info("reset password update rest api start");
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);

            String modifiedByUUID = userBean.getModifiedBy();
            logger.info("Modified by uuid " + modifiedByUUID);
            List userList = userService.getUserByUUID(modifiedByUUID);
            if(userList.size() > 0){
                Map userMap = (Map) userList.get(0);
                Integer userId = (int) userMap.get("user_id");
                logger.info("User id " + userId);
                userBean.setUserId(userId);
                int status = userService.updateUserById(userId, userBean);
                if (status > 0) {
                    resultMap.put("success", true);
                    resultMap.put("message", "password updated successfully");
                    return new ResponseEntity<String>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.OK);
                } else {
                    resultMap.put("success", false);
                    resultMap.put("error", "password update unsuccessful");
                    return new ResponseEntity<String>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.NOT_MODIFIED);
                }
            }else {
                resultMap.put("success", false);
                resultMap.put("error", "No user found");
                return new ResponseEntity<String>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            resultMap.put("success", false);
            resultMap.put("message", "Please check the input parameters");
            resultMap.put("error", "Internal server error");
            return new ResponseEntity<String>(new JSONObject(resultMap).toString(), httpHeaders, HttpStatus.BAD_REQUEST);
        }

    }

    /**
     * rest api for authenticating a user .
     * Created by Prabhakar Thakur on 17/7/2020
     * @param data
     * @return json response
     */
    @RequestMapping(value = UrlPath.LOGIN, method = {RequestMethod.POST}, produces = "application/json")
    public ResponseEntity<String> loginUser(@RequestBody String data) {
        List list = new ArrayList();
        UserBean userBean = null;
        Map resultMap = new HashMap();
        try {

            HashMap<String, String> userDataMap = mapper.readValue(data, HashMap.class);
            String email = userDataMap.get("email");
            String password = userDataMap.get("password");
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            logger.info("User authentication rest api starts");
            list = userAuthenticationService.authenticateUser(email, password);
            if (list.size() > 0) {

                userBean = (UserBean) list.get(0);
                int confirmationStatus = userBean.getConfirmationStatus();
                int userStatus;
                String userStatusString = userBean.getUserStatus();
                userStatus = Integer.parseInt(userStatusString);
                if (userStatus == 1) {
                    if (confirmationStatus == 1) {
                        logger.info("User confirmed");
                        Map userMap = new HashMap();
                        userMap.put("userStatus", userStatusString);
                        userMap.put("userUUID", userBean.getUserUUID());
                        resultMap.put("success", true);
                        resultMap.put("result", userMap);
                        JSONObject jsonObject = new JSONObject(resultMap);
                        logger.info("User authentication rest api ends");
                        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
                    } else {
                        logger.info("User not confirmed");
                        Map responseMap = new HashMap();
                        responseMap.put("success", false);
                        responseMap.put("error", "User not confirmed");
                        JSONObject jsonObject = new JSONObject(responseMap);
                        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
                    }
                } else {
                    logger.info("User not active");
                    Map responseMap = new HashMap();
                    responseMap.put("success", false);
                    responseMap.put("error", "User not active");
                    JSONObject jsonObject = new JSONObject(responseMap);
                    return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
                }
            } else {
                logger.info("User not found");
                Map responseMap = new HashMap();
                responseMap.put("success", false);
                responseMap.put("message", "Invalid username or password");
                JSONObject jsonObject = new JSONObject(responseMap);
                return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.UNAUTHORIZED);
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity<String>(new MessageBean(false).toString(), httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}