package com.dropthought.portal.controller;

import com.dropthought.portal.service.ClientService;
import com.dropthought.portal.service.SMSService;
import com.dropthought.portal.service.SchedulerService;
import com.dropthought.portal.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URLDecoder;
import java.security.Timestamp;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


@RestController
@RequestMapping(path = "/api/v1/portal")
@Validated
public class PortalController {


    private static final Logger logger = LoggerFactory.getLogger(PortalController.class);

    @Autowired
    private ClientService clientService;

    @Autowired
    private SchedulerService schedulerService;

    @Autowired
    private SMSService smsService;

    @Autowired
    private UserService userService;

    @Value("${eliza.url}")
    private String elizaUrl;



    /**
     * Http Headers
     */
    protected final HttpHeaders httpHeaders = new HttpHeaders();

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();


    /**
     * returns the version of the service
     *
     * @return Map(version, major.minor.patch)
     */
    @GetMapping("/version")
    public Map<String, String> version() {
        /*Map returnMap = new HashMap();
        returnMap.put("VERSION", "1.0.0");
        returnMap.put("SERVICE", "PORTAL");
        returnMap.put("DATE", "2021-09-01");
        return returnMap;*/
        logger.info("test bitbucket webhook");
        return Collections.singletonMap("VERSION", "1.0.0");
    }

    @PostMapping("generateshortlinks")
    @ResponseBody
    public ResponseEntity<String> generateshortlink() {
        logger.info("begins generateshortlink");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            schedulerService.validateShortUrlLinkV2();
            resultMap.put("success", true);
        }
        catch (Exception e){
            e.printStackTrace();
            logger.error(e.getMessage());
            resultMap.put("success", false);
            resultMap.put("error", "Error occurred.");
        }
        logger.info("end generateshortlink");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    @PostMapping("/version/test")
    @ResponseBody
    public ResponseEntity<String> versionTest(@RequestBody String data){
        logger.info("begin version test");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            resultMap.put("success", true);
            resultMap.put("data", new JSONObject(data));

        }
        catch (Exception e){
            e.printStackTrace();
            logger.error(e.getMessage());
            resultMap.put("success", false);
            resultMap.put("error", "Error occurred.");
        }
        logger.info("end version test");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    @PostMapping("/message/receive")
    @ResponseBody
    public ResponseEntity<String> receiveMsg(@RequestBody String data) {
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        logger.info("begin receive message");
        Map resultMsg = new HashMap();
        try {
            logger.info("Message received{} ", data);
            String messageToSend = "Thank you for your message! This is a confirmation and test response.";
            String decodedData = URLDecoder.decode(data, "UTF-8");
//            System.out.println(decodedData);
            logger.info("Decoded data {}", decodedData);
            String[] splitVals = decodedData.split("&");
            Map<String, String> outputs = new HashMap();
            for (int i = 0; i < splitVals.length; i++) {
                String item = splitVals[i];
                String[] keyVal = item.split("=");
                if (keyVal.length >= 2) {
                    outputs.put(keyVal[0], keyVal[1]);
                }
            }
            if (outputs.size() > 0 && !outputs.containsKey("From")) {
                throw new Exception("There is no phone number provided");
            }

            if (outputs.size() > 0 && !outputs.containsKey("Body")) {
                throw new Exception("There is no query provided");
            }

            //+140889342xx -> 40889342xx
            //"You are successfully in the customer list"
            String query = outputs.get("Body");
            String phoneNum = outputs.get("From");
            String uri = "https://test-api.dropthought.com/eliza/chatbot/query/api/v1/";
            StringBuilder stringBuilder = new StringBuilder();
            uri = stringBuilder.append(elizaUrl)
                    .append("chatbot/query/api/v1/").toString();
            logger.info("URI: {}", uri);
            String message = (smsService.isValidCustomerByPhoneNumber(phoneNum)) ? smsService.callEliza( query, phoneNum, uri) : "You are not a valid customer";

            clientService.sendMsg(message, phoneNum);
            resultMsg.put("Operation", "Successful");
            resultMsg.put("Message", data);
            resultMsg.put("success", true);
        } catch (Exception e) {
            e.printStackTrace();
            resultMsg.put("Operation", "Could not be completed");
            resultMsg.put("Message", "Could not be received");
        }
        return new ResponseEntity<String>(resultMsg.toString(), httpHeaders, HttpStatus.OK);
    }


    @GetMapping("/demo")
    @ResponseBody
    public ResponseEntity<String> demoDetails(){
        logger.info("begin demo api");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            Map demoMap = clientService.getDemoDetails();
            resultMap.put("success", true);
            resultMap.put("result", demoMap);
        }
        catch (Exception e){
            e.printStackTrace();
            logger.error(e.getMessage());
            resultMap.put("success", false);
            resultMap.put("error", "Error occurred.");
        }
        logger.info("end demo api");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

}
