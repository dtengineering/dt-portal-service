package com.dropthought.portal.controller;

import com.dropthought.portal.service.ClientService;
import com.dropthought.portal.service.SMSService;
import com.dropthought.portal.service.SchedulerService;
import com.dropthought.portal.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URLDecoder;
import java.security.Timestamp;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class SMSController {

    private static final Logger logger = LoggerFactory.getLogger(PortalController.class);

    @Autowired
    private ClientService clientService;

    @Autowired
    private UserService userService;

    @Autowired
    private SMSService smsService;

    /**
     * Http Headers
     */
    protected final HttpHeaders httpHeaders = new HttpHeaders();

    @PostMapping("/version/receive")
    @ResponseBody
    public ResponseEntity<String> receiveMsg(@RequestBody String data) {
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        logger.info("begin receive message");
        Map resultMsg = new HashMap();
        try {
            logger.info("Message received{} ", data);
            String messageToSend = "Thank you for your message! This is a confirmation and test response.";
            String decodedData = URLDecoder.decode(data, "UTF-8");
//            System.out.println(decodedData);
            logger.info("Decoded data {}", decodedData);
            String[] splitVals = decodedData.split("&");
            Map<String, String> outputs = new HashMap();
            for (int i = 0; i < splitVals.length; i++) {
                String item = splitVals[i];
                String[] keyVal = item.split("=");
                if (keyVal.length >= 2) {
                    outputs.put(keyVal[0], keyVal[1]);
                }
            }
            if (outputs.size() > 0 && !outputs.containsKey("From")) {
                throw new Exception("There is no phone number provided");
            }

            if (outputs.size() > 0 && !outputs.containsKey("Body")) {
                throw new Exception("There is no query provided");
            }

            //+140889342xx -> 40889342xx
            //"You are successfully in the customer list"
            String query = outputs.get("Body");
            String phoneNum = outputs.get("From");
            String uri = "https://stage-api.dropthought.com/eliza/chatbot/query/api/v1/";
            String message = (smsService.isValidCustomerByPhoneNumber(phoneNum)) ? smsService.callEliza( query, phoneNum, uri) : "You are not a valid customer";

            clientService.sendMsg(message, phoneNum);
            resultMsg.put("Operation", "Successful");
            resultMsg.put("Message", data);
            resultMsg.put("success", true);
        } catch (Exception e) {
            e.printStackTrace();
            resultMsg.put("Operation", "Could not be completed");
            resultMsg.put("Message", "Could not be received");
        }
        return new ResponseEntity<String>(resultMsg.toString(), httpHeaders, HttpStatus.OK);
    }


    @PostMapping("/version/respond")
    @ResponseBody
    public ResponseEntity<String> respondMsg(@RequestBody String data) {
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        logger.info("begin respond message");
        Map resultMsg = new HashMap();
        try {
            resultMsg.put("Message", data);
            resultMsg.put("Operation", "Successful");
            String message = "Message received";
            String phoneNumber = "4088934205";
            clientService.sendMsg(message, phoneNumber);
        } catch (Exception e) {
            e.printStackTrace();
            resultMsg.put("Message", "Could not be sent");
            resultMsg.put("Operation", "Could not be completed");
        }
        logger.info("end respond message");
        return new ResponseEntity<String>(resultMsg.toString(), httpHeaders, HttpStatus.OK);
    }

}

