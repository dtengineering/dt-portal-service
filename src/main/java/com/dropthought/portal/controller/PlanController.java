package com.dropthought.portal.controller;

import com.dropthought.portal.businessconfigurations.service.BusinessConfigVersionService;
import com.dropthought.portal.businessconfigurations.service.BusinessConfigurationsService;
import com.dropthought.portal.model.PlanConfigurationsBean;
import com.dropthought.portal.service.ClientService;
import com.dropthought.portal.service.PlanService;
import com.dropthought.portal.util.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/api/v1/portal/plan")
@Validated
public class PlanController {

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Http Headers
     */
    protected final HttpHeaders httpHeaders = new HttpHeaders();

    /**
     * Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private ClientService clientService;

    @Autowired
    private BusinessConfigurationsService businessConfigurationsService;

    @Autowired
    private BusinessConfigVersionService businessConfigVersionService;

    @Autowired
    private PlanService planService;


    @GetMapping()
    @ResponseBody()
    public ResponseEntity getActivePlans() {
        logger.info("get active plan details begins");
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try{
            List planList = planService.getActivePlans();
            resultMap.put("success",true);
            resultMap.put("result",planList);
            logger.info("get active plan details ends");
        }catch (Exception e){
            logger.error(e.getMessage());
            resultMap.put("success",false);
            resultMap.put("message",e.getMessage());
            logger.info("get active plan details ends");
        }
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Get business configurations by business uuid
     *
     * @param planId
     * @return
     */
    @GetMapping("/{planId}")
    @ResponseBody
    public ResponseEntity<String> getClientPlanConfigByPlanUUID(@PathVariable("planId") String planId, @RequestParam(defaultValue = "America/Los_Angeles") String timezone) {
        logger.info("get business plan config details begins");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(planId)) {
                int isTrueUUID = planService.getPlanIdByUUID(planId);
                if (isTrueUUID > 0) {
                    List response = planService.getPlanConfigurationsByPlanUUID(planId);
                    if (response.size() > 0) {
                        Map responseMap = (Map) response.get(0);
                        resultMap.put("success", true);
                        resultMap.put("result", responseMap);
                    } else {
                        resultMap.put("success", true);
                        resultMap.put("result", new ArrayList<>());
                    }
                    logger.info("response {}", resultMap);
                } else {
                    resultMap.put("success", false);
                    resultMap.put("error", "Provided planId is invalid");
                }

            }
            else{
                resultMap.put("success", false);
                resultMap.put("error", " Provide planId mandatory field");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put("success", false);
            resultMap.put("error", "Error occurred.");
        }
        logger.info("get plan config details ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Get List of business using plan uuid
     *
     * @return
     */
    @GetMapping("/{planId}/business")
    @ResponseBody
    public ResponseEntity<String> getAllBusinessByPlanUUID(@PathVariable("planId") String planId,@RequestParam(defaultValue = "America/Los_Angeles") String timezone) {
        logger.info("get plan config for business details begins");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            List response = planService.getAllBusinessByPlanUUID(planId);
            if (response.size() > 0) {
                resultMap.put("success", true);
                resultMap.put("result", response);
            } else {
                resultMap.put("success", true);
                resultMap.put("result", new ArrayList<>());
            }
            logger.info("response {}", resultMap);
        }catch (Exception e) {
            logger.info("Exception occured while fetching all plan details");
            e.printStackTrace();
        }
        logger.info("get plan config for business details ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Update client business configuration information by businessUUID
     *
     * @param planUUID
     * @param planConfigurationsbean
     * @return
     */
    @PutMapping("/{planId}")
    @ResponseBody
    public ResponseEntity<String> updatePlanConfig(@PathVariable("planId") String planUUID,
                                                     @Valid @RequestBody PlanConfigurationsBean planConfigurationsbean,
                                                   @RequestParam(value = "updateClient", defaultValue = "false") boolean updateClient) {
        logger.info("client plan config update begins");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(planUUID)) {
                List currentPlanConfig = planService.getPlanConfigurationsByPlanUUID(planUUID);
                if (currentPlanConfig.size() > 0) {
                    Map responseMap = (Map) currentPlanConfig.get(0);
                    Integer planConfigId = Integer.parseInt(responseMap.get("planConfigId").toString());
                    planConfigurationsbean.setPlanConfigId(planConfigId);
                    planConfigurationsbean.setPlanConfigUUID(planUUID);
                    planService.updatePlanConfigurations(planConfigurationsbean);
                    if(updateClient)
                    {
                        planService.convertAndApplyPlanConfigurationsToBusinessConfigurations(planConfigurationsbean,0,"","", "");
                    }
                    List newPlanConfig = planService.getPlanConfigurationsByPlanUUID(planUUID);
                    if (newPlanConfig.size() > 0) {
                        responseMap = (Map) newPlanConfig.get(0);
                        resultMap.put("success", true);
                        resultMap.put("updateClient", updateClient);
                        resultMap.put("result", responseMap);
                    } else {
                        resultMap.put("success", true);
                        resultMap.put("result", new ArrayList<>());
                    }
                    logger.info("response {}", resultMap);
                }
                else {
                    //No plan exists with given uuid so create new plan
                    resultMap.put("success", false);
                    resultMap.put("error", "No plan exists with given planUUID");
                }
            }
            else{
                resultMap.put("success", false);
                resultMap.put("error", " Provide planUUID mandatory field");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put("success", false);
            resultMap.put("error", "Error occurred.");
        }
        logger.info("client plan configuration update ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Create business configurations
     *
     * @param planConfigurationsBean
     * @return
     */
    @PostMapping()
    @ResponseBody
    public ResponseEntity<String> createPlanConfig(@Valid @RequestBody PlanConfigurationsBean planConfigurationsBean) {
        logger.info("begins client business plan config creation");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(planConfigurationsBean)) {
                Map status = planService.createPlanConfigurations(planConfigurationsBean);
                int insertStatus = (int) status.get("insertStatus");
                String planUUID = (String) status.get("planConfigUUID");
                if (insertStatus > 0) {
                    List response = planService.getPlanConfigurationsByPlanUUID(planUUID);
                    if (response.size() > 0) {
                        Map responseMap = (Map) response.get(0);
                        resultMap.put("success", true);
                        resultMap.put("result", responseMap);
                    } else {
                        resultMap.put("success", true);
                        resultMap.put("result", new ArrayList<>());
                    }
                }
            }
            else{
                resultMap.put("success", false);
                resultMap.put("error", " Provide mandatory fields");
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
            e.printStackTrace();
            resultMap.put("success", false);
            resultMap.put("error", "Error occurred.");
        }
        logger.info("create client business plan creation ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Delete Plan configuration by plan UUID and Update client business configurations with default plan as '0'
     *
     * @param planUUID
     * @return
     */
    @DeleteMapping("{planId}")
    @ResponseBody
    public ResponseEntity<String> deletePlanConfig(@PathVariable("planId") String planUUID,
                                                   @RequestParam(required = false, defaultValue = "false") boolean cancelByClient,
                                                   @RequestParam(required = false, defaultValue = "0") String businessUUID) {

        logger.info("delete plan config details begins");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try {
            if(Utils.isNotNull(planUUID)) {
                int planId = planService.retrievePlanIdByPlanUUID(planUUID);
                if (planId > 0) {
                    int deleteStatus = planService.deletePlanConfigByPlanId(planUUID, planId, cancelByClient, businessUUID);
                    if (deleteStatus > 0) {
                        resultMap.put("success", true);
                        resultMap.put("result", new ArrayList<>());
                        resultMap.put("message", "Plan configuration deleted successfully");
                    } else {
                        resultMap.put("success", false);
                        resultMap.put("error", "bad request");
                        resultMap.put("message", "Failed to delete plan configuration");
                    }
                    logger.info("response {}", resultMap);
                } else {
                    resultMap.put("success", false);
                    resultMap.put("error", "Provided planUUID is invalid");
                }

            }
            else{
                resultMap.put("success", false);
                resultMap.put("error", " Provide planID mandatory field");
            }
        } catch (Exception e){
            logger.error(e.getMessage());
            logger.error(e.getMessage());
            resultMap.put("success", false);
            resultMap.put("error", "Error occurred.");
        }
        logger.info("delete plan config details ends");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    /**
     * Apply/modify a plan for a client
     */
    @PutMapping("/config")
    @ResponseBody
    public ResponseEntity<String> updateClientConfig(@RequestBody String data) {
        logger.info("client plan config update begins");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try
        {
            HashMap<String, Object> businessPlanMap = mapper.readValue(data, HashMap.class);
            String businessUUID = businessPlanMap.get("businessUUID").toString();
            int planId = (int) businessPlanMap.get("planId");
            /*int businessId = planService.getBusinessIdByUUID(businessUUID);//not needed
            if(Utils.isNotNull(businessUUID))
            {*/
                Map businessMap = planService.getBusinessIdHippaByUUID(businessUUID);
                if(businessMap.containsKey("business_id"))
                {
                    int businessId = (int) businessMap.get("business_id");
                    String hippa = businessMap.get("hippa").toString(); //hippa details we need to get it from the request instead of plan config DTV-12266
                    String planUUID = planService.retrievePlanUUIDyPlanID(planId);
                    //DTV-12525
                    String startDate = Utils.getDateFromDateTime(businessMap.get("contract_start_date").toString());
                    String endDate = Utils.getDateFromDateTime(businessMap.get("contract_end_date").toString());
                    PlanConfigurationsBean planConfigurationsBean = planService.getPlanConfigByPlanConfigUUID(planUUID);
                    planConfigurationsBean.setHippa(hippa);//DTV-12266
                    logger.info("businessId= "+businessId);
                    planService.convertAndApplyPlanConfigurationsToBusinessConfigurations(planConfigurationsBean, businessId,
                            startDate, endDate, businessUUID);
                    int result = planService.updatePlanIdinBusinessTable(businessUUID,planUUID);
                    resultMap.put("success", true);
                    resultMap.put("message", "plan config successfully applied to business");
                }
//            }
        }catch (Exception e)
        {
            resultMap.put("success", false);
            resultMap.put("error", "Error occurred.");
            e.printStackTrace();
        }
        logger.info("end apply plan to business");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    @GetMapping("/currency")
    @ResponseBody()
    public ResponseEntity getCurrencyDetails() {
        logger.info("get currency details begins");
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try{
            List planList = planService.getCurrencyList();
            resultMap.put("success",true);
            resultMap.put("result",planList);
            logger.info("get currency details ends");
        }catch (Exception e){
            logger.error(e.getMessage());
            resultMap.put("success",false);
            resultMap.put("message",e.getMessage());
            logger.info("get active plan details ends");
        }
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

}
