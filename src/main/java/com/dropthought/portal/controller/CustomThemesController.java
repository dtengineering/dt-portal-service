package com.dropthought.portal.controller;

import com.dropthought.portal.model.CustomThemeBean;
import com.dropthought.portal.service.CustomThemeService;
import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.Utils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(path = "/api/v1/portal/theme")
public class CustomThemesController {
    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Http Headers
     */
    protected final HttpHeaders httpHeaders = new HttpHeaders();

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper()
            .enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT)
            .enable(DeserializationFeature.FAIL_ON_READING_DUP_TREE_KEY );

    @Autowired
    private CustomThemeService customThemeService;

    /**
     * returns the version of the service
     *
     * @return Map(version, major.minor.patch)
     */
    @GetMapping("/version")
    public Map<String, String> version() {
        return Collections.singletonMap("VERSION", "1.0.0");
    }

    @PostMapping()
    @ResponseBody
    public ResponseEntity<String> createTheme(@RequestBody CustomThemeBean customThemeBean) {
        logger.info("begin createTheme");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            if (Utils.isNotNull(customThemeBean)) {
                resultMap = customThemeService.createTheme(customThemeBean);
                logger.info("response {}", resultMap);
            } else {
                resultMap = Utils.failureResponse(" Provide mandatory fields");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity<String>(new JSONObject(Utils.failureResponse()).toString(), httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        logger.info("End createTheme");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    @GetMapping("/{themeUUID}")
    @ResponseBody
    public ResponseEntity<String> getThemeById(@PathVariable("themeUUID") String themeUUID) {
        logger.info("begin get theme by id");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            if (Utils.notEmptyString(themeUUID)) {
                CustomThemeBean customThemeBean = customThemeService.getThemeByThemeUUID(themeUUID);
                resultMap = Utils.successResponse();
                resultMap.put(Constants.RESULT, customThemeBean);
            } else {
                resultMap = Utils.failureResponse("ThemeUUID is mandatory");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity<String>(new JSONObject(Utils.failureResponse()).toString(), httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        logger.info("End get theme by id");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    @PutMapping("/{themeUUID}")
    @ResponseBody
    public ResponseEntity<String> updateThemeById(@PathVariable("themeUUID") String themeUUID, @RequestBody CustomThemeBean customThemeBean) {
        logger.info("begin update theme by id");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            if (Utils.isNotNull(themeUUID)) {
                int themeId = customThemeService.getThemeIdByThemeUUID(themeUUID);
                if(themeId > 0) {
                    customThemeBean.setThemeUUID(themeUUID);
                    resultMap = customThemeService.updateThemeById(customThemeBean);
                }
            } else {
                resultMap = Utils.failureResponse("ThemeUUID is mandatory");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity<String>(new JSONObject(Utils.failureResponse()).toString(), httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        logger.info("End update theme by id");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<String> getAllThemes(@RequestParam(value = "search", required = false, defaultValue = "") String themeName,
                                               @RequestParam(value = "sortBy", required = false, defaultValue = "createdTime") String sortBy,
                                               @RequestParam(value = "exactMatch", required = false, defaultValue = "false") boolean exactMatch,
                                               @RequestParam(value = "pageNo", required = false, defaultValue = "-1") Integer pageNo){
        logger.info("begin get all themes");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            resultMap = customThemeService.getAllThemes(themeName, exactMatch, sortBy, pageNo);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity<String>(new JSONObject(Utils.failureResponse()).toString(), httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        logger.info("End get all themes");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    @DeleteMapping("/{themeUUID}")
    @ResponseBody
    public ResponseEntity<String> deleteThemeById(@PathVariable("themeUUID") String themeUUID) {
        logger.info("begin deleteThemeById");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            if (Utils.isNotNull(themeUUID)) {
                int themeId = customThemeService.getThemeIdByThemeUUID(themeUUID);
                if(themeId > 0) {
                    resultMap = customThemeService.deleteThemeById(themeUUID);
                }
            } else {
                resultMap = Utils.failureResponse("ThemeUUID not present");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity<String>(new JSONObject(Utils.failureResponse()).toString(), httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        logger.info("End deleteThemeById");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }

    @PostMapping("/image/url")
    @ResponseBody
    public ResponseEntity<String> uploadImagetoS3(@RequestBody String base64) {
        logger.info("begin uploadImagetoS3");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map resultMap = new HashMap();
        try {
            if (!base64.isEmpty()) {
                Map requestMap = mapper.readValue(base64, new TypeReference<HashMap>() {
                });
                String base64String = requestMap.get("base64").toString();
                resultMap = customThemeService.uploadImageToS3(base64String);
            } else {
                resultMap = Utils.failureResponse(" Provide mandatory fields");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResponseEntity<String>(new JSONObject(Utils.failureResponse()).toString(), httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        logger.info("End uploadImagetoS3");
        JSONObject jsonObject = new JSONObject(resultMap);
        return new ResponseEntity<String>(jsonObject.toString(), httpHeaders, HttpStatus.OK);
    }



}
