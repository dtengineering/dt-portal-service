package com.dropthought.portal.controller;

import com.dropthought.portal.model.HouseKeepingBean;
import com.dropthought.portal.service.HouseKeepingService;
import com.dropthought.portal.util.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(path = "/api/v1/portal/housekeeping")
@Validated
public class HouseKeeping {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Http Headers
     */
    protected final HttpHeaders httpHeaders = new HttpHeaders();

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private HouseKeepingService houseKeepingService;


    @RequestMapping(method = {RequestMethod.POST}, consumes = "application/json", produces = "application/json")
    public Map manageAccountSettings(@RequestBody HouseKeepingBean houseKeepingBean){
        logger.info("begin house keeping controller");
        Map result = new HashMap();
        try{

            boolean status = houseKeepingService.configureTextAnalytics(houseKeepingBean);
            if(status) {
                result.put(Utils.SUCCESS,true);
            }else{
                result.put(Utils.SUCCESS, false);
            }

        }catch (Exception e){
            logger.error(e.getMessage());
            result.put(Utils.SUCCESS, (false));
        }
        logger.info("end house keeping controller");
        return result;

    }

}
