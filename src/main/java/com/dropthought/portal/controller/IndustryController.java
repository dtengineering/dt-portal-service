package com.dropthought.portal.controller;

import com.dropthought.portal.service.IndustryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/api/v1/portal/industry")
@Validated
public class IndustryController {

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Http Headers
     */
    protected final HttpHeaders httpHeaders = new HttpHeaders();

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private IndustryService industryService;

    /**
     *
     * @return
     */
    @GetMapping()
    @ResponseBody
    public Map getIndustry() {
        logger.info("get industry details begins");
        ResponseEntity<String> responseEntity = null;
        Map resultMap = new HashMap();
        try{
            List industryList = industryService.getIndustry();
            resultMap.put("success",true);
            resultMap.put("result",industryList);
            logger.info("get industry details ends");
        }catch (Exception e){
            logger.error(e.getMessage());
            resultMap.put("success",false);
            resultMap.put("message",e.getMessage());
            logger.info("get industry details ends");
        }
        return resultMap;
    }

}
