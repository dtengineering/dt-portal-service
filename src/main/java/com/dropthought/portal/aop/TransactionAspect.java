package com.dropthought.portal.aop;

import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.Utils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;

@Aspect
@Configuration
public class TransactionAspect {
    private static final Logger logger = LoggerFactory.getLogger(TransactionAspect.class);

    /**
     * worker transaction
     * @param joinPoint
     */
    @Before("execution(* com.dropthought.portal.worker.*.*(..))")
    public void before(JoinPoint joinPoint) {
        String correlationId = generateUniqueCorrelationId();
        MDC.put(Constants.WORKER_TRANSACTION_ID, correlationId);
        logger.info("TransactionAspect  Before call :: {} ", joinPoint);
    }


    @After("execution(* com.dropthought.portal.worker.*.*(..))")
    public void after(JoinPoint joinPoint) {
        logger.info("TransactionAspect after call :: {} ", joinPoint);
        MDC.remove(Constants.WORKER_TRANSACTION_ID);
    }

    /**
     * worker transaction
     * @param joinPoint
     */
    @Before("execution(* com.dropthought.portal.service.*.*(..))")
    public void beforeService(JoinPoint joinPoint) {
        if(Utils.notEmptyString(MDC.get(Constants.WORKER_TRANSACTION_ID)) || Utils.notEmptyString(MDC.get(Constants.TRANSACTION_ID)))
            logger.info("TransactionAspect  Before service call :: {} ", joinPoint);
    }


    @After("execution(* com.dropthought.portal.service.*.*(..))")
    public void afterService(JoinPoint joinPoint) {
        if(Utils.notEmptyString(MDC.get(Constants.WORKER_TRANSACTION_ID)) || Utils.notEmptyString(MDC.get(Constants.TRANSACTION_ID)))
            logger.info("TransactionAspect after service call :: {} ", joinPoint);
    }

    /**
     * to log exception msg in the application
     * @param ex
     */
    @AfterThrowing(value = "execution(* com.dropthought.portal.*.*.*(..))", throwing = "ex")
    public void logError(Exception ex) {
        logger.error("TransactionAspect error log :: {} , url {} ", ex.getMessage());
    }

    private String generateUniqueCorrelationId() {
        return UUID.randomUUID().toString();
    }
}
