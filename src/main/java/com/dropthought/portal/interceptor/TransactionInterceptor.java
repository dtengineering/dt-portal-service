package com.dropthought.portal.interceptor;

import com.dropthought.portal.util.Constants;
import com.dropthought.portal.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

public class TransactionInterceptor implements HandlerInterceptor {
    private static final Logger logger = LoggerFactory.getLogger(TransactionInterceptor.class);

    @Override
    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler) throws Exception {
        String transactionId = request.getHeader(Constants.TRANSACTION_ID);
        if(Utils.emptyString(transactionId)){
            transactionId = generateUniqueId();
        }
        MDC.put(Constants.TRANSACTION_ID, transactionId);
        logger.info("interceptor Before call :: {} ", request.getRequestURI());
        return true;
    }

    @Override
    public void afterCompletion(final HttpServletRequest request, final HttpServletResponse response, final Object handler, final Exception ex) throws Exception {
        logger.info("interceptor After completion :: {} ", request.getRequestURI());
        MDC.remove(Constants.TRANSACTION_ID);
    }

    private String generateUniqueId() {
        return UUID.randomUUID().toString();
    }


}



