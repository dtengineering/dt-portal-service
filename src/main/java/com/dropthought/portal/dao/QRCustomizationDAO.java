package com.dropthought.portal.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
@Component
public class QRCustomizationDAO {
    public static final String SELECT_TAG_LINK_QR_CUSTOMIZATION_BY_ID="select  qr_customization from tag_links_%s where tag_link_uuid=?";
    public static final String TAG_LINK_TABLE="tag_links_";
    @Autowired
    @Qualifier("jdbcDt")
    JdbcTemplate jdbcTemplate;
    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();

    public Map getTagLinkQRCustomizationByTagId(String tagLinkUniqueId, String businessUUID){
        logger.info("getTagLinkQRCustomizationByTagId starts");
        try {
            String sql = String.format(SELECT_TAG_LINK_QR_CUSTOMIZATION_BY_ID, TAG_LINK_TABLE + businessUUID.replaceAll("-","_"));
            return jdbcTemplate.queryForObject(sql, new Object[]{tagLinkUniqueId}, HashMap.class);
        }
        catch (Exception e){
            logger.error("Error in getTagLinkQRCustomizationByTagId() : " + e.getMessage());
        }
        return null;
    }
}
