package com.dropthought.portal.dao;

import com.dropthought.portal.model.CustomThemeBean;
import com.dropthought.portal.service.CustomThemeService;
import com.dropthought.portal.util.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class CustomThemeDAO {

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();
    /**
     * JdbcTemplate for DB Connectivity.
     */

    @Autowired
    @Qualifier("jdbcDt")
    JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("jdbcNamedDt")
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    @Autowired
    CustomThemeService customThemeService;

    /*************** query statements - starts ************************/
    /*** custom_eux_themes tables query *****/
    private static final String INSERT_THEME = "insert into custom_eux_themes (theme_name, business_id, theme_uuid, page_theme_style_code, page_theme_bg_img, buttons_style_code, progress_bar_style_code, created_time, " +
            " smiley_rating_style_code, smiley_rating_bg_img, star_rating_style_code, star_rating_bg_img, thumb_rating_style_code, thumb_rating_bg_img,  " +
            " slider_rating_style_code, slider_rating_bg_img, scale_rating_style_code, scale_rating_bg_img, multiple_response_style_code, muliple_response_bg_img, " +
            " single_response_style_code, single_response_bg_img, dropdown_style_code, open_ended_style_code, nps_style_code, nps_bg_img, ranking_style_code, " +
            " ranking_bg_img, matrix_rating_style_code, matrix_rating_bg_img)" +
            " values (?,?,?,?,?,?,?,current_timestamp,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
    private static final String GET_THEME_ID_BY_THEME_UUID = "select id from custom_eux_themes where theme_uuid = ? ";
    private static final String GET_ALL_THEMES_COUNT = "select count(id) from custom_eux_themes ";
    private static final String DELETE_THEME_BY_ID = "DELETE from custom_eux_themes where theme_uuid = ? ";
    private static final String GET_THEME_BY_THEME_UUID = "select id, theme_name, business_id, theme_uuid, page_theme_style_code, page_theme_bg_img, buttons_style_code, progress_bar_style_code, created_time, modified_time, smiley_rating_style_code, smiley_rating_bg_img, star_rating_style_code, star_rating_bg_img, thumb_rating_style_code, thumb_rating_bg_img, slider_rating_style_code, slider_rating_bg_img, scale_rating_style_code, scale_rating_bg_img, multiple_response_style_code, muliple_response_bg_img, single_response_style_code, single_response_bg_img, dropdown_style_code, open_ended_style_code, nps_style_code, nps_bg_img, ranking_style_code, ranking_bg_img, matrix_rating_style_code, matrix_rating_bg_img from custom_eux_themes where theme_uuid = ? ";


    /**
     * This method is used to create the theme
     * @param bean
     * @return
     */
    public String createTheme(final CustomThemeBean bean){
        logger.debug("begin creating the trigger details");
        String result = "";
        try{

            final String themeUUID = Utils.generateUUID();
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement statement = connection.prepareStatement(INSERT_THEME, Statement.RETURN_GENERATED_KEYS);
                    statement.setString(1, bean.getThemeName());
                    statement.setString(2, Utils.isNotNull(bean.getBusinessId()) ? new JSONArray(bean.getBusinessId()).toString() : new JSONArray().toString());
                    statement.setString(3, themeUUID);
                    statement.setString(4, bean.getPageStyleCode());
                    statement.setString(5, bean.getPageImageUrl());
                    statement.setString(6, bean.getButtonsStyleCode());
                    statement.setString(7, bean.getProgressBarStyleCode());
                    statement.setString(8, bean.getSmileyRatingStyleCode());
                    statement.setString(9, Utils.isNotNull(bean.getSmileyRatingImageUrl()) ? new JSONObject(bean.getSmileyRatingImageUrl()).toString() : new JSONObject().toString());
                    statement.setString(10, bean.getStarRatingStyleCode());
                    statement.setString(11, Utils.isNotNull(bean.getStarRatingImageUrl()) ? new JSONObject(bean.getStarRatingImageUrl()).toString() : new JSONObject().toString());
                    statement.setString(12, bean.getThumbRatingStyleCode());
                    statement.setString(13, Utils.isNotNull(bean.getThumbRatingImageUrl()) ? new JSONObject(bean.getThumbRatingImageUrl()).toString() : new JSONObject().toString());
                    statement.setString(14, bean.getSliderRatingStyleCode());
                    statement.setString(15, bean.getSliderRatingImageUrl());
                    statement.setString(16, bean.getScaleRatingStyleCode());
                    statement.setString(17, Utils.isNotNull(bean.getScaleRatingImageUrl()) ? new JSONObject(bean.getScaleRatingImageUrl()).toString() : new JSONObject().toString());
                    statement.setString(18, bean.getMultipleResponseStyleCode());
                    statement.setString(19, Utils.isNotNull(bean.getMultipleResponseImageUrl()) ? new JSONObject(bean.getMultipleResponseImageUrl()).toString() : new JSONObject().toString());
                    statement.setString(20, bean.getSingleResponseStyleCode());
                    statement.setString(21, Utils.isNotNull(bean.getSingleResponseImageUrl()) ? new JSONObject(bean.getSingleResponseImageUrl()).toString() : new JSONObject().toString());
                    statement.setString(22, bean.getDropdownStyleCode());
                    statement.setString(23, bean.getOpenEndedStyleCode());
                    statement.setString(24, bean.getNpsStyleCode());
                    statement.setString(25, Utils.isNotNull(bean.getNpsImageUrl()) ? new JSONObject(bean.getNpsImageUrl()).toString() : new JSONObject().toString());
                    statement.setString(26, bean.getRankingStyleCode());
                    statement.setString(27, bean.getRankingImageUrl());
                    statement.setString(28, bean.getMatrixRatingStyleCode());
                    statement.setString(29, Utils.isNotNull(bean.getMatrixRatingImageUrl()) ? new JSONObject(bean.getMatrixRatingImageUrl()).toString() : new JSONObject().toString());
                    return statement;
                }
            });
            result = themeUUID;

        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Get ThemeId by themeUUID
     * @param themeUUID
     * @return
     */
    public int getThemeIdByThemeUUID(String themeUUID) {
        int themeId = 0;
        try{
            if(Utils.notEmptyString(themeUUID)){
                themeId = jdbcTemplate.queryForObject(GET_THEME_ID_BY_THEME_UUID, new Object[]{themeUUID}, Integer.class);
            }
        }catch (EmptyResultDataAccessException emptyResultDataAccessException){
            logger.info("ThemeUUID not found");
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return themeId;
    }

    /**
     * Get Theme by themeUUID
     * @param themeUUID
     * @return
     */
    public Map getThemeByThemeUUID(String themeUUID){
        logger.info("begin retrieving the theme details by themeUUID");
        Map themeMap = new HashMap();
        try {
            List result = jdbcTemplate.queryForList(GET_THEME_BY_THEME_UUID, new Object[]{themeUUID});
            if(result.size() > 0)
                themeMap = (Map) result.get(0);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("end retrieving the theme details by themeUUID");
        return themeMap;
    }

    /**
     * update theme by themeUUID
     * @param bean
     * @return
     */
    public int updateThemeById(CustomThemeBean bean){
        int result = 0;
        try{
            ArrayList<Object> objectArrayList = new ArrayList<Object>();
            StringBuilder sql = new StringBuilder("update custom_eux_themes set ");

            if(Utils.isNotNull(bean.getThemeName())){
                logger.info("Theme name present");
                objectArrayList.add(bean.getThemeName());
                sql.append(" theme_name = ?,");
            }
            if(Utils.isNotNull(bean.getBusinessId())){
                logger.info("Business id present");
                objectArrayList.add(new JSONArray(bean.getBusinessId()).toString());
                sql.append(" business_id = ?,");
            }
            if(Utils.isNotNull(bean.getPageStyleCode())){
                logger.info("Page style code present");
                objectArrayList.add(bean.getPageStyleCode());
                sql.append(" page_theme_style_code = ?,");
            }
            if(Utils.isNotNull(bean.getPageImageUrl())){
                logger.info("Page image url present");
                objectArrayList.add(bean.getPageImageUrl());
                sql.append(" page_theme_bg_img = ?,");
            }
            if(Utils.isNotNull(bean.getButtonsStyleCode())){
                logger.info("Buttons style code present");
                objectArrayList.add(bean.getButtonsStyleCode());
                sql.append(" buttons_style_code = ?,");
            }
            if(Utils.isNotNull(bean.getProgressBarStyleCode())){
                logger.info("Progress bar style code present");
                objectArrayList.add(bean.getProgressBarStyleCode());
                sql.append(" progress_bar_style_code = ?,");
            }
            if(Utils.isNotNull(bean.getSmileyRatingStyleCode())){
                logger.info("Smiley rating style code present");
                objectArrayList.add(bean.getSmileyRatingStyleCode());
                sql.append(" smiley_rating_style_code = ?,");
            }
            if(Utils.isNotNull(bean.getSmileyRatingImageUrl())){
                logger.info("Smiley rating image url present");
                objectArrayList.add(new JSONObject(bean.getSmileyRatingImageUrl()).toString());
                sql.append(" smiley_rating_bg_img = ?,");
            }
            if(Utils.isNotNull(bean.getStarRatingStyleCode())){
                logger.info("Star rating style code present");
                objectArrayList.add(bean.getStarRatingStyleCode());
                sql.append(" star_rating_style_code = ?,");
            }
            if(Utils.isNotNull(bean.getStarRatingImageUrl())){
                logger.info("Star rating image url present");
                objectArrayList.add(new JSONObject(bean.getStarRatingImageUrl()).toString());
                sql.append(" star_rating_bg_img = ?,");
            }
            if(Utils.isNotNull(bean.getThumbRatingStyleCode())){
                logger.info("Thumb rating style code present");
                objectArrayList.add(bean.getThumbRatingStyleCode());
                sql.append(" thumb_rating_style_code = ?,");
            }
            if(Utils.isNotNull(bean.getThumbRatingImageUrl())){
                logger.info("Thumb rating image url present");
                objectArrayList.add(new JSONObject(bean.getThumbRatingImageUrl()).toString());
                sql.append(" thumb_rating_bg_img = ?,");
            }
            if(Utils.isNotNull(bean.getSliderRatingStyleCode())){
                logger.info("Slider rating style code present");
                objectArrayList.add(bean.getSliderRatingStyleCode());
                sql.append(" slider_rating_style_code = ?,");
            }
            if(Utils.isNotNull(bean.getSliderRatingImageUrl())){
                logger.info("Slider rating image url present");
                objectArrayList.add(bean.getSliderRatingImageUrl());
                sql.append(" slider_rating_bg_img = ?,");
            }
            if(Utils.isNotNull(bean.getScaleRatingStyleCode())){
                logger.info("Scale rating style code present");
                objectArrayList.add(bean.getScaleRatingStyleCode());
                sql.append(" scale_rating_style_code = ?,");
            }
            if(Utils.isNotNull(bean.getScaleRatingImageUrl())){
                logger.info("Scale rating image url present");
                objectArrayList.add(new JSONObject(bean.getScaleRatingImageUrl()).toString());
                sql.append(" scale_rating_bg_img = ?,");
            }
            if(Utils.isNotNull(bean.getMultipleResponseStyleCode())){
                logger.info("Multiple response style code present");
                objectArrayList.add(bean.getMultipleResponseStyleCode());
                sql.append(" multiple_response_style_code = ?,");
            }
            if(Utils.isNotNull(bean.getMultipleResponseImageUrl())){
                logger.info("Multiple response image url present");
                objectArrayList.add(new JSONObject(bean.getMultipleResponseImageUrl()).toString());
                sql.append(" muliple_response_bg_img = ?,");
            }
            if(Utils.isNotNull(bean.getSingleResponseStyleCode())){
                logger.info("Single response style code present");
                objectArrayList.add(bean.getSingleResponseStyleCode());
                sql.append(" single_response_style_code = ?,");
            }
            if(Utils.isNotNull(bean.getSingleResponseImageUrl())){
                logger.info("Single response image url present");
                objectArrayList.add(new JSONObject(bean.getSingleResponseImageUrl()).toString());
                sql.append(" single_response_bg_img = ?,");
            }
            if(Utils.isNotNull(bean.getDropdownStyleCode())){
                logger.info("Dropdown style code present");
                objectArrayList.add(bean.getDropdownStyleCode());
                sql.append(" dropdown_style_code = ?,");
            }
            if(Utils.isNotNull(bean.getOpenEndedStyleCode())){
                logger.info("Open ended style code present");
                objectArrayList.add(bean.getOpenEndedStyleCode());
                sql.append(" open_ended_style_code = ?,");
            }
            if(Utils.isNotNull(bean.getNpsStyleCode())){
                logger.info("NPS style code present");
                objectArrayList.add(bean.getNpsStyleCode());
                sql.append(" nps_style_code = ?,");
            }
            if(Utils.isNotNull(bean.getNpsImageUrl())){
                logger.info("NPS image url present");
                objectArrayList.add(new JSONObject(bean.getNpsImageUrl()).toString());
                sql.append(" nps_bg_img = ?,");
            }

            if(Utils.isNotNull(bean.getRankingStyleCode())){
                logger.info("Ranking style code present");
                objectArrayList.add(bean.getRankingStyleCode());
                sql.append(" ranking_style_code = ?,");
            }
            if(Utils.isNotNull(bean.getRankingImageUrl())){
                logger.info("Ranking image url present");
                objectArrayList.add(bean.getRankingImageUrl());
                sql.append(" ranking_bg_img = ?,");
            }
            if(Utils.isNotNull(bean.getMatrixRatingStyleCode())){
                logger.info("Matrix rating style code present");
                objectArrayList.add(bean.getMatrixRatingStyleCode());
                sql.append(" matrix_rating_style_code = ?,");
            }
            if(Utils.isNotNull(bean.getMatrixRatingImageUrl())){
                logger.info("Matrix rating image url present");
                objectArrayList.add(new JSONObject(bean.getMatrixRatingImageUrl()).toString());
                sql.append(" matrix_rating_bg_img = ?, ");
            }

            if(objectArrayList.size() > 0) {

                sql.append(" modified_time = ?");
                objectArrayList.add(new Timestamp(System.currentTimeMillis()));

                sql.append(" where theme_uuid = ?");
                objectArrayList.add(bean.getThemeUUID());

                logger.info("SQL query to update theme : " + sql.toString());
                result = jdbcTemplate.update(sql.toString(), objectArrayList.toArray());
            }


        }catch (Exception e){
            e.printStackTrace();
            result = -1;
        }
        return result;
    }

    /**
     * This method is used to get all the themes
     * @param searchTerm
     * @param exactMatch
     * @param sortString
     * @param pageId
     * @return
     */
    public List getAllThemes(String searchTerm, Boolean exactMatch, String sortString, int pageId){
        List result = new ArrayList();
        try{
            int upperLimit = 0;
            int length = 10;
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT id, theme_name, theme_uuid, page_theme_style_code, page_theme_bg_img from custom_eux_themes where id > 0");

            //add search condition if available
            if (Utils.notEmptyString(searchTerm)) {
                String searchStr = URLDecoder.decode(StringEscapeUtils.unescapeJava(searchTerm),
                        StandardCharsets.UTF_8.toString());
                if (exactMatch)
                    sql.append(" and theme_name = '" + searchStr + "' ");
                else
                    sql.append(" and theme_name like '%" + searchStr.toUpperCase() + "%'");
            }


            String sortingOrder = " desc ";
            switch (sortString) {
                case "createdTime":
                    sql.append(" order by created_time ").append(sortingOrder);
                    break;

                case "modifiedTime":
                    sql.append(" order by modified_time ").append(sortingOrder);
                    break;

                case "themeName":
                    // append sort by criteria at the end of query
                    sql.append(" order by theme_name asc ");
                    break;
            }

            if(pageId > -1){
                upperLimit = (pageId - 1) * length;
                sql.append(" limit :upperLimit, :length");
            }

            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("upperLimit", upperLimit);
            parameterSource.addValue("length", length);
            logger.info("SQL query for getting all themes : " + sql.toString());

            //The mapRow() method  doesn't require loading all the results into memory at once, and it can improve performance when dealing with large result sets.
            result = namedParameterJdbcTemplate.query(sql.toString(), parameterSource, new RowMapper() {
                @Override
                public Object mapRow(ResultSet resultSet, int i) throws SQLException {
                    CustomThemeBean bean = new CustomThemeBean();
                    bean.setId(resultSet.getInt("id"));
                    bean.setThemeName(resultSet.getString("theme_name"));
                    bean.setThemeUUID(resultSet.getString("theme_uuid"));
                    bean.setPageStyleCode(Utils.isNull(resultSet.getString("page_theme_style_code")) ? "" : resultSet.getString("page_theme_style_code"));
                    bean.setPageImageUrl(Utils.isNull(resultSet.getString("page_theme_bg_img")) ? "" : resultSet.getString("page_theme_bg_img"));
                    return bean;
                }
            });


        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

    public int deleteThemeById(String themeUUID){
        int result = 0;
        try {
            if(Utils.notEmptyString(themeUUID)){
                result = jdbcTemplate.update(DELETE_THEME_BY_ID, themeUUID);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method to get count of all themes
     * @return
     */
     public int getAllThemesCount(){
         int result = 0;
        try {
            result = jdbcTemplate.queryForObject(GET_ALL_THEMES_COUNT, Integer.class);
        }catch (EmptyResultDataAccessException emptyResultDataAccessException){
            logger.info("No themes found");
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return result;
     }


}
