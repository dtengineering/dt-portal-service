package com.dropthought.portal.dao;

import com.dropthought.portal.util.Utils;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class TekionDAO {
    /**
     * JdbcTemplate for DB Connectivity.
     */
    @Autowired
    @Qualifier("jdbcDt")
    JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("jdbcNamedDt")
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();


    /**
     * Function to get dealers list who have tekion worlflows turned on
     *
     * @return
     */
    public List<Map<String, Object>> getDealersList() {
        List<Map<String, Object>> resultList = null;
        try {
            //fetch business where the workflow is enabled
            String fetchBusiness = "SELECT JSON_ARRAYAGG(business_id) as businessId FROM integrations_workflow_status WHERE integration_workflow_id = '9' and status = '1'";
            List<Map<String, Object>> business = jdbcTemplate.queryForList(fetchBusiness);
            if(Utils.isNotNull(business.get(0))) {
                List<Integer> businessList = Utils.isNotNull(business.get(0).get("businessId")) ? mapper.readValue(business.get(0).get("businessId").toString(), List.class) : new ArrayList<>();
                //fetch dealer id for each business
                if (!businessList.isEmpty()) {
                    String selectSql = "SELECT * FROM config_tekion_business WHERE dealer_id IS NOT NULL AND dealer_id != '' AND business_id IN (:businessList)";
                    MapSqlParameterSource param = new MapSqlParameterSource().addValue("businessList", businessList);
                    resultList = namedParameterJdbcTemplate.queryForList(selectSql, param);
                }
            }
        } catch (Exception e) {
            logger.error("Error in getDealersList() : " + e.getMessage());
        }
        return resultList;
    }

    /**
     * Function to insert repair order details
     *
     * @param businessId
     * @param dataList
     * @return
     */
    public int insertRepairOrderDetails(int businessId, List<Map<String, Object>> dataList) throws JsonParseException {
        int[] result = null;
        try {
            //insert repair orders into database
            final String query = "insert into repair_orders (business_id, repair_order_id, customer_details, vehicle_details, created_time, status, modified_time) values (?,?,?,?,?,?,?)";
            //batch update
            result = jdbcTemplate.batchUpdate(query, new BatchPreparedStatementSetter() {
                public void setValues(PreparedStatement statement, int i) throws SQLException {
                    Map<String, Object> repairOrderMap = dataList.get(i);
                    Map customerDetails = (Map) repairOrderMap.get("customer");
                    JSONObject customerJson = new JSONObject(customerDetails);
                    Map vehicleDetails = (Map) repairOrderMap.get("vehicle");
                    JSONObject vehicleJson = new JSONObject(vehicleDetails);
                    statement.setInt(1, businessId);
                    statement.setString(2, repairOrderMap.get("repairOrderNumber").toString());
                    statement.setString(3, customerJson.toString());
                    statement.setString(4, vehicleJson.toString());
                    statement.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
                    statement.setString(6, "NEW");
                    statement.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
                }

                public int getBatchSize() {
                    return dataList.size();
                }
            });
        } catch (Exception e) {
            logger.error("Error in insertRepairOrderDetails() : " + e.getMessage());
        }
        return result.length;
    }

    /**
     * Function to get previously executed time
     *
     * @param dealerId
     * @return
     */
    public long getPreviouslyExecutedTime(String dealerId, int businessId, String type) {
        long time = 0;
        try {
            switch (type) {
                case "deals":
                    //get last executed time for deals
                    String sql = "SELECT last_executed_time_deals FROM config_tekion_business WHERE dealer_id = ? and business_id = ?";
                    time = jdbcTemplate.queryForObject(sql, new Object[]{dealerId, businessId}, Long.class);
                    break;
                case "repairOrders":
                    //get last executed time for repair orders
                    String repairSql = "SELECT last_executed_time FROM config_tekion_business WHERE dealer_id = ? and business_id = ?";
                    time = jdbcTemplate.queryForObject(repairSql, new Object[]{dealerId, businessId}, Long.class);
                    break;
            }
        } catch (Exception e) {
            logger.info("Last executed time not present");
        }
        return time;
    }

    /**
     * Function to update last executed time
     *
     * @param dealerId
     * @param endTime
     */
    public void updateLastExecutedTime(String dealerId, long endTime, int businessId, String type) {
        try {
            int update = 0;
            switch (type) {
                case "deals":
                    //update last executed time for deals
                    String sql = "UPDATE config_tekion_business SET last_executed_time_deals = ? WHERE dealer_id = ? and business_id = ?";
                    update = jdbcTemplate.update(sql, new Object[]{endTime, dealerId, businessId});
                    break;
                case "repairOrders":
                    //update last executed time for repair orders
                    String repairSql = "UPDATE config_tekion_business SET last_executed_time = ? WHERE dealer_id = ? and business_id = ?";
                    update = jdbcTemplate.update(repairSql, new Object[]{endTime, dealerId, businessId});
                    break;
            }
            if (update > 0) {
                logger.info("Last executed time updated successfully");
            }
        } catch (Exception e) {
            logger.error("Error in updateLastExecutedTime() : " + e.getMessage());
        }
    }

    /**
     * Get app_id and app_secret for tekion
     *
     * @return
     */
    public Map getTekionKeys() {
        Map inputMap = new HashMap();
        try {
            //get app_id and app_secret
            String getTekionKeys = "SELECT app_id, app_secret FROM configs_tekion";
            inputMap = jdbcTemplate.queryForMap(getTekionKeys);
        } catch (Exception e) {
            logger.error("Error in getTekionKeys() : " + e.getMessage());
        }
        return inputMap;
    }

    /**
     * Function to get closed orders
     *
     * @return
     */
    public List<Map<String, Object>> getClosedOrders() {
        List<Map<String, Object>> resultList = new ArrayList<>();
        try {
            //get closed orders
            String selectSql = "SELECT * FROM repair_orders WHERE status = 'NEW'";
            resultList = jdbcTemplate.queryForList(selectSql);
        } catch (Exception e) {
            logger.error("Error in getClosedOrders() : " + e.getMessage());
        }
        return resultList;
    }

    /**
     * Function to get api key by business id
     *
     * @param businessId
     * @return
     */
    public Map getApiKeyByBusinessId(int businessId) {
        logger.info("start get apiKey by businessId in api_key table");
        Map resultMap = new HashMap();
        int enabled = 0;
        try {
            if (businessId > 0) {

                Map businessMap = getBusinessContractDates(businessId);
                String contractStartDate = businessMap.containsKey("contract_start_date") && businessMap.get("contract_start_date") != null ? businessMap.get("contract_start_date").toString() : "";
                String contractEndDate = businessMap.containsKey("contract_end_date") && businessMap.get("contract_end_date") != null ? businessMap.get("contract_end_date").toString() : "";

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                String currentDate = simpleDateFormat.format(timestamp);

                //some old businesses has empty contract end dates
                contractEndDate = Utils.setContractEndDate(contractStartDate, contractEndDate);

                if (Utils.compareDates(contractEndDate, currentDate)) {
                    String sql = "select business_id, api_key, created_time, expiration_time, enabled from api_key where business_id = ? and current_timestamp < expiration_time";
                    List result = jdbcTemplate.queryForList(sql, new Object[]{businessId});
                    if (result.size() > 0) {
                        Map apiKey = (Map) result.get(0);
                        enabled = apiKey.containsKey("enabled") && Utils.isNotNull(apiKey.get("enabled")) ? Integer.parseInt(apiKey.get("enabled").toString()) : 0;
                        if (enabled > 0) {
                            apiKey.put("apiKey", apiKey.remove("api_key"));
                            apiKey.put("expirationTime", apiKey.remove("expiration_time"));
                            apiKey.put("businessId", apiKey.remove("business_id"));

                            apiKey.remove("created_time");
                            apiKey.remove("enabled");
                            resultMap.put("success", true);
                            resultMap.put("result", apiKey);
                        } else {
                            resultMap.put("success", false);
                            resultMap.put("message", "api key is disabled for the account");
                        }
                    } else {
                        resultMap.put("success", false);
                        resultMap.put("message", "no api key found for the account");
                    }
                } else {
                    resultMap.put("success", false);
                    resultMap.put("message", "business is expired");
                }
            } else {
                resultMap.put("success", false);
                resultMap.put("message", "invalid businessId");
            }
            logger.info("end get apiKey by businessId in api_key table");
        } catch (Exception e) {
            logger.error("Error in getApiKeyByBusinessId() : " + e.getMessage());
        }
        return resultMap;
    }

    /**
     * Function to get contract dates for business
     *
     * @param businessId
     * @return
     */
    public Map getBusinessContractDates(Integer businessId) {
        logger.info("business id {}", businessId);
        Map businessMap = new HashMap();
        try {
            List businessList = jdbcTemplate.queryForList("select contract_start_date, contract_end_date from business where business_id = ?", new Object[]{businessId});
            if (businessList.size() > 0) {
                businessMap = (Map) businessList.get(0);
            } else {
                //Fix to login accounts without a start date in the business schedule
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date date = new Date();
                String startDate = dateFormat.format(date);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.add(Calendar.DAY_OF_MONTH, 30);
                String endDate = dateFormat.format(calendar.getTime());
                logger.info("start date {} ", startDate);
                logger.info("end date {}", endDate);
                businessMap.put("contract_start_date", startDate);
                businessMap.put("contract_end_date", endDate);
            }
        } catch (Exception e) {
            logger.error("Error in getBusinessContractDates() : " + e.getMessage());
        }
        return businessMap;
    }

    /**
     * Function to get participant group for tekion
     *
     * @param businessUUID
     * @return
     */
    public String getTekionParticipantGroup(String businessUUID, String actionId) {
        String participantGroupUUID = "";
        try {
            String tableName = "participant_group_" + businessUUID.replace("-", "_");
            String getParticipantGroup = "SELECT participant_group_uuid FROM " + tableName + " WHERE integration_list = '3' AND action_id = ? LIMIT 1";
            participantGroupUUID = jdbcTemplate.queryForObject(getParticipantGroup, new Object[]{actionId}, String.class);
        } catch (Exception e) {
            logger.error("Error in getTekionParticipantGroup() : " + e.getMessage());
        }
        return participantGroupUUID;
    }

    /**
     * Function to get list headers for tekion
     *
     * @return
     */
    public List getTekionListHeaders(String recipeName) {
        List headersList = new ArrayList();
        try {
            String getHeaders = "SELECT headers FROM list_integrations WHERE integration_name = 'tekion' and recipe_name = ?";
            //Map result = jdbcTemplate.queryForMap(getHeaders);
            Map result = jdbcTemplate.queryForMap(getHeaders, new Object[]{recipeName});
            headersList = mapper.readValue(result.get("headers").toString(), List.class);
        } catch (Exception e) {
            logger.error("Error in getTekionListHeaders() : " + e.getMessage());
        }
        return headersList;
    }

    /**
     * Function to update repair order status
     *
     * @param repairOrderId
     */
    public void updateRepairOrderStatus(String repairOrderId) {
        try {
            String updateSql = "update repair_orders set status = 'COMPLETED' where repair_order_id = ?";
            int update = jdbcTemplate.update(updateSql, new Object[]{repairOrderId});
            if (update > 0) {
                logger.info("Repair order status updated successfully");
            }
        } catch (Exception e) {
            logger.error("Error in updateRepairOrderStatus() : " + e.getMessage());
        }
    }

    /**
     * Function to insert repair order details
     *
     * @param businessId
     * @param dataList
     * @return
     */
    public int insertDeals(int businessId, List<Map<String, Object>> dataList) throws JsonParseException {
        int[] result = null;
        try {
            //insert repair orders into database
            final String query = "insert into tekion_deals (business_id, deal_id, customer_details, vehicle_details, payment_details, trade_ins, created_time, status, modified_time) values (?,?,?,?,?,?,?,?,?)";
            //batch update
            result = jdbcTemplate.batchUpdate(query, new BatchPreparedStatementSetter() {
                public void setValues(PreparedStatement statement, int i) throws SQLException {
                    Map<String, Object> dealsMap = dataList.get(i);
                    List<Map<String, Object>> customerDetails = (List<Map<String, Object>>) dealsMap.get("customers");
                    JSONArray customersJson = new JSONArray(customerDetails);
                    List<Map<String, Object>> vehicleDetails = (List<Map<String, Object>>) dealsMap.get("vehicles");
                    JSONArray vehicleJson = new JSONArray(vehicleDetails);
                    Map paymentMap = (Map) dealsMap.get("dealPayment");
                    JSONObject paymentJson = new JSONObject(paymentMap);
                    List<Map<String, Object>> tradeIns = (List<Map<String, Object>>) dealsMap.get("tradeIns");
                    JSONArray tradeInsJson = new JSONArray(tradeIns);
                    statement.setInt(1, businessId);
                    statement.setString(2, dealsMap.get("id").toString());
                    statement.setString(3, customersJson.toString());
                    statement.setString(4, vehicleJson.toString());
                    statement.setString(5, paymentJson.toString());
                    statement.setString(6, tradeInsJson.toString());
                    statement.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
                    statement.setString(8, "NEW");
                    statement.setTimestamp(9, new Timestamp(System.currentTimeMillis()));
                }
                public int getBatchSize() {
                    return dataList.size();
                }
            });
        } catch (Exception e) {
            logger.error("Error in insertDeals() : " + e.getMessage());
        }
        return result.length;
    }

    /**
     * Function to get closed deals
     *
     * @return
     */
    public List<Map<String, Object>> getClosedDeals() {
        List<Map<String, Object>> resultList = new ArrayList<>();
        try {
            //get closed orders
            String selectSql = "SELECT * FROM tekion_deals WHERE status = 'NEW'";
            resultList = jdbcTemplate.queryForList(selectSql);
        } catch (Exception e) {
            logger.error("Error in getClosedDeals() : " + e.getMessage());
        }
        return resultList;
    }

    /**
     * Function to update deal status
     *
     * @param dealId
     */
    public void updateDealStatus(String dealId) {
        try {
            String updateSql = "update tekion_deals set status = 'COMPLETED' where deal_id = ?";
            int update = jdbcTemplate.update(updateSql, new Object[]{dealId});
            if (update > 0) {
                logger.info("Deal status updated successfully");
            }
        } catch (Exception e) {
            logger.error("Error in updateDealStatus() : " + e.getMessage());
        }
    }
}
