package com.dropthought.portal.dao;

import com.dropthought.portal.model.ConnectedAccountBean;
import com.dropthought.portal.util.Utils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class MultiAccountDAO extends BaseDAO{

    final static String UPDATE_ACCOUNT_ENTRY = "update connected_accounts set modified_time = current_timestamp, activation = ?, refresh_token = ?, expiration_time = ? where user_id = ? and connected_user_id = ?";

    final static String GET_ACCOUNT_ENTRY = "SELECT activation, expiration_time, user_id,business_id, connected_user_id,  connected_business_id,  refresh_token " +
            " from connected_accounts where expiration_time < now() and (refresh_token is not null or refresh_token != '') limit 1000";

    /**
     * Method to update switch user connected account
     * @param connectedAccount
     */
    public int updateUserConnectedAccount(ConnectedAccountBean connectedAccount) {
        try{
            return jdbcTemplate.update( UPDATE_ACCOUNT_ENTRY,
                    connectedAccount.getActivation(),
                    connectedAccount.getRefreshToken(),
                    connectedAccount.getExpiration(),
                    connectedAccount.getUserId(),
                    connectedAccount.getConnectedUserId());
        } catch (Exception e) {
            logger.error("Error in updateUserConnectedAccount {}", e.getMessage());
        }
        return -1;
    }

    public int[][] updateUserConnectedAccountsBatch(List<ConnectedAccountBean> connectedAccounts) {
        try {
            return jdbcTemplate.batchUpdate(
                    UPDATE_ACCOUNT_ENTRY,
                    connectedAccounts,
                    100, // Batch size
                    (ps, connectedAccount) -> {
                        ps.setInt(1, connectedAccount.getActivation());
                        ps.setString(2, connectedAccount.getRefreshToken());
                        ps.setString(3, connectedAccount.getExpiration());
                        ps.setInt(4, connectedAccount.getUserId());
                        ps.setInt(5, connectedAccount.getConnectedUserId());
                    }
            );
        } catch (Exception e) {
            logger.error("Error in updateUserConnectedAccountsBatch {}", e.getMessage());
        }
        return null;
    }


    /**
     * Method to get switch user connected account
     * @return
     */
    public List<ConnectedAccountBean> getUserConnectedAccounts() {
        List<ConnectedAccountBean> connectedAccountsList = new ArrayList<>();

        try {
            List<Map<String, Object>> resultMap = jdbcTemplate.queryForList(GET_ACCOUNT_ENTRY);

            if(resultMap.size() > 0){
                //iterate over the result map and set the values in the response bean
                for(Map<String, Object> map : resultMap){
                    ConnectedAccountBean connectedAccounts = new ConnectedAccountBean();
                    connectedAccounts.setUserId((int) map.get("user_id"));
                    connectedAccounts.setActivation((int) map.get("activation") );
                    connectedAccounts.setConnectedUserId((int) map.get("connected_user_id"));
                    connectedAccounts.setRefreshToken((String) map.get("refresh_token"));
                    connectedAccounts.setConnectedBusinessId((int) map.get("connected_business_id"));
                    connectedAccounts.setExpiration(Utils.isNotNull(map.get("expiration_time")) ? map.get("expiration_time").toString() : null);
                    connectedAccountsList.add(connectedAccounts);
                }
            }
        } catch (Exception e) {
            logger.error("Error in getUserConnectedAccount {}", e.getMessage());
        }
        return connectedAccountsList;
    }
}
