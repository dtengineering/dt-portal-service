package com.dropthought.portal.dao;

import com.dropthought.portal.util.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ClientDAO {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * JdbcTemplate for DB Connectivity.
     */
    @Autowired
    @Qualifier("jdbcDt")
    JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("jdbcNamedDt")
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    /**
     * function to retrieve a business by businessUUID
     *
     * @param businessUUID business uniqueId
     * @return businessId
     */
    public int retrieveBusinessIDByUUID(String businessUUID) {
        int businessId = 0;
        try {
            Object businessIdObject = jdbcTemplate.queryForObject(Constants.GET_BUSINESS_ID_BY_UUID, Integer.class, businessUUID);
            if(businessIdObject != null)
                businessId = (int) businessIdObject;
        } catch (EmptyResultDataAccessException emptyResultDataAccessException) {
            return 0;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return businessId;
    }

    /**
     * function to retrieve a business by businessUUID
     *
     * @param businessUUID
     * @return
     */
    public Map<String, Object> retrieveBusinessByUUID(String businessUUID) {
        Map<String, Object> businessMap = new HashMap();
        logger.info("START retrieveBusinessByUUID");
        try {
            businessMap = jdbcTemplate.queryForMap(Constants.GET_BUSINESS_BY_UUID, businessUUID);
        } catch (EmptyResultDataAccessException emptyResultDataAccessException) {
            return businessMap;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("END retrieveBusinessByUUID");
        return businessMap;
    }

    /**
     * Get businessuuid based on businessid
     *
     * @param businessId
     * @return
     */
    public String retrieveBusinessUUIDByID(int businessId) {
        String businessUUID = Constants.EMPTY_STRING;
        try {
            logger.info("START retrieveBusinessUUIDByID");
            try {
                Object businessIdObject = jdbcTemplate.queryForObject(Constants.GET_BUSINESS_UUID_BY_ID, String.class, businessId);
                if(businessIdObject != null)
                    businessUUID =  businessIdObject.toString();
            } catch (EmptyResultDataAccessException emptyResultDataAccessException) {
                return Constants.EMPTY_STRING;
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("END retrieveBusinessUUIDByID");
        return businessUUID;
    }

    /**
     * Get business id by business config uuid
     *
     * @param configUUID
     * @return
     */
    public int retrieveBusinessIdByConfigUUID(String configUUID) {
        logger.info("START retrieveBusinessIdByConfigUUID");
        int businessId = 0;
        try {
            Object businessIdObject = jdbcTemplate.queryForObject(Constants.GET_BUSINESS_ID_BY_CONFIG_UUID, Integer.class, configUUID);
            if(businessIdObject != null)
                businessId = (int) businessIdObject;
        } catch (EmptyResultDataAccessException emptyResultDataAccessException) {
            return 0;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("END retrieveBusinessIdByConfigUUID");
        return businessId;
    }

    /**
     * Method to get business contract start and end date by businessid
     * @param businessId
     * @return
     */
    public Map getBusinessContractDates(Integer businessId) {
        Map businessMap = new HashMap();
        logger.info("START getBusinessContractDates");
        try {
            //get business contract dates
            businessMap = jdbcTemplate.queryForMap(Constants.GET_BUSINESS_CONTRACT_DATES_BY_ID, businessId);
        } catch (EmptyResultDataAccessException emptyResultDataAccessException) {
            return businessMap;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("END getBusinessContractDates");
        return businessMap;
    }
}
