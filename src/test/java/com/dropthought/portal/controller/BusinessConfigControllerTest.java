package com.dropthought.portal.controller;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.hamcrest.CoreMatchers;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringJUnit4ClassRunner.class)
@Configuration
@ComponentScan("com.dropthought.portal.controller")
@TestPropertySource("classpath:test-config.properties")
public class BusinessConfigControllerTest {

    private static String defaultBusinessUUID = null;
    private static JSONObject jsonReq = null;
    private static String businessConfigUUID = null;
    private static Integer businessConfigId = null;

    private static String businessConfigBusinessUUID = null;
    @Autowired
    Environment env;

    /**
     * Load setup before test gets execution. set default values and urls.
     */
    @Before
    public void setup() {
        //Load base config settings
        RestAssured.baseURI = env.getProperty("portal.url");
        System.out.println(RestAssured.baseURI);
        defaultBusinessUUID = env.getProperty("default.business");
        businessConfigBusinessUUID = env.getProperty("businessConfigBusinessUUID");

        //Create business config, so that businessConfigUUID will be set
        if (businessConfigUUID == null) {
            createBusinessConfigTest();
        }
    }


    /**
     * version test
     */
    @Test
    public void versionTest(){
        try {
            get("version")
                    .then()
                    .statusCode(200)
                    .assertThat()
                    .body("VERSION", is("1.0.0"));

            //assertThat(response.jsonPath().getString("success"), true);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Test to create business configurations by businessUUID
     */
    @Test
    public void createBusinessConfigTest() {
        try {
            String file = getFileContent("createBusinessConfig.json");
            Response response = given()
                    .contentType(ContentType.JSON).
                            when().body(file).
                            post("businessconfig");
            response.then().statusCode(200);
            response.then().body("success", CoreMatchers.is(true));

            System.out.println(response.getBody().asString());
            String intentAnalysis = response.path("result.intentAnalysis");

            businessConfigUUID = response.path("result.businessConfigId"); // ex : c2ed92e2-c72c-403a-8ed5-29c9531b3ed7
            businessConfigId = response.path("result.business_config_id"); // ex : 456

            System.out.println("intentAnalysis" + intentAnalysis);
            //Validate success has true
            assertThat(response.jsonPath().getString("success"), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Test to get business configurations by businessConfigUUID
     */
    @Test
    public void businessConfigByConfigUUIDTest() {
        try {
            Response response = given()
                    .pathParam("businessConfigUUID", businessConfigUUID)
                    .when()
                    .get("businessconfig/{businessConfigUUID}")
                    .then()
                    .statusCode(200).extract().response();

            String businessConfigUUID = response.path("result.businessConfigId");
            System.out.println("businessConfigUUID" + businessConfigUUID);
            System.out.println(response.getBody().asString());
            assertThat(response.jsonPath().getString("success"), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Test to update business configurations by businessUUID
     */
    @Test
    public void updateBusinessConfigTest() {
        try {
            String file = getFileContent("updateBusinessConfig.json");
            Response response = given()
                    .contentType(ContentType.JSON)
                    .pathParam("clientId", businessConfigBusinessUUID)
                            .when().body(file).
                            put("businessconfig/business/{clientId}");
            response.then().statusCode(200);
            response.then().body("success", CoreMatchers.is(true));

            System.out.println(response.getBody().asString());
            String intentAnalysis = response.path("result.intentAnalysis");

            businessConfigUUID = response.path("result.businessConfigId"); // ex : c2ed92e2-c72c-403a-8ed5-29c9531b3ed7
            businessConfigId = response.path("result.business_config_id"); // ex : 456

            System.out.println("After update intentAnalysis is 0 == " + intentAnalysis);
            //Validate success has true
            assertThat(response.jsonPath().getString("success"), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Test to get business configurations by businessUUID
     */
    @Test
    public void businessConfigClientUUIDTest() {
        try {
            Response response = given()
                    .pathParam("clientId", businessConfigBusinessUUID)
                    .when()
                    .get("businessconfig/business/{clientId}")
                    .then()
                    .statusCode(200).extract().response();

            String intentAnalysis = response.path("result.intentAnalysis");
            System.out.println("intentAnalysis value by clientUUID" + intentAnalysis);
            System.out.println(response.getBody().asString());
            assertThat(response.jsonPath().getString("success"), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Test to delete business configurations by businessConfigUUID
     */
    @Test
    public void deleteConfigByConfigUUIDTest() {
        try {
            Response response = given()
                    .pathParam("businessConfigUUID", businessConfigUUID)
                    .when()
                    .delete("businessconfig/{businessConfigUUID}")
                    .then()
                    .statusCode(200).extract().response();

            //String businessConfigUUID = response.path("result.businessConfigId");
            //System.out.println("businessConfigUUID" + businessConfigUUID);
            System.out.println(response.getBody().asString());
            assertThat(response.jsonPath().getString("success"), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * method to get file content from json template
     *
     * @param template
     * @return
     */
    protected String getFileContent(String template) {
        StringBuilder result = new StringBuilder();

        try {
            InputStream in = this.getClass().getClassLoader().getResourceAsStream("payload/" + template);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            for (String line; (line = reader.readLine()) != null; ) {
                result.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.toString();
    }
}
