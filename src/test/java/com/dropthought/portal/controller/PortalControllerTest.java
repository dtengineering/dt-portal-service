package com.dropthought.portal.controller;

import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

@RunWith(SpringJUnit4ClassRunner.class)
@Configuration
@ComponentScan("com.dropthought.portal.controller")
@TestPropertySource("classpath:test-config.properties")
public class PortalControllerTest {

    private static String defaultBusinessUUID = null;

    @Autowired
    Environment env;

    /**
     * Load setup before test gets execution. set default values and urls.
     */
    @Before
    public void setup() {
        //Load base config settings
        RestAssured.baseURI = env.getProperty("portal.url");
        System.out.println(RestAssured.baseURI);
        defaultBusinessUUID = env.getProperty("default.business");
    }


    /**
     * version test
     */
    @Test
    public void versionTest(){
        try {
            get("version")
                    .then()
                    .statusCode(200)
                    .assertThat()
                    .body("VERSION", is("1.0.0"));

            //assertThat(response.jsonPath().getString("success"), true);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Test to get business configurations by businessUUID
     */
    @Test
    public void businessConfigTest() {
        try {
            given()
                    .pathParam("clientId", defaultBusinessUUID)
                    .when()
                    .get("businessconfig/business/{clientId}")
                    .then()
                    .statusCode(200)
                    .assertThat()
                    .body("success", is(true));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * method to get file content from template(json)
     *
     * @param template
     * @return
     */
    private String getFileContent(String template) {
        StringBuilder result = new StringBuilder();

        try {
            BufferedReader reader;
            try (InputStream in = this.getClass().getClassLoader().getResourceAsStream("payload/" + template)) {
                reader = new BufferedReader(new InputStreamReader(in));
            }
            for (String line; (line = reader.readLine()) != null; ) {
                result.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.toString();
    }
}
