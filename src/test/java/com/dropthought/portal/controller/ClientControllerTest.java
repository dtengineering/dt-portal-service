package com.dropthought.portal.controller;

import com.dropthought.portal.util.Utils;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Test Client CRUD operation API's via Portal service
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Configuration
@ComponentScan("com.dropthought.portal.controller")
@TestPropertySource("classpath:test-config.properties")
public class ClientControllerTest {

    private static String defaultBusinessUUID = null;
    private static JSONObject jsonReq = null;
    private static JSONObject jObject = new JSONObject();
    private static JSONObject jObjectGlobal = new JSONObject();
    private static String createdBusinessUUID = null;

    private static String businessName = null;
    private static String fullName = null;
    private static String userEmail = null;
    private static String phone = null;
    private static String noOfEmployees = null;
    private static String contractStartDate = null;
    private static String contractEndDate = null;
    private static JSONArray industry = null;
    private static String title = null;
    private static String country = null;

    @Autowired
    Environment env;

    /**
     * Load setup before test gets execution. set default values and urls.
     */
    @Before
    public void setup() {
        //Load base config settings
        RestAssured.baseURI = env.getProperty("portal.url");
        System.out.println(RestAssured.baseURI);
        defaultBusinessUUID = env.getProperty("default.business");


        String jsonStr = getFileContent("createClientConfig.json");
        jsonReq = new JSONObject(jsonStr);
        businessName = jsonReq.getString("businessName");
        fullName = jsonReq.getString("fullName");
        userEmail = jsonReq.getString("email");
        phone = jsonReq.getString("phone");
        noOfEmployees = jsonReq.getString("noOfEmployees");
        contractStartDate = jsonReq.getString("contractStartDate");
        contractEndDate = jsonReq.getString("contractEndDate");
        industry = jsonReq.getJSONArray("industry");
        title = jsonReq.getString("title");
        country = jsonReq.getString("country");
    }

    /**
     * Create client(business) using test assured post call.
     * Read client information from createClientConfig.json
     *
     */
    @Test
    public void case1CreateClientTest() {
        try {
            //String jsonStr = getFileContent("createClientConfig.json");
            JSONObject jObject = new JSONObject();
            jObject.put("businessName", businessName + System.currentTimeMillis());
            jObject.put("email", userEmail + System.currentTimeMillis() + "@yopmail.com");
            jObject.put("fullName", fullName);
            jObject.put("noOfEmployees", noOfEmployees);
            jObject.put("phone", phone);
            jObject.put("contractStartDate", contractStartDate);
            jObject.put("contractEndDate", contractEndDate);
            jObject.put("industry", industry);
            jObject.put("title", title);
            jObject.put("country", country);
            jObjectGlobal = jObject;
            Response response = given()
                    .contentType(ContentType.JSON).
                            when().body(jObject.toString()).
                            post("client");
            response.then().statusCode(200);
            response.then().body("success", is(true));

            System.out.println(response.getBody().asString());
            createdBusinessUUID = response.path("result.businessUUID[0]");

            System.out.println("createdBusinessUUID" + createdBusinessUUID);
            //Validate success has true
            assertThat(response.jsonPath().getString("success"), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Client update using PUT call
     * Update reason and csAccess
     */
    @Test
    public void case2UpdateClientTest() {
        try {
            //if clinetId is not available then we will invoke create client
            if (Utils.emptyString(createdBusinessUUID)) {
                case1CreateClientTest();
            }
            //PUT call with update
            JSONObject jObject = new JSONObject();
            jObject.put("csAccess", 0);
            jObject.put("reason", "Good");

            Response response = given()
                    .contentType(ContentType.JSON).
                            pathParam("clientId", createdBusinessUUID).
                            when().body(jObject.toString()).
                            put("client/{clientId}");
            response.then().statusCode(200);
            response.then().body("success", is(true));

            System.out.println(response.getBody().asString());
            String reason = response.path("result.reason[0]");

            System.out.println("reason" + reason);
            //Validate success has true
            assertThat(response.jsonPath().getString("success"), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get client API test
     */
    @Test
    public void case3GetClientTest() {
        try {
            //if clinetId is not available then we will invoke create client
            if (Utils.emptyString(createdBusinessUUID)) {
                case1CreateClientTest();
            }

            Response response = given()
                    .contentType(ContentType.JSON).
                            pathParam("clientId", createdBusinessUUID).
                            when().body("").
                            get("client/{clientId}");
            response.then().statusCode(200);
            response.then().body("success", is(true));

            System.out.println(response.getBody().asString());
            String businessName = response.path("result.businessName[0]");

            System.out.println("businessName" + businessName);
            //Validate success has true
            assertThat(response.jsonPath().getString("success"), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get all client API test
     */
    @Test
    public void case4GetAllClientTest() {
        try {
            Response response = given()
                    .contentType(ContentType.JSON).
                            //pathParam("clientId", createdBusinessUUID).
                            when().body("").
                            get("client");
            response.then().statusCode(200);
            response.then().body("success", is(true));

            //System.out.println(response.getBody().asString());
            String businessName = response.path("result.businessName[1]");
            String businessName2 = response.path("result.businessName[2]");
            System.out.println("businessName ==>" + businessName);
            System.out.println("businessName2 ==>" + businessName2);
            //Validate success has true
            assertThat(response.jsonPath().getString("success"), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get client summary API test
     */
    @Test
    public void case5GetClientSummaryTest() {
        try {
            Response response = given()
                    .contentType(ContentType.JSON).
                    //pathParam("clientId", createdBusinessUUID).
                            when().body("").
                            get("client/summary");
            response.then().statusCode(200);
            response.then().body("success", is(true));

            System.out.println(response.getBody().asString());
            String businessName = response.path("result.businessName[1]");
            String businessName2 = response.path("result.businessName[2]");
            System.out.println("businessName ==>" + businessName);
            System.out.println("businessName2 ==>" + businessName2);
            //Validate success has true
            assertThat(response.jsonPath().getString("success"), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Enable csuser API test
     */
    @Test
    public void case6EnableCSUerTest() {
        try {
            //if clientId is not available then we will invoke create client
            if (Utils.emptyString(createdBusinessUUID)) {
                case1CreateClientTest();
            }

            System.out.println("Client Id" + createdBusinessUUID);
            JSONObject jObject = new JSONObject();

            jObjectGlobal = jObject;
            Response response = given()
                    .contentType(ContentType.JSON).
                            pathParam("clientId", createdBusinessUUID).
                            when().body(jObject.toString()).
                            post("client/{clientId}/access");
            response.then().statusCode(200);
            response.then().body("success", is(true));

            System.out.println(response.getBody().asString());
            String message = response.path("message");

            System.out.println("cs enable message" + message);
            //Validate success has true
            assertThat(response.jsonPath().getString("success"), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Disable CSuser API test
     */
    @Test
    public void case7DisableCSUerTest() {
        try {
            //if clientId is not available then we will invoke create client
            if (Utils.emptyString(createdBusinessUUID)) {
                case1CreateClientTest();
                case6EnableCSUerTest();
            }
            System.out.println("Client Id" + createdBusinessUUID);
            JSONObject jObject = new JSONObject();

            jObjectGlobal = jObject;
            Response response = given()
                    .contentType(ContentType.JSON).
                            pathParam("clientId", createdBusinessUUID).
                            when().body(jObject.toString()).
                            delete("client/{clientId}/access");
            response.then().statusCode(200);
            response.then().body("success", is(true));

            System.out.println(response.getBody().asString());
            String message = response.path("message");

            System.out.println("cs disable message" + message);
            //Validate success has true
            assertThat(response.jsonPath().getString("success"), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Delete client API test
     */
    @Test
    public void case8DeleteClientTest() {
        try {
            //if clientId is not available then we will invoke create client
            if (Utils.emptyString(createdBusinessUUID)) {
                case1CreateClientTest();
            }
            System.out.println("Client Id" + createdBusinessUUID);
            JSONObject jObject = new JSONObject();

            Response response = given()
                    .contentType(ContentType.JSON).
                            pathParam("clientId", createdBusinessUUID).
                            when().body(jObject.toString()).
                            delete("client/{clientId}");
            response.then().statusCode(200);
            response.then().body("success", is(true));

            System.out.println(response.getBody().asString());
            String message = response.path("message");

            System.out.println("delete message" + message);
            //Validate success has true
            assertThat(response.jsonPath().getString("success"), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get client programs list API test
     */
    @Test
    public void case9GetProgramClientTest() {
        try {
            //if clientId is not available then we will invoke create client
            if (Utils.emptyString(createdBusinessUUID)) {
                case1CreateClientTest();
            }

            Response response = given()
                    .contentType(ContentType.JSON).
                            pathParam("clientId", createdBusinessUUID).
                            when().body("").
                            get("client/{clientId}/programs");
            response.then().statusCode(200);
            response.then().body("success", is(true));

            System.out.println(response.getBody().asString());
            String businessName = response.path("result.businessName[0]");

            System.out.println("businessName" + businessName);
            //Validate success has true
            assertThat(response.jsonPath().getString("success"), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * method to get file content from json template
     *
     * @param template
     * @return
     */
    protected String getFileContent(String template) {
        StringBuilder result = new StringBuilder();

        try {
            InputStream in = this.getClass().getClassLoader().getResourceAsStream("payload/" + template);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            for (String line; (line = reader.readLine()) != null; ) {
                result.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.toString();
    }


}
